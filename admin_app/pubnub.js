import PubNub from 'pubnub/dist/web/pubnub.min.js';

export const pubnub = new PubNub({
  publishKey: process.env.PUBLISH_KEY,
  subscribeKey: process.env.SUBSCRIBE_KEY,
  error: function (error) {
    console.error('PubNub: ', error);
  },
});
