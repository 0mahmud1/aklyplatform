'use strict'

var webpack = require('webpack');
var path = require('path');
var config = require('config');
var extractTextWebpackPlugin = require('extract-text-webpack-plugin');
var webpackManifestPlugin = require('webpack-manifest-plugin');
var cleanWebpackPlugin = require('clean-webpack-plugin');
var postcssImport = require('postcss-import');
var postcssUrl = require('postcss-url');
var autoprefixer = require('autoprefixer');
var CompressionPlugin = require('compression-webpack-plugin');

// start
// for execute additional shell command before and after webpack command

var exec = require('child_process').exec;

function puts(error, stdout, stderr) {
    console.log(stdout);
}

function WebpackShellPlugin(options) {
  var defaultOptions = {
    onBuildStart: [],
    onBuildEnd: []
  };

  this.options = Object.assign(defaultOptions, options);
}

WebpackShellPlugin.prototype.apply = function(compiler) {
  const options = this.options;

  compiler.plugin("compilation", compilation => {
    if(options.onBuildStart.length){
        console.log("Executing pre-build scripts");
        options.onBuildStart.forEach(script => exec(script, puts));
    }
  });

  compiler.plugin("emit", (compilation, callback) => {
    if(options.onBuildEnd.length){
        console.log("Executing post-build scripts");
        options.onBuildEnd.forEach(script => exec(script, puts));
    }
    callback();
  });
};

// end webpack additional shell command

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: {
    'dashboard': './index.js',
  },
  output: {
    path: path.join(__dirname, 'public/build'),
    filename: '[name].[hash].js'
  },
  module: {
    noParse: [
      /aws\-sdk/,
    ],
    loaders: [{
      test: /\.css$/,
      include: [path.resolve(__dirname)],
      loader: extractTextWebpackPlugin.extract('style-loader', 'css-loader!postcss-loader')
    },

      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: __dirname,
        loaders: ['babel']
      },

      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'node_modules/react-switch-button/src/react-switch-button.js')
        ],
        loaders: ['babel']
      },

      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10000&name=images/[name].[ext]',
        include: [
          path.resolve(__dirname)
        ]
      },

      {
        test: /\.(svg|eot|woff|woff2|ttf|otf)$/,
        loader: 'url-loader?limit=10000&name=fonts/[name].[ext]',
        include: [
          path.resolve(__dirname)
        ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(config.util.getEnv('NODE_ENV')),
        BASE_URL: JSON.stringify(config.get('BASE_URL')),
        API_URL: JSON.stringify(config.get('API_URL')),
        AUTH0_BASE_URL: JSON.stringify(config.get('AUTH0_BASE_URL')),
        AUTH0_CLIENT_ID: JSON.stringify(config.get('AUTH0_CLIENT_ID')),
        AUTH0_CONNECTION: JSON.stringify(config.get('AUTH0_CONNECTION')),
        AKLY_LOGO: JSON.stringify(config.get('AKLY_LOGO')),
        PAGE_DEFAULT: JSON.stringify(config.get('PAGE_DEFAULT')),
        LIMIT_DEFAULT: JSON.stringify(config.get('LIMIT_DEFAULT')),
        AWS_KEY_ID: JSON.stringify(config.get('AWS.ACCESS_KEY_ID')),
        AWS_SECRET_KEY: JSON.stringify(config.get('AWS.SECRET_ACCESS_KEY')),
        AWS_BUCKET_NAME: JSON.stringify(config.get('AWS.BUCKET_NAME')),
        SUBSCRIBE_KEY: JSON.stringify(config.get('PUBNUB.SUBSCRIBE_KEY')),
        PUBLISH_KEY: JSON.stringify(config.get('PUBNUB.PUBLISH_KEY')),
      },
    }),
    new extractTextWebpackPlugin("[name].[hash].css"),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),

    new webpackManifestPlugin({
      basePath: '/build/'
    }),

    new cleanWebpackPlugin(['build'], {
      root: path.join(__dirname, 'public'),
      verbose: true
    }),

    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      output: {
        comments: false
      },
      compressor: {
        warnings: false
      }
    }),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
    new WebpackShellPlugin({
        onBuildEnd: ['python public/build.py']
   }),

  ],

  postcss: function (webpack) {
    return [
      postcssImport({addDependencyTo: webpack}),
      postcssUrl(),
      autoprefixer({
        browsers: ['last 10 versions']
      })
    ];
  },
  resolve: {
    root: [
      path.resolve('.'),
    ]
  },
};
