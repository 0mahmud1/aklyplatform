import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory, IndexRedirect } from 'react-router';

import CoreLayout from './modules/core/layouts';
import HomeComponent from './modules/core/components/home';
import HomeClientComponent from './modules/core/components/homeClient';

import NotFoundComponent from './modules/core/components/not-found';

import TagRoute from './modules/tags/routes';
import AuthRoute from './modules/authenticate/routes';
import FoodRoute from './modules/foods/routes';
import UserLayout from './modules/foodproviders/routes';
import CategoryRoute from './modules/categories/routes';
import HubRoute from './modules/hubs/routes';
import UiRoute from './modules/ui/routes';
import LiveViewRoute from './modules/liveView/routes';
import MealPlan from './modules/mealplan/routes';
import DeliveryBoyRoute from './modules/dboys/routes';
import VanRoute from './modules/vans/routes';
import OrderRoute from './modules/orders/routes';
import DeliveryRoute from './modules/deliveries/routes';
import DashBoardCustomer from './modules/customers/routes';
import SimulatorContainer from './modules/simulator/containers';
import SettingsContainer from './modules/authenticate/container/SettingsContainer';
import CouponRoute from './modules/coupons/routes/index';
import * as AuthService from './modules/services/auth';
// import config from './config';

class Routes extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={CoreLayout} onEnter={AuthService.isDashAuthRouter}>
          <IndexRoute component={HomeComponent}/>
          <Route path={`simulator`} component={SimulatorContainer} />
          <Route path={`settings`} component={SettingsContainer} />
          {TagRoute()}
          {UserLayout()}
          {CategoryRoute()}
          {HubRoute()}
          {FoodRoute()}
          {CategoryRoute()}
          {UiRoute()}
          {LiveViewRoute()}
          {MealPlan()}
          {DeliveryBoyRoute()}
          {VanRoute()}
          {OrderRoute()}
          {DeliveryRoute()}
          {DashBoardCustomer()}
          {CouponRoute()}
        </Route>
        {AuthRoute()}
        <Route path='*' component={NotFoundComponent}/>
      </Router>
    );
  }
}

export default Routes;
