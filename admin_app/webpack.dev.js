'use strict';

var config = require('config');
var webpack = require('webpack');
var path = require('path');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var webpackManifestPlugin = require('webpack-manifest-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var postcssImport = require('postcss-import');
var postcssUrl = require('postcss-url');
var autoprefixer = require('autoprefixer');

/*var definePlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify("faysal"),
  __PRERELEASE__: JSON.stringify(JSON.parse(process.env.BUILD_PRERELEASE || 'false'))
});*/

module.exports = {
  devtool: 'eval-source-map',
  entry: {
    'dashboard': './index.js',
  },
  debug: true,
  output: {
    path: path.join(__dirname, 'public/build'),
    filename: '[name].js'
  },
  module: {
    noParse: [
      /aws\-sdk/,
    ],
    loaders: [
      {
        test: /\.css$/,
        include: [path.resolve(__dirname)],
        loader: ExtractTextWebpackPlugin.extract('style-loader', 'css-loader!postcss-loader')
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: __dirname,
        loaders: ['babel']
      },
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'node_modules/react-switch-button/src/react-switch-button.js')
        ],
        loaders: ['babel']
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10000&name=images/[name].[ext]',
        include: [
          path.resolve(__dirname)
        ]
      },
      {
        test: /\.(svg|eot|woff|woff2|ttf|otf)$/,
        loader: 'url-loader?limit=10000&name=fonts/[name].[ext]',
        include: [
          path.resolve(__dirname)
        ]
      },
      {
        test: /\.json$/,
        loader: 'json'
      },

      {
        test: /jquery-mousewheel/,
        loader: "imports?define=>false&this=>window"
    },
  	{
          test: /malihu-custom-scrollbar-plugin/,
          loader: "imports?define=>false&this=>window"
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(config.util.getEnv('NODE_ENV')),
        BASE_URL: JSON.stringify(config.get('BASE_URL')),
        API_URL: JSON.stringify(config.get('API_URL')),
        AUTH0_BASE_URL: JSON.stringify(config.get('AUTH0_BASE_URL')),
        AUTH0_CLIENT_ID: JSON.stringify(config.get('AUTH0_CLIENT_ID')),
        AUTH0_CONNECTION: JSON.stringify(config.get('AUTH0_CONNECTION')),
        AKLY_LOGO: JSON.stringify(config.get('AKLY_LOGO')),
        PAGE_DEFAULT: JSON.stringify(config.get('PAGE_DEFAULT')),
        LIMIT_DEFAULT: JSON.stringify(config.get('LIMIT_DEFAULT')),
        AWS_KEY_ID: JSON.stringify(config.get('AWS.ACCESS_KEY_ID')),
        AWS_SECRET_KEY: JSON.stringify(config.get('AWS.SECRET_ACCESS_KEY')),
        AWS_BUCKET_NAME: JSON.stringify(config.get('AWS.BUCKET_NAME')),
        SUBSCRIBE_KEY: JSON.stringify(config.get('PUBNUB.SUBSCRIBE_KEY')),
        PUBLISH_KEY: JSON.stringify(config.get('PUBNUB.PUBLISH_KEY')),
      },
    }),
    new ExtractTextWebpackPlugin("[name].css"),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),

    new webpackManifestPlugin({
      basePath: '/build/'
    }),

    new CleanWebpackPlugin(['build', 'index.html'], {
      root: path.join(__dirname, 'public'),
      verbose: true
    })

  ],
  postcss: function (webpack) {
    return [
      postcssImport({addDependencyTo: webpack}),
      postcssUrl(),
      autoprefixer({
        browsers: ['last 10 versions']
      })
    ];
  },
  resolve: {
    alias: {
      devConfig: 'dev.config.js',
    },
    root: [
      path.resolve('.'),
      path.resolve("./node_modules"),
      // path.resolve('./provider_modules'),
      // path.resolve('./dashboard_modules')
    ]
  },
};
