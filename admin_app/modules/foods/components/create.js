import React, {Component, PropTypes} from 'react'
//import aws from '../../configs/awsConfig.js';
import ReactDOM from "react-dom";
import Dropzone from 'react-dropzone';
import RichTextEditor from 'react-rte';
import {Link, browserHistory} from 'react-router'
import _ from 'lodash'
import ReactDom from 'react-dom'
import $ from 'jquery'
import toastr from 'toastr'
import 'select2'

// import 'select2/dist/js/select2.min'
// import 'select2/dist/css/select2.min.css'
class FoodCreateComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: RichTextEditor.createEmptyValue(),
      nutritionFiles: null,
      ingredientsArray: []
    };
  }
  componentDidMount() {
    this.initializeSelect2()
  }

  componentDidUpdate(prevProps) {
    if(prevProps.tags.length !== this.props.tags.length) {
      this.initializeSelect2()
    }

  }

  initializeSelect2() {
    let select2settings = {
      tags: true,
      placeholder: 'Select tags',
      tokenSeparators: [',']
    };

    $('#food-tags').select2(select2settings);
  }
  onDrop(files) {
    console.log('Received nutrion files: ', files);
    this.setState({
      nutritionFile: files[0]
    });
  }
  onDrop1(files) {
    console.log('Received food files: ', files);
    this.setState({
      file: files[0]
    });
  }
  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading'>
              <h3>Create New Dishes</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }

  form() {
    return (
      <div className="mp_body">
        <div className="row">
          <div className="col-lg-12">
            <div className="meal_edit">
              <form>
                {this.firstRow()}
                {this.secondRow()}
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }

  uploadModal() {
    return (
      <div className="modal fade aklyEditImagePrompt uploadprompt" tabIndex="-1" role="dialog"
           aria-labelledby="myModalLabel">
        <div className="modal-dialog uploadprompt_dialog" role="document">
          <div className="modal-content uploadprompt_content">
            <div className="modal-body">
              <div className="uploadprompt_button">
                <form action="">
                  <div className="form-group uploadbutn_wrapper">
                    <label htmlFor="uploadAttachment" className="uploadbutn_label">Change Image</label>
                    <input type="file" name="uploadAttachment" id="uploadAttachment"
                           onChange={this.imageSelected.bind(this)} className="uploadbutn_file"/>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  uploadVideoModal() {
    return (
      <div className="modal fade aklyEditVideoPrompt uploadprompt" tabIndex="-1" role="dialog"
           aria-labelledby="myModalLabel">
        <div className="modal-dialog uploadprompt_dialog" role="document">
          <div className="modal-content uploadprompt_content">
            <div className="modal-body">
              <div className="uploadprompt_button">
                <form action="">
                  <div className="form-group uploadbutn_wrapper">
                    <label htmlFor="uploadAttachment" className="uploadbutn_label">Change Video</label>
                    <input type="file" name="uploadAttachment" id="uploadAttachment" className="uploadbutn_file"/>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  rteDraftChange(value) {
    //console.log(value.toString('html'))
    this.setState({value});
  }

  firstRow() {
    return (
      <div className="meal_basiceditsec">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div className="geninput">
              <div className="form-group">
                <label className="geninput_label" htmlFor="exampleInputAmount">Name</label>
                <div className="input-group geninput_group geninput_group_add">
                  <input type="text" className="form-control geninput_formctrl" ref='name'/>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div className="geninput">
              <div className="form-group">
                <label className="geninput_label" htmlFor="exampleInputAmount">Price(QAR)</label>
                <div className="input-group geninput_group geninput_group_add">
                  <input type="text" className="form-control geninput_formctrl" ref='price'/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  tagSection() {
    return this.props.tags.length > 0 ?
      <div className="form-group">
        <label htmlFor="comment" className="geninput_label">Tag this food item</label>
        <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
          <select id="food-tags" multiple="multiple">
            {this.generateTags()}
          </select>

        </div>
        <div className="geninput_helptext">
          <p>Select multiple to index this item properly.</p>
        </div>
      </div>
       : <Link to='/tags/create' style={{'color':'red'}} >No tag available. click here to add</Link>
  }

  // addIngerdient(e) {
  //   e.preventDefault();
  //   let newIngredient = `ingredient-${this.state.ingredients.length}`;
  //   this.setState({ ingredients: this.state.ingredients.concat([newIngredient]) });
  // }
  //
  // removeIngerdient(e) {
  //   e.preventDefault();
  //
  //   // detecting the id of the to be removed element and remove from state array
  //   let removedItemIndex = this.state.ingredients.indexOf(e.target.parentNode.id);
  //   this.state.ingredients.splice(removedItemIndex, 1);
  //
  //   this.setState({
  //     ingredients: this.state.ingredients
  //   });
  // }

  // renderIngredient() {
  //   let ingredientArr = this.state.ingredients;
  //   let hide = ingredientArr.length === 1 ? 'hide' : '';
  //
  //   return ingredientArr.map(el => {
  //     return (
  //         <div className='ingredients' key={el} id={el}>
  //         <input type='text' className='ingredients-input'/>
  //         <span className='glyphicon glyphicon-plus ingredients-add' onClick={this.addIngerdient.bind(this)}></span>
  //         <span className={'glyphicon glyphicon-remove ingredients-remove ' + hide} onClick={this.removeIngerdient.bind(this)}></span>
  //       </div>
  //     )
  //   })
  // }


  inputIngredients(event) {
    if(event.key==='Enter'){
      let ingdt = document.getElementById('ingredients').value
        if(ingdt!==''){
            this.setState({
                ingredientsArray: this.state.ingredientsArray.concat([ingdt])
            }, ()=>{
                document.getElementById('ingredients').value=''
                document.getElementById('ingredientInfo').innerHTML=''
            })
          }
    }
    else {
        document.getElementById('ingredientInfo').innerHTML='Hit Enter to add ingredient'
    }
  }

  deleteIngredient(indx){
      let newIngredients = this.state.ingredientsArray
      newIngredients.splice(indx,1)
    this.setState({
        ingredientsArray: newIngredients
    })
  }
  renderIngredients(){
    return this.state.ingredientsArray.map((item, key)=>{
      return(
          <li key={key} className='ingredients-listitem'>
              <span className='ingredients-text'>{item}</span>
              <span className='glyphicon glyphicon-remove ingredients-remove' onClick={this.deleteIngredient.bind(this, key)}></span>
          </li>
      )
    })
  }

  secondRow() {

    return (
      <div className="meal_tagsimgeditsec">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div className="meal_desc">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Description</label>
                  </div>
                  <textarea className="form-control geninput_textareactrl textarea_descheight" ref='description'/>
                </div>
              </div>
            </div>

            <div className="meal_categories">
              <div className="geninput">
                {/*<div className="form-group">
                  <label htmlFor="comment" className="geninput_label">Tag this food item</label>
                  <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
                    /!*<select id="food-tags" multiple="multiple">
                      {this.generateTags()}
                    </select>*!/
                    {this.tagSection()}
                  </div>
                  <div className="geninput_helptext">
                    <p>Select multiple to index this item properly.</p>
                  </div>
                </div>*/}
                {this.tagSection()}
              </div>
            </div>

            <div className="meal_ingredients">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Ingredients</label>
                  </div>
                  <div className='ingredients'>
                    <input type="text" id="ingredients" className='ingredients-input' onKeyPress={this.inputIngredients.bind(this)}/><span id="ingredientInfo"></span>
                    <ul>
                        {this.renderIngredients()}
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className='row mb-25'>

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="Calorie">Calorie</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='calorie'/>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="protein">Protein</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='protein'/>
                  </div>
                </div>
              </div>
            </div>

             <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="fat">Fat</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='fat'/>
                  </div>
                </div>
              </div>
            </div>

             <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="carbs">Carbs</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='carbs'/>
                  </div>
                </div>
              </div>
            </div>

            </div>





            {/*<div className="meal_recipe">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Recipe</label>
                  </div>
                  /!*<textarea className="form-control geninput_textareactrl textarea_descheight" ref='recipe'/>*!/
                  <MegadraftEditor ref='recipe' editorState={this.state.editorState} />
                </div>
              </div>
            </div>*/}

            <div className='meal_recipe row'>
                <div className="col-lg-12">
                 <h4 className='meal_recipe_heading'>Recipe</h4>
                  <RichTextEditor
                    value={this.state.value}
                    onChange={this.rteDraftChange.bind(this)}
                    editorClassName="rte-editor"
                  />
                </div>
            </div>

          </div>
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 meal_imageditwrapper mb-25">
            {/*<div className="meal_imagedit meal_imageadd">
              <div className="meal_image">
                {this.state && this.state.imagePreviewUrl ? <img src={this.state.imagePreviewUrl} alt=""/> :
                  <img src="/images/no-image.png" alt=""/> }
              </div>
              <div className="meal_addicon" data-toggle="modal" data-target=".aklyEditImagePrompt">
                <a href="#" className="addbtn">
									<span className="icon_plus">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet">
											<path
                        d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/>
										</svg>
									</span>
                </a>
              </div>
            </div>*/}
            <label style={{marginBottom: '10px'}}>Upload Food Image</label>
            <Dropzone
              ref="dropzone1"
              onDrop={this.onDrop1.bind(this)}
              className='food-dropzone'
            >
              {this.state.file ?
                <div className="dropzone-food-imgwrapper">
                  <img src={this.state.file.preview} />
                </div>
                : <div className="text-center" style={{lineHeight: '400px'}}>Try dropping some files here, or click to select files to upload.</div>
              }
            </Dropzone>
          </div>

          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label style={{marginBottom: '10px'}}>Upload Nutrition Image</label>
            <Dropzone
              ref="dropzone"
              onDrop={this.onDrop.bind(this)}
              className='food-dropzone'
              >
              {this.state.nutritionFile ?
                <div className="dropzone-food-imgwrapper">
                  <img src={this.state.nutritionFile.preview} />
                </div>
                : <div className="text-center" style={{lineHeight: '400px'}}>Try dropping some files here, or click to select files to upload.</div>
              }
            </Dropzone>
          </div>
          <hr/>

          {/*<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">*/}
          {/*<input type="file" ref='videofile' onChange={this.videoSelected.bind(this)}/>*/}
          {/*</div>*/}
          {/*<div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 meal_imageditwrapper">*/}
          {/*<div className="meal_imagedit meal_imageadd">*/}
          {/*<div className="meal_image">*/}
          {/*{this.state && this.state.videoPreviewUrl ? <video src={this.state.videoPreviewUrl} alt=""/> : <img src="/images/no-image.png" alt=""/> }*/}
          {/*</div>*/}
          {/*<div className="meal_addicon" data-toggle="modal" data-target=".aklyEditVideoPrompt">*/}
          {/*<a href="#" className="addbtn">*/}
          {/*<span className="icon_plus">*/}
          {/*<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet">*/}
          {/*<path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/>*/}
          {/*</svg>*/}
          {/*</span>*/}
          {/*</a>*/}
          {/*</div>*/}
          {/*</div>*/}
          {/*</div>*/}
        </div>

        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div className="link_youtube">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="carbs">Youtube URL</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='yUrl'/>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12"></div>

        </div>


      </div>
    )
  }

  actionButtons() {
    return (
      <div className="meal_actionbtn actionbutn">
        <div className="row">
          <div className="col-md-12 col-lg-12">
            {
              this.props.foods.uploading ?
                <div className="actionbtn_save">
                  <button type="submit" className="btn butn butn_default" disabled>
										<span className="actionbtn_save_loader actionButnSaveLoader">
											<svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
												<path fill="none" d="M0 0h100v100H0z"/><defs><filter id="a" x="-100%" y="-100%" width="300%"
                                                                             height="300%"><feOffset result="offOut"
                                                                                                     in="SourceGraphic"/><feGaussianBlur
                        result="blurOut" in="offOut"/><feBlend in="SourceGraphic" in2="blurOut"/></filter></defs><path
                        d="M10 50s0 .5.1 1.4c0 .5.1 1 .2 1.7 0 .3.1.7.1 1.1.1.4.1.8.2 1.2.2.8.3 1.8.5 2.8.3 1 .6 2.1.9 3.2.3 1.1.9 2.3 1.4 3.5.5 1.2 1.2 2.4 1.8 3.7.3.6.8 1.2 1.2 1.9.4.6.8 1.3 1.3 1.9 1 1.2 1.9 2.6 3.1 3.7 2.2 2.5 5 4.7 7.9 6.7 3 2 6.5 3.4 10.1 4.6 3.6 1.1 7.5 1.5 11.2 1.6 4-.1 7.7-.6 11.3-1.6 3.6-1.2 7-2.6 10-4.6 3-2 5.8-4.2 7.9-6.7 1.2-1.2 2.1-2.5 3.1-3.7.5-.6.9-1.3 1.3-1.9.4-.6.8-1.3 1.2-1.9.6-1.3 1.3-2.5 1.8-3.7.5-1.2 1-2.4 1.4-3.5.3-1.1.6-2.2.9-3.2.2-1 .4-1.9.5-2.8.1-.4.1-.8.2-1.2 0-.4.1-.7.1-1.1.1-.7.1-1.2.2-1.7.1-.9.1-1.4.1-1.4V54.2c0 .4-.1.8-.1 1.2-.1.9-.2 1.8-.4 2.8-.2 1-.5 2.1-.7 3.3-.3 1.2-.8 2.4-1.2 3.7-.2.7-.5 1.3-.8 1.9-.3.7-.6 1.3-.9 2-.3.7-.7 1.3-1.1 2-.4.7-.7 1.4-1.2 2-1 1.3-1.9 2.7-3.1 4-2.2 2.7-5 5-8.1 7.1L70 85.7c-.8.5-1.7.9-2.6 1.3l-1.4.7-1.4.5c-.9.3-1.8.7-2.8 1C58 90.3 53.9 90.9 50 91l-3-.2c-1 0-2-.2-3-.3l-1.5-.2-.7-.1-.7-.2c-1-.3-1.9-.5-2.9-.7-.9-.3-1.9-.7-2.8-1l-1.4-.6-1.3-.6c-.9-.4-1.8-.8-2.6-1.3l-2.4-1.5c-3.1-2.1-5.9-4.5-8.1-7.1-1.2-1.2-2.1-2.7-3.1-4-.5-.6-.8-1.4-1.2-2-.4-.7-.8-1.3-1.1-2-.3-.7-.6-1.3-.9-2-.3-.7-.6-1.3-.8-1.9-.4-1.3-.9-2.5-1.2-3.7-.3-1.2-.5-2.3-.7-3.3-.2-1-.3-2-.4-2.8-.1-.4-.1-.8-.1-1.2v-1.1-1.7c-.1-1-.1-1.5-.1-1.5z"
                        filter="url(#a)"><animateTransform attributeName="transform" type="rotate" from="0 50 50"
                                                           to="360 50 50" repeatCount="indefinite" dur="0.7s"/></path>
											</svg>
										</span>
                    <span className="actionbtn_save_text">Saving..</span>
                  </button>
                </div> :
                <div className="actionbtn_save">
                  <button type="submit" onClick={this.handleSubmit.bind(this)} className="btn butn butn_default">Add
                  </button>
                </div>
            }

            <div className="actionbtn_cancel">
              <button type="button" className="btn butn butn_cancel"
                      onClick={() => browserHistory.push('/foods')}>Cancel
              </button>
            </div>
            <div className="actionbtn_back">
              <button type="button" className="btn butn butn_back"
                      onClick={() => browserHistory.push('/foods')}>Back
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  imageSelected(event) {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    //console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
    $('.aklyEditImagePrompt').modal('hide');
    //console.log(reader.result);
  }

  // videoSelected(event){
  // 	event.preventDefault();
  // 	console.log(event)
  // 	let reader = new FileReader();
  // 	let file = event.target.files[0];
  // 	console.log('file: ', file);
  // 	console.log('event.target.files[0]: ', event.target.files[0]);
  //
  // 	reader.onloadend = () => {
  // 		this.setState({
  // 			videoFile: file,
  // 			videoPreviewUrl: reader.result
  // 		});
  // 	}
  // 	reader.readAsDataURL(file);
  // 	// $('.aklyEditVideoPrompt').modal('hide');
  // }

  generateTags() {
    return  this.props.tags.map((tag) =>
      <option key={tag._id} value={tag._id}>{tag.name}</option>
    )
  }

  parseTags(ids, list) {
    console.group('parseTags');
    console.log(list);
    console.log(ids);
    console.groupEnd();
    return _.filter(list, function (tag) {
      return _.includes(ids, tag._id)
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    console.group('state in handle submit');
    console.log(this.state.file);
    console.log('ref -> ', this.refs);
    console.groupEnd();

    if (!this.state || !this.state.file) {
      toastr.info('please upload a nice picture of the dish');
      return;
    }
    window.myrefs = this.refs;

    let food = {
      name: this.refs.name.value,
      price: parseInt(this.refs.price.value),
      recipe: this.state.value.toString('html'),
      ingredients: this.state.ingredientsArray.join(),
      calorie: this.refs.calorie.value,
      protein: this.refs.protein.value,
      fat: this.refs.fat.value,
      carbs: this.refs.carbs.value,
      description: this.refs.description.value,
      tags: this.parseTags($('#food-tags').val(), this.props.tags),
      file: this.state.file,
      nutritionFile: this.state.nutritionFile,
      youtubeUrl: this.refs.yUrl.value
      // videoFile: this.state.videoFile
    };

    console.log(food);

    if (!food.name) {
      toastr.warning('Food Name is required');
    } else if (!food.price) {
      toastr.warning('Food Price is required');
    } else if (!food.tags) {
      toastr.warning('Food Tag is required');
    } else {
      this.props.createFood(food);
    }

    //console.log('my food ..', food);

  }

  render() {
    //console.log('food create render')
    return (
      <div>
        {this.heading()}
        {this.form()}
        {this.actionButtons()}
        {this.uploadModal()}
        {/*{this.uploadVideoModal()}*/}
      </div>
    );
  }
}

export default FoodCreateComponent;
