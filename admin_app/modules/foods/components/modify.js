'use strict'

import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import Dropzone from 'react-dropzone';
import {browserHistory, Link} from 'react-router'
import RichTextEditor from 'react-rte';
import _ from 'lodash'
import toastr from 'toastr'
import $ from 'jquery'
import 'select2'

class FoodModifyComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ingredientsArray: this.props.food.ingredients ? this.props.food.ingredients.split(',') : [],
      value: RichTextEditor.createValueFromString(props.food.recipe, 'html')
    };
  }
  componentDidMount() {

    if (this.props.food && this.props.tags) {
      console.log('cdmt')
      this.initializeSelect2()
    }

  }

  componentDidUpdate(prevProps, prevState) {
      if(prevProps.food.ingredients !== this.props.food.ingredients) {
        this.setState({
            ingredientsArray: this.props.food.ingredients.split(',')
        })
      }
      if(prevProps.tags.length !== this.props.tags.length) {
          this.initializeSelect2()
      }
  }

  initializeSelect2() {
    let select2Container = $('#food-tags-edit'),
      select2settings = {
        tags: true,
        placeholder: 'Select tags',
        tokenSeparators: [',']
      };

    select2Container.select2(select2settings);

    //console.log('change trigger : ', this.props.food.tags.map(tag => JSON.stringify(tag))[0] === this.props.tags[2], this.props.tags);

    select2Container.val(this.props.food.tags.map(tag => tag._id)).trigger('change');
  }

  onDrop(files) {
    console.log('Received nutrion files: ', files);
    this.setState({
      nutritionFile: files[0]
    });
  }
  onDrop1(files) {
    console.log('Received food files: ', files);
    this.setState({
      file: files[0]
    });
  }

  inputIngredients(event) {
      if(event.key==='Enter'){
          let ingdt = document.getElementById('ingredients').value
          if(ingdt!==''){
              this.setState({
                  ingredientsArray: this.state.ingredientsArray.concat([ingdt])
              }, ()=>{
                  document.getElementById('ingredients').value=''
                  document.getElementById('ingredientInfo').innerHTML=''
              })
          }
      }
      else {
          document.getElementById('ingredientInfo').innerHTML='Hit Enter to add ingredient'
      }
  }

  deleteIngredient(indx){
      let newIngredients = this.state.ingredientsArray
      newIngredients.splice(indx,1)
      this.setState({
          ingredientsArray: newIngredients
      })
  }
  renderIngredients(){
        return this.state.ingredientsArray.map((item, key)=>{
          if(item!==""){
              return(
                  <li key={key} className='ingredients-listitem'>
                    <span className='ingredients-text'>{item}</span>
                    <span className='glyphicon glyphicon-remove ingredients-remove' onClick={this.deleteIngredient.bind(this, key)}></span>
                  </li>
              )
          }
        })
  }

  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading heading-flex'>
              <h3 className='heading-flexratio'>Edit Food Item</h3>
                <button type="button" className="btn butn butn_danger" data-toggle="modal"
                        data-target="#foodDeleteConfirm">Delete
                </button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  firstRow() {
    return (
      <div className="meal_basiceditsec">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div className="geninput">
              <div className="form-group">
                <label className="geninput_label" htmlFor="exampleInputAmount">Name</label>
                <div className="geninput_group">
                  <input type="text" className="form-control geninput_formctrl" ref='name'
                         defaultValue={this.props.food.name}/>
                  <div className="input-group-addon">

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div className="geninput">
              <div className="form-group">
                <label className="geninput_label" htmlFor="exampleInputAmount">Price(QAR)</label>
                <div className="geninput_group">
                  <input type="text" className="form-control geninput_formctrl" ref='price'
                         defaultValue={this.props.food.price}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  secondRow() {
    return (
      <div className="meal_tagsimgeditsec">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div className="meal_desc">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Description</label>
                    <textarea
                      className="form-control geninput_textareactrl textarea_descheight"
                      defaultValue={this.props.food.description}
                      ref='description'
                      data-insert="mealDescDefaultVal"/>
                  </div>

                </div>
              </div>
            </div>

            <div className="meal_categories">
              <div className="geninput">
                {this.tagSection()}
              </div>
            </div>

            <div className="meal_ingredients">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Ingredients</label>
                  </div>
                  <div className='ingredients'>
                    <input type="text" id="ingredients" className='ingredients-input' onKeyPress={this.inputIngredients.bind(this)}/><span id="ingredientInfo"></span>
                    <ul>
                        {this.renderIngredients()}
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            {/*<div className="meal_recipe">
              <div className="geninput">
                <div className="form-group geninput_textarea">
                  <div className="geninput_textarea_lwi">
                    <label htmlFor="comment" className="geninput_label">Recipe</label>
                    <textarea className="form-control geninput_textareactrl textarea_descheight"
                              defaultValue={this.props.food.recipe}
                              data-insert="mealRecipecDefaultVal" ref='recipe'/>
                  </div>

                </div>
              </div>
            </div>*/}

             <div className='meal_recipe row mb-25'>
                <div className="col-lg-12">
                 <h4 className='meal_recipe_heading'>Recipe</h4>
                  <RichTextEditor
                    value={this.state.value}
                    onChange={this.rteDraftChange.bind(this)}
                    editorClassName="rte-editor"
                  />
                </div>
            </div>

            <div className='row mb-25'>
             <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="Calorie">Calorie</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='calorie'
                           defaultValue={this.props.food.calorie}/>
                  </div>
                </div>
              </div>
            </div>

             <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="protein">Protein</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='protein'
                           defaultValue={this.props.food.protein}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="fat">Fat</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='fat'
                           defaultValue={this.props.food.fat}/>
                  </div>
                </div>
              </div>
            </div>

             <div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div className="geninput">
                <div className="form-group">
                  <label className="geninput_label" htmlFor="carbs">Carbs</label>
                  <div className="input-group geninput_group geninput_group_add">
                    <input type="text" className="form-control geninput_formctrl" ref='carbs'
                           defaultValue={this.props.food.carbs}/>
                  </div>
                </div>
              </div>
            </div>


              </div>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 meal_imageditwrapper mb-25">
            {/*
              <div className="meal_imagedit">
                <div className="meal_image">
                  <img
                    src={this.state && this.state.imagePreviewUrl ? this.state.imagePreviewUrl : this.props.food.images[0]}
                    alt='no image found'/>
                </div>
                <div className="meal_editicon" data-toggle="modal" data-target=".aklyEditImagePrompt">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 112.5" preserveAspectRatio="xMidYMid meet">
                    <path
                      d="M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z"/>
                  </svg>
                </div>
              </div>
            */}

            <label style={{marginBottom: '10px'}}>Upload Food Image</label>
            <Dropzone
              ref="dropzone1"
              onDrop={this.onDrop1.bind(this)}
              className='food-dropzone'
              >
              <div className="dropzone-food-imgwrapper">
                <img src={this.state && this.state.file ? this.state.file.preview : this.props.food.images[0]} />
              </div>
            </Dropzone>
          </div>
          <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label style={{marginBottom: '10px'}}>Upload Nutrition Image</label>
            <Dropzone
              ref="dropzone"
              onDrop={this.onDrop.bind(this)}
              className='food-dropzone'
              >
              <div className="dropzone-food-imgwrapper">
                <img src={this.state && this.state.nutritionFile ? this.state.nutritionFile.preview : this.props.food.nutritionImage } />
              </div>
            </Dropzone>
          </div>
          <hr/>
          <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <div className="geninput">
              <div className="form-group">
                <label className="geninput_label" htmlFor="carbs">Youtube URL</label>
                <div className="input-group geninput_group geninput_group_add">
                  <input type="text" className="form-control geninput_formctrl" ref='yUrl' defaultValue={this.props.food.youtubeUrl}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  form() {
    return (
      <div className="mp_body">
        <div className="row">
          <div className="col-lg-12">
            <div className="meal_edit">
              <form>
                {this.firstRow()}
                {this.secondRow()}
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }

  actionButtons() {
    return (
      <div className="meal_actionbtn actionbutn">
        <div className="row">
          <div className="col-md-12 col-lg-12">
            <div className="actionbtn_save">
              <button type="submit" onClick={this.handleSubmit.bind(this)} className="btn butn butn_default">Save
              </button>
            </div>
            <div className="actionbtn_cancel">
              <button type="button" className="btn butn butn_cancel"
                      onClick={() => browserHistory.push('/foods')}>Cancel
              </button>
            </div>
            {/*<div className="actionbtn_back">*/}
              {/*<button type="button" className="btn butn butn_back"*/}
                      {/*onClick={() => browserHistory.push('/foods')}>Back*/}
              {/*</button>*/}
            {/*</div>*/}
          </div>
        </div>
      </div>
    )
  }

  rteDraftChange(value) {
    //console.log(value.toString('html'))
    this.setState({value});
  }

  uploadModal() {
    return (
      <div className="modal fade aklyEditImagePrompt uploadprompt" tabIndex="-1" role="dialog"
           aria-labelledby="myModalLabel">
        <div className="modal-dialog uploadprompt_dialog" role="document">
          <div className="modal-content uploadprompt_content">
            <div className="modal-body">
              <div className="uploadprompt_button">
                <form action="">
                  <div className="form-group uploadbutn_wrapper">
                    <label htmlFor="uploadAttachment" className="uploadbutn_label">Change Image</label>
                    <input type="file" name="uploadAttachment" id="uploadAttachment"
                           onChange={this.imageSelected.bind(this)} className="uploadbutn_file"/>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  deleteModal() {
    return(
      <div
        id='foodDeleteConfirm'
        className='modal fade delprompt'
        tabIndex='-1'
        role='dialog'>
        <div className='modal-dialog delprompt_dialog' role='document'>
          <div className='modal-content delprompt_content'>
            <div className='modal-body'>
              <div className="delprompt_text">
                <p>Do you want to delete this dish?</p>
              </div>
            </div>
            <div className='modal-footer'>
              <button type="button" className="btn butn butn_danger" data-dismiss="modal" onClick={this.props.deleteFood.bind(this)}>Delete</button>
              <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

  tagSection() {
    return this.props.tags.length > 0 ?
      <div className="form-group">
        <label htmlFor="comment" className="geninput_label">Tag this food item</label>
        <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
          <select id="food-tags-edit" multiple="multiple">
            {this.generateTags()}
          </select>

        </div>
        <div className="geninput_helptext">
          <p>Select multiple to index this item properly.</p>
        </div>
      </div>
      : <Link to='/tags/create' style={{'color':'red'}} >No tag available. click here to add</Link>
  }

  parseTags(ids,list) {
    return _.filter(list, function (tag) {
      return _.includes(ids, tag._id)
    })
  }

  generateTags() {
    return  this.props.tags.map((tag) =>
      <option key={tag._id} value={tag._id}>{tag.name}</option>
    )
  }

  imageSelected(event) {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    //console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
    //console.log(reader.result);
    $('.aklyEditImagePrompt').modal('hide');
  }

  render() {
    return (
      <div>
        {
          /*<div className='row'>
           <div className='col-xs-12'>
           <h3>Information Food</h3>
           { this.props.food
           ? <form onSubmit={this.handleSubmit.bind(this)}>
           <div className='form-group'>
           <label htmlFor='ID'>ID</label>
           <input
           type='text'
           className='form-control'
           ref='id'
           placeholder='ID'
           readOnly='true'
           defaultValue={this.props.food._id}/>
           </div>
           <div className='form-group'>
           <label htmlFor='Name'>Name</label>
           <input
           type='text'
           className='form-control'
           ref='name'
           placeholder='Name'
           defaultValue={this.props.food.name}/>
           </div>
           <div className='form-group'>
           <label htmlFor='Email'>Price</label>
           <input
           type='Number'
           className='form-control'
           ref='price'
           defaultValue={this.props.food.price}/>
           </div>
           <div className='form-group'>
           <label htmlFor='CreatedAt'>CreatedAt</label>
           <input
           type='text'
           className='form-control'
           ref='createdAt'
           placeholder='CreatedAt'
           readOnly='true'
           defaultValue={moment(this.props.food.createdAt).format('MMMM Do YYYY, h:mm:ss a')}/>
           </div>
           <div>
           <select multiple='multiple' id='food-tags-edit' className='select2-general'>
           {this.generateTags()}
           </select>
           </div>
           <div>
           <img className='img-circle'
           src={this.state && this.state.imagePreviewUrl ? this.state.imagePreviewUrl: this.props.food.images[2]}
           alt="no image found"/>
           <input type='file' onChange={this.imageSelected.bind(this)}/>
           </div>
           <button type='submit' className='btn btn-primary btn-flat'>Update</button>
           <a
           className='btn btn-default btn-flat'
           data-toggle='modal'
           data-target='#foodDeleteConfirm'>
           Delete
           </a>
           <a
           className='btn btn-default btn-flat'
           onClick={() => browserHistory.push('/foods')}>
           Back
           </a>
           </form>
           : <h2>Loading .....</h2>
           }
           </div>
           {/!* Food delete confirmation modal *!/}
           <div
           id='foodDeleteConfirm'
           className='modal fade'
           tabIndex='-1'
           role='dialog'>
           <div className='modal-dialog' role='document'>
           <div className='modal-content'>
           <div className='modal-header'>
           <button
           type='button'
           className='close'
           data-dismiss='modal'
           aria-label='Close'>
           <span aria-hidden='true'>&times;</span>
           </button>
           <h4 className='modal-title text-center'>Food Delete Confirmation</h4>
           </div>
           <div className='modal-body'>
           <p>Are you really want to delete this food?</p>
           </div>
           <div className='modal-footer'>
           <div className='row'>
           <div className='col-xs-offset-6 col-xs-3'>
           <button
           type='button'
           className='btn btn-default btn-block btn-flat'
           data-dismiss='modal'>
           No
           </button>
           </div>
           <div className='col-xs-3'>
           <button
           type='button'
           className='btn btn-primary btn-block btn-flat'
           data-dismiss='modal'
           onClick={this.props.deleteFood}>
           Yes
           </button>
           </div>
           </div>
           </div>
           </div>
           </div>
           </div>
           </div>*/
        }
        {this.heading()}
        {this.form()}
        {this.actionButtons()}
        {this.uploadModal()}
        {this.deleteModal()}
      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault()
    const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    const tags = this.parseTags($('#food-tags-edit').val(), this.props.tags);
    const price = ReactDOM.findDOMNode(this.refs.price).value.trim();
    const file = this.state && this.state.file ? this.state.file : undefined;
    const nutritionFile = this.state && this.state.nutritionFile ? this.state.nutritionFile : undefined;
    const ingredients = this.state.ingredientsArray.join();  //ReactDOM.findDOMNode(this.refs.ingredients).value.trim();
    const recipe = /*ReactDOM.findDOMNode(this.refs.recipe).value.trim()*/ this.state.value.toString('html');
    const description = ReactDOM.findDOMNode(this.refs.description).value.trim();
    const calorie = this.refs.calorie.value;
    const protein = this.refs.protein.value;
    const fat = this.refs.fat.value;
    const carbs = this.refs.carbs.value;
    const youtubeUrl = this.refs.yUrl.value;

    //  PROPER VALIDATION REQUIRED
    if (name.length < 1) {
      toastr.warning('Name is required')
      return false
    }
    else if (price <= 0) {
      toastr.warning('Input valid price')
      return false
    }

    // create a object
    const food = {name, price, tags, file,nutritionFile, description, recipe, ingredients, calorie, protein, fat, carbs, youtubeUrl}
    console.log('handleSubmit food=>>>',food.file);
    console.log('handleSubmit nutrition=>>>',food.nutritionFile);
    // call update action
    console.log('update food1', food);
    this.props.updateFood(this.props.food._id, food);
  }
}

FoodModifyComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  food: PropTypes.object,
  deleteFood: PropTypes.func.isRequired,
  updateFood: PropTypes.func.isRequired
}

export default FoodModifyComponent
