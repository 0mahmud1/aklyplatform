'use strict'

import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import toastr from 'toastr'
import {getTags} from '../../tags/actions';
import {readFood, deleteFood, updateFood} from '../actions/index'
import FoodModifyComponent from '../components/modify'
import {browserHistory} from 'react-router'
import LoaderBox from '../../core/components/loader'

class FoodModifyContainer extends Component {

  constructor(props){
    super(props);
    this.state = { upload : false}
  }

  componentWillMount() {
    this.props.readFood(this.props.params._id);
    this.props.getTags();
  }

  deleteFood() {
    this.props.deleteFood(this.props.params._id, function (err, data) {
      if (data) {
        browserHistory.push('/foods');
        toastr.success('Food deleted successfully!');
      }
      if (err) {
        toastr.error(err);
      }
    });
  }

  updateFood(foodId, patch) {
    console.log('foodId',foodId, this);
    this.setState({ upload: true });
    this.props.updateFood(foodId, patch, function (err, data) {
      if (data) {
        //this.setState({ upload: false });
        toastr.success('Food updated Successfully!');
        //cb(null, data);
        browserHistory.push('/foods');
      }
      if (err) {
        //cb(err, null);
        toastr.warning(err.message);
      }
    });
  }

  render() {
    let shouldLoad = (this.props.food && this.props.params._id === this.props.food._id && !this.state.upload)
    console.log('shouldLoad',shouldLoad);
    return shouldLoad ? (

      <FoodModifyComponent
        food={this.props.food}
        updateFood={this.updateFood.bind(this)}
        deleteFood={this.deleteFood.bind(this)}
        createTag={this.props.createTag}
        tags={this.props.tags}
      />
    ) : <LoaderBox />
  }
}


function mapStateToProps(store) {
  return {
    food: store.foods.food,
    tags: store.tags.tagList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    readFood: (_id) => dispatch(readFood(_id)),
    updateFood: (_id,patch,cb) => dispatch(updateFood(_id,patch,cb)),
    deleteFood: (_id,cb) => dispatch(deleteFood(_id, cb)),
    getTags: () => dispatch(getTags())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodModifyContainer)
