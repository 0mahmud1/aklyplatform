'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import toastr from 'toastr'
import {getTags} from '../../tags/actions';

import {createFoodExperiment} from '../actions/index'
import FoodCreateComponent from '../components/create'
import LoaderBox from '../../core/components/loader'

class FoodCreateContainer extends Component {

  constructor(props){
    super(props);
    this.state = { upload : false}
  }
  componentWillMount() {
    this.props.getTags();
  }

  render() {
    let shouldLoad = (this.props.tags && !this.state.upload);
    return (
        shouldLoad ? 
      <FoodCreateComponent
        createFood={this.createFood.bind(this)}
        foods={this.props.foods}
        tags={this.props.tags}
      /> : <LoaderBox /> 
    )
  }

  createFood(Food, cb) {
    // store dispatcher via action
    //this.props.createFood(Food);
    this.setState({ upload: true });
    this.props.createFood(Food, function (err, data) {
      if (data) {
        this.setState({ upload: false });
        toastr.success('Food created');
        browserHistory.push('/foods');
      }
      if (err) {
        cb(err, null);
        toastr.warning(err.message);
      }
    });
  }
}

/*FoodCreateContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 createTag: PropTypes.func.isRequired
 }*/

function mapStateToProps(store) {
  return {
    foods: store.foods,
    tags: store.tags.tagList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getTags: () => dispatch(getTags()),
    createFood : (food, cb) => dispatch(createFoodExperiment(food, cb))
  }
};


// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(mapStateToProps, mapDispatchToProps)(FoodCreateContainer)
