'use strict'

import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getFoods, getFilteredFoods} from '../actions/index'
import FoodListComponent from '../components/list'
import LoaderBox from '../../core/components/loader'


class FoodListContainer extends Component {
  componentWillMount() {
    this.props.getFoods(20,1);
  }

  render() {
    //let shouldLoad = this.props.foods.length > 0 ? true : false
    return (
     // shouldLoad ?
      <FoodListComponent
        getFoods={this.props.getFoods}
        getFilteredFoods={this.props.getFilteredFoods}
        totalPage={this.props.totalPage}
        totalData={this.props.totalData}
        limit={this.props.limit}
        page={this.props.page}
        foods={this.props.foods}/>
        //  : <LoaderBox />
    )
  }
}

/*FoodListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getFoods: PropTypes.func.isRequired,
 foods: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to FoodListContainer
//  > whenever store changes, the FoodListContainer will automatically re-render
function mapStateToProps(store) {
  return {
    foods: store.foods.foodList,
    totalPage: store.foods.totalPage,
    totalData: store.foods.totalData,
    limit: store.foods.limit,
    page: store.foods.page
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getFoods: getFoods,
    getFilteredFoods: getFilteredFoods
  }, dispatch)
}

// const mapDispatchToProps = (dispatch) => {
//   return {
//     getFoods: () => dispatch(getFoods())
//   }
// };

// We don't want to return the plain FoodListContainer (component) anymore,
// we want to return the smart Container
//  > FoodListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(FoodListContainer)
