import { browserHistory } from 'react-router';
import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';
import aws from '../../configs/awsConfig.js';
import Chance from 'chance';
import path from 'path';
import async from 'async';

export function createFoodExperiment(food, cb) {
  return function (dispatch) {
    let functionsToCall = [];
    if(food.file) {
      const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: 'original/' + new Chance().guid() + path.extname(food.file.name),
        ContentType: food.file.type,
        Body: food.file,
        ACL: 'public-read',
      };
      functionsToCall.push(
        function (callback) {
          // uploadToS3(params,callback(null,data))
          uploadToS3(params, function (image) {
            callback(null, image);
          });
        }
      );

    }
    if(food.nutritionFile) {
      const nutritionImage = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: 'original/' + new Chance().guid() + path.extname(food.nutritionFile.name),
        ContentType: food.nutritionFile.type,
        Body: food.nutritionFile,
        ACL: 'public-read',
      };
      functionsToCall.push(
        function (callback) {
          uploadToS3(nutritionImage, function (image) {
            callback(null, image);
          });
        }
      );
    }

    dispatch({ type: 'CREATE_FOOD_PENDING' });
    async.parallel(functionsToCall,
      function (err, results) {
          if (err) {
            // ONE NOTIFICATIONS GOES HERE
          } else {
            // saving food url here with object.
            //let video = results[0];
            console.log('async.parallel=>>>',results);
            if(results[0]) {
              let image = results[0];
              food.images = generateUrlNames(image.Location);
            }
            if(results[1]) {
              let nutritionImage = results[1];
              food.nutritionImage =nutritionImage.Location;
            }
            delete food.file;
            delete food.nutritionFile;

            console.log('FINAL FOOD OBJECT ......', food);
            axios.post('api/foods', food)
              .then((response) => {
                console.log(response);
                toastr.success('Food created Successfully.......!');
                browserHistory.push('/foods');
              })
              .catch((err) => {
                dispatch({ type: 'FOOD_CREATE_REJECTED', payload: err });
                toastr.warning(err.message);
              });
          }
      });

    // let video = uploadVideoToS3(videoParams)
    //
    // dispatch({type: 'CREATE_FOOD_PENDING'})
    //
    // uploadToS3(params, function (image) {
    //     food.images = generateUrlNames(image.Location);
    //     delete food.file;
    //
    //     food.video = video.Location;
    //     delete food.videoFile;
    //
    //     console.log('FINAL FOOD OBJECT ......', food);
    //     axios.post(API_URL + 'api/foods', food, axiosHeader)
    //         .then((response) => {
    //             console.log(response);
    //             dispatch({type: 'CREATE_FOOD_RESOLVED', payload: response.data.data});
    //             if (cb) {
    //                 cb(null, response);
    //             }
    //             //toastr.success('Food created Successfully..!');
    //             //browserHistory.push('/foods');
    //         })
    //         .catch((err) => {
    //             dispatch({type: 'FOOD_CREATE_REJECTED', payload: err});
    //             if (cb) {
    //                 cb(err, null);
    //             }
    //         })
    // });
  };
}

export function createFood(food) {
  return function (dispatch) {
    let s3;
    let images = [];

    // pending dispatch goes here.
    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'original/' + new Chance().guid() + path.extname(food.file.name),
      ContentType: food.file.type,
      Body: food.file,
      ACL: 'public-read',
    };
    const nutritionImage = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'original/' + new Chance().guid() + path.extname(food.nutritionFile.name),
      ContentType: food.nutritionFile.type,
      Body: food.nutritionFile,
      ACL: 'public-read',
    };
    async.parallel([
        function (callback) {
          // uploadToS3(params,callback(null,data))
          uploadToS3(params, function (image) {
            callback(null, image);
          });
        },
        function (callback) {
          uploadToS3(nutritionImage, function (image) {
            callback(null, image);
          });
        },
      ],
      function (err, results) {
          if (err) {
            // ONE NOTIFICATIONS GOES HERE
          } else {
            // saving food url here with object.
            //let video = results[0];
            console.log('async.parallel=>>>',result);
            let image = results[0];
            let nutritionImage = result[1];
            food.images = generateUrlNames(image.Location);
            food.nutritionImage =nutritionImage.Location;
            delete food.file;
            delete food.nutritionFile;

            console.log('FINAL FOOD OBJECT ......', food);
            axios.post('api/foods', food)
              .then((response) => {
                console.log(response);
                toastr.success('Food created Successfully.......!');
                browserHistory.push('/foods');
              })
              .catch((err) => {
                dispatch({ type: 'FOOD_CREATE_REJECTED', payload: err });
                toastr.warning(err.message);
              });
          }
      });
    // s3 = new aws.S3();
    //
    // console.log('params:......', params);
    //
    // // on('httpUploadProgress', function(evt) {
    // // 	console.log('Uploaded :: ' + parseInt((evt.loaded * 100) / evt.total)+'%');
    // // })
    // // - See more at: http://www.tothenew.com/blog/aws-s3-file-upload-with-progress-bar-using-javascript-sdk/#sthash.ixUrCgKu.dpuf
    //
    // s3.upload(params).on('httpUploadProgress', function (evt) {
    //   console.log('Uploaded :: ' + parseInt((evt.loaded * 100) / evt.total) + '%');
    // }).send(function (err, data) {
    //   if (err) {
    //     // ONE NOTIFICATIONS GOES HERE
    //   } else {
    //     // saving food url here with object.
    //     food.images = generateUrlNames(data.Location);
    //     delete food.file;
    //
    //     console.log('FINAL FOOD OBJECT ......', food);
    //     axios.post('api/foods', food)
    //       .then((response) => {
    //         console.log(response);
    //         toastr.success('Food created Successfully.......!');
    //         browserHistory.push('/foods');
    //       })
    //       .catch((err) => {
    //         dispatch({ type: 'FOOD_CREATE_REJECTED', payload: err });
    //         toastr.warning(err.message);
    //       });
    //   }
    // });
  };
}

export function getFoods(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: 'GET_FOOD_PENDING' });
    axios.get('api/foods' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get food... : ', response);
        dispatch({ type: 'GET_FOOD_RESOLVED', payload: response });
        //toastr.success('Food List Loaded');
      })
      .catch((err) => {
        dispatch({ type: 'GET_FOOD_REJECTED', payload: err });
        toastr.error(err);
      });
  };
}

export function getFilteredFoods(options) {
    return function (dispatch) {
        dispatch({ type: 'GET_FOOD_PENDING' });
        let params = {}
        if (options.name){
            params.name = options.name
        }
        if (options.tags){
            params.tags = options.tags
        }
        let url = 'api/foods'
        axios.get(url, {
            params: params,
        }).then((response) => {
            console.log('get filtered food... : ', response);
            dispatch({ type: 'GET_FOOD_RESOLVED', payload: response });
            //toastr.success('Food List Loaded');
        })
            .catch((err) => {
                dispatch({ type: 'GET_FOOD_REJECTED', payload: err });
                toastr.error(err);
            });
    }
}

export function readFood(_id) {
  return function (dispatch) {
    //dispatch({type: constants.READ_USER_PENDING});
    console.log('api params: ', _id);
    axios.get('api/foods/' + _id)
      .then((response) => {
        console.log('get selected food... : ', response);
        dispatch({ type: constants.READ_FOOD_RESOLVED, payload: response });
        //toastr.success('Food found Successfully');
      })
      .catch((err) => {
        dispatch({ type: constants.READ_FOOD_REJECTED, payload: err });
        toastr.error("Food not found");
      });
  };
}

export function updateFood(_id, patch, cb) {
  return function (dispatch) {
    //dispatch({type: constants.UPDATE_FOOD_});
    console.log('PATCH: ', patch, _id);
    if(patch.file || patch.nutritionFile) {
      if (patch.file) {
        const params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Key: 'original/' + new Chance().guid() + path.extname(patch.file.name),
          ContentType: patch.file.type,
          Body: patch.file,
          ACL: 'public-read',
        };
        uploadToS3(params, function (image) {
          console.log('Food Image ',image);
          patch.images = generateUrlNames(image.Location);
          delete patch.file;
          updateFoodAPICall('api/foods/' + _id, patch, dispatch, cb);
        });
      }
      if(patch.nutritionFile) {
        const nutritionImage = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Key: 'original/' + new Chance().guid() + path.extname(patch.nutritionFile.name),
          ContentType: patch.nutritionFile.type,
          Body: patch.nutritionFile,
          ACL: 'public-read',
        };
        uploadToS3(nutritionImage, function (image) {
          console.log('Nutrition Image ',image);
          patch.nutritionImage = image.Location;
          delete patch.nutritionFile;
          updateFoodAPICall('api/foods/' + _id, patch, dispatch, cb);
        });

      }

    }
    else {
      updateFoodAPICall('api/foods/' + _id, patch, dispatch, cb);
    }
  };
}

export function deleteFood(_id, cb) {
  return function (dispatch) {
    axios.delete('api/foods/' + _id)
      .then((response) => {
        console.log('get food... : ', response);
        dispatch({ type: constants.DELETE_FOOD_RESOLVED, payload: response });
        if (cb) {
          cb(null, { type: constants.DELETE_FOOD_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.DELETE_FOOD_REJECTED, payload: err });
        console.warn('Read a Food-Action: ', err);
        if (cb) {
          cb(err, null);
        }
      });
  };
}

function generateUrlNames(original) {
  const chunks = original.split('/');

  // skipping index 1, because there's slash after http, which makes index 1 null.
  let baseUrl = chunks[0] + '//' + chunks[2];
  let directory = chunks[3];
  let fileName = chunks[4];

  // temp
  let mobile = '50x50';
  let tab = '100x100';
  let desktop = '400x400';

  let url1 = baseUrl + '/' + 'resized' + '/' + mobile + '_' + fileName;
  let url2 = baseUrl + '/' + 'resized' + '/' + tab + '_' + fileName;

  return [original, url1, url2];
}

function updateFoodAPICall(url, obj, dispatch, cb) {
  console.log('nested method');
  axios.put(url, obj)
    .then((response) => {
      console.log(response);
      dispatch({ type: constants.UPDATE_FOOD_RESOLVED, payload: response });
      if (cb) {
        cb(null, { type: constants.UPDATE_FOOD_RESOLVED, payload: response });
      }
    })
    .catch((err) => {
      dispatch({ type: constants.UPDATE_FOOD_REJECTED, payload: err });
      if (cb) {
        cb(err, null);
      }
    });
}

function uploadToS3(params, cb) {
  let s3 = new aws.S3();
  s3.upload(params, {queueSize: 5, partSize: 1024 * 1024 * 10}).on('httpUploadProgress', function (evt) {
    console.log('Uploaded :: ', params , " ==== " + parseInt((evt.loaded * 100) / evt.total) + '%');
  }).send(function (err, data) {
    if (err) {
      // ONE NOTIFICATIONS GOES HERE
      toastr.error('Upload error');
    } else {
      cb(data);
    }
  });
}

function uploadVideoToS3(params, cb) {
  let s3 = new aws.S3();
  s3.upload(params).on('httpUploadProgress', function (evt) {
    console.log('Video Uploaded :: ' + parseInt((evt.loaded * 100) / evt.total) + '%');
  }).send(function (err, data) {
    if (err) {
      console.group('error uploadVideoToS3');
      console.log(err);
      console.groupEnd();

      // ONE NOTIFICATIONS GOES HERE
      toastr.error('Upload error');
    } else {
      console.log('uploadVideoTos3', data);
      cb(data); // returns s3 uploaded file
    }
  });
}

// function uploadVideoToS3(params) {
//     return new Promise((resolve, reject) => {
//         try{
//             let s3 = new aws.S3();
//             s3.upload(params).on('httpUploadProgress', function (evt) {
//                 console.log('Video Uploaded :: ' + parseInt((evt.loaded * 100) / evt.total) + '%');
//             }).send(function (err, data) {
//                 if (err) {
//                     // ONE NOTIFICATIONS GOES HERE
//                     toastr.error('Upload error');
//                 } else {
//                     console.log('uploadVideoTos3', data)
//                     // response = data;
//                     resolve(data); // returns s3 uploaded file
//                 }
//             });
//         }
//         catch (e){
//             reject(e)
//         }
//     })
// }
