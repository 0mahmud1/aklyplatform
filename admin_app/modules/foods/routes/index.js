'use strict'

import React, { Component } from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import FoodLayout from '../layout';
import FoodCreateContainer from '../containers/create.js';
import FoodListContainer from '../containers/list';
import FoodModifyContainer from '../containers/modify';
import * as AuthService from '../../services/auth';

export default function () {
return (
	<Route path='foods' component={FoodLayout} onEnter={AuthService.isDashAuthRouter}>
		<IndexRoute component={FoodListContainer} />
		<Route path='create' component={FoodCreateContainer} />
		<Route path=':_id' component={FoodModifyContainer} />
	</Route>
)
}
