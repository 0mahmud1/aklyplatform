import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import toastr from 'toastr';

import { getZones, getDBoys, updateDBoy, getFoodProviders, getVans, updateVan } from '../actions';

import SimulatorComponent from '../components';

class SimulatorContainer extends Component {
  componentDidMount() {
    console.log('simulator');
    this.props.getZones();
  }

  render() {
    return (
      this.props.google ?
      <SimulatorComponent {...this.props}/> : <div className="spinner"></div>
    );
  }
}

function mapStateToProps(store) {
  return {
    google: store.auth.googleLoaded,
    simulator: store.simulator,
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    //createMap: (map) => dispatch({type:'CREATE_MAP',payload: map}),
    getZones: () => dispatch(getZones()),
    selectZone: (zoneId) => dispatch({ type:'SIMULATOR_SELECTED_ZONE', payload: zoneId }),
    getDeliveryBoys: () => dispatch(getDBoys()),
    getFoodProviders: () => dispatch(getFoodProviders()),
    getVans: () => dispatch(getVans()),
    updateDeliveryBoy: (_id, patch) => dispatch(updateDBoy(_id, patch)),
    updateVan: (_id, patch) => dispatch(updateVan(_id, patch)),
  };
};

// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(mapStateToProps, mapDispatchToProps)(SimulatorContainer);
