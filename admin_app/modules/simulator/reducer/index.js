import _ from 'lodash'

const defaultState = {}

const simulatorReducer = (state = defaultState, action) => {
  let zone,dboys,vans,findIndex;
  switch (action.type) {
    // case 'CREATE_MAP':
    //   return Object.assign({}, state, {map: action.payload})

    case 'SIMULATOR_GET_ZONE_RESOLVED':
      return Object.assign({}, state, {zones: action.payload})

    case 'SIMULATOR_SELECTED_ZONE':
      zone = _.find(state.zones,z => z._id === action.payload)
      return Object.assign({}, state, {selectedZone: zone})

    case 'SIMULATOR_GET_DBOY_RESOLVED':
      return Object.assign({}, state, {deliveryBoys: action.payload})

    case 'SIMULATOR_GET_FOOD_PROVIDER_RESOLVED':
      return Object.assign({}, state, {foodProviders: action.payload})

    case 'SIMULATOR_GET_VANS_RESOLVED':
      return Object.assign({}, state, {vans: action.payload})

    case 'SIMULATOR_UPDATE_DBOY_RESOLVED':
      dboys = [...state.deliveryBoys]
      findIndex = _.findIndex(dboys,db => {
        return db._id === action.payload._id
      })

      console.log('find index ', findIndex, dboys.length)

      if(findIndex == -1) {
        console.log('impossible though')
        return state
      }

      dboys[findIndex] = action.payload

      return Object.assign({},state,{deliveryBoys: dboys})

      case 'SIMULATOR_UPDATE_VAN_RESOLVED' :

      vans = [...state.vans]
      findIndex = _.findIndex(vans,vn => {
        return vn._id === action.payload._id
      })

      console.log('find index ', findIndex, vans.length)

      if(findIndex == -1) {
        console.log('impossible though')
        return state
      }

      vans[findIndex] = action.payload

      return Object.assign({},state,{vans: vans})

    default :
      return state
  }
}

export default simulatorReducer