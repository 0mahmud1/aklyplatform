import axios from 'axios';
import toastr from 'toastr';

export function getZones() {
  return function (dispatch) {
    //dispatch({type: 'READ_HUB_PENDING'});
    axios.get('api/zones')
      .then((response) => {
        console.log('all zone... : ', response);
        dispatch({ type: 'SIMULATOR_GET_ZONE_RESOLVED', payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_GET_ZONE_REJECTED', payload: err });
        console.warn('all hub err ', err);
      });
  };
}

export function getFoodProviders() {
  return function (dispatch) {
    return axios.get('api/foodproviders')
      .then((response) => {
        dispatch({ type: 'SIMULATOR_GET_FOOD_PROVIDER_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_GET_FOOD_PROVIDER_REJECTED', payload: err });
      });
  };
}

export function getVans() {
  return function (dispatch) {
    //dispatch({type: 'GET_VANS_PENDING}'});
    return axios.get('api/vans')
      .then((response) => {
        dispatch({ type: 'SIMULATOR_GET_VANS_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_GET_VANS_REJECTED', payload: err });
      });
  };
}

export function getDBoys() {
  return function (dispatch) {
    //dispatch({ type: 'GET_DBOY_PENDING'});

    return axios.get('api/dboys')
      .then((response) => {
        console.log('getDBoys response', response);
        dispatch({ type: 'SIMULATOR_GET_DBOY_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_GET_DBOY_REJECTED', payload: err });
        return err;
      });
  };
}

export function updateDBoy(_id, patch) {
  return function (dispatch) {
    console.log('PATCH: ', patch, _id);
    return axios.put('api/dboys' + '/' + _id, patch)
      .then((response) => {
        console.log('UPDATE dboy... : ', response);
        dispatch({ type: 'SIMULATOR_UPDATE_DBOY_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_UPDATE_DBOY_REJECTED', payload: err });
        return err.data.data;
      });
  };
}

export function updateVan(_id, patch) {
  return function (dispatch) {
    console.log('PATCH: ', patch, _id);
    return axios.put('api/vans' + '/' + _id, patch)
      .then((response) => {
        console.log('UPDATE dboy... : ', response);
        dispatch({ type: 'SIMULATOR_UPDATE_VAN_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'SIMULATOR_UPDATE_VAN_REJECTED', payload: err });
        return err.data.data;
      });
  };
}
