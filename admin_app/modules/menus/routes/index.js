'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import MenuLayout from '../layouts'

import MenuListComponent from '../components/list.js'
import * as AuthService from '../../services/auth';

export default function () {
  return (
    <Route path='menus' component={MenuLayout} onEnter={AuthService.isDashAuthRouter}>
      <IndexRoute component={MenuListComponent} />

    </Route>
  )
}
