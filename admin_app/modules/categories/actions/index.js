import axios from 'axios';
import toastr from 'toastr';

export function getCategories(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: 'CATEGORIES_LIST_PENDING' });
    axios.get('api/categories' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        dispatch({ type: 'CATEGORIES_LIST_RESOLVED', payload: response.data });
      })
      .catch((err) => {
        dispatch({ type: 'CATEGORIES_LIST_REJECTED', payload: err });
        toastr.error(err);
      });
  };
}

export function readCategory(_id) {
  return function (dispatch) {
    axios.get('api/categories/' + _id)
      .then((response) => {
        //console.log(response);
        dispatch({ type: 'CATEGORY_READ_RESOLVED', payload: response });
        //toastr.success('Category loaded successfully!!!');
      })
      .catch((err) => {
        dispatch({ type: 'CATEGORY_READ_REJECTED', payload: err });
        toastr.error("No category found");
      });
  };
}

export function createCategory(category, cb) {
  return function (dispatch) {
    dispatch({ type: 'CATEGORY_CREATE_PENDING' });
    axios.post('api/categories', category)
      .then((response) => {
        //console.log('from createCategory action...', response, response.data, response.data.data);
        dispatch({ type: 'CATEGORY_CREATE_RESOLVED', payload: response.data.data });
	      toastr.success('Category created');
        if (cb) {
          cb(null, response);
        }
      })
      .catch((err) => {
        dispatch({ type: 'CATEGORY_CREATE_REJECTED', payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function updateCategory(patch, _id, cb) {

  return function (dispatch) {
    axios.put('api/categories/' + _id, patch)
      .then((response) => {
        dispatch({ type: 'CATEGORY_UPDATE_RESOLVED', payload: response });
	      toastr.success('category udpated');
        if (cb) {
          cb(null, { type: 'CATEGORY_UPDATE_RESOLVED', payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: 'CATEGORY_UPDATE_REJECTED', payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function deleteCategory(_id, cb) {
  return function (dispatch) {
    axios.delete('api/categories/' + _id)
      .then((response) => {
        dispatch({ type: 'CATEGORY_DELETE_RESOLVED', payload: { _id: _id } });
        if (cb) {
          cb(null, { type: 'CATEGORY_DELETE_RESOLVED', payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: 'CATEGORY_DELETE_REJECTED', payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}
