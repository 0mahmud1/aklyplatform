
'use strict'

import React from 'react'
import {Route, IndexRoute} from 'react-router'

import CategoryLayout from '../layouts'
import CategoryListContainer from '../containers/list'
//import CategoryCreateContainer from '../containers/create'
//import CategoryModifyContainer from '../containers/modify'
import * as AuthService from '../../services/auth';

export default function () {
    return (
        <Route path='categories' component={CategoryLayout} onEnter={AuthService.isDashAuthRouter}>
            <IndexRoute component={CategoryListContainer}/>
	        {/*<Route path='create' component={CategoryCreateContainer} />
            <Route path=':_id' component={CategoryModifyContainer} />*/}
        </Route>
    )
}
