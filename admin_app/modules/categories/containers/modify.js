'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import toastr from 'toastr'

import {readCategory, updateCategory, deleteCategory} from '../actions/index'
import CategoryModifyComponent from '../components/modify'

class CategoryModifyContainer extends Component {
  componentDidMount() {
    this.props.readCategory(this.props.params._id)
  }

  render() {
    return (
      <CategoryModifyComponent
        category={this.props.category}
        updateCategory={this.updateCategory.bind(this)}
        deleteCategory={this.deleteCategory.bind(this)}/>
    )
  }

  updateCategory(category) {
    this.props.updateCategory(category, this.props.params._id, function (err, data) {
      if (data && data.type === 'CATEGORY_UPDATE_RESOLVED') {
        browserHistory.push('/categories');
        toastr.success('Category Updated Successfully!!!');
      }
      if(err){
        toastr.error(err);
      }
    });

    // if (res.payload.success) {
    //   toastr.success(res.payload.message)
    //   browserHistory.push('/categories')
    // } else {
    //   toastr.warning(res.payload.responseJSON.message)
    // }
  }

  deleteCategory() {
    this.props.deleteCategory(this.props.params._id, function (err, data) {
      console.log('inside container modify',data);
      if(data){
        browserHistory.push('/categories');
        toastr.success('Category Deleted Successfully!!!');
      }
      if(err){
        toastr.error(err);
      }
    });
    // if (res.payload.success) {
    //   toastr.success(res.payload.message)
    //   browserHistory.push('/categories')
    // } else {
    //   toastr.warning(res.payload.responseJSON.message)
    // }
  }
}

CategoryModifyContainer.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  params: PropTypes.object.isRequired,
  category: PropTypes.object,
  readCategory: PropTypes.func.isRequired,
  updateCategory: PropTypes.func.isRequired,
  deleteCategory: PropTypes.func.isRequired
}

// Get apps store and pass it as props to CategoryModifyContainer
//  > whenever store changes, the CategoryModifyContainer will automatically re-render
function mapStateToProps(store) {
  return {
    category: store.categories.category
  }
}

// Get actions and pass them as props to to CategoryModifyContainer
//  > now CategoryModifyContainer has this.props.readCategory
//  > now CategoryModifyContainer has this.props.updateCategory
//  > now CategoryModifyContainer has this.props.deleteCategory
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    readCategory: readCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory
  }, dispatch)
}

// We don't want to return the plain CategoryModifyContainer (component) anymore,
// we want to return the smart Container
//  > CategoryModifyContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(CategoryModifyContainer)
