'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { createCategory } from '../actions/index'
import CategoryCreateComponent from '../components/create'

class CategoryCreateContainer extends Component {
    render () {
        return (
            <CategoryCreateComponent
                createCategory={this.createCategory.bind(this)} />
        )
    }

    createCategory (category) {
        this.props.createCategory(category, function(err, data){
          if(data){
            browserHistory.push('/categories');
            toastr.success('Category Created Successfully!!');
          }
          if(err){
            toastr.error(err);
          }
        });
    }
}

CategoryCreateContainer.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    createCategory: PropTypes.func.isRequired
}

// Get actions and pass them as props to to CategoryCreateContainer
//  > now CategoryCreateContainer has this.props.createCategory
function matchDispatchToProps (dispatch) {
    return bindActionCreators({
        createCategory: createCategory
    }, dispatch)
}

// We don't want to return the plain CategoryCreateContainer (component) anymore,
// we want to return the smart Container
//  > CategoryCreateContainer is now aware of actions
export default connect(null, matchDispatchToProps)(CategoryCreateContainer)
