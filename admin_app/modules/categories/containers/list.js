import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getCategories, createCategory, updateCategory, deleteCategory } from '../actions/index';
import CategoryListComponent from '../components/list';

class CategoryListContainer extends Component {
  componentWillMount() {
    this.props.getCategories(
      sessionStorage.getItem('categoryDataLimit'),
      sessionStorage.getItem('categoryCurrentPage')
    );
  }

  render() {
    let shouldLoad = this.props.categories ? true : false;
    return (
      shouldLoad ?
        <CategoryListComponent
          getCategories={this.props.getCategories}
          createCategory={this.props.createCategory}
          updateCategory={this.props.updateCategory}
          deleteCategory={this.props.deleteCategory}
          totalPage={this.props.totalPage}
          categories={this.props.categories}/> : <div className="spinner"></div>
      );
  }
}

CategoryListContainer.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  getCategories: PropTypes.func.isRequired,
};

// Get apps store and pass it as props to CategoryListContainer
//  > whenever store changes, the CategoryListContainer will automatically re-render
function mapStateToProps(store) {
  return {
    categories: store.categories.categoryList,
    totalPage: store.categories.totalPage,
  };
}

// Get actions and pass them as props to to CategoryListContainer
//  > now CategoryListContainer has this.props.getCategories
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getCategories: getCategories,
    createCategory: createCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory,
  }, dispatch);
}

// We don't want to return the plain CategoryListContainer (component) anymore,
// we want to return the smart Container
//  > CategoryListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(CategoryListContainer);
