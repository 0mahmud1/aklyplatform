/**
 * Created by Mitul on 9/4/16.
 */
'use strict';

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import toastr from 'toastr'

class CategoryCreateComponent extends Component {
    render () {
        return (
            <div className='row'>
                <div className='col-xs-12'>
                    <h3>Create Category</h3>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className='form-group'>
                            <label htmlFor='Name'>Name</label>
                            <input
                                type='text'
                                className='form-control'
                                ref='name'
                                placeholder='Name' />
                        </div>
                        <button type='submit' className='btn btn-primary btn-flat'>Create</button>
                        <a
                            className='btn btn-default btn-flat'
                            onClick={() => browserHistory.push('/categories')}>
                            Back
                        </a>
                    </form>
                </div>
            </div>
        )
    }

    handleSubmit (event) {
        event.preventDefault();

        const name = ReactDOM.findDOMNode(this.refs.name).value.trim();

        // validations
        if (name.length < 1) {
            toastr.warning('Name is required');
            return false
        }

        // create a object
        const category = { name };

        // call create action
        this.props.createCategory(category)
    }
}

CategoryCreateComponent.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    createCategory: PropTypes.func.isRequired
};

export default CategoryCreateComponent
