'use strict'

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import moment from 'moment'
import toastr from 'toastr'

class CategoryModifyComponent extends Component {
    render () {
        return (
            <div className='row'>
                <div className='col-xs-12'>
                    <h3>Modify Category</h3>
                    { this.props.category
                        ? <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className='form-group'>
                            <label htmlFor='ID'>ID</label>
                            <input
                                type='text'
                                className='form-control'
                                ref='id'
                                readOnly='true'
                                defaultValue={this.props.category._id} />
                        </div>
                        <div className='form-group'>
                            <label htmlFor='Name'>Name</label>
                            <input
                                type='text'
                                className='form-control'
                                ref='name'
                                placeholder='Name'
                                defaultValue={this.props.category.name} />
                        </div>
                        <div className='form-group'>
                            <label htmlFor='CreatedAt'>CreatedAt</label>
                            <input
                                type='text'
                                className='form-control'
                                ref='createdAt'
                                readOnly='true'
                                defaultValue={moment(this.props.category.createdAt).format('MMMM Do YYYY, h:mm:ss a')} />
                        </div>
                        <div className='form-group'>
                            <label htmlFor='UpdatedAt'>UpdatedAt</label>
                            <input
                                type='text'
                                className='form-control'
                                ref='updatedAt'
                                readOnly='true'
                                defaultValue={moment(this.props.category.updatedAt).format('MMMM Do YYYY, h:mm:ss a')} />
                        </div>
                        <button type='submit' className='btn btn-primary btn-flat'>Update</button>
                        <a
                            className='btn btn-default btn-flat'
                            data-toggle='modal'
                            data-target='#categoryDeleteConfirm'>
                            Delete
                        </a>
                        <a
                            className='btn btn-default btn-flat'
                            onClick={() => browserHistory.push('/categories')}>
                            Back
                        </a>
                    </form>
                        : <p><b><i>Loading.......</i></b></p>
                    }
                </div>
                {/* User delete confirmation modal */}
                <div
                    id='categoryDeleteConfirm'
                    className='modal fade'
                    tabIndex='-1'
                    role='dialog'>
                    <div className='modal-dialog' role='document'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <button
                                    type='button'
                                    className='close'
                                    data-dismiss='modal'
                                    aria-label='Close'>
                                    <span aria-hidden='true'>&times;</span>
                                </button>
                                <h4 className='modal-title text-center'>Category Delete Confirmation</h4>
                            </div>
                            <div className='modal-body'>
                                <p>Are you really want to delete this category?</p>
                            </div>
                            <div className='modal-footer'>
                                <div className='row'>
                                    <div className='col-xs-offset-6 col-xs-3'>
                                        <button
                                            type='button'
                                            className='btn btn-default btn-block btn-flat'
                                            data-dismiss='modal'>
                                            No
                                        </button>
                                    </div>
                                    <div className='col-xs-3'>
                                        <button
                                            type='button'
                                            className='btn btn-primary btn-block btn-flat'
                                            data-dismiss='modal'
                                            onClick={this.props.deleteCategory}>
                                            Yes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    handleSubmit (event) {
        event.preventDefault()

        const name = ReactDOM.findDOMNode(this.refs.name).value.trim()

        // validations
        if (name.length < 1) {
            toastr.warning('Name is required')
            return false
        }

        // create a object
        const category = { name }

        // call update action
        this.props.updateCategory(category)
    }
}

CategoryModifyComponent.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    category: PropTypes.object,
    deleteCategory: PropTypes.func.isRequired,
    updateCategory: PropTypes.func.isRequired
}

export default CategoryModifyComponent
