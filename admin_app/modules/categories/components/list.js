'use strict'

import React, {Component, PropTypes} from 'react'
// import ReactDOM from 'react-dom'
// import {browserHistory, Link} from 'react-router'

import PaginationComponent from '../../core/components/pagination'

class CategoryListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: sessionStorage.getItem('categoryCurrentPage') ? sessionStorage.getItem('categoryCurrentPage') : 1,
      currentLimit: sessionStorage.getItem('categoryDataLimit') ? sessionStorage.getItem('categoryDataLimit') : 20
    };
    sessionStorage.setItem('categoryCurrentPage', this.state.currentPage);
  }

  changeLimit(limit) {
    sessionStorage.setItem('categoryDataLimit', limit);
    sessionStorage.setItem('categoryCurrentPage', 1);
    this.props.getCategories(limit, sessionStorage.getItem('categoryCurrentPage'));
  }

  changePageNumber(number) {
    sessionStorage.setItem('categoryCurrentPage', number);
    this.setState({currentPage: number});
    this.props.getCategories(sessionStorage.getItem('categoryDataLimit'), number);
  }

  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading'>
              <h3>Categories</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }

  searchBar() {
    return (
      <div className='mp_body'>
        <div className="row">
          <div className="col-lg-12">
            <div className="akly_menulist akly_list">
              <div className="akly_listsearchadd_tag">
                <div className="akly_listsearch">
                  <div className="sb">
                    <div className="form-group">
                      <label className="sb_label" htmlFor="exampleInputAmount">Search</label>
                      <div className="input-group sb_inputgroup">
                        <input type="search" className="form-control sb_formctrl" id="exampleInputAmount"/>
                        <div className="input-group-addon">
                          <div className="sb_searchicon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125"
                                 preserveAspectRatio="xMidYMid meet">
                              <path
                                d="M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z"
                              />
                            </svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="akly_addinput_tag">
                  <div className="akly_inputadd geninput">
                    <div className="form-group inputadd_form">
                      <label className="geninput_label" htmlFor="exampleInputAmount">Add Category</label>
                      <div className="input-group geninput_group geninput_group_add">
                        <input type="text" className="form-control geninput_formctrl" ref='newCategory'/>
                      </div>
                    </div>
                    <div className="inputadd_butn">
                      <button className="addbtn" onClick={this.createCategory.bind(this)}>
													<span className="icon_plus">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                                 preserveAspectRatio="xMidYMid meet">
															<path
                                d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/>
														</svg>
													</span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  skeleton() {
    return (
      <div className='akly_menulisttbl akly_listbl'>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
            <div className='table-responsive'>
              <table className='table table-bordered akly_admintable akly_admintable_inlinedit'>
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {this.renderList()}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  rowClick(_id, event) {
    console.log('row click', _id, event.target, this);
    this.setState({selectedRow: _id});
  }

  createCategory(event) {
    event.preventDefault();
    const name = this.refs.newCategory.value;

    //validation

    const category = {name};

    console.log('category: ', category);

    this.props.createCategory(category);

    this.refs.newCategory.value = '';
  }

  handleEdit(event) {
    if (event.key === 'Enter') {
      console.log(this, event.target.value);
      let category = {
        name: event.target.value
      };
      this.props.updateCategory(category, this.state.selectedRow);
    }

  }


  render() {
    return (
      <div>
        {this.heading()}
        {this.searchBar()}
        {this.skeleton()}
        <PaginationComponent current={this.state.currentPage} dataLimitStorage="categoryDataLimit" pageNumberStorage="categoryCurrentPage" totalPage={this.props.totalPage} onPageChange={this.changePageNumber.bind(this)} changeLimit={this.changeLimit.bind(this)}/>
        {this.deleteModal()}
      </div>
    )
  }

  renderList() {

    return (this.props.categories) ? this.props.categories.map((category) => {
      let editUrl = '/categories/' + category._id;
      return (
        <tr key={category._id} onClick={(evt) => this.rowClick(category._id, evt)}>
          <td>
            <input type='text' defaultValue={category.name} onKeyPress={this.handleEdit.bind(this)}/>
          </td>
          <td>
            {/*Edit button
             <Link to={editUrl} className='akly_tbledit' data-toggle='tooltip' data-placement='top' title='Edit'>
             <span className='akly_tblicon'>
             <svg xmlns='http://www.w3.org/2000/svg'
             viewBox='0 0 90 112.5'
             preserveAspectRatio='xMidYMid meet'>
             <path
             d='M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z'/>
             </svg>
             </span>
             </Link>
             */}

            {/*delete button*/}
            <a href='#' className='akly_tbldel' data-toggle='tooltip' data-placement='top' title='Delete'>
							<span className='akly_tblicon' data-toggle='modal'
                    data-target='.aklyDelPrompt'>
								<svg xmlns='http://www.w3.org/2000/svg'
                     viewBox='0 0 100 125'
                     preserveAspectRatio='xMidYMid meet'>
									<path
                    d='M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z'/>
									<path
                    d='M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z'/>
								</svg>
							</span>
            </a>
          </td>
        </tr>
      )
    }) : null;
  }


  deleteModal() {
    //console.log('inside delete modal', this.state.selectedRow);
    return (
      <div className='modal fade aklyDelPrompt delprompt' tabIndex='-1' role='dialog' aria-labelledby='myModalLabel'>
        <div className='modal-dialog delprompt_dialog' role='document'>
          <div className='modal-content delprompt_content'>
            <div className='modal-body'>
              <div className='delprompt_text'>
                <p>Do you want to delete this Category?</p>
              </div>
            </div>
            <div className='modal-footer'>
              <button type='button' className='btn butn butn_danger' data-dismiss='modal'
                      onClick={() => this.props.deleteCategory(this.state.selectedRow)}>Delete
              </button>
              <button type='button' className='btn butn butn_default' data-dismiss='modal'>No</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CategoryListComponent