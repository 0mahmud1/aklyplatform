'use strict'

/*
 * This reducer will always return an array of categories no matter what
 * You need to return something, so if there are no categories then just return an empty array
 */

const defaultState = {
    categoryList : [],
    category : null
}
const categoriesReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'CATEGORY_CREATE_PENDING':
            return Object.assign({}, state, {

            });
	    case 'CATEGORY_CREATE_RESOLVED':
		    let newList = state.categoryList.concat(action.payload);
            return Object.assign({}, state, {
                categoryList: newList
            });
        case 'CATEGORY_CREATE_REJECTED':
            return Object.assign({}, state, {
                error: action.payload.error
            });

        case 'CATEGORY_READ_RESOLVED':
            return Object.assign({}, state, {
                category: action.payload.data.data
            });
        case 'CATEGORY_READ_REJECTED':
            return Object.assign({}, state, {
                error: action.payload.error
            });

        case 'CATEGORY_UPDATE_RESOLVED':
            return Object.assign({}, state, {
                category: action.payload.data
            });
        case 'CATEGORY_UPDATE_REJECTED':
            return Object.assign({}, state, {
                error: action.payload.error
            });

        case 'CATEGORY_DELETE_RESOLVED':
	        let filteredList = _.filter(state.categoryList, (category) => {
		        return category._id !== action.payload._id;
	        });
            return Object.assign({}, state, {
	            categoryList: filteredList
            });
        case 'CATEGORY_DELETE_REJECTED':
            return Object.assign({}, state, {
                error: action.payload.error
            });

        case 'CATEGORIES_LIST_PENDING':
            return Object.assign(({}, state, {

            }));
        case 'CATEGORIES_LIST_RESOLVED':
            return Object.assign({}, state, {
                categoryList: action.payload.data,
                totalPage: action.payload.limit == 0 ? 1 : Math.ceil(action.payload.total / action.payload.limit) // if limit is 0 set total number of page to one
            });
        case 'CATEGORIES_LIST_REJECTED':
            return Object.assign({}, state, {
                error: action.payload.error
            });

        default:
            return state

    }
}

export default categoriesReducer
