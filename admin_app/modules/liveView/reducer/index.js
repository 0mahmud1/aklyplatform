import _ from 'lodash';

const defaultState = {};

const liveViewReducer = (state = defaultState, action) => {
  let zone;
  let dboys;
  let vans;
  let findIndex;
  switch (action.type) {
    case 'LIVE_VIEW_GET_ZONE_RESOLVED':
      return Object.assign({}, state, { zones: action.payload });

    case 'LIVE_VIEW_SELECTED_ZONE':
      zone = _.find(state.zones, z => z._id === action.payload);
      return Object.assign({}, state, { selectedZone: zone });

    case 'LIVE_VIEW_GET_DBOY_RESOLVED':
      return Object.assign({}, state, { deliveryBoys: action.payload });

    case 'LIVE_VIEW_GET_FOOD_PROVIDER_RESOLVED':
      return Object.assign({}, state, { foodProviders: action.payload });

    case 'LIVE_VIEW_GET_VANS_RESOLVED':
      return Object.assign({}, state, { vans: action.payload });

    case 'LIVE_VIEW_UPDATE_DBOY_RESOLVED':
      dboys = [...state.deliveryBoys];
      findIndex = _.findIndex(dboys, db => {
        return db._id === action.payload._id;
      });
      console.log('find index ', findIndex, dboys.length);
      if (findIndex == -1) {
        console.log('impossible though');
        return state;
      }

      dboys[findIndex] = action.payload;
      return Object.assign({}, state, { deliveryBoys: dboys });

    case 'LIVE_VIEW_UPDATE_VAN_RESOLVED' :
      vans = [...state.vans];
      findIndex = _.findIndex(vans, vn => {
        return vn._id === action.payload._id;
      });
      console.log('find index ', findIndex, vans.length);
      if (findIndex == -1) {
        console.log('impossible though');
        return state;
      }

      vans[findIndex] = action.payload;
      return Object.assign({}, state, { vans: vans });

    default :
      return state;
  }
};

export default liveViewReducer;
