import axios from 'axios';
import toastr from 'toastr';

export function getZonesLV() {
  return function (dispatch) {
    axios.get('api/zones')
      .then((response) => {
        console.log('all zone... : ', response);
        dispatch({ type: 'LIVE_VIEW_GET_ZONE_RESOLVED', payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: 'LIVE_VIEW_GET_ZONE_REJECTED', payload: err });
        console.warn('all hub err ', err);
      });
  };
}

export function getFoodProvidersLV() {
  return function (dispatch) {
    return axios.get('api/foodproviders')
      .then((response) => {
        dispatch({ type: 'LIVE_VIEW_GET_FOOD_PROVIDER_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'LIVE_VIEW_GET_FOOD_PROVIDER_REJECTED', payload: err });
      });
  };
}

export function getVansLV() {
  return function (dispatch) {
    return axios.get('api/vans')
      .then((response) => {
        dispatch({ type: 'LIVE_VIEW_GET_VANS_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'LIVE_VIEW_GET_VANS_REJECTED', payload: err });
      });
  };
}

export function getDBoysLV() {
  return function (dispatch) {
    return axios.get('api/dboys')
      .then((response) => {
        console.log('getDBoys response', response);
        dispatch({ type: 'LIVE_VIEW_GET_DBOY_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'LIVE_VIEW_GET_DBOY_REJECTED', payload: err });
        return err;
      });
  };
}

export function updateDBoyLV(_id, patch) {
  return function (dispatch) {
    console.log('PATCH: ', patch, _id);
    return axios.put('api/dboys' + '/' + _id, patch)
      .then((response) => {
        console.log('UPDATE dboy... : ', response);
        dispatch({ type: 'LIVE_VIEW_UPDATE_DBOY_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        console.error('updateDBoyLV ERR: ', err);
        dispatch({ type: 'LIVE_VIEW_UPDATE_DBOY_REJECTED', payload: err });
        return err.data.data;
      });
  };
}

export function updateVanLV(_id, patch) {
  return function (dispatch) {
    console.log('PATCH: ', patch, _id);
    return axios.put('api/vans' + '/' + _id, patch)
      .then((response) => {
        console.log('UPDATE dboy... : ', response);
        dispatch({ type: 'LIVE_VIEW_UPDATE_VAN_RESOLVED', payload: response.data.data });
        return response.data.data;
      })
      .catch((err) => {
        dispatch({ type: 'LIVE_VIEW_UPDATE_VAN_REJECTED', payload: err });
        return err.data.data;
      });
  };
}
