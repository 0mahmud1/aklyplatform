'use strict'

import React, { Component, PropTypes } from 'react'

class DumbLiveViewComponent extends Component {
	componentDidMount() {
		let map = new google.maps.Map(document.getElementById('static-map-live-view'), {
			center: {lat: 25.3017432, lng: 51.0458962},
			zoom: 8
		});

		//25.3017432,51.0458962
	}

	render() {
		return (
			<div id='static-map-live-view' className="liview_wrapper"></div>
		)
	}
}

export default DumbLiveViewComponent