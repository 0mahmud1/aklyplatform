import React from 'react';
import { Route, IndexRoute } from 'react-router';

import LiveViewContainer from '../containers';
import * as AuthService from '../../services/auth';
export default function () {
	return (
		<Route path='live' onEnter={AuthService.isDashAuthRouter}>
			<IndexRoute component={LiveViewContainer} />
		</Route>
	);
}
