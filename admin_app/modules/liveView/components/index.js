import React, { Component, PropTypes } from 'react';
import 'select2';
import $ from 'jquery';
import _ from 'lodash';
import toastr from 'toastr';

import { pubnub } from '../../../pubnub';

class LiveViewComponent extends Component {
  componentWillMount() {
    const _this = this;
    pubnub.addListener({
      message: function (response) {
        console.log('PubNub response: ', response);
        if (response.channel == 'deliveryBoys-administrator' && response.message.data) {
          _this.props.updateDeliveryBoyLV(response.message.data._id, response.message.data.locations).then(res => {
            toastr.success('deliveryBoy position updated');
            _this.drawDboyPinsLV();
          });
        } else if (response.channel == 'vanOperators-administrator' && response.message.data) {
          _this.props.updateVanLV(response.message.data._id, response.message.data.locations).then(res => {
            toastr.success('vanOperator position updated');
            _this.drawVanPinsLV();
          });
        }
      },
    });
  }

  componentDidMount() {
    this.initializeMapLV();
    this.select2InitializationLV();
  }

  initializeMapLV() {
    let map = new google.maps.Map(document.getElementById('map-live-view'), {
      center: { lat: 25.3017432, lng: 51.0458962 },
      zoom: 14,
    });
    this.setState({
      map: map,
    });
  }

  select2InitializationLV() {
    const _this = this;
    $('#zone-select').select2({
      minimumResultsForSearch: Infinity,
    }).on('change', (e) => {
      if (e.target.value === '') {
        return;
      }

      _this.props.selectZoneLV(e.target.value);
      _this.drawZoneLV(_this.state.map, _this.props.liveView.selectedZone);
      _this.props.getDeliveryBoysLV().then(result => {
        console.log('deliveryboys loaded: ', result);
        _this.drawDboyPinsLV();
      });
      _this.props.getFoodProvidersLV() .then(rs => {
        console.log('fp lod :', rs);
        _this.drawProviderPinsLV();
        return _this.props.getVansLV();
      }).then(rs2 => {
        console.log('van loaded ', rs2);
        _this.drawVanPinsLV();
      });
    });
  }

  drawZoneLV(map, zone) {
    console.log('drawing zone', map, zone);
    const _this = this;
    let oldPolygon = this.state.polygon;

    if (oldPolygon) {
      oldPolygon.setMap(null);
    }

    let zpolygon = new google.maps.Polygon({
      paths: _this.convertPolygonArrayToObjectLV(zone.locations.coordinates[0]),
      strokeColor: '#32CD32',
      strokeOpacity: 1,
      strokeWeight: 2,
      fillColor: '#32CD32',
      fillOpacity: 0.2,
    });
    zpolygon.setMap(map);

    _this.setState({
      polygon: zpolygon,
    });

    map.setCenter({ lng: zone.locations.coordinates[0][0][0], lat: zone.locations.coordinates[0][0][1] });
  }

  clearDBMarkersLV() {
    if (this.state.markers) {
      let markers = this.state.markers;
      markers.forEach(marker => marker.setMap(null));
      this.setState({ markers: null });
    }
  }

  clearFPMarkersLV() {
    if (this.state.providerMarkers) {
      let pmarkers = this.state.providerMarkers;
      pmarkers.forEach(marker => marker.setMap(null));
    }

    this.setState({ providerMarkers: null });
  }

  clearVanMarkersLV() {
    if (this.state.vanMarkers) {
      let vmarkers = this.state.vanMarkers;
      vmarkers.forEach(marker => marker.setMap(null));
    }

    this.setState({ vanMarkers: null });
  }

  drawDboyPinsLV() {
    this.clearDBMarkersLV();

    const _this = this;
    let markers = [];
    let selectedZoneDboys = _this.props.liveView.selectedZone.deliveryBoys.map(szd => szd._id);
    let filtered = _.filter(_this.props.liveView.deliveryBoys, bz => {
      return _.includes(selectedZoneDboys, bz._id);
    });
    let data = filtered.map(db => {
      return { _id: db._id, name: db.name, email: db.email, locations: db.locations };
    });

    data.forEach(d => {
      let marker = new google.maps.Marker({
        position: { lat: d.locations.coordinates[1], lng: d.locations.coordinates[0] },
        map: _this.state.map,
        icon: {
          url: '/icons/dboy.png',
          scaledSize: new google.maps.Size(25, 25),
        },
      });

      let infowindow = new google.maps.InfoWindow({
        content: `${d.name}<br/>${d.email}`,
      });

      // by default open
      infowindow.open(_this.state.map, marker);

      marker.addListener('click', function () {
        infowindow.open(_this.state.map, marker);
      });

      marker.addListener('dragend', function (marker) {
        console.log('marker drag', marker, d._id);
        _this.props.updateDeliveryBoyLV(d._id, _this.makeFormattedGeoPointFromMarkerLV(marker))
          .then(res => {
            toastr.success('delivery boy position updated');
          });
      });

      markers.push(marker);
    });

    _this.setState({
      markers: markers,
    });
  }

  drawProviderPinsLV() {
    this.clearFPMarkersLV();

    let _this = this;
    let markers = [];
    let selectedZoneFps = _this.props.liveView.selectedZone.foodProviders.map(fp => fp._id);
    let filtered = _.filter(_this.props.liveView.foodProviders, fpz => {
      return _.includes(selectedZoneFps, fpz._id);
    });
    let data = filtered.map(fp => {
      let type = 'Subscription';
      if (fp.providerType.length == 2) {
        type = 'Both';
      } else if (fp.providerType[0] == 'onDemand') {
        type = 'On Demand';
      }

      return { _id: fp._id, name: fp.name, email: fp.email, locations: fp.locations, type: type };
    });

    data.forEach(d => {
      let marker = new google.maps.Marker({
        position: { lat: d.locations.coordinates[1], lng: d.locations.coordinates[0] },
        map: _this.state.map,
        icon: {
          url: '/icons/provider.png',
          scaledSize: new google.maps.Size(25, 25),
        },
      });

      let infowindow = new google.maps.InfoWindow({
        content: `${d.name} (${d.type})<br/>${d.email}`,
      });

      // by default open
      infowindow.open(_this.state.map, marker);

      marker.addListener('click', function () {
        infowindow.open(_this.state.map, marker);
      });

      /*marker.addListener('dragend', function (marker) {
        console.log('marker drag', marker, d._id);
        _this.props.updateDeliveryBoyLV(d._id, _this.makeFormattedGeoPointFromMarkerLV(marker))
          .then(res => {
            toastr.success('delivery boy position updated');
          });
      });*/

      markers.push(marker);
    });

    _this.setState({
      providerMarkers: markers,
    });
  }

  drawVanPinsLV() {
    this.clearVanMarkersLV();

    const _this = this;
    let vmarkers = [];
    let selectedZoneFps = _this.props.liveView.selectedZone.foodProviders.map(fp => fp._id);
    let selectedFpsOriginal = _.filter(_this.props.liveView.foodProviders, fp => _.includes(selectedZoneFps, fp._id));
    let selectedVansFromFP = _.uniq(_.flatten(selectedFpsOriginal.map(fpo => {
      if (fpo.providerType.includes('subscription')) {
        return fpo.vans.map(v => v._id);
      }
    })));

    let filtered = _.filter(_this.props.liveView.vans, vn => {
      return _.includes(selectedVansFromFP, vn._id);
    });
    let data = _.compact(filtered.map(v => {
      if (v.locations) {
        return { _id: v._id, name: v.name, email: v.email, locations: v.locations };
      }
    }));

    console.log('semi final: ', selectedZoneFps, selectedFpsOriginal, selectedVansFromFP, filtered,data);

    data.forEach(d => {
      let marker = new google.maps.Marker({
        position: { lat: d.locations.coordinates[1], lng: d.locations.coordinates[0] },
        map: _this.state.map,
        icon: {
          url: '/icons/van.png',
          scaledSize: new google.maps.Size(25, 25),
        },
      });

      let infowindow = new google.maps.InfoWindow({
        content: `${d.name}<br/>${d.email}`,
      });

      // by default open
      infowindow.open(_this.state.map, marker);

      marker.addListener('click', function () {
        infowindow.open(_this.state.map, marker);
      });

      marker.addListener('dragend', function (marker) {
        console.log('marker drag of van ', marker, d._id);
        _this.props.updateVanLV(d._id, _this.makeFormattedGeoPointFromMarkerLV(marker))
          .then(res => {
            toastr.success('van position updated');
          });
      });

      vmarkers.push(marker);
    });

    _this.setState({
      vanMarkers: vmarkers,
    });
  }

  makeFormattedGeoPointFromMarkerLV(marker) {
    return {
      locations: { coordinates: [marker.latLng.lng(), marker.latLng.lat()] },
    };
  }

  convertPolygonArrayToObjectLV(polygon) {
    let pl = [];
    polygon.forEach(point => {
      pl.push({ lng: point[0], lat: point[1] });
    });
    return pl;
  }

  generateZoneListLV() {
    return (
      this.props.liveView.zones ?
      this.props.liveView.zones.map(zone => <option key={zone._id} value={zone._id}>{zone.name}</option>) : null
    );
  }

  render() {
    return (
      <div className="mp_wrapper akly_mp" data-hook="mainPageWrapper">
          <div className="liview_wrapper mapside col-lg-2">
            <div>
              <div>
                <label htmlFor="zone">Select a zone</label>
              </div>
              <div>
                <select className="simulator-select" id='zone-select'>
                  <option value="">select</option>
                  {this.generateZoneListLV()}
                </select>
              </div>
            </div>
          </div>
          <div id="map-live-view" className="liview_wrapper col-lg-10"></div>
      </div>

    );
  }
}

export default LiveViewComponent;
