import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import toastr from 'toastr';

import {
  getZonesLV,
  getDBoysLV,
  updateDBoyLV,
  getFoodProvidersLV,
  getVansLV,
  updateVanLV
} from '../actions';

import LiveViewComponent from '../components';

class LiveViewContainer extends Component {
  componentDidMount() {
    this.props.getZonesLV();
  }

  render() {
    return (
      this.props.google ?
      <LiveViewComponent {...this.props} /> : <div className="spinner"></div>
    );
  }
}

function mapStateToProps(store) {
  return {
    google: store.auth.googleLoaded,
    liveView: store.liveView,
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getZonesLV: () => dispatch(getZonesLV()),
    selectZoneLV: (zoneId) => dispatch({ type: 'LIVE_VIEW_SELECTED_ZONE', payload: zoneId }),
    getDeliveryBoysLV: () => dispatch(getDBoysLV()),
    getFoodProvidersLV: () => dispatch(getFoodProvidersLV()),
    getVansLV: () => dispatch(getVansLV()),
    updateDeliveryBoyLV: (_id, patch) => dispatch(updateDBoyLV(_id, patch)),
    updateVanLV: (_id, patch) => dispatch(updateVanLV(_id, patch)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveViewContainer);
