import React, {Component, PropTypes} from 'react'
//import ItemTypes from './ItemTypes'
import { DragSource } from 'react-dnd'


const dropDownItemSource = {
  beginDrag(props) {
    return {
      name: props.name
    };
  },

  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();

    if (dropResult) {
      window.alert(
        `You dropped ${item.name} into ${dropResult.name}!`
      );
    }
  }
};

@DragSource('box', dropDownItemSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))

class DropDownItemComponent extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
  };

  render() {
    const { isDragging, connectDragSource } = this.props;
    const { name } = this.props;

    return (
      connectDragSource(
        <li>
          <a href="#">
            <span>
              {name}
            </span>
          </a>
        </li>
      )
    );
  }
}

export default DropDownItemComponent