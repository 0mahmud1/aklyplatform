'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'

class VanAddComponent extends Component {
	render() {
		return (
			<div>
				<div className="mp_heading_mb">
					<div className="row">
						<div className="col-lg-12">
							<div className="mp_heading">
								<h3>Add Van</h3>
							</div>
						</div>
					</div>
				</div>


				<div className="mp_body">
					<div className="row">
						<div className="col-lg-12">
							<div className="van_add">
							<form>
							<div className="van_add_form">
								<div className="row">
									<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
										<div className="geninput">
											<div className="form-group">
												<label className="geninput_label" htmlFor="exampleInputAmount">Name</label>
												<div className="input-group geninput_group geninput_group_add">
													<input type="text" className="form-control geninput_formctrl" id="exampleInputAmount"/>
												</div>
											</div>
										</div>
									</div>
									<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
										<div className="geninput">
											<div className="form-group">
												<label className="geninput_label" htmlFor="exampleInputAmount">Email</label>
												<div className="input-group geninput_group geninput_group_add">
													<input type="email" className="form-control geninput_formctrl" id="exampleInputAmount"/>
												</div>
											</div>
										</div>
									</div>
									<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
										<div className="geninput">
											<div className="form-group">
												<label className="geninput_label" htmlFor="exampleInputAmount">Contact No.</label>
												<div className="input-group geninput_group geninput_group_add">
													<input type="tel" className="form-control geninput_formctrl" id="exampleInputAmount"/>
												</div>
											</div>
										</div>
									</div>
									<div className="col-xs-12 col-sm-6 col-md-3 col-lg-3">
										<div className="geninput">
											<div className="form-group">
												<label className="geninput_label" htmlFor="exampleInputAmount">License No.</label>
												<div className="input-group geninput_group geninput_group_add">
													<input type="text" className="form-control geninput_formctrl" id="exampleInputAmount"/>
												</div>
											</div>
										</div>
									</div>
							   	</div>
							  </div>

							  <div className="van_add_actionbtn actionbutn">
								  <div className="row">
									  <div className="col-md-12 col-lg-12">
										  <div className="actionbtn_save">
											  <button type="submit" className="btn butn butn_default">
												 <span className="actionbtn_save_loader actionButnSaveLoader">
												  <svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><path fill="none" d="M0 0h100v100H0z"/><defs><filter id="a" x="-100%" y="-100%" width="300%" height="300%"><feOffset result="offOut" in="SourceGraphic"/><feGaussianBlur result="blurOut" in="offOut"/><feBlend in="SourceGraphic" in2="blurOut"/></filter></defs><path d="M10 50s0 .5.1 1.4c0 .5.1 1 .2 1.7 0 .3.1.7.1 1.1.1.4.1.8.2 1.2.2.8.3 1.8.5 2.8.3 1 .6 2.1.9 3.2.3 1.1.9 2.3 1.4 3.5.5 1.2 1.2 2.4 1.8 3.7.3.6.8 1.2 1.2 1.9.4.6.8 1.3 1.3 1.9 1 1.2 1.9 2.6 3.1 3.7 2.2 2.5 5 4.7 7.9 6.7 3 2 6.5 3.4 10.1 4.6 3.6 1.1 7.5 1.5 11.2 1.6 4-.1 7.7-.6 11.3-1.6 3.6-1.2 7-2.6 10-4.6 3-2 5.8-4.2 7.9-6.7 1.2-1.2 2.1-2.5 3.1-3.7.5-.6.9-1.3 1.3-1.9.4-.6.8-1.3 1.2-1.9.6-1.3 1.3-2.5 1.8-3.7.5-1.2 1-2.4 1.4-3.5.3-1.1.6-2.2.9-3.2.2-1 .4-1.9.5-2.8.1-.4.1-.8.2-1.2 0-.4.1-.7.1-1.1.1-.7.1-1.2.2-1.7.1-.9.1-1.4.1-1.4V54.2c0 .4-.1.8-.1 1.2-.1.9-.2 1.8-.4 2.8-.2 1-.5 2.1-.7 3.3-.3 1.2-.8 2.4-1.2 3.7-.2.7-.5 1.3-.8 1.9-.3.7-.6 1.3-.9 2-.3.7-.7 1.3-1.1 2-.4.7-.7 1.4-1.2 2-1 1.3-1.9 2.7-3.1 4-2.2 2.7-5 5-8.1 7.1L70 85.7c-.8.5-1.7.9-2.6 1.3l-1.4.7-1.4.5c-.9.3-1.8.7-2.8 1C58 90.3 53.9 90.9 50 91l-3-.2c-1 0-2-.2-3-.3l-1.5-.2-.7-.1-.7-.2c-1-.3-1.9-.5-2.9-.7-.9-.3-1.9-.7-2.8-1l-1.4-.6-1.3-.6c-.9-.4-1.8-.8-2.6-1.3l-2.4-1.5c-3.1-2.1-5.9-4.5-8.1-7.1-1.2-1.2-2.1-2.7-3.1-4-.5-.6-.8-1.4-1.2-2-.4-.7-.8-1.3-1.1-2-.3-.7-.6-1.3-.9-2-.3-.7-.6-1.3-.8-1.9-.4-1.3-.9-2.5-1.2-3.7-.3-1.2-.5-2.3-.7-3.3-.2-1-.3-2-.4-2.8-.1-.4-.1-.8-.1-1.2v-1.1-1.7c-.1-1-.1-1.5-.1-1.5z" filter="url(#a)"><animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" dur="0.7s"/></path></svg>
											  </span>
											  <span className="actionbtn_save_text">Add</span>
											 </button>
										  </div>
										  <div className="actionbtn_cancel">
											  <button type="button" className="btn butn butn_cancel">Cancel</button>
										  </div>
										  <div className="actionbtn_back">
											  <button type="button" className="btn butn butn_back">Back</button>
										  </div>
									  </div>
								  </div>
							  </div>
							</form>
                        </div>
                    </div>
                </div>
			</div>
			</div>
		)
	}
}

export default VanAddComponent