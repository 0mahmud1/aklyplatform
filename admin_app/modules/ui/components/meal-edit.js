'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
//import moment from 'moment'

class MealEditComponent extends Component {
    render() {
        return (
            <div>
            <div className="mp_heading_mb">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="mp_heading">
                                <h3>Edit Food Meal</h3>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="mp_body">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="meal_edit">
                                <form action="">
                                    <div className="meal_basiceditsec">
                                        <div className="row">
                                            <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <div className="geninput">
                                                    <div className="form-group">
                                                        <label className="geninput_label" htmlFor="exampleInputAmount">Name</label>
                                                        <div className="geninput_group">
                                                            <input type="text" className="form-control geninput_formctrl" id="exampleInputAmount" value="Spicy Chicken" readOnly="readOnly"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                <div className="geninput">
                                                    <div className="form-group">
                                                        <label className="geninput_label" htmlFor="exampleInputAmount">Price(QAR)</label>
                                                        <div className="geninput_group">
                                                            <input type="text" className="form-control geninput_formctrl" id="exampleInputAmount" value="599" readOnly="readOnly"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="meal_tagsimgeditsec">
                                        <div className="row">
                                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div className="meal_desc">
                                                    <div className="geninput">
                                                        <div className="form-group geninput_textarea">
                                                            <label htmlFor="comment" className="geninput_label">Description</label>
                                                            <textarea className="form-control geninput_textareactrl textarea_descheight" id="comment" readOnly="readOnly" data-insert="mealDescDefaultVal" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="meal_categories">
                                                    <div className="geninput">
                                                        <div className="form-group">
                                                            <label htmlFor="comment" className="geninput_label">Categories [Meal Tags]</label>
                                                            <input type="text" className="form-control" id="exampleInputAmount"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="meal_ingredients">
                                                    <div className="geninput">
                                                        <div className="form-group geninput_textarea">
                                                            <label htmlFor="comment" className="geninput_label">Ingredients</label>
                                                            <textarea className="form-control geninput_textareactrl textarea_ingredientheight" id="comment" readOnly="readOnly" data-insert="mealIngredientDefaultVal"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="meal_recipe">
                                                    <div className="geninput">
                                                        <div className="form-group geninput_textarea">
                                                            <label htmlFor="comment" className="geninput_label">Recipe</label>
                                                            <textarea className="form-control geninput_textareactrl textarea_descheight" id="comment" readOnly="readOnly" data-insert="mealRecipecDefaultVal"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 meal_imageditwrapper">
                                                <div className="meal_imagedit">
                                                    <div className="meal_image">
                                                        <img src="/images/spicy_chicken.png" alt=""/>
                                                    </div>
                                                    <div className="meal_editicon" data-toggle="modal" data-target=".aklyEditImagePrompt">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 112.5" preserveAspectRatio="xMidYMid meet">
                                                            <path d="M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z" />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="meal_actionbtn actionbutn">
                                        <div className="row">
                                            <div className="col-md-12 col-lg-12">
                                                <div className="actionbtn_save">
                                                    <button type="submit" className="btn butn butn_default">Save</button>
                                                </div>
                                                <div className="actionbtn_cancel">
                                                    <button type="button" className="btn butn butn_cancel">Cancel</button>
                                                </div>
                                                <div className="actionbtn_back">
                                                    <button type="button" className="btn butn butn_back">Back</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade aklyEditImagePrompt uploadprompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog uploadprompt_dialog" role="document">
                        <div className="modal-content uploadprompt_content">
                            <div className="modal-body">
                                <div className="uploadprompt_button">
                                    <form action="">
                                        <div className="form-group uploadbutn_wrapper">
                                            <label htmlFor="uploadAttachment" className="uploadbutn_label">Change Image</label>
                                            <input type="file" name="uploadAttachment" id="uploadAttachment" className="uploadbutn_file"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MealEditComponent