'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
//import moment from 'moment'

class MealTagAddComponent extends Component {
    render() {
        return (
            <div>
            <div className="mp_heading_mb">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="mp_heading">
                                <h3>Meals Tags</h3>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="mp_body">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="akly_menulist akly_list">
                                <div className="akly_listsearchadd_tag">
                                    <div className="akly_listsearch">
                                        <div className="sb">
                                            <div className="form-group">
                                                <label className="sb_label" htmlFor="exampleInputAmount">Search</label>
                                                <div className="input-group sb_inputgroup">
                                                    <input type="search" className="form-control sb_formctrl" id="exampleInputAmount" />
                                                    <div className="input-group-addon">
                                                        <div className="sb_searchicon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet">
                                                                <path d="M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="akly_addinput_tag">
                                        <div className="akly_inputadd geninput">
                                            <div className="form-group inputadd_form">
                                                <label className="geninput_label" htmlFor="exampleInputAmount">Add Categories</label>
                                                <div className="input-group geninput_group geninput_group_add">
                                                    <input type="text" className="form-control geninput_formctrl" id="exampleInputAmount"/>
                                                </div>
                                            </div>
                                            <div className="inputadd_butn">
                                                 <a href="#" className="addbtn">
                                                <span className="icon_plus">
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/></svg>
                                                </span>
                                            </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="akly_menulisttbl akly_listbl">
                                    <div className="row">
                                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div className="table-responsive">
                                                <table className="table table-bordered akly_admintable akly_admintable_gen">
                                                    <thead>
                                                        <tr>
                                                            <th>Tag Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Spicy Chicken</td>
                                                            <td>
                                                                <a href="#" className="akly_tbledit" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                    <span className="akly_tblicon" data-toggle="modal" data-target=".aklyTagEditPrompt">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 112.5" preserveAspectRatio="xMidYMid meet"><path d="M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z"/></svg>
                                                                    </span>
                                                                </a>

                                                                <a href="#" className="akly_tbldel" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                    <span className="akly_tblicon" data-toggle="modal" data-target=".aklyDelPrompt">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>Spicy Chicken</td>
                                                            <td>
                                                                <a href="#" className="akly_tbledit" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                    <span className="akly_tblicon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 112.5" preserveAspectRatio="xMidYMid meet"><path d="M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z"/></svg>
                                                                    </span>
                                                                </a>

                                                                <a href="#" className="akly_tbldel" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                    <span className="akly_tblicon" data-toggle="modal" data-target=".aklyDelPrompt">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>Spicy Chicken</td>
                                                            <td>
                                                                <a href="#" className="akly_tbledit" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                    <span className="akly_tblicon">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 112.5" preserveAspectRatio="xMidYMid meet"><path d="M15.13 62.142l47.73-47.73L75.586 27.14l-47.73 47.73zM11.948 65.324l12.728 12.728-15.91 3.182M66.038 11.228l2.828-2.83 12.73 12.727-2.828 2.83z"/></svg>
                                                                    </span>
                                                                </a>

                                                                <a href="#" className="akly_tbldel" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                    <span className="akly_tblicon" data-toggle="modal" data-target=".aklyDelPrompt">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="akly_adminpagination">
                                    <div className="akly_paginationwrapper">
                                       <div className="akly_pagination_arrow_left">
                                           <div className="icon_leftarrow">
                                               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 30" preserveAspectRatio="xMidYMid meet"><path d="M17.422 2.406L7.827 12l9.59 9.59L16 23 5 12 16.007.99z"/></svg>
                                           </div>
                                       </div>
                                        <div className="akly_pagination_state">
                                            <p>Page <span className="currentpage">1</span> of 20</p>
                                        </div>
                                        <div className="akly_pagination_arrow_right">
                                           <div className="icon_rightarrow">
                                               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 30" preserveAspectRatio="xMidYMid meet"><path d="M19 12L8 23l-1.413-1.414 9.59-9.59-9.595-9.594L7.996.988z"/></svg>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            <div className="modal fade aklyDelPrompt delprompt" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div className="modal-dialog delprompt_dialog" role="document">
                    <div className="modal-content delprompt_content">
                        <div className="modal-body">
                            <div className="delprompt_text">
                                <p>Do you want to delete this meal item?</p>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn butn butn_danger">Delete</button>
                            <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div> 
               
            <div className="modal fade aklyTagEditPrompt edit-tag-prompt" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div className="modal-dialog edit-tag-prompt_dialog" role="document">
                    <div className="modal-content edit-tag-prompt_content">
                        <div className="modal-body">
                            <div className="edit-tag-prompt_input">
                                <div className="form-group geninput">
                                    <div className="input-group geninput_group geninput_group_add">
                                        <input type="text" className="form-control geninput_formctrl" id="exampleInputAmount" value="Spicy Chicken"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                           <button type="button" className="btn butn butn_default">Save</button>
                            <button type="button" className="btn butn butn_cancel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}

export default MealTagAddComponent