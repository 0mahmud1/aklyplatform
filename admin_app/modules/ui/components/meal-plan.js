'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import DropDownItemComponent from './meal_plan_dnd/sb_dropdown_item'
import DropItem from './meal_plan_dnd/drop_item'
import 'bootstrap/dist/js/bootstrap.min'

@DragDropContext(HTML5Backend)
class MealPlanComponent extends Component {
	componentDidMount() {
		// bind click events to meal plan card
		let $cell = $('.meal_plan_card');

		$cell.find('.meal_plan_info').click(function(event) {
		event.preventDefault();
  		 let $thisCell = $(this).closest('.meal_plan_card');

		 if ($thisCell.hasClass('is_collapsed')) {
    		$cell.not($thisCell).removeClass('is_expanded').addClass('is_collapsed');
    		$thisCell.removeClass('is_collapsed').addClass('is_expanded');
  		}

  	else {
    	$thisCell.removeClass('is_expanded').addClass('is_collapsed');
  	}
	 });

	$cell.find('.expanded_close').click(function(event){
		event.preventDefault();

  		let $thisCell = $(this).closest('.meal_plan_card');

  		$thisCell.removeClass('is_expanded').addClass('is_collapsed');
	});

	}

	componentDidMount() {
	$('#mealSearchModal').on('shown.bs.modal', function () {
      $(".modal-backdrop.in").hide();
   })
	}
	render() {
		return (
			<div>
				<div className="mp_heading_mb">
					<div className="row">
						<div className="col-lg-12">
							<div className="mp_heading">
								<h3>Meal Plan</h3>
							</div>
						</div>
					</div>
				</div>


				<div className="mp_body">
					<div className="row">
						<div className="col-lg-12">
							<div className="meal_plan">
								<div className="meal_plan_basicinfo">
									<form>
										<div className="row">
											<div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
												<div className="geninput">
													<div className="form-group">
														<label className="geninput_label" htmlFor="exampleInputAmount">Title</label>
														<div className="input-group geninput_group geninput_group_add">
															<input type="text" className="form-control geninput_formctrl" id="exampleInputAmount"/>
														</div>
													</div>
												</div>
											</div>

											<div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
												<div className="geninput">
													<div className="form-group">
														<label className="geninput_label" htmlFor="exampleInputAmount">Short Description</label>
														<div className="input-group geninput_group geninput_group_add">
															<input type="text" className="form-control geninput_formctrl" id="exampleInputAmount"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div className="meal_plan_search">
									<div className="row">
										<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div className="meal_plan_sb_wrapper">
												<div className="search_icon" data-toggle="tooltip" data-placement="top" title="Search Food">
													<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet" data-toggle="modal" data-target=".aklyMealSearchPrompt">
														<path d="M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z"/>
													</svg>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div className="meal_plan_selected_food">
									<div className="row">
										<div className="col-lg-3">
											<div className="selected_food_wrapper">
												<div className="selected_food" data-toggle="modal" data-target=".aklyFoodDetailPrompt">
													<div className="food_info">
														<div className="food_name">
															<p>Spicy Chicken</p>
														</div>
													</div>
													<div className="food_remove">
														<div className="food_remove_closeicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div className="col-lg-3">
											<div className="selected_food_wrapper">
												<div className="selected_food">
													<div className="food_info">
														<div className="food_name">
															<p>Spicy Chicken</p>
														</div>
													</div>
													<div className="food_remove">
														<div className="food_remove_closeicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div className="col-lg-3">
											<div className="selected_food_wrapper">
												<div className="selected_food">
													<div className="food_info">
														<div className="food_name">
															<p>Spicy Chicken</p>
														</div>
													</div>
													<div className="food_remove">
														<div className="food_remove_closeicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div className="col-lg-3">
											<div className="selected_food_wrapper">
												<div className="selected_food">
													<div className="food_info">
														<div className="food_name">
															<p>Spicy Chicken</p>
														</div>
													</div>
													<div className="food_remove">
														<div className="food_remove_closeicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div className="col-lg-3">
											<div className="selected_food_wrapper">
												<div className="selected_food">
													<div className="food_info">
														<div className="food_name">
															<p>Spicy Chicken</p>
														</div>
													</div>
													<div className="food_remove">
														<div className="food_remove_closeicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
														</div>
													</div>
												</div>
											</div>
										</div>





									</div>
								</div>

								<div className="meal_plan_view meal_plan_table">
									<div className="row">
										<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<div className="table-responsive">
                                                <table className="table table-bordered akly_admintable akly_admintable_gen">
                                                    <thead>
                                                        <tr>
                                                            <th>slot</th>
                                                            <th>Sunday</th>
                                                            <th>Monday</th>
                                                            <th>Tuesday</th>
                                                            <th>Wednesday</th>
                                                            <th>Thursday</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="meal_plan_slots">
																<div className="lunch">
																	<p>Lunch</p>
																</div>
																<div className="dinner">
																	<p>Dinner</p>
																</div>
                                                            </td>
                                                            <td className="meal_plan_added_foods">
																<div className="lunch">
																	<div className="added_food_card_wrapper">
																		<div className="added_food_card">
																			<div className="food_name">
																				<p data-toggle="modal" data-target=".aklyFoodDetailPrompt">Spicy Chicken</p>
																			</div>
																			<div className="food_quantity">
																				<div className="quantity_plus">
																					<div className="quantity_icon">
																						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/></svg>
																					</div>
																				</div>
																				<div className="quantity_total">
																					<p>1</p>
																				</div>
																				<div className="quantity_minus">
																					<div className="quantity_icon">
																						  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path style={{textIndent: 0, textAlign: 'start', lineHeight: 'normal', textTransform: 'none', blockProgression: 'tb', InkscapeFontSpecification: 'Sans', color: '#000', overflow: 'visible', fontFamily: '"Sans"'}} d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z" /></svg>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div className="added_food_card">
																			<div className="food_name">
																				<p>Spicy Chicken</p>
																			</div>
																			<div className="food_quantity">
																				<div className="quantity_plus">
																					<div className="quantity_icon">
																						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/></svg>
																					</div>
																				</div>
																				<div className="quantity_total">
																					<p>1</p>
																				</div>
																				<div className="quantity_minus">
																					<div className="quantity_icon">
																						  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path style={{textIndent: 0, textAlign: 'start', lineHeight: 'normal', textTransform: 'none', blockProgression: 'tb', InkscapeFontSpecification: 'Sans', color: '#000', overflow: 'visible', fontFamily: '"Sans"'}} d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z" /></svg>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div className="added_food_card">
																			<div className="food_name">
																				<p>Spicy Chicken</p>
																			</div>
																			<div className="food_quantity">
																				<div className="quantity_plus">
																					<div className="quantity_icon">
																						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/></svg>
																					</div>
																				</div>
																				<div className="quantity_total">
																					<p>1</p>
																				</div>
																				<div className="quantity_minus">
																					<div className="quantity_icon">
																						  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path style={{textIndent: 0, textAlign: 'start', lineHeight: 'normal', textTransform: 'none', blockProgression: 'tb', InkscapeFontSpecification: 'Sans', color: '#000', overflow: 'visible', fontFamily: '"Sans"'}} d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z" /></svg>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>

																<div className="dinner">

																</div>
                                                            </td>
                                                            <td className="meal_plan_added_foods">
																<div className="lunch">

																</div>

																<div className="dinner">

																</div>
                                                            </td>
                                                            <td className="meal_plan_added_foods">
																<div className="lunch">

																</div>

																<div className="dinner">

																</div>
                                                            </td>
                                                            <td className="meal_plan_added_foods">
																<div className="lunch">

																</div>

																<div className="dinner">

																</div>
                                                            </td>
                                                            <td className="meal_plan_added_foods">
																<div className="lunch">

																</div>

																<div className="dinner">

																</div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
										</div>
									</div>
									{/* <div className="meal_plan_view_wrapper">
										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">
												<div className="meal_plan_day">
													<h3>Sunday</h3>
												</div>
												<div className="meal_plan_slot">
													<h4>Lunch</h4>
												</div>
												<div className="meal_plan_itemwrapper">
													<DropItem />
												</div>
												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="#" className="expanded_close"></a>
												<div className="meal_item_quantitywrapper">
													<div className="meal_item_quantity">
														<div className="meal_item_info">
															<div className="meal_item_name">
																<p>Spicy Chicken</p>
															</div>
															<div className="meal_item_number">
																<p><span>x</span> 2</p>
															</div>
														</div>
														<div className="meal_item_cancel">
															<div className="meal_item_closeicon">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">
													<div className="meal_plan_day">
														<h3>Monday</h3>
													</div>
													<div className="meal_plan_slot">
														<h4>Lunch</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_day">
														<h3>Tuesday</h3>
													</div>
													<div className="meal_plan_slot">
														<h4>Lunch</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_day">
														<h3>Wednesday</h3>
													</div>
													<div className="meal_plan_slot">
														<h4>Lunch</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_day">
														<h3>Thursday</h3>
													</div>
													<div className="meal_plan_slot">
														<h4>Lunch</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_slot">
														<h4>Dinner</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_slot">
														<h4>Dinner</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_slot">
														<h4>Dinner</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_slot">
														<h4>Dinner</h4>
													</div>
													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>

										<div className="meal_plan_card is_collapsed">
											<div className="meal_plan_info">

													<div className="meal_plan_slot">
														<h4>Dinner</h4>
													</div>

													<div className="meal_plan_itemwrapper">
														<DropItem />
													</div>

												<div className="arrow_up"></div>
											</div>
											<div className="meal_plan_info_expanded">
												<a href="close-jump-0" className="expanded_close"></a>
													<div className="meal_item_quantitywrapper">
														<div className="meal_item_quantity">
															<div className="meal_item_info">
																<div className="meal_item_name">
																	<p>Spicy Chicken</p>
																</div>
																<div className="meal_item_number">
																	<p><span>x</span> 2</p>
																</div>
															</div>
															<div className="meal_item_cancel">
																<div className="meal_item_closeicon">
																	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>
									</div> */}
								</div>
                            </div>
                        </div>
                    </div>
                </div>
				<div className="modal aklyMealSearchPrompt meal-search-prompt" id="mealSearchModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div className="modal-dialog meal-search-prompt_dialog" role="document">
						<div className="modal-content meal-search-prompt_content">
							<div className="meal-search-prompt-close">
								<div className="close_icon" data-dismiss="modal">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
								</div>
							</div>
							<div className="modal-body">
								<div className="meal-search-sb">
									<div className="sb">
										<div className="form-group">
											<label className="pseudo_helperlabel">Search food, select it and then press enter.</label>
											<div className="sb_inputgroup">
												<input type="search" className="form-control sb_formctrl" id="exampleInputAmount" autoComplete="off" placeholder="Search Food"/>
											</div>

											<ul className="sb_dropdown">
												<li>
													<a href="#">
														<span>
															Spicy chicken
														</span>
													</a>
												</li>

												<li>
													<a href="#">
														<span>
															Spicy chicken
														</span>
													</a>
												</li>

												<li>
													<a href="#">
														<span>
															Spicy chicken
														</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="modal aklyFoodDetailPrompt food_detailsprompt" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
	                <div className="modal-dialog food_detailsprompt_dialog" role="document">
	                    <div className="modal-content food_detailsprompt_content">
							<div className="food_detailsprompt-close">
								<div className="close_icon" data-dismiss="modal">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
								</div>
							</div>
	                        <div className="modal-body">
								<div className="food_info_wrapper">
									<div className="food_basicinfo">
										<div className="row">
											<div className="col-lg-12">
												<div className="food_name">
													<p>Spicy Chicken</p>
												</div>
												<div className="food_price">
													<p>$299</p>
												</div>
												<div className="food_tags">
													<div className="food_tag">
														<p>spicy</p>
													</div>
													<div className="food_tag">
														<p>chicken</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div className="food_basicinfo_withimage">
										<div className="row">
											<div className="col-lg-6">
												<div className="food_description_wrapper">
													<div className="food_description">
														<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
													</div>
													<div className="food_ingredients">
														<h4>Ingredients</h4>
														<p>spicy, chicken</p>
													</div>
													<div className="food_recipe">
														<h4>Recipe</h4>
														<ol>
															<li>
																Lorem ipsum dolor sit amet
															</li>
															<li>
																consectetur adipiscing elit
															</li>
															<li>
																sed do eiusmod tempor incididunt
															</li>
														</ol>
													</div>
												</div>
											</div>
											<div className="col-lg-6">
												<div className="food_image_wrapper">
													<div className="food_image">
														<img src="/images/spicy_chicken.png" alt=""/>
													</div>
												</div>
											</div>
			                            </div>
									 </div>
								</div>

	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
		)
	}
}

export default MealPlanComponent