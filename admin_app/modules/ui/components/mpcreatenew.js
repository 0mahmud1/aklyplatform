import React, {Component} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

export default class MealPlanCreateNew extends Component {
    constructor(props) {
        super(props);

        this.searchMealItemClicked = this.searchMealItemClicked.bind(this);
    }

    MealPlanTitle() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Title</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <input type="text" className="form-control geninput_formctrl" ref='name' />
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    MealPlanShortDescription() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Short Description</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <input type="text" className="form-control geninput_formctrl" ref='description' />
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    generateCategoryList() {
      return (
          <option  value='chicken'>chicken</option>
      );
    }

    MealPlanCategory() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Categories</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <select className="form-control geninput_formctrl" id="categories-tags"
                        multiple="multiple">
                        {this.generateCategoryList()}
                      </select>
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    generatePlanType() {
        let planType=[
            {
                id: 1,
                value: 'lunchOnly',
                name: 'Lunch Only'
            },

            {
                id: 2,
                value: 'fullDay',
                name: 'Full Day'
            }
        ];

        return planType.map(el => {
            return (
                <option  key={el.id} value={el.value}>{el.name}</option>
            )
        })
    }

    MealPlanType() {
        return (
            <div className='col-lg-3'>
                <div className="geninput mp-builder-select2">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Type</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <select className="form-control geninput_formctrl" id="mealPlanType">
                        {this.generatePlanType()}
                      </select>
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    MealPlanTotalCalorie() {
        return (
            <div className="col-lg-3">
                <div className="geninput text-center">
                    <div className="form-group">
                      <label className="geninput_label" htmlFor="exampleInputAmount" style={{display: 'block', textAlign: 'left'}}>Total Calorie</label>
                      <p className='input-group geninput_group geninput_group_add' style={{textAlign: 'left'}}>
                        150 kCal
                      </p>
                    </div>
                </div>
            </div>
        );
    }

    MealPlanTotalPrice() {
        return (
            <div className="col-lg-3">
                <div className="geninput text-center">
                    <div className="form-group">
                      <label className="geninput_label" htmlFor="exampleInputAmount" style={{display: 'block', textAlign: 'left'}}>Total Price</label>
                      <p className='input-group geninput_group geninput_group_add' style={{textAlign: 'left'}}>
                        QR 300
                      </p>
                    </div>
                </div>
            </div>
        );
    }

    MealPlanSetPrice() {
        return (
            <div className="col-lg-3">
                <div className="geninput">
                    <div className="form-group">
                        <label className="geninput_label" htmlFor="exampleInputAmount">Set Price</label>
                        <div className="input-group geninput_group geninput_group_add">
                            <input type="text" className="form-control geninput_formctrl" ref='price' />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    generateTabs() {
        const TabNames = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

        return TabNames.map(el => {
            return (
                <Tab key={el}>{el}</Tab>
            )
        })
    }

    generateTableHeader() {
        const HeaderItems = ['Day', 'Breakfast', 'Lunch', 'Dinner'];

        return HeaderItems.map(el => {
            return (
                <th key={el} role={el.toLowerCase()}>{el}</th>
            )
        })
    }

    addMealIcon() {
        return (
            <span role='addmeal' data-toggle="modal" data-target=".aklyMealSearchPrompt">
                <i className='glyphicon glyphicon-plus'></i>
            </span>
        );
    }

    showSelectedFoods() {
        const Styles = {
            textIndent: 0,
            textAlign: 'start',
            lineHeight: 'normal',
            textTransform: 'none',
            blockProgression: 'tb',
            InkscapeFontSpecification: 'Sans',
            color: '#000',
            overflow: 'visible',
            fontFamily: '"Sans"'
        };

        return (
            <div className="added_food_card">
                <div className="food_info">
                    <div className="food_thunmbnailimg">
                        <img src="/images/spicy_chicken.png" />
                    </div>
                    <div className="food_name">
                        <p>Rye Bread Shakshooka Benedictine</p>
                    </div>
                </div>
                <div className="food_quantity text-center">
                    <div className="quantity_plus">
                        <div className="quantity_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"></path>
                            </svg>
                        </div>
                    </div>

                    <div className="quantity_total">
                        <p>1</p>
                    </div>
                    <div className="quantity_minus">
                        <div className="quantity_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z" style={{Styles}}></path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    generateTable() {
        return (
            <div className="meal_plan_view meal_plan_table">
                <div className="table-responsive">
                    <table className="table table-bordered akly_admintable akly_admintable_gen mp-builder-table">
                        <thead>
                            <tr>
                                {this.generateTableHeader()}
                            </tr>
                        </thead>

                        <tbody>
                            {this.generateTableRow()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    generateTableRow() {
        const days = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday'];

        return days.map(el => {
            return (
                <tr>
                    <td role='day'  style={{width: '130px'}}>
                        <span>{el}</span>
                    </td>
                    <td role='breakfast' style={{width: '350px'}}>
                        {this.addMealIcon()}
                        {this.showSelectedFoods()}
                    </td>
                    <td role='lunch' style={{width: '350px'}}>
                        {this.addMealIcon()}
                        {this.showSelectedFoods()}
                    </td>
                    <td role='dinner' style={{width: 'calc(100% - (130px + 350px + 350px))'}}>
                        {this.addMealIcon()}
                        {this.showSelectedFoods()}
                    </td>
                </tr>
            )
        })
    }

    searchMealItemClicked() {
        console.log('search Meal Item clicked');
    }

    generateTabPanels() {
        const PanelItems = ['week1', 'week2', 'week3', 'week4'];

        return PanelItems.map(el => {
            return (
                <TabPanel key={el}>
                    {this.generateTable()}
                </TabPanel>
            )
        })
    }

    renderSearchResult() {
        const Arr = [1, 2, 3, 4, 5];

      return Arr.map(el => {
        return (
          <li key={el} onClick={this.searchMealItemClicked}>
            <a href="#">
              <span className="food_thunmbnailimg">
                <img src='/images/spicy_chicken.png' />
              </span>
              <span className="food_name">
                Spicy Chicken
              </span>
            </a>
          </li>
        )
      })
    }

    componentDidMount() {
        $('#mealSearchModal').on('shown.bs.modal', function () {
          $(".modal-backdrop.in").hide();
      });

        let categoriesSelect2settings = {
          tags: true,
          placeholder: 'Select categories',
          tokenSeparators: [','],
        };

        let mealPlanTypeSelect2settings = {
          tags: false,
          placeholder: 'Select type',
          tokenSeparators: [','],
          minimumResultsForSearch: -1
        };

        $('#categories-tags').select2(categoriesSelect2settings);
        $('#mealPlanType').select2(mealPlanTypeSelect2settings);
    }

    render() {
      console.log('meal plan create component');
        return (
            <div>
                {/* page heading start */}
                <div className="mp_heading_mb">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="mp_heading">
                        <h3>Meal Plan</h3>
                      </div>
                    </div>
                  </div>
                </div>
                {/* page heading end */}

                {/* page body start */}
                <div className="mp_body">
                    <div className="row">
                        <div className="col-lg-12">

                            {/* meal plan content start */}
                            <div className="meal_plan">
                                <form>
                                    {/* meal plan basic information start */}
                                    <div className="meal_plan_basicinfo mb-40">
                                        <div className='row mb-25'>
                                            {this.MealPlanTitle()}
                                            {this.MealPlanShortDescription()}
                                            {this.MealPlanCategory()}
                                            {this.MealPlanType()}
                                        </div>

                                        <div className='row'>
                                            {this.MealPlanTotalCalorie()}
                                            {this.MealPlanTotalPrice()}
                                            {this.MealPlanSetPrice()}
                                        </div>
                                    </div>
                                    {/* meal plan basic information end */}

                                    {/* devider start */}
                                    <div className='admin-devider mb-25'></div>
                                    {/* devider end */}

                                    {/* tab start */}
                                    <div className='mp-builder-content'>
                                        <Tabs>
                                            <TabList>
                                                {this.generateTabs()}
                                            </TabList>
                                            
                                            {this.generateTabPanels()}

                                        </Tabs>
                                    </div>
                                    {/* tab end */}

                                </form>
                            </div>
                            {/* meal plan content end */}

                        </div>
                    </div>
                </div>
                {/* page body end */}

                {/* Search modal start */}
                <div className="modal aklyMealSearchPrompt meal-search-prompt" id="mealSearchModal" tabIndex="-1" role="dialog"
                  aria-labelledby="myModalLabel">
                  <div className="modal-dialog meal-search-prompt_dialog" role="document">
                    <div className="modal-content meal-search-prompt_content">
                      <div className="meal-search-prompt-close">
                        <div className="close_icon" data-dismiss="modal">
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet">
                            <path
                              d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z" />
                            <path
                              d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z" />
                          </svg>
                        </div>
                      </div>
                      <div className="modal-body">
                        <div className="meal-search-sb">
                          <div className="sb">
                            <div className="form-group">
                              <label className="pseudo_helperlabel">Search food, select & click.</label>
                              <div className="sb_inputgroup">
                                <input type="search" className="form-control sb_formctrl" autoComplete="off" id='search-food'
                                  placeholder="Search Food" />
                              </div>

                              <ul className="sb_dropdown">
                                {this.renderSearchResult()}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* Search modal end */}
            </div>
        );
    }
}
