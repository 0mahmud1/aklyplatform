'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import UiLayout from '../layouts'

import MealEditComponent from '../components/meal-edit.js'
import MealAddComponent from '../components/meal-add.js'
import MealTagAddComponent from '../components/meal-tag-add.js'
import MealTagInlineEditComponent from '../components/meal-tag-inlinedit.js'
import MealPlanComponent from '../components/meal-plan.js'
import VanAddComponent from '../components/van-add.js'
import MealPlanCreateNew from '../components/mpcreatenew.js'

import * as AuthService from '../../services/auth';

export default function () {
  return (
    <Route path='ui' component={UiLayout} onEnter={AuthService.isDashAuthRouter}>
        <IndexRoute component={MealEditComponent} />
        <Route path='mealadd' component={MealAddComponent} />
        <Route path='mealtagadd' component={MealTagAddComponent} />
        <Route path='mealtaginline' component={MealTagInlineEditComponent} />
        <Route path='mealplan' component={MealPlanComponent} />
        <Route path='vanadd' component={VanAddComponent} />
        <Route path='createnew' component={MealPlanCreateNew} />
    </Route>
  )
}
