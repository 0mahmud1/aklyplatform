'use strict'

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import moment from 'moment'
import toastr from 'toastr'
import $ from 'jquery'
import 'select2'

class VanModifyComponent extends Component {
    // componentDidMount(prev) {
    //
    // }

  heading() {
      return (
          <div className='mp_heading_mb'>
              <div className='row'>
                  <div className='col-lg-12'>
                      <div className='mp_heading'>
                          <h3>Edit Van Item</h3>
                      </div>
                  </div>
              </div>
          </div>
      )
  }

  nameField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="name">Name</label>
        <input ref='name' defaultValue={this.props.van.name} type="text" className="form-control geninput_formctrl" id="name" placeholder="Name"/>
      </div>
    );
  }

  emailField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="email">Email</label>
        <input ref='email' readOnly="readOnly" defaultValue={this.props.van.email} type="text" className="form-control geninput_formctrl" id="email" placeholder="Email"/>
      </div>
    );
  }

  passwordField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="name">Password</label>
        <input ref='password' readOnly="readOnly" defaultValue="********" type="password" className="form-control geninput_formctrl" id="password" placeholder="Password"/>
      </div>
    );
  }

  licenseField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="name">License No.</label>
        <input ref='license' defaultValue={this.props.van.license} type="text" className="form-control geninput_formctrl" id="license" placeholder="License No."/>
      </div>
    );
  }

  addressField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="address">Address</label>
        <input ref='address' defaultValue={this.props.van.address} type="text" className="form-control geninput_formctrl" id="address" placeholder="Address"/>
      </div>
    );
  }

  capacityField() {
    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="name">Capacity</label>
        <input ref='capacity' defaultValue={this.props.van.capacity} type="text" className="form-control geninput_formctrl" id="capacity" placeholder="Capacity"/>
      </div>
    );
  }



  assignField() {

    let isAssociated = this.props.van.isAssociated ? "checked" : "";

    return (
      <div className="form-group geninput">
        <label className="geninput_label" htmlFor="name">Assign</label>
        <input ref="isAssociated" defaultChecked={isAssociated} type="checkbox" />
      </div>
    )
  }

  form() {
      return (
          <div className="mp_body">
              <div className="row">
                  <div className="col-lg-12">
                      <div className="van_edit">
                          <form>
                            <div className="row" style={{marginBottom: '25px'}}>
                              <div className="col-md-6">
                                {this.nameField()}
                                {this.emailField()}
                                {this.passwordField()}
                              </div>
                              <div className="col-md-6">
                                {this.licenseField()}
                                {this.addressField()}
                                {this.capacityField()}
                                {this.assignField()}
                              </div>
                            </div>

                          </form>
                      </div>
                  </div>
              </div>
          </div>
      )
  }

  actionButtons() {
      return (
          <div className="meal_actionbtn actionbutn">
              <div className="row">
                  <div className="col-md-12 col-lg-12">
                      <div className="actionbtn_save">
                          <button type="submit" onClick={this.handleSubmit.bind(this)} className="btn butn butn_default">Save</button>
                      </div>
                      <div className="actionbtn_cancel">
                          <button type="button" className="btn butn butn_cancel" onClick={this.props.deleteVan}>Delete</button>
                      </div>
                      <div className="actionbtn_back">
                          <button type="button" className="btn butn butn_back" onClick={() => browserHistory.push('/vans')}>Back</button>
                      </div>
                  </div>
              </div>
          </div>
      )
  }

  render() {
    return (
      <div>
        {this.heading()}
        {this.form()}
        {this.actionButtons()}
      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault();
    const van = {};
    van.name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    van.email = ReactDOM.findDOMNode(this.refs.email).value.trim();
    van.license = ReactDOM.findDOMNode(this.refs.license).value.trim();
    van.password = ReactDOM.findDOMNode(this.refs.password).value.trim();
    van.address = ReactDOM.findDOMNode(this.refs.address).value.trim();
    van.capacity = ReactDOM.findDOMNode(this.refs.capacity).value.trim();
    van.isAssociated = ReactDOM.findDOMNode(this.refs.isAssociated).checked;
    //console.log(van.isAssociated);
    console.log('goes through validations');

      //  PROPER VALIDATION REQUIRED
      if (van.name.length < 1) {
          toastr.warning('Name is required');
          return false
      }
      else if (van.email.length < 1) {
          toastr.warning('Input email');
          return false
      }
      else if (van.address.length < 1) {
          toastr.warning('Input valid contact no');
          return false
      }
      else if (van.license.length < 1) {
          toastr.warning('Input valid license no');
          return false
      }
      // create a object
      //const van = {name, email, address , license}

      // call update action
      console.log('update van', van);
      this.props.updateVan(this.props.van._id, van)
  }
}

VanModifyComponent.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    van: PropTypes.object,
    deleteVan: PropTypes.func.isRequired,
    updateVan: PropTypes.func.isRequired
}

export default VanModifyComponent
