'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'
import { readVan, deleteVan, updateVan } from '../actions/index'
import VanModifyComponent from '../components/modify'
import {browserHistory} from 'react-router'

class VanModifyContainer extends Component{

    componentWillMount() {
        console.log(this.props.params._id)
        this.props.readVan(this.props.params._id);
    }

    deleteVan() {
        this.props.deleteVan(this.props.params._id, function (err, data) {
            if(data){
                browserHistory.push('/vans');
                toastr.success('Van deleted successfully!');
            }
            if(err){
                toastr.error(err);
            }
        });
    }

    updateVan(vanId, patch) {
        console.log(vanId, this);
        this.props.updateVan(vanId,patch, function (err, data) {
            if(data){
                toastr.success('Van updated Successfully!');
                browserHistory.push('/vans');
            }
            if(err){
                toastr.warning(err.message);
            }
        });
    }

    render() {
        let shouldLoad = (this.props.van && this.props.params._id === this.props.van._id)
        console.log("inside modify.js ",shouldLoad);
        return shouldLoad ? (
            <VanModifyComponent
                van={this.props.van}
                updateVan={this.updateVan.bind(this)}
                deleteVan={this.deleteVan.bind(this)}
            />
        ) : <h2>Loading ...</h2>
    }
}


function mapStateToProps (store) {
    return {
        van: store.vans.van
    }
}

// Get actions and pass them as props to to VanListContainer
//  > now VanListContainer has this.props.getVans
function mapDispatchToProps (dispatch) {
    return bindActionCreators({
        readVan: readVan,
        updateVan: updateVan,
        deleteVan: deleteVan
    }, dispatch)
}

// We don't want to return the plain VanListContainer (component) anymore,
// we want to return the smart Container
//  > VanListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(VanModifyContainer)
