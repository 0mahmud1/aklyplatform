'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import toastr from 'toastr'

import {createVan} from '../actions/index'
import VanCreateComponent from '../components/create'

class VanCreateContainer extends Component {
    render() {
        return (
            <VanCreateComponent
                vans = {this.props.vans}
                createVan={this.createVan.bind(this)}/>
        )
    }

    createVan(van) {
        console.log('inside container -> createVan');
        this.props.createVan(van, function (err, data) {
            if (data) {
                browserHistory.push('/vans');
                toastr.success('Van created Successfully.......!');
            }
            if (err) {
                toastr.error(err);
            }
        });
    }
}

VanCreateContainer.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    createVan: PropTypes.func.isRequired
}

function mapStateToProps(store) {
	return {
		vans: store.vans
	}
}

// Get actions and pass them as props to to VanCreateContainer
//  > now VanCreateContainer has this.props.createVan
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        createVan: createVan
    }, dispatch)
}

// We don't want to return the plain VanCreateContainer (component) anymore,
// we want to return the smart Container
//  > VanCreateContainer is now aware of actions
export default connect(mapStateToProps, matchDispatchToProps)(VanCreateContainer)
