'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getVans, getFilteredVans } from '../actions/index'
import VanListComponent from '../components/list'

class VanListContainer extends Component {
	componentWillMount () {
		this.props.getVans(sessionStorage.getItem('vanDataLimit'),sessionStorage.getItem('vanCurrentPage'));
	}

	render () {
		return (
			<VanListComponent
				getVans={this.props.getVans}
				getFilteredVans={this.props.getFilteredVans}
				totalPage={this.props.totalPage}
				total={this.props.total}
				vans={this.props.vans} />
		)
	}
}

VanListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 	getVans: PropTypes.func.isRequired,
 	vans: PropTypes.object.isRequired
}

// Get apps store and pass it as props to VanListContainer
//  > whenever store changes, the VanListContainer will automatically re-render
function mapStateToProps (store) {
	return {
		vans: store.vans,
		totalPage: store.vans.totalPage,
		total: store.vans.total
	}
}

// Get actions and pass them as props to to VanListContainer
//  > now VanListContainer has this.props.getVans
function matchDispatchToProps (dispatch) {
	return bindActionCreators({
		getVans: getVans,
        getFilteredVans: getFilteredVans
	}, dispatch)
}

// We don't want to return the plain VanListContainer (component) anymore,
// we want to return the smart Container
//  > VanListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(VanListContainer)
