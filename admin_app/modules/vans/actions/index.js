import axios from 'axios';
import toastr from 'toastr';

import * as constants from '../constants';

/**
 * @returns {Function}
 */

export function getVans(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: constants.GET_VANS_PENDING });
    axios.get('api/vans' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get vans... : ', response.data);
        dispatch({ type: constants.GET_VANS_RESOLVED, payload: response.data });
        //toastr.success('Van List Loaded');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_VANS_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}
export function getFilteredVans(options) {
    return function (dispatch) {
        let params = {}
        if(options.email){
            params.email = options.email
        }
        if(options.name){
            params.name = options.name
        }

        dispatch({ type: constants.GET_VANS_PENDING });
        axios.get('api/vans',{
            params: params
        })
            .then((response) => {
                console.log('get filtered vans... : ', response.data);
                dispatch({ type: constants.GET_VANS_RESOLVED, payload: response.data });
                //toastr.success('Van List Loaded');
            })
            .catch((err) => {
                dispatch({ type: constants.GET_VANS_REJECTED, payload: err });
                toastr.error(err);
            });
    };
}

/**
 * @param van
 * @param cb
 * @returns {Function}
 */

export function createVan(van, cb) {
  return function (dispatch) {
    dispatch({ type: constants.CREATE_VAN_PENDING });
    axios.post('api/vans', van)
      .then((response) => {
        dispatch({ type: constants.CREATE_VAN_RESOLVED, payload: response });
        if (cb) {
          cb(null, { type: constants.CREATE_VAN_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.CREATE_VAN_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

/**
 * @param _id
 * @returns {Function}
 */

export function readVan(_id) {
  return function (dispatch) {
    dispatch({ type: constants.READ_VAN_PENDING });
    axios.get('api/vans/' + _id)
      .then((response) => {
        dispatch({ type: constants.READ_VAN_RESOLVED, payload: response });
        //toastr.success('van found successfully');
      })
      .catch((err) => {
        dispatch({ type: constants.READ_VAN_REJECTED, payload: err });
        toastr.error("Van not found");
      });
  };
}

/**
 * @param patch
 * @param _id
 * @param cb
 * @returns {Function}
 */

export function updateVan(_id, patch, cb) {
  return function (dispatch) {
    dispatch({ type: constants.UPDATE_VAN_PENDING });
    axios.put('api/vans/' + _id, patch)
      .then((response) => {
        dispatch({ type: constants.UPDATE_VAN_RESOLVED, payload: response });
        if (cb) {
          cb(null, { type: constants.UPDATE_VAN_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.UPDATE_VAN_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

/**
 * @param _id
 * @param cb
 * @returns {Function}
 */

export function deleteVan(_id, cb) {
  return function (dispatch) {
    dispatch({ type: constants.DELETE_VAN_PENDING });
    axios.delete('api/vans/' + _id)
      .then((response) => {
        dispatch({ type: constants.DELETE_VAN_RESOLVED, payload: { _id: _id } });
        if (cb) {
          cb(null, { type: constants.DELETE_VAN_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.DELETE_VAN_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}
