'use strict'

import React, { Component } from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import VanLayout from '../layout';
import VanCreateContainer from '../containers/create.js';
import VanListContainer from '../containers/list';
import VanModifyContainer from '../containers/modify';
import * as AuthService from '../../services/auth';

export default function () {
return (
	<Route path='vans' component={VanLayout} onEnter={AuthService.isDashAuthRouter}>
		<IndexRoute component={VanListContainer} />
		<Route path='create' component={VanCreateContainer} />
		<Route path=':_id' component={VanModifyContainer} />
	</Route>
)
}
