import * as constants from '../constants';
import * as _ from 'lodash';

let defaultState = {
	van: {},
	vanList: []
};

const vanReducer = (state = defaultState, action) => {
	switch (action.type) {
		case constants.GET_VANS_PENDING:
			return Object.assign({}, state, {uploading: true});
    case constants.GET_VANS_RESOLVED:
			return Object.assign({}, state,
				{
					vanList: action.payload.data,
					uploading: false,
					total:action.payload.total,
					totalPage: action.payload.limit == 0 ? 1 : Math.ceil(action.payload.total / action.payload.limit)
				}
			);
		case constants.GET_VANS_REJECTED:
			return Object.assign({}, state, {uploading: false});

		case constants.CREATE_VAN_PENDING:
			return Object.assign({}, state,{uploading:true});
		case constants.CREATE_VAN_RESOLVED:
			return Object.assign({}, state, {van: action.payload, uploading:false});
		case constants.CREATE_VAN_REJECTED:
			return Object.assign({}, state, {uploading: false});

		case constants.READ_VAN_PENDING:
			return Object.assign({}, state,{uploading:true});
		case constants.READ_VAN_RESOLVED:
			return Object.assign({}, state, {van: action.payload.data.data, uploading:false});
		case constants.READ_VAN_REJECTED:
			return Object.assign({}, state, {uploading: false});

		case constants.UPDATE_VAN_PENDING:
			return Object.assign({}, state,{uploading:true});
		case constants.UPDATE_VAN_RESOLVED:
			return Object.assign({}, state, {van: action.payload, uploading:false});
		case constants.UPDATE_VAN_REJECTED:
			return Object.assign({}, state, {uploading: false});

		case constants.DELETE_VAN_PENDING:
			return Object.assign({}, state,{uploading:true});
		case constants.DELETE_VAN_RESOLVED:
			let filteredList = _.filter(state.vanList, (van) => {
				return van._id !== action.payload._id;
			});
			return Object.assign({}, state, {vanList: filteredList,uploading: false});
		case constants.DELETE_VAN_REJECTED:
			return Object.assign({}, state, {uploading: false});

		default :
			return state;
	}
};


export default vanReducer;
