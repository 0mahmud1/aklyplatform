'use strict'

import React, { Component } from 'react'
import {Link} from 'react-router'

export default class HomeClientComponent extends Component {

  render() {
    return (
      <div className="wlcmsg_wrapper">
        <div className="wlcmsg">
          <div className="wlcmsg_image">
            <img src="/images/akly_dbdlogo.png" alt="Akly logo"/>
          </div>
          <div className="wlcmsg_text">
            <p>We're still cooking this page.</p>
            <Link to="/" ><span>Visit Dashboard</span></Link>
          </div>
        </div>
      </div>
    )
  }
}
