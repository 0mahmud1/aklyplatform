import React, { Component } from 'react';
import { Link } from 'react-router';


export default class NavComponent extends Component {

	render() {
		return (
			<div className='navigbar akly_navigbar' data-hook='navigBar'>
				<div className='navigbar_layout navigbar_style'>
					<div className='ctrlof'>
						<div className='navigbar_content'>
							<div className='navigbar_icon'>
								<div className='navigbar_hamburger' data-hook='sidebarToggle'>
									<svg version='1' viewBox='0 0 24 30' preserveAspectRatio='xMidYMid meet'>
										<path
											d='M21 13H3c-.552 0-1-.448-1-1s.448-1 1-1h18c.552 0 1 .448 1 1s-.448 1-1 1zm0-7H3c-.552 0-1-.448-1-1s.448-1 1-1h18c.552 0 1 .448 1 1s-.448 1-1 1zM3 18h18c.552 0 1 .448 1 1s-.448 1-1 1H3c-.552 0-1-.448-1-1s.448-1 1-1z'/>
										</svg>
								</div>
							</div>

							<div className='navigbar_profile_logout_layout'>
								<div className='akly_navigbar_profilebox'>
									<div className='dropdown'>
										<div className='navigbar_profilebox' id='dLabel' data-toggle='dropdown' aria-haspopup='true'
										     aria-expanded='false'>
											<div className='navigbar_proimage'>
												<div className='propic_rbi'>
													<img src='/images/profilephoto.jpg' alt='Admin profile photo'/>
												</div>
											</div>
											<div className='navigbar_proname'>
												<p>Hello, {this.props.auth.user.name}</p>
											</div>
											<div className='navigbar_dropdown'>
												<div className='navigbar_downarrow'>
													<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 20'
													     preserveAspectRatio='xMidYMid meet'>
														<path
															d='M12.91 5.742c0 .077-.03.146-.088.206l-4.16 4.16c-.06.06-.13.09-.207.09-.077 0-.145-.03-.205-.09l-4.16-4.16c-.06-.06-.09-.13-.09-.206 0-.077.03-.146.09-.206l.446-.446c.06-.06.128-.09.206-.09.077 0 .145.03.205.09l3.51 3.51 3.508-3.51c.06-.06.128-.09.206-.09.078 0 .146.03.206.09l.446.446c.06.06.09.13.09.206z'
														/>
													</svg>
												</div>
											</div>
										</div>
										<ul className='dropdown-menu' id='menu2' aria-labelledby='dLabel'>
											<li><Link to='#'><span>Profile</span></Link></li>
											<li><Link to='/settings'><span>Settings</span></Link></li>
										</ul>
									</div>
								</div>
								<div className='navigbar_logout' onClick={() => this.props.logout()}>
									<div className='navigbar_logouticon' data-toggle='tooltip' data-placement='bottom' title='logout'>
										<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 125' preserveAspectRatio='xMidYMid meet'>
											<path d='M17.813 6A2 2 0 0 0 16 8v84a2 2 0 0 0 2 2h24a2 2 0 1 0 0-4H20V10h22a2 2 0 1 0 0-4H18a2 2 0 0 0-.188 0zM67.78 30.97a2 2 0 0 0-1.31 3.28L77.78 48H28a2 2 0 0 0-.188 0A2.002 2.002 0 1 0 28 52h49.78L66.47 65.72a2 2 0 1 0 3.06 2.53l14-17a2 2 0 0 0 0-2.53l-14-17a2 2 0 0 0-1.75-.75z'
											/>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	);
	}
}
