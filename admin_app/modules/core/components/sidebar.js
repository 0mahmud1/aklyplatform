'use strict';

import React, {Component} from 'react';
//import ReactDOM from 'react-dom'
import {Link} from 'react-router';
//import toastr from 'toastr';


export default class SidebarComponent extends Component {
  componentDidMount() {
    $('.is_single-item--clicked').on('click', function (event) {
      // event.preventDefault();
      const $collapsibleMenuEl = $('.is_collapsible');
      for (var i = 0; i < $collapsibleMenuEl.length; i++) {
        if (($collapsibleMenuEl).hasClass("in")) {
          let $tgtElId = "#" + $('.is_collapsible.in').attr("id");
          $($tgtElId).collapse('hide');
        }
      }
    });
  }

  render() {
    return (
      <div className="sidebar akly_sidebar">
        <div className="sidebar_layout sidebar_style sidebar_width" data-hook="sidbarLayout">
          <div className="ctrlof">
            <div className="sidebar_content">
              <div className="sidebar_logo">
                <img src="/images/akly_dbdlogo.png" alt="Akly logo"/>
              </div>

              <nav className="sidebar_menu">
                <ul className="sidebar_menuitems nav">
                  <li>
                    <Link to='/' activeClassName='active' className="is_single-item--clicked"><span>Dashboard</span></Link>
                  </li>

                  <li>
                    <Link to='/orders' activeClassName='active' className="is_single-item--clicked"><span>Orders</span></Link>
                  </li>

                  <li>
                    <Link to='/coupons' activeClassName='active' className="is_single-item--clicked"><span>Coupons</span></Link>
                  </li>

                    {/*<li>
                    <Link to='/live' activeClassName='active' className="is_single-item--clicked"><span>Live View</span></Link>
                  </li>*/}

                  <li>
                    <Link to='/simulator' activeClassName='active' className="is_single-item--clicked"><span>Live View</span></Link>
                  </li>

                  <li>
                    <Link to='/foods' activeClassName='active' className="is_single-item--clicked"><span>Dishes</span></Link>
                  </li>

                  <li>
                    <Link to='/categories' activeClassName='active' className="is_single-item--clicked"><span>Categories</span></Link>
                  </li>

                  <li>
                    <Link to='/tags' activeClassName='active' className="is_single-item--clicked"><span>Food Tags</span></Link>
                  </li>

                  <li>
                    <Link to='/mealplans' activeClassName='active' className="is_single-item--clicked"><span>Meal Plans</span></Link>
                  </li>

                  <li>
                    <a href="#" id="nav-user" data-toggle="collapse" data-target="#usermenu" aria-expanded="false">
                      <span>Profiles</span>
                    </a>

                    <div className="nav collapse is_collapsible akly_sidebar_submenu" id="usermenu"
                         aria-labelledby="nav-user">
                      <ul className="akly_sidebar_submenuitems">
                        <li>
                          <Link to='/foodProviders' activeClassName='active'>
                            <span>Food Provider</span>
                          </Link>
                        </li>

                        <li>
                          <Link to='/dboys' activeClassName='active'>
                            <span>Delivery Boy</span>
                          </Link>
                        </li>

                        <li>
                          <Link to='/vans' activeClassName='active'>
                            <span>Van Operator</span>
                          </Link>
                        </li>

                        <li>
                          <Link to='/customers' activeClassName='active'>
                            <span>Customer</span>
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li>
                    <Link to='/hubs' activeClassName='active' className="is_single-item--clicked"><span>Zone</span></Link>
                  </li>

                  <li>
                    <Link to='/settings' activeClassName='active'
                          className="is_single-item--clicked"><span>Settings</span>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
