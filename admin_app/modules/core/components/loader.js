import React, {
  Component,
  PropTypes,
} from 'react';

class LoaderBox extends Component {
  render() {
    return (
      <div className="spinner"></div>
    );
  }
}


export default LoaderBox;
