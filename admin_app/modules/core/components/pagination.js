'use strict'

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class PaginationComponent extends Component {

  setLimit() {
    const limit = ReactDOM.findDOMNode(this.refs.limit_select).value.trim();
    this.props.changeLimit(limit);
    console.log(limit)
  }

  renderLimitOptions() {
    return [5, 10, 20, 50, 100].map((value) => {
      return (
        <option key={value} value={value}>
          {value}
        </option>
      );
    })
  }

  render() {
    let current = sessionStorage.getItem(this.props.pageNumberStorage);
    console.log('current', current);
    let disableRight = current >= this.props.totalPage? 'pagination-button-disable' : '';
    console.log('disableRight', disableRight);
    let disableLeft = current <= 1 ? 'pagination-button-disable' : '';

    return (
      <div className="akly_adminpagination">
        <div className="col-xs-3">
          <select ref="limit_select" className="form-control" onChange={this.setLimit.bind(this)}
                  defaultValue={sessionStorage.getItem(this.props.dataLimitStorage) ? sessionStorage.getItem(this.props.dataLimitStorage) : process.env.LIMIT_DEFAULT}>
            {this.renderLimitOptions()}
          </select>
        </div>
        <div className="akly_paginationwrapper">
          <div className="akly_pagination_arrow_left" id={disableLeft}
               onClick={() => this.props.onPageChange(current > 1 ? (--current) : 1)}>
            <div className="icon_leftarrow">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 30" preserveAspectRatio="xMidYMid meet">
                <path d="M17.422 2.406L7.827 12l9.59 9.59L16 23 5 12 16.007.99z"/>
              </svg>
            </div>
          </div>
          <div className="akly_pagination_state" >
            <p>Page <span className="currentpage">{sessionStorage.getItem(this.props.pageNumberStorage)}</span> of {this.props.totalPage}</p>
          </div>
          <div className="akly_pagination_arrow_right" id={disableRight}
               onClick={() => this.props.onPageChange(current < this.props.totalPage ? (++current) : this.props.totalPage)}>
            <div className="icon_rightarrow">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 30" preserveAspectRatio="xMidYMid meet">
                <path d="M19 12L8 23l-1.413-1.414 9.59-9.59-9.595-9.594L7.996.988z"/>
              </svg>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
