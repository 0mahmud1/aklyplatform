import React, { Component, PropTypes } from 'react';

import NavContainer from '../containers/nav.js';
import SidebarComponent from '../components/sidebar.js';

import { pubnub } from '../../../pubnub';

class CoreLayout extends Component {
	componentWillMount() {
		console.log('CoreLayout...componentWillMount');
		pubnub.addListener({
			status: function (status) {
				console.log('PubNub status: ', status);
			},
		});

		pubnub.subscribe({
			channels: ['deliveryBoys-administrator'],
			withPresence: true,
		});

		pubnub.subscribe({
			channels: ['vanOperators-administrator'],
			withPresence: true,
		});
	}

	render() {
		console.log('CoreLayout...render');
		return (
			<div>
				<SidebarComponent />
				<NavContainer />
				{this.props.children}
			</div>
		);
	}
}

CoreLayout.propTypes = {
	children: PropTypes.element.isRequired,
}

export default CoreLayout;
