'use strict'

import React, { Component } from 'react'
//import { Link } from 'react-router'
import {connect} from 'react-redux';
import toastr from 'toastr';
import {bindActionCreators} from 'redux';
import $ from 'jquery'
import 'bootstrap/dist/js/bootstrap.min'
import {browserHistory} from 'react-router';
import NavComponent from '../components/nav.js';

class NavContainer extends Component {
	componentDidMount() {
		// declaring variables
		var screenWidth = 600;

//caching the jquery object in variables
		let $sidebarLayout = $('[data-hook="sidbarLayout"]');

// grouping selectors
		let $navigBarAndMpWrapper = $('[data-hook="navigBar"], [data-hook="mainPageWrapper"]');

// function for making sidebar toggle
		function makeSidebarToggle(event) {

			// check if the browser width is greater than 600px
			if (window.screen.width > screenWidth) {

				//check if the element has the particular class
				if (!$(this).hasClass('toggledState')) {
					$(this).addClass('toggledState');
					$sidebarLayout.removeClass('sidebar_slidein').addClass('sidebar_slideout');
					$navigBarAndMpWrapper.removeClass('navigbar_slideright').addClass('navigbar_slideleft');
				}

				else {
					$(this).removeClass('toggledState');
					$sidebarLayout.removeClass('sidebar_slideout').addClass('sidebar_slidein');
					$navigBarAndMpWrapper.removeClass('navigbar_slideleft').addClass('navigbar_slideright');
				}
			}

			else {
				if (!$(this).hasClass('toggledState')) {
					$(this).addClass('toggledState');
					$sidebarLayout.removeClass('sidebar_mobislideout').addClass('sidebar_mobislidein');
					$navigBarAndMpWrapper.removeClass('navigbar_mobislideleft').addClass('navigbar_mobislideright');
				}

				else {
					$(this).removeClass('toggledState');
					$sidebarLayout.removeClass('sidebar_mobislidein').addClass('sidebar_mobislideout');
					$navigBarAndMpWrapper.removeClass('navigbar_mobislideright').addClass('navigbar_mobislideleft');
				}
			}
		}


		$(document).ready(function() {

			// activating the tooltip
			$('[data-toggle="tooltip"]').tooltip();

			// sidebar toggle functionality
			$('[data-hook="sidebarToggle"]').click(makeSidebarToggle);

			var mealDescText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

			var mealIngredientText = "spice, chicken, garlic";

			var mealRecipeText = "1. Mix together brown sugar, honey, soy sauce, ginger, garlic and hot sauce in a small bowl.\n2. Lightly salt and pepper the chicken strips.";

			// inserting text area default value
			$('[data-insert="mealDescDefaultVal"]').val(mealDescText);
			$('[data-insert="mealIngredientDefaultVal"]').val(mealIngredientText);
			$('[data-insert="mealRecipecDefaultVal"]').val(mealRecipeText);
		});
	}
	constructor() {
		super();
		this.logout = this.logout.bind(this);
	}

	logout() {
		// hard-code removing from local storage
		localStorage.removeItem('jwt');
		localStorage.removeItem('email');

		// removing from redux store
		console.log('this...., ', this);
		this.props.logoutHard();
		browserHistory.push('/login');
	}
	settings() {
		toastr.success('roadmap feature...');
	}
	render() {
		return(
			<NavComponent
				auth={this.props.auth}
				logout={this.logout}
				settings={this.settings}
			/>
		);
	}
}

function mapStateToProps (store) {
	return {
		auth: store.auth
	}
}

const mapDispatchToProps = function(dispatch, ownProps) {
	return {
		logoutHard: function() {
			dispatch({ type:'USER_LOGOUT' });
		}
	}
};


export default connect(mapStateToProps, mapDispatchToProps)(NavContainer);