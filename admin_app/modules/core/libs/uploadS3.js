import aws from '../../configs/awsConfig.js';
import toastr from 'toastr';


export function uploadToS3(params, cb) {
    try {
        let s3 = new aws.S3();
        console.log('params', params);
        s3.upload(params,{queueSize: 5, partSize: 1024 * 1024 * 10}).on('httpUploadProgress', function (evt) {
            console.log('Uploaded :: ', params, " ==== " + parseInt((evt.loaded * 100) / evt.total) + '%');
        }).send(function (err, data) {
            if (err) {
                cb(err, null);
                // ONE NOTIFICATIONS GOES HERE
                toastr.error('Upload error');
            } else {
                console.log('result', data);
                cb(null, data);
            }
        });
    } catch (err) {
        console.log(err);
    }

}

export function generateUrlNames(original) {
    try {
        const chunks = original.split('/');

        // skipping index 1, because there's slash after http, which makes index 1 null.
        let baseUrl = chunks[0] + '//' + chunks[2];
        let directory = chunks[3];
        let fileName = chunks[4];

        // temp
        let mobile = '50x50';
        let tab = '100x100';
        let desktop = '400x400';

        let url1 = baseUrl + '/' + 'resized' + '/' + mobile + '_' + fileName;
        let url2 = baseUrl + '/' + 'resized' + '/' + tab + '_' + fileName;

        return [original, url1, url2];
    } catch (err) {
        console.log(err);
    }

}
