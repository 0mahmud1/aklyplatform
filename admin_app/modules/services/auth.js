
/*
* all these helper functions are for checking route authorization & log out purposes
* */
import store from '../../store'

'use strict';

import { browserHistory } from 'react-router';
//import moment from 'moment';
//import cookie from 'react-cookie';
import toastr from 'toastr';

/**
 * check user authentication from react router
 * @param nextState
 * @param replaceState
 * @returns void
 */
export function isDashAuthRouter(nextState, replaceState) {
	//console.log('service helper ', store.getState())
	if(!store.getState().auth.user) {
		toastr.warning('Need to login.');
		replaceState('/login');
	}
  let user = store.getState().auth.user;
  //console.log('service helper ', store.getState().auth.user)
  if(!user || !user.roles || user.roles[0] !== 'administrator' || new Date(user.exp*1000).getTime() < new Date().getTime())
  {
    toastr.warning('Need to login.');
    replaceState('/login');
  }
}