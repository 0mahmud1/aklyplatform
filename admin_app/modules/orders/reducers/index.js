import * as constants from '../constants'

let defaultState = {
  order: {},
  orderList: [],
  totalData: 0
};

const orderReducer = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_ORDERS_RESOLVED:
      // console.log(action.payload.data.data);
      return Object.assign({}, state,
        {
          orderList: action.payload.data.data,
          totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit), // if limit is 0 set total number of page to one});
          totalData: action.payload.data.total
        });
    case constants.GET_ORDERS_REJECTED:
      // console.log(action.payload.data.data);
      return Object.assign({}, state, {orderList: action.payload.data.data});

    case 'READ_ORDER_RESOLVED':
      return Object.assign({}, state, {order: action.payload.data.data});

    default :
      return state;
  }
};


export default orderReducer;