import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';
import * as _ from 'lodash';
export function getOrders(limit, page) {
  return function (dispatch) {
    dispatch({ type: constants.GET_ORDERS_PENDING });

    if(limit < 0){
      limit = 0;
    }
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;

    return axios.get('api/orders' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get orders... : ', response);
        dispatch({ type: constants.GET_ORDERS_RESOLVED, payload: response });
        //toastr.success('Order List Loaded');
        return response;
      })
      .catch((err) => {
        dispatch({ type: constants.GET_ORDERS_REJECTED, payload: err });
        toastr.error(err);
        return err;
      });
  };
}

export function getFilteredOrders(options) {
  return function (dispatch) {
    dispatch({ type: constants.GET_ORDERS_PENDING });
    let params = {};
    if(options.customerEmail) {
      params.customerEmail = options.customerEmail;
    }
    if(options.foodProviderEmail) {
      params.foodProviderEmail = options.foodProviderEmail;
    }
      if(options.orderRefId) {
          params.orderRefId = options.orderRefId;
      }
    let url = 'api/orders';
    axios.get(url, {
      params: params,
    }).then((response) => {
        console.log('get orders... : ', response);
        dispatch({ type: constants.GET_ORDERS_RESOLVED, payload: response });
        //toastr.success('Order List Loaded');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_ORDERS_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}


export function getOrdersByEmail(email) {
  return function (dispatch) {
    dispatch({ type: constants.GET_ORDERS_PENDING });
    console.log('email', email);
    if (_.includes(email, '+')) {
      email = email.replace('+', '%2B');
    }

    console.log('email', email);
    axios.get('api/orders' + '?email=' + email)
      .then((response) => {
        console.log('get orders... : ', response);
        dispatch({ type: constants.GET_ORDERS_RESOLVED, payload: response });
        //toastr.success('Order List Loaded');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_ORDERS_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}

export function getFilteredDeliveries(options) {
  return function (dispatch) {
    dispatch({ type: 'GET_DELIVERIES_PENDING' });
    let params = {};
    if(options.customerEmail) {
      params.customerEmail = options.customerEmail;
    }
    let url = 'api/deliveries';
    axios.get(url, {
      params: params
    }).then((response) => {
      console.log('get Delivery List... : ', response);
      dispatch({ type: 'GET_DELIVERIES_RESOLVED', payload: response });
      //toastr.success('Delivery List Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_DELIVERIES_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getDeliveries(orderRefId) {
  return function (dispatch) {
    dispatch({ type: 'GET_DELIVERIES_PENDING' });
    let url = 'api/deliveries';
    axios.get(url, {
      params: {
        orderRefId: orderRefId,
      },
    }).then((response) => {
      console.log('get Delivery List... : ', response);
      dispatch({ type: 'GET_DELIVERIES_RESOLVED', payload: response });
      //toastr.success('Delivery List Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_DELIVERIES_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}
