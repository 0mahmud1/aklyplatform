'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getOrdersByEmail } from '../actions/index'
import OrderListComponent from '../components/list'

class OrderListByMailContainer extends Component {
    componentWillMount () {
        console.log(this.props.params.email);
        this.props.getOrdersByEmail(this.props.params.email);
    }

    render () {
        let shouldLoad = this.props.orders.length > 0;
        return (
            shouldLoad ?
                <OrderListComponent
                    orders={this.props.orders} />
                :
                <div><h3>No Active Orders</h3></div>
        )
    }
}

/*OrderListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getOrders: PropTypes.func.isRequired,
 orders: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to OrderListContainer
//  > whenever store changes, the OrderListContainer will automatically re-render
function mapStateToProps (store) {
    return {
        orders: store.orders.orderList
    }
}

// Get actions and pass them as props to to OrderListContainer
//  > now OrderListContainer has this.props.getOrders
function mapDispatchToProps (dispatch, ownProps) {
    return bindActionCreators({
        getOrdersByEmail: getOrdersByEmail
    }, dispatch)
}

// We don't want to return the plain OrderListContainer (component) anymore,
// we want to return the smart Container
//  > OrderListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(OrderListByMailContainer)
