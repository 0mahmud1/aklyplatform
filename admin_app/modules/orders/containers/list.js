'use strict'

import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import { getOrders , getFilteredOrders } from '../actions/index'
import { readMealPlanAction } from '../../mealplan/actions/index'
import OrderListComponent from '../components/list'
import LoaderBox from '../../core/components/loader'


class OrderListContainer extends Component {
  constructor(props){
      super(props);
      this.state = {
        loader: false
      }
  }
  componentWillMount() {
    console.log(sessionStorage.getItem('orderDataLimit'), 'order limit+++++++', sessionStorage.getItem('orderCurrentPage'));
    return this.props.getOrders(sessionStorage.getItem('orderDataLimit'), sessionStorage.getItem('orderCurrentPage'))
      .then((res) => {
        this.setState({ loader: true });
      });
  }

  render() {
    console.log(this.state.loader);
    //let shouldLoad = this.props.orders  ? true : false
    return (
      this.state.loader ?
        <OrderListComponent
          getOrders={this.props.getOrders}
          getFilteredOrders={this.props.getFilteredOrders}
          resetSelectedMealPlan={this.props.resetSelectedMealPlan}
          readMealPlan={this.props.readMealPlan}
          totalPage={this.props.totalPage}
          totalData = {this.props.totalData}
          orders={this.props.orders}/> :
       <LoaderBox />
    )
  }
}

/*OrderListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getOrders: PropTypes.func.isRequired,
 orders: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to OrderListContainer
//  > whenever store changes, the OrderListContainer will automatically re-render
function mapStateToProps(store) {
  return {
    orders: store.orders.orderList,
    totalPage: store.orders.totalPage,
    totalData: store.orders.totalData
  }
}

// Get actions and pass them as props to to OrderListContainer
//  > now OrderListContainer has this.props.getOrders
function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators({
    getOrders: getOrders,
    getFilteredOrders: getFilteredOrders,
    readMealPlan: readMealPlanAction,
    resetSelectedMealPlan: () => dispatch({ type: 'RESET_MEALPLAN_REDUCER' }),
  }, dispatch)
}

// We don't want to return the plain OrderListContainer (component) anymore,
// we want to return the smart Container
//  > OrderListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(OrderListContainer)
