'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getDeliveries , getFilteredDeliveries } from '../actions/index'
import DeliveryListComponent from '../components/deliveryList'

class DeliveryListContainer extends Component {
    componentWillMount () {
        this.props.getDeliveries(this.props.params.orderRefId);
    }

    render () {
        let shouldLoad = this.props.deliveries.length > 0;
        return (
            shouldLoad ?
            <DeliveryListComponent
                deliveries={this.props.deliveries}
                mealPlan={this.props.mealPlan}
                getFilteredDeliveries={this.props.getFilteredDeliveries} />
            : <div className="spinner"></div>
        )
    }
}

/*DeliveryListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getDeliveries: PropTypes.func.isRequired,
 deliveries: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to DeliveryListContainer
//  > whenever store changes, the DeliveryListContainer will automatically re-render
function mapStateToProps (store) {
    return {
        deliveries: store.deliveries.deliveryList,
        mealPlan: store.mealPlans.mealPlan,
    }
}

// Get actions and pass them as props to to DeliveryListContainer
//  > now DeliveryListContainer has this.props.getDeliveries
// function matchDispatchToProps (dispatch) {
//     return bindActionCreators({
//         getDeliveries: (orderRefId) => dispatch(getDeliveries(orderRefId)),
//     }, dispatch)
// }

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getDeliveries : (orderRefId) => dispatch(getDeliveries(orderRefId)),
        getFilteredDeliveries: (options) => dispatch(getFilteredDeliveries(options)),

    }
}

// We don't want to return the plain DeliveryListContainer (component) anymore,
// we want to return the smart Container
//  > DeliveryListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(DeliveryListContainer)
