'use strict'

import React, { Component } from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import OrderLayout from '../layout'
import OrderListContainer from '../containers/list';
import DeliveryListContainer from '../containers/deliveryList'
import OrderListByMailContainer from '../containers/orderListByMailContainer'
import * as AuthService from '../../services/auth';

export default function () {
    return (
        <Route path='orders' component={OrderLayout} onEnter={AuthService.isDashAuthRouter}>
            <IndexRoute component={OrderListContainer} />
            {<Route path=':orderRefId' component={DeliveryListContainer}/>}
            {<Route path='email/:email' component={OrderListByMailContainer}/>}
        </Route>
    )
}
