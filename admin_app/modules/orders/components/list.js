'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory, Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn, SearchField } from 'react-bootstrap-table';
import moment from 'moment'
import toastr from 'toastr'
import { pubnub } from '../../../pubnub';
import ReactTable from 'react-table'
import PaginationComponent from '../../core/components/pagination'

class OrderListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        pageNum: 1,
        pageSize: 20
    };
  }

  componentDidMount() {
    const _this = this;

    pubnub.addListener({
      message: function (message) {
        console.log('PubNub message: ', message);
        toastr.success('New Order Placed');
        _this.props.getOrders(_this.state.currentLimit, _this.state.currentPage)
        //_this.props.getOrders(sessionStorage.getItem('orderDataLimit'),sessionStorage.getItem('orderCurrentPage'));
      },

      presence: function (presence) {
        console.log('PubNub presence: ', presence);
      },

      status: function (status) {
        console.log('PubNub status: ', status);
          _this.props.getOrders(_this.state.currentLimit, _this.state.currentPage)
          // _this.props.getOrders(sessionStorage.getItem('orderDataLimit'),sessionStorage.getItem('orderCurrentPage'));
      },
    });

    pubnub.subscribe({
      channels: [`orders-admin`],
      withPresence: true,
    });
  }

  // componentDidUpdate(){
  //     this.props.getOrders(this.state.currentLimit, this.state.currentPage)
  // }

  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading'>
              <h3>Orders</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
    
  fetchData(state, instance) {
      // console.log('fetchData state=>', state)
      if(state.filtering.length>0){
          let options = {}
          _.map(state.filtering, (item, i)=>{
              options[item['id']] = item['value']
          })
          // console.log('search options',options)
          this.props.getFilteredOrders(options)
      } else {
          this.setState({
              pageNum: state.page,
              pageSize: state.pageSize
          }, ()=>{
              this.props.getOrders(this.state.pageSize, this.state.pageNum+1)
          })
      }
  }

  skeleton() {
   const _this = this;
   const columns = [
       {
           header: 'Customer Email',
           accessor: 'customerEmail',
           sortable: false,
           filterRender: ({filter, onFilterChange}) => {
               return (
                   <input type='text'
                          placeholder="Search By Customer"
                          style={{
                              width: '100%'
                          }}
                          onChange={(event) => onFilterChange(event.target.value)}
                   />
               )
           },
           render:({row})=>{
             return row.createdBy.email
           },
           footer: (
               <span>
                    <strong>Total : </strong>{this.props.totalData}
                </span>
           )
       },
       {
           header: 'Order Ref-Id',
           accessor: 'orderRefId',
           sortable: false,
           filterRender: ({filter, onFilterChange}) => {
               return (
                   <input type='text'
                          placeholder="Search By orderRefId"
                          style={{
                              width: '100%'
                          }}
                          onChange={(event) => onFilterChange(event.target.value)}
                   />
               )
           }
       },
       {
           header: 'Date',
           accessor: 'createdAt',
           sortable: false,
           hideFilter: true,
           render:({row})=>{
               return moment(row.createdAt).format('lll')
           }
       },
       {
           header: 'Type',
           accessor: 'orderType',
           sortable: false,
           hideFilter: true,
           render:({row})=>{
               console.log(row.orderType)
               if (row.orderType ==='onDemand') {
                   return (<div style={{color: '#ec6608'}}>onDemand</div>)
               }
               else {
                   return (<div style={{color: '#b00835'}}>subscription</div>)
               }
           }
       },
       {
           header: 'Payment',
           accessor: 'paymentType',
           sortable: false,
           hideFilter: true,
       },
       {
           header: 'Provider',
           accessor: 'foodProviderEmail',
           sortable: false,
           filterRender: ({filter, onFilterChange}) => {
               return (
                   <input type='text'
                          placeholder="Search By Provider Email"
                          style={{
                              width: '100%'
                          }}
                          onChange={(event) => onFilterChange(event.target.value)}
                   />
               )
           },
           render:({row})=>{
               return (
                   <div>
                       <div>{row.provider.email}</div>
                       <div>{row.provider.name}</div>
                   </div>
               )
           }
       },

   ]
    return (
      <div className='akly_menulisttbl akly_listbl'>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
            <ReactTable
                manual
                className='-highlight custom-rt'
                data={this.props.orders}
                pages={this.props.totalPage}
                onChange={this.fetchData.bind(this)}
                columns={columns}
                defaultPageSize={20}
                minRows={0}
                showFilters = {true}
                style={{cursor: 'pointer'}}
                getTrProps={(state, rowInfo, column, instance) => ({
                    onClick: e => {
                         console.log('onClick...mealPlan', rowInfo.row.mealPlan)
                        if(rowInfo.row.mealPlan !== '' && rowInfo.row.mealPlan !== undefined){
                          _this.props.readMealPlan(rowInfo.row.mealPlan);
                        }
                        else {
                          _this.props.resetSelectedMealPlan();
                        }
                        // browserHistory.push('/orders/' + rowInfo.row.orderRefId)
                        window.open('/orders/' + rowInfo.row.orderRefId)
                    }
                })}
            />
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.heading()}
        {/*{this.searchBar()}*/}
        {this.skeleton()}
      </div>
    )
  }

}

OrderListComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  orders: PropTypes.array.isRequired
};

export default OrderListComponent
