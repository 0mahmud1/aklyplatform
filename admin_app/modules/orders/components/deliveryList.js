'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory, Link } from 'react-router'
import moment from 'moment'
import * as _ from 'lodash'
import ReactTable from 'react-table'

class DeliveryListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageNum: 1,
            pageSize: 20
        };
    }

    heading() {
        return (
            <div className='mp_heading_mb'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='mp_heading'>
                            <h3>Delivery List</h3>
                        </div>
                        <br/>
                        <div>
                            <h4 style={{color: '#ec6608'}}>Order id - {this.props.deliveries[0].orderRefId}</h4>
                        </div>
                        {!_.isEmpty(this.props.mealPlan) ?
                          <div>
                            <p>Meal Plan: {this.props.mealPlan.name}</p>
                            <p>Meal Plan Description: {this.props.mealPlan.description}</p>
                          </div>
                          :
                          <p>Not selected from meal plan</p>
                        }

                    </div>
                </div>
            </div>
        )
    }

    searchBar() {
        return (
            <div className='mp_body'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='akly_menulist akly_list'>
                            <div className='akly_listsearch-filter-add'>
                                <div className='akly_listsearch'>
                                    <div className='sb'>
                                        <div className='form-group'>
                                            <label className='sb_label' htmlFor='exampleInputAmount'>Search</label>
                                            <div className='input-group sb_inputgroup'>
                                                <input type='search' className='form-control sb_formctrl' id='exampleInputAmount'/>
                                                <div className='input-group-addon'>
                                                    <div className='sb_searchicon'>
                                                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 125'
                                                             preserveAspectRatio='xMidYMid meet'>
                                                            <path
                                                                d='M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z'/>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    skeleton() {

      const columns = [
          {
              header: 'Customer Email',
              accessor: 'customerEmail',
              sortable: false,
              filterRender: ({filter, onFilterChange}) => {
                  return (
                      <input type='text'
                             placeholder="Customer Email"
                             style={{
                                 width: '100%'
                             }}
                             onChange={(event) => onFilterChange(event.target.value)}
                      />
                  )
              },
              render:({row})=>{
                  return row.createdBy.email
              }
          },
          {
              header: 'Delivery Ref-id',
              accessor: 'deliveryRefId',
              sortable: false,
              filterRender: ({filter, onFilterChange}) => {
                  return (
                      <input type='text'
                             placeholder="DeliveryRefId"
                             style={{
                                 width: '100%'
                             }}
                             onChange={(event) => onFilterChange(event.target.value)}
                      />
                  )
              }
          },
          {
              header: 'Date',
              accessor: 'deliveryDate',
              sortable: false,
              hideFilter: true,
              render:({row})=>{
                  return moment(row.deliveryDate).format('lll')
              }
          },
          {
              header: 'Type',
              accessor: 'orderType',
              sortable: false,
              hideFilter: true,
              render:({row})=>{
                  console.log(row.orderType)
                  if (row.orderType ==='onDemand') {
                      return (<div style={{color: '#ec6608'}}>onDemand</div>)
                  }
                  else {
                      return (<div style={{color: '#b00835'}}>subscription</div>)
                  }
              }
          },
          {
              header: 'Payment',
              accessor: 'paymentType',
              sortable: false,
              hideFilter: true,
          },
          {
              header: 'Quantity',
              accessor: 'quantity',
              sortable: false,
              hideFilter: true,
          },
          {
              header: 'Shift',
              accessor: 'shift',
              sortable: false,
              filterRender: ({filter, onFilterChange}) => {
                  return (
                      <input type='text'
                             placeholder="Shift"
                             style={{
                                 width: '100%'
                             }}
                             onChange={(event) => onFilterChange(event.target.value)}
                      />
                  )
              }
          },
          {
              header: 'Status',
              accessor: 'status',
              sortable: false,
              hideFilter: true,
              render: ({row})=>{
                  if (row.status==='cancel') {
                      return <div className="text-danger">{row.status}</div>
                  } else if (row.status==='completed') {
                      return <div className="text-success">{row.status}</div>
                  } else {
                      return row.status
                  }
              }
          },
          {
              header: 'Subtotal / Total',
              accessor: 'total',
              sortable: false,
              hideFilter: true,
              render: ({row})=>{
                  return row.subTotal +' / '+row.total
              }
          },

      ]
      return (
        <div className='akly_menulisttbl akly_listbl'>
          <div className='row'>
            <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                <ReactTable
                    // manual
                    className='-highlight custom-rt'
                    data={this.props.deliveries}
                    // pages={this.props.totalPage}
                    // onChange={this.fetchData.bind(this)}
                    columns={columns}
                    defaultPageSize={20}
                    minRows={0}
                    showFilters = {true}
                    style={{cursor: 'pointer'}}
                    getTrProps={(state, rowInfo, column, instance) => ({
                        onClick: e => {
                            console.log('onClick', rowInfo)
                            browserHistory.push('/deliveries/' + rowInfo.row._id)
                        }
                    })}
                />
            </div>
          </div>
        </div>
      )
    }


    // skeleton() {
    //     return (
    //         <div className='akly_menulisttbl akly_listbl'>
    //             <div className='row'>
    //                 <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
    //                     <div className='table-responsive'>
    //                         <table className='table table-bordered akly_admintable akly_admintable_gen'>
    //                             <thead>
    //                             <tr>
    //                                 <th>Customer Email</th>
    //                                 <th>DeliveryRefId</th>
    //                                 <th>Order Type</th>
    //                                 <th>Date</th>
    //                                 <th>Payment Type</th>
    //                                 <th>Quantity</th>
    //                                 <th>Shift</th>
    //                                 <th>Status</th>
    //                                 <th>Subtotal</th>
    //                                 <th>Total</th>
    //                             </tr>
    //                             </thead>
    //                             <tbody>
    //                             {this.renderList()}
    //                             </tbody>
    //                         </table>
    //                     </div>
    //                 </div>
    //             </div>
    //         </div>
    //     )
    // }
    //
    // renderList () {
    //     // let dlvs = _.filter(this.props.deliveries, (d) => {
    //     //     return d.status !== 'cancel';
    //     // })
    //     return this.props.deliveries.map((delivery) => {
    //             let txtColor;
    //             if(delivery.status === 'cancel'){
    //                 txtColor = 'text-danger';
    //             } else if(delivery.status === 'completed') {
    //                 txtColor = 'text-success';
    //             } else {
    //                 txtColor = '';
    //             }
    //             return (
    //             <tr
    //                 key={delivery._id}
    //                 onClick={() => browserHistory.push('/deliveries/' + delivery._id)}>
    //                 <td>{delivery.createdBy.email}</td>
    //                 <td>{delivery.deliveryRefId}</td>
    //                 <td>{delivery.orderType}</td>
    //                 <td>{moment(delivery.deliveryDate).format('ll')}</td>
    //                 <td>{delivery.paymentType}</td>
    //                 <td>{delivery.quantity}</td>
    //                 <td>{delivery.shift}</td>
    //                 <td><span className={txtColor}>{delivery.status}</span></td>
    //                 <td>{delivery.subTotal}</td>
    //                 <td>{delivery.total}</td>
    //             </tr>
    //         )
    //
    //     })
    // }

    render () {
      console.log('DeliveryListComponent ',this.props);
        return (
            <div>
                {this.heading()}
                {this.skeleton()}
            </div>
        )
    }
}

DeliveryListComponent.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    deliveries: PropTypes.array.isRequired
}

export default DeliveryListComponent
