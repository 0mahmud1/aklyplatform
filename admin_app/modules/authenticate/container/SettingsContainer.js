/**
 * Created by Sajid on 2/5/17.
 */
import React, { Component } from 'react'
import {connect} from 'react-redux'
import {changePassword} from '../actions'
import SettingsComponent from '../settings_component'

class SettingsContainer extends Component{
    render(){
        return(
            <SettingsComponent
                email = {this.props.email}
                changePassword = {this.props.changePassword}
            />
        )
    }
}

const mapStateToProps = (store)=>{
    return {
        email: store.auth.user.email
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changePassword: (data) => { dispatch(changePassword(data))}
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer)