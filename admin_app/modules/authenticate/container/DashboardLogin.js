'use strict'

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import toastr from 'toastr'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import DashboardLogin from '../Login.js';
import { authorizeUser , getUserProfile } from '../actions.js';
import * as _ from 'lodash'

class DashboardLoginContainer extends Component {
  // constructor() {
  // 	super();
  // }
  componentDidMount() {
    console.log('process.env.NODE_ENV',process.env.NODE_ENV);
    console.log('container ...');
  }

  clickedSubmit(event) {
    let _this = this;
    event.preventDefault();

    this.props.authorizeUser(this.refs.child.getQuery(), function (err, res) {
      if(err) {
        toastr.error(err.data.message);
        return false;
      }
      if(_this.props.auth.user.roles[0] == 'administrator') {
        _this.props.getUserProfile('administrator', function(err, res) {
          if(err) {
            toastr.error('administrator err');
            return false;
          }
          browserHistory.push('/');
        });
      } else if(_this.props.auth.user.roles[0] == 'foodProvider') {
        _this.props.getUserProfile('foodproviders', function(err, res) {
          if(err) {
            toastr.error('foodproviders err');
            return false;
          }
          browserHistory.push('/provider/analytics');
        });
      } else {
        toastr.error('Un-Authorize User...');
        return false;
      }
    });
  }

  render() {
    return (
      <DashboardLogin ref="child" {...this.props} clickedSubmit={this.clickedSubmit.bind(this)}/>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // readDelivery: (_id) => dispatch(readDelivery(_id))
        authorizeUser: (user, cb) => dispatch(authorizeUser(user, cb)),
        getUserProfile: (role, cb) => dispatch(getUserProfile(role, cb))
    }
};

// function mapDispatchToProps(dispatch) {
//   return bindActionCreators({
//     authorizeUser: authorizeUser,
//     getUserProfile: getUserProfile
//   }, dispatch);
// }

function mapStateToProps(store) {
  return {
    auth: store.auth
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardLoginContainer);
