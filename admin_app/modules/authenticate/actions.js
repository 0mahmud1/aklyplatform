import axios from 'axios';
import toastr from 'toastr';

export function authorizeUserAjax(user) {
  let type = 'USER_AUTH';
  let payload = {};
  $.ajax({
    type: 'POST',
    url: 'api/authenticate',
    data: user,
    success: function (res) {
      payload = res;
      localStorage.setItem('jwt', res.token);
      localStorage.setItem('email', user.email);
      console.log('.. setting items');
    },

    error: function (err) {
      payload = err;
    },

    async: false,
  });
  return { type, payload, user };
}

export function getUserProfile(role, callback) {
  return function (dispatch) {
    dispatch({ type: 'USER_READ_PENDING' });
    axios.get('api/' + role + '/check')
      .then((response) => {
        localStorage.setItem('userProfile', JSON.stringify(response.data.data));
        dispatch({ type: 'USER_READ_RESOLVED', payload: response.data.data });
        callback(null, response);
      })
      .catch((err) => {
        dispatch({ type: 'USER_READ_REJECTED', payload: err });
        console.log(err);
        callback(err, null);
      });
  };
}

export function authorizeUser(user, cb) {
  return function (dispatch) {
    dispatch({ type: 'AUTHENTICATION_PENDING' });
    axios.post('api/authenticate', user)
      .then((response) => {
        //response.data.email = user.email;
        console.log('user info ', response);
        dispatch({ type: 'AUTHENTICATION_SUCCESS', payload: response.data.data });
        localStorage.setItem('jwt', response.data.data.id_token);
        localStorage.setItem('access_token', response.data.data.access_token);
        localStorage.setItem('email', response.data.data.email);
        localStorage.setItem('auth0Object', JSON.stringify(response.data.data));
        cb(null, response.data.data);
      })
      .catch((error) => {
        console.log('ACTION error info ', error);
        dispatch({ type: 'AUTHENTICATION_REJECTED' });
        cb(error.response, null);
      });
  };
}

export function changePassword(data) {
  return function (dispatch) {
    dispatch({ type: 'ADMIN_CHANGE_PASSWORD_PENDING' });
    axios.post('api/changePassword', data)
      .then((response) => {
        toastr.success('password changed');
        dispatch({ type: 'ADMIN_CHANGE_PASSWORD_SUCCESS' });
      })
      .catch((err) => {
        toastr.warning('Invalid password');
        dispatch({ type: 'ADMIN_CHANGE_PASSWORD_REJECT' });
      });
  };
}
