'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import AuthLayout from '../layouts';
import DashboardLoginContainer from '../container/DashboardLogin.js';

export default function () {
	return (
		<Route path='login' component={AuthLayout}>
			<IndexRoute component={DashboardLoginContainer} />
		</Route>
	)
}
