/**
 * Created by Sajid on 2/5/17.
 */
import React, {Component, PropTypes} from 'react'
import toastr from 'toastr'


class SettingsComponent extends Component{
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault()
        let _this = this
        if(_this.refs.newPassword.value !== _this.refs.confirmNewPassword.value ) {
            //if new password and confirm password does not match, warning show
            toastr.warning('New password and Confirm Password  does not match')
        } else {
            //toastr.success("same password")
            //new password and confirm password match, now check old password is correct or not
            let data = {
                email : _this.props.email,
                password : _this.refs.oldPassword.value,
                newPassword : _this.refs.newPassword.value
            }
            // console.log("change password values ",JSON.stringify(data))
            this.props.changePassword(data)

        }
    }

    render(){
        return (
            <div className='mp_wrapper akly_mp' data-hook="mainPageWrapper">
                <div className='mp_content'>
                    <div className="mp_heading_mb">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="mp_heading">
                                    <h3>Settings</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='mp_body'>
                        <form onSubmit={this.handleSubmit}>
                            <div className='row'>
                                <div className='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
                                    <div className='geninput'>
                                        <div className="form-group">
                                            <label className='geninput_label'>Old Password</label>
                                            <div className='input-group geninput_group geninput_group_add'>
                                                <input id="oldPassword" ref="oldPassword" name="oldPassword" type="password" placeholder="" className="form-control geninput_formctrl" required=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
                                    <div className='geninput'>
                                        <div className="form-group">
                                            <label className='geninput_label'>New Password</label>
                                            <div className='input-group geninput_group geninput_group_add'>
                                                <input id="newPassword" ref="newPassword" name="newPassword" type="password" placeholder="" className="form-control geninput_formctrl" required=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className='col-xs-12 col-sm-4 col-md-4 col-lg-4'>
                                    <div className='geninput'>
                                        <div className="form-group">
                                            <label className='geninput_label'>Confirm New Password</label>
                                            <div className='input-group geninput_group geninput_group_add'>
                                                <input id="confirmNewPassword" ref="confirmNewPassword" name="confirmNewPassword" type="password" placeholder="" className="form-control geninput_formctrl" required=""/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className='actionbutn' style={{marginTop: '2.5rem'}}>
                                <div className="actionbtn_save">
                                    <button id="changePassword" name="changePassword" className="btn butn butn_default">Change Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default SettingsComponent