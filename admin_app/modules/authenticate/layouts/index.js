'use strict'

import React, {Component, PropTypes} from 'react'


class AuthLayout extends Component {
	render() {
		console.log('AuthLayout...render');
		return (
			this.props.children
		)
	}
}

AuthLayout.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	children: PropTypes.element.isRequired
}

export default AuthLayout
