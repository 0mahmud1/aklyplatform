'use strict'

import React, { Component, PropTypes } from 'react'

class CustomerLayout extends Component {
	render() {
		console.log('CustomerLayout...render');
		return (
			<div className="mp_wrapper akly_mp" data-hook="mainPageWrapper">
				<div className="mp_content">
					{this.props.children}
				</div>
			</div>
		)
	}
}

CustomerLayout.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	children: PropTypes.element.isRequired
}

export default CustomerLayout
