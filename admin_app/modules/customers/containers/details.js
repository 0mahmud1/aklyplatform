'use strict'

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import { readCustomer } from '../actions/index'
import CustomerDetailsComponent from '../components/details'

class CustomerDetailsContainer extends Component {
    componentWillMount () {
        this.props.readCustomer(this.props.params._id);
    }

    render () {
        let shouldLoad  = this.props.customer && this.props.customer._id == this.props.params._id;
        console.log("inside CustomerDetailsContainer",shouldLoad);
        return shouldLoad?(
            <CustomerDetailsComponent
                customer={this.props.customer} />
        ): (<h2>Loading....</h2>)
    }
}

/*CustomerListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getCustomers: PropTypes.func.isRequired,
 customers: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to CustomerListContainer
//  > whenever store changes, the CustomerListContainer will automatically re-render
function mapStateToProps (store) {
    return {
        customer: store.customers.customer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        readCustomer: (_id) => dispatch(readCustomer(_id))
    }
};

// We don't want to return the plain CustomerListContainer (component) anymore,
// we want to return the smart Container
//  > CustomerListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetailsContainer)
