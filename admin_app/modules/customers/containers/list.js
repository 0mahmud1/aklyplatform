'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getCustomers, getFilteredCustomers } from '../actions/index'
import CustomerListComponent from '../components/list'

class CustomerListContainer extends Component {
	componentWillMount () {
		// this.props.getCustomers(sessionStorage.getItem('customerDataLimit'),sessionStorage.getItem('customerCurrentPage'));
		this.props.getCustomers(20,1);
	}

	render () {
		return (
			<CustomerListComponent
				getCustomers={this.props.getCustomers}
				getFilteredCustomers={this.props.getFilteredCustomers}
				customers={this.props.customers}
        		totalPage={this.props.totalPage}
				totalData={this.props.totalData}
			/>
		)
	}
}

/*CustomerListContainer.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	getCustomers: PropTypes.func.isRequired,
	customers: PropTypes.array.isRequired
}*/

// Get apps store and pass it as props to CustomerListContainer
//  > whenever store changes, the CustomerListContainer will automatically re-render
function mapStateToProps (store) {
	return {
		customers: store.customers.customerList,
    	totalPage: store.customers.totalPage,
		totalData: store.customers.totalData
	}
}

// Get actions and pass them as props to to CustomerListContainer
//  > now CustomerListContainer has this.props.getCustomers
function mapDispatchToProps (dispatch) {
	return bindActionCreators({
		getCustomers: getCustomers,
        getFilteredCustomers: getFilteredCustomers
	}, dispatch)
}

// We don't want to return the plain CustomerListContainer (component) anymore,
// we want to return the smart Container
//  > CustomerListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(CustomerListContainer)
