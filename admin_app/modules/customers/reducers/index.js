import * as constants from '../constants';

const defaultState = {
	customerList: [],
	customer: {},
	totalData: 0
};

const customersReducer = (state = defaultState, action) => {
	switch (action.type) {
		case constants.CREATE_CUSTOMER_PENDING :
			return Object.assign({}, state, {});

		case constants.CREATE_CUSTOMER_RESOLVED :
			return Object.assign({}, state, {});

		case constants.CREATE_CUSTOMER_REJECTED :
			return Object.assign({}, state, {error: action.payload.data.data});

		case constants.GET_CUSTOMER_PENDING:
			return Object.assign({}, state, {});

		case constants.GET_CUSTOMER_RESOLVED:
			return Object.assign({}, state,
				{
					customerList: action.payload.data.data,
					totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit), // if limit is 0 set total number of page to one
					totalData: action.payload.data.total
				}
			);

		case constants.GET_CUSTOMER_REJECTED:
			return Object.assign({}, state, {});


		case constants.READ_CUSTOMER_PENDING:
			return Object.assign({}, state, {});

		case constants.READ_CUSTOMER_RESOLVED:
			return Object.assign({}, state, {customer: action.payload.data.data});

		case constants.READ_CUSTOMER_REJECTED:
			return Object.assign({}, state, {});

		default :
			return state;
	}
};

export default customersReducer