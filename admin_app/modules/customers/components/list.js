'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory, Link } from 'react-router'
import moment from 'moment'
import ReactTable from 'react-table'

class CustomerListComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
        pageNum: 1,
        pageSize:20
    };
  }

	heading() {
		return (
			<div className='mp_heading_mb'>
				<div className='row'>
					<div className='col-lg-12'>
						<div className='mp_heading'>
							<h3>Customers</h3>
						</div>
					</div>
				</div>
			</div>
		)
	}
    fetchData(state, instance) {
        // console.log('fetchData state=>', state)
        if(state.filtering.length>0){
            let options = {}
            _.map(state.filtering, (item, i)=>{
                options[item['id']] = item['value']
            })
            console.log('search options',options)
            this.props.getFilteredCustomers(options)
        } else {
            this.setState({
                pageNum: state.page,
                pageSize: state.pageSize
            }, ()=>{
                this.props.getCustomers(this.state.pageSize, this.state.pageNum+1)
            })
        }
    }

	skeleton() {
	    const columns = [
            {
                header: 'Name',
                accessor: 'name',
                sortable: false,
                filterRender: ({filter, onFilterChange}) => {
                    return (
                        <input type='text'
                               placeholder="Search By Name"
                               style={{
                                   width: '100%'
                               }}
                               onChange={(event) => onFilterChange(event.target.value)}
                        />
                    )
                },
                footer: (
                    <span>
                    <strong>Total Customers: </strong>{this.props.totalData}
                </span>
                )
            },
            {
                header: 'Email',
                accessor: 'email',
                sortable: false,
                filterRender: ({filter, onFilterChange}) => {
                    return (
                        <input type='text'
                               placeholder="Search By Email"
                               style={{
                                   width: '100%'
                               }}
                               onChange={(event) => onFilterChange(event.target.value)}
                        />
                    )
                }
            },
            {
                header: 'Join Date',
                accessor: 'createdAt',
                sortable: false,
                hideFilter:true,
                render:({row})=>{
                    return moment(row.createdAt).format('lll')
                }
            }
        ]
		return (
			<div className='akly_menulisttbl akly_listbl'>
				<div className='row'>
					<div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>'

                        <ReactTable
                            manual
                            className='-highlight custom-rt'
                            data={this.props.customers}
                            pages={this.props.totalPage}
                            onChange={this.fetchData.bind(this)}
                            columns={columns}
                            defaultPageSize={20}
                            minRows={0}
                            showFilters = {true}
                            style={{cursor: 'pointer'}}
                            getTrProps={(state, rowInfo, column, instance) => ({
                                onClick: e => {
                                    // browserHistory.push('/customers/' + rowInfo.row._id)   //go to customer details page
                                    window.open('/customers/' + rowInfo.row._id)              //open in new tab
                                }
                            })}
                        />
					</div>
				</div>
			</div>
		)
	}

	render () {
		return (
			<div>
				{this.heading()}
				{this.skeleton()}
			</div>
		)
	}


}

// CustomerListComponent.propTypes = {
// 	// This component gets the task to display through a React prop.
// 	// We can use propTypes to indicate it is required
// 	customers: PropTypes.array.isRequired
// }

export default CustomerListComponent
