'use strict';

import React, {Component, PropTypes} from 'react'
import { Link } from 'react-router'

class CustomerDetailsComponent extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return(<div>hello</div>)
    }

    render() {
        return (
            <div>
                <div className="mp_heading_mb">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="mp_heading">
                                <h3>Customer Details</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mp_body">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="customer_details">
                                { this.props.customer
                                    ?
                                    <div>
                                        <div className='akly_menulisttbl akly_listbl'>
                                            <div className='row'>
                                                <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                                    <div className='table-responsive'>
                                                        <table
                                                            className='table table-bordered akly_admintable akly_admintable_gen'>
                                                            <tbody>
                                                            <tr>
                                                                <td>Email</td>
                                                                <td>{this.props.customer.email}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Name</td>
                                                                <td>{this.props.customer.name?this.props.customer.name:"No name provided"}</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td><Link to={`/orders/email/${this.props.customer.email}`}>Order Details</Link></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : <p><b><i>Loading.......</i></b></p>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/*CustomerDetailsComponent.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    customer: PropTypes.object
};*/

/*<tr>
 <td>Address</td>
 <tr>
 {
 this.props.customer.address.map((address) => {
 if(address === undefined || address === null)
 return (<td>No address</td>);
 else
 return (<td>Street {address.street}, Apartment no {address.apt}</td>)
 })
 }
 </tr>
 </tr>*/

export default CustomerDetailsComponent
