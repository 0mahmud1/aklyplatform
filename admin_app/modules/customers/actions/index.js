import { browserHistory } from 'react-router';
import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

// export function createCustomerAction(customer, cb) {
//     return function (dispatch) {
//         console.log('... in action ', customer);
//
//         dispatch({type: constants.CREATE_CUSTOMER_PENDING});
//         axios.post(API_URL + 'api/customers', customer)
//             .then((response) => {
//                 dispatch({type: constants.CREATE_CUSTOMER_RESOLVED});
//                 if (cb) {
//                     cb(null, {type: constants.CREATE_CUSTOMER_RESOLVED, payload: response});
//                 }
//             })
//             .catch((err) => {
//                 dispatch({type: constants.CREATE_CUSTOMER_REJECTED, payload: err});
//                 if (cb) {
//                     cb(err, null);
//                 }
//             });
//     }
// }

export function getFilteredCustomers(options) {
    return function (dispatch) {
        dispatch({ type: constants.GET_CUSTOMER_PENDING })
        let params = {}
        if (options.name){
            params.name = options.name
        }
        if (options.email){
            params.email = options.email
        }
        let url = 'api/customers';
        axios.get(url, {
            params: params,
        }).then((response) => {
            console.log('get filtered customer... : ', response);
            dispatch({ type: constants.GET_CUSTOMER_RESOLVED, payload: response });
            //toastr.success('Order List Loaded');
        })
            .catch((err) => {
                dispatch({ type: constants.GET_CUSTOMER_REJECTED, payload: err });
                toastr.error(err);
            });
    }

}

export function getCustomers(limit, page) {
  let l = limit ? limit : process.env.LIMIT_DEFAULT;
  let p = page ? page : process.env.PAGE_DEFAULT;
  return function (dispatch) {
    dispatch({ type: constants.GET_CUSTOMER_PENDING });
    axios.get('api/customers' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get customer... : ', response);
        dispatch({ type: constants.GET_CUSTOMER_RESOLVED, payload: response });
        //toastr.success('Customers loaded successfully!!');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_CUSTOMER_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}

export function readCustomer(_id) {
  return function (dispatch) {
    dispatch({ type: constants.READ_CUSTOMER_PENDING });
    console.log('api params: ', _id);
    axios.get('api/customers/' + _id)
      .then((response) => {
        console.log('get customer... : ', response);
        dispatch({ type: constants.READ_CUSTOMER_RESOLVED, payload: response });
        console.log('Read a Customer-Action: ', response);
        //toastr.success('Customer found successfully!!');
      })
      .catch((err) => {
        dispatch({ type: constants.READ_CUSTOMER_REJECTED, payload: err });
        console.warn('Read a Customer-Action: ', err);
        toastr.error("Customer not found");
      });
  };
}

// export function updateCustomer(_id, patch, cb) {
//     return function (dispatch) {
//         dispatch({type: constants.UPDATE_CUSTOMER_PENDING});
//
//         console.log('PATCH: ', patch, _id);
//
//         axios.put(API_URL + 'api/customers/' + _id, patch)
//             .then((response) => {
//                 console.log('UPDATE customer... : ', response);
//                 dispatch({type: constants.UPDATE_CUSTOMER_RESOLVED, payload: response});
//                 //console.log('createCustomerAction: ', response)
//                 if (cb) {
//                     cb(null, {type: constants.UPDATE_CUSTOMER_RESOLVED, payload: response});
//                 }
//             })
//             .catch((err) => {
//                 dispatch({type: constants.UPDATE_CUSTOMER_REJECTED, payload: err});
//                 //console.warn('getCustomer-Action: ', err)
//                 if (cb) {
//                     cb(err, null);
//                 }
//             });
//     }
// }

// export function deleteCustomer(_id, cb) {
//     return function (dispatch) {
//         axios.delete(API_URL + 'api/customers/' + _id)
//             .then((response) => {
//                 console.log('get customer... : ', response);
//                 dispatch({type: constants.DELETE_CUSTOMER_RESOLVED, payload: response});
//                 console.log('Read a Customer-Action: ', response);
//                 if (cb) {
//                     cb(null, {type: constants.DELETE_CUSTOMER_RESOLVED, payload: response});
//                 }
//             })
//             .catch((err) => {
//                 dispatch({type: constants.DELETE_CUSTOMER_REJECTED, payload: err});
//                 console.warn('Read a Customer-Action: ', err);
//                 if (cb) {
//                     cb(err, null);
//                 }
//             });
//     }
// }
