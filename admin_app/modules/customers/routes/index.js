'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import CustomerLayout from '../layouts'

import CustomerDetailsContainer from '../containers/details'
import CustomerListContainer from '../containers/list';
import * as AuthService from '../../services/auth';

export default function () {
	return (
		<Route path='customers' component={CustomerLayout} onEnter={AuthService.isDashAuthRouter}>
			<IndexRoute component={CustomerListContainer} />
			<Route path=':_id' component={CustomerDetailsContainer} />
		</Route>
	)
}
