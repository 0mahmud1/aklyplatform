/**
 * Created by sajid on 6/11/17.
 */
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import * as AuthService from '../../services/auth';

import CouponLayout from '../layout/index'
import  CouponListContainer from '../containers/list'
import CouponCreateContainer from '../containers/create'
import CouponUpdateContainer from '../containers/modify'

export default function () {
    return(
        <Route path="coupons" component={CouponLayout} onEnter={AuthService.isDashAuthRouter}>
            <IndexRoute component={CouponListContainer}/>
            <Route path='create' component={CouponCreateContainer} />
            <Route path=':_id' component={CouponUpdateContainer} />

        </Route>
    )
}