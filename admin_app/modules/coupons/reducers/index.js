/**
 * Created by sajid on 6/11/17.
 */
import * as constants from '../constants'

const defaultState = {
  coupons : [],
  coupon : {}
  // total : 0,
  // totalPage : 1

};

const couponReducer = (state = defaultState, action)=>{
    switch (action.type){
        case constants.READ_COUPON_RESOLVED:
            return Object.assign({}, state, {
                coupons: action.payload.data.data,
                total: action.payload.data.total,
                totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit)
            })

        case constants.CREATE_COUPON_RESOLVED:
            return Object.assign({}, state, {
                coupon: action.payload.data
            })
        case constants.GET_COUPON_PENDING:
            return state
        case constants.GET_COUPON_RESOLVED:
            return Object.assign({}, state, {
                coupon: action.payload.data
            })
        case constants.GET_COUPON_REJECTED:
            return state

        default:
            return defaultState
    }
}

export default couponReducer
