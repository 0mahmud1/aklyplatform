/**
 * Created by sajid on 6/11/17.
 */

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import toastr from 'toastr'
import moment from 'moment'
import 'select2/dist/js/select2.js';
import 'select2/dist/css/select2.css';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

class CouponUpdateComponent extends Component {
    constructor(props) {
        super(props);
        console.log('props ====>>>', this.props);
        this.state = {
            _id: this.props._id,
            startDate: this.props.coupon.startedAt ? moment(this.props.coupon.startedAt) : null,
            endDate: this.props.coupon.expiredAt ? moment(this.props.coupon.expiredAt) : null
        };
    }

    componentDidMount() {

        const coupontypeSelect2 = {
            tags: false,
            minimumResultsForSearch: -1
        };

        $('#couponType').select2(coupontypeSelect2);

    }
    heading() {
        return (
            <div className='mp_heading_mb'>
                <div className='row'>
                    <div className='col-lg-9'>
                        <div className='mp_heading'>
                            <h3>Update Coupon</h3>

                        </div>
                    </div>
                    <div className="col-lg-3">
                      <button type="button" className="btn butn butn_danger flr" data-toggle="modal"
                          data-target="#coupunDeleteConfirm">Delete
                      </button>
                    </div>
                </div>
            </div>
        )
    }

    form() {
        return (
            <div className="mp_body">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="meal_edit mbot5">
                            <form onSubmit={this.handleSubmit.bind(this)}>
                                <div style={{ marginBottom: '25px' }}>
                                    <div className="geninput" style={{ width: '400px' }}>
                                        <div className="form-group">
                                            <label className="cpn_label" htmlFor='Name'>Coupon Details</label>

                                            <div>
                                                <label>Coupon Code: </label> <input type="text" ref='code' defaultValue={this.props.coupon.code} className="form-control" /> <br />
                                                <label>Value: </label> <input type="text" ref='value' placeholder="0" defaultValue={this.props.coupon.value} className="form-control" /> <br />
                                                <div className="mbot1">
                                                    <label>Coupon Type: </label>
                                                    <select className="form-control" defaultValue={this.props.coupon.couponType} ref="couponType" id="couponType">
                                                        <option value="percentage">Percentage</option>
                                                        <option value="fixed" >Fixed</option>
                                                    </select>
                                                </div>
                                                <br />
                                                <label>Maximum Use: </label> <input type="text" ref='maxUsed' defaultValue={this.props.coupon.maxUsed} className="form-control" /> <br />
                                                {/* <label>Starts At: </label> <input type="date" ref='startedAt'/> <br/>
                                                <label>Expires At: </label> <input type="date" ref='expiredAt'/> <br/> */}
                                                <div className="cpn-datepicker mbot1">
                                                    <label>Coupon Start and Expires Date:</label>
                                                    <DateRangePicker
                                                        endDatePlaceholderText={"Expired Date"}
                                                        startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                        endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                        onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                                        focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                        onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                    />
                                                </div>
                                                <label>Description: </label> <textarea className="form-control" defaultValue={this.props.coupon.description} rows="5" ref="description" id="comment"></textarea> <br />
                                            </div>


                                        </div>
                                    </div>
                                </div>


                                <div className="user_create_actionbtn actionbutn">
                                    <div className="row">
                                        <div className="col-md-12 col-lg-12">
                                            <div className="actionbtn_save">
                                                <button type="submit" className="btn butn butn_default">Update</button>
                                            </div>

                                            <div className="actionbtn_back">
                                                <button type="button" className="btn butn butn_back" onClick={() => browserHistory.push('/coupons')}>Back</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    deleteModal() {
        return (
            <div
                id='coupunDeleteConfirm'
                className='modal fade delprompt'
                tabIndex='-1'
                role='dialog'>
                <div className='modal-dialog delprompt_dialog' role='document'>
                    <div className='modal-content delprompt_content'>
                        <div className='modal-body'>
                            <div className="delprompt_text">
                                <p>Do you want to delete this Coupun?</p>
                            </div>
                        </div>
                        <div className='modal-footer'>
                            <button type="button" className="btn butn butn_danger" data-dismiss="modal" onClick={this.deleteCoupun.bind(this)}>Delete</button>
                            <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    handleSubmit(event) {
        event.preventDefault()

        const code = ReactDOM.findDOMNode(this.refs.code).value.trim()
        const value = ReactDOM.findDOMNode(this.refs.value).value.trim()
        const couponType = ReactDOM.findDOMNode(this.refs.couponType).value.trim().toLowerCase()
        const maxUsed = ReactDOM.findDOMNode(this.refs.maxUsed).value.trim()
        const description = ReactDOM.findDOMNode(this.refs.description).value.trim()

        if (code.length < 1 || value < 1) {
            toastr.warning("Code and value should be valid")
            return false
        }

        let option = {
            code: code,
            value: value,
            couponType: couponType,
            maxUsed: maxUsed,
            startedAt: this.state.startDate ? moment(this.state.startDate, 'YYYY-MM-DD').format('YYYY-MM-DD') : null,
            expiredAt: this.state.endDate ? moment(this.state.endDate, 'YYYY-MM-DD').format('YYYY-MM-DD') : null,
            description: description
        }

        this.props.updateCoupon(this.state._id, option, (error, data) => {
            if (data) {
                browserHistory.push('/coupons')
                toastr.success("Coupon Update successfully")
            } else {
                toastr.error("Unable to Update coupon")
            }
        })

    }

    deleteCoupun(){
        this.props.deleteCoupon(this.state._id, (error, data) => {
            if (data) {
                browserHistory.push('/coupons')
                toastr.success("Coupon Delete successfully")
            } else {
                toastr.error("Unable to Delete coupon")
            }
        })
    }
    render() {
        return (
            <div>
                {this.heading()}
                {this.form()}
                {this.deleteModal()}
            </div>
        )
    }
}

export default CouponUpdateComponent
