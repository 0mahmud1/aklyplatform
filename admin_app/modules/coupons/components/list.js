/**
 * Created by sajid on 6/11/17.
 */

import React, {Component, PropTypes} from 'react'
import {browserHistory, Link} from 'react-router'
import ReactTable from 'react-table'
import moment from 'moment'

class CouponListComponent extends Component{
    constructor(props) {
        super(props);
        this.state = {
            pageNum: 1,
            pageSize:20
        };
    }


    heading() {
        return (
            <div className='mp_heading_mb'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='mp_heading'>
                            <h3>Coupons</h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    searchBar() {
        return (
            <div className='mp_body'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='akly_menulist akly_list'>
                            <div className='akly_listsearch-filter-add'>

                                <div className='akly_addnew' data-toggle='tooltip' data-placement='top'
                                     title='Add coupon'>
                                    <Link to='/coupons/create' className='addbtn'>
											<span className='icon_plus'>
												<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'
                                                     preserveAspectRatio='xMidYMid meet'>
													<path
                                                        d='M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z'/>
												</svg>
											</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    fetchData(state, instance) {
        console.log('fetchData state=>', state.filtering)
        this.props.readCoupons(state.pageSize, state.page+1)
    }

    skeleton() {

        const columns = [
            {
                header: 'Code',
                accessor: 'code',
                sortable: false,
            },
            {
                header: 'Value',
                accessor: 'value',
                sortable: false,
            },
            {
                header: 'coupon Type',
                accessor: 'couponType',
                sortable: false,
            },
            {
                header: 'Maximum Use',
                accessor: 'maxUsed',
                sortable: false,
            },
            {
                header: 'Starting Date',
                accessor: 'startedAt',
                sortable: false,
                render:({row})=>{
                    if (row.startedAt) {
                        return moment(row.startedAt).format("MMM-DD-YYYY")
                    } else {
                        return null
                    }
                }
            },
            {
                header: 'Expiring Date',
                accessor: 'expiredAt',
                sortable: false,
                render:({row})=>{
                    if (row.startedAt) {
                        return moment(row.expiredAt).format("MMM-DD-YYYY")
                    } else {
                        return null
                    }
                }
            }
        ]
        return (
            <div className='akly_menulisttbl akly_listbl'>
                <div className='row'>
                    <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                        <ReactTable
                            manual
                            className='-highlight custom-rt'
                            data={this.props.coupons}
                            pages={this.props.totalPage}
                            onChange={this.fetchData.bind(this)}
                            columns={columns}
                            defaultPageSize={20}
                            minRows={0}
                            showFilters = {false}
                            style={{cursor: 'pointer'}}
                            getTrProps={(state, rowInfo, column, instance) => ({
                                onClick: e => {
                                    // browserHistory.push('/foodProviders/' + rowInfo.row._id)
                                    window.open('/coupons/' + rowInfo.row._id)
                                }
                            })}
                        />
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                {this.heading()}
                {this.searchBar()}
                {this.skeleton()}
            </div>
        )
    }






}

export default CouponListComponent