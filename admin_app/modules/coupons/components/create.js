/**
 * Created by sajid on 6/11/17.
 */

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import toastr from 'toastr'
import moment from 'moment'
import 'select2/dist/js/select2.js';
import 'select2/dist/css/select2.css';
import { DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';


class CouponCreateComponent extends Component{

    constructor(props){
    super(props);
    this.state={
      startDate: null,
      endDate : null
    };
  }

    componentDidMount() {

        const coupontypeSelect2 = {
            tags: false,
            minimumResultsForSearch: -1
        };

        $('#couponType').select2(coupontypeSelect2);

    }

    heading() {
        return (
            <div className='mp_heading_mb'>
                <div className='row'>
                    <div className='col-lg-12'>
                        <div className='mp_heading'>
                            <h3>Create New Coupon</h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    form() {
        return(
            <div className="mp_body">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="meal_edit mbot5">
                            <form onSubmit={this.handleSubmit.bind(this)}>
                                <div style={{marginBottom: '25px'}}>
                                    <div className="geninput" style={{width: '400px'}}>
                                        <div className="form-group">
                                            <label className="cpn_label" htmlFor='Name'>Coupon Details</label>

                                            <div>
                                                <label>Coupon Code: </label> <input type="text" ref='code' className="form-control"/> <br/>
                                                <label>Value: </label> <input type="text" ref='value' placeholder="0"  className="form-control"/> <br/>
                                                <div className="mbot1">
                                                <label>Coupon Type: </label>
                                                <select className="form-control"  ref="couponType" id="couponType">
                                                        <option value="percentage">Percentage</option>
                                                        <option value="fixed" >Fixed</option>
                                                </select>
                                                </div>
                                                <br/>
                                                <label>Maximum Use: </label> <input type="text" ref='maxUsed' className="form-control"/> <br/>
                                                {/* <label>Starts At: </label> <input type="date" ref='startedAt'/> <br/>
                                                <label>Expires At: </label> <input type="date" ref='expiredAt'/> <br/> */}
                                                <div className="cpn-datepicker mbot1">
                                                  <label>Coupon Start and Expires Date:</label>
                                                  <DateRangePicker
                                                    endDatePlaceholderText={"Expired Date"}
                                                    startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                                    endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                                    onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                                    focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                                    onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                                  />
                                                </div>
                                                <label>Description: </label> <textarea className="form-control" rows="5" ref="description" id="comment"></textarea> <br/>
                                            </div>


                                        </div>
                                    </div>
                                </div>


                                <div className="user_create_actionbtn actionbutn">
                                    <div className="row">
                                        <div className="col-md-12 col-lg-12">
                                            <div className="actionbtn_save">
                                                <button type="submit" className="btn butn butn_default">Create</button>
                                            </div>

                                            <div className="actionbtn_back">
                                                <button type="button" className="btn butn butn_back" onClick={() => browserHistory.push('/coupons')}>Back</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    handleSubmit (event) {
        event.preventDefault()

        const code = ReactDOM.findDOMNode(this.refs.code).value.trim()
        const value = ReactDOM.findDOMNode(this.refs.value).value.trim()
        const couponType = ReactDOM.findDOMNode(this.refs.couponType).value.trim().toLowerCase()
        const maxUsed = ReactDOM.findDOMNode(this.refs.maxUsed).value.trim()
        // const startedAt = ReactDOM.findDOMNode(this.refs.startedAt).value.trim()
        // const expiredAt = ReactDOM.findDOMNode(this.refs.expiredAt).value.trim()
        const description = ReactDOM.findDOMNode(this.refs.description).value.trim()

        if(code.length<1 || value<1){
            toastr.warning("Code and value should be valid")
            return false
        }

        let option = {
            code: code,
            value: value,
            couponType: couponType,
            maxUsed: maxUsed,
            startedAt: this.state.startDate ? moment(this.state.startDate, 'YYYY-MM-DD').format('YYYY-MM-DD') : null,
            expiredAt: this.state.endDate ? moment(this.state.endDate, 'YYYY-MM-DD').format('YYYY-MM-DD') : null,
            description: description
        }
        console.log('option ==>>', option);
        this.props.createCoupon(option, (error, data)=>{
            if(data){
                browserHistory.push('/coupons')
                toastr.success("Coupon created successfully")
            } else {
                toastr.error("Unable to create coupon")
            }
        })

    }


    render () {
        return (
            <div>
                {this.heading()}
                {this.form()}
            </div>
        )
    }
}

export default CouponCreateComponent
