/**
 * Created by sajid on 6/11/17.
 */

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { readCoupons } from '../action/index'
import CouponListComponent from '../components/list'

class CouponListContainer extends Component{
    componentWillMount(){
        this.props.readCoupons(20,1)
    }

    render(){

        return (
            <CouponListComponent
                coupons = {this.props.coupons}
                total = {this.props.total}
                totalPage = {this.props.totalPage}
                readCoupons = {this.props.readCoupons}
            />

        )
    }
}


function mapStateToProps(store) {
    return{
        coupons:store.coupons.coupons,
        total: store.coupons.total,
        totalPage: store.coupons.totalPage
    }
}

function matchDispatchToProps (dispatch) {
    return bindActionCreators({
        readCoupons: readCoupons
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CouponListContainer)
