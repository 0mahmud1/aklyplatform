/**
 * Created by sajid on 6/11/17.
 */
import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { createCoupon } from '../action/index'
import CouponCreateComponent from '../components/create'


class CouponCreateContainer extends Component{
    render(){
        return(
            <CouponCreateComponent
                createCoupon = {this.props.createCoupon}
            />
        )
    }
}

CouponCreateContainer.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    createCoupon: PropTypes.func.isRequired
}

// Get actions and pass them as props to to TagCreateContainer
//  > now TagCreateContainer has this.props.createTag
function matchDispatchToProps (dispatch) {
    return bindActionCreators({
        createCoupon: createCoupon
    }, dispatch)
}

// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(null, matchDispatchToProps)(CouponCreateContainer)
