/**
 * Created by sajid on 6/11/17.
 */
import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { updateCoupon, getCoupon, deleteCoupon } from '../action/index'
import CouponUpdateComponent from '../components/modify'


class CouponUpdateContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            loader : true
        }
    }
    componentWillMount(){
        console.log('get id ====>>>', this.props.params);
        this.props.getCoupon(this.props.params._id)
        .then(res =>{
            this.setState({loader : false});
        });

    }
    render(){
        return(
            !this.state.loader ? 
            <CouponUpdateComponent
                updateCoupon = {this.props.updateCoupon}
                deleteCoupon = {this.props.deleteCoupon}
                _id = {this.props.params._id}
                coupon = {this.props.coupon}
            /> : null
        ) 
    }
}

CouponUpdateContainer.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    updateCoupon: PropTypes.func.isRequired
}

function mapStateToProps(store){
    return {
        coupon: store.coupons.coupon,
    }
}


// Get actions and pass them as props to to CouponUpdateContainer
//  > now CouponUpdateContainer has this.props.createTag
function matchDispatchToProps (dispatch) {
    return bindActionCreators({
        updateCoupon: updateCoupon,
        deleteCoupon: deleteCoupon,
        getCoupon : getCoupon
    }, dispatch)
}

// We don't want to return the plain CouponUpdateContainer (component) anymore,
// we want to return the smart Container
//  > CouponUpdateContainer is now aware of actions
export default connect(mapStateToProps, matchDispatchToProps)(CouponUpdateContainer)
