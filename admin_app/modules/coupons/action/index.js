/**
 * Created by sajid on 6/11/17.
 */

import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

export function readCoupons(limit, page) {
    return function (dispatch) {
        dispatch({type: constants.READ_COUPON_PENDING});
        let l = limit ? limit : process.env.LIMIT_DEFAULT;
        let p = page ? page : process.env.PAGE_DEFAULT;
        axios.get('api/coupons' + '?limit=' + l + '&page=' + p)
            .then(response => {
                // console.log('get coupon : ', response);
                dispatch({type: constants.READ_COUPON_RESOLVED, payload: response});
            })
            .catch(error=>{
                dispatch({type: constants.READ_COUPON_REJECTED, payload:error});
                toastr.error("Failed to get coupons")
            })
    }
}
export function getCoupon(_id) {
    return function (dispatch) {
        dispatch({type: constants.GET_COUPON_PENDING});
       return axios.get('api/coupons' + '/' + _id)
            .then(response => {
                // console.log('get coupon : ', response);
                dispatch({type: constants.GET_COUPON_RESOLVED, payload: response.data});
            })
            .catch(error=>{
                dispatch({type: constants.GET_COUPON_REJECTED, payload:error});
                toastr.error("Failed to get coupons")
            })
    }
}

export function createCoupon(options, callback) {
    console.log("actions", options)
    return function (dispatch) {
        dispatch({type: constants.CREATE_COUPON_PENDING});
        axios.post('api/coupons', options)
            .then(response => {
                console.log("create coupon", response)
                dispatch({type: constants.CREATE_COUPON_RESOLVED, payload: response});
                if(callback){
                    callback(null, {type: constants.CREATE_COUPON_RESOLVED})
                }
            })
            .catch(error=>{
                dispatch({type: constants.CREATE_COUPON_REJECTED, payload: error})
                toastr.error("Failed to create coupon")
                if (callback){
                    callback(error, null)
                }
            })
    }
}

export function updateCoupon(_id, patch, callback) {
    return function (dispatch) {
        dispatch({type: constants.UPDATE_COUPON_PENDING});
        axios.put('api/coupons' + '/' + _id, patch)
            .then(response => {
                console.log("update coupon", response)
                dispatch({type: constants.UPDATE_COUPON_RESOLVED, payload: response});
                if(callback){
                    callback(null, response.data)
                }
            })
            .catch(error=>{
                dispatch({type: constants.UPDATE_COUPON_REJECTED, payload: error})
                toastr.error("Failed to update coupon")
                if (callback){
                    callback(error, null)
                }
            })
    }
}

export function deleteCoupon(_id, callback) {
    return function (dispatch) {
        dispatch({type: constants.DELETE_COUPON_PENDING});
        axios.delete('api/coupons' + '/' + _id)
            .then(response => {
                console.log("Delete coupon", response)
                dispatch({type: constants.DELETE_COUPON_RESOLVED, payload: response});
                if(callback){
                    callback(null, response.data)
                }
            })
            .catch(error=>{
                dispatch({type: constants.DELETE_COUPON_REJECTED, payload: error})
                toastr.error("Failed to Delete coupon")
                if (callback){
                    callback(error, null)
                }
            })
    }
}