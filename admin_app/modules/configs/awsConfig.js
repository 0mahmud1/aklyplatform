import 'aws-sdk/dist/aws-sdk';
const aws = window.AWS;

aws.config.update({
	accessKeyId: process.env.AWS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_KEY,
});

export default aws;
