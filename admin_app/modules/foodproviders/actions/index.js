import { browserHistory } from 'react-router';
import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

export function createFoodProviderAction(user, cb) {
  return function (dispatch) {
    console.log('... in action ', user);
    dispatch({ type: constants.CREATE_FOOD_PROVIDER_PENDING });
    axios.post('api/foodproviders', user)
      .then((response) => {
        dispatch({ type: constants.CREATE_FOOD_PROVIDER_RESOLVED });
        if (cb) {
          cb(null, { type: constants.CREATE_FOOD_PROVIDER_RESOLVED });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.CREATE_FOOD_PROVIDER_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function getFoodProviders(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: constants.GET_FOOD_PROVIDER_PENDING });
    axios.get('api/foodproviders' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get foodproviders... : ', response);
        dispatch({ type: constants.GET_FOOD_PROVIDER_RESOLVED, payload: response.data });
        //toastr.success('FoodProviders loaded successfully!!');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_FOOD_PROVIDER_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}

export function getFilteredFoodProviders(options) {
    return function (dispatch) {
        let params = {};
        if (options['name']) {
            params['name'] = options['name']
        }
        if (options['email']) {
            params['email'] = options['email']
        }
        dispatch({ type: constants.GET_FOOD_PROVIDER_PENDING });
        axios.get('api/foodproviders',{
          params:params
        }).then((response) => {
                console.log('get filtered foodproviders... : ', response);
                dispatch({ type: constants.GET_FOOD_PROVIDER_RESOLVED, payload: response.data });
                //toastr.success('FoodProviders loaded successfully!!');
            })
            .catch((err) => {
                dispatch({ type: constants.GET_FOOD_PROVIDER_REJECTED, payload: err });
                toastr.error(err);
            });
    };
}

export function readFoodProvider(_id) {
  return function (dispatch) {
    dispatch({ type: constants.READ_FOOD_PROVIDER_PENDING });
    console.log('api params: ', _id);
    axios.get('api/foodproviders/' + _id)
      .then((response) => {
        console.log('get user... : ', response);
        dispatch({ type: constants.READ_FOOD_PROVIDER_RESOLVED, payload: response });
        console.log('Read a FoodProvider-Action: ', response);
        //toastr.success('FoodProvider found successfully!!');
      })
      .catch((err) => {
        dispatch({ type: constants.READ_FOOD_PROVIDER_REJECTED, payload: err });
        console.warn('Read a FoodProvider-Action: ', err);
        toastr.error("Food provider not found");
      });
  };
}

export function updateFoodProvider(_id, patch, cb) {
  return function (dispatch) {
    dispatch({ type: constants.UPDATE_FOOD_PROVIDER_PENDING });
    console.log('PATCH: ', patch, _id);
    axios.put('api/foodproviders/' + _id, patch)
      .then((response) => {
        console.log('UPDATE user... : ', response);
        dispatch({ type: constants.UPDATE_FOOD_PROVIDER_RESOLVED, payload: response });
        if (cb) {
          cb(null, { type: constants.UPDATE_FOOD_PROVIDER_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.UPDATE_FOOD_PROVIDER_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function deleteFoodProvider(_id, cb) {
  return function (dispatch) {
    axios.delete('api/foodproviders/' + _id)
      .then((response) => {
        console.log('get user... : ', response);
        dispatch({ type: constants.DELETE_FOOD_PROVIDER_RESOLVED, payload: response });
        console.log('Read a FoodProvider-Action: ', response);
        if (cb) {
          cb(null, { type: constants.DELETE_FOOD_PROVIDER_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.DELETE_FOOD_PROVIDER_REJECTED, payload: err });
        console.warn('Read a FoodProvider-Action: ', err);
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function getUnAssociatedDBoys() {
  return function (dispatch) {
    axios.get('api/dboys', {
      params: {
        isAssociated: false,
      },
    })
    .then((response) => {
      console.group('getUnAssociatedDBoys');
      console.log(response.data.data);
      console.groupEnd();
      dispatch({ type: constants.GET_DBOYS_FP_RESOLVED, payload: response.data.data });
      //toastr.success('Delivery Boys List Loaded');
    })
    .catch((err) => {
      dispatch({ type: 'GET_VANS_FP_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getUnAssociatedVans() {
  return function (dispatch) {
    axios.get('api/vans', {
      params: {
        isAssociated: false,
      },
    })
    .then((response) => {
      console.group('getUnAssociatedVans');
      console.log(response.data.data);
      console.groupEnd();
      dispatch({ type: constants.GET_VANS_FP_RESOLVED, payload: response.data.data });
      //toastr.success('Van List Loaded');
    })
    .catch((err) => {
      dispatch({ type: 'GET_VANS_FP_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}
