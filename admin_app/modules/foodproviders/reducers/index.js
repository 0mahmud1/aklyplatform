import * as constants from '../constants';

const defaultState = {
  //foodProviderList: [],
  //foodProvider: {},
  //vans: []
};

const foodProviderReducer = (state = defaultState, action) => {
  switch (action.type) {
    case constants.CREATE_FOOD_PROVIDER_PENDING :
      return Object.assign({}, state, {
        //isRequestRunning: true,
        //requestCompleted: false,
        //requestFailed: false
      });

    case constants.CREATE_FOOD_PROVIDER_RESOLVED :
      return Object.assign({}, state, {
        //isRequestRunning: false,
        //requestCompleted: true,
        //requestFailed: false,
      });

    case constants.CREATE_FOOD_PROVIDER_REJECTED :
      return Object.assign({}, state, {
        //isRequestRunning: false,
        //requestCompleted: true,
        //requestFailed: true,
        error: action.payload.error
      });

    case constants.GET_FOOD_PROVIDER_PENDING:
      return Object.assign({}, state, {
        //getFoodProvidersPending: true,
        //getFoodProvidersCompleted: false,
        //getFoodProvidersFailed: false
      });

    case constants.GET_FOOD_PROVIDER_RESOLVED:
      return Object.assign({}, state, {
        //getFoodProvidersPending: false,
        //getFoodProvidersCompleted: true,
        //getFoodProvidersFailed: false,
        foodProviderList: action.payload.data,
        total: action.payload.total,
        totalPage: action.payload.limit == 0 ? 1 : Math.ceil(action.payload.total / action.payload.limit)
      });

    case constants.GET_FOOD_PROVIDER_REJECTED:
      return Object.assign({}, state, {
        //getFoodProvidersPending: false,
        //getFoodProvidersCompleted: false,
        //getFoodProvidersFailed: true
      });


    case constants.READ_FOOD_PROVIDER_PENDING:
      return Object.assign({}, state, {
        //readFoodProviderPending: true,
        //readFoodProviderCompleted: false,
        //readFoodProviderFailed: false,
      });

    case constants.READ_FOOD_PROVIDER_RESOLVED:
      return Object.assign({}, state, {
        //readFoodProviderPending: false,
        //readFoodProviderCompleted: true,
        //readFoodProviderFailed: false,
        foodProvider: action.payload.data.data,
        //clonedFoodProvider: action.payload.data.data
      });

    case constants.READ_FOOD_PROVIDER_REJECTED:
      return Object.assign({}, state, {
        //readFoodProviderPending: false,
        //readFoodProviderCompleted: false,
        //readFoodProviderFailed: true
      });

    // case constants.FOOD_PROVIDER_SELECT:
    // 	return Object.assign({}, state, {vendorSelected: action.payload})

    case constants.FOOD_PROVIDER_MARKER:
      return Object.assign({}, state, {vendorMarker: action.payload});

    case constants.GET_VANS_FP_RESOLVED:
      return Object.assign({}, state, {vans: action.payload});

    case constants.GET_DBOYS_FP_RESOLVED:
      return Object.assign({}, state, {deliveryBoys: action.payload});

    default :
      return state;
  }
};

export default foodProviderReducer