'use strict'

import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import toastr from 'toastr'

import {FP_URL} from '../constants';
import {readFoodProvider, deleteFoodProvider, updateFoodProvider, getUnAssociatedVans} from '../actions/index'
import FoodProviderModifyComponent from '../components/modify'

class FoodProviderModifyContainer extends Component {

  componentWillMount() {
    this.props.getVans();
    //this.props.getDBoys();
    this.props.readFoodProvider(this.props.params._id);
  }


  componentDidUpdate(prevProps, prevStates) {
  }


  deleteFoodProvider() {
    this.props.deleteFoodProvider(this.props.params._id, function (err, data) {
      if (data) {
        browserHistory.push(FP_URL);
        toastr.success('FoodProvider deleted successfully!!');
      }
      if (err) {
        toastr.error(err);
      }
    });
  }

  updateFoodProvider(userId, patch) {
    console.log(userId, this);
    this.props.updateFoodProvider(userId, patch, function (err, data) {
      if (data) {
        browserHistory.push(FP_URL);
        toastr.success('FoodProvider updated successfully!!');
      }
      if (err) {
        toastr.error(err);
      }
    });
  }

  loadingUI() {
    return (
      <div>
        <h1>Loading.. </h1>
      </div>
    )
  }

  render() {
    let shouldFoodProviderRender = (this.props.foodProvider && this.props.params._id === this.props.foodProvider._id && this.props.googleLoaded && this.props.vans);
    return (
      shouldFoodProviderRender ?
        <FoodProviderModifyComponent
          foodProvider={this.props.foodProvider}
          newPosition={this.props.newPosition}
          vans={this.props.vans}
          saveMarkerPosition={this.props.saveMarkerPosition.bind(this)}
          updateFoodProvider={this.updateFoodProvider.bind(this)}
          deleteFoodProvider={this.deleteFoodProvider.bind(this)}/>
        : this.loadingUI()
    )

  }
}


function mapStateToProps(store) {
  return {
    googleLoaded: store.auth.googleLoaded,
    foodProvider: store.foodProviders.foodProvider,
    newPosition: store.foodProviders.vendorMarker,
    vans: store.foodProviders.vans,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    readFoodProvider: (_id) => dispatch(readFoodProvider(_id)),
    updateFoodProvider: (_id, patch, cb) => dispatch(updateFoodProvider(_id, patch, cb)),
    deleteFoodProvider: (_id, cb) => dispatch(deleteFoodProvider(_id, cb)),
    saveMarkerPosition: (position) => dispatch({type: 'FOOD_PROVIDER_MARKER', payload: position}),
    getVans: () => dispatch(getUnAssociatedVans())
  }
};

// We don't want to return the plain FoodProviderListContainer (component) anymore,
// we want to return the smart Container
//  > FoodProviderListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(FoodProviderModifyContainer)
