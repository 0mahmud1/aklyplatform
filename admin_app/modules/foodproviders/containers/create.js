'use strict';

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import toastr from 'toastr'

import { FP_URL } from '../constants';
import { createFoodProviderAction } from '../actions/index'
import { getUnAssociatedVans } from '../actions'
import FoodProviderCreateComponent from '../components/create'

class FoodProviderCreateContainer extends Component {

  componentDidMount() {
    this.props.getVans();
  }

  render() {
    return (
      this.props.googleLoaded ?
      <FoodProviderCreateComponent
        createFoodProvider={this.createFoodProvider.bind(this)}
        foodProviders={this.props.foodProviders}
        saveMarkerPosition={this.props.saveMarkerPosition}
      /> : null
    )
  }

  createFoodProvider(user) {
    this.props.createFoodProvider(user, function (err, data) {
      if (data) {
        browserHistory.push(FP_URL);
        toastr.success('FoodProvider created successfully');
      }
      if (err) {
        toastr.error(err);
      }
    });
  }
}

FoodProviderCreateContainer.propTypes = {
  createFoodProvider: PropTypes.func.isRequired
};

function mapStateToProps(store) {
  return {
    googleLoaded: store.auth.googleLoaded,
    foodProviders: store.foodProviders
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createFoodProvider: (user, cb) => dispatch(createFoodProviderAction(user, cb)),
    vendorSelect: (value) => dispatch({type: 'FOOD_PROVIDER_SELECT', payload: value}),
    saveMarkerPosition: (position) => dispatch({type: 'FOOD_PROVIDER_MARKER', payload: position}),
    getVans: () => dispatch(getUnAssociatedVans()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FoodProviderCreateContainer)
