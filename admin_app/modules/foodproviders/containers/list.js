'use strict';

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getFoodProviders, getFilteredFoodProviders } from '../actions/index'
import FoodProviderListComponent from '../components/list'

class FoodProviderListContainer extends Component {
	componentWillMount () {
		this.props.getFoodProviders(sessionStorage.getItem('foodProviderDataLimit'),sessionStorage.getItem('foodProviderCurrentPage'));
	}

	render () {
		let shouldLoad = this.props.foodProviders;
		return (
			shouldLoad ?
			<FoodProviderListComponent
			getFoodProviders={this.props.getFoodProviders}
			getFilteredFoodProviders={this.props.getFilteredFoodProviders}
			totalPage={this.props.totalPage}
			total={this.props.total}
			foodProviders={this.props.foodProviders} /> : null
		)
	}
}

/*FoodProviderListContainer.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	getFoodProviders: PropTypes.func.isRequired,
	foodProviders: PropTypes.array.isRequired
}*/

// Get apps store and pass it as props to FoodProviderListContainer
//  > whenever store changes, the FoodProviderListContainer will automatically re-render
function mapStateToProps (store) {
	return {
		foodProviders: store.foodProviders.foodProviderList,
		totalPage: store.foodProviders.totalPage,
        total: store.foodProviders.total
    }
}

// Get actions and pass them as props to to FoodProviderListContainer
//  > now FoodProviderListContainer has this.props.getFoodProviders
function matchDispatchToProps (dispatch) {
	return bindActionCreators({
		getFoodProviders: getFoodProviders,
        getFilteredFoodProviders: getFilteredFoodProviders
	}, dispatch)
}

// We don't want to return the plain FoodProviderListContainer (component) anymore,
// we want to return the smart Container
//  > FoodProviderListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(FoodProviderListContainer)
