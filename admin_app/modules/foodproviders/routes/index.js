'use strict';

import React from 'react'
import { Route, IndexRoute } from 'react-router'
import { FP_URL_NAME } from '../constants';

import FoodProviderLayout from '../layouts'
import FoodProviderCreateContainer from '../containers/create'
import FoodProviderListContainer from '../containers/list';
import FoodProviderModifyContainer from '../containers/modify';
import * as AuthService from '../../services/auth';

export default function () {
	return (
		<Route path={FP_URL_NAME} component={FoodProviderLayout} onEnter={AuthService.isDashAuthRouter}>
			<IndexRoute component={FoodProviderListContainer} />
			<Route path='create' component={FoodProviderCreateContainer} />
			<Route path=':_id' component={FoodProviderModifyContainer} />
		</Route>
	)
}
