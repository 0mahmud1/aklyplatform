'use strict';

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import toastr from 'toastr'
import 'select2'
import _ from 'lodash'
import $ from 'jquery'

import {FP_URL} from '../constants';

class FoodProviderCreateComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onDemandChecked: true,
      subscriptionChecked: false,
      both: false
    };
  }

  componentDidMount() {
    this.initializeMap()
  }

  componentDidUpdate(PrevProps, prevStates) {
    console.log('state change');

    let reactDOM = this;
    if (this.state.subscriptionChecked && !PrevProps.subscriptionChecked) {
      $('#van-tags').select2();
    }

    //$eventSelect.on("select2:open", function (e) { log("select2:open", e); });
  }

  initializeMap() {
    let reactDOM = this;
    let map = new google.maps.Map(document.getElementById('vendor-map'), {
      center: { lat: 25.2854473, lng: 51.53103979999992 },
      zoom: 12
    });

    let marker = new google.maps.Marker({
      position: { lat: 25.2854473, lng: 51.53103979999992 },
      map: map,
      draggable:true,
    });

    reactDOM.props.saveMarkerPosition([51.53103979999992, 25.2854473]);

    console.log('map called');
    let input = document.getElementById('search-place-vendor');
    let searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    searchBox.addListener('places_changed', function () {
      marker.setMap(null);
      let place = searchBox.getPlaces()[0]; //WE ARE ONLY CONCERN ABOUT THE FIRST ELEMENT
      console.log('select primary area   ', place);

      if (!place.geometry) {
        return;
      }

      map.setCenter(place.geometry.location);

      marker = new google.maps.Marker({
        map: map,
        title: place.name,
        draggable: true,
        position: place.geometry.location
      });
      console.log(place.geometry.location.lng(), place.geometry.location.lat());

      reactDOM.props.saveMarkerPosition([place.geometry.location.lng(), place.geometry.location.lat()]);
      google.maps.event.addListener(marker, 'dragend', function (evt) {
        console.log('marker dragged ', evt.latLng.lat(), evt.latLng.lng());
        reactDOM.props.saveMarkerPosition([evt.latLng.lng(), evt.latLng.lat()]);
      });
    });
    google.maps.event.addListener(marker, 'dragend', function (evt) {
      console.log('marker dragged ', evt.latLng.lat(), evt.latLng.lng());
      reactDOM.props.saveMarkerPosition([evt.latLng.lng(), evt.latLng.lat()]);
    });
  }


  generateVanTags() {
    return (
      this.props.foodProviders.vans && this.props.foodProviders.vans.length > 0 ?
        this.props.foodProviders.vans.map((van) => {
          return <option key={van._id} value={JSON.stringify(van)}>{van.name}</option>
        }) : null
    )
  }

  render() {
    //let mapShow = (this.props.users.vendorSelected) ? 'block' : 'none';
    return (
      <div>
        <div className="mp_heading_mb">
          <div className="row">
            <div className="col-lg-12">
              <div className="mp_heading">
                <h3>Create Food Provider</h3>
              </div>
            </div>
          </div>
        </div>

        <div className="mp_body">
          <div className='row'>
            <div className='col-lg-12'>
              <div className="user_create">
                <div onSubmit={this.handleSubmit.bind(this)}>
                  <div className="row" style={{marginBottom: '25px'}}>

                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <div className="geninput">
                        <div className='form-group'>
                          <label className="geninput_label" htmlFor='Name'>Name</label>
                          <div className="input-group geninput_group geninput_group_add">
                            <input
                              type='text'
                              className='form-control geninput_formctrl'
                              ref='name'
                              placeholder='Name'/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <div className="geninput">
                        <div className='form-group'>
                          <label className="geninput_label" htmlFor='email'>Email</label>
                          <div className="input-group geninput_group geninput_group_add">
                            <input
                              type='email'
                              className='form-control geninput_formctrl'
                              ref='email'
                              placeholder='something@real.com'/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <div className="geninput">
                        <div className='form-group'>
                          <label className="geninput_label" htmlFor='password'>Password</label>
                          <div className="input-group geninput_group geninput_group_add">
                            <input
                              type='password'
                              className='form-control geninput_formctrl'
                              ref='password'
                              placeholder='super secret !'/>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

                    <div className='row' style={{marginBottom: '25px'}}>
                      <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div className="geninput">
                          <div className='form-group' onChange={this.handleChange.bind(this)}>
                            <label className="geninput_label" htmlFor='on-demand-capacity' style={{display: 'block'}}>Food Provider Type</label>
                            <div style={{display: 'inline-block', marginRight: '15px'}}>
                              <input type="radio" name="type" defaultChecked={true} value="onDemand"/> OnDemand
                            </div>

                            <div style={{display: 'inline-block', marginRight: '15px'}}>
                              <input type="radio" name="type" value="subscription"/> Subscription
                            </div>
                            <div style={{display: 'inline-block'}}>
                                <input type="radio" name="type" value="both"/> Both
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {
                      this.state.subscriptionChecked ?
                        <div className='row' style={{marginBottom: '25px'}}>
                          <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div className="geninput">
                              <div className='form-group'>
                                <label className="geninput_label" htmlFor='subscription-capacity'>Subscription
                                  Capacity</label>
                                <div className="input-group geninput_group geninput_group_add">
                                  <input
                                    type='number'
                                    className='form-control geninput_formctrl'
                                    ref='subscriptionCapacity'
                                    defaultValue='50'
                                    placeholder='Subscription Capacity'/>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div className="geninput">
                              <div className='form-group'>
                                <label className="geninput_label" htmlFor='vans'>Vans</label>
                                <div className="input-group geninput_group geninput_group_add">
                                  <select id="van-tags" multiple="multiple" className="form-control">
                                    {this.generateVanTags()}
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        : null
                    }

                  <div className="user_create_vendor">
                    <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div id='vendor-map' className="gmap_wrapper" style={{height: '500px'}}></div>
                        <input id='search-place-vendor' className='controls gmap_search' type='text'
                               placeholder='Search Box'
                        />
                      </div>
                    </div>
                  </div>

                  <div className="user_create_actionbtn actionbutn">
                    <div className="row">
                      <div className="col-md-12 col-lg-12">
                        <div className="actionbtn_save">
                          <button onClick={this.handleSubmit.bind(this)} type="submit" className="btn butn butn_default">
                            Create
                          </button>
                        </div>
                        <div className="actionbtn_back">
                          <button type="button" className="btn butn butn_back"
                                  onClick={() => browserHistory.push(FP_URL)}>Back
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    )
  }

  handleSubmit(event) {
    event.preventDefault();
    console.log('hello world');

    let self = this;
    let vanSelector = $('#van-tags');
    console.log(ReactDOM.findDOMNode(this.refs.name).value.trim());
    let user = {
      name: ReactDOM.findDOMNode(this.refs.name).value.trim(),
      email: ReactDOM.findDOMNode(this.refs.email).value.trim(),
      password: ReactDOM.findDOMNode(this.refs.password).value.trim(),
      address: {
        geoLocation: {coordinates: [self.props.foodProviders.vendorMarker]}
      },
      locations: {
        coordinates: self.props.foodProviders.vendorMarker
      },
      user_metadata: {
        roles: ['foodProvider']
      },
    };

    // console.log(ReactDOM.findDOMNode(this.refs.subscriptionCapacity).value)
    // console.log(ReactDOM.findDOMNode(this.refs.onDemandCapacity).value)

    if (this.state.subscriptionChecked && !this.state.onDemandChecked) {
      Object.assign(user, user, {
        subscriptionCapacity: ReactDOM.findDOMNode(this.refs.subscriptionCapacity).value,
        vans: vanSelector.val().map(van => JSON.parse(van)),
        providerType: ["subscription"]
      })
    } else if (!this.state.subscriptionChecked && this.state.onDemandChecked) {
      Object.assign(user, user, {
        // onDemandCapacity: ReactDOM.findDOMNode(this.refs.onDemandCapacity).value,
        providerType: ["onDemand"]
      })
    } else if (this.state.subscriptionChecked && this.state.onDemandChecked) {
      Object.assign(user, user, {
        subscriptionCapacity: ReactDOM.findDOMNode(this.refs.subscriptionCapacity).value,
        vans: vanSelector.val().map(van => JSON.parse(van)),
        providerType: ["onDemand", "subscription"]
      })
    }
    let isReturn = false;
    if (user.name.length <= 0) {
      toastr.warning('Name is required');
      isReturn = true;
      // return;
    }
    if (user.email.length <= 0) {
      toastr.warning('email is required');
      isReturn = true;
      // return
    }
    if (user.password.length <= 0) {
      toastr.warning('Password is required');
      isReturn = true;
      // return
    }
    if (this.state.subscriptionChecked) {
      if (user.vans == undefined) {
        toastr.warning('Must associate van with food provider');
        isReturn = true;
        // return
      }
    }

    if (isReturn) {
      return;
    }

    // call create action
    this.props.createFoodProvider(user);
  }

  handleChange(event) {
    // todo : Subscription and On-Demand selection
    // event.preventDefault();
    console.log(event.target.value);

    if (event.target.value == "onDemand") {
      this.setState({
        onDemandChecked: true,
        subscriptionChecked: false,
      })
    }

    else if (event.target.value == "subscription") {
      console.log('...');
      this.setState({
        onDemandChecked: false,
        subscriptionChecked: true,
      })
    }

    else if (event.target.value == "both") {
      this.setState({
        onDemandChecked: true,
        subscriptionChecked: true
      })
    }

    // this.setState({
    //   onDemandChecked: !this.state.onDemandChecked,
    //   subscriptionChecked: !this.state.subscriptionChecked
    // })
  }
}

FoodProviderCreateComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  createFoodProvider: PropTypes.func.isRequired
};

export default FoodProviderCreateComponent

function getdBoyObjectfromId(list, ids) {
  //console.log('function params ', list, ids);
  let deliveryBoys = [], temp;
  for (var i = 0; i < ids.length; i++) {
    temp = _.find(list, function (dBoy) {
      return dBoy._id === ids[i];
    });
    //console.log('temp ', temp);

    if (temp)
      deliveryBoys.push(temp)
  }

  console.log('all deliveryBoys from selection ', deliveryBoys);

  return deliveryBoys;
}

function getVanObjectfromId(list, ids) {
  //console.log('function params ', list, ids);
  let vans = [], temp;
  for (let i = 0; i < ids.length; i++) {
    temp = _.find(list, function (van) {
      return van._id === ids[i];
    });
    //console.log('temp ', temp);

    if (temp) {
      vans.push(temp)
    }
  }

  console.log('all vans from selection ', vans);

  return vans;
}
