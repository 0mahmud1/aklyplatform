'use strict';

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import moment from 'moment'
import toastr from 'toastr'
import _ from 'lodash';

import {FP_URL} from '../constants';
class FoodProviderModifyComponent extends Component {
  constructor(props) {
    super(props);
    let providerType = this.props.foodProvider.providerType;

    this.state = {
      onDemandChecked: providerType.includes('onDemand'),
      subscriptionChecked: providerType.includes('subscription')
    };
  }

  componentDidMount() {
    if (this.state.onDemandChecked) {
      $('#dboy-tags').select2();
    }
    if (this.state.subscriptionChecked) {
      $('#van-tags').select2();
    }
    this.initializeMap()
  }

  initializeMap() {
    console.log('map required update');

    let reactDOM = this,
      marker,
      user = this.props.foodProvider;
    let map = new google.maps.Map(document.getElementById('vendor-map-edit'), {
      center: this.convertGeoArrayToObject(user.locations.coordinates),
      zoom: 18
    });
    console.log('map called');
    let input = document.getElementById('search-place-vendor-edit');
    let searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    marker = new google.maps.Marker({
      map: map,
      title: 'vendor position',
      draggable: true,
      position: this.convertGeoArrayToObject(user.locations.coordinates)
    });

    searchBox.addListener('places_changed', function () {
      let place = searchBox.getPlaces()[0]; //WE ARE ONLY CONCERN ABOUT THE FIRST ELEMENT
      console.log('select primary area   ', place);

      if (!place.geometry) {
        return;
      }
      console.log('locations------>>>>>>>>', place.geometry.locations)
      console.log('location------>>>>>>>>', place.geometry.location)
      map.setCenter(place.geometry.location);
      marker.setPosition(place.geometry.location);

    });

    google.maps.event.addListener(marker, 'dragend', function (evt) {
      console.log('marker dragged ', evt.latLng.lat(), evt.latLng.lng());
      reactDOM.props.saveMarkerPosition([evt.latLng.lng(), evt.latLng.lat()]);
    });
  }

  convertGeoArrayToObject(point) {
    return {lat: point[1], lng: point[0]}
  }

  vanList() {
    const vanList = _.uniqBy(_.union(this.props.vans, this.props.foodProvider.vans), '_id');
    console.group('vanList');
    console.log(vanList);
    console.groupEnd();
    return (
      vanList && vanList.length > 0 ?
        vanList.map((van) => {
          return <option key={van._id} value={van._id}>{van.name}</option>
        }) : null
    )
  }

  selectedVans() {
    return (
      this.props.foodProvider.vans && this.props.foodProvider.vans.length > 0 ?
        this.props.foodProvider.vans.map((van) => {
          return van._id;
        }) : null
    )
  }

  render() {

    return (
      <div>
        <div className="mp_heading_mb">
          <div className="row">
            <div className="col-lg-12">
              <div className="mp_heading">
                <h3>Edit Food Provider</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="mp_body">
          <div className="row">
            <div className="col-lg-12">
              <div className="user_edit">
                { this.props.foodProvider
                  ? <form onSubmit={this.handleSubmit.bind(this)}>
                  <div className="row">
                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4 db-mb">
                      <div className="geninput">
                        <div className='form-group'>
                          <label className="geninput_label" htmlFor='Name'>Name</label>
                          <div className="geninput_group">
                            <input
                              type='text'
                              className='form-control geninput_formctrl'
                              ref='name'
                              placeholder='Name'
                              defaultValue={this.props.foodProvider.name}/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-4 db-mb">
                      <div className="geninput">
                        <div className='form-group'>
                          <label className="geninput_label" htmlFor='Email'>Email</label>
                          <div className="geninput_group">
                            <input
                              type='text'
                              className='form-control geninput_formctrl'
                              readOnly='true'
                              ref='email'
                              defaultValue={this.props.foodProvider.email}/>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div className="fp-type">
                    <div className="row ">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="geninput">
                          <div className='form-group' onChange={this.handleChange.bind(this)}>
                            <div className="mp_heading">
                              <h3>Food Provider Type</h3>
                            </div>
                            <div className="mp_list_mgn">
                              <label className="radio-inline">
                                <input type="radio" name="type"
                                       defaultChecked={this.props.foodProvider.providerType.includes('onDemand')}
                                       value="onDemand"/> OnDemand
                              </label>
                              <label className="radio-inline">
                                <input type="radio" name="type"
                                       defaultChecked={this.props.foodProvider.providerType.includes('subscription')}
                                       value="subscription"/> Subscription
                              </label>
                              <label className="radio-inline">
                                <input type="radio" name="type"
                                       defaultChecked={this.props.foodProvider.providerType.includes('subscription') && this.props.foodProvider.providerType.includes('onDemand')}
                                       value="both"/> On Demand and Subscription
                              </label>

                            </div>
                          </div>
                        </div>

                        <div className="row">
                          {
                            this.state.subscriptionChecked ?
                              <div>
                                <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3 db-mb">
                                  <div className="geninput">
                                    <div className='form-group'>
                                      <label className="geninput_label"
                                             htmlFor='subscription-capacity'>Subscription
                                        Capacity</label>
                                      <div
                                        className="input-group geninput_group geninput_group_add">
                                        <input
                                          type='number'
                                          className='form-control geninput_formctrl'
                                          ref='subscriptionCapacity'
                                          defaultValue={this.props.foodProvider.subscriptionCapacity}
                                          placeholder='Subscription Capacity'/>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3 db-mb">
                                  <div className="geninput">
                                    <div className='form-group'>
                                      <label className="geninput_label"
                                             htmlFor='email'>Vans</label>
                                      <div
                                        className="input-group geninput_group geninput_group_add">
                                        <select id="van-tags" multiple={true}
                                                defaultValue={this.selectedVans()}
                                                className="form-control">
                                          {this.vanList()}
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                              : null
                          }
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="user_role">
                    <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="user_create_vendor">
                          <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <div id='vendor-map-edit' className="gmap_wrapper"></div>
                              <input id='search-place-vendor-edit'
                                     className='controls gmap_search' type='text'
                                     placeholder='Search Box'
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="user_edit_actionbtn actionbutn">
                    <div className="row">
                      <div className="col-md-12 col-lg-12">
                        <div className="actionbtn_save">
                          <button type="submit" className="btn butn butn_default">
                            Update
                          </button>
                        </div>
                        <div className="actionbtn_delete">
                          <button type="button" className="btn butn butn_danger"
                                  data-toggle='modal'
                                  data-target='#userDeleteConfirm'>Delete
                          </button>
                        </div>
                        <div className="actionbtn_back">
                          <button type="button" className="btn butn butn_back"
                                  onClick={() => browserHistory.push(FP_URL)}>Back
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                  : <p><b><i>Loading.......</i></b></p>
                }
              </div>
            </div>
          </div>
        </div>

        {/* FoodProvider delete confirmation modal */}
        <div
          id='userDeleteConfirm'
          className='modal fade delprompt'
          tabIndex='-1'
          role='dialog'>
          <div className='modal-dialog delprompt_dialog' role='document'>
            <div className='modal-content delprompt_content'>
              <div className="modal-body">
                <div className="delprompt_text">
                  <p>Do you want to delete this user?</p>
                </div>
              </div>
              <div className='modal-footer'>
                <button type="button" className="btn butn butn_danger"
                        onClick={this.deleteFoodProvider.bind(this)}>
                  Delete
                </button>
                <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault();
    const _this = this;
    const vansID = $('#van-tags').val();
    let address = {};

    const vanList = _.uniqBy(_.union(this.props.vans, this.props.foodProvider.vans), '_id');
    const selectedVans = _.filter(vanList, function (data) {
      return _.includes(vansID, data._id);
    });

    let name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    address = {geoLocation: {coordinates: [_this.props.newPosition]}};

    console.log(_this.props.newPosition)
    let locations = {coordinates: _this.props.newPosition}
    console.log(locations)
    // create a object
    let options = {name, address, vans: selectedVans, locations: locations};

    if(!_this.props.newPosition)
      delete options.locations

    if (this.state.subscriptionChecked && !this.state.onDemandChecked) {
      Object.assign(options, options, {
        subscriptionCapacity: ReactDOM.findDOMNode(this.refs.subscriptionCapacity).value,
        vans: selectedVans,
        providerType: ["subscription"]
      })
    }
    else if (!this.state.subscriptionChecked && this.state.onDemandChecked) {
      Object.assign(options, options, {
        providerType: ["onDemand"]
      })
    }
    else if (this.state.subscriptionChecked && this.state.onDemandChecked) {
      Object.assign(options, options, {
        subscriptionCapacity: ReactDOM.findDOMNode(this.refs.subscriptionCapacity).value,
        vans: selectedVans,
        providerType: ["onDemand", "subscription"]
      })
    }

    console.log('options .... ', options)
    console.log('goes through validations');

    // validations
    if (name.length < 1) {
      toastr.warning('Name is required');
      return false
    }

    if (this.state.subscriptionChecked) {
      if (options.vans === undefined || options.vans === null || options.vans === []) {
        toastr.warning('Must associate van with food provider')
        return false
      }
    }

    // call update action
    _this.props.updateFoodProvider(_this.props.foodProvider._id, options)
  }

  // vans: getVanObjectfromId(this.props.foodProviders.vans, vanSelector.val()),

  handleChange(event) {
    // todo : Subscription and On-Demand selection
    // event.preventDefault();
    console.log('handle change')
    console.log(event.target.value);

    if (event.target.value == "onDemand") {
      this.setState({
        onDemandChecked: true,
        subscriptionChecked: false,
      })
    }

    else if (event.target.value == "subscription") {
      this.setState({
        onDemandChecked: false,
        subscriptionChecked: true,
      })
    }

    else if (event.target.value == "both") {
      this.setState({
        onDemandChecked: true,
        subscriptionChecked: true
      })
    }

    // this.setState({
    //   onDemandChecked: !this.state.onDemandChecked,
    //   subscriptionChecked: !this.state.subscriptionChecked
    // })
  }

  // handleSelection(event) {
  //     event.preventDefault();
  //     console.log(event.target.value);
  //     //this.refs.role.value = event.target.value;
  // }

  deleteFoodProvider() {
    this.props.deleteFoodProvider();
    $('#userDeleteConfirm').modal('hide');
  }
}

// function getDBoyObjectfromId(list, ids) {
//     //console.log('function params ', list, ids);
//     let deliveryBoys = [], temp;
//     for(var i=0; i< ids.length; i++) {
//         temp = _.find(list, function (dBoy) {
//             return dBoy._id === ids[i];
//         });
//         //console.log('temp ', temp);
//
//         if(temp)
//             deliveryBoys.push(temp)
//     }
//
//     console.log('all deliveryBoys from selection ', deliveryBoys);
//
//     return deliveryBoys;
// }
//
// function getVanObjectfromId(list, ids) {
//     //console.log('function params ', list, ids);
//     let vans = [], temp;
//     for(var i=0; i< ids.length; i++) {
//         temp = _.find(list, function (van) {
//             return van._id === ids[i];
//         });
//         //console.log('temp ', temp);
//
//         if(temp)
//             vans.push(temp)
//     }
//
//     console.log('all vans from selection ', vans);
//
//     return vans;
// }

FoodProviderModifyComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  foodProvider: PropTypes.object,
  deleteFoodProvider: PropTypes.func.isRequired,
  //updateFoodProvider: PropTypes.func.isRequired
};

export default FoodProviderModifyComponent
