'use strict'

import * as constants from '../constants'

/*
 * All reducers get two parameters passed in, state and action that occurred
 * state isn't entire apps state, only the part of state that this reducer is responsible for
 * "state = null" is set so that we don't throw an error when app first boots up
 *
 * state = null is changed to defaultState
 * This reducer will always return an array of tags no matter what
 * You need to return something, so if there are no tags then just return an empty array
 */

const defaultState = {
    tagList: [],
    tag: {}
};

const tagsReducer = (state = defaultState, action) => {
    switch (action.type) {

        case 'TAG_READ':
            return action.payload.data;

        case constants.TAG_CREATE_PENDING:
            return Object.assign({}, state, {});
        case constants.TAG_CREATE_RESOLVED:
            return Object.assign({}, state, {tag: action.payload.data.data});
        case constants.TAG_CREATE_REJECTED:
            return Object.assign({}, state, {tag: action.payload.data.data});

        case constants.TAG_READ_PENDING:
            return Object.assign({}, state, {});
        case constants.TAG_READ_RESOLVED:
            return Object.assign({}, state, {tag: action.payload.data.data});
        case constants.TAG_READ_REJECTED:
            return Object.assign({}, state, {tag: action.payload.data.data});

        case constants.TAG_UPDATE_PENDING:
            return Object.assign({}, state, {});
        case constants.TAG_UPDATE_RESOLVED:
            return Object.assign({}, state, {tag: action.payload.data.data});
        case constants.TAG_UPDATE_REJECTED:
            return Object.assign({}, state, {tag: action.payload.data.data});

        case constants.TAG_DELETE_PENDING:
            return Object.assign({}, state, {});
        case constants.TAG_DELETE_RESOLVED:
            return Object.assign({}, state, {tag: action.payload.data.data});
        case constants.TAG_DELETE_REJECTED:
            return Object.assign({}, state, {tag: action.payload.data.data});

        case constants.TAGS_LIST_PENDING:
            return Object.assign({}, state, {});
        case constants.TAGS_LIST_RESOLVED:
            return Object.assign({}, state, 
            {
                total: action.payload.data.total,
                tagList: action.payload.data.data,
                totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit)
            });
        case constants.TAGS_LIST_REJECTED:
            return Object.assign({}, state, {tagList: action.payload.data.data});

        // case 'TAG_CREATE_RESOLVED':
        //     let newList = state.tagList.concat(action.payload);
        //     return Object.assign({}, state, {
        //       tagList: newList
        //     });
        // case 'TAG_CREATE_REJECTED':
        // 	return;

        // case 'TAG_DELETE_RESOLVED':
        //     let filteredList = _.filter(state.tagList, (tag) => {
        //       return tag._id !== action.payload._id;
        //     });
        //
        //     return Object.assign({}, state, {
        //       tagList: filteredList
        //     });

        // case 'TAG_DELETE_REJECTED':

        default:
            return state;
    }
};

export default tagsReducer
