'use strict'

import _ from 'lodash';

/*
 * This reducer will always return an array of tags no matter what
 * You need to return something, so if there are no tags then just return an empty array
 */

const defaultState = {
	list: [],
	tag: {}
}
const tagsReducer = (state = defaultState, action) => {
	switch (action.type) {
		case 'TAG_CREATE_RESOLVED':
			let newList = state.list.concat(action.payload);
			return Object.assign({}, state, {
				list: newList
			});
		// case 'TAG_CREATE_REJECTED':
		// 	return;

		case 'TAG_DELETE_RESOLVED':
			let filteredList = _.filter(state.list, (tag) => {
				return tag._id !== action.payload._id;
			});

			return Object.assign({}, state, {
				list: filteredList
			});

		// case 'TAG_DELETE_REJECTED':


		case 'TAGS_LIST_RESOLVED':
			return Object.assign({}, state, {list: action.payload});

		case 'TAGS_LIST_REJECTED':
			return state;

		case 'TAGS_LIST_PENDING':
			return state;
		
		default:
			return state;
	}
}

export default tagsReducer
