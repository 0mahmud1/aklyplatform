import axios from 'axios';
import toastr from 'toastr';
import { browserHistory } from 'react-router';
import aws from '../../configs/awsConfig.js';
import async from 'async';
import path from 'path';

import * as constants from '../constants';

export function getTags(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: constants.TAGS_LIST_PENDING });
    axios.get('api/tags' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        //toastr.success('Tags fetched successfully!!');
        dispatch({ type: constants.TAGS_LIST_RESOLVED, payload: response });
      })
      .catch((err) => {
        toastr.error(err);
        dispatch({ type: constants.TAGS_LIST_REJECTED, payload: err });
      });
  };
}

export function resetTagList() {
  return function (dispatch) {
    dispatch({ type: constants.TAGS_RESET });
  };
}

export function createTag(tag) {
  return function (dispatch) {
    const activeImage = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'tags/' + new Chance().guid() + path.extname(tag.images[0].name),
      ContentType: tag.images[0].type,
      Body: tag.images[0],
      ACL: 'public-read',
    };
    const nonActiveImage = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'tags/' + new Chance().guid() + path.extname(tag.images[1].name),
      ContentType: tag.images[1].type,
      Body: tag.images[1],
      ACL: 'public-read',
    };

    dispatch({ type: constants.TAG_CREATE_PENDING });

    async.parallel([
      function (callback) {
        uploadToS3(activeImage, function (image) {
          callback(null, image);
        });
      },

      function (callback) {
        uploadToS3(nonActiveImage, function (image) {
          callback(null, image);
        });
      },
    ], function (err, results) {
      if (err) {
        console.log('error', err);
      } else {
        console.log('results', results);
        let activeImage = results[0];
        let nonActiveImage = results[1];

        delete tag.images;
        tag.images = [activeImage.Location, nonActiveImage.Location];

        axios.post('api/tags', tag)
          .then((response) => {
            console.log(response);
            dispatch({ type: constants.TAG_CREATE_RESOLVED, payload: response });
            // toastr.success('Tag created Successfully.......!');
            browserHistory.push('/tags');
          })
          .catch((err) => {
            dispatch({ type: constants.TAG_CREATE_REJECTED, payload: err });
            toastr.warning(err);
          });
      }
    });
  };
}

export function readTag(_id) {
  return function (dispatch) {
    dispatch({ type: constants.TAG_READ_PENDING });
    axios.get('api/tags/' + _id)
      .then((response) => {
        dispatch({ type: constants.TAG_READ_RESOLVED, payload: response });
        // toastr.success('Tag loaded Successfully.......!');
      })
      .catch((err) => {
        dispatch({ type: constants.TAG_READ_REJECTED, payload: err });
        toastr.error("Tag not found");
      });
  };
}

export function updateTag(tag, _id) {
  return function (dispatch) {
    const activeImage = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'tags/' + new Chance().guid() + path.extname(tag.images[0].name),
      ContentType: tag.images[0].type,
      Body: tag.images[0],
      ACL: 'public-read',
    };
    const nonActiveImage = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: 'tags/' + new Chance().guid() + path.extname(tag.images[1].name),
      ContentType: tag.images[1].type,
      Body: tag.images[1],
      ACL: 'public-read',
    };
    console.log('tag=>>',tag);
    dispatch({ type: constants.TAG_UPDATE_PENDING });

    async.parallel([
      function (callback) {
        uploadToS3(activeImage, function (image) {
          callback(null, image);
        });
      },

      function (callback) {
        uploadToS3(nonActiveImage, function (image) {
          callback(null, image);
        });
      },
    ], function (err, results) {
      if (err) {
        console.log('error', err);
      } else {
        console.log('results', results);
        let activeImage = results[0];
        let nonActiveImage = results[1];

        delete tag.images;
        tag.images = [activeImage.Location, nonActiveImage.Location];

        axios.put('api/tags/' + _id, tag)
          .then((response) => {
            dispatch({ type: constants.TAG_UPDATE_RESOLVED, payload: response });
            browserHistory.push('/tags');
            // toastr.success('Tag updated successfully.......!');
          })
          .catch((err) => {
            dispatch({ type: constants.TAG_UPDATE_REJECTED, payload: err });
            toastr.error(err);
          });
      }
    });
  };
}

export function deleteTag(_id) {
  return function (dispatch) {
    dispatch({ type: constants.TAG_DELETE_PENDING });
    axios.delete('api/tags/' + _id)
      .then((response) => {
        dispatch({ type: constants.TAG_DELETE_RESOLVED, payload: response });
        // toastr.success('tag deleted successfully');
        browserHistory.push('/tags');
      })
      .catch((err) => {
        dispatch({ type: constants.TAG_DELETE_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}

function uploadToS3(params, cb) {
  let s3 = new aws.S3();
  s3.upload(params,{queueSize: 5, partSize: 1024 * 1024 * 10})
    .on('httpUploadProgress', function (evt) {
      console.log('Uploaded :: ' + parseInt((evt.loaded * 100) / evt.total) + '%');
    }).send(function (err, data) {
      if (err) {
        // ONE NOTIFICATIONS GOES HERE
        console.log(err);
        toastr.error('Upload error');
      } else {
        cb(data);
      }
    });
}
