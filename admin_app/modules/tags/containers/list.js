'use strict'

import React, { Component, PropTypes } from 'react'
import { dispatch , bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { createTag,getTags, deleteTag, updateTag } from '../actions/index'
import TagListComponent from '../components/list'
import LoaderBox from '../../core/components/loader'

class TagListContainer extends Component {
  componentDidMount () {
    this.props.getTags(20,1)
    // this.props.getTags(sessionStorage.getItem('tagDataLimit'),sessionStorage.getItem('tagCurrentPage'))
  }

  render () {
    let shouldLoad = this.props.tags.tagList.length > 0 ? true : false;
    return (
      shouldLoad ?
      <TagListComponent
        tags={this.props.tags}
        getTags={this.props.getTags}
        createTag={this.props.createTag}
        deleteTag= {this.props.deleteTag}
        updateTag={this.props.updateTag}
        totalPage={this.props.totalPage}
        total={this.props.total}
      />: <LoaderBox />
    )
  }
}

TagListContainer.propTypes = {
  getTags: PropTypes.func.isRequired,
  tags: PropTypes.object.isRequired
}

// Get apps store and pass it as props to TagListContainer
//  > whenever store changes, the TagListContainer will automatically re-render
function mapStateToProps (store) {
  return {
    tags: store.tags,
    totalPage: store.tags.totalPage,
    total: store.tags.total
  }
}

function matchDispatchToProps (dispatch) {
  return bindActionCreators({
    createTag: createTag,
    updateTag: updateTag,
    getTags: getTags,
    deleteTag: deleteTag
  }, dispatch)
}

// const mapDispatchToProps = (dispatch, ownProps) => {
// 	return {
// 		createTag: (tag) => dispatch(createTag(tag)),
// 		updateTag: (patch,_id) => dispatch(updateTag(patch,_id)),
// 		getTags: () => dispatch(getTags()),
// 		deleteTag: (_id) => dispatch(deleteTag(_id))
// 	}
// }


export default connect(mapStateToProps, matchDispatchToProps)(TagListContainer)
