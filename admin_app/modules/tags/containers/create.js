'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { createTag } from '../actions/index'
import TagCreateComponent from '../components/create'

class TagCreateContainer extends Component {
  render () {
    return (
      <TagCreateComponent
        createTag={this.createTag.bind(this)} />
    )
  }

  createTag (tag) {
	  console.log('....');
    this.props.createTag(tag, function (err, data) {
      if(data){
        browserHistory.push('/tags');
        //toastr.success('Tag created Successfully.......!');
      }
      if(err){
        //toastr.error(err);
      }
    });
  }
}

TagCreateContainer.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  createTag: PropTypes.func.isRequired
}

// Get actions and pass them as props to to TagCreateContainer
//  > now TagCreateContainer has this.props.createTag
function matchDispatchToProps (dispatch) {
  return bindActionCreators({
    createTag: createTag
  }, dispatch)
}

// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(null, matchDispatchToProps)(TagCreateContainer)
