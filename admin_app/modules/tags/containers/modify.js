'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { readTag, updateTag, deleteTag } from '../actions/index'
import TagModifyComponent from '../components/modify'
import LoaderBox from '../../core/components/loader'


class TagModifyContainer extends Component {
  componentWillMount () {
    this.props.readTag(this.props.params._id)
  }

  render () {
    let shouldLoad = (this.props.tag && this.props.tag && this.props.params._id === this.props.tag._id)? true : false;
    return (
        shouldLoad?
      <TagModifyComponent
        tag={this.props.tag}
        updateTag={this.updateTag.bind(this)}
        deleteTag={this.deleteTag.bind(this)} /> : <LoaderBox />
    )
  }

  updateTag (tag) {
    this.props.updateTag(tag, this.props.params._id, function (err, data) {
      if(data){
        browserHistory.push('/tags');
        //toastr.success('tag updated successfully');
      }
      if(err){
        //toastr.error(err);
      }
    });

    // if (res.payload.success) {
    //   toastr.success(res.payload.message)
    //   browserHistory.push('/tags')
    // } else {
    //   toastr.warning(res.payload.responseJSON.message)
    // }
  }

  deleteTag () {
    const res = this.props.deleteTag(this.props.params._id, function (err, data) {
      if(data){
        //toastr.success('tag deleted successfully');
        browserHistory.push('/tags');
      }
      if(err){
        //toastr.error(err);
      }
    });
    // if (res.payload.success) {
    //   toastr.success(res.payload.message)
    //   browserHistory.push('/tags')
    // } else {
    //   toastr.warning(res.payload.responseJSON.message)
    // }
  }
}

TagModifyContainer.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  params: PropTypes.object.isRequired,
  tag: PropTypes.object,
  readTag: PropTypes.func.isRequired,
  updateTag: PropTypes.func.isRequired,
  deleteTag: PropTypes.func.isRequired
}

// Get apps store and pass it as props to TagModifyContainer
//  > whenever store changes, the TagModifyContainer will automatically re-render
function mapStateToProps (store) {
  return {
    tag: store.tags.tag
  }
}

// Get actions and pass them as props to to TagModifyContainer
//  > now TagModifyContainer has this.props.readTag
//  > now TagModifyContainer has this.props.updateTag
//  > now TagModifyContainer has this.props.deleteTag
function matchDispatchToProps (dispatch) {
  return bindActionCreators({
    readTag: readTag,
    updateTag: updateTag,
    deleteTag: deleteTag
  }, dispatch)
}

// We don't want to return the plain TagModifyContainer (component) anymore,
// we want to return the smart Container
//  > TagModifyContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(TagModifyContainer)
