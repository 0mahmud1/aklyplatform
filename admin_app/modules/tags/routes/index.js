'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import TagLayout from '../layouts'
import TagListContainer from '../containers/list'
import TagCreateContainer from '../containers/create'
import TagModifyContainer from '../containers/modify'
import * as AuthService from '../../services/auth';

export default function () {
  return (
    <Route path='tags' component={TagLayout} onEnter={AuthService.isDashAuthRouter}>
      <IndexRoute component={TagListContainer} />
	    <Route path='create' component={TagCreateContainer} />
        <Route path=':_id' component={TagModifyContainer} />
    </Route>
  )
}
