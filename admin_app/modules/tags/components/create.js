'use strict'

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import toastr from 'toastr'

class TagCreateComponent extends Component {
  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading'>
              <h3>Create New Dishes</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
  form() {
    return(
      <div className="mp_body">
        <div className="row">
          <div className="col-lg-12">
            <div className="meal_edit">
              <form onSubmit={this.handleSubmit.bind(this)}>
                  <div style={{marginBottom: '25px'}}>
                    <div className="geninput" style={{width: '400px'}}>
                        <div className="form-group">
                            <label className="geninput_label" htmlFor='Name'>Tag Name</label>
                            <div className="geninput_group geninput_group_add">
                                <input type="text" className="form-control geninput_formctrl" ref='name' placeholder="Name" />
                            </div>
                        </div>
                    </div>
                  </div>
                    <div className='row'>
                      <div className='col-lg-6' style={{borderRight: '1px solid rgba(151, 151, 151, 0.2)'}}>
                        <label htmlFor='Name'>Active Image</label>
                        <input
                          type='file'
                          className='form-control'
                          ref='imageActive'
                          onChange={this.activeImageSelected.bind(this)}
                          style={{width: '200px'}}
                         />
                        <div className="meal_image">
                          {this.state && this.state.activeImagePreviewUrl ? <img src={this.state.activeImagePreviewUrl} alt=""/> :
                            <img src="/images/no-image.png" alt=""/> }
                        </div>
                      </div>
                      <div className='col-lg-6'>
                        <label htmlFor='Name'>Inactive Image</label>
                        <input
                          type='file'
                          className='form-control'
                          ref='imageNonActive'
                          onChange={this.nonactiveImageSelected.bind(this)}
                          style={{width: '200px'}}
                        />
                        <div className="meal_image">
                          {this.state && this.state.inactiveImagePreviewUrl ? <img src={this.state.inactiveImagePreviewUrl} alt=""/> :
                            <img src="/images/no-image.png" alt=""/> }
                        </div>
                      </div>
                    </div>

                    <div className="user_create_actionbtn actionbutn">
                      <div className="row">
                        <div className="col-md-12 col-lg-12">
                            <div className="actionbtn_save">
                              <button type="submit" className="btn butn butn_default">Create</button>
                            </div>
                            <div className="actionbtn_back">
                              <button type="button" className="btn butn butn_back" onClick={() => browserHistory.push('/tags')}>Back</button>
                            </div>
                        </div>
                      </div>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render () {
    return (
      <div>
        {this.heading()}
        {this.form()}
      </div>
    )
  }

  activeImageSelected(event){
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        imageActive: file,
        activeImagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
  }

  nonactiveImageSelected(event){
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    //console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        imageNonActive: file,
        inactiveImagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
  }

  handleSubmit (event) {
    event.preventDefault()

    const name = ReactDOM.findDOMNode(this.refs.name).value.trim()
    const images = [this.state.imageActive,this.state.imageNonActive]
    // validations
    if (name.length < 1) {
      toastr.warning('Name is required')
      return false
    }

    if(images === [] || this.state.imageActive === null || this.state.imageActive === undefined){
      toastr.warning('select an active image')
      return false
    }

    if(images === [] || this.state.imageNonActive === null || this.state.imageNonActive === undefined){
      toastr.warning('select an Inactive image')
      return false
    }

    // console.log('ReactDOM.findDOMNode(this.refs.imageActive).value ------>>>>>>',ReactDOM.findDOMNode(this.refs.imageActive).value)
    // console.log('this.state.imageActive',this.state.imageActive)
    // console.log('ReactDOM.findDOMNode(this.refs.imageNonActive).value) ------>>>>>>',ReactDOM.findDOMNode(this.refs.imageNonActive).value)
    // console.log('this.state.imageNonActive',this.state.imageNonActive)

    // create a object
    const tag = { name , images }

    // call create action
    this.props.createTag(tag)
  }
}

TagCreateComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  createTag: PropTypes.func.isRequired
}

export default TagCreateComponent
