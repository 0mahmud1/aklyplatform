'use strict'

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import moment from 'moment'
import toastr from 'toastr'

class TagModifyComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  tagForm() {
    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <div className='form-group'>
          <label htmlFor='Name'>Name</label>
          <input
            type='text'
            className='form-control'
            ref='name'
            placeholder='Name'
            defaultValue={this.props.tag.name} />
        </div>
        <div className='form-group'>
          <label htmlFor='Name'>Active Image</label>
          <input
            type='file'
            className='form-control'
            ref='imageActive'
            onChange={this.activeImageSelected.bind(this)} />
          <div className="meal_image">
            {this.state && this.state.activeImagePreviewUrl ? <img src={this.state.activeImagePreviewUrl} alt=""/> :
              <img src={this.props.tag.images[0]} alt=""/> }
          </div>
          <label htmlFor='Name'>Inactive Image</label>
          <input
            type='file'
            className='form-control'
            ref='imageNonActive'
            onChange={this.nonactiveImageSelected.bind(this)}/>
          <div className="meal_image">
            {this.state && this.state.inactiveImagePreviewUrl ? <img src={this.state.inactiveImagePreviewUrl} alt=""/> :
              <img src={this.props.tag.images[1]} alt=""/> }
          </div>
        </div>
        <button type='submit' className='btn btn-primary btn-flat'>Update</button>
        <a className='btn btn-default btn-flat' data-toggle='modal' data-target='#tagDeleteConfirm'>
          Delete
        </a>
        <a className='btn btn-default btn-flat' onClick={() => browserHistory.push('/tags')}>
          Back
        </a>
      </form>
    );
  }

  render () {
    let shouldLoad = this.props.tag ? true : false;
    return shouldLoad?(
        <div className='row'>
          <div className='col-xs-12'>
            <h3>Modify Tag</h3>
            { this.props.tag
              ? this.tagForm()
              : <p><b><i>Loading.......</i></b></p>
            }
          </div>
          {/* User delete confirmation modal */}
          <div
            id='tagDeleteConfirm'
            className='modal fade'
            tabIndex='-1'
            role='dialog'>
            <div className='modal-dialog' role='document'>
              <div className='modal-content'>
                <div className='modal-header'>
                  <button
                    type='button'
                    className='close'
                    data-dismiss='modal'
                    aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                  </button>
                  <h4 className='modal-title text-center'>Tag Delete Confirmation</h4>
                </div>
                <div className='modal-body'>
                  <p>Do you really want to delete this tag?</p>
                </div>
                <div className='modal-footer'>
                  <div className='row'>
                    <div className='col-xs-offset-6 col-xs-3'>
                      <button
                        type='button'
                        className='btn btn-default btn-block btn-flat'
                        data-dismiss='modal'>
                        No
                      </button>
                    </div>
                    <div className='col-xs-3'>
                      <button
                        type='button'
                        className='btn btn-primary btn-block btn-flat'
                        data-dismiss='modal'
                        onClick={this.props.deleteTag.bind(this)}>
                        Yes
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    ):<h3>Loading</h3>
  }

  activeImageSelected(event){
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    //console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        imageActive: file,
        activeImagePreviewUrl: reader.result
      });
    };

    reader.readAsDataURL(file);
  }

  nonactiveImageSelected(event){
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    //console.log('file: ', file);
    reader.onloadend = () => {
      this.setState({
        imageNonActive: file,
        inactiveImagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
  }

  handleSubmit (event) {
    event.preventDefault();
    console.log(this.refs);

    const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    const images = [this.state.imageActive, this.state.imageNonActive];
    // validations
    if (name.length < 1) {
      toastr.warning('Name is required');
      return false
    }

    if(images === [] || this.state.imageActive === null || this.state.imageActive === undefined){
      toastr.warning('select an active image');
      return false
    }

    if(images === [] || this.state.imageNonActive === null || this.state.imageNonActive === undefined){
      toastr.warning('select an imageNonActive image');
      return false
    }

    console.log('ReactDOM.findDOMNode(this.refs.imageActive).value ------>>>>>>',ReactDOM.findDOMNode(this.refs.imageActive).value)
    console.log('this.state.imageActive',this.state.imageActive)
    console.log('ReactDOM.findDOMNode(this.refs.imageNonActive).value) ------>>>>>>',ReactDOM.findDOMNode(this.refs.imageNonActive).value)
    console.log('this.state.imageNonActive',this.state.imageNonActive)

    // create a object
    const tag = { name , images }

    // call update action
    this.props.updateTag(tag)
  }
}

TagModifyComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  tag: PropTypes.object,
  deleteTag: PropTypes.func.isRequired,
  updateTag: PropTypes.func.isRequired
}

export default TagModifyComponent
