'use strict'

import React, {Component, PropTypes} from 'react'
// import ReactDOM from 'react-dom'
import ReactTable from 'react-table'
import { browserHistory } from 'react-router'
import moment from 'moment'
import PaginationComponent from '../../core/components/pagination'

class TagListComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
            pageNum:0  ,
            pageSize: 20,
			selectedRow: null,
		};
	}

    fetchData(state, instance) {
        console.log('fetchData=>', state)
        this.setState({
            pageNum: state.page,
            pageSize: state.pageSize
        }, ()=>{
            this.props.getTags(state.pageSize, state.page+1)
        })
    }

	skeleton(){

        const columns = [
			{
                header: 'Text',
                accessor: 'name',
                sortable: false,
                // filterRender: ({filter, onFilterChange}) => {
                //     return (
					// 	<input type='text'
					// 		   placeholder="Search Tag Name"
					// 		   style={{
                //                    width: '100%'
                //                }}
					// 		   onChange={(event) => onFilterChange(event.target.value)}
					// 	/>
                //     )
                // },
                footer: (
					<span>
                    <strong>Total Tags: </strong>{this.props.total}
                </span>
                )
			},
            {
                header: 'Icon (active)',
                accessor: 'image[0]',
                sortable: false,
                hideFilter: true,
                render: ({row}) =>{
                  // console.log('tag table row', row)
					return (
						<div>
							<img src={row.images[0]} alt={row.name}/>
						</div>
					)

                }
            },
            {
                header: 'Icon (inactive)',
                accessor: 'image[0]',
                sortable: false,
                hideFilter: true,
                render: ({row}) =>{
                    // console.log('tag table row', row)
                    return (
						<div>
							<img src={row.images[1]} alt={row.name}/>
						</div>
                    )

                }
            },
            {
                header: 'Status',
                accessor: 'status',
                sortable: false,
                hideFilter: true,
            },
            {
                header: 'Last Modified',
                accessor: 'updatedAt',
                hideFilter: true,
                sortable: false,
                render: ({row}) =>{
                    return (
						<div>
							{moment(row.updatedAt).format('lll')}
						</div>
                    )

                }
            }
		]
		return(
			<div className='akly_menulisttbl akly_listbl'>
				<div className='row'>
					<div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>

						<ReactTable
							manual
							className='-highlight custom-rt'
							data={this.props.tags.tagList}
							pages={this.props.totalPage}
							onChange={this.fetchData.bind(this)}
							columns={columns}
							defaultPageSize={20}
							minRows={0}
							// showFilters = {true}
							style={{cursor: 'pointer'}}
							getTrProps={(state, rowInfo, column, instance) => ({
                                onClick: e => {
                                    // browserHistory.push('/tags/' + rowInfo.row._id)
                                    window.open('/tags/' + rowInfo.row._id)
                                }
                            })}
						/>
					</div>
				</div>
			</div>
		)
	}

	render() {
		return (
			<div>
				<div className='mp_heading_mb'>
					<div className='row'>
						<div className='col-lg-12'>
							<div className='mp_heading'>
								<h3>Meal Tags</h3>
							</div>
						</div>
					</div>
				</div>
				<div className="mp_body">
					<div className="row">
						<div className="col-lg-12">
							<div className="akly_menulist akly_list">
								<div className="akly_listsearchadd_tag">
									{/*<div className="akly_listsearch">
										<div className="sb">
											<div className="form-group">
												<label className="sb_label" htmlFor="exampleInputAmount">Search</label>
												<div className="input-group sb_inputgroup">
													<input type="search" className="form-control sb_formctrl" id="exampleInputAmount"/>
													<div className="input-group-addon">
														<div className="sb_searchicon">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125"
															     preserveAspectRatio="xMidYMid meet">
																<path
																	d="M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z"
																/>
															</svg>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>*/}
									<div className="akly_addinput_tag">
										<div className="akly_inputadd geninput">
											{/*<div className="form-group inputadd_form">
												<label className="geninput_label" htmlFor="exampleInputAmount">Add Tag</label>
												<div className="input-group geninput_group geninput_group_add">
													<input type="text" className="form-control geninput_formctrl" ref='newTag'/>
												</div>
											</div>*/}
											<div className="inputadd_butn">
												<button className="addbtn" onClick={() => browserHistory.push('/tags/create')}>
													<span className="icon_plus">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
														     preserveAspectRatio="xMidYMid meet">
															<path
																d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/>
														</svg>
													</span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{this.skeleton()}
        {/*<PaginationComponent current={this.state.currentPage} dataLimitStorage="tagDataLimit" pageNumberStorage="tagCurrentPage" totalPage={this.props.totalPage} onPageChange={this.changePageNumber.bind(this)} changeLimit={this.changeLimit.bind(this)}/>*/}
        {/*{this.deleteModal()}*/}
			</div>
		)
	}

	createTag(event) {
		event.preventDefault();
		const name = this.refs.newTag.value;

		//validation

		const tag = {name};

		console.log('tag: ', tag);

		this.props.createTag(tag);

		this.refs.newTag.value = '';
	}

	rowClick(_id, event) {
		console.log('row click', _id, event.target, this);
		this.setState({selectedRow: _id});
	}

	handleEdit(event) {
		if(event.key === 'Enter') {
			console.log(this,event.target.value);
			let tag = {
				name: event.target.value
			}
			this.props.updateTag(tag,this.state.selectedRow);
		}

	}
	// this.createTag.bind(this)
	// {(evt) => this.rowClick(tag._id, evt)}

	// renderList() {
	// 	return this.props.tags.tagList.map((tag) => {
	// 		// let editUrl = '/tags/' + tag._id;
	// 		return (
	// 			<tr key={tag._id} onClick={(evt) => {this.rowClick(tag._id, evt);browserHistory.push('/tags/' + tag._id)}}>
	// 				<td>
	// 					{tag.name}
	// 					{/*<input type="text" defaultValue={tag.name} onKeyPress={this.handleEdit.bind(this)}/>*/}
	// 				</td>
     //      <td>
     //        <img src={tag.images[0]} alt="active image"/>
     //      </td>
     //      <td>
     //        <img src={tag.images[1]} alt="inactive image"/>
     //      </td>
     //      <td>
     //        {tag.status}
     //      </td>
     //      <td>{moment(new Date(tag.updatedAt)).format('lll')}</td>
	// 			</tr>
	// 		)
	// 	})
	// }

	// deleteModal() {
	// 	console.log('inside delete modal', this.state.selectedRow);
	// 	return (
	// 		<div className='modal fade aklyDelPrompt delprompt' tabIndex='-1' role='dialog' aria-labelledby='myModalLabel'>
	// 			<div className='modal-dialog delprompt_dialog' role='document'>
	// 				<div className='modal-content delprompt_content'>
	// 					<div className='modal-body'>
	// 						<div className='delprompt_text'>
	// 							<p>Do you want to delete this meal tag?</p>
	// 						</div>
	// 					</div>
	// 					<div className='modal-footer'>
	// 						<button type='button' className='btn butn butn_danger' data-dismiss='modal'
	// 						        onClick={() => this.props.deleteTag(this.state.selectedRow)}>Delete
	// 						</button>
	// 						<button type='button' className='btn butn butn_default' data-dismiss='modal'>No</button>
	// 					</div>
	// 				</div>
	// 			</div>
	// 		</div>
	// 	)
	// }
}

TagListComponent.propTypes = {
	tags: PropTypes.object.isRequired
}

export default TagListComponent
