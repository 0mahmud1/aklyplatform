import * as constants from '../constants';

const defaultState = {
	/*isRequestRunning:false,
	requestCompleted: false,
	requestFailed: false,
	getDBoysPending: false,
	getDBoysCompleted: false,
	getDBoysFailed: false,
	readDBoyPending: false,
	readDBoyCompleted: false,
	readDBoyFailed: false,*/
	list: [],
	dBoy: {}
};

const dBoyReducer = (state = defaultState, action) => {
	switch (action.type) {
		case constants.CREATE_DBOY_PENDING :
			return Object.assign({}, state, {
				isRequestRunning: true,
				requestCompleted: false,
				requestFailed: false
			});

		case constants.CREATE_DBOY_RESOLVED :
			return Object.assign({}, state, {
				isRequestRunning: false,
				requestCompleted: true,
				requestFailed: false,
			});

		case constants.CREATE_DBOY_REJECTED :
			return Object.assign({}, state, {
				isRequestRunning: false,
				requestCompleted: true,
				requestFailed: true,
				error: action.payload.error
			});

		case constants.GET_DBOY_PENDING:
			return Object.assign({}, state, {
				getDBoysPending: true,
				getDBoysCompleted: false,
				getDBoysFailed: false
			});
    case constants.GET_DBOY_RESOLVED:
		  console.log('GET_DBOY_RESOLVED: ', action.payload);
			return Object.assign({}, state, {
				getDBoysPending: false,
				getDBoysCompleted: true,
				getDBoysFailed: false,
				list: action.payload.data.data,
				total:action.payload.data.total,
				totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit) // if limit is 0 set total number of page to one
			});
		case constants.GET_DBOY_REJECTED:
			return Object.assign({}, state, {
				getDBoysPending: false,
				getDBoysCompleted: false,
				getDBoysFailed: true
			});

		case constants.READ_DBOY_PENDING:
			return Object.assign({}, state, {
				readDBoyPending: true,
				readDBoyCompleted: false,
				readDBoyFailed: false,
			});
		case constants.READ_DBOY_RESOLVED:
			return Object.assign({}, state, {
				readDBoyPending: false,
				readDBoyCompleted: true,
				readDBoyFailed: false,
        clonedDBoy: action.payload.data.data
			});
		case constants.READ_DBOY_REJECTED:
			return Object.assign({}, state, {
				readDBoyPending: false,
				readDBoyCompleted: false,
				readDBoyFailed: true
			});
		
		case constants.DBOY_VENDOR_SELECT:
			return Object.assign({}, state, {vendorSelected: action.payload});
		case constants.DBOY_VENDOR_MARKER:
			return Object.assign({}, state, {vendorMarker: action.payload});
		case 'CLEAR_HUB':
			return defaultState;

		default :
			return state;
	}
};

export default dBoyReducer