'use strict'

import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import toastr from 'toastr'

import { createDBoy } from '../actions/index'
import DBoyCreateComponent from '../components/create'

class DBoyCreateContainer extends Component {
	render() {
		return (
			<DBoyCreateComponent
				createDBoy={this.createDBoy.bind(this)}
				dBoys={this.props.dBoys}
			/>
		)
	}

	createDBoy(options) {
		this.props.createDBoy(options, function (err, data) {
      if(data){
        browserHistory.push('/dboys/');
        toastr.success('delivery boy created successfully');
      }
      if(err){
        toastr.error(err);
      }
    });
	}
}

DBoyCreateContainer.propTypes = {
	createDBoy: PropTypes.func.isRequired
};

function mapStateToProps(store) {
	return {
		dBoys: store.dBoys
	}
}

// function mapDispatchToProps(dispatch) {
// 	return bindActionCreators({
// 		createUser: createUserAction
// 	}, dispatch)
// }

const mapDispatchToProps = (dispatch) => {
	return {
		createDBoy: (options, cb) => dispatch(createDBoy(options, cb)),
		vendorSelect: (value) => dispatch({ type: 'DBOY_VENDOR_SELECT', payload: value }),
		saveMarkerPosition: (position) => dispatch({ type:'DBOY_VENDOR_MARKER', payload: position })
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(DBoyCreateContainer)
