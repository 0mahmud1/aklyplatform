'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import toastr from 'toastr'

import { readDBoy, deleteDBoy, updateDBoy } from '../actions/index';

import DBoyModifyComponent from '../components/modify';

class DBoyModifyContainer extends Component{

	componentWillMount() {
		this.props.readDBoy(this.props.params._id);
	}
	
	componentWillUnmount(){
		this.props.clearHub();
	}
	componentDidUpdate(prevProps, prevStates) {
		console.log('PREVIOUS PROPS:', prevProps, '\n NEW PROPS', this.props);
	}

	deleteDBoy() {
		this.props.deleteDBoy(this.props.params._id, function (err, data) {
      if(data){
        browserHistory.push('/dboys/');
        toastr.success('delivery boy deleted successfully!!');
      }
      if(err){
        toastr.error(err);
      }
    });
	}

	updateDBoy(userId, patch) {
		console.log(userId, this);
		this.props.updateDBoy(userId,patch, function (err, data) {
      if(data){
        browserHistory.push('/dboys/');
        toastr.success('Delivery Boy updated successfully!!');
      }
      if(err){
        toastr.error(err);
      }
    });
	}

	loadingUI() {
		return (
			<div>
				<h1>Loading.... </h1>
			</div>
		)
	}

	render() {
		let shouldRender = (this.props.dBoy && this.props.params._id == this.props.dBoy._id);
		return(
			shouldRender ?
			<DBoyModifyComponent
				dBoy={this.props.dBoy}
				newPosition={this.props.newPosition}
				updateDBoy={this.updateDBoy.bind(this)}
				deleteDBoy={this.deleteDBoy.bind(this)}
      />
			: this.loadingUI()
    )

	}
}


function mapStateToProps (store) {
	return {
		dBoy: store.dBoys.clonedDBoy,
		newPosition: store.dBoys.vendorMarker
	}
}

// Get actions and pass them as props to to DBoysListContainer
//  > now DBoysListContainer has this.props.getDBoys
// function mapDispatchToProps (dispatch) {
// 	return bindActionCreators({
// 		readUser: readUser,
// 		updateUser: updateUser,
// 		deleteUser: deleteUser
// 	}, dispatch)
// }

const mapDispatchToProps = (dispatch) => {
	return {
		readDBoy: (_id) => dispatch(readDBoy(_id)),
		updateDBoy: (_id, patch, cb) => dispatch(updateDBoy(_id, patch,cb)),
		deleteDBoy: (_id, cb) => dispatch(deleteDBoy(_id, cb)),
		saveMarkerPosition: (position) => dispatch({ type:'DBOY_VENDOR_MARKER', payload: position }),
		clearHub: () => dispatch({type:'CLEAR_HUB'})
	}
};

// We don't want to return the plain UserListContainer (component) anymore,
// we want to return the smart Container
//  > UserListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(DBoyModifyContainer)
