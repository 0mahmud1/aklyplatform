'use strict';

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getDBoys, getFilteredDBoys } from '../actions/index'
import DBoyListComponent from '../components/list'

class DBoyListContainer extends Component {
	componentWillMount () {
		this.props.getDBoys(sessionStorage.getItem('deliveryBoyDataLimit'), sessionStorage.getItem('deliveryBoyCurrentPage'));
	}

	render () {
	  console.log('DBoyListContainer render');
		console.log(this.props);
		return (
			<DBoyListComponent
				getDBoys={this.props.getDBoys}
				getFilteredDBoys={this.props.getFilteredDBoys}
				totalPage={this.props.totalPage}
				total={this.props.total}
				dBoys={this.props.dBoys.list} />
		)
	}
}

/*DBoyListContainer.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	getDBoys: PropTypes.func.isRequired,
	dBoys: PropTypes.array.isRequired
}*/

// Get apps store and pass it as props to UserListContainer
//  > whenever store changes, the UserListContainer will automatically re-render
function mapStateToProps (store) {
	return {
		dBoys: store.dBoys,
		totalPage: store.dBoys.totalPage,
        total: store.dBoys.total
	}
}

// Get actions and pass them as props to to UserListContainer
//  > now DBoysListContainer has this.props.getDBoys
function matchDispatchToProps (dispatch) {
	return bindActionCreators({
		getDBoys: getDBoys,
        getFilteredDBoys: getFilteredDBoys
	}, dispatch)
}

// We don't want to return the plain DBoysListContainer (component) anymore,
// we want to return the smart Container
//  > DBoysListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(DBoyListContainer)
