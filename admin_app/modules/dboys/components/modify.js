'use strict';

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import moment from 'moment'
import toastr from 'toastr'

class DBoyModifyComponent extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {

    let reactDOM = this;
    console.log(reactDOM);

    //$eventSelect.on("select2:open", function (e) { log("select2:open", e); });
  }

  render() {
    let isAssign = this.props.dBoy.isAssociated ? "checked" : "";
    let isBusy = this.props.dBoy.isBusy ? "checked" : "";

    return (
      <div>
        <div className="mp_heading_mb">
          <div className="row">
            <div className="col-lg-12">
              <div className="mp_heading">
                <h3>Edit Delivery Boy</h3>
              </div>
            </div>
          </div>
        </div>
        <div className="mp_body">
          <div className="row">
            <div className="col-lg-12">
              <div className="user_edit">
                { this.props.dBoy
                  ? <form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)}>
                    <div className="form-group geninput">
                      <label htmlFor="name" className="col-sm-2 control-label geninput_label">Name</label>
                      <div className="col-sm-10 geninput_group geninput_group_add">
                        <input defaultValue={this.props.dBoy.name} type="text" ref="name" className="form-control geninput_formctrl" id="fullName" placeholder="Full Name" />
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="email" className="col-sm-2 control-label control-label geninput_label">Email</label>
                      <div className="col-sm-10">
                        <input defaultValue={this.props.dBoy.email} readOnly="readOnly" type="email" ref="email" className="form-control geninput_formctrl" id="email" />
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="phone" className="col-sm-2 control-label control-label geninput_label">Phone</label>
                      <div className="col-sm-10">
                        <input defaultValue={this.props.dBoy.phone} type="text" ref="phone" className="form-control geninput_formctrl" id="phone" placeholder="Cell no" />
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="license" className="col-sm-2 control-label control-label geninput_label">License</label>
                      <div className="col-sm-10">
                        <input defaultValue={this.props.dBoy.license} type="text" ref="license" className="form-control geninput_formctrl" id="license" placeholder="bike license" />
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="address" className="col-sm-2 control-label control-label geninput_label">Address</label>
                      <div className="col-sm-10">
                        <textarea defaultValue={this.props.dBoy.address} ref="address" className="form-control geninput_formctrl" id="address" placeholder="Address">
                        </textarea>
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="capacity" className="col-sm-2 control-label control-label geninput_label">Capacity</label>
                      <div className="col-sm-10">
                        <input defaultValue={this.props.dBoy.capacity} type="number" ref="capacity" className="form-control geninput_formctrl" id="capacity" placeholder="Capacity" />
                      </div>
                    </div>
                  
                    <div className="form-group geninput">
                      <label htmlFor="isAssociated" className="col-sm-2 control-label control-label geninput_label">Associated</label>
                      <div className="col-sm-10">
                        <input defaultChecked={isAssign} type="checkbox" ref="associated" className="form-control geninput_formctrl" />
                      </div>
                    </div>

                    <div className="form-group geninput">
                      <label htmlFor="isBusy" className="col-sm-2 control-label control-label geninput_label">Busy</label>
                      <div className="col-sm-10">
                        <input defaultChecked={isBusy} type="checkbox" ref="busy" className="form-control geninput_formctrl" />
                      </div>
                    </div>

                    <div className="form-group user_edit_actionbtn actionbutn">
                      <div className="col-sm-offset-2 col-sm-10">
                        <div className="actionbtn_save">
                          <button className="btn butn butn_default" type='submit'>Update</button>
                        </div>
                        <div className="actionbtn_delete">
                          <button type="button" className="btn butn butn_danger" data-toggle='modal'
                                  data-target='#modalRemove'>Delete
                          </button>
                        </div>
                        <div className="actionbtn_back">
                          <button type="button" className="btn butn butn_back"
                                  onClick={() => browserHistory.push('/dboys')}>
                            Back
                          </button>
                        </div>
                      </div>
                    </div>
                </form>
                  : <p><b><i>Loading.......</i></b></p>
                }
              </div>
            </div>
          </div>
        </div>

        {/* User delete confirmation modal */}
        <div id='modalRemove' className='modal fade delprompt' tabIndex='-1' role='dialog'>
          <div className='modal-dialog delprompt_dialog' role='document'>
            <div className='modal-content delprompt_content'>
              <div className="modal-body">
                <div className="delprompt_text">
                  <p>Do you want to delete this delivery boy?</p>
                </div>
              </div>
              <div className='modal-footer'>
                <button type="button" className="btn butn butn_danger" onClick={this.deleteDBoy.bind(this)}>Yes</button>
                <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }

  handleSubmit(event) {
    event.preventDefault();

    const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    const email = ReactDOM.findDOMNode(this.refs.email).value.trim()
    const phone = ReactDOM.findDOMNode(this.refs.phone).value.trim()
    const license = ReactDOM.findDOMNode(this.refs.license).value.trim()
    const address = ReactDOM.findDOMNode(this.refs.address).value.trim()
    const capacity = ReactDOM.findDOMNode(this.refs.capacity).value.trim()
    const isAssociated = ReactDOM.findDOMNode(this.refs.associated).checked;
    const isBusy = ReactDOM.findDOMNode(this.refs.busy).checked;

    console.log('goes through validations');

    // validations
    if (name.length < 1) {
      toastr.warning('Name is required');
      return false
    }
    else if (email.length < 1) {
      toastr.warning('Input email')
      return false
    }
    else if (phone.length < 1) {
      toastr.warning('Input phone')
      return false
    }
    else if (capacity < 1) {
      toastr.warning('Capacity Must be getter then 0')
      return false
    }
    else if (license.length < 1) {
      toastr.warning('Input license')
      return false
    }
    else if (address.length < 1) {
      toastr.warning('Input address')
      return false
    }

    // create a object
    const dBoy = { name, email, phone, license, address, capacity, isAssociated, isBusy};

    // call update action
    this.props.updateDBoy(this.props.dBoy._id, dBoy)
  }

  handleSelection(event) {
    event.preventDefault();
    console.log(event.target.value);
    //this.refs.role.value = event.target.value;
  }

  deleteDBoy() {
    this.props.deleteDBoy();
    $('#modalRemove').modal('hide');
  }
}

DBoyModifyComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  dBoy: PropTypes.object,
  deleteDBoy: PropTypes.func.isRequired,
  updateDBoy: PropTypes.func.isRequired
};

export default DBoyModifyComponent
