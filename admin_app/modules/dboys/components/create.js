'use strict';

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import toastr from 'toastr'

class DeliveryBoyCreateComponent extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<div className="mp_heading_mb">
					<div className="row">
						<div className="col-lg-12">
							<div className="mp_heading">
								<h3>Add Delivery Boy</h3>
							</div>
						</div>
					</div>
				</div>

				<div className="mp_body">
					<div className='row'>
						<div className='col-lg-12'>
							<div className="user_create">
								<form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)}>
                  <div className="form-group geninput">
                    <label htmlFor="name" className="col-sm-2 control-label geninput_label">Name</label>
                    <div className="col-sm-10 geninput_group geninput_group_add">
                      <input type="text" ref="name" className="form-control geninput_formctrl" id="fullName" placeholder="Full Name" />
                    </div>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="email" className="col-sm-2 control-label geninput_label">Email</label>
                    <div className="col-sm-10 geninput_group geninput_group_add">
                      <input type="email" ref="email" className="form-control geninput_formctrl" id="email" placeholder="something@real.com" />
                    </div>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="password" className="col-sm-2 control-label geninput_label">Password</label>
                    <div className="col-sm-10 geninput_group geninput_group_add">
                      <input type="password" ref="password" className="form-control geninput_formctrl" id="password" placeholder="super secret!" />
                    </div>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="phone" className="col-sm-2 control-label geninput_label">Phone</label>
                    <div className="col-sm-10">
                      <input type="text" ref="phone" className="form-control geninput_formctrl" id="phone" placeholder="Cell no" />
                    </div>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="license" className="col-sm-2 control-label geninput_label">License</label>
                    <div className="col-sm-10">
                      <input type="text" ref="license" className="form-control geninput_formctrl" id="license" placeholder="bike license" />
                    </div>
                  </div>

									<div className="form-group geninput">
										<label htmlFor="capacity" className="col-sm-2 control-label geninput_label">Capacity</label>
										<div className="col-sm-10">
											<input type="number" ref="capacity" defaultValue="2" className="form-control geninput_formctrl" id="capacity" placeholder="Capacity" />
										</div>
									</div>

                  <div className="form-group geninput">
                    <label htmlFor="address" className="col-sm-2 control-label geninput_label">Address</label>
                    <div className="col-sm-10">
                      <textarea ref="address" className="form-control geninput_formctrl" id="address" placeholder="Address">
                      </textarea>
                    </div>
                  </div>

                  <div className="form-group user_create_actionbtn actionbutn">
                    <div className="col-sm-offset-2 col-sm-10">
                      <button type="submit" className="btn butn butn_default">Create</button> &nbsp;
                      <button type="button" className="btn butn butn_back"
                              onClick={() => browserHistory.push('/dboys')}>Back</button>
                    </div>
                  </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	handleSubmit(event) {
		event.preventDefault();
		const self = this;
    	console.log(self.refs);
    	const options = {
			name: ReactDOM.findDOMNode(self.refs.name).value.trim(),
			email: ReactDOM.findDOMNode(self.refs.email).value.trim(),
			password: ReactDOM.findDOMNode(self.refs.password).value,
			phone: ReactDOM.findDOMNode(self.refs.phone).value.trim(),
			license: ReactDOM.findDOMNode(self.refs.license).value.trim(),
			address: ReactDOM.findDOMNode(self.refs.address).value.trim(),
			capacity: ReactDOM.findDOMNode(self.refs.capacity).value.trim()
		};
		// validations
		if (options.name.length <= 0) {
			toastr.warning('Name is required');
			return
		}
		else if (options.email.length <= 0) {
			toastr.warning('email is required');
			return
		}
		else if (options.password.length <= 0) {
			toastr.warning('Password is required');
			return
		}
		else if(options.phone.length <= 0){
			toastr.warning('Phone No is required');
			return
		}
		else if(options.license.length <= 0){
			toastr.warning('License No is required');
			return
		}
		else if(options.address.length <= 0){
			toastr.warning('Address is required');
			return
		}
		else if (options.capacity === undefined || options.capacity === null) {
			toastr.warning('Input valid capacity')
			return
		}

		console.log(options, '...');

		// call create action
		this.props.createDBoy(options);
	}
}

DeliveryBoyCreateComponent.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	createDBoy: PropTypes.func.isRequired
};

export default DeliveryBoyCreateComponent
