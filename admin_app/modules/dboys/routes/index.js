'use strict';

import React from 'react';
import { Route, IndexRoute } from 'react-router';

import DeliveryBoyLayout from '../layouts';
import DBoyListContainer from '../containers/list';
import DBoyCreateContainer from '../containers/create';
import DBoyModifyContainer from '../containers/modify';
import * as AuthService from '../../services/auth';

export default function () {
  console.log('delivery boy router');
  return (
    <Route path='dboys' component={DeliveryBoyLayout} onEnter={AuthService.isDashAuthRouter}>
      <IndexRoute component={DBoyListContainer}/>
      <Route path='create' component={DBoyCreateContainer} />
      <Route path=':_id' component={DBoyModifyContainer} />
    </Route>
  )
};
