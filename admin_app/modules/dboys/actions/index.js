import { browserHistory } from 'react-router';
import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

const moduleUrl = 'api/dboys';

export function createDBoy(options, cb) {
  return function (dispatch) {
    console.log('... in action ', options);
    dispatch({ type: constants.CREATE_DBOY_PENDING });
    axios.post(moduleUrl, options)
    .then((response) => {
      console.log('createDBoy response', response);
      dispatch({ type: constants.CREATE_DBOY_RESOLVED });
      if (cb) {
        cb(null, { type: constants.CREATE_DBOY_RESOLVED });
      }
    })
    .catch((err) => {
      dispatch({ type: constants.CREATE_DBOY_REJECTED, payload: err });
      if (cb) {
        cb(err, null);
      }
    });
  };
}

export function getDBoys(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: constants.GET_DBOY_PENDING });
    axios.get(moduleUrl + '?limit=' + l + '&page=' + p)
    .then((response) => {
      console.log('getDBoys response', response);
      dispatch({ type: constants.GET_DBOY_RESOLVED, payload: response });
    })
    .catch((err) => {
      dispatch({ type: constants.GET_DBOY_REJECTED, payload: err });
      toastr.error(err);
    });
  };
}

export function getFilteredDBoys(options) {
    return function (dispatch) {
        let params = {};
        if (options['name']) {
          params['name'] = options['name']
        }
        if (options['email']) {
            params['email'] = options['email']
        }
        dispatch({ type: constants.GET_DBOY_PENDING });
        axios.get(moduleUrl , {
            params: params
          })
            .then((response) => {
                console.log('getDBoys response', response);
                dispatch({ type: constants.GET_DBOY_RESOLVED, payload: response });
            })
            .catch((err) => {
                dispatch({ type: constants.GET_DBOY_REJECTED, payload: err });
                toastr.error(err);
            });
    };
}

export function readDBoy(_id) {
  return function (dispatch) {
    dispatch({ type: constants.READ_DBOY_PENDING });
    console.log('api params: ', _id);

    axios.get(moduleUrl + '/' + _id)
    .then((response) => {
      console.log('get user... : ', response);
      dispatch({ type: constants.READ_DBOY_RESOLVED, payload: response });
      console.log('Read a User-Action: ', response);
    })
    .catch((err) => {
      dispatch({ type: constants.READ_DBOY_REJECTED, payload: err });
      console.warn('Read a User-Action: ', err);
      toastr.error("Delivery Boy not found");
    });
  };
}

export function updateDBoy(_id, patch, cb) {
  return function (dispatch) {
    dispatch({ type: constants.UPDATE_DBOY_PENDING });
    console.log('PATCH: ', patch, _id);
    axios.put(moduleUrl + '/' + _id, patch)
    .then((response) => {
      console.log('UPDATE user... : ', response);
      dispatch({ type: constants.UPDATE_DBOY_RESOLVED, payload: response });
      if (cb) {
        cb(null, { type: constants.UPDATE_DBOY_RESOLVED, payload: response });
      }
    })
    .catch((err) => {
      dispatch({ type: constants.UPDATE_DBOY_REJECTED, payload: err });
      if (cb) {
        cb(err, null);
      }
    });
  };
}

export function deleteDBoy(_id, cb) {
  return function (dispatch) {
    axios.delete(moduleUrl + '/' + _id)
    .then((response) => {
      console.log('response deleteDBoy : ', response);
      dispatch({ type: constants.DELETE_DBOY_RESOLVED, payload: response });
      console.log('Read a User-Action: ', response);
      if (cb) {
        cb(null, { type: constants.DELETE_DBOY_RESOLVED, payload: response });
      }
    })
    .catch((err) => {
      dispatch({ type: constants.DELETE_DBOY_REJECTED, payload: err });
      console.warn('Read a DBoy-Action: ', err);
      if (cb) {
        cb(err, null);
      }
    });
  };
}
