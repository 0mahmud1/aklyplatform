import { browserHistory } from 'react-router';
import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

export function getActiveDeliveryBoys() {
	return function (dispatch) {
		//dispatch({type: constants.READ_HUB_PENDING});
		axios.get('api/dboys', {
			params: {
        isAssociated: false,
			},
		}).then((response) => {
      console.group('getActiveDeliveryBoys');
			console.log(response);
      console.groupEnd();
			dispatch({ type: constants.GET_DELIVERYBOY_RESOLVED, payload: response.data.data });
		}).catch((err) => {
			dispatch({ type: constants.GET_DELIVERYBOY_REJECTED, payload: err });
			console.warn('all hub err ', err);
		});
	};
}

export function getActiveFoodProviders() {
	return function (dispatch) {
		//dispatch({type: constants.READ_HUB_PENDING});
		axios.get('api/foodproviders', {
			params: {
				isAssociated: false,
			},
		}).then((response) => {
      console.group('getActiveFoodProviders');
      console.log(response);
      console.groupEnd();
			dispatch({ type: constants.GET_FOODPROVIDER_RESOLVED, payload: response.data.data });
		}).catch((err) => {
			dispatch({ type: constants.GET_FOODPROVIDER_REJECTED, payload: err });
			console.warn('all hub err ', err);
		});
	}
}

export function createHubAction(hub, cb) {
  return function (dispatch) {
    dispatch({ type: constants.CREATE_HUB_PENDING });
    axios.post('api/zones', hub)
      .then((response) => {
        dispatch({ type: constants.CREATE_HUB_RESOLVED });
        //browserHistory.push('/users/');
        console.log('createHubAction: ', response);
        if (cb) {
          cb(null, { type: constants.CREATE_HUB_RESOLVED });
        }
        console.log('createHubAction: ', response);
        //toastr.success('hub created successfully');
	      //browserHistory.push('/hubs/');
      })
      .catch((err) => {
        dispatch({ type: constants.CREATE_HUB_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
        console.warn('createHubAction: ', err);
      });
  }
}


export function readHubAction(_id) {
  console.log('..in action');
  return function (dispatch) {
    dispatch({ type: 'READ_HUB_PENDING' });
    axios.get(('api/zones/' + _id))
      .then((response) => {
        console.log('...response', response);
        dispatch({ type: constants.READ_HUB_RESOLVED, payload: response.data.data });
        //browserHistory.push('/users/');
        console.log('read hub action: ', response);
        //toastr.success('hub read successfully');
      })
      .catch((err) => {
        dispatch({ type: constants.READ_HUB_REJECTED, payload: err });
        console.warn('Read HubAction: ', err);
        toastr.error("No zone found")
      });
  }
}

export function getHubsAction() {
  return function (dispatch) {
    //dispatch({type: constants.READ_HUB_PENDING});
    axios.get('api/zones')
      .then((response) => {
        console.log('all hub... : ', response);
        dispatch({ type: constants.GET_HUB_RESOLVED, payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: constants.GET_HUB_REJECTED, payload: err });
        console.warn('all hub err ', err);
      });
  }
}


export function updateHubAction(_id, patch, cb) {
  return function (dispatch) {
    dispatch({ type: constants.UPDATE_HUB_PENDING });
    console.log('PATCH: ', patch, _id);
    axios.put('api/zones/' + _id, patch)
      .then((response) => {
        console.log('UPDATE hub... : ', response);
        dispatch({ type: constants.UPDATE_HUB_RESOLVED, payload: response });
        if (cb) {
          cb(null,{ type: constants.UPDATE_HUB_RESOLVED, payload: response });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.UPDATE_HUB_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  }
}


export function deleteHubAction(_id, cb) {
  return function (dispatch) {
    axios.delete('api/zones/' + _id)
      .then((response) => {
        console.log('delete hub... : ', response);
        dispatch({ type: constants.DELETE_HUB_RESOLVED, payload: response });
        if (cb) {
          cb(null, { type: constants.DELETE_HUB_RESOLVED, payload: response });
        }
        //browserHistory.push('/hubs');
        console.log('delete a hub-Action: ', response);
      })
      .catch((err) => {
        dispatch({ type: constants.DELETE_HUB_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
        console.warn('delete rejected: ', err);
      });
  }
}
