'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import HubLayout from '../layouts'
import HubCreateContainer from '../containers/create';
import HubListContainer from '../containers/list';
import HubModifyContainer from '../containers/modify';
import * as AuthService from '../../services/auth';
export default function () {
  return (
    <Route path='hubs' component={HubLayout} onEnter={AuthService.isDashAuthRouter}>
      <IndexRoute component={HubListContainer} />
      <Route path='create' component={HubCreateContainer} />
      <Route path=':_id' component={HubModifyContainer} />
    </Route>
  )
};
