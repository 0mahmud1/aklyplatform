'use strict'

import React, {Component, PropTypes} from 'react'
import ReactDOM from 'react-dom'
import {browserHistory} from 'react-router'
import toastr from 'toastr'

class HubCreateComponent extends Component {
	generateDeliveryBoyList() {
		return this.props.deliveryBoys.map((db) => {
			return <option key={db._id} data={db} value={db._id}>{db.name}</option>
		})
	}

	generateFoodProviderList() {
		return this.props.foodProviders.map((fp) => {
			return <option key={fp._id} data={fp} value={fp._id}>{fp.name}</option>
		})
	}

	handleSubmit(event) {
		event.preventDefault();
		let locations = {
			coordinates : [this.props.polygon]
		}
		const name = this.refs.name.value;

		console.log('name :', name);
		console.log('area ', this.props.polygon);

		//validation goes here
		if(name.length <= 0){
			toastr.warning('No zone name provided');
			return
		}
		if(!this.props.polygon){
			toastr.warning('Please draw zone in map');
			return
		}
		console.log('final location: ', locations.coordinates)

		this.props.createHub({name: name, locations: locations, deliveryBoys: this.props.dbs, foodProviders: this.props.fps})
	}

	render() {
		console.log('hub create render');
		return (
			<div>
				<div className="mp_heading_mb">
					<div className="row clearfix">
						<div className="col-lg-12">
							<div className="mp_heading">
								<h3>Define Your Zone</h3>
							</div>
						</div>
					</div>
				</div>

				<div className="mp_body">
					<div className="row clearfix">
						<div className="col-lg-12">
							<div className="zone_create">
								<form onSubmit={this.handleSubmit.bind(this)}>

                  <div className="form-group geninput">
                    <label htmlFor="name" className="geninput_label">Zone Name</label>
                    <input id="name" type="text" className="form-control geninput_formctrl" ref='name' required='required'/>
                  </div>

                  <div className="form-group geninput">
                    <div className="zone_create_gmap_heading">
                      <h4>Click the polygon icon to select your zone.</h4>
                    </div>
                    <div id='hub-map' className="gmap_wrapper"></div>
                    <input id='search-place' className='controls gmap_search' type='text' placeholder='Please Search'/>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="deliveryBoy" className="geninput_label">Associated Delivery Boy</label>
                    <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
                      <select id="deliveryBoy" multiple="multiple" className='form-control'>
                        {this.generateDeliveryBoyList()}
                      </select>
                    </div>
                  </div>

                  <div className="form-group geninput">
                    <label htmlFor="foodProvider" className="geninput_label">Associated Food Provider</label>
										<div className="input-group geninput_group geninput_group_add geninput_select2_tag">
											<select id="foodProvider" multiple="multiple" className='form-control'>
												{this.generateFoodProviderList()}
											</select>
										</div>
									</div>

									<div className="zone_create_actionbtn actionbutn">
										<div className="row">
											<div className="col-md-12 col-lg-12">
												<div className="actionbtn_save">
													<button className="btn butn butn_default" type='submit'>
														Save
													</button>
												</div>

												<div className="actionbtn_back">
													<button type="button" className="btn butn butn_back"
													        onClick={() => browserHistory.push('/hubs')}>Back
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>

		)
	}
}


export default HubCreateComponent
