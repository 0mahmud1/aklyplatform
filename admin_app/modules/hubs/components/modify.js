import React, {Component} from 'react'
import {Link} from 'react-router'
import toastr from 'toastr'
import _ from 'lodash';
import $ from 'jquery';

import {browserHistory} from 'react-router';

class HubModifyComponent extends Component {

  componentDidMount() {
    let reactDOM = this;
    let select2container = $('#deliveryBoy');
    let select2settings = {
      tags: true,
      placeholder: 'Select delivery boy',
      tokenSeparators: [',']
    };
    let dbIds = this.props.hub.deliveryBoys.map((db) => db._id);
    console.log('xx ', dbIds);

    $('#foodProvider').select2();

    /////////// MAP CONFIGS //////////
    this.initializeGoogleMap();


    ///////////// SELECT2 CONFIGS   ///////////////

    //select2Container.val(this.props.food.tags).trigger('change');

    select2container.select2(select2settings).on('select2:select', (evt) => {
      console.log('selected ', evt);
      // evt.params.data.id
      let selectedDb = _.find(reactDOM.props.hubs.deliveryBoys, function (deliveryBoy) {
        return deliveryBoy._id == evt.params.data.id
      });

      //console.log('db found ', selectedDb);
      reactDOM.props.selectDb(selectedDb);

    }).on('select2:unselect', (evt) => {
      console.log('unselect: ', evt);
      reactDOM.props.unselectDb(evt.params.data.id);
    });

    select2container.val(dbIds).trigger('change'); // if you want to propagate with values make sure you have the options & val should be an array of value(id) not the text

    reactDOM.props.populateDbs(reactDOM.props.hubs.hub.deliveryBoys);
  }

  initializeGoogleMap() {
    let reactDOM = this;
    let markers = [];
    let polygon = [];
    let map = new google.maps.Map(document.getElementById('hub-map-edit'), {
      center: reactDOM.convertGeoArrayToObject(reactDOM.props.hub.locations.coordinates[0][0]),
      zoom: 12
    });
    google.maps.event.addListenerOnce(map, 'idle', function () {
      // do something only the first time the map is loaded
      console.log(map.getBounds());

    });

    let input = document.getElementById('search-place-hub-edit');
    let searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    searchBox.addListener('places_changed', function () {
      let places = searchBox.getPlaces()[0]; //WE ARE ONLY CONCERN ABOUT THE FIRST ELEMENT
      console.log('select primary area   ', places);

      if (places.length == 0) {
        return;
      }

      // changing center to get a better view of the desired location where area will be drawn.
      map.setCenter(places.geometry.location);
    });


    let drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT,
        drawingModes: ['polygon']
      }
    });
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
      console.log('drawing event fired: ', event);
      if (event.type == 'polygon') {
        let areas = event.overlay.latLngs.b[0].b;
        console.log('area line length: ', areas.length);
        areas.forEach(function (point) {
          console.log(point.lat(), ' ', point.lng());
          //let LatLng = {lat: point.lat(), lng: point.lng()};
          polygon.push([point.lng(), point.lat()]);
          //polygon.push(LatLng);
          console.log('pushing');
        });
        polygon.push([areas[0].lng(), areas[0].lat()]);

        reactDOM.props.savePolygon(polygon);
      }
    });
    // google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
    //   console.log('polygon:...', polygon)
    // });

    let polygonCoords = reactDOM.props.hub.locations.coordinates[0];
    console.log('area...', polygonCoords)

    map.addListener('bounds_changed', function () {
      console.log('bound changes fired ..');
      //searchBox.setBounds(map.getBounds());
    });

    // Construct the polygon.
    let oldPolygon = new google.maps.Polygon({
      paths: reactDOM.convertPolygonArrayToObject(polygonCoords),
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35
    });

    oldPolygon.setMap(map);
  }

  convertGeoArrayToObject(point) {
    return {lng: point[0], lat: point[1]}
  }
  convertPolygonArrayToObject(polygon) {
    let pl = [];
    polygon.forEach(point => {
      pl.push({lng: point[0], lat: point[1]})
    })

    return pl
  }

  handleSubmit(event) {
    event.preventDefault();
    const name = this.refs.name.value;
    const locations = {
      coordinates: [this.props.hubs.drawingArea]
    }
    const deliveryBoys = this.props.hubs.selectedDbs;
    // const foodProviders =

    const foodProviderIDs = $('#foodProvider').val();

    const foodProviderList = _.uniqBy(_.union(this.props.foodProviders, this.props.hub.foodProviders), '_id');
    const foodProviders = _.filter(foodProviderList, function (data) {
      return _.includes(foodProviderIDs, data._id);
    });

    // if(name.length < 1 || !this.props.newArea) {
    //   toastr.error('validation error');
    //   return;
    // }
    //console.log('>> ',{name, geoLocations,deliveryBoys} )

    //validation goes here
    if(name.length <= 0){
      toastr.warning('No zone name provided');
      return
    }
    if(!this.props.hubs.drawingArea){
      toastr.warning('Please draw zone in map');
      return
    }

    console.log('update; ', {name,locations,deliveryBoys, foodProviderList})
    this.props.updateHub({name, locations, deliveryBoys, foodProviders})
  }

  generateDeliveryBoyList() {
    const dBoyList = _.uniqBy(_.union(this.props.dBoys, this.props.hub.deliveryBoys), '_id');
    return dBoyList.map((data) => {
      return <option key={data._id} value={data._id}>{data.name}</option>
    });
  }

  selectedDBoys() {
    return (
      this.props.hub.deliveryBoys && this.props.hub.deliveryBoys.length > 0 ?
        this.props.hub.deliveryBoys.map((data) => {
          return data._id;
        }) : null
    )
  }

  generateFoodProviderList() {
    const foodProviderList = _.uniqBy(_.union(this.props.foodProviders, this.props.hub.foodProviders), '_id');
    return foodProviderList.map((data) => {
      return <option key={data._id} data={data} value={data._id}>{data.name}</option>
    })
  }

  selectedFoodProviders() {
    return (
      this.props.hub.foodProviders && this.props.hub.foodProviders.length > 0 ?
        this.props.hub.foodProviders.map((data) => {
          return data._id;
        }) : null
    )
  }

  deleteHub() {
    this.props.deleteHub();
    $('#hubDeleteConfirm').modal('hide');
  }

  loadingUI() {
    return (
      <div className="spinner"></div>
    );
  }

  pageTitle() {
    return (
      <div className="mp_heading_mb">
        <div className="row">
          <div className="col-lg-12">
            <div className="mp_heading">
              <h3>Edit This Zone</h3>
            </div>
          </div>
        </div>
      </div>
    );
  }


  render() {
    return (

      <div>
        {
          this.props.hub ?
            <div>
              {this.pageTitle()}

              <div className="mp_body">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="zone_edit">
                      <form onSubmit={this.handleSubmit.bind(this)}>

                        <div className="form-group geninput">
                          <label htmlFor="name" className="geninput_label">Zone Name</label>
                          <input id="name" type="text" defaultValue={this.props.hub.name}
                                 className="form-control geninput_formctrl" ref='name' required='required'/>
                        </div>

                        <div className="form-group geninput">
                          <div className="zone_edit_gmap_heading">
                            <h4>Click the polygon icon to select your zone.</h4>
                          </div>
                          <div id='hub-map-edit' className="gmap_wrapper"></div>
                          <input id='search-place-hub-edit' className='controls gmap_search' type='text'
                                 placeholder='Please Search'/>
                        </div>

                        <div className="form-group geninput">
                          <label htmlFor="deliveryBoy" className="geninput_label">Associated Delivery Boy</label>
                          <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
                            <select id="deliveryBoy" defaultValue={this.selectedDBoys()} multiple="multiple"
                                    className='form-control'>
                              {this.generateDeliveryBoyList()}
                            </select>
                          </div>
                        </div>

                        <div className="form-group geninput">
                          <label htmlFor="foodProvider" className="geninput_label">Associated Food Provider</label>
                          <div className="input-group geninput_group geninput_group_add geninput_select2_tag">
                            <select id="foodProvider" defaultValue={this.selectedFoodProviders()} multiple="multiple"
                                    className='form-control'>
                              {this.generateFoodProviderList()}
                            </select>
                          </div>
                        </div>

                        <div className="zone_edit_actionbtn actionbutn">
                          <div className="row">
                            <div className="col-md-12 col-lg-12">
                              <div className="actionbtn_save">
                                <button type='submit' className="btn butn butn_default">
                                  Save
                                </button>
                              </div>

                              <div className="actionbtn_back">
                                <button type="button" className="btn butn butn_back"
                                        onClick={() => browserHistory.push('/hubs')}>Back
                                </button>
                              </div>

                              <div className="actionbtn_delete">
                                <button type="button" className="btn butn butn_danger" data-toggle="modal"
                                        data-target="#hubDeleteConfirm"
                                >Delete
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>

                    </div>

                  </div>
                </div>
              </div>
            </div>

            : this.loadingUI()
        }

        {/* User delete confirmation modal */}
        <div
          id='hubDeleteConfirm'
          className='modal fade delprompt'
          tabIndex='-1'
          role='dialog'>
          <div className='modal-dialog delprompt_dialog' role='document'>
            <div className='modal-content delprompt_content'>
              <div className='modal-body'>
                <div className="delprompt_text">
                  <p>Do you want to delete this zone?</p>
                </div>
              </div>
              <div className='modal-footer'>
                <button type="button" className="btn butn butn_danger" onClick={this.deleteHub.bind(this)}>Delete
                </button>
                <button type="button" className="btn butn butn_default" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default HubModifyComponent;
