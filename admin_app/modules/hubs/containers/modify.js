import React,{ Component} from 'react'
import HubModifyComponent from '../components/modify'
import {connect} from 'react-redux'
import { readHubAction, updateHubAction, deleteHubAction, getActiveDeliveryBoys, getActiveFoodProviders } from '../actions'
import toastr from 'toastr'
import {browserHistory} from 'react-router'

class HubModifyContainer extends Component {

  componentWillMount() {
    this.props.readHub(this.props.params._id);
	  this.props.getDeliveryBoys();
    this.props.getFoodProviders();
    console.log('....unmount ...', this.props);
  }

  componentWillUnmount() {
    this.props.clearHub();
  }

  updateHub(patch) {
    this.props.updateHub(this.props.params._id, patch, function(err, data) {
      if (data) {
        browserHistory.push('/hubs');
        toastr.success('Hub Updated Successfully!!!');
      }
      if(err){
        toastr.error(err);
      }
    });
  }

  deleteHub() {
    this.props.deleteHub(this.props.params._id, function (err, data) {
      if(data){
        browserHistory.push('/hubs');
        toastr.success('Hub Deleted Successfully!!!');
      }
      if(err){
        toastr.error(err);
      }
    })
  }

  render() {
    let shouldHubRender = (this.props.hubs && this.props.params._id == this.props.hubs.hub._id && this.props.googleLoaded && this.props.hubs.deliveryBoys);
	  console.log('hub md render', shouldHubRender, this.props.hubs.deliveryBoys, this.props.googleLoaded);
    return(
	    shouldHubRender ?
		    <HubModifyComponent
			    hubs={this.props.hubs}
			    hub={this.props.hubs.hub}
			    updateHub={this.updateHub.bind(this)}
			    deleteHub={this.deleteHub.bind(this)}
			    savePolygon={this.props.savePolygon}
			    selectDb={this.props.selectDb}
			    unselectDb={this.props.unselectDb}
			    populateDbs={this.props.populateDbs}
          foodProviders={this.props.hubs.foodProviders}
          dBoys={this.props.hubs.deliveryBoys}
		    /> :
		    <h2>Loading...</h2>
    );
  }
}
//deleteHub={() => {this.props.deleteHub(this.props.params._id)}}
const mapStateToProps = (store) => {
  return {
    googleLoaded: store.auth.googleLoaded,
    hubs: store.hubs
    // newArea: store.hubs.drawingArea
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
	  populateDbs: (dbs) => dispatch({type:'POPULATE_SELECT_DB_HUB',payload:dbs}),
	  getDeliveryBoys: () => dispatch(getActiveDeliveryBoys()),
    getFoodProviders: () => dispatch(getActiveFoodProviders()),
    savePolygon: (area) => dispatch({type:'SAVE_AREA_POLYGON',payload:area}),
	  unselectDb: (dbId) => dispatch({type:'UNSELECT_DB_HUB', payload:dbId}),
	  selectDb: (db) => dispatch({type:'SELECT_DB_HUB', payload:db}),
    readHub: (_id) => dispatch(readHubAction(_id)),
    updateHub: (_id,patch,cb) => dispatch(updateHubAction(_id,patch,cb)),
    deleteHub: (_id,cb) => dispatch(deleteHubAction(_id,cb)),
    clearHub: () => dispatch({type:'CLEAR_HUB'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(HubModifyContainer)
