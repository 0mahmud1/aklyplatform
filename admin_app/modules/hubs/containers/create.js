'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import toastr from 'toastr'
import _ from 'lodash'

import { createHubAction, getActiveDeliveryBoys, getActiveFoodProviders } from '../actions/index'
import HubCreateComponent from '../components/create'

class HubCreateContainer extends Component {

	componentWillMount() {
		this.props.getDeliveryBoys();
		this.props.getFoodProviders();
	}


  componentDidMount() {
    let reactDOM = this;

	  //let reactDOM = this;
	  let select2settings = {
		  tags: true,
		  // placeholder: 'Select a db',
		  tokenSeparators: [','],
	  };

	  $('#deliveryBoy').select2(select2settings).on('select2:select',(evt) => {
		  console.log('selected ',evt);
		  // evt.params.data.id
		  let selectedDb = _.find(reactDOM.props.hubs.deliveryBoys, function (deliveryBoy) {
			  return deliveryBoy._id == evt.params.data.id
		  });

		  //console.log('db found ', selectedDb);
		  reactDOM.props.selectDb(selectedDb);

	  }).on('select2:unselect',(evt) => {
		  console.log('unselect: ', evt);
		  reactDOM.props.unselectDb(evt.params.data.id);
	  });

		$('#foodProvider').select2(select2settings).on('select2:select',(evt) => {
		  console.log('selected ',evt);
		  // evt.params.data.id
		  let selectedFp = _.find(reactDOM.props.hubs.foodProviders, function (fp) {
			  return fp._id == evt.params.data.id
		  })

		  //console.log('db found ', selectedDb);
		  reactDOM.props.selectFp(selectedFp);

	  }).on('select2:unselect',(evt) => {
		  console.log('unselect: ', evt);
		  reactDOM.props.unselectFp(evt.params.data.id);
	  });


    let markers = [],
      polygon = [];
    let map = new google.maps.Map(document.getElementById('hub-map'), {
      center: { lat: 25.27932, lng: 51.52245 },
      zoom: 12
    });
    google.maps.event.addListenerOnce(map, 'idle', function () {
      // do something only the first time the map is loaded
      console.log(map.getBounds());

    });

    let input = document.getElementById('search-place');
    let searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    let drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT,
        drawingModes: ['polygon']
      },
      markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
    });
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
      console.log('drawing event fired: ', event);
      if (event.type == 'polygon') {
        let areas = event.overlay.latLngs.b[0].b;
        console.log('area line length: ', areas.length);
        areas.forEach(function (point) {
          console.log(point.lat(),' ', point.lng());
          //let LatLng = {lat:point.lat(), lng: point.lng()};
          polygon.push([point.lng(), point.lat()]);
          console.log('pushing');
        })

        polygon.push([areas[0].lng(), areas[0].lat()])

        reactDOM.props.savePolygon(polygon);
      }
    });
    // google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
    //   console.log('polygon:...', polygon)
    // });

    map.addListener('bounds_changed', function() {
      console.log('bound changes fired ..');
      searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces()[0]; //WE ARE ONLY CONCERN ABOUT THE FIRST ELEMENT
      console.log('select primary area   ', places);

      if (places.length == 0) {
        return;
      }

      // changing center to get a better view of the desired location where area will be drawn.
      map.setCenter(places.geometry.location);

      // Clear out the old markers.
      /*markers.forEach(function (marker) {
       marker.setMap(null);
       });
       markers = [];

       // For each place, get the icon, name and location.
       var bounds = new google.maps.LatLngBounds();
       places.forEach(function (place) {
       if (!place.geometry) {
       console.log("Returned place contains no geometry");
       return;
       }
       // var icon = {
       //   url: place.icon,
       //   size: new google.maps.Size(71, 71),
       //   origin: new google.maps.Point(0, 0),
       //   anchor: new google.maps.Point(17, 34),
       //   scaledSize: new google.maps.Size(25, 25)
       // };

       // Create a marker for each place.
       markers.push(new google.maps.Marker({
       map: map,
       title: place.name,
       position: place.geometry.location
       }));

       if (place.geometry.viewport) {
       // Only geocodes have viewport.
       bounds.union(place.geometry.viewport);
       } else {
       bounds.extend(place.geometry.location);
       }
       });
       map.fitBounds(bounds);*/
    });

  }

  createHub(hub) {
    console.log('in container ...', hub);
    this.props.createHub(hub, function(err, data){
      if(data){
        browserHistory.push('/hubs');
        toastr.success('Hub Created Successfully!!');
      }
      if(err){
        toastr.error(err);
      }
    });
    console.log('in container execution ...');
  }


  render() {
    return(
      <HubCreateComponent
        polygon = {this.props.hubs.drawingArea}
        dbs={this.props.hubs.selectedDbs}
				fps={this.props.hubs.selectedFps}
        createHub={this.createHub.bind(this)}
        deliveryBoys={this.props.hubs.deliveryBoys}
				foodProviders={this.props.hubs.foodProviders}
      />
    );
  }
}


const mapStateToProps = (store) => {
  return {
	  hubs: store.hubs,
	  //polygon: store.hubs.drawingArea
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
	  unselectDb: (dbId) => dispatch({type:'UNSELECT_DB_HUB', payload:dbId}),
	  selectDb: (db) => dispatch({type:'SELECT_DB_HUB', payload:db}),
		unselectFp: (fpId) => dispatch({type:'UNSELECT_FP_HUB', payload:fpId}),
	  selectFp: (fp) => dispatch({type:'SELECT_FP_HUB', payload:fp}),
    savePolygon: (area) => dispatch({type:'SAVE_AREA_POLYGON',payload:area}),
	  getDeliveryBoys: () => dispatch(getActiveDeliveryBoys()),
		getFoodProviders: () => dispatch(getActiveFoodProviders()),
    createHub: (hub, cb) => dispatch(createHubAction(hub, cb))
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(HubCreateContainer)
