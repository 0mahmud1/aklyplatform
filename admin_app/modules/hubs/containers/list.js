'use strict'

import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getHubsAction} from '../actions/index'
import HubListComponent from '../components/list'

class HubListContainer extends Component {
  componentWillMount() {
    this.props.getHubs();
  }

  render() {
    return this.props.hubs ?
      <HubListComponent
        hubs={this.props.hubs}
      /> : null


  }
}

/*HubListContainer.propTypes = {
 // This component gets the task to display through a React prop.
 // We can use propTypes to indicate it is required
 getHubs: PropTypes.func.isRequired,
 hubs: PropTypes.array.isRequired
 }*/

// Get apps store and pass it as props to HubListContainer
//  > whenever store changes, the HubListContainer will automatically re-render
const mapStateToProps = (store) => {
  return {
    hubs: store.hubs.hubList,
    totalData: store.hubs.totalData
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHubs: () => dispatch(getHubsAction())
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(HubListContainer)