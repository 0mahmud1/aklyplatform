'use strict'

import * as constants from '../constants';

/*
 * This reducer will always return an array of categories no matter what
 * You need to return something, so if there are no categories then just return an empty array
 */

const defaultState = {
	hub: {},
	selectedDbs: [],
	deliveryBoys: [],
	foodProviders: [],
	selectedFps: []
};


const HubsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'SAVE_AREA_POLYGON':
      return Object.assign({}, state, { drawingArea: action.payload });

	  case 'SELECT_DB_HUB':
		  return Object.assign({}, state, { selectedDbs: [...state.selectedDbs, action.payload] });

		case 'SELECT_FP_HUB':
		  return Object.assign({}, state, { selectedFps: [...state.selectedFps, action.payload] });

	  case 'POPULATE_SELECT_DB_HUB':
		  return Object.assign({}, state, { selectedDbs: action.payload });

	  case 'UNSELECT_DB_HUB':
		  _.remove(state.selectedDbs, (db) => {
			  return db._id == action.payload;// we only pass unselected _id here.
		  });
		  //console.log('after unselect', newDbs);
		  return Object.assign({}, state, {selectedDbs: state.selectedDbs });


		case 'UNSELECT_FP_HUB':
		  _.remove(state.selectedFps, (fp) => {
			  return fp._id == action.payload;// we only pass unselected _id here.
		  });
		  //console.log('after unselect', newDbs);
		  return Object.assign({}, state, { selectedFps: state.selectedFps });

		case 'CREATE_HUB_RESOLVED':
			return Object.assign({}, state, { selectedFps: null, selectedDbs: null });

    case constants.READ_HUB_RESOLVED:
      return Object.assign({}, state, { hub: action.payload, drawingArea: action.payload.locations.coordinates[0] });

    case constants.GET_HUB_RESOLVED:
      return Object.assign({}, state, { hubList: action.payload });

	  case constants.GET_DELIVERYBOY_RESOLVED:
		  return Object.assign({}, state, { deliveryBoys: action.payload });

		case constants.GET_FOODPROVIDER_RESOLVED:
		  return Object.assign({}, state, { foodProviders: action.payload });
		case 'CLEAR_HUB':
			return defaultState;
    default:
      return state
  }
};

export default HubsReducer
