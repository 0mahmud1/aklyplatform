export function getCenterOfPolygon(polygon) {
  var bounds = new google.maps.LatLngBounds();
  var i;

// The Bermuda Triangle
  var polygonCoords = [
    new google.maps.LatLng(25.774252, -80.190262),
    new google.maps.LatLng(18.466465, -66.118292),
    new google.maps.LatLng(32.321384, -64.757370),
    new google.maps.LatLng(25.774252, -80.190262)
  ];

  for (i = 0; i < polygonCoords.length; i++) {
    bounds.extend(polygonCoords[i]);
  }

// The Center of the Bermuda Triangle - (25.3939245, -72.473816)
  console.log(bounds.getCenter());
}