'use strict'

import React, { Component } from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import DeliveryLayout from '../layout'
import DeliveryListContainer from '../../orders/containers/list';
import DeliveryDetailsContainer from '../containers/deliveryDetails'
import * as AuthService from '../../services/auth';

export default function () {
    return (
        <Route path='deliveries' component={DeliveryLayout} onEnter={AuthService.isDashAuthRouter}>
            <IndexRoute component={DeliveryListContainer} />
            {<Route path=':_id' component={DeliveryDetailsContainer}/>}
        </Route>
    )
}
