'use strict'

import React, { Component, PropTypes } from 'react'

class DeliveryLayout extends Component {
    render() {
        console.log('DeliveryLayout...render');
        return (
            <div className="mp_wrapper akly_mp" data-hook="mainPageWrapper">
                <div className="mp_content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

DeliveryLayout.propTypes = {
    // This component gets the task to display through a React prop.
    // We can use propTypes to indicate it is required
    children: PropTypes.element.isRequired
}

export default DeliveryLayout
