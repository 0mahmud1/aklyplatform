'use strict';

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import moment from 'moment'
import toastr from 'toastr'
import _ from 'lodash'
import Rodal from 'rodal'
import $ from 'jquery'

class DeliveryDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cancelModalShow: false
    };

    this.props.updateDeliveryStatus.bind(this);

    this.toggleModal = this.toggleModal.bind(this);
  }

  renderVanOrDeliveryBoy() {
    if (this.props.delivery.van)
      return this.renderVan();
    if (this.props.delivery.deliveryBoy)
      return this.renderdBoy()

    return this.props.delivery.orderType == 'onDemand' ?
      <span style={{ 'color': 'red' }}>Delivery Boy not decided yet</span> : null
  }

  toggleModal() {
    this.setState(
      {
        cancelModalShow: !this.state.cancelModalShow
      }
    )

  }

  updateStatus() {
    this.toggleModal();
    this.props.updateDeliveryStatus(this.props.delivery._id);
  }

  cancelModal() {
    return <Rodal visible={this.state.cancelModalShow} onClose={this.toggleModal} height={160} className="custstyle_modal">
      <div className='binary_prompt_content'>
        <div className='prompt_text'>
          <p>Are you sure to cancel this order?</p>
        </div>
        <div className='prompt_btn'>
          <button className='btn butn butn_default' onClick={this.updateStatus.bind(this)}>
            <span>Ok</span>
          </button>
          <button className='btn butn butn_danger' onClick={this.toggleModal} >
            <span>No</span>
          </button>
        </div>
      </div>
    </Rodal>
  }
  renderVan() {
    return (
      <div>
        <div className="mp_list_mgn">
          <div className="mp_heading">
            <h3>Van</h3>
          </div>
        </div>

        <div className='akly_menulisttbl akly_listbl'>
          <div className='row'>
            <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
              <div className='table-responsive'>
                <table className='table table-bordered akly_admintable akly_admintable_gen'>
                  <tbody>
                    <tr>
                      <td>Name</td>
                      <td>{this.props.delivery.van.name}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>{this.props.delivery.van.email}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderdBoy() {
    return (
      <div>
        <div className="mp_list_mgn">
          <div className="mp_heading">
            <h3>DeliveryBoy</h3>
          </div>
        </div>

        <div className='akly_menulisttbl akly_listbl'>
          <div className='row'>
            <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
              <div className='table-responsive'>
                <table className='table table-bordered akly_admintable akly_admintable_gen'>
                  <tbody>
                    <tr>
                      <td>Name</td>
                      <td>{this.props.delivery.deliveryBoy.name}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>{this.props.delivery.deliveryBoy.email}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {

    let statusText = "";
    if (this.props.delivery.status === 'pending') {
      statusText = "text-warning";
    } else if (this.props.delivery.status === 'completed') {
      statusText = "text-success";
    } else if (this.props.delivery.status === 'cancel') {
      statusText = "text-danger";
    } else if (this.props.delivery.status === 'ready') {
      statusText = "text-primary";
    } else if (this.props.delivery.status === 'onTheWay') {
      statusText = "text-info";
    }

    const headerStyle = {
      display: 'inline-block',
      verticalAlign: 'middle',
      marginRight: '15px'
    };
    return (
      <div>
        <div className="mp_heading_mb">
          <div className="row">
            <div className="col-lg-12">
              <div className="mp_heading">
                <h3 style={headerStyle}>Delivery Details</h3>
                {
                  <select id="select" className="delivarystatus-select">
                    {this.generateOption()}
                  </select>
                }
                {
                  <span>
                    <button type='button' className='btn butn_default update-btn' onClick={this.updateStatusByAdmin.bind(this)}>
                      Update Delivery
                      </button>
                  </span>
                }

              </div>
            </div>
          </div>
        </div>
        <div className="mp_body">
          <div className="row">
            <div className="col-lg-12">
              <div className="delivery_details">
                {this.props.delivery
                  ?
                  <div>
                    <div className='akly_menulisttbl akly_listbl'>
                      <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                          <div className='table-responsive'>
                            <table className='table table-bordered akly_admintable akly_admintable_gen'>
                              <tbody>
                                <tr>
                                  <td>Order Type</td>
                                  <td>{this.props.delivery.orderType}</td>
                                </tr>
                                <tr>
                                  <td>Delivery Date</td>
                                  <td>{moment(this.props.delivery.deliveryDate).format('ll')}</td>
                                </tr>
                                <tr>
                                  <td>Delivery RefId</td>
                                  <td>{this.props.delivery.deliveryRefId}</td>
                                </tr>
                                <tr>
                                  <td>Quantity</td>
                                  <td>{this.props.delivery.quantity}</td>
                                </tr>
                                <tr>
                                  <td>Shift</td>
                                  <td>{this.props.delivery.shift}</td>
                                </tr>
                                <tr>
                                  <td>Status</td>
                                  <td id="status"><span className={statusText}>{this.props.delivery.status}</span></td>
                                </tr>
                                <tr>
                                  <td>Sub Total</td>
                                  <td>{this.props.delivery.subTotal}</td>
                                </tr>
                                <tr>
                                  <td>Total</td>
                                  <td>{this.props.delivery.total}</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="mp_list_mgn">
                      <div className="mp_heading">
                        <h3>Customer</h3>
                      </div>
                    </div>

                    <div className='akly_menulisttbl akly_listbl'>
                      <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                          <div className='table-responsive'>
                            <table className='table table-bordered akly_admintable akly_admintable_gen'>
                              <tbody>
                                <tr>
                                  <td>Name</td>
                                  <td>{this.props.delivery.createdBy.name ? this.props.delivery.createdBy.name : 'No name provided'}</td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>{this.props.delivery.createdBy.email}</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                    {
                      this.props.delivery.provider ?
                        <div>
                          <div className="mp_list_mgn">
                            <div className="mp_heading">
                              <h3>Food Provider</h3>
                            </div>
                          </div>
                          <div className='akly_menulisttbl akly_listbl'>
                            <div className='row'>
                              <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                                <div className='table-responsive'>
                                  <table className='table table-bordered akly_admintable akly_admintable_gen'>
                                    <tbody>
                                      <tr>
                                        <td>Name</td>
                                        <td>{this.props.delivery.provider.name ? this.props.delivery.provider.name : 'No name provided'}</td>
                                      </tr>
                                      <tr>
                                        <td>Email</td>
                                        <td>{this.props.delivery.provider.email}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> : null
                    }
                    {this.renderVanOrDeliveryBoy()}

                    <div className="mp_list_mgn">
                      <div className="mp_heading">
                        <h3>Food List</h3>
                      </div>
                    </div>

                    <div className='akly_menulisttbl akly_listbl'>
                      <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                          <div className='table-responsive'>
                            <table className='table table-bordered akly_admintable akly_admintable_gen'>
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Price</th>
                                  <th>Quantity</th>
                                  <th>Total</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.renderList()}
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td><b>Total</b></td>
                                  <td>{this.generateSum()}</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  : <p><b><i>Loading.......</i></b></p>
                }
              </div>
            </div>
          </div>
        </div>
        {this.cancelModal()}
      </div>
    )
  }

  //returns sum of price in all cells in Food List
  generateSum() {
    return _.sum(this.props.delivery.meals.map((food) => {
      return food.item.price * food.quantity
    })
    )
  }
  updateStatusByAdmin() {
    let status = $("#select").val()
    // console.log('updateStatusByAdmin', status)
    this.props.updateStatusByAdmin(this.props.delivery._id, status)
    $("#status").text(status)
  }
  generateOption() {
    return ['pending', 'cooking', 'ready', 'onTheWay', 'accepted', 'denied', 'completed', 'cancel', 'Archived'].map((status) => {
      if (status === this.props.delivery.status) {
        return <option key={status} value={status} selected>{status}</option>
      } else {
        return <option key={status} value={status}>{status}</option>
      }
    })
  }

  renderList() {
    return this.props.delivery.meals.map((food) => {
      return (
        <tr
          key={food.item._id}
          onClick={() => browserHistory.push('/foods/' + food.item._id)}>
          <td>{food.item.name}</td>
          <td>{food.item.price}</td>
          <td>{food.quantity}</td>
          <td>{food.item.price * food.quantity}</td>
        </tr>
      )
    })
  }
}

DeliveryDetailsComponent.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  delivery: PropTypes.object
};

export default DeliveryDetailsComponent