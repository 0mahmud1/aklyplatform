import * as constants from '../constants'

let defaultState = {
    delivery: {},
    deliveryList: []
};

const deliveryReducer = (state = defaultState, action) => {
    switch (action.type) {
        case constants.GET_DELIVERY_PENDING:
            return state;
        case constants.GET_DELIVERY_RESOLVED:
            // console.log("inside reducer",action.payload.data.data);
            return Object.assign({}, state, {delivery: action.payload.data.data});
        case constants.GET_DELIVERY_REJECTED:
            // console.log(action.payload.data.data);
            return Object.assign({}, state, {delivery: action.payload.data.data});

        case constants.GET_DELIVERIES_PENDING:
            return state;
        case constants.GET_DELIVERIES_RESOLVED:
            // console.log("inside reducer",action.payload.data.data);
            return Object.assign({}, state, {deliveryList: action.payload.data.data});
        case constants.GET_DELIVERIES_REJECTED:
            // console.log(action.payload.data.data);
            return Object.assign({}, state, {deliveryList: action.payload.data.data});
        default :
            return state;
    }
};


export default deliveryReducer;