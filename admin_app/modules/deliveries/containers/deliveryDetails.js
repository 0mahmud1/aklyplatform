'use strict'

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import toastr from 'toastr'

import { readDelivery, updateDeliveryStatus, updateStatusByAdmin } from '../actions/index';

import DeliveryDetailsComponent from '../components/deliveryDetails';

class DeliveryDetailsContainer extends Component{

    componentWillMount() {
        this.props.readDelivery(this.props.params._id);
    }

    loadingUI() {
        return (
            <div>
                <h1>Loading.... </h1>
            </div>
        )
    }
    updateDeliveryStatus(_id) {
        console.log('updateDeliveryStatus  shad');
        console.log(_id, this);
        this.props.updateDeliveryStatus(_id, function (err, data) {
            if (data) {
                browserHistory.push('/orders');
                toastr.success(' updated successfully!!');
            }
            if (err) {
                toastr.error(err);
            }
        });
    }

    render() {
        let shouldRender = (this.props.delivery && this.props.params._id == this.props.delivery._id);
        return(
            shouldRender ?
                <DeliveryDetailsComponent
                    delivery={this.props.delivery}
                    updateDeliveryStatus={this.updateDeliveryStatus.bind(this)}
                    updateStatusByAdmin={this.props.updateStatusByAdmin}
                />
                : this.loadingUI()
        )

    }
}


function mapStateToProps (store) {
    return {
        delivery: store.deliveries.delivery
    }
}

// Get actions and pass them as props to to DBoysListContainer
//  > now DBoysListContainer has this.props.getDBoys
// function mapDispatchToProps (dispatch) {
// 	return bindActionCreators({
// 		readUser: readUser,
// 		updateUser: updateUser,
// 		deleteUser: deleteUser
// 	}, dispatch)
// }

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators ({
        readDelivery: readDelivery,
        updateDeliveryStatus: updateDeliveryStatus,
        updateStatusByAdmin:updateStatusByAdmin
    }, dispatch)
};

// We don't want to return the plain UserListContainer (component) anymore,
// we want to return the smart Container
//  > UserListContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(DeliveryDetailsContainer)
