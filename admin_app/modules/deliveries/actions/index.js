import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';

// export function getDeliveries(orderRefId) {
//     return function (dispatch) {
//         dispatch({type: constants.GET_DELIVERIES_PENDING});
//
//         axios.get(API_URL + 'api/deliveries?orderRefId='+orderRefId)
//             .then((response) => {
//                 console.log('get deliveries... : ', response);
//                 dispatch({type: constants.GET_DELIVERIES_RESOLVED, payload: response});
//                 toastr.success('Deliveries List Loaded');
//             })
//             .catch((err) => {
//                 dispatch({type: constants.GET_DELIVERIES_REJECTED, payload: err});
//                 toastr.error(err);
//             });
//     }
// }

export function readDelivery(_id) {
  return function (dispatch) {
    dispatch({ type: constants.GET_DELIVERY_PENDING });
    axios.get('api/deliveries/' + _id)
      .then((response) => {
        console.log('read a delivery... : ', response);
        dispatch({ type: constants.GET_DELIVERY_RESOLVED, payload: response });
        //toastr.success('Delivery details Loaded');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_DELIVERY_REJECTED, payload: err });
        toastr.error("Delivery not found");
      });
  };
}

export function updateDeliveryStatus(_id, cb) {
  return function (dispatch) {
    console.log('... in action ', _id);
    dispatch({ type: constants.UPDATE_DELIVERY_STATUS_PENDING });
    axios.put('api/deliveries/status/' + _id)
      .then((response) => {
        dispatch({ type: constants.UPDATE_DELIVERY_STATUS_RESOLVED });
        if (cb) {
          cb(null, { type: constants.UPDATE_DELIVERY_STATUS_RESOLVED });
        }
      })
      .catch((err) => {
        dispatch({ type: constants.UPDATE_DELIVERY_STATUS_REJECTED, payload: err });
        if (cb) {
          cb(err, null);
        }
      });
  };
}

export function updateStatusByAdmin(_id, status) {
    return function (dispatch) {
        dispatch({ type: constants.UPDATE_DELIVERY_STATUS_PENDING })
        axios.put('api/deliveries/status/' + _id, {status:status})
            .then((response) => {
                dispatch({ type: constants.UPDATE_DELIVERY_STATUS_RESOLVED });
                toastr.success(' updated successfully!!');
            })
            .catch((err) => {
                dispatch({ type: constants.UPDATE_DELIVERY_STATUS_REJECTED, payload: err });
                toastr.error('can not update status !!');
            });
    }

}
