'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import toastr from 'toastr'
import _ from 'lodash'
import {getFoods} from '../../foods/actions'
import {getCategories} from '../../categories/actions'
import {readMealPlanAction, updateMealPlanAction, deleteMealPlanAction} from '../actions'

import MealPlanEditComponent from '../components/edit'

class MealPlanEditContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      loader: false
    }
  }
  componentWillMount() {
    this.props.readMealPlanAction(this.props.params._id)
    this.props.getFoods('all',1);
    this.props.getCategories();
  }

  componentWillUnmount() {
    this.props.resetReducer();
  }

  updateMealPlan(_id, patch, cb){
      let _this = this;
      console.log(patch);
      _this.setState({loader : true});
      _this.props.updateMealPlan(_id, patch, function(err, data){
          if(data){
            _this.setState({loader : false});
            cb(null, data);
          } else {
            cb(err, null);
          }
      });
  }
  deleteMealPlan(){
    this.props.deleteMealPlan(this.props.params._id, (err, data)=>{
      if(data){
          browserHistory.push('/mealplans');
          toastr.success('Meal Plan deleted successfully!');
      }
      if(err){
        toastr.success(err)
      }
    });
  }
  render() {
    // console.group('edit meal plan render');
    // console.log(this.props);
    // console.groupEnd();
    const shouldRender = (!_.isEmpty(this.props.mealplans.mealPlan) && this.props.categories.categoryList) && !this.state.loader;
    console.log("MealPlanEditContainer...shouldRender=>>>",shouldRender);
    return (
      shouldRender ?
      <MealPlanEditComponent
        foodList={this.props.foods.foodList}
        categoryList={this.props.categories.categoryList}
        pushItem={this.props.pushItem}
        mealBuilder={this.props.mealBuilder}
        mealplans={this.props.mealplans}
        searchFood={this.props.searchFood}
        searchResult={this.props.foods.searchResult}
        pushQuickList={this.props.pushQuickList}
        removeQuickList={this.props.removeQuickList}
        quickList={this.props.foods.quickList}
        changeModalContent={this.props.changeModalContent}
        modalContent={this.props.mealplans.modalContent}
        increaseItem={this.props.increaseItem}
        reduceItem={this.props.reduceItem}
        updateMealPlan={this.updateMealPlan.bind(this)}
        deleteMealPlan={this.deleteMealPlan.bind(this)}/>
        :
        <div className="spinner"></div>
    )
  }


}


function mapStateToProps(store) {
  return {
    foods: store.foods,
    mealBuilder: store.mealPlans.mealBuilder,
    mealplans: store.mealPlans,
    categories: store.categories
  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getFoods: (limit,page) => dispatch(getFoods(limit,page)),
    getCategories: () => dispatch(getCategories()),
    pushItem: (data) => dispatch({type: 'PUSH_ITEM', payload: data}),
    pushQuickList: (food) => dispatch({type: 'PUSH_QUICK_LIST', payload: food}),
    searchFood: (data) => dispatch({type: 'SEARCH_FOOD', payload: data}),
    changeModalContent: (food) => dispatch({type: 'MODAL_CONTENT', payload: food}),
    removeQuickList: (food) => dispatch({type: 'REMOVE_ITEM_FROM_QUICK_LIST', payload: food}),
    increaseItem: (data) => dispatch({type:'INCREASE_ITEM', payload: data}),
    reduceItem: (data) => dispatch({type: 'REDUCE_ITEM', payload: data}),
    readMealPlanAction: (_id) => dispatch(readMealPlanAction(_id)),
    updateMealPlan: (_id, patch,cb) => dispatch(updateMealPlanAction(_id, patch,cb)),
    deleteMealPlan: (_id, callback) => dispatch(deleteMealPlanAction(_id, callback)),
    resetReducer: () => dispatch({type: 'RESET_MEALPLAN_REDUCER'})
  }
}


// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(mapStateToProps, mapDispatchToProps)(MealPlanEditContainer)
