'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import toastr from 'toastr'
import {getFoods} from '../../foods/actions'
import {getCategories} from '../../categories/actions'
import {createMealPlanAction} from '../actions'

import MealPlanCreateComponent from '../components/create'

import LoaderBox from '../../core/components/loader'

class MealPlanCreateContainer extends Component {

  componentWillMount() {
    this.props.getFoods('all',1);
    this.props.getCategories();
  }

  render() {
    return (
      <MealPlanCreateComponent
        foodList={this.props.foods.foodList}
        categoryList={this.props.categories.categoryList}
        pushItem={this.props.pushItem}
        selectWeek={this.props.selectWeek}
        setMealPlanType={this.props.setMealPlanType}
        mealBuilder={this.props.mealBuilder}
        searchFood={this.props.searchFood}
        searchResult={this.props.foods.searchResult}
        pushQuickList={this.props.pushQuickList}
        quickList={this.props.foods.quickList}
        removeQuickList={this.props.removeQuickList}
        changeModalContent={this.props.changeModalContent}
        modalContent={this.props.mealplans.modalContent}
        reduceItem={this.props.reduceItem}
        increaseItem={this.props.increaseItem}
        createMealPlan ={this.createMealPlan.bind(this)}
      />
    )
  }

  createMealPlan(mealPlan){
    console.log('inside container -> createVan');
    this.props.createMealPlan(mealPlan, function (err, data) {
      if (data) {
        browserHistory.push('/mealplans');
        toastr.success('Meal Plan created Successfully.......!');
      }
      if (err) {
        toastr.error(err.data.message);
      }
    });
  }

}


function mapStateToProps(store) {
  return {
    foods: store.foods,
    mealBuilder: store.mealPlans.mealBuilder,
    mealplans: store.mealPlans,
    categories: store.categories
  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getFoods: (limit,page) => dispatch(getFoods(limit,page)),
    getCategories: () => dispatch(getCategories()),
    selectWeek: (number) => dispatch({type: 'SELECT_WEEK_NUMBER', payload: number}),
    setMealPlanType: (data) => dispatch({type: 'SET_MEAL_PLAN_tYPE', payload: data}),
    pushItem: (data) => dispatch({type: 'PUSH_ITEM', payload: data}),
    pushQuickList: (food) => dispatch({type: 'PUSH_QUICK_LIST', payload: food}),
    removeQuickList: (food) => dispatch({type: 'REMOVE_ITEM_FROM_QUICK_LIST', payload: food}),
    searchFood: (data) => dispatch({type: 'SEARCH_FOOD', payload: data}),
    changeModalContent: (food) => dispatch({type: 'MODAL_CONTENT', payload: food}),
    reduceItem: (data) => dispatch({type: 'REDUCE_ITEM', payload: data}),
    increaseItem: (data) => dispatch({type:'INCREASE_ITEM', payload: data}),
    createMealPlan: (data, cb) => dispatch(createMealPlanAction(data, cb))
  }
}


// We don't want to return the plain TagCreateContainer (component) anymore,
// we want to return the smart Container
//  > TagCreateContainer is now aware of actions
export default connect(mapStateToProps, mapDispatchToProps)(MealPlanCreateContainer)
