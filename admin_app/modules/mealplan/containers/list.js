'use strict'

import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getMealPlans, getFilteredMealPlans} from '../actions/index'
import MealPlanListComponent from '../components/list'

import LoaderBox from '../../core/components/loader'


class MealPlanListContainer extends Component {
  componentWillMount() {
    this.props.getMealPlans(sessionStorage.getItem('mealPlanDataLimit'), sessionStorage.getItem('mealPlanCurrentPage'));
  }

  render() {    
    // const shouldRender = this.props.mealPlans.mealPlanList.length > 0 ? true: false
    return (
      // shouldRender ?
      <MealPlanListComponent
        getMealPlans={this.props.getMealPlans}
        getFilteredMealPlans={this.props.getFilteredMealPlans}
        totalPage={this.props.totalPage}
        total={this.props.total}
        mealPlans={this.props.mealPlans}/>
      // : <LoaderBox />
    )
  }
}

// MealPlanListContainer.propTypes = {
// This component gets the task to display through a React prop.
// We can use propTypes to indicate it is required
// getMealPlans: PropTypes.func.isRequired,
// mealPlans: PropTypes.object.isRequired
// }

// Get apps store and pass it as props to MealPlanListContainer
//  > whenever store changes, the MealPlanListContainer will automatically re-render
function mapStateToProps(store) {
  return {
    mealPlans: store.mealPlans,
    totalPage: store.mealPlans.totalPage,
    total: store.mealPlans.total
  }
}

// Get actions and pass them as props to to MealPlanListContainer
//  > now MealPlanListContainer has this.props.getMealPlans
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getMealPlans: getMealPlans,
    getFilteredMealPlans: getFilteredMealPlans
  }, dispatch)
}

// We don't want to return the plain MealPlanListContainer (component) anymore,
// we want to return the smart Container
//  > MealPlanListContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(MealPlanListContainer)
