import React, {Component, PropTypes} from 'react'
//import ItemTypes from './ItemTypes'
import { DragSource } from 'react-dnd'
import toastr from 'toastr';
import * as _ from 'lodash';

const dropDownItemSource = {
	beginDrag(props) {
		console.log('dragging started');
		return {
			name: props.name,
			food: props.food
		};
	},

	endDrag(props, monitor) {
		const item = monitor.getItem();
		const dropResult = monitor.getDropResult();

		console.log('get Item: ', monitor.getItem())
		console.log('props :' , props)

		if (dropResult) {
			// let text = `You dropped ${item.name} into ${dropResult.day} in ${dropResult.slot} slot!`;
			// toastr.success(text);
			props.pushItem({day: dropResult.day, slot:dropResult.slot, food: item.food})
		}
	}
};

@DragSource('box', dropDownItemSource, (connect, monitor) => ({
	connectDragSource: connect.dragSource(),
	isDragging: monitor.isDragging()
}))

class DraggableFoodItemComponent extends Component {
	static propTypes = {
		connectDragSource: PropTypes.func.isRequired,
		isDragging: PropTypes.bool.isRequired,
		name: PropTypes.string.isRequired
	};

	removeDraggableFoodItem(){
        this.props.removeQuickList(this.props.food)
	}

	render() {
		const { isDragging, connectDragSource } = this.props;
		const { name } = this.props;

		return (
			connectDragSource(
				<div className="col-lg-3" style={{ opacity: isDragging ? 0.5 : 1 }}>
					<div className="selected_food_wrapper">
						<div className="selected_food" data-toggle="modal" data-target=".aklyFoodDetailPrompt">
							<div className="food_info">
								<div className="food_thunmbnailimg">
									<img src={this.props.food.images[1]} />
								</div>
								<div className="food_name">
									<span>
										{this.props.food.name}
									</span>
								</div>
							</div>
							<div className="food_remove" onClick={this.removeDraggableFoodItem.bind(this)}>
								<div className="food_remove_closeicon" >
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet"><path d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z"/><path d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z"/></svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		);
	}
}

export default DraggableFoodItemComponent
