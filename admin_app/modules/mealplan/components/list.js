'use strict'

import React, {Component, PropTypes} from 'react'
import {browserHistory, Link} from 'react-router'
import PaginationComponent from '../../core/components/pagination'
import ReactTable from 'react-table'

class MealPlanListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum:0  ,
      pageSize: 20
    };
  }

  fetchData(state, instance) {
      this.setState({
          pageNum: state.page,
          pageSize: state.pageSize
      }, ()=>{
          this.props.getMealPlans(state.pageSize, state.page+1)
      })
  }

  heading() {
    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading'>
              <h3>Meal plans</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }

  searchBar() {
    return (
      <div className='mp_body'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='akly_menulist akly_list'>
              <div className='akly_listsearch-filter-add'>
                {/*<div className='akly_listsearch'>*/}
                  {/*<div className='sb'>*/}
                    {/*<div className='form-group'>*/}
                      {/*<label className='sb_label' htmlFor='exampleInputAmount'>Search</label>*/}
                      {/*<div className='input-group sb_inputgroup'>*/}
                        {/*<input type='search' className='form-control sb_formctrl' id='exampleInputAmount'/>*/}
                        {/*<div className='input-group-addon'>*/}
                          {/*<div className='sb_searchicon'>*/}
                            {/*<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 125'*/}
                                 {/*preserveAspectRatio='xMidYMid meet'>*/}
                              {/*<path*/}
                                {/*d='M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z'/>*/}
                            {/*</svg>*/}
                          {/*</div>*/}
                        {/*</div>*/}
                      {/*</div>*/}
                    {/*</div>*/}
                  {/*</div>*/}
                {/*</div>*/}
                <div className='akly_addnew' data-toggle='tooltip' data-placement='top' title='Add Menu'>
                  <Link to='/mealplans/createnew' className='addbtn'>
											<span className='icon_plus'>
												<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'
                             preserveAspectRatio='xMidYMid meet'>
													<path
                            d='M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z'/>
												</svg>
											</span>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  skeleton() {
    const columns = [
        {
            header: 'Name',
            accessor: 'name',
            sortable: false,
            filterRender: ({filter, onFilterChange}) => {
                return (
                    <input type='text'
                           placeholder="Search Meal Plan"
                           style={{
                               width: '100%'
                           }}
                           onChange={(event) => onFilterChange(event.target.value)}
                    />
                )
            },
            footer: (
                <span>
                    <strong>Total Meal Plan: </strong>{this.props.total}
                </span>
            )
        },
        {
            header: 'Calories',
            // id:'calories',
            accessor: 'calorie',
            sortable: false,
            //if all plans have calories field, accessor will be ok, to customize, render is used
            render: ({row}) =>{
                if (row.calorie) {
                    return (
                    <div>
                        {row.calorie+" calories"}
                    </div>)
                } else return (
                    <div>
                        Not Available
                    </div>
                )
            },
            hideFilter: true,
        },
        {
            header: 'Total Price',
            hideFilter: true,
            accessor: 'totalPrice',
            sortable: false,
            render: ({row}) =>{
                if (row.totalPrice) {
                    return (
                        <div>
                            {row.totalPrice+" QAR"}
                        </div>)
                } else return (
                    <div>
                        Not Available
                    </div>
                )
            }

        },
        {
            header: 'Plan Type',
            accessor: 'planType',
            sortable: false,
            hideFilter: true,
        }
    ]
    return (
      <div className='akly_menulisttbl akly_listbl'>
        <div className='row'>
          <div className='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
              <ReactTable
                  manual
                  className='-highlight custom-rt'
                  data={this.props.mealPlans.mealPlanList}
                  pages={this.props.totalPage}
                  onChange={this.fetchData.bind(this)}
                  columns={columns}
                  defaultPageSize={20}
                  minRows={0}
                  showFilters = {true}
                  onFilteringChange={(column, value) => {
                      if(value===''){
                          this.props.getMealPlans(this.state.pageSize, this.state.pageNum)
                      } else{
                          this.props.getFilteredMealPlans(value)
                      }
                  }}
                  style={{cursor: 'pointer'}}
                  getTrProps={(state, rowInfo, column, instance) => ({
                      onClick: e => {
                          // browserHistory.push('/mealplans/' + rowInfo.row._id)
                          window.open('/mealplans/' + rowInfo.row._id)
                      }
                  })}
              />
          </div>
        </div>
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.heading()}
        {this.searchBar()}
        {this.skeleton()}
        {/*<PaginationComponent current={this.state.currentPage} dataLimitStorage="mealPlanDataLimit" pageNumberStorage="mealPlanCurrentPage" totalPage={this.props.totalPage} onPageChange={this.changePageNumber.bind(this)} changeLimit={this.changeLimit.bind(this)}/>*/}
      </div>
    )
  }

}

// MealPlanListComponent.propTypes = {
// This component gets the task to display through a React prop.
// We can use propTypes to indicate it is required
// mealPlans: PropTypes.object.isRequired
// }

export default MealPlanListComponent
