import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { WithContext as ReactTags } from 'react-tag-input';
import toastr from 'toastr'
import { browserHistory } from 'react-router'
import Dropzone from 'react-dropzone';


export default class MealPlanCreateNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
          planType: localStorage.getItem('mealPlanType') ? localStorage.getItem('mealPlanType') : 'lunch',
          selectedDay: '',
          selectedMealTime: '',
          file: null,
          breakfastLists: [],
          lunchLists: [],
          dinnerLists: [],
          suggestions: []
        }
        this.searchMealItemClicked = this.searchMealItemClicked.bind(this);
        this.handleBreakfastDelete = this.handleBreakfastDelete.bind(this);
        this.handleBreakfastAddition = this.handleBreakfastAddition.bind(this);
        this.handleBreakfastDrag = this.handleBreakfastDrag.bind(this);
        this.handleLunchDelete = this.handleLunchDelete.bind(this);
        this.handleLunchAddition = this.handleLunchAddition.bind(this);
        this.handleLunchDrag = this.handleLunchDrag.bind(this);
        this.handleDinnerDelete = this.handleDinnerDelete.bind(this);
        this.handleDinnerAddition = this.handleDinnerAddition.bind(this);
        this.handleDinnerDrag = this.handleDinnerDrag.bind(this);
    }
    handleBreakfastDelete(i) {
        let breakfastLists = this.state.breakfastLists;
        breakfastLists.splice(i, 1);
        this.setState({breakfastLists: breakfastLists});
    }

    handleBreakfastAddition(tag) {
        let breakfastLists = this.state.breakfastLists;
        breakfastLists.push({
            id: breakfastLists.length + 1,
            text: tag
        });
        this.setState({breakfastLists: breakfastLists});
    }

    handleBreakfastDrag(tag, currPos, newPos) {
        let breakfastLists = this.state.breakfastLists;

        // mutate array
        breakfastLists.splice(currPos, 1);
        breakfastLists.splice(newPos, 0, tag);

        // re-render
        this.setState({ breakfastLists: breakfastLists });
    }
    handleLunchDelete(i) {
        let lunchLists = this.state.lunchLists;
        lunchLists.splice(i, 1);
        this.setState({lunchLists: lunchLists});
    }

    handleLunchAddition(tag) {
        let lunchLists = this.state.lunchLists;
        lunchLists.push({
            id: lunchLists.length + 1,
            text: tag
        });
        this.setState({lunchLists: lunchLists});
    }

    handleLunchDrag(tag, currPos, newPos) {
        let lunchLists = this.state.lunchLists;

        // mutate array
        lunchLists.splice(currPos, 1);
        lunchLists.splice(newPos, 0, tag);

        // re-render
        this.setState({ lunchLists: lunchLists });
    }
    handleDinnerDelete(i) {
        let dinnerLists = this.state.dinnerLists;
        dinnerLists.splice(i, 1);
        this.setState({dinnerLists: dinnerLists});
    }

    handleDinnerAddition(tag) {
        let dinnerLists = this.state.dinnerLists;
        dinnerLists.push({
            id: dinnerLists.length + 1,
            text: tag
        });
        this.setState({dinnerLists: dinnerLists});
    }

    handleDinnerDrag(tag, currPos, newPos) {
        let dinnerLists = this.state.dinnerLists;

        // mutate array
        dinnerLists.splice(currPos, 1);
        dinnerLists.splice(newPos, 0, tag);

        // re-render
        this.setState({ dinnerLists: dinnerLists });
    }

    MealPlanTitle() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Title</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <input type="text" className="form-control geninput_formctrl" ref='name' />
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    MealPlanShortDescription() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Short Description</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <input type="text" className="form-control geninput_formctrl" ref='description' />
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    generateCategoryList() {
      return this.props.categoryList && this.props.categoryList.length > 0 ?
        this.props.categoryList.map((category) =>
          <option key={category._id} value={category.name}>{category.name}</option>
        ) : null
    }

    MealPlanCategory() {
        return (
            <div className='col-lg-3'>
                <div className="geninput">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Categories</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <select className="form-control geninput_formctrl" id="categories-tags"
                        multiple="multiple">
                        {this.generateCategoryList()}
                      </select>
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    generatePlanType() {
        let planType=[
            {
                id: 1,
                value: 'lunch',
                name: 'Lunch Only'
            },

            {
                id: 2,
                value: 'fullDay',
                name: 'Full Day'
            }
        ];

        return planType.map(el => {
            return (
                <option  key={el.id} value={el.value}>{el.name}</option>
            )
        })
    }

    MealPlanType() {
        return (
            <div className='col-lg-3'>
                <div className="geninput mp-builder-select2">
                  <div className="form-group">
                    <label className="geninput_label" htmlFor="exampleInputAmount">Type</label>
                    <div className="input-group geninput_group geninput_group_add">
                      <select className="form-control geninput_formctrl" id="mealPlanType">
                        {this.generatePlanType()}
                      </select>
                    </div>
                  </div>
                </div>
            </div>
        );
    }

    totalCalorie() {
      let totalCalorie = 0;
      this.props.mealBuilder.map((mealPlan, index) => {
        // console.log('foods', mealPlan.foods)
        return mealPlan.foods.map(foodItem => {
          let cal = foodItem.item.calorie===''?0:foodItem.item.calorie
          totalCalorie = parseInt(totalCalorie) + (parseInt(cal) * foodItem.quantity);
        });
      });
      return totalCalorie;
    }
    MealPlanTotalCalorie() {
        return (
            <div className="col-lg-3">
                <div className="geninput text-center">
                    <div className="form-group">
                      <label className="geninput_label" htmlFor="exampleInputAmount" style={{display: 'block', textAlign: 'left'}}>Total Calorie</label>
                      <p className='input-group geninput_group geninput_group_add' style={{textAlign: 'left'}}>
                        {this.totalCalorie()} kCal
                      </p>
                    </div>
                </div>
            </div>
        );
    }

    totalPrice() {
      let totalPrice = 0;
      this.props.mealBuilder.map((mealPlan, index) => {
        // console.log('foods', mealPlan.foods)
        return mealPlan.foods.map(foodItem => {
          totalPrice = parseInt(totalPrice) + parseInt(foodItem.item.price * foodItem.quantity);
        });
      });
      return totalPrice;

    }

    MealPlanTotalPrice() {
        return (
            <div className="col-lg-3">
                <div className="geninput text-center">
                    <div className="form-group">
                      <label className="geninput_label" htmlFor="exampleInputAmount" style={{display: 'block', textAlign: 'left'}}>Total Price</label>
                      <p className='input-group geninput_group geninput_group_add' style={{textAlign: 'left'}}>
                        QR {this.totalPrice()}
                      </p>
                    </div>
                </div>
            </div>
        );
    }

    MealPlanSetPrice() {
        return (
            <div className="col-lg-3">
                <div className="geninput">
                    <div className="form-group">
                        <label className="geninput_label" htmlFor="exampleInputAmount">Set Price</label>
                        <div className="input-group geninput_group geninput_group_add">
                            <input type="text" className="form-control geninput_formctrl" ref='price' />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    generateTabs() {
        const TabNames = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

        return TabNames.map(el => {
            return (
                <Tab key={el}>{el}</Tab>
            )
        })
    }

    generateTableHeader() {
        const HeaderItems = ['Day', 'Breakfast', 'Lunch', 'Dinner'];
        const _this = this;
        return HeaderItems.map(el => {
            return (
                _this.state.planType === 'lunch' && (el === 'Breakfast' || el === 'Dinner') ?
                null
                :
                <th key={el} role={el.toLowerCase()}>{el}</th>
            )
        })
    }

    setDayAndMealTime(day,mealTime) {
      this.setState({
        selectedDay: day,
        selectedMealTime: mealTime
      })
    }

    addMealIcon(day,mealTime) {
        return (
            <span role='addmeal' data-toggle="modal" data-target=".aklyMealSearchPrompt" onClick={this.setDayAndMealTime.bind(this,day,mealTime)}>
                <i className='glyphicon glyphicon-plus'></i>
            </span>
        );
    }

    reduceItemQuantity(day,week,mealTime,food) {
      this.props.reduceItem({day: day, slot: mealTime, food: food, week: week})
    }

    increaseItemQuantity(day,week,mealTime,food) {
      this.props.increaseItem({day: day, slot: mealTime, food: food, week: week})
    }

    showSelectedFoods(day,mealTime) {
      const Styles = {
          textIndent: 0,
          textAlign: 'start',
          lineHeight: 'normal',
          textTransform: 'none',
          blockProgression: 'tb',
          InkscapeFontSpecification: 'Sans',
          color: '#000',
          overflow: 'visible',
          fontFamily: '"Sans"'
      };
      let week = parseInt(localStorage.getItem('selectedWeek'));
      let selectedDayFoods = _.find(this.props.mealBuilder, function (meal) {
        return meal.day == day && meal.slot == mealTime && meal.week == parseInt(localStorage.getItem('selectedWeek'))
      });
      return (
        selectedDayFoods && selectedDayFoods.foods.length > 0
        ?
        selectedDayFoods.foods.map((food) => {
          return <div className="added_food_card">
              <div className="food_info">
                  <div className="food_thunmbnailimg">
                      <img src={food.item.images[1]} />
                  </div>
                  <div className="food_name">
                      <p>{food.item.name}</p>
                  </div>
              </div>
              <div className="food_quantity text-center">
                  <div className="quantity_plus">
                      <div className="quantity_icon" onClick={this.increaseItemQuantity.bind(this,day,week,mealTime,food)} >
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"></path>
                          </svg>
                      </div>
                  </div>

                  <div className="quantity_total">
                      <p>{food.quantity}</p>
                  </div>
                  <div className="quantity_minus">
                      <div className="quantity_icon" onClick={this.reduceItemQuantity.bind(this,day,week,mealTime,food)} >
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet"><path d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z" style={{Styles}}></path>
                          </svg>
                      </div>
                  </div>
              </div>
          </div>
        }) : null
      )
    }

    generateTable() {
        return (
            <div className="meal_plan_view meal_plan_table">
                <div className="table-responsive">
                    <table className="table table-bordered akly_admintable akly_admintable_gen mp-builder-table">
                        <thead>
                            <tr>
                                {this.generateTableHeader()}
                            </tr>
                        </thead>

                        <tbody>
                            {this.generateTableRow()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

    generateTableRow() {
        let days = [];
        {
          this.state.planType === 'lunch' ?
          days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday']
          :
          days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday','saturday']
        }

        return days.map((day, index) => {
            console.log('generateTableRow', index);
            return (
                <tr key={index}>
                    <td role='day'  style={{width: '130px'}}>
                        <span>{day[0].toUpperCase() + day.slice(1)}</span>
                    </td>
                    {
                      this.state.planType === 'fullDay' ?
                      <td role='breakfast' style={{width: '350px'}}>
                          {this.addMealIcon(day,'breakfast')}
                          {this.showSelectedFoods(day,'breakfast')}
                      </td>
                      : null
                    }
                    <td role='lunch' style={{width: '350px'}}>
                        {this.addMealIcon(day,'lunch')}
                        {this.showSelectedFoods(day,'lunch')}
                    </td>
                    {
                      this.state.planType === 'fullDay' ?
                      <td role='dinner' style={{width: 'calc(100% - (130px + 350px + 350px))'}}>
                          {this.addMealIcon(day,'dinner')}
                          {this.showSelectedFoods(day,'dinner')}
                      </td>
                      : null
                    }

                </tr>
            )
        })
    }

    generateSubstitutionRow() {
      const { breakfastLists, lunchLists, dinnerLists, suggestions } = this.state;
      return (
        <div className="meal_plan_view meal_plan_table">
            <div className="table-responsive oxn">
                <table className="table table-bordered akly_admintable akly_admintable_gen mp-builder-table">
                    <thead>
                        <tr>
                          {
                            this.state.planType === 'fullDay' ?
                            <th>Breakfast</th>
                            : null
                          }
                          <th>Lunch</th>
                          {
                            this.state.planType === 'fullDay' ?
                            <th>Dinner</th>
                            : null
                          }
                        </tr>
                    </thead>

                    <tbody>
                      <tr >
                          {
                            this.state.planType === 'fullDay' ?
                            <td role='breakfast' style={{width: '350px'}}>
                              <ReactTags tags={breakfastLists}
                                  suggestions={suggestions}
                                  placeholder={'Add new food'}
                                  handleDelete={this.handleBreakfastDelete}
                                  handleAddition={this.handleBreakfastAddition}
                                  handleDrag={this.handleBreakfastDrag} />
                            </td>
                            : null
                          }
                          <td role='lunch' style={{width: '350px'}}>
                            <ReactTags tags={lunchLists}
                                suggestions={suggestions}
                                placeholder={'Add new food'}
                                handleDelete={this.handleLunchDelete}
                                handleAddition={this.handleLunchAddition}
                                handleDrag={this.handleLunchDrag} />
                          </td>
                          {
                            this.state.planType === 'fullDay' ?
                            <td role='dinner' style={{width: 'calc(100% - (130px + 350px + 350px))'}}>
                              <ReactTags tags={dinnerLists}
                                  suggestions={suggestions}
                                  placeholder={'Add new food'}
                                  handleDelete={this.handleDinnerDelete}
                                  handleAddition={this.handleDinnerAddition}
                                  handleDrag={this.handleDinnerDrag} />
                            </td>
                            : null
                          }

                      </tr>
                    </tbody>
                </table>
            </div>
        </div>

      )
    }

    searchMealItemClicked(food) {
        let week = parseInt(localStorage.getItem('selectedWeek'));
        this.props.pushItem({day: this.state.selectedDay.toLowerCase(), slot: this.state.selectedMealTime, food: food , week : week })
    }

    generateTabPanels() {
        const PanelItems = ['week1', 'week2', 'week3', 'week4'];

        return PanelItems.map(el => {
            return (
                <TabPanel key={el}>
                    {this.generateTable()}
                </TabPanel>
            )
        })
    }

    onDrop(files) {
      console.log('Received food files: ', files);
      this.setState({
        file: files[0]
      });
    }

    imageDropzoneContainer() {
      return (
        <div className="mb-25">
          <Dropzone
            ref="dropzone"
            onDrop={this.onDrop.bind(this)}
            className='dropzone-mpbuilder'
            >
            {this.state.file ?
              <div className="dropzone-mpbuilder-imgwrapper">
                <img src={this.state.file.preview} />
              </div>
              : <div className="text-center">Upload meal plan image</div>
            }
          </Dropzone>
        </div>
      )
    }

    actionButtons() {
      return (
        <div className="meal_plan_actionbtn actionbutn">
          <div className="row">
            <div className="col-md-12 col-lg-12">
              <div className="actionbtn_save">
                <button type="submit" className="btn butn butn_default">
                  {/* <span className="actionbtn_save_loader actionButnSaveLoader">
                   <svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><path fill="none" d="M0 0h100v100H0z"/><defs><filter id="a" x="-100%" y="-100%" width="300%" height="300%"><feOffset result="offOut" in="SourceGraphic"/><feGaussianBlur result="blurOut" in="offOut"/><feBlend in="SourceGraphic" in2="blurOut"/></filter></defs><path d="M10 50s0 .5.1 1.4c0 .5.1 1 .2 1.7 0 .3.1.7.1 1.1.1.4.1.8.2 1.2.2.8.3 1.8.5 2.8.3 1 .6 2.1.9 3.2.3 1.1.9 2.3 1.4 3.5.5 1.2 1.2 2.4 1.8 3.7.3.6.8 1.2 1.2 1.9.4.6.8 1.3 1.3 1.9 1 1.2 1.9 2.6 3.1 3.7 2.2 2.5 5 4.7 7.9 6.7 3 2 6.5 3.4 10.1 4.6 3.6 1.1 7.5 1.5 11.2 1.6 4-.1 7.7-.6 11.3-1.6 3.6-1.2 7-2.6 10-4.6 3-2 5.8-4.2 7.9-6.7 1.2-1.2 2.1-2.5 3.1-3.7.5-.6.9-1.3 1.3-1.9.4-.6.8-1.3 1.2-1.9.6-1.3 1.3-2.5 1.8-3.7.5-1.2 1-2.4 1.4-3.5.3-1.1.6-2.2.9-3.2.2-1 .4-1.9.5-2.8.1-.4.1-.8.2-1.2 0-.4.1-.7.1-1.1.1-.7.1-1.2.2-1.7.1-.9.1-1.4.1-1.4V54.2c0 .4-.1.8-.1 1.2-.1.9-.2 1.8-.4 2.8-.2 1-.5 2.1-.7 3.3-.3 1.2-.8 2.4-1.2 3.7-.2.7-.5 1.3-.8 1.9-.3.7-.6 1.3-.9 2-.3.7-.7 1.3-1.1 2-.4.7-.7 1.4-1.2 2-1 1.3-1.9 2.7-3.1 4-2.2 2.7-5 5-8.1 7.1L70 85.7c-.8.5-1.7.9-2.6 1.3l-1.4.7-1.4.5c-.9.3-1.8.7-2.8 1C58 90.3 53.9 90.9 50 91l-3-.2c-1 0-2-.2-3-.3l-1.5-.2-.7-.1-.7-.2c-1-.3-1.9-.5-2.9-.7-.9-.3-1.9-.7-2.8-1l-1.4-.6-1.3-.6c-.9-.4-1.8-.8-2.6-1.3l-2.4-1.5c-3.1-2.1-5.9-4.5-8.1-7.1-1.2-1.2-2.1-2.7-3.1-4-.5-.6-.8-1.4-1.2-2-.4-.7-.8-1.3-1.1-2-.3-.7-.6-1.3-.9-2-.3-.7-.6-1.3-.8-1.9-.4-1.3-.9-2.5-1.2-3.7-.3-1.2-.5-2.3-.7-3.3-.2-1-.3-2-.4-2.8-.1-.4-.1-.8-.1-1.2v-1.1-1.7c-.1-1-.1-1.5-.1-1.5z" filter="url(#a)"><animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" dur="0.7s"/></path></svg>
                   </span> */}
                  <span className="actionbtn_save_text">Create</span>
                </button>
              </div>
              <div className="actionbtn_cancel">
                <button type="button" className="btn butn butn_cancel"
                  onClick={() => browserHistory.push('/mealplans')}>Cancel
                </button>
              </div>
            </div>
          </div>
        </div>
      )
    }

    handleSearch(event) {
      event.preventDefault();
      this.props.searchFood(event.target.value);
    }

    handleTabSelect(index, last) {
      this.props.selectWeek(index);
    }

    renderSearchResult() {
      let shouldRender = (this.props.searchResult && this.props.searchResult.length > 0)
      console.log('should render ', shouldRender);
      return shouldRender ? this.props.searchResult.map((food) => {
        return (
          <li key={food._id} onClick={this.searchMealItemClicked.bind(this, food)}>
            <a href="#">
              <span className="food_thunmbnailimg">
                <img src={food.images[1]} />
              </span>
              <span className="food_name">
                {food.name}
              </span>
            </a>
          </li>
        )
      }): null
    }

    componentWillReceiveProps(nextProps) {
      const _this = this;
      if (nextProps.foodList && !_.isEqual(nextProps.foodList, this.state.suggestions)) {
        let lists = [];
        nextProps.foodList.map((food) => {
          lists.push(food.name)
          _this.setState({ suggestions: lists});
        })

      }
    }

    componentDidMount() {
        const _this = this;

        $('#mealSearchModal').on('shown.bs.modal', function () {
          $(".modal-backdrop.in").hide();
        });

        let categoriesSelect2settings = {
          tags: true,
          placeholder: 'Select categories',
          tokenSeparators: [','],
        };

        let mealPlanTypeSelect2settings = {
          tags: false,
          placeholder: 'Select type',
          tokenSeparators: [','],
          minimumResultsForSearch: -1
        };

        $('#categories-tags').select2(categoriesSelect2settings);
        $('#mealPlanType').select2(mealPlanTypeSelect2settings).val(localStorage.getItem('mealPlanType')).trigger('change');
        $('#mealPlanType').on("change", function(e) {
            _this.setState({
              planType: e.target.value
            });
            _this.props.setMealPlanType(e.target.value);
        });
    }

    handleSubmit(event) {
      event.preventDefault();
      let _this = this;
      console.log('handleSubmit...mealBuilder >>>',_this.props.mealBuilder.length);
      let substitutionMeals = {};
      substitutionMeals.breakfastLists = _this.state.breakfastLists;
      substitutionMeals.lunchLists = _this.state.lunchLists;
      substitutionMeals.dinnerLists = _this.state.dinnerLists;
      let mealPlan = {
        name: _this.refs.name.value.trim(),
        categories: $('#categories-tags').val(),
        description: _this.refs.description.value.trim(),
        plans: _this.props.mealBuilder,
        totalPrice : _this.totalPrice(),
        planType: localStorage.getItem('mealPlanType') === 'lunch' ? 'lunch' : 'fullDay',
        setPrice: parseInt(_this.refs.price.value.trim()) ? parseInt(_this.refs.price.value.trim()) : _this.totalPrice(),
        file: _this.state.file,
        substitutionMeals: substitutionMeals,
      }
      if (mealPlan.categories.length === 0) {
        toastr.error("Select Meal Plan categories");
        return;
      }
      console.log('submit meal-plan: ', mealPlan);
      _this.props.createMealPlan(mealPlan);
    }

    render() {
      console.log('meal plan create component >>> ',this.props);
        return (
            <div>
                {/* page heading start */}
                <div className="mp_heading_mb">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="mp_heading">
                        <h3>Meal Plan</h3>
                      </div>
                    </div>
                  </div>
                </div>
                {/* page heading end */}

                {/* page body start */}
                <div className="mp_body">
                    <div className="row">
                        <div className="col-lg-12">

                            {/* meal plan content start */}
                            <div className="meal_plan">
                                <form onSubmit={this.handleSubmit.bind(this)}>
                                    {/* meal plan basic information start */}
                                    <div className="meal-plan-basicinfo">
                                        <div className='row mb-25'>
                                            {this.MealPlanTitle()}
                                            {this.MealPlanShortDescription()}
                                            {this.MealPlanCategory()}
                                            {this.MealPlanType()}
                                        </div>

                                        <div className='row'>
                                            {this.MealPlanTotalCalorie()}
                                            {this.MealPlanTotalPrice()}
                                            {this.MealPlanSetPrice()}
                                        </div>
                                    </div>
                                    {/* meal plan basic information end */}

                                    {/* tab start */}
                                    <div className='mp-builder-content'>
                                        <Tabs onSelect={this.handleTabSelect.bind(this)} selectedIndex={localStorage.getItem('selectedWeek') ? parseInt(localStorage.getItem('selectedWeek')) : 0}>
                                            <TabList>
                                                {this.generateTabs()}
                                            </TabList>

                                            {this.generateTabPanels()}

                                        </Tabs>
                                    </div>
                                    {/* tab end */}
                                    {this.generateSubstitutionRow()}
                                    {this.imageDropzoneContainer()}
                                    {this.actionButtons()}
                                </form>
                            </div>
                            {/* meal plan content end */}

                        </div>
                    </div>
                </div>
                {/* page body end */}

                {/* Search modal start */}
                <div className="modal aklyMealSearchPrompt meal-search-prompt" id="mealSearchModal" tabIndex="-1" role="dialog"
                  aria-labelledby="myModalLabel">
                  <div className="modal-dialog meal-search-prompt_dialog" role="document">
                    <div className="modal-content meal-search-prompt_content">
                      <div className="meal-search-prompt-close">
                        <div className="close_icon" data-dismiss="modal">
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet">
                            <path
                              d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z" />
                            <path
                              d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z" />
                          </svg>
                        </div>
                      </div>
                      <div className="modal-body">
                        <div className="meal-search-sb">
                          <div className="sb">
                            <div className="form-group">
                              <label className="pseudo_helperlabel">Search food, select & click.</label>
                              <div className="sb_inputgroup">
                                <input type="search" className="form-control sb_formctrl" autoComplete="off" id='search-food'
                                  placeholder="Search Food" onChange={this.handleSearch.bind(this)}/>
                              </div>

                              <ul className="sb_dropdown">
                                {this.renderSearchResult()}
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* Search modal end */}
            </div>
        );
    }
}
