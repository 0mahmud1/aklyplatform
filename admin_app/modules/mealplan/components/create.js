'use strict'

import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import DraggableFoodItemComponent from './draggableFoodItemComponent'
import DropFoodItem from './dropFoodItemComponent'
import Dropzone from 'react-dropzone';
import 'select2';
import toastr from 'toastr';
import $ from 'jquery';

@DragDropContext(HTML5Backend)
class MealPlanComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null
    }
  }
  componentDidMount() {
    $('#mealSearchModal').on('shown.bs.modal', function () {
      $(".modal-backdrop.in").hide();
    })
    let _this = this;
    let select2settings = {
      tags: true,
      placeholder: 'Select categories',
      tokenSeparators: [','],
      // createTag: function (params) {
      //   return {
      //     id: params.term,
      //     text: params.term,
      //     newOption: true
      //   };
      // }
    };
    $('#categories-tags').select2(select2settings)
    /*.on('select2:selecting', function (event) {
     let isNewTagCreated = event.params.args.data.newOption,
     name = event.params.args.data.text;
     if (isNewTagCreated && name !== '') {
     _this.props.createTag({name});
     }
     }
     );*/

  }

  handleSubmit(event) {
    event.preventDefault();
    let _this = this;
    if (_this.props.mealBuilder.length === 0) {
      toastr.error("Select Meal Plan using search button");
      return;
    }

    let mealPlan = {
      name: _this.refs.name.value.trim(),
      categories: $('#categories-tags').val(),
      description: _this.refs.description.value.trim(),
      plans: _this.props.mealBuilder,
      totalPrice : _this.totalPrice(),
      setPrice: parseInt(_this.refs.price.value.trim()) ? parseInt(_this.refs.price.value.trim()) : _this.totalPrice(),
      file: _this.state.file
    }
    if (mealPlan.categories.length === 0) {
      toastr.error("Select Meal Plan categories");
      return;
    }
    if (mealPlan.setPrice > mealPlan.totalPrice) {
      toastr.error("Meal Plan Set price can't be getter then total price");
      return;
    }
    console.log('submit meal-plan: ', mealPlan);
    _this.props.createMealPlan(mealPlan);
  }

  generateCategoryList() {
    return this.props.categoryList && this.props.categoryList.length > 0 ?
      this.props.categoryList.map((category) =>
        <option key={category._id} value={category.name}>{category.name}</option>
      ) : null
  }

  renderDraggableFoods() {
    return this.props.quickList.map((food) => {
      return (
        <DraggableFoodItemComponent
          key={food._id}
          quickList={this.props.quickList}
          pushItem={this.props.pushItem}
          removeQuickList={this.props.removeQuickList}
          name={food.name}
          food={food} />
      )
    })
  }
  onDrop(files) {
    console.log('Received food files: ', files);
    this.setState({
      file: files[0]
    });
  }
  totalPrice() {
    let totalPrice = 0;
    this.props.mealBuilder.map((mealPlan, index) => {
      console.log('foods', mealPlan.foods)
      // mealPlan.index = index;
      return mealPlan.foods.map(foodItem => {
        console.log(foodItem);
        console.log(foodItem.item.price);
        totalPrice = parseInt(totalPrice) + parseInt(foodItem.item.price * foodItem.quantity);
        console.log(totalPrice);
      });
    });
    return totalPrice;

  }

  selectedFood(food) {
    let isAdded = _.find(this.props.quickList, function (foodFromList) {
      return foodFromList._id === food._id;
    });

    if (isAdded) {
      toastr.info('Food Item Already Selected');
    } else {
      this.props.pushQuickList(food);
      toastr.success(`${food.name} added in the list`);
    }
  }

  renderSearchResult() {
    let shouldRender = (this.props.searchResult && this.props.searchResult.length > 0)
    console.log('should render ', shouldRender);
    return shouldRender ? this.props.searchResult.map((food) => {
      return (
        <li key={food._id} onClick={this.selectedFood.bind(this, food)}>
          <a href="#">
            <span className="food_thunmbnailimg">
              <img src={food.images[1]} />
            </span>
            <span className="food_name">
              {food.name}
            </span>
          </a>
        </li>
      )
    }) : null
  }

  handleSearch(event) {
    console.log(event);
    event.preventDefault();
    this.props.searchFood(event.target.value);
  }

  foodDetailModal(food) {
    return (
      <div className="modal aklyFoodDetailPrompt food_detailsprompt" tabIndex="-1" role="dialog"
        aria-labelledby="myModalLabel">
        <div className="modal-dialog food_detailsprompt_dialog" role="document">
          <div className="modal-content food_detailsprompt_content">
            <div className="food_detailsprompt-close">
              <div className="close_icon" data-dismiss="modal">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet">
                  <path
                    d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z" />
                  <path
                    d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z" />
                </svg>
              </div>
            </div>
            <div className="modal-body">
              <div className="food_info_wrapper">
                <div className="food_basicinfo">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="food_name">
                        <p>{food.name}</p>
                      </div>
                      <div className="food_price">
                        <p>{food.price}</p>
                      </div>
                      <div className="food_tags">
                        {food.tags.map((tag) => {
                          return (
                            <div key={tag} className="food_tag">
                              <p>{tag}</p>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="food_basicinfo_withimage">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="food_description_wrapper">
                        <div className="food_description">
                          <p>{food.description}</p>
                        </div>
                        <div className="food_ingredients">
                          <h4>Ingredients</h4>
                          <p>{food.ingredients}</p>
                        </div>
                        <div className="food_recipe">
                          <h4>Recipe</h4>
                          <p>{food.recipe}</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="food_image_wrapper">
                        <div className="food_image">
                          <img src={food.images[0]} alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    )
  }

  header() {
    return (
      <div className="mp_heading_mb">
        <div className="row">
          <div className="col-lg-12">
            <div className="mp_heading">
              <h3>Meal Plan</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    console.log('is Rendered ', this.props.modalContent);
    return (
      <div>
        <div className="mp_heading_mb">
          <div className="row">
            <div className="col-lg-12">
              <div className="mp_heading">
                <h3>Meal Plan</h3>
              </div>
            </div>
          </div>
        </div>

        <div className="mp_body">
          <div className="row">
            <div className="col-lg-12">
              <div className="meal_plan">
                <form onSubmit={this.handleSubmit.bind(this)}>
                  <div className="meal_plan_basicinfo">
                    <div className="row">
                      <div className="col-md-5ths col-lg-5ths">
                        <div className="geninput">
                          <div className="form-group">
                            <label className="geninput_label" htmlFor="exampleInputAmount">Title</label>
                            <div className="input-group geninput_group geninput_group_add">
                              <input type="text" className="form-control geninput_formctrl" ref='name' />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-5ths col-lg-5ths">
                        <div className="geninput">
                          <div className="form-group">
                            <label className="geninput_label" htmlFor="exampleInputAmount">Short Description</label>
                            <div className="input-group geninput_group geninput_group_add">
                              <input type="text" className="form-control geninput_formctrl" ref='description' />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-5ths col-lg-5ths">
                        <div className="geninput">
                          <div className="form-group">
                            <label className="geninput_label" htmlFor="exampleInputAmount">Categories</label>
                            <div className="input-group geninput_group geninput_group_add">
                              <select className="form-control geninput_formctrl" id="categories-tags"
                                multiple="multiple">
                                {this.generateCategoryList()}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-5ths col-lg-5ths">
                        <div className="geninput text-center">
                          <div className="form-group">
                            <label className="geninput_label" htmlFor="exampleInputAmount" style={{display: 'block'}}>Total Price</label>
                            <p className='input-group geninput_group geninput_group_add'>
                              {this.totalPrice()}
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className="col-md-5ths col-lg-5ths">
                        <div className="geninput">
                          <div className="form-group">
                            <label className="geninput_label" htmlFor="exampleInputAmount">Set Price</label>
                            <div className="input-group geninput_group geninput_group_add">
                              <input type="number" className="form-control geninput_formctrl" ref='price' />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="meal_plan_search">
                    <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="meal_plan_sb_wrapper">
                          <div className="search_icon" data-toggle="tooltip" data-placement="top" title="Search Food">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125"
                              preserveAspectRatio="xMidYMid meet" data-toggle="modal"
                              data-target=".aklyMealSearchPrompt" id='launchModal'>
                              <path
                                d="M96.394 92.655l-28.31-28.317c5.554-6.5 8.928-14.916 8.928-24.117 0-20.514-16.688-37.202-37.202-37.202S2.607 19.706 2.607 40.22c0 20.515 16.69 37.204 37.203 37.204 8.604 0 16.512-2.962 22.82-7.885L91.067 97.98l5.328-5.327zM39.81 69.89c-16.36 0-29.668-13.31-29.668-29.67S23.45 10.554 39.81 10.554c16.358 0 29.67 13.31 29.67 29.668S56.167 69.89 39.81 69.89z" />
                            </svg>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="meal_plan_selected_food">
                    <div className="row">
                      {this.renderDraggableFoods()}
                    </div>
                  </div>

                  <div className="meal_plan_view meal_plan_table">
                    <div className="row">
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div className="table-responsive">
                          <table className="table table-bordered akly_admintable akly_admintable_gen">
                            <thead>
                              <tr>
                                <th>slot</th>
                                <th>Sunday</th>
                                <th>Monday</th>
                                <th>Tuesday</th>
                                <th>Wednesday</th>
                                <th>Thursday</th>
                                <th>Friday</th>
                                <th>Saturday</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="meal_plan_slots">
                                  <div className="lunch">
                                    <p>Lunch & Breakfast</p>
                                  </div>
                                  <div className="dinner">
                                    <p>Dinner</p>
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='sunday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='sunday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='monday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='monday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='tuesday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='tuesday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='wednesday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='wednesday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='thursday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='thursday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='friday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='friday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                                <td className="meal_plan_added_foods">
                                  <div className="lunch">
                                    <DropFoodItem day='saturday' slot='lunch' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>

                                  <div className="dinner">
                                    <DropFoodItem day='saturday' slot='dinner' mealBuilder={this.props.mealBuilder}
                                      changeModalContent={this.props.changeModalContent}
                                      increaseItem={this.props.increaseItem}
                                      reduceItem={this.props.reduceItem} />
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6 meal_imageditwrapper mb-25">

                    <label>Upload Meal Paln Image</label>
                    <Dropzone
                      ref="dropzone"
                      onDrop={this.onDrop.bind(this)}
                      style={{ "width": "100%", "height": "51rem", "border-style": "dashed", "border-color": "rgb(102, 102, 102)", "margin-top": "15px" }}>
                      {this.state.file ?
                        <div className="upload-food_img">
                          <img src={this.state.file.preview} />
                        </div>
                        : <div className="mtopl">Try dropping some files here, or click to select files to upload.</div>
                      }
                    </Dropzone>
                  </div>
                  <div className="meal_plan_actionbtn actionbutn">
                    <div className="row">
                      <div className="col-md-12 col-lg-12">
                        <div className="actionbtn_save">
                          <button type="submit" className="btn butn butn_default">
                            {/* <span className="actionbtn_save_loader actionButnSaveLoader">
                             <svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><path fill="none" d="M0 0h100v100H0z"/><defs><filter id="a" x="-100%" y="-100%" width="300%" height="300%"><feOffset result="offOut" in="SourceGraphic"/><feGaussianBlur result="blurOut" in="offOut"/><feBlend in="SourceGraphic" in2="blurOut"/></filter></defs><path d="M10 50s0 .5.1 1.4c0 .5.1 1 .2 1.7 0 .3.1.7.1 1.1.1.4.1.8.2 1.2.2.8.3 1.8.5 2.8.3 1 .6 2.1.9 3.2.3 1.1.9 2.3 1.4 3.5.5 1.2 1.2 2.4 1.8 3.7.3.6.8 1.2 1.2 1.9.4.6.8 1.3 1.3 1.9 1 1.2 1.9 2.6 3.1 3.7 2.2 2.5 5 4.7 7.9 6.7 3 2 6.5 3.4 10.1 4.6 3.6 1.1 7.5 1.5 11.2 1.6 4-.1 7.7-.6 11.3-1.6 3.6-1.2 7-2.6 10-4.6 3-2 5.8-4.2 7.9-6.7 1.2-1.2 2.1-2.5 3.1-3.7.5-.6.9-1.3 1.3-1.9.4-.6.8-1.3 1.2-1.9.6-1.3 1.3-2.5 1.8-3.7.5-1.2 1-2.4 1.4-3.5.3-1.1.6-2.2.9-3.2.2-1 .4-1.9.5-2.8.1-.4.1-.8.2-1.2 0-.4.1-.7.1-1.1.1-.7.1-1.2.2-1.7.1-.9.1-1.4.1-1.4V54.2c0 .4-.1.8-.1 1.2-.1.9-.2 1.8-.4 2.8-.2 1-.5 2.1-.7 3.3-.3 1.2-.8 2.4-1.2 3.7-.2.7-.5 1.3-.8 1.9-.3.7-.6 1.3-.9 2-.3.7-.7 1.3-1.1 2-.4.7-.7 1.4-1.2 2-1 1.3-1.9 2.7-3.1 4-2.2 2.7-5 5-8.1 7.1L70 85.7c-.8.5-1.7.9-2.6 1.3l-1.4.7-1.4.5c-.9.3-1.8.7-2.8 1C58 90.3 53.9 90.9 50 91l-3-.2c-1 0-2-.2-3-.3l-1.5-.2-.7-.1-.7-.2c-1-.3-1.9-.5-2.9-.7-.9-.3-1.9-.7-2.8-1l-1.4-.6-1.3-.6c-.9-.4-1.8-.8-2.6-1.3l-2.4-1.5c-3.1-2.1-5.9-4.5-8.1-7.1-1.2-1.2-2.1-2.7-3.1-4-.5-.6-.8-1.4-1.2-2-.4-.7-.8-1.3-1.1-2-.3-.7-.6-1.3-.9-2-.3-.7-.6-1.3-.8-1.9-.4-1.3-.9-2.5-1.2-3.7-.3-1.2-.5-2.3-.7-3.3-.2-1-.3-2-.4-2.8-.1-.4-.1-.8-.1-1.2v-1.1-1.7c-.1-1-.1-1.5-.1-1.5z" filter="url(#a)"><animateTransform attributeName="transform" type="rotate" from="0 50 50" to="360 50 50" repeatCount="indefinite" dur="0.7s"/></path></svg>
                             </span> */}
                            <span className="actionbtn_save_text">Create</span>
                          </button>
                        </div>
                        <div className="actionbtn_cancel">
                          <button type="button" className="btn butn butn_cancel"
                            onClick={() => browserHistory.push('/mealplans')}>Cancel
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>

          </div>
        </div>
        <div className="modal aklyMealSearchPrompt meal-search-prompt" id="mealSearchModal" tabIndex="-1" role="dialog"
          aria-labelledby="myModalLabel">
          <div className="modal-dialog meal-search-prompt_dialog" role="document">
            <div className="modal-content meal-search-prompt_content">
              <div className="meal-search-prompt-close">
                <div className="close_icon" data-dismiss="modal">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 125" preserveAspectRatio="xMidYMid meet">
                    <path
                      d="M91.368 95c-.93 0-1.86-.355-2.568-1.064L6.063 11.2c-1.42-1.417-1.42-3.72 0-5.136 1.42-1.42 3.718-1.42 5.137 0L93.937 88.8c1.42 1.417 1.42 3.72 0 5.136-.71.71-1.64 1.064-2.57 1.064z" />
                    <path
                      d="M8.632 95c-.93 0-1.86-.355-2.568-1.064-1.42-1.417-1.42-3.72 0-5.136L88.8 6.063c1.42-1.42 3.718-1.42 5.137 0 1.42 1.417 1.42 3.72 0 5.136L11.2 93.935C10.49 94.646 9.56 95 8.632 95z" />
                  </svg>
                </div>
              </div>
              <div className="modal-body">
                <div className="meal-search-sb">
                  <div className="sb">
                    <div className="form-group">
                      <label className="pseudo_helperlabel">Search food, select & click.</label>
                      <div className="sb_inputgroup">
                        <input type="search" className="form-control sb_formctrl" autoComplete="off" id='search-food'
                          placeholder="Search Food" onChange={this.handleSearch.bind(this)} />
                      </div>

                      <ul className="sb_dropdown">
                        {this.renderSearchResult()}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* {this.props.modalContent ? this.foodDetailModal(this.props.modalContent) : null} */}
      </div>
    )
  }
}

export default MealPlanComponent
