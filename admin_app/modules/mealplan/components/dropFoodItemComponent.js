import React, {PropTypes, Component} from 'react';
//import ItemTypes from './ItemTypes';
import {DropTarget} from 'react-dnd';
import _ from 'lodash';


const boxTarget = {
  drop(props, monitor, component) {
    return {
      day: props.day,
      slot: props.slot
    }
  }
};

const collect = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }
}

/*@DropTarget('box', boxTarget, (connect, monitor) => ({
 connectDropTarget: connect.dropTarget(),
 isOver: monitor.isOver(),
 canDrop: monitor.canDrop()
 }))*/

class DropFoodItem extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
  };

  /*renderFoodList2() {
   let reactdom = this
   let oneDayMealPlan = _.find(reactdom.props.mealBuilder, function(o) {
   return o.day == reactdom.props.day && o.slot == reactdom.props.slot
   });

   return(
   oneDayMealPlan  && oneDayMealPlan.foods.length > 0
   ?
   oneDayMealPlan.foods.map((food) =>{
   return <li key={food.item._id}>{food.item.name}, {food.quantity}</li>
   }) : null
   )

   }*/


  showModal(food) {
    console.log('calling show modal');
    this.props.changeModalContent(food);

    $('.aklyFoodDetailPrompt').modal('show');
  }

  reduceItemQuantity(food) {
    this.props.reduceItem({day: this.props.day, slot: this.props.slot, food: food})
  }

  increaseItemQuantity(food) {
    this.props.increaseItem({day: this.props.day, slot: this.props.slot, food: food})
  }

  renderFoodList() {
    let reactdom = this
    let oneDayMealPlan = _.find(reactdom.props.mealBuilder, function (o) {
      return o.day == reactdom.props.day && o.slot == reactdom.props.slot
    });

    return (
      oneDayMealPlan && oneDayMealPlan.foods.length > 0
        ?
        oneDayMealPlan.foods.map((food) => {
          return <div className="added_food_card" key={food.item._id} data={food}>
            <div className="food_info">
              <div className="food_thunmbnailimg">
                <img src={food.item.images[1]}/>
              </div>
              <div className="food_name">
                <p data-toggle="modal" onClick={this.showModal.bind(this, food.item)}>{food.item.name}</p>
              </div>
            </div>
            <div className="food_quantity">
              <div className="quantity_plus" onClick={this.increaseItemQuantity.bind(this, food)}>
                <div className="quantity_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet">
                    <path
                      d="M89.465 45.868H54.132V10.535c0-2.282-1.85-4.132-4.132-4.132s-4.132 1.85-4.132 4.132v35.333H10.535c-2.282 0-4.132 1.85-4.132 4.132s1.85 4.132 4.132 4.132h35.333v35.333c0 2.282 1.85 4.132 4.132 4.132s4.132-1.85 4.132-4.132V54.132h35.333c2.282 0 4.132-1.85 4.132-4.132s-1.85-4.132-4.132-4.132z"/>
                  </svg>
                </div>
              </div>
              <div className="quantity_total">
                <p>{food.quantity}</p>
              </div>
              <div className="quantity_minus" onClick={this.reduceItemQuantity.bind(this, food)}>
                <div className="quantity_icon">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet">
                    <path style={{
                      textIndent: 0,
                      textAlign: 'start',
                      lineHeight: 'normal',
                      textTransform: 'none',
                      blockProgression: 'tb',
                      InkscapeFontSpecification: 'Sans',
                      color: '#000',
                      overflow: 'visible',
                      fontFamily: '"Sans"'
                    }} d="M12.813 48.013a2.002 2.002 0 1 0 .187 4h74a2 2 0 1 0 0-4H13a2 2 0 0 0-.188 0z"/>
                  </svg>
                </div>
              </div>
            </div>
          </div>
        }) : null
    )

  }

  render() {
    const {canDrop, isOver, connectDropTarget} = this.props;
    const isActive = canDrop && isOver;
    let bgColor = '#f6f6f6';
    isActive ? bgColor : bgColor = '#fff';
    return connectDropTarget(
      <div className="added_food_card_wrapper" style={{'backgroundColor': bgColor}}>
        {this.props.mealBuilder.length > 0 ? this.renderFoodList() : null}
      </div>
    );
  }
}
export default DropTarget('box', boxTarget, collect)(DropFoodItem);
