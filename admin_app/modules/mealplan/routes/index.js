'use strict'

import React, { Component } from 'react'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import MealPlanLayout from '../layouts';
import MealPlanCreateContainer from '../containers/create.js';
import MealPlanNewCreateContainer from '../containers/mpcreatenew.js';
import MealPlanEditContainer from '../containers/editnew';
import MealPlanListContainer from '../containers/list';
import * as AuthService from '../../services/auth';

export default function () {
  return (
    <Route path='mealplans' component={MealPlanLayout}>
      <IndexRoute component={MealPlanListContainer} />
      <Route path='create' component={MealPlanCreateContainer} />
      <Route path='createnew' component={MealPlanNewCreateContainer} />
      <Route path=':_id' component={MealPlanEditContainer} />
    </Route>
  )
}
