import axios from 'axios';
import * as constants from '../constants';
import toastr from 'toastr';
import aws from '../../configs/awsConfig.js';
import { uploadToS3, generateUrlNames } from '../../core/libs/uploadS3.js';
import Chance from 'chance';
import path from 'path';
import async from 'async';

export function getMealPlans(limit, page) {
  return function (dispatch) {
    let l = limit ? limit : process.env.LIMIT_DEFAULT;
    let p = page ? page : process.env.PAGE_DEFAULT;
    dispatch({ type: constants.GET_MEALPLAN_PENDING });
    axios.get('api/meal-plans' + '?limit=' + l + '&page=' + p)
      .then((response) => {
        console.log('get meal plans... : ', response.data.data);
        dispatch({ type: constants.GET_MEALPLAN_RESOLVED, payload: response.data });
        //toastr.success('MealPlans Loaded');
      })
      .catch((err) => {
        dispatch({ type: constants.GET_MEALPLAN_REJECTED, payload: err });
        toastr.error(err);
      });
  };
}

export function getFilteredMealPlans(name) {
    return function (dispatch) {
        dispatch({ type: constants.GET_MEALPLAN_PENDING });
        axios.get('api/meal-plans' + '?name=' + name)
            .then((response) => {
                console.log('get filtered meal plans... : ', response.data.data);
                dispatch({ type: constants.GET_MEALPLAN_RESOLVED, payload: response.data });
                //toastr.success('MealPlans Loaded');
            })
            .catch((err) => {
                dispatch({ type: constants.GET_MEALPLAN_REJECTED, payload: err });
                toastr.error(err);
            });
    };
}

export function createMealPlanAction(mealplan, cb) {
  return function (dispatch) {
    dispatch({ type: constants.CREATE_MEALPLAN_PENDING });
    if (mealplan.file) {
      let s3, image = [];
      const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: 'original/' + new Chance().guid() + path.extname(mealplan.file.name),
        ContentType: mealplan.file.type,
        Body: mealplan.file,
        ACL: 'public-read',
      };
      console.log('mealplan', mealplan);
      async.parallel([
        function (callback) {
          // uploadToS3(params,callback(null,data))
          uploadToS3(params, function (err, image) {
            console.log('images', image);
            callback(null, image);
          });
        }
      ],
        function (err, results) {
          if (err) {
            dispatch({ type: constants.CREATE_MEALPLAN_REJECTED, payload: err });
            if (cb) {
              cb(err, null);
            }
          } else {
            // saving food url here with object.
            //let video = results[0];
            console.log('async.parallel=>>>', results);
            let image = results[0];
            console.log('image object', image);
            image = generateUrlNames(image.Location);
            mealplan.imageUrl = image[0];
            console.log(mealplan.imageUrl);
            delete mealplan.file;

            console.log('FINAL MealPlans OBJECT ......', mealplan);
            axios.post('api/meal-plans', mealplan)
              .then((response) => {
                dispatch({ type: constants.CREATE_MEALPLAN_RESOLVED });
                if (cb) {
                  cb(null, { type: constants.CREATE_MEALPLAN_RESOLVED });
                }

                console.log('createMealPlanAction: ', response);
              })
              .catch((err) => {
                dispatch({ type: constants.CREATE_MEALPLAN_REJECTED, payload: err });
                if (cb) {
                  cb(err, null);
                }

                console.warn('createMealPlanAction: ', err);
              });
          }
        });
    } else {
      axios.post('api/meal-plans', mealplan)
        .then((response) => {
          dispatch({ type: constants.CREATE_MEALPLAN_RESOLVED });
          if (cb) {
            cb(null, { type: constants.CREATE_MEALPLAN_RESOLVED });
          }

          console.log('createMealPlanAction: ', response);
        })
        .catch((err) => {
          dispatch({ type: constants.CREATE_MEALPLAN_REJECTED, payload: err });
          if (cb) {
            cb(err, null);
          }

          console.warn('createMealPlanAction: ', err);
        });
    }

  };
}

export function readMealPlanAction(_id) {
  return function (dispatch) {
    dispatch({ type: constants.READ_MEALPLAN_PENDING });
    axios.get(('api/meal-plans/' + _id))
      .then((response) => {
        dispatch({ type: constants.READ_MEALPLAN_RESOLVED, payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: constants.READ_MEALPLAN_REJECTED, payload: err });
        console.warn('Read mealPlanAction: ', err);
        toastr.error("Meal plan not found")
      });
  };
}

export function updateMealPlanAction(_id, patch, cb) {
  return function (dispatch) {
    dispatch({ type: constants.UPDATE_MEALPLAN_PENDING });
    if (patch.file) {
      let s3, image = [];
      console.log('PATCH: ', patch, _id);

      const params = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: 'original/' + new Chance().guid() + path.extname(patch.file.name),
        ContentType: patch.file.type,
        Body: patch.file,
        ACL: 'public-read',
      };
      console.log('mealplan', patch);
      async.parallel([
        function (callback) {
          // uploadToS3(params,callback(null,data))
          uploadToS3(params, function (err, image) {
            console.log('images', image);
            callback(null, image);
          });
        }
      ],
        function (err, results) {
          if (err) {
            dispatch({ type: constants.UPDATE_MEALPLAN_REJECTED, payload: err });
            if (cb) {
              cb(err, null);
            }
          } else {
            // saving food url here with object.
            //let video = results[0];
            console.log('async.parallel=>>>', results);
            let image = results[0];
            console.log('image object', image);
            image = generateUrlNames(image.Location);
            patch.imageUrl = image[0];
            console.log(patch.imageUrl);
            delete patch.file;

            console.log('FINAL MealPlans OBJECT ......', patch);

            updateMealPlanApi('api/meal-plans/' + _id, patch, dispatch, cb);

          }
        });
    } else {
      updateMealPlanApi('api/meal-plans/' + _id, patch, dispatch, cb);
    }

  };
}

export function deleteMealPlanAction(_id, callback) {
    return function (dispatch) {
        axios.delete('api/meal-plans/'+_id)
            .then((response)=> {
                console.log('meal-plans to be deleted == ', response);
                dispatch({type: constants.DELETE_MEALPLAN_RESOLVED, payload: response})
                if(callback){
                  callback(null, {type: constants.DELETE_MEALPLAN_RESOLVED, payload: response})
                }
            })
            .catch((err)=>{
                dispatch({ type: constants.DELETE_MEALPLAN_REJECTED, payload: err });
                console.warn('error in delete meal plan == ', err);
                if (callback) {
                    callback(err, null);
                }
            });
    }
}

function updateMealPlanApi(url, patch, dispatch, cb) {
  axios.put(url, patch)
    .then((response) => {
      console.log('UPDATE hub... : ', response);
      dispatch({ type: constants.UPDATE_MEALPLAN_RESOLVED, payload: response });
      if (cb) {
        cb(null, { type: constants.UPDATE_MEALPLAN_RESOLVED, payload: response });
      }
    })
    .catch((err) => {
      dispatch({ type: constants.UPDATE_MEALPLAN_REJECTED, payload: err });
      if (cb) {
        cb(err, null);
      }
    });

}
