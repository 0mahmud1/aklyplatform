import { combineReducers } from 'redux';

import categoriesReducer from './modules/categories/reducers/categories';
import tagsReducer from './modules/tags/reducers/index.js';
import authenticateReducer from './modules/authenticate/reducer.js';
import foodProviderReducer from './modules/foodproviders/reducers/index.js';
import foodReducer from './modules/foods/reducers';
import HubsReducer from './modules/hubs/reducers';
import MealPlanReducer from './modules/mealplan/reducer';
import dBoyReducer from './modules/dboys/reducers';
import vanReducer from './modules/vans/reducers';
import orderReducer from './modules/orders/reducers';
import deliveryReducer from './modules/deliveries/reducers';
import customersReducer from './modules/customers/reducers';
import liveViewReducer from './modules/liveView/reducer';
import simulatorReducer from './modules/simulator/reducer';
import couponReducer from './modules/coupons/reducers'

const reducers = combineReducers({
  auth: authenticateReducer,
  foodProviders: foodProviderReducer,
  tags: tagsReducer,
  mealPlans: MealPlanReducer,
  dBoys: dBoyReducer,
  categories: categoriesReducer,
  hubs: HubsReducer,
  foods: foodReducer,
  vans: vanReducer,
  orders: orderReducer,
  deliveries: deliveryReducer,
  customers: customersReducer,
  liveView: liveViewReducer,
  simulator: simulatorReducer,
  coupons: couponReducer
});

export default reducers;
