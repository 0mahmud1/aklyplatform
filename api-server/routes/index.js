import jwt from 'jsonwebtoken';
import app from '../app.js';

let express = require('express');
let router = express.Router();


import { logger } from '../libs/logger.js';

/* GET home page. */
router.get('/', function(req, res, next) {
    logger.info('server is ready');
  res.render('index', { title: 'Express API server Boilerplate' });
});

export default router;
