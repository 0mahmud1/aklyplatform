import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a sub schema
const createdBySchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required.']
  }
});

// create a sub schema
const lunchSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required.']
  }
});

// create a sub schema
const dinnerSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required.']
  }
});

const foodSchema = mongoose.Schema({
  item: {
    type: Schema.Types.Mixed,
    required: [true, 'Food Object must be present in the mealplan']
  },
  quantity: {
    type: Number,
    min: 1,
    required: [true,'qunatity of food required']
  }
});

// create a sub schema
const planSchema = mongoose.Schema({
  day: {
    type: String,
    required: [true, 'Day is required.']
  },
  slot: {
    type: String,
    required: [true, 'Slot is required.']
  },
  foods: {
    type: [foodSchema]
  },
  week: {
    type: Number,
    required: [true, 'Week is required.']
  },
});

const substitutionMealsSchema = mongoose.Schema({
  breakfast : {
    type: [Schema.Types.Mixed]
  },
  lunch: {
    type: [Schema.Types.Mixed]
  },
  dinner: {
    type: [Schema.Types.Mixed]
  },
});

// create a schema
const mealPlanSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  categories: {
    type: [String],
    required: [true, 'Category is required.']
  },
  createdBy:{
    type: createdBySchema,
    // required: [true, 'Created by is required.']
  },
  description: {
    type: String,
    required: [true, 'Short description is required.']
  },
  imageUrl: {
    type: String,
    validate: {
      validator: function(value) {
        return /(https?:\/\/.*\.(?:png|jpg))/i.test(value);
      },
      message: '{VALUE} is not a valid image url!'
    },
    // required: [true, 'Image url is required.']
  },
  plans: {
    type: [planSchema],
    required: [true, 'Plans is required.']
  },
  totalPrice: {
    type: Number,
    //required: [true, 'Total Price is required.']
  },
  setPrice: {
    type : Number
  },
  calorie: {
    type: Number,
    required: false
  },
  imageUrl: {
    type: String,
    required: false
  },
  planType: {
    type: String,
    enum: ['lunch', 'fullDay'],
    default: 'fullDay'
  },
  protein: {
    type: Number,
    required: false
  },
  fat: {
    type: Number,
    required: false
  },
  carbs: {
    type: Number,
    required: false
  },
  isSideDish: {
    type: Boolean,
    required: false
  },
  isSnack: {
    type: Boolean,
    required: false
  },
  perDayKcal: {
    type: Number,
    required: false
  },
  perDayMeal: {
    type: Number,
    required: false
  },
  duration: {
    type: String,
    required: false
  },
  cookingStatus: {
    type: String,
    required: false
  },
  substitutionMeals: {
    type: substitutionMealsSchema
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
}, { versionKey: false });

// on every save, add the date
mealPlanSchema.pre('save', function(next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }

  if (!this.createdBy) {
    // this.createdBy =
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const MealPlan = mongoose.model('MealPlan', mealPlanSchema);

// make this available in our applications
export default MealPlan;
