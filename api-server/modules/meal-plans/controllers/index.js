import MealPlan from '../models';
import {logger} from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { findFoodInformation } from '../../foods/libs/index.js'
import async from 'async';

/*
*  RESTful CRUD APIs for MealPlan
*/

/**
* @api {GET} /api/meal-plans All active mealPlans
* @apiName GetMealPlans
* @apiGroup MealPlan
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active mealPlans
* @apiSuccess (200) {Object[]} categories see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": String,
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Numebr,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                  {
*                    "status": String,
*                    "images": [String],
*                    "__v": 0,
*                    "name": "Soup",
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  }
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
*   ]
* }
*/

export const getMealPlans = (req, res, next) => {
  try {
    logger.info('request for get active meal plans ',req.query);

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = { status: 'Active' };
    // @todo insert required queries in query param
    if(req.query['name']) {
        let name = req.query['name'];
        query['name'] = RegExp(name,'i') ;

    }

    if(req.query['planType']) {
      query['planType'] = req.query['planType'];
    }
    //this promise returns number of total object
    let findCountPromise = MealPlan.find(query).count();

    //this promise returns actual data
    let findDataPromise = MealPlan.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active meal plans',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getMealPlans',
      error: e
    });
  }
};


/**
* @api {POST} /api/meal-plans Create a mealPlan
* @apiName CreateMealPlan
* @apiGroup MealPlan
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator
* @apiParam {Object} data see Request-Example
* @apiParamExample {Object} Request-Example:
*  {
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Numebr,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
* }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a mealPlan
* @apiSuccess (200) {Object[]} categories see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": String,
*   "data":
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Number,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
* }
*/

export const createMealPlan = (req, res, next) => {
  try {
      let newMealPlan = new MealPlan(req.body);
      async.parallel({
        findFoodInformation : function(callback) {
          let substitutionMeals = {
            breakfast: [],
            lunch: [],
            dinner: [],
          };
          if(req.body.substitutionMeals) {
            let breakfastLists = getAllFoods(req.body.substitutionMeals.breakfastLists);
            let lunchLists = getAllFoods(req.body.substitutionMeals.lunchLists);
            let dinnerLists = getAllFoods(req.body.substitutionMeals.dinnerLists);
            Promise.all([breakfastLists,lunchLists,dinnerLists])
              .then((response) => {
                substitutionMeals.breakfast = response[0];
                substitutionMeals.lunch = response[1];
                substitutionMeals.dinner = response[2];
                callback(null,substitutionMeals);
              })
              .catch((error) => {
                callback(error,null)
              })
          }
        }
      }, function(err, results) {
        let totalCalorie = 0 ,totalProtein = 0, totalFat = 0, totalCarbs = 0;
        newMealPlan.plans.forEach(function (plan) {
          plan.foods.forEach(function (food) {
            totalCalorie += parseFloat(food.item.calorie===''?0:food.item.calorie) * parseFloat(food.quantity);
            totalProtein += parseFloat(food.item.protein===''?0:food.item.protein) * parseFloat(food.quantity);
            totalFat += parseFloat(food.item.fat===''?0:food.item.fat) * parseFloat(food.quantity);
            totalCarbs += parseFloat(food.item.carbs===''?0:food.item.carbs) * parseFloat(food.quantity);
            // logger.info('food---------------->>>>>>>>>>',food,'------------>>>>>>>>>>>',totalCalorie,'||||',totalProtein,'||||',totalFat,'||||',totalCarbs)
          })
        });
        logger.info(totalCalorie,totalProtein,totalFat,totalCarbs);
        Object.assign(newMealPlan , newMealPlan , {"substitutionMeals": results.findFoodInformation,"calorie":totalCalorie.toFixed(2),"protein":totalProtein.toFixed(2),"fat":totalFat.toFixed(2),"carbs":totalCarbs.toFixed(2)});
        // logger.info('newMealPlan----------------------->>>>>>>>>>>>>>>>',newMealPlan);

        newMealPlan.save()
        .then((response) => {
          return res.status(200).json({
            success: true,
            message: 'Create a mealPlan',
            data: response
          })
        })
        .catch((error) => {
          res.status(400).json({
            success: false,
            message: 'Failed to create a mealPlan',
            data: error
          })
        })
      });

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createMealPlan',
      error: e
    });
  }
};

function getAllFoods(lists) {
  console.log('lists here ',lists);
  return new Promise(function(resolve, reject) {
    try {
      async.map(lists, findFoodInformation, function (error, result) {
          //console.log('inside findFoodInformation =>>>',result);
          if(error){
            logger.error(error);
            reject(error);
          } else {
            resolve(result);
          }
      });
    } catch (e) {
      logger.error(e);
      logger.debug(e.stack);
      reject(e);
    }
  });
}


/**
* @api {GET} /api/meal-plans/:_id Read a mealPlan
* @apiName ReadMealPlan
* @apiGroup MealPlan
* @apiPermission all
* @apiParam {String} _id MealPlan unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active mealPlans
* @apiSuccess (200) {Object[]} categories see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": String,
*   "data":
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Numebr,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
* }
*/

export const readMealPlan = (req, res, next) => {
  // MealPlan.findById(req.params._id).exec((err, mealPlan) => {
  //     if (err) return next(err);
  //     return res.json({
  //       success: true,
  //       message: 'Read a mealPlan',
  //       mealPlan: mealPlan
  //     });
  //   });
  try {
    MealPlan.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((mealPlan) => {
      if (!mealPlan) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      //logger.info('my mealPlan: ...', mealPlan.toObject());

      return res.status(200).json({
        success: true,
        message: 'Read a mealPlan',
        data: mealPlan
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readMealPlan',
      error: e
    });
  }
};

/**
* @api {PUT} /api/meal-plans/:_id Update a mealPlan
* @apiName UpdateMealPlan
* @apiGroup MealPlan
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator
* @apiParam {String} _id MealPlan unique ID
* @apiParamExample {Object} Request-Example:
*  {
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Numebr,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
* }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a mealPlan
* @apiSuccess (200) {Object[]} categories see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": String,
*   "data":
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*          {
*              "day": String,
*              "slot": String,
*              "_id": String,
*              "foods":[
*              {
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": "",
*                "status": String,
*                "load": Numebr,
*                "images": [String],
*                "provider": [],
*                "tags": [
*                  {
*                    "status": "Active",
*                    "images": [String],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*              "quantity": Number,
*              "_id": String
*           },
*       ]
*       "categories": [
*        String
*      ]
*     }
* }
*/

export const updateMealPlan = (req, res, next) => {
  try {
    MealPlan.findOne({_id: req.params._id}).exec()
    .then((mealPlan) => {
      if (!mealPlan) {
        throw new Error('no mealPlan found for update');
      }
      const newMealPlan = req.body;
      async.parallel({
        findFoodInformation : function(callback) {
          let substitutionMeals = {
            breakfast: [],
            lunch: [],
            dinner: [],
          };
          if(req.body.substitutionMeals) {
            let breakfastLists = getAllFoods(req.body.substitutionMeals.breakfastLists);
            let lunchLists = getAllFoods(req.body.substitutionMeals.lunchLists);
            let dinnerLists = getAllFoods(req.body.substitutionMeals.dinnerLists);
            Promise.all([breakfastLists,lunchLists,dinnerLists])
              .then((response) => {
                substitutionMeals.breakfast = response[0];
                substitutionMeals.lunch = response[1];
                substitutionMeals.dinner = response[2];
                callback(null,substitutionMeals);
              })
              .catch((error) => {
                callback(error,null)
              })
          }
        }
      }, function(err, results) {
        let totalCalorie = 0 ,totalProtein = 0, totalFat = 0, totalCarbs = 0;
        newMealPlan.plans.forEach(function (plan) {
          plan.foods.forEach(function (food) {
              totalCalorie += parseFloat(food.item.calorie===''?0:food.item.calorie) * parseFloat(food.quantity);
              totalProtein += parseFloat(food.item.protein===''?0:food.item.protein) * parseFloat(food.quantity);
              totalFat += parseFloat(food.item.fat===''?0:food.item.fat) * parseFloat(food.quantity);
              totalCarbs += parseFloat(food.item.carbs===''?0:food.item.carbs) * parseFloat(food.quantity);
          })
        });
        Object.assign(mealPlan , newMealPlan , {"substitutionMeals": results.findFoodInformation ,"calorie":totalCalorie.toFixed(2),"protein":totalProtein.toFixed(2),"fat":totalFat.toFixed(2),"carbs":totalCarbs.toFixed(2)});
        return mealPlan.save();
      });
    })
    .then((updatedMealPlan) => {
      return res.status(200).json(placeHolder(true, 'Update a mealPlan', updatedMealPlan))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updatedMealPlan',
      error: e
    });
  }
};

/**
* @api {DELETE} /api/meal-plans/:_id Delete a mealPlan
* @apiName DeleteMealPlan
* @apiGroup MealPlan
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator
* @apiParam {String} _id MealPlan unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a mealPlan
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    "success": "true",
*    "message": "Delete a mealPlan"
*  }
*/

export const deleteMealPlan = (req, res, next) => {
  // MealPlan.findById(req.params._id).exec((err, mealPlan) => {
  //   if (err) return next(err);
  //
  //   mealPlan.status = 'Archived';
  //
  //   mealPlan.save((err, mealPlan) => {
  //     if (err) return next(err);
  //     return res.json({
  //       success: true,
  //       message: 'Delete a mealPlan'
  //     });
  //   });
  // });
  try {
    MealPlan.findOne({_id: req.params._id}).exec()
    .then((mealPlan) => {
      logger.info('mealPlan to be DELETED: ', mealPlan);
      mealPlan.status = 'Archived';
      return mealPlan.save();
    })
    .then((deletedMealPlan) => {
      return res.status(200).json(placeHolder(true, 'Deleted a mealPlan'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deletedMealPlan',
      error: e
    });
  }
};
