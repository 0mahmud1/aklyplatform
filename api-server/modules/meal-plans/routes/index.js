import express from 'express';

import { logger } from 'libs/logger.js';
import { getMealPlans, createMealPlan, readMealPlan, updateMealPlan, deleteMealPlan } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const mealPlanRoutes = express.Router();

mealPlanRoutes.use(['/meal-plans','/meal-plans/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in meal-plans route.`);
    if (req.method === 'GET') {
        next();
    } else {
        checkAuthToken(req, res, next);
    }
});
mealPlanRoutes.route('/meal-plans')
  // All active meal-plans (accessed at GET /api/meal-plans)
  .get(getMealPlans)
  // Create a meal-plan (accessed at POST /api/meal-plans)
  .post(isAuthorizedRoles("administrator", "manager", "customer", "healthProfessionals"), createMealPlan);

mealPlanRoutes.route('/meal-plans/:_id')
  // Read a meal-plan (accessed at GET /api/meal-plans/:_id)
  .get(readMealPlan)
  // Update a meal-plan (accessed at PUT /api/meal-plans/:_id)
  .put(isAuthorizedRoles("administrator", "manager", "customer", "healthProfessionals"), updateMealPlan)
  // Delete a meal-plan (accessed at DELETE /api/meal-plans/:_id)
  .delete(isAuthorizedRoles("administrator", "manager", "customer", "healthProfessionals"), deleteMealPlan);

export default mealPlanRoutes;
