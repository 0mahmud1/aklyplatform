import express from 'express';

import { logger } from 'libs/logger.js';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';
import { updateSubscription } from '../controllers/updateSubscription';
import { readySubscription } from '../controllers/readySubscription';
import { getDeliveries } from '../controllers/getDeliveries'
import { createDelivery } from '../controllers/createDelivery'
import { readDelivery } from '../controllers/readDelivery'
import { updateDelivery } from '../controllers/updateDelivery'
import { deleteDelivery } from '../controllers/deleteDelivery'
import { updateDeliveryStatus } from '../controllers/updateDeliveryStatus'
const deliveryRoutes = express.Router();

deliveryRoutes.use(['/deliveries','/deliveries/:_id'],(req, res, next) => {
  logger.info(`a ${req.method} request in deliveries route.`);
  checkAuthToken(req, res, next);
});
deliveryRoutes.route('/deliveries')
  // All active deliveries (accessed at GET /api/deliveries)
  .get(getDeliveries)
  // Create a delivery (accessed at POST /api/deliveries)
  .post(isAuthorizedRoles("administrator", "customer"), createDelivery);

deliveryRoutes.route('/deliveries/subscriptions')
  .put(isAuthorizedRoles("administrator", "manager", "foodProvider"), updateSubscription);

deliveryRoutes.route('/deliveries/subscriptions/ready')
  .put(isAuthorizedRoles("administrator", "manager", "foodProvider"), readySubscription);

deliveryRoutes.route('/deliveries/status/:_id')
  .put(isAuthorizedRoles("administrator", "manager", "customer", "foodProvider", "deliveryBoy", "vanOperator"), updateDeliveryStatus)

deliveryRoutes.route('/deliveries/:_id')
  // Read a delivery (accessed at GET /api/deliveries/:_id)
  .get(isAuthorizedRoles("administrator", "manager", "customer", "foodProvider", "deliveryBoy", "vanOperator"), readDelivery)
  // Update a delivery (accessed at PUT /api/deliveries/:_id)
  .put(isAuthorizedRoles("administrator", "manager", "customer", "foodProvider", "deliveryBoy", "vanOperator"), updateDelivery)
  // Delete a delivery (accessed at DELETE /api/deliveries/:_id)
  .delete(isAuthorizedRoles("administrator", "manager","customer"), deleteDelivery);


export default deliveryRoutes;
