import mongoose, { Schema } from 'mongoose';
import shortid from 'shortid';
import _ from 'lodash';
import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'

const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Point',
  },
  coordinates: {
    type: [Number]
  }
});

// create a schema
export const deliverySchema = new Schema({
  orderRefId: {
    type: String,
  },
  deliveryRefId: {
    type: String,
  },
  orderType: {
    type: String,
    enum: ['onDemand', 'subscription'],
  },
  planType: {
    type: String,
    enum: ['lunch', 'fullDay'],
    default: 'fullDay'
  },
  shift: {
    type: String,
    enum: ['Breakfast', 'Lunch', 'Dinner'],
    default: 'Lunch'
    // required: [true, 'Shift is required.']
  },
  deliveryDate: {
    type: Date,
    required: [true, ' Delivery date is required.']
  },
  zone: {
    type: Schema.Types.Mixed,
    required: [true, 'zone required'],
  },
  defaultMeals: {
    type: [Schema.Types.Mixed],
  },
  meals: {
    type: [Schema.Types.Mixed],
    required: [true, 'meals required'],
  },
  subTotal: {
    type: Number,
  },
  total: {
    type: Number,
  },
  quantity: {
    type: Number,
  },
  load: {
    type: Number,
  },
  createdBy: {
    type: Schema.Types.Mixed,
    // required: [true, 'Created by is required.']
  },
  deliveryBoy: {
    type: Schema.Types.Mixed
  },
  van: {
    type: Schema.Types.Mixed,
    // required: [true, 'Provider is required.']
  },
  provider: {
    type: Schema.Types.Mixed,
    // required: [true, 'Provider is required.']
  },
  address: {
    type: Schema.Types.Mixed,
    // required: [true, 'Address is required.']
  },
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  paymentType: {
    type: String,
    enum: ['cash', 'card'],
    // required: [true, 'Payment Type is required.']
  },
  deliveryNote: {
    type: String
  },
  mealPlan: {
    type: String,
  },
  sequence: {
    type: Number,
    default: 0
  },
  status: {
    type: String,
    enum: ['pending', 'cooking', 'ready', 'onTheWay', 'accepted', 'denied', 'completed', 'canceled','Archived'],
    default: 'pending'
  },
  createdAt: Date,
  updatedAt: Date
}, { versionKey: false });

// on every save, add the date
deliverySchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  if (!this.deliveryRefId) {
    this.deliveryRefId = `akly-D-${shortid.generate()}`;
  }
  next();
});

deliverySchema.post('insertMany', function (result) {
  _.each(result, (data) => {
    if (data.orderType === 'onDemand') {
      let channelName = 'deliveries-' + data.provider._id;
      let information = "new delivery inserted";
      pubnubNotification(channelName, information, { orderRefId: data.orderRefId })
    }
  });
});

deliverySchema.post('save', function (result) {
  console.log('------- insert delivery ----- ');
  console.log(result);
  console.log('------- end insert delivery ----- ');
  if (result.status === 'pending') {
    let channelName = 'deliveries-' + result.provider._id;
    let information = "new delivery inserted";
    pubnubNotification(channelName, information, { orderRefId: result.orderRefId });
  }

});

// we need to create a model using it
const Delivery = mongoose.model('Delivery', deliverySchema);

// make this available in our applications
export default Delivery;
