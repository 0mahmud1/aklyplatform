/**
 * Created by sajid on 5/24/17.
 */

import _ from 'lodash';
import Delivery from '../models';
import { logger } from 'libs/logger.js';
import { weeklyFormat } from '../lib/index';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';


/**
 * @api {get} /api/deliveries list of delivery
 * @apiPermission All
 * @apiVersion 1.0.0
 * @apiName GetDeliveries
 * @apiGroup Delivery
 *
 * @apiHeader {String} Authorization valid access token
 * @apiParam { String } [deliveryDate] YYYY-MM-DD format. match with given date(s) (ex. 2017-02-12 or 2017-01-13, 2017-01-14)
 * @apiParam { String } [status] 'pending', 'cooking', 'ready', 'onTheWay', 'accepted', 'denied', 'completed' (ex. onTheway or onTheway,ready)
 * @apiParam { String } [deliveryDateRange] YYYY-MM-DD format. match with given range. (ex.2017-01-01(low), 2017-01-31(high))
 * @apiParam { String } [orderRefId] match with given orderRefId(s).
 *
 * @apiSuccess {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess {String} message Read a delivery
 * @apiSuccess {Object} data see Details of objects can be viewed in their related api
 * @apiSuccessExample {JSON} Success-Response:
 * {
 *      "success": true,
 *       "message": "Read a delivery",
 *       "data": [{
 *           "_id": String,
 *           "orderRefId": String,
 *           "deliveryDate": Date,
 *           "orderType": String,
 *           "paymentType": String,
 *           "address": Object,
 *           "locations": [longitude, latitude],
 *           "zone": Object,
 *           "provider": Object,
 *           "total": Number,
 *           "load": Number,
 *           "subTotal": Number,
 *           "createdBy": Object,
 *           "deliveryRefId": String,
 *           "deliveryNote": String,
 *           "createdAt": Date,
 *           "updatedAt": Date,
 *           "status": String,
 *           "sequence": Number,
 *           "meals": [Object],
 *           "planType": String
 *       }]
 * }
 */
export const getDeliveries = (req, res, next) => {
    logger.info('-------function getDeliveries --------');
    logger.info('function caller email: ', req.decoded.email);
    try {
        // logger.info('delivery query: ', req.query);

        let isWeeklyFormat = false;                     //
        let limit = parseInt(req.query.limit);          //query limit from request, default is 20
        let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
        let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

        let query = {};                                 //empty query object
        // @todo insert required queries in query param
        if (!req.decoded.roles || req.decoded.roles[0] === 'customer') {//if customer is calling the function
            query['createdBy.email'] = req.decoded.email;               //query using customer's email to get his/her deliveries
        } else if (req.decoded.roles[0] === 'vanOperator') {            //else if vanOperator is calling the function
            query['van.email'] = req.decoded.email;                     //query using vanOperator's email to get deliveries assigned to that van
        } else if (req.decoded.roles[0] === 'foodProvider') {           //else if foodProvider is calling the function
            query['provider.email'] = req.decoded.email;                //query using foodProvider's email to get deliveries assigned to that foodProvider
        } else if (req.decoded.roles[0] === 'deliveryBoy') {            //else if deliveryBoy is calling the function
            query['deliveryBoy.email'] = req.decoded.email;             //query using deliveryBoy's email to get deliveries assigned to that deliveryBoy
        }
        // console.log('delivery Date >>>',req.query['deliveryDate']);
        if(req.query['deliveryDate']) {                 //if need to get deliveries of selected dates,
            let deliveryDates = _.map(req.query['deliveryDate'].split(','), dDate => {//an array of date objects are created
                return new Date(dDate);
            });
            query['deliveryDate'] = { $in: deliveryDates };
        }
        if(req.query['deliveryDateRange']) {            //if need to get all deliveries in date range
            let rangeDates = _.map(req.query['deliveryDateRange'].split(','), dDate => {
                return new Date(dDate);
            });
            query['deliveryDate'] = { $gte: rangeDates[0], $lte: rangeDates[1] };//get all deliveries in that date range
        }
        if(req.query['orderRefId']) {                   //query by orderRefId
            query['orderRefId'] = req.query['orderRefId'];
        }
        if(req.query['status']){                        //query by status
            query['status'] = {
                $in: req.query['status'].split(','),
            }
        }
        if(req.query['orderType']) {                    //query by orderType
            query['orderType'] = {
                $in: req.query['orderType'].split(','),
            }
        }
        if(req.query['deliveryRefId']) {                //query by deliveryRefId
            query['deliveryRefId'] = req.query['deliveryRefId'];
        }
        if(req.query['cuemail']) {                      //admin search customer's by customer email
            let cuemail = req.query['cuemail'];
            let str = RegExp.escape(cuemail);
            query['createdBy.email'] = RegExp(str,'i');
        }
        if(req.query['shift']) {                        //query by shift
            query['shift'] = RegExp(req.query['deliveryRefId'],'i');
        }
        let findDataPromise = {};
        if(req.query['sortByOrderDate'] === 'true') {   //if result needed to be sorted
            // console.log('sortByOrderDate here ',req.query['sortByOrderDate']);
            findDataPromise = Delivery.find(query).limit(limit).skip(skip).sort({'createdAt': -1});//final query for sorted data
        } else {
            findDataPromise = Delivery.find(query).limit(limit).skip(skip);//final query for unsorted data
        }
        if(req.query['format']) {                       //formatting data in weekly format
            isWeeklyFormat = (req.query['format'] === 'week');//set isWeeklyFormat to true or false
        }
        // logger.info('delivery query: ', query);
        //this promise returns number of total object
        let findCountPromise = Delivery.find(query).count();

        //this promise returns actual data
        Promise.all([findCountPromise , findDataPromise])//execute all queries using Promise
            .then(response => {                         //get result from promise query execution
                return res.status(200).send({           //response with related data and other properties
                    total: response[0],                 //return value of findCountPromise
                    limit: limit ? limit : 0,           //if there is no limit set limit to 0
                    skip: skip,
                    page: parseInt(page),
                    success: true,
                    message: 'Get all active deliveries',
                    data: isWeeklyFormat ? weeklyFormat(response[1]): response[1] //return value of findDataPromise
                })
            })
            .catch((error) => {                         //error in promise
                logger.debug(error.stack);
                logger.error('error created by: ',req.decoded.email)
                logger.error(error);                    //log error
                next(error)                             //pass error to next function
            });
    } catch (e) {                                       //try can not be executed, error occurred
        logger.debug(e.stack);
        logger.error('error created by: ',req.decoded.email)
        logger.error(e);                                //log error
        return next(e);                                 //pass error to next function
    }
};