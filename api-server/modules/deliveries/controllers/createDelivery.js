/**
 * Created by sajid on 5/24/17.
 */

import _ from 'lodash';
import shortid from 'shortid'
import async from 'async'
import Delivery from '../models';
import Food from '../../foods/models/index'
import Order from 'modules/orders/models/index';
import { logger } from 'libs/logger.js';
import { updateOrder } from '../lib/index';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';


/**
 * @api {POST} /api/deliveries Create an delivery
 * @apiName CreateDelivery
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiPermission administrator, manager
 * @apiParam {Object} data See Request-Response
 * @apiParamExample {JSON} Request-Response:
 * {
*       "orderRefId": String,
*       "deliveryDate": Date,
*       "shift": String,
*       "meals": [
*           {
*              "quantity": Number
*              "item": {
*                 "_id": String,
*                }
*           }
*       ]
* }
 *
 *@apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Create a delivery
 * @apiSuccess (200) {Object} data see Details of objects can be viewed in their related api
 * @apiSuccessExample {JSON} Success-Response:
 * {
*      "success": true,
*       "message": "Create a delivery",
*       "data": {
*           "_id": String,
*           "orderRefId": String,
*           "deliveryDate": Date,
*           "deliveryNote": String,
*           "orderType": String,
*           "paymentType": String,
*           "address": Object,
*           "locations": [longitude, latitude],
*           "zone": Object,
*           "provider": Object,
*           "total": Number,
*           "load": Number,
*           "subTotal": Number,
*           "createdBy": Object,
*           "deliveryRefId": String,
*           "createdAt": Date,
*           "updatedAt": Date,
*           "status": String,
*           "sequence": Number,
*           "planType": String,
*           "meals": [
*               {
*              "quantity": Number
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": String,
*                "status": String,
*                "load": Number,
*                "images": [
*                  String
*                ],
*                "tags": [
*                  {
*                    "status": String,
*                    "images": [
*                      String
*                    ],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*            }
*           ]
*      }
* }
 */
export const createDelivery = (req, res, next) => {
    logger.info('-------function createDelivery --------');
    logger.info('function caller email: ', req.decoded.email);//to check who is calling the function
    try {                                                     //try to create a delivery
        // logger.info('createDelivery',req.body);
        if (!_.has(req.body, 'orderRefId')) {                 //if request body has no orderRefId, return 404 error
            res.status(404).json({
                success: false,
                message: 'Order Ref. Id not found in request',
                data: null
            })
        }
        const newDelivery = new Delivery()                    //create delivery Object
        newDelivery.load = 0                                  //set load, quantity, total and subtotal to 0 as it will be calculated
        newDelivery.quantity = 0
        newDelivery.total = 0
        newDelivery.subTotal = 0
        function prepareMeal(data, cb) {                      //function to calculate meal properties
            logger.info('-------call function createDelivery / prepareMeal --------');
            // console.log(data);
            const meal = {};                                  //empty Object

            meal.quantity = parseInt(data.quantity);          //set meal quantity from passed data
            Food.findOne({_id: data.item._id}).exec()         //find food in Food collection using food _id
                .then(food => {
                    if (food) {                               //if food found, calculate meal properties
                        meal.item = food;
                        newDelivery.load += parseInt((food.load * parseInt(data.quantity)));
                        newDelivery.total += (food.price * parseInt(data.quantity));
                        newDelivery.subTotal += (food.price * parseInt(data.quantity));
                        newDelivery.quantity += parseInt(data.quantity);
                        cb(null, meal);                       //pass meal Object to callback
                    } else {                                  //if food not found,
                        cb('food not found',null)                  //pass a message to callback
                    }
                }).catch(e =>{                                //food not found in Food DB, error occurred
                logger.error('error created by: ',req.decoded.email)
                logger.error(e)                               //log error
                cb(e, null);                                  //pass error to callback
            });
        }

        async.parallel({                                      //asynchronous parallel function call
            getOrder: function (callback) {                   //get the order for whom delivery is being created
                Order.findOne( { orderRefId: req.body.orderRefId }).exec()
                    .then((order)=>{                          //order found in Order collection
                        logger.info('inside async getOrder, found order ', order.orderRefId)
                        callback(null, order);                //pass order object to callback
                    })
                    .catch((error) => {                       //if order not in collection, error occurs
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error);                  //log error
                        callback(error, null);                      //pass error to callback
                    });
            },
            createMeal: function (callback) {                 //create meals from passed meal item _id and quantity
                let meals = req.body.meals
                async.map(meals, prepareMeal, function (error, result) { //async call to prepareMeal function
                    if(error){                                //if error occurs, pass error to callback
                        logger.error('inside async create Meal, error')
                        callback(error, null);
                    } else {                                  //if success, pass result to callback
                        logger.info('inside async create Meal, success')
                        callback(null, result);
                    }
                })
            }
        }, function (err, result) {                           //final callback function to async
            if( !err){                                        //if no error occurs, get necessary information from order object and assign to delivery object
                logger.info('reached async final callback')
                newDelivery.deliveryDate = req.body.deliveryDate;
                newDelivery.planType = result.getOrder.planType;
                newDelivery.shift = req.body.shift;
                newDelivery.orderRefId = result.getOrder.orderRefId;
                newDelivery.paymentType = result.getOrder.paymentType;

                newDelivery.deliveryNote = result.getOrder.deliveryNote;
                newDelivery.van = {};
                newDelivery.zone = result.getOrder.zone;
                newDelivery.provider = result.getOrder.provider;
                newDelivery.locations = result.getOrder.locations;
                newDelivery.createdBy = result.getOrder.createdBy;
                newDelivery.address = result.getOrder.address;
                newDelivery.orderType = result.getOrder.orderType;
                newDelivery.defaultMeals = result.createMeal
                newDelivery.createdBy = result.getOrder.createdBy;
                newDelivery.deliveryRefId = `akly-D-${shortid.generate()}`;
                newDelivery.meals = result.createMeal
                logger.info('async create delivery completed')
                // logger.info( newDelivery)
                newDelivery.save()                            //save created delivery in Delivery collection
                    .then((response) => {                     //after inserting data to Delivery collection
                        updateOrder(response);                //update order collection with new created deliveries
                        return res.status(200).json({         //response with 200 code
                            success: true,
                            message: 'Create a delivery',
                            data: response
                        })
                    })
                    .catch((error) => {                       //if any error occurs in creating delivery
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error);                  //log error
                        res.status(400).json({                //response with 400 error code
                            success: false,
                            message: 'Failed to create a delivery',
                            data: error
                        })
                    });
            }
            else {                                            //error in final callback of async
                logger.error('Failed to create a delivery');
                logger.error('error created by: ',req.decoded.email)
                logger.error(err);                            //log error
                return res.status(404).json({                 //response with 404 error code
                    success: false,
                    message: err.message,
                    data: err
                })
            }

        })
    } catch (error) {                          //try failed, error occurred, executing catch block
        logger.error('error created by: ',req.decoded.email)//from whose request error created
        logger.error(error);                   //log error
        return next(error);                    //pass error to next function
    }
};