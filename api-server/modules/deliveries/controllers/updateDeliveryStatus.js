/**
 * Created by sajid on 5/24/17.
 */

import Delivery from '../models';
import Order from '../../orders/models/index'
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { sendTransactionalEmail } from 'libs/emails.js'
import moment from 'moment';

export const updateDeliveryStatus = (req, res, next) => {
    logger.info('-------function updateDeliveryStatus --------');
    logger.info('function caller email: ', req.decoded.email);
    try{                                                   //try to update delivery status
        Delivery.findOne({_id: req.params._id}).exec()     //find delivery by delivery _id
            .then((delivery) => {
                logger.info('delivery to be status update', delivery.deliveryRefId);
                logger.info(req.body)
                if(req.body['status']){
                    delivery.status = req.body['status']
                    if(req.body['status']==='cancel'){     //if delivery is cancelled, remove delivery boy
                        delivery.deliveryBoy = undefined;
                    }
                    let messageBody = "";
                    if(req.body['status'] === 'completed') {
                      messageBody = 'Your Order has been completed! Please pick your order from delivery boy :)';
                      messageBody += '<br><br>Order reference Id is ' + delivery.orderRefId;
                      messageBody += '<br><br>Order Type : ' + delivery.orderType;
                      messageBody += '<br><br>The foods you selected are: <br><br>';
                      let foodLists = delivery.meals;
                      foodLists.map((list,index) => {
                        messageBody += (index + 1) + '. ' +list.item.name + " (" + list.quantity + "pcs)<br>";
                      })
                      sendTransactionalEmail(null,delivery.createdBy,{           // send mail to customer who created order
                        subject: `Dear ${delivery.createdBy.name}, Order Completed - ${delivery.deliveryRefId}`,
                        html: messageBody
                      },function(err,res) {
                        if(err) {
                          logger.error('Failed to send email')
                          logger.error('error created by: ',req.decoded.email)
                          logger.error(err);
                          return res.status(400).json({
                            success: false,
                            message: 'Failed to send email',
                            data: err
                          });
                        }
                        logger.info('Email sent successfully');
                      })
                    }
                    return delivery.save();
                }
            })
            .then((updateDeliveryStatus) => {
                // console.log("updateDeliveryStatus")
                // console.log(updateDeliveryStatus)
                if(updateDeliveryStatus.orderType==="onDemand"){    //if onDemand order, then update status in order collection
                    try{
                        Order.findOne({"orderRefId":updateDeliveryStatus.orderRefId}).exec()
                            .then( order => {
                                order.status= updateDeliveryStatus.status
                                return order.save()
                            })
                            .catch(error => {
                                logger.error('error created by: ',req.decoded.email)
                                logger.error(error)                        //log error
                                return next(error);                        //pass error to next function
                            })

                    } catch (error){
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error)                                  //log error
                        return next(error)                                   //pass error to next function
                    }
                }
            })
            .then(updateOrderStatus=>{
                // console.log("updateOrderStatus")
                // console.log(updateOrderStatus)
                return res.status(200).json(placeHolder(true, 'Update delivery status'))//response with 200
            })
            .catch((error) => {                            //error in finding delivery
                logger.error('error created by: ',req.decoded.email)
                logger.error(error)                        //log error
                return next(error);                        //pass error to next function
            });

    } catch (err) {                                        //try failed, catch executed
        logger.error('error created by: ',req.decoded.email)
        logger.error(err)                                  //log error
        return next(err)                                   //pass error to next function
    }
};
