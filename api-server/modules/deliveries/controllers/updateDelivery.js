/**
 * Created by sajid on 5/24/17.
 */

import _ from 'lodash';
import moment from 'moment'
import async from 'async'
import Delivery from '../models';
import Order from '../../orders/models/index'
import Food from '../../foods/models/index'
import { logger } from 'libs/logger.js';
import { assignDeliveryBoy, notifyDeliveryBoy, updateOrder } from '../lib/index';
import { pubnubNotification, pubnubNotificationiOS, pubnubNotificationiOSsubscription } from '../../../libs/pubnub_notifications.js'
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import DeliveryBoy from 'modules/deliveryBoys/models/index.js';

/**
 * @api {PUT} /api/deliveries/:_id Update an delivery
 * @apiName UpdateDelivery
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiPermission administrator, manager, customer, foodProvider, deliveryBoy, vanOperator
 * @apiParam {String} _id Delivery unique ID
 * @apiParam {Object} data See Request-Response
 * @apiParamExample {json} Request-Example:
 * {
*       "meals": [
*           {
*              "quantity": Number
*              "item":
*                {
*                  "_id": String,
*                }
*           }
*       ]
*    }
 *
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Read a delivery
 * @apiSuccess (200) {Object} data Details of objects can be viewed in their related api
 * @apiSuccessExample {JSON} Success-Response:
 * {
*      "success": true,
*       "message": "Read a delivery",
*       "data": {
*           "_id": String,
*           "orderRefId": String,
*           "deliveryDate": Date,
*           "deliveryNote": String,
*           "orderType": String,
*           "paymentType": String,
*           "address": Object,
*           "locations": [longitude, latitude],
*           "zone": Object,
*           "provider": Object,
*           "total": Number,
*           "load": Number,
*           "subTotal": Number,
*           "createdBy": Object,
*           "deliveryRefId": String,
*           "createdAt": Date,
*           "updatedAt": Date,
*           "status": String,
*           "sequence": Number,
*           "planType": String,
*           "meals": [
*               {
*              "quantity": Number
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": String,
*                "status": String,
*                "load": Number,
*                "images": [
*                  String
*                ],
*                "tags": [
*                  {
*                    "status": String,
*                    "images": [
*                      String
*                    ],
*                    "name": String,
*                    "updatedAt": Date,
*                    "createdAt": Date,
*                    "_id": String
*                  },
*                ]
*              },
*            }
*           ]
*      }
* }
 */
export const updateDelivery = (req, res, next) => {
    logger.info('-------function updateDelivery --------');
    logger.info('function caller email: ', req.decoded.email);
    try {                                                  //try to update a delivery
        // logger.info(req.body);
        let load = 0, quantity = 0, total = 0, subTotal = 0;
        function prepareMeal(data, cb) {                   //function to support async parallel
            logger.info('-------function updateDelivery/prepareMeal --------');
            // console.log(data);
            const meal = {};                               //empty meal object

            meal.quantity = parseInt(data.quantity);
            Food.findOne({ _id: data.item._id }).exec()      //find food fromm Food colection using food _id
                .then(food => {                            //if food found, calculate meal properties
                    if (food) {
                        meal.item = food;
                        load += parseInt((food.load * parseInt(data.quantity)));
                        total += (food.price * parseInt(data.quantity));
                        subTotal += (food.price * parseInt(data.quantity));
                        quantity += parseInt(data.quantity);
                        cb(null, meal);                    //pass meal to callback
                    } else {                               //if error, pass message to callback
                        cb('food not found', null)
                    }
                }).catch(e => {                             //error in find food
                    logger.error('-------error in updateDelivery/prepareMeal --------')
                    logger.error('error created by: ', req.decoded.email)
                    logger.error(e)                            //log error
                    cb(e, null);                               //pass error to callback
                });
        }

        async.parallel({
            getDelivery: function (callback) {             //function to get delivery
                Delivery.findOne({ _id: req.params._id }).exec()//get delivery by _id
                    .then((delivery) => {                    //if delivery found
                        logger.info('inside async getDelivery delivery id: ', delivery.deliveryRefId)
                        if (req.body.status) {             //if status in request body, change delivery status with it
                            delivery.status = req.body.status;
                        }
                        callback(null, delivery);          //pass delivery to callback
                    })
                    .catch((error) => {                    //error in find delivery
                        logger.error('error created by: ', req.decoded.email)
                        logger.error(error);               //log error
                        callback(error, null);                   //pass error to next function
                    });
            },
            createMeal: function (callback) {              //function to create meals from request body meal ids
                if (req.body.meals) {
                    let meals = req.body.meals
                    async.map(meals, prepareMeal, function (error, result) {
                        logger.info('inside async create Meal in update delivery, meal created')
                        callback(null, result);            //pass result to callback
                    })
                } else {
                    callback(null, {})
                }
            }
        }, function (err, result) {                        //final callback of async
            if (!err) {                                      //if no error
                logger.info('async update final delivery result success')
                let delivery = result.getDelivery
                if (req.body.meals) {                      //assign meal properties
                    delivery.load = load
                    delivery.total = total
                    delivery.subTotal = subTotal
                    delivery.quantity = quantity
                    delivery.meals = result.createMeal
                    // delivery.defaultMeals = result.createMeal
                }
                // logger.info(delivery)
                // if orderType onDemand
                if (delivery.orderType === 'onDemand') {   //update onDemand delivery
                    logger.info('Ondemand delivery');
                    if (delivery.status === 'ready') {     //if delivery status is ready
                        logger.info('delivery is ready %s', delivery.deliveryRefId);
                        assignDeliveryBoy({                //assign delivery boy
                            _id: req.params._id,
                            status: delivery.status
                        }).then(response => {              //get response from assignDeliveryBoy

                            logger.info('response from assignDeliveryBoy');
                            // logger.info(response.deliveryBoy.toObject());
                            if (_.has(response, 'deliveryBoy')) {//delivery boy found
                                logger.info('deliveryBoy found')
                                // console.log(response.deliveryBoy);
                            } else {                       //delivery boy not found
                                logger.error('error created by: ', req.decoded.email)
                                logger.error('deliveryBoy not found')
                                // console.log('do not find')
                            }

                            Delivery.update({ _id: delivery._id }, delivery, function (err) {
                                if (err) {
                                    return next(err);      //if error in saving delivery, pass error to next function
                                } else {
                                    let channelName = 'deliveries-' + delivery.provider._id;
                                    let information = `delivery is ${delivery.status}`;
                                    pubnubNotification(channelName, information, {deliveryRefId: delivery.deliveryRefId, status: delivery.status})
                                    return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));//response with 200 and data
                                }
                            });

                        }).catch(error => {                //error in getting response from assignDeliveryBoy
                            logger.error('error created by: ', req.decoded.email)
                            logger.error(error)            //log error
                            return next(errorPlaceHolder(400, false, error));//pass error to next function
                        });
                    } else if (_.indexOf(['onTheWay', 'accepted'], delivery.status) >= 0) {//if delivery status is onTheWay or accepted
                        notifyDeliveryBoy({                //notify delivery boy
                            _id: req.params._id,
                            status: delivery.status,
                            isBusy: false,
                        });

                        Delivery.update({ _id: delivery._id }, delivery, function (err) {
                            if (err) {
                                return next(err);      //if error in saving delivery, pass error to next function
                            } else {
                                let channelName = 'deliveries-' + delivery.provider._id;
                                let information = `delivery is ${delivery.status}`;
                                    pubnubNotification(channelName, information, {deliveryRefId: delivery.deliveryRefId, status: delivery.status})
                                return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));//response with 200 and data
                            }
                        });


                    } else if (delivery.status === 'completed' || delivery.status === 'denied') {//if delivery status is completed or denied

                        if (delivery.deliveryBoy) {
                            DeliveryBoy.findOne({ _id: delivery.deliveryBoy._id }).exec()  //update delivery boy status in deliveryBoy collection
                                .then((dboy) => {
                                    dboy.isBusy = false;
                                    dboy.save();
                                })
                                .catch((error) => {            //if error in updating delivery boy
                                    logger.error('Error in updating DBoy');
                                    logger.error('error created by: ', req.decoded.email)
                                    logger.error(error)        //log error
                                })
                        }


                        if (delivery.status === 'denied') { //if delivery status is undefined, remove delivery boy from delivery
                            delivery.deliveryBoy = undefined;
                        }

                        if (delivery.status === 'completed') {   // if any order completed , then notify customer 
                            console.log('delivery completed');
                            const notification = {
                                place: 'Collection',
                                name: 'deliveries',
                                type: 'status',
                                value: delivery.status,
                                id: delivery.createdBy._id,
                                messageType: "rateOnDemandOrder",
                                deliveryId: delivery._id,
                                orderRefId: delivery.orderRefId

                            };
                            // PubNub
                            let channelName = 'deliveries-' + delivery.createdBy._id;
                            let information = `delivery is ${delivery.status}`;
                            pubnubNotification(channelName, information, notification);
                            let options = {
                                messageType: "rateOnDemandOrder",
                                orderId:"",
                                orderRefId: delivery.orderRefId
                            };
                            try {                          //try to obtain order _id to notify iOS
                                Order.findOne({orderRefId: delivery.orderRefId}).exec()
                                    .then(order=>{
                                        options.orderId = order._id
                                        //send notification on iOS
                                        pubnubNotificationiOS(channelName,options)
                                    })
                                    .catch(error=>{
                                        logger.error('error created by: ', req.decoded.email)
                                        logger.error(error)                                //log error
                                        return next(error);                                //pass error to next function
                                    })
                            } catch (error){
                                logger.error('error created by: ', req.decoded.email)
                                logger.error(error)                                //log error
                                return next(error);                                //pass error to next function
                            }
                        }

                        Delivery.update({ _id: delivery._id }, {$set:{status:'completed'}}, function (err, result) {
                            if (err) {

                                return next(err);      //if error in saving delivery, pass error to next function
                            } else {
                                let channelName = 'deliveries-' + delivery.provider._id;
                                let information = `delivery is ${delivery.status}`;
                                    pubnubNotification(channelName, information, {deliveryRefId: delivery.deliveryRefId, status: delivery.status})
                                return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));//response with 200 and data
                            }
                        });

                    } else {
                        Delivery.update({ _id: delivery._id }, delivery, function (err) {
                            if (err) {
                                return next(err);      //if error in saving delivery, pass error to next function
                            } else {
                                let channelName = 'deliveries-' + delivery.provider._id;
                                let information = `delivery is ${delivery.status}`;
                                    pubnubNotification(channelName, information, {deliveryRefId: delivery.deliveryRefId, status: delivery.status})
                                return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));//response with 200 and data
                            }
                        });
                    }
                } else {                                   //update subscription delivery
                    logger.info('subscription delivery');

                    Delivery.update({ _id: delivery._id }, delivery, function (err) {
                        if (err) {
                            return next(err);              //if error in saving delivery, pass error to next function
                        } else {

                            if(delivery.status="completed" && moment(delivery.deliveryDate).format('dddd')==="Saturday"){ //push notification to iOS
                                let options = {
                                    messageType: "rateSubscriptionOrder",
                                    orderId:"",
                                    orderRefId: delivery.orderRefId,
                                    weekNum: 0
                                };
                                try {
                                    Order.findOne({orderRefId:delivery.orderRefId}).exec()
                                        .then(order=>{
                                            options.orderId = order._id
                                            let startDay = moment(order.firstDeliveryDate)
                                            let today = moment(delivery.deliveryDate)
                                            options.weekNum = today.diff(startDay,'weeks')  // different of today from start day in weeks to detect week
                                            let channelName = 'deliveries-' + delivery.createdBy._id;
                                            pubnubNotificationiOSsubscription(channelName, options)
                                        })
                                        .catch(error=>{
                                            logger.error('error created by: ', req.decoded.email)
                                            logger.error(error)                                //log error
                                            return next(error);                                //pass error to next function
                                        })
                                } catch (error){
                                    logger.error('error created by: ', req.decoded.email)
                                    logger.error(error)                                //log error
                                    return next(error);                                //pass error to next function
                                }
                            }
                            
                            if (req.query['updateOrder'] === 'true') {// if delivery is updated, reflect them in order collection to
                                updateOrder(delivery);
                                return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));
                            } else {
                                return res.status(200).json(placeHolder(true, 'Update a delivery', delivery));
                            }

                        }
                    });

                }
            } else {                                       //error occurred
                logger.error('Failed to update delivery');
                logger.error('error created by: ', req.decoded.email)
                logger.error(err);                         //log error
                return res.status(404).json({              // response with 404 error
                    success: false,
                    message: err.message,
                    data: err
                })
            }
        })
    } catch (error) {                                      //update delivery try failed, error occurred
        logger.error('error created by: ', req.decoded.email)
        logger.error(error)                                //log error
        return next(error);                                //pass error to next function
    }
};
