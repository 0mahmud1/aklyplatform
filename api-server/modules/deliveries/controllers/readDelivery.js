/**
 * Created by sajid on 5/24/17.
 */

import Delivery from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';


/**
 * @api {GET} /api/deliveries/:_id Read an delivery
 * @apiName ReadDelivery
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiParam {String} _id Delivery unique ID
 * @apiPermission administrator, manager, customer, foodProvider, deliveryBoy, vanOperator
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Read a delivery
 * @apiSuccess (200) {Object} data see Details of objects can be viewed in their related api
 * @apiSuccessExample {JSON} Success-Response:
 * {
*      "success": true,
*       "message": "Read a delivery",
*       "data": {
*           "_id": String,
*           "orderRefId": String,
*           "deliveryDate": Date,
*           "deliveryNote": String,
*           "orderType": String,
*           "paymentType": String,
*           "address": Object,
*           "locations": [longitude, latitude],
*           "zone": Object,
*           "provider": Object,
*           "total": Number,
*           "load": Number,
*           "subTotal": Number,
*           "createdBy": Object,
*           "deliveryRefId": String,
*           "createdAt": Date,
*           "updatedAt": Date,
*           "status": String,
*           "sequence": Number,
*           "planType": String,
*           "meals": [Object]
*      }
* }
 */

export const readDelivery = (req, res, next) => {
    logger.info('-------function readDelivery --------');
    logger.info('function caller email: ', req.decoded.email);
    try {                                                  //try to read a delivery
        Delivery.findOne({_id: req.params._id}).exec()     //get delivery from Delivery collection with passed _id
            .then((delivery) => {
                //  logger.info('my delivery: ...', delivery.toObject());
                if (!delivery) {
                    logger.warn('query return null');
                    return next(errorPlaceHolder(403, false, 'no data found'));//if no delivery, pass error to next function
                }

                return res.status(200).json({              //if delivery found, response 200 with data
                    success: true,
                    message: 'Read a delivery',
                    data: delivery
                })
            })
            .catch((error) => {                            //if error in findOne catch executed
                logger.error('no delivery found')
                logger.error('error created by: ',req.decoded.email)
                logger.error(error)                        //log error
                next(error);                               //pass error to next function
            });

    } catch (err) {                                        //try failed, error occurred
        logger.error('error in readDelivery')
        logger.error('error created by: ',req.decoded.email)
        logger.error(error)                                //log error
        return next(err);                                  //pass error to next function
    }
};
