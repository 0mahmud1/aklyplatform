/**
 * Created by sajid on 5/24/17.
 */

import _ from 'lodash';
import axios from 'axios';
import config from 'config';
import async from 'async';
import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'
import Delivery from '../models/index';

/**
 * @api {PUT} /api/deliveries/subscriptions/ready Food provider is ready to deliver and foods are loaded in the vans
 * @apiName ReadySubscription
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiPermission administrator, manager, foodProvider
 * @apiParam {Object} data See Request-Response
 * @apiParamExample {json} Request-Example:
 * {
*    van._id: String
* }
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message delivery is loaded
 * @apiSuccess (200) {Object} data See Success-Response
 * @apiSuccessExample {JSON} Success-Response:
 * {
*      "success": true,
*       "message": "delivery is loaded",
*       "data": [{
*           "_id": String,
*           "orderRefId": String,
*           "deliveryDate": Date,
*           "deliveryNote": String,
*           "orderType": String,
*           "paymentType": String,
*           "address": Object,
*           "locations": [longitude, latitude],
*           "zone": Object,
*           "provider": Object,
*           "total": Number,
*           "load": Number,
*           "subTotal": Number,
*           "createdBy": Object,
*           "deliveryRefId": String,
*           "createdAt": Date,
*           "updatedAt": Date,
*           "status": String,
*           "sequence": Number,
*           "meals": [Object],
*           "van": Object
*       }]
*   }
 */

export function readySubscription(req, res, next) {
    logger.info('-------function readySubscription --------');
    logger.info('function caller email: ', req.decoded.email);
    try {                                                  //try to ready subscription deliveries
        const query = Object.assign({}, req.body);         //build query param from request body
        let vanId;

        const routeInfo = {};                              //routific info object

        routeInfo.fleet = {                                //routific fleet
            van: {
                start_location: {
                    id: "initial",
                    name: "provider",
                    lat: req.user.locations.coordinates[1],
                    lng: req.user.locations.coordinates[0]
                }
            }
        };

        Delivery.find(query).exec().then(deliveries => {   //find deliveries using query
            try {                                          //try to generate path using routific
                logger.info('total deliveries found for van ', deliveries.length);
                vanId = deliveries[0].van._id;
                routeInfo.visits = {};                     //routific visiting points object
                _.each(deliveries, data=> {                //create delivery location points
                    console.log(data.locations);
                    routeInfo.visits[data.deliveryRefId] = {//use deliveryRefId as visits key
                        location: {
                            name: data.deliveryRefId,
                            lat: data.locations.coordinates[1],
                            lng: data.locations.coordinates[0]
                        }
                    };
                });

                axios({                                    //post routific data to routific for generating routes
                    method: 'post',
                    url: config.get('ROUTIFIC.URL') + '/vrp',
                    data: routeInfo,
                    headers: { "Authorization": "bearer "+ config.get('ROUTIFIC.TOKEN') }
                }).then(function (response) {              //get response from routific
                    try {                                  //try updating deliveries
                        const sequenceList = response.data.solution.van;//get all location in sequence
                        let sequence = 0;

                        async.each(sequenceList, (data, callback) => {
                            if (data.location_id !== 'initial') {
                                Delivery.findOne({deliveryRefId: data.location_id}).then(delivery => {//find delivery by deliveryRefId
                                    try {                  //try update delivery status
                                        sequence += 1;
                                        delivery.sequence = sequence;
                                        delivery.status = 'onTheWay';
                                        delivery.save().catch(error=> { console.log(error)});
                                        // console.log('ok ... im here');
                                        callback(null, delivery.deliveryRefId);
                                    } catch (error) {      //error in update
                                        logger.error('error created by: ',req.decoded.email)
                                        logger.error(error);//log error
                                        callback(error,null);   //pass error to callback
                                    }
                                }).catch(error=>{          //error in finding delivery
                                    logger.error('error created by: ',req.decoded.email)
                                    logger.error(error);   //log error
                                    callback(error, null);       //pass error to callback
                                });
                            } else {
                                callback(null, data.location_id);
                            }
                        }, (error, data) => {
                            try {                          //try to send pubnub notification
                                const notification = {
                                    place: 'Collection',
                                    name: 'deliveries',
                                    type: 'status',
                                    value: 'onTheWay',
                                    data: data
                                };
                                // PubNub
                                let channelName = 'deliveries-' + vanId;
                                let information = 'delivery is loaded';
                                pubnubNotification(channelName,information,notification)

                                let anotherChannelName = 'deliveries-' + req.user._id
                                pubnubNotification(anotherChannelName, information, notification)

                                return res.status(200).json({//response with 200
                                    success: true,
                                    message: 'response',
                                    data: data
                                });
                            } catch (error) {              //error in pubnub notification
                                logger.error('error created by: ',req.decoded.email)
                                logger.error(error);       //log error
                                next(error);               //pass error to next function
                            }
                        });
                    } catch (error) {              //error in routific response
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error);               //log error
                        next(error);                       //pass error to next function
                    }
                }).catch(function (error) {
                    logger.error('error created by: ',req.decoded.email)
                    logger.error(error);
                    next(error);
                });
            } catch (error) {                      //find deliveries try failed, error occurred
                logger.error('error created by: ',req.decoded.email)
                logger.error(error);                       //log error
                next(error);                               //pass error to next function
            }
        });

    } catch (error) {                                      //try failed, error occurred
        logger.error('error created by: ',req.decoded.email)
        logger.error(error);                               //log error
        next(error);                                       //pass error to next function
    }
}