/**
 * Created by sajid on 5/24/17.
 */

import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';


/**
 * @api {DELETE} /api/deliveries/:_id DELETE an delivery
 * @apiName DeleteDelivery
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiParam {String} _id Delivery unique ID
 * @apiPermission administrator, manager
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Deleted a delivery
 * @apiSuccessExample {JSON} Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *    "success": "true",
 *    "message": "Deleted a delivery"
 *  }
 */

export const deleteDelivery = (req, res, next) => {
    logger.info('-------function deleteDelivery --------');
    logger.info('function caller email: ', req.decoded.email);//to check who is calling the function
    try {                                               //try to delete a delivery
        Delivery.findOne({_id: req.params._id}).exec()  //find one delivery to be deleted with its _id
            .then((delivery) => {                       //if the delivery with requested _id is found
                logger.info('delivery to be DELETED: ', delivery.deliveryRefId);
                delivery.status = 'Archived';           //make delivery status as Archived instead of removing from database
                return delivery.save();                 //save the delivery in collection
            })
            .then((deletedDelivery) => {                //after deleting, response with success message
                return res.status(200).json(placeHolder(true, 'Deleted a delivery'))
            })
            .catch((error) => {                         //if no delivery is found with provided _id, error occurs
                logger.error('error in deleteDelivery ')
                logger.error('error created by: ',req.decoded.email)
                logger.error(error)                     //log error
                return next(error);                     //pass error to next function
            });
    } catch (err) {                                     //try failed, executing catch block
        logger.error('error in deleteDelivery ')
        logger.error('error created by: ',req.decoded.email)
        logger.error(error)                             //log error
        return next(err);                               //pass error to next function
    }
};