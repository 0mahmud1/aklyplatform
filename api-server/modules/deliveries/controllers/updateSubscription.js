/**
 * Created by sajid on 5/24/17.
 */

import _ from 'lodash';
import axios from 'axios';
import config from 'config';
import moment from 'moment';
import async from 'async';

import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'
import Delivery from '../models/index';
import Van from 'modules/vans/models/index';

/**
 * @api {PUT} /api/deliveries/subscriptions update status by provider
 * @apiName UpdateSubscription
 * @apiGroup Delivery
 * @apiHeader {String} Authorization valid access token
 * @apiPermission administrator, manager, foodProvider
 * @apiSuccess {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess {String} message delivery is ready
 * @apiSuccess {Object} data See Success-Response
 * @apiSuccessExample {JSON} Success-Response:
 * {
*      "success": true,
*       "message": "delivery is ready",
*       "data": [{
*           "_id": String,
*           "orderRefId": String,
*           "deliveryDate": Date,
*           "deliveryNote": String,
*           "orderType": String,
*           "paymentType": String,
*           "address": Object,
*           "locations": [longitude, latitude],
*           "zone": Object,
*           "provider": Object,
*           "total": Number,
*           "load": Number,
*           "subTotal": Number,
*           "createdBy": Object,
*           "deliveryRefId": String,
*           "createdAt": Date,
*           "updatedAt": Date,
*           "status": String,
*           "sequence": Number,
*           "meals": [Object],
*           "van": Object
*       }]
* }
 */

export function updateSubscription(req, res, next) {
    logger.info('-------function updateSubscription --------');
    logger.info('function caller email: ', req.decoded.email);
    try {                                                  //update status of subscription deliveries
        const routific = {};                               //routifc data object
        let vanList = {};

        async.parallel({
            delivery: function(callback) {                 //get deliveries
                try {
                    let query = {};
                    let visits = {};
                    let myVisits = {}
                    query.orderType = 'subscription';
                    if (_.isEmpty(req.body)) {
                        /**
                         * check if today is Thursday or not, if thursday, then search also for
                         * saturday's deliveries, otherwise only search for today's deliveries
                         */
                        let today = moment(new Date()).format("YYYY-MM-DD");
                        let dayNow = moment(today).format('dddd')
                        if (dayNow==='Thursday'){
                            let deliveryDates = []
                            deliveryDates.push(today)
                            deliveryDates.push(moment(today).add(2,'day').format("YYYY-MM-DD"))
                            query['deliveryDate'] = { $in: deliveryDates };
                        } else {
                            query.deliveryDate = moment().format('YYYY-MM-DD');
                        }
                    }

                    if (req.decoded.roles[0] === 'foodProvider' ) {//if food provider calls this function
                        Object.assign(query, { 'provider.email': req.user.email });
                    }

                    if (_.has(req.body, 'deliveryDate')) { //if deliveryDate in request body
                        Object.assign(query, { deliveryDate: moment(new Date(req.body.deliveryDate)).format('YYYY-MM-DD') });
                    }

                    if (_.has(req.body, 'deliveryRefId')) {//if deliveryRefId in request body
                        let deliveryRefIds = req.body['deliveryRefId'].split(',');
                        Object.assign(query, { deliveryRefId: { $in: deliveryRefIds } });
                    }

                    Delivery.find(query).exec().then(deliveries => {//find deliveries
                            let groupByOrderRefId = _.groupBy(deliveries, 'orderRefId') //rearrange deliveries by orderRefId
                            // now concat orders for same orderReId
                            try {                          //try to create data for routific
                                _.each(groupByOrderRefId, (order, key)=>{
                                    // console.log('key==',key,'\norder\n', order)
                                    let data = {
                                        location: {
                                            name: order[0].createdBy.email,
                                            lat: order[0].locations.coordinates[1],
                                            lng: order[0].locations.coordinates[0],
                                        },
                                        load: 0,
                                    }
                                    //calculate total load(total meal)
                                    data.load = _.sumBy(order, (item)=>{
                                        return item.load
                                    })
                                    myVisits[key] = data
                                })
                                // logger.info('my visits========',myVisits)
                                callback(null, myVisits)   //pass myVisits to callback
                            } catch (error){               //error in creating routific data
                                logger.error('error created by: ',req.decoded.email)
                                logger.error(error);       //log error
                                callback(error, null);           //pass error to callback
                            }
                        }
                    ).catch(error => {                     //error in finding deliveries
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error);               //log error
                        callback(error, null);                   //pass error to callback
                    });
                } catch (error) {                          //error in get deliveries
                    logger.error('error created by: ',req.decoded.email)
                    logger.error(error);                   //log error
                    callback(error, null)                        //pass error to callback
                }
            },
            van: function(callback) {                      //async function to get van
                try {
                    const vanIds = _.map(req.user.vans, van => { return van._id});//get van ids from food providers van list
                    let fleets = {};
                    if (vanIds.length === 0) {             //if no vans available
                        logger.error('error created by: ',req.decoded.email)
                        logger.error('van not found in provider');//log error
                        callback(new Error('no van found in provider'), null);//pass error to calback
                    }
                    Van.find({ _id: { $in: vanIds }}).exec().then(vans => {//finid vans from Van collection
                        try {                              //try to create routific data for van
                            vanList = vans;
                            _.each(vans, van=> {
                                fleets[van._id.toString()] = {
                                    start_location: {
                                        id: van._id.toString(),
                                        name: van.email,
                                        lat: req.user.locations.coordinates[1],
                                        lng: req.user.locations.coordinates[0],
                                    },
                                    capacity: van.capacity,
                                }
                            });
                            callback(null, fleets);        //pass routific data to callback
                        } catch (error) {                  //error in creating routific data
                            logger.error('error created by: ',req.decoded.email)
                            logger.error(error);           //log error
                            callback(error, null);               //pass error to callback
                        }
                    });
                } catch (e) {                              //function to get van have error, catch executed
                    logger.error('error created by: ',req.decoded.email)
                    logger.error(e);                       //log error
                    callback(e, null);                           //pass error to callback
                }
            }
        }, function(err, results) {                        //callback to async
            if (!err) {                                    //all function worked well
                try {                                      //callback to async, try to update routific route
                    routific.visits = results.delivery;    //routific data
                    routific.fleet = results.van;          //routific data

                    logger.info('==========routific success==========')
                    axios({                                //post data to routific
                        method: 'post',
                        url: config.get('ROUTIFIC.URL') + '/vrp',
                        data: routific,
                        headers: { "Authorization": "bearer " + config.get('ROUTIFIC.TOKEN') }
                    }).then(response => {                  //get response from routific
                        try {                              //update deliveries in collection
                            logger.info(response.data);
                            const routificData = response.data;
                            async.map(_.keys(routificData.solution), (vanId, callback)=>{ //assign deliveries to van
                                const van = _.find(vanList, van=> { return van._id.toString() === vanId; });
                                let orderRefIds = []
                                let deliveryRefIds = [];
                                // get orderRefIds first
                                _.each(routificData.solution[vanId], data=>{
                                    if (data.location_id !== vanId) {
                                        orderRefIds.push(data.location_id);
                                    }
                                });
                                // now get deliveryRefIds for these orderRefId for today
                                let newQuery = {}
                                Object.assign(newQuery, {orderRefId: { $in: orderRefIds }})
                                if (_.isEmpty(req.body)) {
                                    /**
                                     * check if today is Thursday or not, if thursday, then search also for
                                     * saturday's deliveries, otherwise only search for today's deliveries
                                     */
                                    let today = moment(new Date()).format("YYYY-MM-DD");
                                    let dayNow = moment(today).format('dddd')
                                    if (dayNow==='Thursday'){
                                        let deliveryDates = []
                                        deliveryDates.push(today)
                                        deliveryDates.push(moment(today).add(2,'day').format("YYYY-MM-DD"))
                                        newQuery['deliveryDate'] = { $in: deliveryDates };
                                    } else {
                                        newQuery.deliveryDate = moment().format('YYYY-MM-DD');
                                    }
                                }
                                if (_.has(req.body, 'deliveryDate')) {
                                    Object.assign(newQuery, { deliveryDate: moment(new Date(req.body.deliveryDate)).format('YYYY-MM-DD') });
                                }
                                Delivery.find(newQuery).then((delivery)=>{//get deliveryRefId in an array
                                    deliveryRefIds = _.map(delivery, (item)=>{
                                        return item.deliveryRefId
                                    })
                                    // console.log('deliveryRefIds after routific solution========', deliveryRefIds)
                                    // console.log('orderRefIds after routific solution========', orderRefIds)
                                }).then((founded)=>{       //update deliveries in collection
                                        Delivery.update( {deliveryRefId: { $in: deliveryRefIds }}, {
                                            $set: { van: van, status: 'ready' }
                                        }, { multi: true }, (error, docs) => {
                                            if (error) {
                                                callback(error, null);
                                            }
                                            // console.log('vanId ===>>', vanId);
                                            // console.log('van ===>>', van);
                                            const notification = {
                                                place: 'Collection',
                                                name: 'deliveries',
                                                type: 'status',
                                                value: 'ready',
                                                deliveryRefId: deliveryRefIds
                                            };
                                            // PubNub notification
                                            let channelName = 'deliveries-' + vanId;
                                            let information = 'delivery is ready';
                                            pubnubNotification(channelName, information, notification)
                                            let anotherChannelName = 'deliveries-' + req.user._id;
                                            pubnubNotification(anotherChannelName,information,notification)
                                            callback(null, van._id)
                                        });
                                    }
                                ).catch(error => {         //update in collection failed
                                    logger.error('error created by: ',req.decoded.email)
                                    logger.error(error);   //log error
                                    next(error)            //pass error to next function
                                })
                            }, (error , results) => {      //callback to async map
                                if (error) {               //if error occurs
                                    logger.error('error created by: ',req.decoded.email)
                                    logger.error(error)    //log error
                                    next(error);           //pass error to next function
                                }
                                return res.status(200).json({//if no error, response with 200 and data
                                    success: true,
                                    message: 'van assigned successfully',
                                    data: results
                                });
                            });

                        } catch (error) {
                            logger.error('error created by: ',req.decoded.email)
                            logger.error(error);           //log error
                            next(error);                   //pass error to next function
                        }
                    }).catch(function (error) {            //error in routific
                        logger.error('error created by: ',req.decoded.email)
                        logger.error(error);
                        next(error);
                    });
                } catch (error) {                          //error in update
                    logger.error('error created by: ',req.decoded.email)
                    logger.error(error);                   //log error
                    next(error);                           //pass error to next function
                }
            } else {                                       //error occurred anyway
                logger.error('error created by: ',req.decoded.email)
                logger.error(err);                         //log error
                next(err);                                 //pass error to next function
            }
        });
    } catch (error) {                                      //update status failed, error occurred
        logger.error('error created by: ',req.decoded.email)
        logger.error(error);                               //log error
        next(error);                                       //pass error to next function
    }
}