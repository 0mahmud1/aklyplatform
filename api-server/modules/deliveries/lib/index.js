/**
 * Created by zahed on 11/13/16.
 */
import _ from 'lodash';
import axios from 'axios';
import config from 'config';
import moment from 'moment';
import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'
import Delivery from '../models';
import Order from '../../orders/models';
import DeliveryBoy from 'modules/deliveryBoys/models/index.js';

// update order table when any delivery edited by customer in subscription edit order
export function updateOrder(delivery) {
    logger.info('-------function updateOrder --------');
  Delivery.find({orderRefId : delivery.orderRefId}).exec()
    .then(deliveries => {
      let copyDeliveries = [];
      _.each(deliveries, (data) => {
        copyDeliveries.push({
          meals: data.meals,
          shift: data.shift,
          total: data.total,
          load: data.load,
          quantity: data.quantity,
          deliveryDate: data.deliveryDate
        })
      });
      Order.findOne({orderRefId : delivery.orderRefId}).exec()
        .then(order => {
            order.deliveries = copyDeliveries;
            order.save();
        })
        .catch(error => {
          logger.error(error)
        })
    })
    .catch(error => {
        logger.error(error)
    })

}

export function generateSequence(options) {
    logger.info('-------function generateSequence --------');
  return new Promise(function(resolve, reject) {
    try {
      const query = {
        orderType: 'subscription',
        deliveryDate: new Date(options.deliveryDate),
        shift: options.shift,
        'van.email': options.van.email,
      };
      const routeInfo = {};

      routeInfo.fleet = {
        van: {
          start_location: {
            id: "initial",
            name: "provider",
            lat: options.provider.address.geoLocation.lat,
            lng: options.provider.address.geoLocation.lng
          }
        }
      };

      Delivery.find(query).exec()
      .then(deliveries => {
        routeInfo.visits = {};
        _.each(deliveries, data=> {
          routeInfo.visits[data.deliveryRefId] = {
            location: {
              name: data.deliveryRefId,
              lat: data.locations.coordinates[1],
              lng: data.locations.coordinates[0]
            }
          };
        });

        axios({
          method: 'post',
          url: config.get('ROUTIFIC.URL') + '/vrp',
          data: routeInfo,
          headers: { "Authorization": "bearer "+ config.get('ROUTIFIC.TOKEN') }
        }).then(function (response) {
          const sequenceList = response.data.solution.van;
          let sequence = 0;
          _.each(sequenceList, (data) => {
            if (data.location_id !== 'initial') {
              Delivery.findOne({deliveryRefId: data.location_id})
              .then(delivery => {
                sequence += 1;
                delivery.sequence = sequence;
                delivery.status = 'onTheWay';
                delivery.save();
              });
            }
          });
          resolve(sequence);
        })
        .catch(function (error) {
            logger.error(error)
         });
      });
    } catch (e) {
      logger.error('error in generateSequence');
      logger.error(e);
      logger.debug(e.stack);
      reject(e);
    }
  });
}

/**
 * Assign delivery boy when order is ready
 * @param options
 * @returns {Promise}
 */
export function assignDeliveryBoy(options) {
    logger.info('-------function assignDeliveryBoy --------');
  return new Promise(function (resolve, reject) {
    try {
      logger.info('call assignDeliveryBoy');
      Delivery.findOne({ _id: options._id }).exec()
        .then((delivery) => {
          const dBoyIds = _.map(delivery.zone.deliveryBoys, (data) => {
            return data._id;
          });
          logger.info('dbides', dBoyIds);
          DeliveryBoy.find({
            _id: { $in: dBoyIds },
            status: 'Active',
            isBusy: false  // for the time being, we only have one dB, so a dB can be assigned though he is already assigned
          }).exec().then(dBoyList => {
            try {
              // console.log(dBoyList);
              logger.info("isEmpty dBoyList ", _.isEmpty(dBoyList))
              if (_.isEmpty(dBoyList)) {
                // console.log('isEmpty');
                DeliveryBoy.find({
                  _id: { $in: dBoyIds },
                  status: 'Active',
                }).exec().then(dBoyList => {
                    try {
                      // console.log('dBoyList', dBoyList);
                      // console.log('options status', options.status);
                      logger.info('delivery boys found %s', dBoyList.length);
                      const routific = {};
                      routific.visits = {};
                      routific.fleet = {};
                      routific.visits[delivery.deliveryRefId] = {
                        location: {
                          name: 'provider location',
                          lat: delivery.provider.locations.coordinates[1],
                          lng: delivery.provider.locations.coordinates[0],
                        }
                      };
                      _.each(dBoyList, dBoy => {
                        routific.fleet[dBoy._id.toString()] = {
                          start_location: {
                            id: dBoy._id.toString(),
                            name: dBoy.email,
                            lat: dBoy.locations.coordinates[1],
                            lng: dBoy.locations.coordinates[0],
                          }
                        }
                      });
                      logger.info('========routific generated=========');
                      let axiosRequest = {};
                      axiosRequest.url = config.get('ROUTIFIC.URL') + '/vrp';
                      axiosRequest.headers = {
                        Authorization: "bearer " + config.get('ROUTIFIC.TOKEN')
                      };
                      axiosRequest.method = 'post';
                      axios({
                        method: axiosRequest.method,
                        url: axiosRequest.url,
                        data: routific,
                        headers: axiosRequest.headers
                      }).then(response => {
                        logger.info('got response from routific');
                        try {
                          let solution = response.data.solution;
                          const dBoyId = _.find(_.keys(solution), value => {
                            return solution[value].length > 1;
                          });

                          delivery.deliveryBoy = _.find(dBoyList, dBoy => {
                            console.log(dBoy);
                            console.log(dBoyId);
                            return dBoy._id.toString() === dBoyId;
                          });

                          delivery.save().then(data => {
                            const notification = {
                              place: 'Collection',
                              name: 'deliveries',
                              type: 'status',
                              value: options.status,
                              id: data._id
                            };
                            // PubNub
                            let channelName = 'deliveries-' + data.deliveryBoy._id;
                            let information = "delivery is " + data.status;
                            pubnubNotification(channelName, information, notification)

                            // console.log('assignDeliveryBoy <- ', delivery.deliveryBoy);
                            DeliveryBoy.findOne({ _id: delivery.deliveryBoy._id }).exec()
                              .then((dboy) => {
                                dboy.isBusy = true;
                                dboy.save();
                              })
                              .catch((error) => {
                                  logger.error(error)
                                // console.log('Error in updating DBoy');
                              })
                            resolve(delivery);
                          });
                        } catch (error) {
                            logger.error(error);
                          reject(error);
                        }
                      }).catch(error => {
                        // console.error('assignDeliveryBoy <- ', error.response.data);
                        logger.error('error in routific axios');
                        logger.error(error);
                        reject(new Error(error.response.data.error));
                      });

                    } catch (error) {
                      logger.error(error);
                      reject(error);
                    }
                });

              } else {
                // console.log('Full');
                try {
                  // console.log('dBoyList', dBoyList);
                  console.log('options status', options.status);
                  logger.info('delivery boys found %s', dBoyList.length);
                  const routific = {};
                  routific.visits = {};
                  routific.fleet = {};
                  routific.visits[delivery.deliveryRefId] = {
                    location: {
                      name: 'provider location',
                      lat: delivery.provider.locations.coordinates[1],
                      lng: delivery.provider.locations.coordinates[0],
                    }
                  };
                  _.each(dBoyList, dBoy => {
                    routific.fleet[dBoy._id.toString()] = {
                      start_location: {
                        id: dBoy._id.toString(),
                        name: dBoy.email,
                        lat: dBoy.locations.coordinates[1],
                        lng: dBoy.locations.coordinates[0],
                      }
                    }
                  });
                  logger.info(routific);
                  let axiosRequest = {};
                  axiosRequest.url = config.get('ROUTIFIC.URL') + '/vrp';
                  axiosRequest.headers = {
                    Authorization: "bearer " + config.get('ROUTIFIC.TOKEN')
                  };
                  axiosRequest.method = 'post';
                  axios({
                    method: axiosRequest.method,
                    url: axiosRequest.url,
                    data: routific,
                    headers: axiosRequest.headers
                  }).then(response => {
                      logger.info('========routific generated=========');
                    try {
                      let solution = response.data.solution;
                      const dBoyId = _.find(_.keys(solution), value => {
                        return solution[value].length > 1;
                      });

                      delivery.deliveryBoy = _.find(dBoyList, dBoy => {
                        console.log(dBoy);
                        console.log(dBoyId);
                        return dBoy._id.toString() === dBoyId;
                      });

                      delivery.save().then(data => {
                        const notification = {
                          place: 'Collection',
                          name: 'deliveries',
                          type: 'status',
                          value: options.status,
                          id: data._id
                        };
                        // PubNub
                        let channelName = 'deliveries-' + data.deliveryBoy._id;
                        let information = "delivery is " + data.status;
                        pubnubNotification(channelName, information, notification)

                        // console.log('assignDeliveryBoy <- ', delivery.deliveryBoy);
                        DeliveryBoy.findOne({ _id: delivery.deliveryBoy._id }).exec()
                          .then((dboy) => {
                            dboy.isBusy = true;
                            dboy.save();
                          })
                          .catch((error) => {
                            logger.error(error)
                          })
                        resolve(delivery);
                      });
                    } catch (error) {
                        logger.error(error);
                      reject(error);
                    }
                  }).catch(error => {
                    // console.error('assignDeliveryBoy <- ', error.response.data);
                    logger.error('error in routific axios , first attempt');
                    logger.error(error);
                    reject(new Error(error.response.data.error));
                  });

                } catch (error) {
                    logger.error(error);
                  reject(error);
                }
              }
            } catch (error) {
                logger.error(error);
              reject(error);
            }
          });
        });
    } catch (error) {
      logger.error('error in assign Delivery Boy');
      logger.error(error);
      // logger.debug(error.stack);
      reject(error);
    }
  });
}

function assignDeliveryBoyProcess(options, delivery, dBoyList) {
    logger.info('-------function assignDeliveryBoyProcess --------');
    return new Promise(function (resolve, reject) {
    try {
      // console.log('dBoyList',dBoyList);
      // console.log('options status',options.status);
      logger.info('delivery boys found %s', dBoyList.length);
      const routific = {};
      routific.visits = {};
      routific.fleet = {};
      routific.visits[delivery.deliveryRefId] = {
        location: {
          name: 'provider location',
          lat: delivery.provider.locations.coordinates[1],
          lng: delivery.provider.locations.coordinates[0],
        }
      };
      _.each(dBoyList, dBoy => {
        routific.fleet[dBoy._id.toString()] = {
          start_location: {
            id: dBoy._id.toString(),
            name: dBoy.email,
            lat: dBoy.locations.coordinates[1],
            lng: dBoy.locations.coordinates[0],
          }
        }
      });
      logger.info(routific);
      let axiosRequest = {};
      axiosRequest.url = config.get('ROUTIFIC.URL') + '/vrp';
      axiosRequest.headers = {
        Authorization: "bearer " + config.get('ROUTIFIC.TOKEN')
      };
      axiosRequest.method = 'post';
      axios({
        method: axiosRequest.method,
        url: axiosRequest.url,
        data: routific,
        headers: axiosRequest.headers
      }).then(response => {
        console.log('get response from routific', _.keys(response.data.solution));
        try {
          let solution = response.data.solution;
          const dBoyId = _.find(_.keys(solution), value => {
            return solution[value].length > 1;
          });

          delivery.deliveryBoy = _.find(dBoyList, dBoy => {
            console.log(dBoy);
            console.log(dBoyId);
            return dBoy._id.toString() === dBoyId;
          });

          delivery.save().then(data => {
            const notification = {
              place: 'Collection',
              name: 'deliveries',
              type: 'status',
              value: options.status,
              id: data._id
            };
            // PubNub
            let channelName = 'deliveries-' + data.deliveryBoy._id;
            let information =  "delivery is " + data.status;
            pubnubNotification(channelName, information, notification)

            console.log('assignDeliveryBoy <- ', delivery.deliveryBoy);
            DeliveryBoy.findOne({ _id: delivery.deliveryBoy._id }).exec()
              .then((dboy) => {
                dboy.isBusy = true;
                dboy.save();
              })
              .catch((error) => {
                  logger.error(error)
              })
            resolve(delivery);
          });
        } catch (error) {
            logger.error(error);
          reject(error);
        }
      }).catch(error => {
        // console.error('assignDeliveryBoy <- ', error.response.data);
        // logger.error('error in routific axios');
          logger.error(error);
        reject(new Error(error.response.data.error));
      });

    } catch (error) {
        logger.error(error);
      reject(error);
    }
  });

}

/**
 * Notify changes to Delivery boy in assigned delivery
 * @param options
 */
export function notifyDeliveryBoy(options) {
    logger.info('-------function notifyDeliveryBoy --------');
    try {
    Delivery.findOne({ _id: options._id }).exec()
    .then((delivery) => {

      const notification = {
        place: 'Collection',
        name: 'deliveries',
        type: 'status',
        value: options.status,
        id: delivery._id
      };
      // PubNub
      let channelName = 'deliveries-' + delivery.deliveryBoy._id;
      let information =  `delivery is ${options.status}`;
      pubnubNotification(channelName, information, notification)

    });
  } catch (e) {
    logger.error('error notifyDeliveryBoy');
        logger.error('error created by: ',req.decoded.email)
        logger.error(e);
    throw new Error(e)
  }
}

export function changeDeliveryBoyBusyStatus(options) {
  try {
    console.log('changeDeliveryBoyBusyStatus...',options);
  } catch (e) {
    logger.error('error in assign Delivery Boy');
      logger.error(e);
    logger.debug(e.stack);
  }
}

/**
 * Weekly Format in given deliveries for customer iOS/Android
 * @param options
 */
export function weeklyFormat(options) {
  try {
    let dataSet = [];
    let weekSet = [];
    let minWeek = 0;
    _.each(options, (data)=> {
      weekSet.push(parseInt(moment(data.deliveryDate).format('w')));
      console.log(moment(data.deliveryDate).format('YYYY-MM-DD : w'))
    });
    console.log(_.uniq(weekSet));
    minWeek = parseInt(_.min(weekSet));
    _.each(options, (data)=> {
      let momentDate = moment(data.deliveryDate);
      let weekNum = parseInt(momentDate.format('w'));
      let weekDay = parseInt(momentDate.weekday());
      let dayName = momentDate.format('dddd').toLowerCase();

      if (_.isUndefined(dataSet[weekNum])) { //check if week exists
        dataSet[weekNum] = [];
      }
      if (_.isUndefined(dataSet[weekNum][weekDay])) { //check if week day exists in particular week
        dataSet[weekNum][weekDay] = {
          dayName: dayName,
          deliveryDate: data.deliveryDate,
          delivery: []
        };
      }

      dataSet[weekNum][weekDay].delivery.push(data);
    });
    // logger.info(dataSet);
    return _.map(_.uniq(weekSet), (data)=>{
      return { week: data - minWeek, weeklyDeliveries: dataSet[data].filter(Boolean) }
    });
  } catch (e) {
    logger.error('error in weeklyFormat');
      logger.error(e);
    throw new Error(e)
  }
}