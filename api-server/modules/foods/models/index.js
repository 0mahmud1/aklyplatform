import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a schema
export const foodSchema = new Schema({
  name: {
    type: String,
    required: true,
    default: ''
  },
  price: {
    type: Number,
    required: true,
    default: 0
  },
  changedPrice: {
    type: Number,
    required: true,
    default: 0
  },
  tags: {
    type: [Schema.Types.Mixed],
    required: false,
    default: []
  },
  ingredients: {
    type: String,
    required: false,
    default: ''
  },
  description: {
    type: String,
    required: false,
    default: ''
  },
  calorie: {
    type: String,
    required: false,
    default: ''
  },
  protein: {
    type: String,
    required: false,
    default: ''
  },
  fat: {
    type: String,
    required: false,
    default: ''
  },
  carbs: {
    type: String,
    required: false,
    default: ''
  },
  recipe: {
    type: String,
    required: false,
    default: ''
  },
  provider: { // will remove this key
    type: [],
    required: false,
    default: []
  },
  images: {
    type: [String],
    required: false,
    default: []
  },
  nutritionImage: {
    type: String,
    required: false,
    default: ''
  },
  video: {
    type: String,
    required: false,
    default: ''
  },
  youtubeUrl: {
    type: String,
    required: false,
    default: ''
  },
  load: {
    type: Number,
    default: 1,
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
});

// on every save, add the date
foodSchema.pre('save', function (next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  if (!this.changedPrice) {
    this.changedPrice = (this.price - 20 <= 0) ? 0 : this.price - 20;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const Food = mongoose.model('Food', foodSchema);

// make this available in our applications
export default Food;
