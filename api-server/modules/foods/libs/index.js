import _ from 'lodash';
import axios from 'axios';
import config from 'config';
import moment from 'moment';
import { logger } from 'libs/logger.js';
import Food from '../models';


export function findFoodInformation(options,callback) {
  logger.info('-------function findFoodInformation --------');
  return new Promise(function(resolve, reject) {
    try {
      let query = {status: 'Active'};
      if(options.text) {
        query['name'] = options.text;
      }
      Food.findOne(query).exec()
      .then((food) => {
        callback(null,food);
        resolve(food);
      })
      .catch((error) => {
        reject(error);
      })
    } catch (e) {
      logger.error('error in findFoodInformation');
      logger.error(e);
      logger.debug(e.stack);
      reject(e);
    }
  });
}
