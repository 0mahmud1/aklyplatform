import express from 'express';

import { logger } from 'libs/logger.js';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';
import { getFoods, createFood, deleteFood, readFood, updateFood } from '../controllers';

const foodRoutes = express.Router();

foodRoutes.use(['/foods','/foods/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in foods route.`);

    if (req.method === 'GET') {
        next();
    } else {
        checkAuthToken(req, res, next);
    }
});

foodRoutes.route('/foods')
	.get(getFoods)
	.post(isAuthorizedRoles("administrator", "manager"), createFood);


foodRoutes.route('/foods/:_id')
	.get(readFood)
	.delete(isAuthorizedRoles("administrator", "manager"), deleteFood)
	.put(isAuthorizedRoles("administrator", "manager"), updateFood);

export default foodRoutes;
