import mongoose from 'mongoose';
import Food from '../models/index.js';
import MealPlan from '../../meal-plans/models/index'
import {logger} from 'libs/logger.js';
import _ from 'lodash';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import moment from "moment"
/*
*  RESTful CRUD APIs for Food
*/

/**
* @api {GET} /api/foods All active foods
* @apiName GetFoods
* @apiGroup Food
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Get all foods
* @apiSuccess (200) {Object[]} Foods see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*   "success": true,
*   "message": "Get all foods",
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }]
* }
*/

export const getFoods = (req, res, next) => {
  try {
    logger.info('request for get active foods',moment().format());
    let limit,skip,page;
    if(req.query.limit === 'all'){
      limit = 0;
      skip = 0;
    } else {
      limit = parseInt(req.query.limit);
      skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    }
    page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1
    let query = { status: 'Active' };
    // @todo insert required queries in query param

    if(req.query['tags']){
      let tagList = _.map(req.query['tags'].split(','), (item)=>{
        return RegExp(item,'i')
      })
      query['tags.name'] = {
        $in: tagList,
        // $in: req.query['tags'].split(','),
      }
    }

    console.log('tags query', query)

    if(req.query['name']) {
      let name = req.query['name'];
      query['name'] = RegExp(name,'i') ;

    }
    //this promise returns number of total object
    let findCountPromise = Food.find(query).count();

    //this promise returns actual data
    let findDataPromise = Food.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active foods',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getFoods',
      error: e
    });
  }
};

/**
* @api {POST} /api/foods Create a food
* @apiName CreateFood
* @apiGroup Food
* @apiPermission Administrator, Manager
* @apiHeader {String} Authorization valid access token
* @apiParam {Object} Data see Request-Example
*  @apiParamExample {json} Request-Example:
*  {
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a food
* @apiSuccess (200) {Object} Foods see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*   "success": true,
*   "message": "Create a food",
*   "data": {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "__v": Number,
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "__v": Number,
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }
* }
*/

export const createFood = (req, res, next) => {
  try {
    ///let reqUser = req.decoded._doc;
    const food = new Food(req.body);

    food.save()
    .then((doc) => {
      logger.info('new food added ', doc);
      return res.json({
        success: true,
        message: 'Create a food',
        food: doc
      });
    }).catch((error) => {
      logger.error('food creation failed', error);
      next(error);
    });


  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createFoods',
      error: e
    });
  }
  // try {
  // 	const newFood = new Food(req.body);
  //
  // 	newFood.save().then((food) => {
  // 		appLog.info('new food added ', food);
  // 		return res.json({
  // 			success: true,
  // 			message: 'Create a food',
  // 			food: food
  // 		});
  // 	}).catch((error) => {
  // 		appLog.error('food creation failed', error);
  // 		next(error);
  // 	});
  // } catch (e) {
  // 	appLog.error('server error ', e);
  // 	next(e);
  // }

};


/**
* @api {GET} /api/foods/:_id Read a specific food
* @apiName ReadFood
* @apiGroup Food
* @apiPermission All
* @apiParam {String} _id Food unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a food
* @apiSuccess (200) {Object} Foods see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*   "success": true,
*   "message": "Read a food",
*   "data": {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "__v": Number,
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "__v": Number,
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }
* }
*/

export const readFood = (req, res, next) => {
  try {
    // let query = { _id: req.params._id };
    // let projection = {};
    //
    // Food.findOne(query, projection).exec((err, doc) => {
    //     if (err) return next(err);
    //     return res.json({
    //         success: true,
    //         message: 'Read a user',
    //         data: doc
    //     });
    // });
    Food.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((food) => {
      logger.info('my food:', food)
      if (!food) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a food',
        data: food
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readFoods',
      error: e
    });
  }
};


/**
* @api {PUT} /api/foods/:_id Update a food
* @apiName UpdateFood
* @apiGroup Food
* @apiPermission Administrator, Manager
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Food unique ID
*  @apiParamExample {json} Request-Example:
*  {
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a food
* @apiSuccess (200) {Object} Foods see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*   "success": true,
*   "message": "Update a food",
*   "data": {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "price": Number,
*       "recipe": String,
*       "ingredients": String,
*       "calorie": String,
*       "protein": String,
*       "fat": String,
*       "carbs": String,
*       "description":String,
*       "youtubeUrl": "",
*       "__v": Number,
*       "status": "Active",
*       "load": Number,
*       "images": [String],
*       "provider": [],
*       "tags": [
*         {
*           "status": "Active",
*           "images": [String],
*           "__v": Number,
*           "name": String
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": String
*         }
*       ]
*   }
* }
*/


export const updateFood = (req, res, next) => {
  try {
    //let reqUser = req.decoded;
    // let query = { _id: req.params._id };
    // let projection = {};
    Food.findOne({_id: req.params._id}).exec()
    .then((food) => {
      logger.info('food to be UPDATED: ', food);
      if (!food) {
        throw new Error('no food found for update');
      }
      Object.assign(food, food, req.body);
      return food.save();
    })
    .then((updatedFood) => {
        let query = {"plans.foods.item._id":updatedFood._id}//.toString()}
        MealPlan.find(query).exec() //{"plans.foods.item._id":updatedFood._id}
            .then((mPlans)=>{ // all mealplans that contains updated food
                mPlans.map((mealplan, index)=>{
                  console.log('meal plan to be updated', mealplan._id)
                    mealplan.plans.map((plan,indx)=>{
                        plan.foods.map((food, idx)=>{
                            if(food.item._id.toString() === updatedFood._id.toString()) {
                                let queryString = 'plans.'+indx+'.foods.'+idx+'.item'//+'._id'
                                let qry = {}
                                qry[queryString] = updatedFood
                                MealPlan.update({ _id: mealplan._id }, { $set: qry}).exec()
                            }

                        })
                    })

                })
            }).then(()=>{
            return res.status(200).json(placeHolder(true, 'Update a food', updatedFood))
        })
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateFoods',
      error: e
    });
  }
};

/**
* @api {DELETE} /api/foods/:_id  Delete/Archive a food
* @apiName DeleteFood
* @apiGroup Food
* @apiPermission Administrator, Manager
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Food unique ID
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a order
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Deleted a order",
*  }
 */

export const deleteFood = (req, res, next) => {
  try {
    let query = {_id: req.params._id};

    Food.findOne(query).exec()
    .then((food) => {
      logger.info('food to be DELETED: ', food);
      food.status = 'Archived';
      return food.save();
    })
    .then((deletedFood) => {
      return res.status(200).json(placeHolder(true, 'Deleted a food'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteFoods',
      error: e
    });
  }
  // const foodId = req.params._id;
  // appLog.log('food ID: ', foodId);
  //
  // Food.findById(foodId).then((food) => {
  // 	Food.update({_id: food._id}, {$set: {status: 'Archive'}}).then(()=> {
  // 		return res.status(200).json({
  // 			success: true,
  // 			message: 'Archived'
  // 		});
  // 	});
  // }).catch((err) => {
  // 	return res.status(400).json({
  // 		success: false,
  // 		message: 'Unable to find food',
  // 		error: err
  // 	});
  // });
};
