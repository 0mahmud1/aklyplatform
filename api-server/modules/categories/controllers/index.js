import Category from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import async from 'async'

/*
*  RESTful CRUD APIs for Category
*/

/**
* @api {GET} /api/categories All active categories
* @apiName GetCategories
* @apiGroup Category
* @apiPermission all
* @apiSuccess (200) {Number} total total number of categories
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active categories
* @apiSuccess (200) {Object[]} categories see see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*       "total": Number,
*       "limit": Number,
*       "skip": Number,
*       "success": true,
*       "message": "get active categories",
*        "data": [
*           {
*               "_id": "String",
*               "createdAt": Date,
*               "updatedAt": Date,
*               "name": "String",
*               "__v": 0,
*               "status": "Active"
*           },
*        ]
*  }
*
*/

export const getCategories = (req, res, next) => {
  try {
    logger.info('request to get active categories');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param

    //this promise returns number of total object
    let findCountPromise = Category.find(query).count();

    //this promise returns actual data
    let findDataPromise = Category.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active categories',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getCategories',
      error: e
    });
  }
};

/**
* @api {POST} /api/categories Create a category
* @apiVersion 0.0.1
* @apiName CreateCategory
* @apiDescription Meal plan Category
* @apiGroup Category
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager
* @apiParam {String} name  Category Name
* @apiParamExample {json} Request-Example:
*     {
*       "name": "category name"
*     }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a category
* @apiSuccess (200) {Object} See Success-Response
* @apiSuccessExample {json} Success-Response:
*{
*   "success": true
*   "message": "Create a category"
*   "category": {
*   "__v": 0
*   "createdAt": Date,
*   "updatedAt": Date,
*   "name": "String",
*   "_id": "String",
*   "status": "Active"
*   }-
*}
*/


export const createCategory = (req, res, next) => {
  try {
    const newCategory = new Category({
      name: req.body.name
    });

    newCategory.save()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Create a category',
        data: response
      })
    })
    .catch((error) => {
      res.status(400).json({
        success: false,
        message: 'Failed to create a category',
        data: error
      })
    });

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createCategory',
      error: e
    });
  }

  // newCategory.save((err, category) => {
  //   if (err) return next(err);
  //   return res.json({
  //     success: true,
  //     message: 'Create a category',
  //     category: category
  //   });
  // });
};

/**
* @api {GET} /api/categories/:_id Read a category
* @apiName ReadCategory
* @apiGroup Category
* @apiParam {String} _id Category unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a category
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Read a category",
*      "data": {
*         "_id": "String",
*         "createdAt": Date,
*         "updatedAt": Date,
*         "name": "String",
*         "status": "Active"
*      }
*  }
*/
export const readCategory = (req, res, next) => {
  // Category.findById(req.params._id).exec((err, category) => {
  //   if (err) return next(err);
  //   return res.json({
  //     success: true,
  //     message: 'Read a category',
  //     category: category
  //   });
  // });
  try {
    Category.findOne( { _id: req.params._id, status: 'Active'  } ).exec()
    .then((category) => {
      logger.info('my category: ...', category);
      if(!category) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403,false,'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a category',
        data: category
      })
    })
    .catch((error) => {
      next(error);
    });

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readCategory',
      error: e
    });
  }
};

/**
* @api {PUT} /api/categories/:_id Update a category
* @apiName UpdateCategory
* @apiGroup Category
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Category unique ID
* @apiPermission administrator, manager
* @apiParamExample {json} Request-Example:
*     {
*       "name": "String"
*     }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a category
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Update a category",
*      "data": {
*         "_id": "String",
*         "createdAt": Date,
*         "updatedAt": Date,
*         "name": "String",
*         "status": "Active"
*      }
*  }
*/
export const updateCategory = (req, res, next) => {
  // Category.findById(req.params._id).exec((err, category) => {
  //   if (err) return next(err);
  //
  //   category.name = req.body.name;
  //
  //   category.save((err, category) => {
  //     if (err) return next(err);
  //     return res.json({
  //       success: true,
  //       message: 'Update a category',
  //       category: category
  //     });
  //   });
  // });
  try {
    Category.findOne({_id: req.params._id}).exec()
    .then((category) => {
      logger.info('category to be UPDATED: ',category);
      if(!category){
        throw new Error('no category found for update');
      }
      category.name = req.body.name;
      return category.save();
    })
    .then((updatedCategory) => {
      return res.status(200).json(placeHolder(true,'Update a category',updatedCategory))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403,false,error.message,error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateCategory',
      error: e
    });
  }

};

/**
* @api {DELETE} /api/categories/:_id Delete a category
* @apiName DeleteCategory
* @apiGroup Category
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Category unique ID
* @apiPermission administrator, manager
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a category
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Deleted a category"
*  }
*/
export const deleteCategory = (req, res, next) => {
  // Category.findById(req.params._id).exec((err, category) => {
  //   if (err) return next(err);
  //
  //   category.status = 'Archived';
  //
  //   category.save((err, category) => {
  //     if (err) return next(err);
  //     return res.json({
  //       success: true,
  //       message: 'Delete a category'
  //     });
  //   });
  // });
  try {
    Category.findOne({_id: req.params._id}).exec()
    .then((category) => {
      logger.info('category to be DELETED: ',category);
      category.status = 'Archived';
      return category.save();
    })
    .then((deletedCategory) => {
      return res.status(200).json(placeHolder(true,'Deleted a category'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteCategory',
      error: e
    });
  }
};
