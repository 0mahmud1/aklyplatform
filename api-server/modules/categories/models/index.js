import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a schema
const categorySchema = new Schema({
  name : {
    type: String,
    required: true
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
}, { versionKey: false });

// on every save, add the date
categorySchema.pre('save', function(next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const Category = mongoose.model('Category', categorySchema);

// make this available in our applications
export default Category;
