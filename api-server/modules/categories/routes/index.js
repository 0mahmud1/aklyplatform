import express from 'express';

import { logger } from 'libs/logger.js';
import { getCategories, createCategory, readCategory, updateCategory, deleteCategory } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const categoryRoutes = express.Router();

categoryRoutes.use(['/categories','/categories/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in categories route.`);
    if (req.method === 'GET') {
        next();
    } else {
        checkAuthToken(req, res, next);
    }
});

categoryRoutes.route('/categories')
    // All active categories (accessed at GET /api/categories)
    .get(getCategories)
    // Create a category (accessed at POST /api/categories)
    .post(isAuthorizedRoles("administrator", "manager"), createCategory);


categoryRoutes.route('/categories/:_id')
    // Read a category (accessed at GET /api/categories/:_id)
    .get(readCategory)
    // Update a category (accessed at PUT /api/categories/:_id)
    .delete(isAuthorizedRoles("administrator", "manager"), deleteCategory)
    // Delete a category (accessed at DELETE /api/categories/:_id)
    .put(isAuthorizedRoles("administrator", "manager"), updateCategory);

export default categoryRoutes;
