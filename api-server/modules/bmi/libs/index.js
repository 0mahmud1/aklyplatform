'use strict';

/*
* Calculate Body Mass Index (BMI)
* Classify Body Mass Index (BMI) Category
*/
export function calculateBodyMassIndex(customer) {
  if (!customer.profile
    || !customer.profile.weight
    || !customer.profile.height) {
    return {
      status: false,
      message: 'Required data are missing...',
    };
  }

  let weight = parseFloat(customer.profile.weight);
  let height = parseFloat(customer.profile.height) / 100;

  let bodyMassIndex = weight / (height * height);
  let category = null;

  if (bodyMassIndex < 18.5) {
    category = 'Underweight';
  } else if (bodyMassIndex < 25.0) {
    category = 'Normal';
  } else if (bodyMassIndex < 30.0) {
    category = 'Overweight';
  } else {
    category = 'Obese';
  }

  return {
    status: true,
    value: bodyMassIndex.toFixed(2),
    category: category,
  };
};

/*
* Calculate Daily Calorie Allowance
*/
export function calculateDailyCalorieAllowance(customer) {
  if (!customer.profile
    || !customer.profile.weight
    || !customer.profile.height
    || !customer.profile.age
    || !customer.profile.gender
    || !customer.profile.activityLevel) {
    return {
      status: false,
      message: 'Required data are missing...',
    };
  }

  let weight = parseFloat(customer.profile.weight);
  let height = parseFloat(customer.profile.height);
  let age = parseFloat(customer.profile.age);
  let gender = customer.profile.gender;
  let activityLevel = customer.profile.activityLevel;

  let harrisBenedictEqu = 0.00;
  if (gender === 'Male') {
    harrisBenedictEqu = 66.5 + (13.75 * weight) + (5.0 * height) - (6.78 * age);
  } else if (gender === 'Female') {
    harrisBenedictEqu = 65.5 + (9.56 * weight) + (1.85 * height) - (4.68 * age);
  } else {
    return {
      status: false,
      message: 'Gender is not correct.',
    };
  }

  let factor = 0.0;
  if (activityLevel == 'Sedentary') {
    factor = 1.2;
  } else if (activityLevel == 'Little') {
    factor = 1.375;
  } else if (activityLevel == 'Moderate') {
    factor = 1.55;
  } else if (activityLevel == 'Heavy') {
    factor = 1.725;
  } else if (activityLevel == 'Super Star') {
    factor = 1.9;
  } else {
    return {
      status: false,
      message: 'Activity Level is not correct.',
    };
  }

  let dailyCalorieAllowance = harrisBenedictEqu * factor;
  return {
    status: true,
    value: dailyCalorieAllowance.toFixed(2),
  };
}

/*
* Get Recommendation Status
*/
export function getRecommendation(customer) {
  let objBodyMassIndex = calculateBodyMassIndex(customer);
  let recommendation = null;

  if (!objBodyMassIndex.status) {
    return {
      status: false,
      message: 'Required data are missing...',
    };
  }

  if (objBodyMassIndex.category == 'Normal') {
    recommendation = 'Maintain';
  } else if (objBodyMassIndex.category == 'Overweight' || objBodyMassIndex.category == 'Obese') {
    recommendation = 'Lose';
  } else if (objBodyMassIndex.category == 'Underweight') {
    recommendation = 'Gain';
  }

  return {
    status: true,
    value: recommendation,
  };
}

/*
* Calculate Kcal Range
*/
export function calculateKcalRange(statistic, userRecommend) {
  let listedCalorie = ["1200", "1500", "2000", "2500"];
  let suggestedCalorie = parseInt(statistic.dailyCalorieAllowance.value);
  if (userRecommend === 'Lose') {
    if (suggestedCalorie <= 1200) {
      listedCalorie = ["1200"];
    } else if (suggestedCalorie > 1200 && suggestedCalorie <= 1500) {
      listedCalorie = ["1200"];
    } else if (suggestedCalorie > 1500 && suggestedCalorie <= 2000) {
      listedCalorie = ["1200", "1500"];
    } else if (suggestedCalorie > 2000 && suggestedCalorie <= 2500) {
      listedCalorie = ["1200", "1500", "2000"];
    } else {
      listedCalorie = ["1500", "2000", "2500"];
    }
  } else if (userRecommend === 'Gain') {
    if (suggestedCalorie <= 1200) {
      listedCalorie = ["2000", "2500"];
    } else if (suggestedCalorie > 1200 && suggestedCalorie <= 1500) {
      listedCalorie = ["2000", "2500"];
    } else if (suggestedCalorie > 1500 && suggestedCalorie <= 2000) {
      listedCalorie = ["2000", "2500"];
    } else if (suggestedCalorie > 2000 && suggestedCalorie <= 2500) {
      listedCalorie = ["2500"];
    } else {
      listedCalorie = ["3000", "2500"];
    }
  } else {
    if (suggestedCalorie <= 1200) {
      listedCalorie = ["1200"];
    } else if (suggestedCalorie > 1200 && suggestedCalorie <= 1500) {
      listedCalorie = ["1200", "1500"];
    } else if (suggestedCalorie > 1500 && suggestedCalorie <= 2000) {
      listedCalorie = ["1500", "2000"];
    } else if (suggestedCalorie > 2000 && suggestedCalorie <= 2500) {
      listedCalorie = ["2000", "2500"];
    } else {
      listedCalorie = ["2500"];
    }
  }

  return listedCalorie;
}
