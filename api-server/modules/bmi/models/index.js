'use strict';

import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a schema
export const statisticSchema = new Schema({
  customer: {
    type: Schema.Types.Mixed,
    required: true,
  },
  bodyMassIndex: {
    type: Schema.Types.Mixed,
  },
  dailyCalorieAllowance: {
    type: Schema.Types.Mixed,
  },
  recommendation: {
    type: Schema.Types.Mixed,
  },
  createdAt: Date,
  updatedAt: Date,
}, { versionKey: false });

// on every save, add the date
statisticSchema.pre('save', function (next) {
  // get the current date
  const currentDate = new Date();

  // change the updated_at field to current date
  this.updatedAt = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }

  next();
});

// the schema is useless so far
// we need to create a model using it
const Statistic = mongoose.model('Statistic', statisticSchema);

// make this available in our applications
export default Statistic;
