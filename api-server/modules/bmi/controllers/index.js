'use strict';

import async from 'async';

import Customer from '../../customers/models';
import MealPlan from '../../meal-plans/models';
import Statistic from '../models';

import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

import {
  calculateBodyMassIndex,
  calculateDailyCalorieAllowance,
  getRecommendation,
  calculateKcalRange
} from '../libs';

/*
*  RESTful CRUD APIs for BMI
*/
/**
* @api {GET} /api/bmi/user-profile Get customer profile
* @apiName GetUserProfile
* @apiGroup BMI
* @apiHeader {String} Authorization valid access token
* @apiPermission customer
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Get User Profile Data
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Get User Profile Data",
*   "data": {
*     "userProfile": {
*       "activityLevel": String,
*       "gender": String,
*       "age": Number,
*       "weight": Number,
*       "height": Number
*     }
*   }
* }
*/
export const getUserProfile = (req, res, next) => {
  try {
    Customer.findOne({
      email: req.decoded.email,
      status: 'Active',
    }).exec((err, customer) => {
      if (err) callback(err);

      return res.status(200).json({
        success: true,
        message: 'Get User Profile Data',
        data: {
          userProfile: customer.profile ? customer.profile : {},
        },
      });
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: 'Error in getUserProfile',
      error: error,
    });
  }
};

/**
* @api {POST} /api/bmi/user-profile Update customer profile
* @apiName UpdateUserProfile
* @apiGroup BMI
* @apiHeader {String} Authorization valid access token
* @apiPermission customer
* @apiParamExample {Object} Request-Example:
* {
*      "activityLevel": String,
*      "gender": String,
*      "age": Number,
*      "weight": Number,
*      "height": Number
* }
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Update User Profile Data
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Update User Profile Data",
*   "data": {
*     "userProfile": {
*       "activityLevel": String,
*       "gender": String,
*       "age": Number,
*       "weight": Number,
*       "height": Number
*     }
*   }
* }
 */

export const updateUserProfile = (req, res, next) => {
  try {
    console.log('updateUserProfile ', req.body);
    Customer.findOne({
      email: req.decoded.email,
      status: 'Active',
    }).exec((err, customer) => {
      if (err) callback(err);

      customer.profile = req.body;
      customer.save(function (err) {
        if (err) callback(err);

        return res.status(201).json({
          success: true,
          message: 'Update User Profile Data',
          data: { userProfile: customer.profile },
        });
      });
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: 'Error in getUserProfile',
      error: error,
    });
  }
};

/**
* @api {GET} /api/bmi Get customer BMI
* @apiName GetUserBMI
* @apiGroup BMI
* @apiHeader {String} Authorization valid access token
* @apiPermission customer
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Get body mass index
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*  "success": true,
*  "message": "Get body mass index",
*  "data": {
*    "statistic": {
*      "createdAt": Date,
*      "updatedAt": Date,
*      "customer": {
*        "profile": {
*          "height": Number,
*          "weight": Number,
*          "age": Number,
*          "gender": String,
*          "activityLevel": String
*        },
*        "email": String,
*        "_id": String
*      },
*      "bodyMassIndex": {
*        "category":String,
*        "value": String,
*        "status": Boolean
*      },
*      "dailyCalorieAllowance": {
*        "value": String,
*        "status": Boolean
*      },
*      "recommendation": {
*        "value": String,
*        "status": Boolean
*      },
*      "_id": String
*    }
*  }
* }
*/

export const getBodyMassIndex = (req, res, next) => {
  try {
    async.waterfall([
      function (callback) {
        Customer.findOne({
          email: req.decoded.email,
          status: 'Active',
        }).exec((err, customer) => {
          if (err) callback(err);
          callback(null, customer);
        });
      },

      function (customer, callback) {
        let newStatistic = new Statistic({
          customer: {
            _id: customer._id,
            email: customer.email,
            profile: customer.profile,
          },
          bodyMassIndex: calculateBodyMassIndex(customer),
          dailyCalorieAllowance: calculateDailyCalorieAllowance(customer),
          recommendation: getRecommendation(customer),
        });

        if (!newStatistic.bodyMassIndex.status) {
          callback(newStatistic.bodyMassIndex);
        } else if (!newStatistic.dailyCalorieAllowance.status) {
          callback(newStatistic.dailyCalorieAllowance);
        } else if (!newStatistic.recommendation.status) {
          callback(newStatistic.recommendation);
        } else {
          newStatistic.save(function (err, statistic) {
            if (err) callback(err);
            callback(null, statistic);
          });
        }
      },
    ], function (err, statistic) {
      if (err) return next(err);

      return res.status(200).json({
        success: true,
        message: 'Get body mass index',
        data: {
          statistic: statistic,
        },
      });
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: 'Error in getBodyMassIndex',
      error: error,
    });
  }
};

/**
* @api {GET} /api/bmi/suggestion/:_statisticId Get customer meal suggestion
* @apiName GetUserMealSuggestion
* @apiGroup BMI
* @apiHeader {String} Authorization valid access token
* @apiPermission customer
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Get meal plan suggestions
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*     "message": "Get meal plan suggestions",
*     "data": {
*   "statistic": {
*     "_id": String,
*     "createdAt": Date,
*     "updatedAt": Date,
*     "customer": {
*       "_id": String,
*       "email": String,
*       "profile": {
*         "activityLevel": String,
*         "gender": String,
*         "age": Number,
*         "weight": Number,
*         "height": Number
*       }
*     },
*     "bodyMassIndex": {
*       "status": Boolean,
*       "value": String,
*       "category": String
*     },
*     "dailyCalorieAllowance": {
*       "status": Boolean,
*       "value": String
*     },
*     "recommendation": {
*       "status": Boolean,
*       "value": String
*     }
*   },
*   "mealPlans": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "carbs": Number,
*       "fat": Number,
*       "protein": Number,
*       "calorie": Number,
*       "name": String,
*       "description": String,
*       "status": String,
*       "plans": [
*         {
*           "day": String,
*           "slot": String,
*           "_id": String,
*           "foods": [
*             {
*               "item": {
*                 "_id": String,
*                 "__v": Number,
*                 "calorie": String,
*                 "carbs": String,
*                 "createdAt": Date,
*                 "description": String,
*                 "fat": String,
*                 "ingredients": String,
*                 "name": String,
*                 "price": Number,
*                 "protein": String,
*                 "recipe": String,
*                 "updatedAt": Date,
*                 "youtubeUrl": String,
*                 "status": String,
*                 "load": Number,
*                 "images": [String],
*                 "provider": [],
*                 "tags": [
*                  {
*                     "status": String,
*                     "images": [String],
*                     "__v": Number,
*                     "name": String,
*                     "updatedAt": Date,
*                     "createdAt": Date,
*                     "_id": String
*                   },
*                 ]
*               },
*               "quantity": Number,
*               "_id": String
*             },
*               "quantity": Number,
*               "_id": String
*             }
*           ]
*         },
*       ],
*       "categories": [
*         String
*       ]
*     }
*   ]
*  }
* }
*/

export const getSuggestions = (req, res, next) => {
  try {
    let statisticId = req.params._statisticId;
    let userRecommend = req.query.userRecommend;
    async.waterfall([
      function (callback) {
        Statistic.findById(statisticId).exec((err, statistic) => {
          if (err) callback(err);
          callback(null, statistic);
        });
      },

      function (statistic, callback) {
        let listedCalorie = calculateKcalRange(statistic, userRecommend);

        logger.info('listedCalorie', listedCalorie);

        MealPlan.find({
          $and: [
            { status: 'Active' },
            { description: { $in: listedCalorie } },
          ],
        }).sort({
          calorie: 1,
        }).exec((err, mealPlans) => {
          if (err) callback(err);
          callback(null, statistic, mealPlans);
        });
      },
    ], function (err, statistic, mealPlans) {
      if (err) return next(err);

      return res.status(200).json({
        success: true,
        message: 'Get meal plan suggestions',
        data: { userRecommend, statistic, mealPlans },
      });
    });
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: 'Error in getSuggestions',
      error: error,
    });
  }
};
