'use strict';

import express from 'express';

import { logger } from 'libs/logger.js';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';
import {
  getUserProfile,
  updateUserProfile,
  getBodyMassIndex,
  getSuggestions
} from '../controllers';

const bmiRoutes = express.Router();

bmiRoutes.use(['/bmi'], (req, res, next) => {
    logger.info(`a ${req.method} request in tags route.`);
    checkAuthToken(req, res, next);
  }
);

bmiRoutes.route('/bmi')
  .get(getBodyMassIndex);

bmiRoutes.route('/bmi/suggestion/:_statisticId')
  .get(getSuggestions);

bmiRoutes.route('/bmi/user-profile')
  .get(getUserProfile)
  .post(updateUserProfile);

export default bmiRoutes;
