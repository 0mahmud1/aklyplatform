import express from 'express';

import { logger } from 'libs/logger.js';
import { getUsers, createUser, readUser, updateUser, deleteUser , checkUser } from '../controllers';
import { checkAuthToken, authenticateUser, isAuthorizedRoles } from 'modules/auth/controllers';

const administratorRoutes = express.Router();

administratorRoutes.use(['/administrator', '/administrator/:_id'],(req, res, next) => {
  logger.info(`a ${req.method} request in users route.`);
  checkAuthToken(req, res, next);

});

administratorRoutes.route('/authenticate').post(authenticateUser);

administratorRoutes.route('/administrator')
  .get(getUsers)
  .post(isAuthorizedRoles('administrator', 'manager'),createUser);

administratorRoutes.route('/administrator/check')
  .get(checkUser);

administratorRoutes.route('/administrator/:_id')
    // Read a user (accessed at GET /api/administrator/:_id)
    .get(readUser)
    // Update a user (accessed at PUT /api/administrator/:_id)
    .put(isAuthorizedRoles('administrator', 'manager'),updateUser)
    // Delete a user (accessed at DELETE /api/administrator/:_id)
    .delete(isAuthorizedRoles('administrator', 'manager'),deleteUser);

// router.post('/authenticate', authController.authenticateUser);

export default administratorRoutes;
