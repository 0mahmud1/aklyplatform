import colors from 'colors';
import _ from 'lodash';
import jwt from 'jsonwebtoken'
import config from 'config'
import axios from 'axios'

import Administrator from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

/*
*  RESTful CRUD APIs for User
*/

/**
* @api {GET} /api/administrator All active administrator
* @apiName GetAdministrator
* @apiGroup Administrator
* @apiVersion 0.0.0
* @apiHeader {String} Authorization valid access token
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Get all active administrator
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Get all active administrator",
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "email": String,
*       "auth0": String,
*       "__v": Number,
*       "status": String,
*       "address": [],
*       "profile": []
*     }
*   ]
* }
*/
export const getUsers = (req, res, next) => {
	try {
		let query = {status: 'Active'};
		let projection = {password: 0, service: 0};

		logger.info('request user via jwt-express \n', req.user);

		/*
		* genaralized filter
		* // {roles: {$in : ['admin','deliveryBoy']}}
		* */

		if (req.query.roles) {
			query.roles = {
				$in: req.query.roles.split(',')
			}
		}

		if(req.query.email){
			query.email = req.query.email
		}

		logger.info('QUERY: ', query);

    Administrator.find(query, projection).exec((err, user) => {
			if (err) return next(err);
			return res.json({
				success: true,
				message: 'Get all active administrator',
				data: user
			});
		});


	} catch (e) {
		logger.debug(e.stack);
		logger.error(e);
	}
};

export const checkUser = (req, res, next) => {
	logger.info('call check administrator', req.decoded);

	try {
		let query = { auth0: req.decoded.sub, email: req.decoded.email };
		logger.info('query: ', query)
    Administrator.findOne(query).exec()
			.then((response) => {
				if (response) {
					return res.status(200).json({
						success: true,
						message: 'Admin found',
						data: response
					});
				} else {
					logger.info('No admin Found');
					logger.debug(req.decoded);
					let adminProfile = {
						email: req.decoded.email,
						auth0: req.decoded.sub
					};

					if(req.decoded.name){
						adminProfile = Object.assign(adminProfile,adminProfile,{ name: req.decoded.name })
					}

					const admin = new User(adminProfile);

					admin.save()
						.then(response => {
							return res.status(200).json({
								success: true,
								message: 'Created new admin',
								data: response
							});
						})
				}
			})
			.catch(err => next(err));
	} catch (e) {
		// logger.debug(e.stack);
		logger.error(e);
		res.status(500).json({
			success: false,
			message: 'Error in check admin',
			error: e
		});
	}
};

/**
* @api {POST} /api/administrator Create a administrator
* @apiName CreateAdministrator
* @apiGroup Administrator
* @apiVersion 0.0.0
* @apiHeader {String} Authorization valid access token
* @apiParamExample {Object} Request-Example:
*  {
*      "email": "String",
*      "name": "String",
*      "password": "String",
*      "roles": ["String"],
*      "address": [Object]
*  }
*
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Get all active administrator",
*      "data": {
*          "_id": "String",
*          "email": "String",
*          "name": "String",
*          "password": "String",
*          "roles": ["String"],
*          "status": "Active",
*          "service": [Object],
*          "address": [Object]
*      }
*  }
*/

export const createUser = (req, res, next) => {
	try {
		const authToken = jwt.sign({
			"aud": config.get('AUTH0.API_KEY'),
			"scopes": {
				"users": {
					"actions": ["create"]
				}
			}
		}, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
		const axiosConfig = {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + authToken
			}
		};
		const url = config.get('AUTH0.BASE_URL') + 'api/v2/users';
		let authData = {
			'connection': config.get('AUTH0.CONNECTION'),
			'name': req.body.name,
			'email': req.body.email,
			'password': req.body.password,
			'user_metadata': req.body.user_metadata
		};

		axios.post(url, authData, axiosConfig)
		.then((response1) => {

			let userInfo = Object.assign({}, req.body, {auth0: response1.data.user_id});
			delete userInfo.user_metadata;
			delete userInfo.password;
			const newUser = new User(userInfo);
			return newUser.save();

		})
		.then((response2) => {
			return res.status(201).json({
				success: true,
				message: 'Employee is created successfully and add to Auth0!',
				data: response2._doc
			});
		})
		.catch((error) => {
			console.error('Can\'t Add User to Auth0...', error);
			const err = new Error(error.response.data.message);
			err.status = error.response.status;
			return next(err);
		});

	} catch (e) {
		res.status(400).json({
			success: false,
			message: 'Unable to create a user',
			error: e
		});
	}
};

/**
* @api {GET} /api/administrator/:_id Read a administrator
* @apiName ReadAdministrator
* @apiGroup Administrator
* @apiVersion 0.0.0
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Get all active administrator",
*      "data": [{
*          "_id": "String",
*          "email": "String",
*          "name": "String",
*          "password": "String",
*          "roles": ["String"],
*          "status": "Active",
*          "service": [Object],
*          "address": [Object]
*      }]
*  }
*/
export const readUser = (req, res, next) => {
	try {
		let reqUser = req.user;
		let query = {_id: reqUser._id};
		let projection = {password: 0, service: 0};

		// User.findOne(query, projection).exec((err, user) => {
		//   if (err) return next(err);
		//   return res.json({
		//     success: true,
		//     message: 'Read a user',
		//     data: user
		//   });
		// });
    Administrator.findOne(query, projection).exec()
		.then((user) => {
			logger.info('my user: ...', user);
			if (!user) {
				logger.warn('query return null');
				return next(errorPlaceHolder(403, false, 'no data found'));
			}

			return res.status(200).json({
				success: true,
				message: 'Read a user',
				data: user
			})
		})
		.catch((error) => {
			next(error);
		});
	} catch (e) {
		logger.debug(e.stack);
		logger.error(e);
	}
};

/**
* @api {PUT} /api/administrator/:_id Update a administrator
* @apiName UpdateAdministrator
* @apiGroup Administrator
* @apiVersion 0.0.0
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiParamExample {Object} Request-Example:
*  {
*      "name": "String",
*      "address": [Object]
*  }
*
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Get all active administrator",
*      "data": {
*          "_id": "String",
*          "email": "String",
*          "name": "String",
*          "roles": ["String"],
*          "status": "Active",
*          "service": [Object],
*          "address": [Object]
*      }
*  }
*/

export const updateUser = (req, res, next) => {

	logger.info('request for update a user');

	let query = {_id: req.params._id};
	let projection = {password: 0, service: 0};

  Administrator.findOne(query, projection).exec((err, user) => {
		if (err) return next(err);

		// let fields = _.keys(req.body);
		// for (var key in fields) {
		//     user[fields[key]] = req.body[fields[key]];
		// }

		logger.info('PATCH USER ', req.body);

		Object.assign(user, user, req.body);

		user.save((err, user) => {
			if (err) return next(err);
			return res.json({
				success: true,
				message: 'Update a user',
				data: user
			});
		});
	});

	// nodeAcl.hasRole( reqUser._id, "admin", function(err, hasRole){
	//     if (hasRole) {
	//         processQuery(query, projection);
	//     } else if (reqUser._id.toString() === req.params._id.toString()) {
	//         processQuery(query, projection);
	//     } else {
	//         return res.json({
	//             success: false,
	//             message: 'not authorized to edit',
	//             error: { message: "unauthorized access", code: 401}
	//         });
	//     }
	// });
};


/**
* @api {DELETE} /api/administrator/:_id Delete a administrator
* @apiName DeleteAdministrator
* @apiGroup Administrator
* @apiVersion 0.0.0
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    "success": "true",
*    "message": "Delete a user"
*  }
*/

export const deleteUser = (req, res, next) => {

	// User.findOne(query, projection).exec((err, user) => {
	//   if (err) return next(err);
	//   user.status = 'Archived';
	//
	//   user.save((err, user) => {
	//     if (err) return next(err);
	//     return res.json({
	//       success: true,
	//       message: 'Archived a user',
	//       data: user
	//     });
	//   });
	// });

  Administrator.findOne({_id: req.params._id}).exec()
	.then((user) => {
		logger.info('user to be DELETED: ', user);
		user.status = 'Archived';
		return user.save();
	})
	.then((deletedUser) => {
		return res.status(200).json(placeHolder(true, 'Deleted a user'))
	})
	.catch((error) => {
		return next(error);
	});
};
