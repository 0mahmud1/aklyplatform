import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// deprecated soon
export const userMinSchema = new Schema({
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	roles: {
		type: [String],
		required: [true, 'Role is required.']
	}
});

const administratorSchema = new Schema({
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.'],
		unique: true
	},
	auth0: {
		type: String,
		required: [true, 'you cannot create user unless creating in auth0 first']
	},
	address: [Schema.Types.Mixed],
	profile: Schema.Types.Mixed,
	status: {
		type: String,
		enum: ['Active', 'Archived'],
		default: 'Active'
	},
	createdAt: Date,
	updatedAt: Date
});

administratorSchema.pre('save', function (next) {
	// get the current date
	let currentDate = new Date();
	// change the updated_at field to current date
	this.updatedAt = currentDate;
	// if created_at doesn't exist, add to that field
	if (!this.createdAt) {
		this.createdAt = currentDate;
	}
	next();
});

const Administrator = mongoose.model('administrator', administratorSchema);
export default Administrator;

