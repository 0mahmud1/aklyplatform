/**
 * @name configureAdminUser
 * @description Create default admin user if do not exist any admin user
 * @author Zahedul Alam<z.alam015@gmail.com>
 */

import _ from 'lodash';
import colors from 'colors';
import jwt from 'jsonwebtoken'
import config from 'config'
import axios from 'axios'

import { logger } from 'libs/logger.js';
import Administrator from '../models';

/**
 * Create A default admin user if not exists
 */
export const configureAdminUser = () => {
  try {
    Administrator.findOne({}, (err, data) => {
      if (!_.hasIn(data, '_id')) {
        logger.info('no admin user found');
        const basicInfo = {
          name: 'Akly Administrator',
          email: 'admin@akly.com',
          password: 'qweqwe',
        };

        const authToken = jwt.sign({
          aud: config.get('AUTH0.API_KEY'),
          scopes: {
            users: {
              actions: ["read", "create"]
            }
          }
        }, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
        console.log(authToken);
        console.log('-----------');
        const axiosConfig = {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + authToken
          }
        };
        let url = config.get('AUTH0.BASE_URL') + 'api/v2/users';
        url += '?connection=' + config.get('AUTH0.CONNECTION');
        const authData = {
          connection: config.get('AUTH0.CONNECTION'),
          name: basicInfo.name,
          email: basicInfo.email,
          password: basicInfo.password,
          user_metadata: {
            roles: ['administrator']
          },
        };

        axios.get(url+'&email:"'+basicInfo.email+'"', axiosConfig)
          .then((response)=>{
            console.log('check admin in auth user', response.data);
            if(response.data.length > 0) {
              const user = new Administrator({
                name: response.data[0].name,
                email: response.data[0].email,
                auth0: response.data[0].user_id,
                status: 'Active',
              });
              user.save((err, user) => {
                if (err) throw err;
                console.log("------- Admin Info -----".bold.cyan);
                console.log("email: %s".bold.magenta, user.email.bold.blue);
                console.log("password: %s".bold.magenta, 'qweqwe'.bold.blue);
                console.log("------------------------".bold.cyan);
                logger.info('administrator added ', user._doc);

              }).catch((error) => {
                logger.error('administrator creation failed', error);
              });
            } else {
              axios.post(url, authData, axiosConfig).then((response) => {
                logger.info('response from auth0', response.data);

                const user = new Administrator({
                  name: response.data.name,
                  email: response.data.email,
                  auth0: response.data.user_id,
                  status: 'Active',
                });
                user.save((err, user) => {
                  if (err) throw err;
                  console.log("------- Admin Info -----".bold.cyan);
                  console.log("email: %s".bold.magenta, user.email.bold.blue);
                  console.log("password: %s".bold.magenta, 'qweqwe'.bold.blue);
                  console.log("------------------------".bold.cyan);
                  logger.info('administrator added ', user._doc);

                }).catch((error) => {
                  logger.error('administrator creation failed', error);
                });
              }).catch((error) => {
                console.error('Can\'t Add Administrator to Auth0...', error);
                const err = new Error(error.response.data.message);
                err.status = error.response.status;
                throw err;
              });
            }

          }).catch((error) => {
            console.error('Can\'t Add Administrator to Auth0...', error);
            const err = new Error(error.response.data.message);
            err.status = error.response.status;
            throw err;
          });
      } else {
        logger.info('administrator found');
        console.log("|------- Admin Info ------|".bold.cyan);
        console.log("|- email: %s -|".bold.magenta, data.email.bold.blue);
        console.log("|- password: %s".bold.magenta, 'qweqwe ------|'.bold.blue);
        console.log("|-------------------------|".bold.cyan);
      }
    });
  } catch (e) {
    logger.error(e);
  }
};
