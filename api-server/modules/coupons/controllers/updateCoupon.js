import Coupon from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {PUT} /api/coupons/:_id Update a coupon
* @apiName updateCoupon
* @apiGroup Coupon
* @apiParam {String} _id Coupon unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a coupon
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Update a coupon",
*      "data": {
*       "__v": 0,
*       "_id": "String",
*       "code": String,
*       "couponType": String,
*       "value": Number,
*       "maxUsed": Number,
*       "startedAt": Date,
*       "expiredAt": Date,
*       "description": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*      }
*  }
*/
export const updateCoupon = (req, res, next) => {
  logger.info('request in update coupon route');
  try {
        console.log('req ==>>', req.body);

    Coupon.findOne({_id: req.params._id , status: 'Active'}).exec()
    .then((coupon) => {
      console.log('coupon to be UPDATED: ', coupon);
      if (!coupon) {
        throw new Error('no coupon found for update');
      }
      Object.assign(coupon, coupon ,req.body);
      return coupon.save();
    })
    .then((updatedCoupon) => {
      return res.status(200).json(placeHolder(true, 'Update a coupon', updatedCoupon))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateCoupon',
      error: e
    });
  }
};
