import Coupon from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {DELETE} /api/coupons/:_id Delete a coupon
* @apiName deleteCoupon
* @apiGroup Coupon
* @apiParam {String} _id Coupon unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Delete a coupon
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "delete a coupon",
*  }
*/
export const deleteCoupon = (req, res, next) => {
  try {
    logger.info('delete request in coupon route');

    Coupon.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((coupon) => {
      logger.info('coupon to be DELETED: ', coupon);
      coupon.status = 'Archived';
      return coupon.save();
    })
    .then((deletedCoupon) => {
      return res.status(200).json(placeHolder(true, 'Deleted a coupon'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteCoupon',
      error: e
    });
  }
};
