import Coupon from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {GET} /api/coupons/:_id Read a category
* @apiName readCoupon
* @apiGroup Coupon
* @apiParam {String} _id Coupon unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a coupon
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Read a coupon",
*      "data": {
*       "__v": 0,
*       "_id": "String",
*       "code": String,
*       "couponType": String,
*       "value": Number,
*       "maxUsed": Number,
*       "startedAt": Date,
*       "expiredAt": Date,
*       "description": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*      }
*  }
*/
export const readCoupon = (req, res, next) => {
  logger.info('request in read coupon route');
  try {
    Coupon.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((coupon) => {
      console.log('my coupon: ...', coupon);
      if (!coupon) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      return res.status(200).json({
        success: true,
        message: 'Read a coupon',
        data: coupon
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readCoupon',
      error: e
    });
  }
};
