import Coupon from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

import { checkValidity } from '../libs/checkValidity';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

/**
* @api {GET} /api/coupons/validate/:code check a coupon validity
* @apiName validateCoupon
* @apiGroup Coupon
* @apiParam {String} code Coupon unique code
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message validate a coupon
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "check coupon validity",
*      "data": {
*       "__v": 0,
*       "isValidate": Boolean,
*       "isExpired": Boolean,
*       "coupon": Object,
*       "isExceedLimit": Boolean,
*      }
*  }
*/
export const validateCoupon = (req, res, next) => {
  try {
    logger.info('request in coupon validation check');
    const code = req.params.code;
    const userEmail = req.decoded.email;
    const options = {
      userEmail,
      code
    };
    checkValidity(options, (err,response) => {
      if(err) {
        console.log('catch error here ',err);
        res.status(500).json({
          success: false,
          message: 'Error in validate Coupon',
          error: err
        });
      }
      else {
        return res.status(200).json({
          success: true,
          message: 'validate a coupon',
          data: response
        })
      }
    })

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in validate Coupon',
      error: e
    });
  }
};
