import Coupon from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {POST} /api/coupons Create a coupon
* @apiVersion 0.0.1
* @apiName createCoupon
* @apiDescription create new coupon
* @apiGroup coupon
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a coupon
* @apiSuccess (200) {Object} See Success-Response
* @apiSuccessExample {json} Success-Response:
*{
*   "success": true
*   "message": "Create a coupon"
*   "coupon": {
*    "__v": 0,
*    "_id": "String",
*    "code": String,
*    "couponType": String,
*    "value": Number,
*    "maxUsed": Number,
*    "startedAt": Date,
*    "expiredAt": Date,
*    "description": String,
*    "createdAt": Date,
*    "updatedAt": Date,
*   }-
*}
*/


export const createCoupon = (req, res, next) => {
  logger.info('request in create coupon route');
  try {
    const newCoupon = new Coupon(req.body);
    newCoupon.save()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Create a coupon',
        data: response
      })
    })
    .catch((error) => {
      res.status(400).json({
        success: false,
        message: 'Failed to create a coupon',
        data: error
      })
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createCoupon',
      error: e
    });
  }

};
