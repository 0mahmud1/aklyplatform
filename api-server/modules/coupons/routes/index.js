import express from 'express';
import { logger } from 'libs/logger.js';

import { getCoupons } from '../controllers/getCoupons.js';
import { createCoupon } from '../controllers/createCoupon.js';
import { readCoupon } from '../controllers/readCoupon.js';
import { updateCoupon } from '../controllers/updateCoupon.js';
import { deleteCoupon } from '../controllers/deleteCoupon.js';
import { validateCoupon } from '../controllers/validateCoupon.js';

import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const couponRoutes = express.Router();

couponRoutes.use(['/coupons','/coupons/:_id', '/coupons/validate/:code'],(req, res, next) => {
    logger.info(`a ${req.method} request in coupons route.`);
    checkAuthToken(req, res, next);
});
couponRoutes.route('/coupons')
  // get all active coupons (accessed at GET /api/coupons)
  .get(getCoupons)
  // create a coupon (accessed at POST /api/coupons)
  .post(isAuthorizedRoles("administrator", "customer"),createCoupon)

couponRoutes.route('/coupons/validate/:code')
  // check validate coupon
  .get(isAuthorizedRoles("administrator", "customer"),validateCoupon)

couponRoutes.route('/coupons/:_id')
  // Read a coupon (accessed at GET /api/coupons/:_id)
  .get(isAuthorizedRoles("administrator", "customer"),readCoupon)
  // Update a coupon (accessed at PUT /api/coupons/:_id)
  .put(isAuthorizedRoles("administrator", "customer"),updateCoupon)
  // Delete a coupon (accessed at DELETE /api/coupons/:_id)
  .delete(isAuthorizedRoles("administrator", "customer"),deleteCoupon)

export default couponRoutes;
