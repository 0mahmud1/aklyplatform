import Coupon from '../models';
import CouponUser from '../../couponUsers/models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

export function checkValidity(options,cb) {
  console.log("inside checkValidity function ");
  try {
    if(!options.userEmail)
      throw new Error('no user email found in params');
    if(!options.code)
      throw new Error('no coupon code found in params');

    const userEmail = options.userEmail;
    const couponCode = options.code;
    Coupon.findOne({code : couponCode}).exec()
    .then((coupon) => {
      if (!coupon) {
        let data = {};
        data.isValidate = false;              // if no coupon found, validity false
        data.isExpired = true;                // if no coupon found , coupon expired
        data.coupon = null;                   // if no coupon found , send null
        cb(null,data);
      } else {
        let data = {};
        data.isValidate = true;               // if coupon found, validity true
        data.coupon = coupon;                 // if coupon found
        let currentDate = new Date();
        if(currentDate > coupon.expiredAt) {  // if coupon expired
          data.isExpired = true;
        } else {
          data.isExpired = false;             // if current date is less than coupon expire date
        }
        CouponUser.findOne({                  // find a coupon user by email and coupon code
          'user.email': userEmail,
          'coupon.code': couponCode
        }).exec()
        .then((couponUser) => {
          if (!couponUser) {
            let user = {
              email : userEmail
            };
            let count = 0;
            const newCouponUser = new CouponUser({
              user,
              coupon,
              count
            });
            newCouponUser.save()
            .then((couponUser) => {
              console.log('new coupon user inserted',couponUser);
              data.couponUser = couponUser;
              if(couponUser.count >= coupon.maxUsed) {        // if number of coupon uses exceed max use limit
                data.isExceedLimit = true;
              } else {
                data.isExceedLimit = false;
              }
              cb(null,data);
            })
            .catch((error) => {
              console.log('Eroor in creating cupon user');
            })

          } else {
            data.couponUser = couponUser;
            if(couponUser.count >= coupon.maxUsed) {        // if number of coupon uses exceed max use limit
              data.isExceedLimit = true;
            } else {
              data.isExceedLimit = false;
            }
            cb(null,data);
          }
        })
        .catch((error) => {
          console.log('Error in finding coupon user');
        })

      }

    })
    .catch((error) => {
      cb(error,null)
    })
  } catch(e) {
    console.log('catch error ',e);
    cb(e,null)
  }
}
