import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a schema
const couponSchema = new Schema({
  code : {                               // unique code/key for each coupon
    type: String,
    required: true
  },
  couponType: {                          // discount is in percentage(10% discount) or in fixed amount(50$ discount)
    type: String,
    enum: ['percentage','fixed']
  },
  value: {
    type: Number
  },
  maxUsed: {                             // how many times this coupon can be used.
    type: Number                         // If maxUsed is 0 than coupon can be used unlimited times
  },
  startedAt: {                           // from when one can start use this coupon
    type: Date
  },
  expiredAt: {                           // expire date of this coupon
    type: Date
  },
  description: {
    type: String                         // other description about coupon
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
});

// on every save, add the date
couponSchema.pre('save', function(next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const Coupon = mongoose.model('Coupon', couponSchema);

// make this available in our applications
export default Coupon;
