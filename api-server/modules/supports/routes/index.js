import express from 'express';

import { logger } from 'libs/logger.js';
import { sendQuery } from '../controllers';

const supportRoutes = express.Router();


supportRoutes.route('/support')
    .post(sendQuery);


export default supportRoutes;
