import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { sendTransactionalEmail } from 'libs/emails.js'
import async from 'async'

export const sendQuery = (req, res, next) => {
  try {
    console.log('send query here ',req.body);
    let receiver = {
      name : req.body.name,
      email: req.body.email
    }
    sendTransactionalEmail(null,receiver,{
      subject: `Support Message`,
      html: req.body.comment
    })
    return res.status(200).json({
      success: true,
      message: 'success',
      data: receiver
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    return res.status(500).json({
      success: false,
      message: 'Error in deleteCategory',
      error: e
    });
  }
};
