import DeliveryBoy from '../models';
import {logger} from 'libs/logger.js';
import colors from 'colors';
import _ from 'lodash';
import jwt from 'jsonwebtoken'
import config from 'config'
import axios from 'axios'
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'

/*
*  RESTful CRUD APIs for User
*/

/**
* @api {GET} /api/dboys All active DeliveryBoys
* @apiVersion 0.0.0
* @apiName GetDeliveryBoy
* @apiGroup DeliveryBoy
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active Delivery Boys
* @apiSuccess (200) {Object[]} deliveryBoy see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "get active Delivery Boys",
*   "data": [
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "phone": "String",
*       "license": "String",
*       "address": Object,
*       "capacity": Number,
*       "auth0": "String",
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           Number
*         ],
*         "type": "String"
*       },
*       "status": "String",
*       "isBusy": Boolean,
*       "isAssociated": Boolean
*     },
*   ]
* }
*/

export const getAll = (req, res, next) => {
  try {
    logger.info('request for get all delivery boy');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = { status: 'Active' };

    delete req.query.limit;
    delete req.query.page;
    if(req.query) {
      Object.assign(query, query,{...req.query})
    }
    if(req.query['name']) {
        let name = req.query['name'];
        query['name'] = RegExp(name,'i') ;

    }
    if(req.query['email']) {
        let name = req.query['email'];
        query['email'] = RegExp(name,'i') ;

    }
    //this promise returns number of total object
    let findCountPromise = DeliveryBoy.find(query).count();

    //this promise returns actual data
    let findDataPromise = DeliveryBoy.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active Delivery Boys',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in get delivery boys',
      error: e
    });
  }
};

/**
* @api {POST} /api/dboys Create a DeliveryBoy
* @apiVersion 0.0.0
* @apiName CreateDeliveryBoy
* @apiGroup DeliveryBoy
* @apiHeader {String} Authorization valid access token
* @apiPermission admin, manager
* @apiParam data see Request-Example
*  @apiParamExample {Object} Request-Example:
* {
*	"name": "String",
*    "email": "String",
*    "password":"String",
*    "phone": "String",
*    "license": "String",
*    "address": Object,
*    "capacity": Number,
*    "locations": {Object},
*    "status": "String"
* }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Delivery Boy is created successfully and add into Auth0!
* @apiSuccess (200) {Object} deliveryBoys see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*  "success": true,
*  "message": "Delivery Boy is created successfully and add into Auth0!",
*  "data": {
*    "isAssociated": Boolean,
*    "isBusy": Boolean,
*    "status": "String",
*    "_id": "String",
*    "locations": {
*      "coordinates": [ Number ],
*      "type": "String"
*    },
*    "auth0": "String",
*    "capacity": Number,
*    "address": Object,
*    "license": "String",
*    "phone": "String",
*    "email": "String",
*    "name": "String",
*    "updatedAt": Date,
*    "createdAt": Date
*  }
* }
*/

export const createOne = (req, res, next) => {
  try {
    logger.info('delivery boy createOne');
    console.info(req.body);
    if (_.has(req.body, 'user_metadata')) {
      req.body.user_metadata.roles = ['deliveryBoy'];
    } else {
      req.body.user_metadata = { roles: ['deliveryBoy'] };
    }
    const authToken = jwt.sign({
      "aud": config.get('AUTH0.API_KEY'),
      "scopes": {
        "users": {
          "actions": ["create"]
        }
      }
    }, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authToken
      }
    };
    const url = config.get('AUTH0.BASE_URL') + 'api/v2/users';
    const authData = {
      'connection': config.get('AUTH0.CONNECTION'),
      'name': req.body.name,
      'email': req.body.email,
      'password': req.body.password,
      'user_metadata': req.body.user_metadata
    };
    //authData.user_metadata.roles = ['deliveryBoy'];

    axios.post(url, authData, axiosConfig).then((response) => {
      let userInfo = Object.assign({}, req.body, {auth0: response.data.user_id});
      delete userInfo.user_metadata;
      delete userInfo.password;
      const newUser = new DeliveryBoy(userInfo);
      return newUser.save();
    }).then((response) => {
      return res.status(201).json({
        success: true,
        message: 'Delivery Boy is created successfully and add into Auth0!',
        data: response._doc
      });
    }).catch((error) => {
      console.log(error);
      // logger.debug(error.stack);
      const err = new Error(error.response.data.message);
      err.status = error.response.status;
      return next(err);
    });

  } catch (e) {
    logger.error(e);
    res.status(400).json({
      success: false,
      message: 'Unable to create a user',
      error: e
    });
  }
};

/**
* @api {GET} /api/dboys/:_id Read a DeliveryBoy
 * @apiVersion 0.0.0
* @apiName ReadDeliveryBoy
* @apiGroup DeliveryBoy
* @apiPermission all
* @apiParam {String} _id DeliveryBoy unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a user
* @apiSuccess (200) {Object[]} deliveryBoy see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Read a user",
*   "data": [
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "phone": "String",
*       "license": "String",
*       "address": Object,
*       "capacity": Number,
*       "auth0": "String",
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           Number
*         ],
*         "type": "String"
*       },
*       "status": "String",
*       "isBusy": Boolean,
*       "isAssociated": Boolean
*     },
*   ]
* }
*/

export const readOne = (req, res, next) => {
  try {
    // let reqUser = req.decoded;
    let query = { _id: req.params._id };
    let projection = {};

    DeliveryBoy.findOne(query, projection).exec()
    .then((user) => {
      logger.info('delivery user: ', user);
      if (!user) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a user',
        data: user
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);

    return res.status(404).json({
      success: true,
      message: 'unable to read a delivery boy',
      error: e
    })
  }
};

/**
* @api {PUT} /api/dboys/:_id Update a DeliveryBoy
* @apiVersion 0.0.0
* @apiName UpdateDeliveryBoy
* @apiGroup DeliveryBoy
*
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id DeliveryBoy unique ID
* @apiPermission admin, manager, deliveryBoy
* @apiParamExample {Object} Request-Example-admin:
* {
*     "locations": {
*       "_id": "String",
*       "coordinates": [
*         longitude, latitude
*       ],
*       "type": "String"
*     }
*   }
*
* @apiParamExample {Object} Request-Example-deliveryBoy:
 * {
*     "name": "String",
*     "phone": "String",
*     "license": "String",
*     "address": Object,
*     "capacity": Number,
*     "locations": {
*       "_id": "String",
*       "coordinates": [
*         longitude, latitude
*       ],
*       "type": "String"
*     },
*     "isBusy": Boolean,
*     "isAssociated": Boolean
*   }
 *
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update Delivery Boy
* @apiSuccess (200) {Object[]} deliveryBoy see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Update Delivery Boy",
*   "data": [
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "phone": "String",
*       "license": "String",
*       "address": Object,
*       "capacity": Number,
*       "auth0": "String",
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           longitude, latitude
*         ],
*         "type": "String"
*       },
*       "status": "String",
*       "isBusy": Boolean,
*       "isAssociated": Boolean
*     },
*   ]
* }
*/


export const updateOne = (req, res, next) => {

  logger.info('request for update a user');

  try {
    let reqUser = req.decoded;
    let query = { _id: req.params._id };
    if (_.includes(reqUser.roles, 'deliveryBoy')) {
      DeliveryBoy.findOne(query).exec((err, data) => {
        if (err) return next(err);
        logger.info('PATCH Delivery Boy ', req.body);
        // data = Object.assign({}, data, req.body);
        logger.info(data.auth0,'------------------',reqUser.sub);
        if (data.auth0 === reqUser.sub) {
          if (req.body.address || req.body.locations || req.body.name || req.body.phone || req.body.license || req.body.capacity || req.body.password) {
            data = Object.assign(data, req.body);
            data.save((err, user) => {
              if (err) return next(err);

              // PubNub
              let information = 'deliveryBoy is update';
              let channelName = 'deliveryBoys-administrator';
              let data = Object.assign({ _id: req.params._id }, req.body);
              pubnubNotification(channelName,information,data)

              return res.json({
                success: true,
                message: 'Update Delivery Boy',
                data: user
              });
            }).catch(err => {
              console.log(err);
            });
          } else {
            next(errorPlaceHolder(400,false,'not a proper update request'));
          }
        } else {
          next(errorPlaceHolder(401,false,'Not expected Delivery Boy'));
        }
      });
    } else if(_.includes(reqUser.roles,'administrator','manager')){
      DeliveryBoy.findOne(query).exec((err, data) => {
        if (err) return next(err);
        logger.info('PATCH Delivery Boy ', req.body);

        data = Object.assign(data, req.body);
        if(req.body.locations){
          data.locations = req.body.locations
        }
        data.save((err, user) => {
          if (err) return next(err);
          return res.json({
            success: true,
            message: 'Update Delivery Boy',
            data: user
          });
        });
      });
    } else {
      next(errorPlaceHolder(401,false,'not expected user type'))
    }
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    next(errorPlaceHolder(500,false,'Error in Delivery Boy'))
  }
};

/**
* @api {DELETE} /api/dboys/:_id Delete a DeliveryBoy
* @apiVersion 0.0.0
* @apiName DeleteDeliveryBoy
* @apiGroup DeliveryBoy
*
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id DeliveryBoy unique ID
* @apiPermission admin, manager
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a user
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    "success": "true",
*    "message": "Deleted a user"
*  }
*/

export const deleteOne = (req, res, next) => {

  DeliveryBoy.findOne({_id: req.params._id}).exec()
  .then((user) => {
    logger.info('delivery boy to be DELETED: ', user);
    user.status = 'Archived';
    return user.save();
  })
  .then(() => {
    return res.status(200).json(placeHolder(true, 'Deleted a user'))
  })
  .catch((error) => {
    return next(error);
  });
};
