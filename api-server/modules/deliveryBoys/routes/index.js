import express from 'express';

import { logger } from 'libs/logger.js';
import { getAll, createOne, readOne, updateOne, deleteOne } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers';

let _ = require('lodash');

const dBoyRoutes = express.Router();

dBoyRoutes.use(['/dboys', '/dboys/:_id'],(req, res, next) => {
  logger.info(`a ${req.method} request in dboys route.`);
  logger.info(req.body);

  if (req.method === 'GET') {
    next();
  } else {
    checkAuthToken(req, res, next);
  }
});

dBoyRoutes.route('/dboys')
  .get(getAll)
  .post(isAuthorizedRoles('administrator', 'manager'), createOne);

dBoyRoutes.route('/dboys/:_id')
  .get(readOne)
  .put(isAuthorizedRoles('administrator', 'manager', 'deliveryBoy'), updateOne)
  .delete(isAuthorizedRoles('administrator', 'manager'), deleteOne);

export default dBoyRoutes;
