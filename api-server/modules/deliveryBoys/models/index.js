import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a sub schema
const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Point',
  },
  coordinates: {
    type: [Number]
  }
}, { _id : false });

const deliveryBoySchema = new Schema({
  auth0: {
    type: String
  },
  imgUrl: String,
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    unique: true
  },
  capacity: {
    type: Number,
    required: [true, 'capacity information required']
  },
  phone: String,
  license: String,
  address: Schema.Types.Mixed, // delivery boy address
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  profile: Schema.Types.Mixed, // delivery boy others information
  isAssociated: {
    type: Boolean,
    default: 0
  },
  isBusy: {
    type: Boolean,
    default: false
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdBy: Schema.Types.Mixed,
  createdAt: Date,
  updatedAt: Date
}, { versionKey: false });

deliveryBoySchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

const DeliveryBoy = mongoose.model('DeliveryBoy', deliveryBoySchema);

// make this available in our applications
export default DeliveryBoy;
