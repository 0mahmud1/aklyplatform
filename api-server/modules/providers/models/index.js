import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a sub schema
const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Point',
  },
  coordinates: {
    type: [Number]
  }
});

const providerSchema = new Schema({
  auth0: {
    type: String
  },
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    unique: true
  },
  isAssociated: {
    type: Boolean,
    default: 0
  },
  onDemandCapacity: {
    type: Number,
    default: 2
  },
  subscriptionCapacity: {
    type: Number,
    default: 50
  },
  providerType:{
    type: [String]
  },
  vans: [Schema.Types.Mixed],
  address: Schema.Types.Mixed, // foodProvider address
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  profile: Schema.Types.Mixed, // foodProvider others information
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdBy: Schema.Types.Mixed,
  createdAt: Date,
  updatedAt: Date
});

providerSchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

const FoodProvider = mongoose.model('FoodProvider', providerSchema);

// make this available in our applications
export default FoodProvider;
