import FoodProvider from '../models';
import Van from 'modules/vans/models';
import { logger } from 'libs/logger.js';
import colors from 'colors';
import _ from 'lodash';
import jwt from 'jsonwebtoken'
import config from 'config'
import axios from 'axios'
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

/*
*  RESTful CRUD APIs for foodproviders
*/

/**
* @api {GET} /api/foodproviders All active FoodProvider
* @apiVersion 0.0.0
* @apiName GetFoodProviders
* @apiGroup FoodProvider
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message get active Food Providers
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "get active Food Providers",
*   "data": [
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "address": {
*         "geoLocation": {
*           "coordinates": [
*             Number
*           ]
*         }
*       },
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           Number
*         ],
*         "type": "String"
*       },
*       "auth0": "String",
*       "__v": Number,
*       "status": "String",
*       "vans": [
*         {
*           "isAssociated": "String",
*           "status": "String",
*           "__v": Number,
*           "auth0": "String",
*           "capacity": Number,
*           "address": "String",
*           "license": "String",
*           "email": "String",
*           "name": "String",
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": "String"
*         }
*       ],
*       "providerType": [
*          String
*       ],
*       "subscriptionCapacity": Number,
*       "onDemandCapacity": Number,
*       "isAssociated": Boolean
*     }
*   ]
* }
*/

export const getAll = (req, res, next) => {
  try {
    logger.info('request to get active food providers');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param
    if(req.query['name']) {
        let name = req.query['name'];
        query['name'] = RegExp(name,'i') ;

    }
    if(req.query['email']) {
        let name = req.query['email'];
        query['email'] = RegExp(name,'i') ;

    }
    // if(req.query) {
    //   Object.assign(query, query,{...req.query})
    // }

    //this promise returns number of total object
    let findCountPromise = FoodProvider.find(query).count();

    //this promise returns actual data
    let findDataPromise = FoodProvider.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active food providers',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in get food provider',
      error: e
    });
  }
};

export const checkUser = (req, res, next) => {
  logger.info('call check provider', req.decoded);

  try {
    let query = { auth0: req.decoded.sub, email: req.decoded.email };
    logger.info('query: ', query)
    FoodProvider.findOne(query).exec()
      .then((response) => {
        if (response) {
          return res.status(200).json({
            success: true,
            message: 'Provider found',
            data: response
          });
        } else {
          logger.info('No provider Found');
          logger.debug(req.decoded);
          let providerProfile = {
            email: req.decoded.email,
            auth0: req.decoded.sub
          };

          if(req.decoded.name){
            providerProfile = Object.assign(providerProfile,providerProfile,{ name: req.decoded.name });
          }

          const provider = new User(providerProfile);

          provider.save()
            .then(response => {
              return res.status(200).json({
                success: true,
                message: 'Created new provider',
                data: response
              });
            })
        }
      })
      .catch(err => next(err));
  } catch (e) {
    // logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in check provider',
      error: e
    });
  }
};

/**
* @api {POST} /api/foodproviders Create a FoodProvider
* @apiVersion 0.0.0
* @apiName CreateFoodProvider
* @apiGroup FoodProvider
* @apiPermission admin, manager
* @apiHeader {String} Authorization valid access token
* @apiParamExample {Object} Request-Example:
{
     "name": "String",
     "email": "String",
     "password": "String",
     "address": {
       "geoLocation": {
         "coordinates": [
           Number
         ]
       }
     },
     "locations": {
       "_id": "String",
       "coordinates": [
         Number
       ],
       "type": "String"
     },
     "status": "String",
     "vans": [ Object ],
     "providerType": [
       "String"
     ],
     "subscriptionCapacity": Number,
     "onDemandCapacity": Number,
     "isAssociated": Boolean
   }
*
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Food Provider created successfully and add to Auth0!
 * @apiSuccess (200) {Object[]} data see Success-Response
 * @apiSuccessExample {JSON} Success-Response:
 *  HTTP/1.1 200 OK
 {
   "success": true,
   "message": "Food Provider created successfully and add to Auth0!",
   "data": {
     "isAssociated": Boolean,
     "onDemandCapacity": Number,
     "subscriptionCapacity": Number,
     "providerType": [
       "String"
     ],
     "vans": [Object],
     "status": "String",
     "_id": "String",
     "auth0": "String",
     "locations": {
       "_id": "String",
       "coordinates": [
         Number
       ],
       "type": "String"
     },
     "address": {
       "geoLocation": {
         "coordinates": [
           Number
         ]
       }
     },
     "email": "String",
     "name": "String",
     "updatedAt": Date,
     "createdAt": Date,
   }
 }

 */

export const createOne = (req, res, next) => {
  try {
    //@todo check email and password is exists
    console.info(req.body);
    const authToken = jwt.sign({
      "aud": config.get('AUTH0.API_KEY'),
      "scopes": {
        "users": {
          "actions": ["create"]
        }
      }
    }, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authToken
      }
    };
    const url = config.get('AUTH0.BASE_URL') + 'api/v2/users';
    const authData = {
      'connection': config.get('AUTH0.CONNECTION'),
      'name': req.body.name,
      'email': req.body.email,
      'password': req.body.password,
      'user_metadata': req.body.user_metadata
    };
    //authData.user_metadata.roles = ['FoodProvider'];

    axios.post(url, authData, axiosConfig).then((response) => {
      let userInfo = Object.assign({}, req.body, {auth0: response.data.user_id});
      delete userInfo.user_metadata;
      delete userInfo.password;
      const newUser = new FoodProvider(userInfo);
      return newUser.save();
    }).then((response) => {
      console.log('after response');
      console.log(response);
      if (req.body.vans) {
        req.body.vans.forEach(function (van) {
          Van.update({
            _id: van._id
          },{
            $set: { isAssociated: 'true', locations: response.locations }
          }).then(res => {
            logger.info(`van association status changed`)
          }).catch(error => {
            logger.error(error);
          });
        });
      }

      return res.status(201).json({
        success: true,
        message: 'Food Provider created successfully and add to Auth0!',
        data: response._doc
      });
    }).catch((error) => {
      console.error('Can\'t Add User to Auth0...', error);
      const err = new Error(error.response.data.message);
      err.status = error.response.status;
      return next(err);
    });

  } catch (e) {
    res.status(400).json({
      success: false,
      message: 'Unable to create a Food Provider',
      error: e
    });
  }
};

/**
* @api {GET} /api/foodproviders/:_id Read a FoodProvider
* @apiVersion 0.0.0
* @apiName ReadFoodProviders
* @apiGroup FoodProvider
* @apiParam {String} _id FoodProvider unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Read a FoodProvider
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Read a FoodProvider",
*   "data":
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "address": {
*         "geoLocation": {
*           "coordinates": [
*             Number
*           ]
*         }
*       },
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           Number
*         ],
*         "type": "String"
*       },
*       "auth0": "String",
*       "__v": Number,
*       "status": "String",
*       "vans": [
*         {
*           "isAssociated": "String",
*           "status": "String",
*           "__v": Number,
*           "auth0": "String",
*           "capacity": Number,
*           "address": "String",
*           "license": "String",
*           "email": "String",
*           "name": "String",
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": "String"
*         }
*       ],
*       "providerType": [
*          String
*       ],
*       "subscriptionCapacity": Number,
*       "onDemandCapacity": Number,
*       "isAssociated": Boolean
*     }
* }
 */

export const readOne = (req, res, next) => {
  try {
    let projection = {password: 0, service: 0},
    query = {_id: req.params._id};

    logger.info('update rq: ', req.query)

    FoodProvider.findOne(query, projection).exec()
    .then((user) => {
      logger.info('food provider user: ', user);
      if (!user) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a FoodProvider',
        data: user
      })
    })
    .catch((error) => {
      next(error);
    });

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
  }
};

/**
* @api {PUT} /api/foodproviders/:_id Update a FoodProvider
* @apiVersion 0.0.0
* @apiName UpdateFoodProvider
* @apiGroup FoodProvider
* @apiHeader {String} Authorization valid access token
* @apiPermission admin, manager, foodProvider
* @apiParam {String} _id FoodProvider unique ID
*
* @apiParamExample {Object} Request-Example:
* {
*   "name": "String",
*   "address": {
*     "geoLocation": {
*       "coordinates": [
*         Number
*       ]
*     }
*   },
*   "locations": Object,
*   "status": "Active",
*   "vans": [],
*   "providerType": [
*     "String"
*   ],
*   "subscriptionCapacity": Number,
*   "onDemandCapacity": Number,
*   "isAssociated": Boolean
* }
*
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Update food provider
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Update food provider",
*   "data":
*     {
*       "_id": "String",
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": "String",
*       "email": "String",
*       "address": {
*         "geoLocation": {
*           "coordinates": [
*             Number
*           ]
*         }
*       },
*       "locations": {
*         "_id": "String",
*         "coordinates": [
*           Number
*         ],
*         "type": "String"
*       },
*       "auth0": "String",
*       "__v": Number,
*       "status": "String",
*       "vans": [
*         {
*           "isAssociated": "String",
*           "status": "String",
*           "__v": Number,
*           "auth0": "String",
*           "capacity": Number,
*           "address": "String",
*           "license": "String",
*           "email": "String",
*           "name": "String",
*           "updatedAt": Date,
*           "createdAt": Date,
*           "_id": "String"
*         }
*       ],
*       "providerType": [
*          String
*       ],
*       "subscriptionCapacity": Number,
*       "onDemandCapacity": Number,
*       "isAssociated": Boolean
*     }
* }
*/

export const updateOne = (req, res, next) => {
  try {

    logger.info('request for update a food provider');

    let reqUser = req.decoded;
    let query = { _id: req.params._id };
    // let projection = {};

    function processQuery(query) {
      FoodProvider.findOne(query).exec((err, user) => {
        if (err) return next(err);

        logger.info('PATCH food provider ', req.body);

        Object.assign(user, user, req.body);

        user.save((err, user) => {
          if (err) return next(err);
          req.body.vans.forEach(function (van) {
            Van.update({
              _id: van._id
            },{
              $set: { isAssociated: 'true', locations: user.locations }
            }).then(vur => logger.info(`van association status changed`));
          });
          return res.json({
            success: true,
            message: 'Update food provider',
            data: user
          });
        });
      });
    }

    if (reqUser.roles["0"] === "administrator" || reqUser.roles["0"] === "manager" || reqUser.roles['0'] === 'foodProvider') {
      processQuery(query);
    } else if (reqUser._id.toString() === req.params._id.toString()) {
      processQuery(query);
    } else {
      return res.json({
        success: false,
        message: 'not authorized to edit',
        error: {message: "unauthorized access", code: 401}
      });
    }
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);

    return res.json({
      success: false,
      message: 'Error in code block',
      error: { message: 'Error in code block', code: 500, reason: e.reason, detail: e }
    });
  }
};

/**
* @api {DELETE} /api/foodproviders/:_id Delete a FoodProvider
 * @apiVersion 0.0.0
* @apiName DeleteFoodProvider
* @apiGroup FoodProvider
 * @apiHeader {String} Authorization valid access token
* @apiParam {String} _id FoodProvider unique ID
*
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    "success": "true",
*    "message": "Delete a user"
*  }
*/

export const deleteOne = (req, res, next) => {

  FoodProvider.findOne({_id: req.params._id}).exec()
  .then((user) => {
    logger.info('user to be DELETED: ', user);
    user.status = 'Archived';
    return user.save();
  })
  .then(() => {
    return res.status(200).json(placeHolder(true, 'Deleted a user'))
  })
  .catch((error) => {
    return next(error);
  });
};
