import express from 'express';

import {logger} from 'libs/logger.js';
import { getAll, createOne, readOne, updateOne, deleteOne, checkUser } from '../controllers';
import {checkAuthToken, isAuthorizedRoles} from '../../auth/controllers';

let _ = require('lodash');

const fpRoutes = express.Router();

fpRoutes.use(['/foodproviders', '/foodproviders/:_id'], (req, res, next) => {
    logger.info(`a ${req.method} request in food providers route.`);
    if (req.method !== 'GET') {
        checkAuthToken(req, res, next);
    } else {
        next();
    }
});

fpRoutes.use('/foodproviders/check', (req, res, next) => {
    logger.info(`a ${req.method} request in food providers/check route.`);
    checkAuthToken(req, res, next);
});

fpRoutes.route('/foodproviders')
    .get(getAll)
    .post(isAuthorizedRoles('administrator', 'manager'),createOne);

fpRoutes.route('/foodproviders/check')
  .get(checkUser);

fpRoutes.route('/foodproviders/:_id')
// Read a user (accessed at GET /api/foodproviders/:_id)
    .get(readOne)
    // Update a user (accessed at PUT /api/foodproviders/:_id)
    .put(isAuthorizedRoles('administrator', 'manager','foodProvider'), updateOne)
    // Delete a user (accessed at DELETE /api/foodproviders/:_id)
    .delete(isAuthorizedRoles('administrator', 'manager'),deleteOne);

// router.post('/authenticate', authController.authenticateUser);

export default fpRoutes;