import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const customerSchema = new Schema({
  auth0: {
    type: String
  },
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    unique: true
  },
  phone: String,
  address: [Schema.Types.Mixed],
  profile: Schema.Types.Mixed,
  avatar: {
    name: String,
    key: String,
    location: String
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
}, { versionKey: false });

customerSchema.pre('save', function (next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

const Customer = mongoose.model('Customer', customerSchema);

// make this available in our applications
export default Customer;
