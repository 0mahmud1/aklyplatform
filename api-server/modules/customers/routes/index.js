import express from 'express';

import { logger } from 'libs/logger.js';
import { getAll, readOne, updateOne, deleteOne, readOneByAuth0, checkCustomer } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers';

let _ = require('lodash');

const customerRoutes = express.Router();

customerRoutes.use(['/customers', '/customers/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in customers route.`);
    logger.info(req.body);
    checkAuthToken(req, res, next);
});

customerRoutes.route('/customers')
    .get(getAll);
    //.post(isAuthorizedRoles('administrator', 'manager','customer'), synchronize);


customerRoutes.route('/customers/check')
  .get(checkCustomer);

// @todo will implement roles. pause for client problem
customerRoutes.route('/customers/:_id')
    .get(readOne)
    .put(updateOne)
    .delete(isAuthorizedRoles('administrator', 'manager'), deleteOne);

export default customerRoutes;
