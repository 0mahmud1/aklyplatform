import Customer from '../models';
import {logger} from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

/*
 *  RESTful CRUD APIs for User
 */

/**
* @api {GET} /api/customers All active Users
* @apiName GetUsers
* @apiGroup Customer
* @apiHeader {String} Authorization valid access token
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Get all active customers
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Get all active customers",
*      "data": [{
*          "_id": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "email": String,
*          "auth0": String,
*          "status": "active",
*          "phone": String,
*          "address":
*          [
*            {
*              "street": String,
*              "apt": String,
*              "label": String,
*              "geoLocation": {
*                "lat": Number,
*                "lng": Number
*              }
*            }
*          ]
*      }]
*  }
*/

export const getAll = (req, res, next) => {
  try {
    logger.info('request for get all customer');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    if (req.query['name']) {
      query['name'] = RegExp(RegExp.escape(req.query['name']),'i')//req.query['name']

    }
      if (req.query['email']) {
          query['email']= RegExp(RegExp.escape(req.query['email']),'i')
      }
    // @todo insert required queries in query param

    // if (req.query) {
    //   Object.assign(query, query, {...req.query})
    // }

    //this promise returns number of total object
    let findCountPromise = Customer.find(query).count();

    //this promise returns actual data
    let findDataPromise = Customer.find(query).sort({createdAt:-1}).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'Get all active customers',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in get all active customer',
      error: e
    });
  }
};


/**
 * @api {GET} /api/customers/check Check customer in database
 * @apiName Check Customers
 * @apiGroup Customer
 * @apiHeader {String} Authorization valid access token
 * @apiPermission Customer
 * @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
 * @apiSuccess (200) {String} message Get all customer
 * @apiSuccess (200) {Object[]} data see Success-Response
 * @apiSuccessExample { JSON } Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *     "success": true,
 *     "message": "Created new customer",
 *     "data": {
 *       "createdAt": "Date | ISODate",
 *       "updatedAt": "Date | ISODate",
 *       "email": "email | String",
 *       "auth0": "auth0 id ! String",
 *       "_id": "mongodb id | String",
 *       "status": "Active",
 *       "phone": String,
 *       "address": []
 *     }
 *  }
 */
export const checkCustomer = (req, res, next) => {
  logger.info('call check customer', req.decoded);

  try {
    let query = { auth0: req.decoded.sub, email: req.decoded.email };
    logger.info('query: ', query);
    Customer.findOne(query).exec()
    .then((response) => {
      if (response) {
        return res.status(200).json({
          success: true,
          message: 'Customer found',
          data: response
        });
      } else {
        logger.info('No customer Found');
        logger.debug(req.decoded);
        const customerProfile = {
          email: req.decoded.email,
          name: req.decoded.app_metadata.full_name,
          auth0: req.decoded.sub
        };

        const customer = new Customer(customerProfile);

        customer.save()
        .then(response => {
          return res.status(200).json({
            success: true,
            message: 'Created new customer',
            data: response
          });
        })
      }
    })
    .catch(err => next(err));
  } catch (e) {
    // logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in check customer',
      error: e
    });
  }
};

/**
* @api {GET} /api/customers/:_id Read a user
* @apiName ReadUser
* @apiGroup Customer
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Get active customers
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Get active customer",
*      "data": [{
*          "_id": "String",
*          "createdAt": Date,
*          "updatedAt": Date,
*          "email": "String",
*          "auth0": "String",
*          "status": "active",
*          "phone": String,
*          "address":
*          [
*            {
*              "street": String,
*              "apt": String,
*              "label": String,
*              "geoLocation": {
*                "lat": Number,
*                "lng": Number
*              }
*            }
*          ]
*      }]
*  }
*/

export const readOne = (req, res, next) => {
  try {
    // let reqUser = req.decoded;
    let query = {_id: req.params._id};
    let projection = {};

    Customer.findOne(query, projection).exec()
    .then((user) => {
      logger.info('customer user: ', user);
      if (!user) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a customer user',
        data: user
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);

    return res.status(404).json({
      success: true,
      message: 'unable to read a customer',
      error: e
    })
  }
};

export const readOneByAuth0 = (req, res, next) => {
  try {
    // let reqUser = req.decoded;
    let query = {auth0: req.params.auth0};
    let projection = {};

    Customer.findOne(query, projection).exec()
    .then((user) => {
      logger.info('customer user: ', user);
      if (!user) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a customer user by auth0 id',
        data: user
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);

    return res.status(404).json({
      success: true,
      message: 'unable to read a customer',
      error: e
    })
  }
};

/**
* @api {PUT} /api/customers/:_id Update a User
* @apiName UpdateUser
* @apiGroup Customer
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiPermission All
* @apiParamExample {Object} Request-Example:
*  {
*      "name": "String",
*      "phone": String,
*      "address":
*          [
*            {
*              "street": String,
*              "apt": String,
*              "label": String,
*              "geoLocation": {
*                "lat": Number,
*                "lng": Number
*              }
*            }
*          ]
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Update active customer
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Update active customer",
*      "data": {
*          "_id": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "email": String,
*          "auth0": String,
*          "status": "active",
*          "phone": String,
*          "address":
*          [
*            {
*              "street": String,
*              "apt": String,
*              "label": String,
*              "geoLocation": {
*                "lat": Number,
*                "lng": Number
*              }
*            }
*          ]
*      }
*  }
*/

export const updateOne = (req, res, next) => {

  logger.info('request for update a customer user');

  try {
    let query = {_id: req.params._id};

    Customer.findOne(query).exec((err, user) => {
      if (err) return next(err);

      logger.info('PATCH CUSTOMER USER ', req.body);

      Object.assign(user, user, req.body);

      user.save((err, user) => {
        if (err) return next(err);
        return res.json({
          success: true,
          message: 'Update a customer user',
          data: user
        });
      });
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in update customer',
      error: e
    });
  }
};

/**
* @api {DELETE} /api/customers/:_id Delete a User
* @apiName DeleteUser
* @apiGroup Customer
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id User unique ID
* @apiPermission administrator, manager
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Delete a user
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    "success": "true",
*    "message": "Delete a user"
*  }
*/

export const deleteOne = (req, res, next) => {

  Customer.findOne({_id: req.params._id}).exec()
  .then((user) => {
    logger.info('customer boy to be DELETED: ', user);
    user.status = 'Archived';
    return user.save();
  })
  .then(() => {
    return res.status(200).json(placeHolder(true, 'Deleted a customer user'))
  })
  .catch((error) => {
    return next(error);
  });
};
