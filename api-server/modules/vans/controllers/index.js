import Van from '../models';
import {logger} from 'libs/logger';
import colors from 'colors';
import _ from 'lodash';
import jwt from 'jsonwebtoken'
import config from 'config'
import axios from 'axios'
import { checkAuthToken } from '../../auth/controllers'
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { AklyPubNub } from 'libs/pubnub.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'

/*
*  RESTful CRUD APIs for User
*/

/**
* @api {GET} /api/vans All active Van
* @apiVersion 0.0.0
* @apiName GetVans
* @apiGroup Van
* @apiHeader {String} Authorization valid access token
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get all active vans
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "get all active vans",
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "email": String,
*       "license": String,
*       "address": String,
*       "capacity": Number,
*       "auth0": String,
*       "__v": Number,
*       "status": String,
*       "isAssociated": Boolean
*     },
*   ]
* }
*/

export const getAll = (req, res, next) => {
  try {
    logger.info('request to get active vans');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param
    delete req.query.limit;
    delete req.query.page;
    if(req.query) {
        Object.assign(query, query,{...req.query})
    }
    if(req.query['email']){
      query['email'] = RegExp(RegExp.escape(req.query['email']),'i')
    }
    if(req.query['name']){
        query['name'] = RegExp(RegExp.escape(req.query['name']),'i')
    }

    //this promise returns number of total object
    let findCountPromise = Van.find(query).count();

    //this promise returns actual data
    let findDataPromise = Van.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active vans',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getAll van',
      error: e
    });
  }
};

/**
* @api {POST} /api/vans Create a Van
* @apiVersion 0.0.0
* @apiName CreateVan
* @apiGroup Van
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager
* @apiParamExample {Object} Request-Example:
* {
*    "name": String,
*    "license": Object,
*    "password": String,
*    "address": Object,
*    "capacity": Number,
*    "locations": {
*      "coordinates": [
*        longitude,
*        latitude
*      ]
*    }
* }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Van created successfully and add to Auth0!
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Van created successfully and add to Auth0!",
*   "data": {
*     "isAssociated": Boolean,
*     "status": String,
*     "_id": String,
*     "auth0": String,
*     "locations": {
*       "_id": String,
*       "coordinates": [
*         longitude,
*         latitude
*       ],
*       "type": String
*     },
*     "capacity": Number,
*     "address": Object,
*     "license": Object,
*     "email": String,
*     "name": String,
*     "updatedAt": Date,
*     "createdAt": Date,
*     "__v": 0
*   }
* }
*/

export const createOne = (req, res, next) => {
  try {
    //@todo check email and password is exists
    logger.info('request for create a van');
    logger.debug(req.body);

    if (_.has(req.body, 'user_metadata')) {
      req.body.user_metadata.roles = ['vanOperator'];
    } else {
      req.body.user_metadata = { roles: ['vanOperator'] };
    }

    const authToken = jwt.sign({
      "aud": config.get('AUTH0.API_KEY'),
      "scopes": {
        "users": {
          "actions": ["create"]
        }
      }
    }, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
    const axiosConfig = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authToken
      }
    };
    const url = config.get('AUTH0.BASE_URL') + 'api/v2/users';
    const authData = {
      'connection': config.get('AUTH0.CONNECTION'),
      'name': req.body.name,
      'email': req.body.email,
      'password': req.body.password,
      'user_metadata': req.body.user_metadata
    };
    //authData.user_metadata.roles = ['Van'];

    axios.post(url, authData, axiosConfig).then((response) => {
      let userInfo = Object.assign({}, req.body, { auth0: response.data.user_id });
      delete userInfo.user_metadata;
      delete userInfo.password;
      const newUser = new Van(userInfo);
      return newUser.save();
    }).then((response) => {
      return res.status(201).json({
        success: true,
        message: 'Van created successfully and add to Auth0!',
        data: response._doc
      });
    }).catch((error) => {
      console.error('Can\'t Add User to Auth0...', error);
      const err = new Error(error.response.data.message);
      err.status = error.response.status;
      return next(err);
    });

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createOne van',
      error: e
    });
  }
};

/**
* @api {GET} /api/vans/:_id Read a Van
* @apiVersion 0.0.0
* @apiName ReadVan
* @apiGroup Van
* @apiPermission all
* @apiParam {String} _id User unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a van
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Read a Van",
*   "data": {
*     "_id": "String",
*     "createdAt": Date,
*     "updatedAt": Date,
*     "name": String,
*     "email": String,
*     "license": String,
*     "address": String,
*     "capacity": Number,
*     "auth0": String,
*     "__v": Number,
*     "status": String,
*     "isAssociated": Boolean
*   }
* }
*/

export const readOne = (req, res, next) => {
  logger.info('request for read a user');

  try {
    let query = {_id: req.params._id, status: 'Active'};
    Van.findOne(query).exec()
    .then((user) => {
      logger.info('van user: ', user);
      if (!user) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      return res.status(200).json({
        success: true,
        message: 'Read a Van',
        data: user
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readOne van',
      error: e
    });
  }
};

/**
* @api {PUT} /api/vans/:_id Update a Van
* @apiVersion 0.0.0
* @apiName UpdateVan
* @apiGroup Van
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager, vanOperator
* @apiParam {String} _id User unique ID
* @apiParam {Object} data see Request-Example
* @apiParamExample {Object} Request-Example:
* {
*     "address": Object,
*     "locations": {
*       "coordinates": [
*         longitude,
*         latitude
*       ],
*       "type": String
*     },
*     "name": String,
*     "capacity": Number,
*     "license": Object,
*     "password": String
* }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update van operator
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Update van operator",
*   "data": {
*     "_id": String,
*     "createdAt": Date,
*     "updatedAt": Date,
*     "name": String,
*     "email": String,
*     "license": Object,
*     "address": Object,
*     "capacity": Number,
*     "locations": {
*       "coordinates": [
*         longitude,
*         latitude
*       ],
*       "type": String
*     },
*     "auth0": String,
*     "__v": Number,
*     "status": String,
*     "isAssociated": Boolean
*   }
* }
*/

export const updateOne = (req, res, next) => {
  logger.info('request for update a van');
  try {
    let reqUser = req.decoded;
    let query = { _id: req.params._id };

    if (_.includes(reqUser.roles, 'vanOperator')) {
      Van.findOne(query).exec((err, data) => {
        if (err) return next(err);
        logger.info('PATCH van operator ', req.body);

        data = Object.assign(data, req.body);
        logger.info(data.auth0,'------------------',reqUser.sub)
        if (data.auth0 === reqUser.sub) {
          if (req.body.address || req.body.locations || req.body.name || req.body.phone ||req.body.license || req.body.capacity || req.body.password) {
            data = Object.assign(data, req.body);
            data.save((err, user) => {
              if (err) return next(err);
              // PubNub
              let channelName = 'vanOperators-administrator';
              let information = 'vanOperator is update';
              let data =  Object.assign({ _id: req.params._id }, req.body)
              pubnubNotification(channelName,information,data)

              return res.json({
                success: true,
                message: 'Update van operator',
                data: user
              });
            });
          } else {
            next(errorPlaceHolder(400,false,'not a proper update request'));
          }
        } else {
          next(errorPlaceHolder(401,false,'Not expected Van Operator'));
        }
      });
    } else if(_.includes(reqUser.roles,'administrator','manager')){
      Van.findOne(query).exec((err, data) => {
        if (err) return next(err);
        logger.info('PATCH van operator ', req.body);

        data = Object.assign(data, req.body);
        if(req.body.location){
          data = Object.assign(data, req.body.location)
        }
        data.save((err, user) => {
          if (err) return next(err);
          return res.json({
            success: true,
            message: 'Update van operator',
            data: user
          });
        });
      });
    } else {
      next(errorPlaceHolder(401,false,'not expected user type'))
    }
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    next(errorPlaceHolder(500,false,'Error in updateOne van'))
  }
};

/**
* @api {DELETE} /api/vans/:_id Delete a Van
* @apiVersion 0.0.0
* @apiName DeleteVan
* @apiGroup Van
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager
* @apiParam {String} _id User unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update van operator
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Deleted a user"
* }
*/

export const deleteOne = (req, res, next) => {
  logger.info('request for delete a user');
  try {
    let query = {_id: req.params._id};

    Van.findOne(query).exec()
    .then((user) => {
      logger.info('user to be DELETED: ', user);
      user.status = 'Archived';
      return user.save();
    })
    .then(() => {
      return res.status(200).json(placeHolder(true, 'Deleted a user'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteOne van',
      error: e
    });
  }
};
