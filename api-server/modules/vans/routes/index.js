import express from 'express';

import { logger } from 'libs/logger.js';
import { getAll, createOne, readOne, updateOne, deleteOne } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers';

let _ = require('lodash');

const vanRoutes = express.Router();

vanRoutes.use(['/vans', '/vans/:_id'],(req, res, next) => {
  logger.info(`a ${req.method} request in vans route.`);
  checkAuthToken(req, res, next);
});

vanRoutes.route('/vans')
  .get(getAll)
  .post(isAuthorizedRoles('administrator', 'manager'), createOne);

// vanRoutes.route('/vans/:geo')
//   .put(isAuthorizedRoles('administrator', 'manager', 'vanOp'),);

vanRoutes.route('/vans/:_id')
    // Read a user (accessed at GET /api/vans/:_id)
    .get(readOne)
    // Update a user (accessed at PUT /api/vans/:_id)
    .put(isAuthorizedRoles('administrator', 'manager', 'vanOperator'), updateOne)
    // Delete a user (accessed at DELETE /api/vans/:_id)
    .delete(isAuthorizedRoles('administrator', 'manager'), deleteOne);

// router.post('/authenticate', authController.authenticateUser);

export default vanRoutes;
