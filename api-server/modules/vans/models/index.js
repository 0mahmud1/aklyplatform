import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a sub schema
const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Point',
  },
  coordinates: {
    type: [Number]
  }
});

const vanSchema = new Schema({
  auth0: {
    type: String
  },
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    unique: true
  },
  license: {
    type: Schema.Types.Mixed,
    required: [true, 'license information required']
  },
  capacity: {
    type: Number,
    required: [true, 'capacity information required']
  },
  address: Schema.Types.Mixed, // van address
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  profile: Schema.Types.Mixed, // van others information
  isAssociated: {
    type: Boolean,
    default: false
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdBy: Schema.Types.Mixed,
  createdAt: Date,
  updatedAt: Date
});

vanSchema.pre('save', function (next) {
  // get the current date
  let currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

const Van = mongoose.model('van', vanSchema);

// make this available in our applications
export default Van;
