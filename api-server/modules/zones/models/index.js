import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// create a sub schema
const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Polygon',
  },
  coordinates: {
    type: [[[Number]]]
  }
});

// create a schema
const zoneSchema = new Schema({
  name : {
    type: String,
    required: true
  },
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  deliveryBoys: {
    type: [Schema.Types.Mixed]
  },
  foodProviders: {
    type: [Schema.Types.Mixed]
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
});

// on every save, add the date
zoneSchema.pre('save', function(next) {
  // get the current date
  let currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const Zone = mongoose.model('Zone', zoneSchema);

// make this available in our applications
export default Zone;
