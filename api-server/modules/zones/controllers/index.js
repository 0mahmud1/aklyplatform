import Zone from '../models/index.js';
import DeliveryBoy from '../../deliveryBoys/models';
import FoodProvider from '../../providers/models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { getPolygonCentroid } from 'libs/utils.js';

/*
*  RESTful CRUD APIs for zones
*/

/**
* @api {GET} /api/zones All active zones
* @apiVersion 0.0.0
* @apiName GetZones
* @apiGroup zone
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active zones
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "get active zones",
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "locations": {
*         "_id": String,
*         "coordinates": [
*             [longitude, latitude],
*         ],
*         "type": String
*       },
*       "status": String,
*       "foodProviders": [Object],
*       "deliveryBoys": [Object]
*     }
*   ]
* }
*/

export const getAll = (req, res, next) => {
  try {
    logger.info('request to get active zones');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param

    //this promise returns number of total object
    let findCountPromise = Zone.find(query).count();

    //this promise returns actual data
    let findDataPromise = Zone.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active zones',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getHubs',
      error: e
    });
  }
};

/**
* @api {POST} /api/zones Create a zone
* @apiVersion 0.0.0
* @apiName CreateZone
* @apiGroup zone
* @apiPermission admin, manager
* @apiHeader {String} Authorization valid access token
* @apiParam {Object} data see Request-Example
* @apiParamExample {json} Request-Example:
* {
*    "name": String,
*    "locations": {
*      "coordinates": [
*          [longitude, latitude]
*      ],
*      "type": "Number"
*    },
*    "status": "Active",
*    "foodProviders": [Object],
*   "deliveryBoys": [Object]
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message create a new zone
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*
* {
*  "success": true,
*  "message": "create a new zone",
*  "data":
*    {
*      "_id": String,
*      "createdAt": Date,
*      "updatedAt": Date,
*      "name": String,
*      "locations": {
*        "_id": String,
*        "coordinates": [
*            [longitude, latitude],
*        ],
*        "type": String
*      },
*      "status": String,
*      "foodProviders": [Object],
*      "deliveryBoys": [Object]
*    }
* }
*/

export const createOne = (req, res, next) => {
  try {
    logger.info('a request in create zone');
    logger.debug(req.body);

    const newDocument = new Zone(req.body);
    newDocument.save().then(zone => {
      try {
        if (req.body.deliveryBoys) {
          req.body.deliveryBoys.forEach(function (dBoy) {
            DeliveryBoy.update({
              _id: dBoy._id,
            },{
              $set: {
                isAssociated: 'true',
                'locations.coordinates': getPolygonCentroid(zone.locations.coordinates[0])
              }
            }).then(vur => logger.info(`dBoy association status changed`));
          });
        }

        if (req.body.foodProviders) {
          req.body.foodProviders.forEach(function (foodProvider) {
            FoodProvider.update({
              _id: foodProvider._id
            }, {
              $set: {isAssociated: 'true'}
            }).then(vur => logger.info(`food provider association status changed`));
          });
        }

        return res.status(200).json({
          success: true,
          message: 'create a new zone',
          data: zone
        })
      } catch (error) {
        logger.error(error);
        next(error);
      }
    }).catch(error=>{
      logger.error(error);
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);

    return res.json({
      success: false,
      message: 'Error in code block',
      error: { message: 'Error in code block', code: 500, reason: e.reason, detail: e }
    });
  }
};

/**
* @api {GET} /api/zones/:_id Read a zone
* @apiVersion 0.0.0
* @apiName ReadZone
* @apiGroup zone
* @apiPermission all
* @apiParam {String} _id Zones unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active zones
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "get active zones",
*   "data": [
*     {
*       "_id": String,
*       "createdAt": Date,
*       "updatedAt": Date,
*       "name": String,
*       "locations": {
*         "_id": String,
*         "coordinates": [
*             [longitude, latitude],
*        ],
*         "type": String
*       },
*       "status": String,
*       "foodProviders": [Object],
*       "deliveryBoys": [Object]
*     }
*   ]
* }
*
*/

export const readOne = (req, res, next) => {
  try {
    Zone.findOne({ _id: req.params._id, status: 'Active' }).exec()
    .then((doc) => {
      //logger.info('my hub: ...', hub);
      console.log(doc);
      if (!doc) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      return res.status(200).json({
        success: true,
        message: 'read a zone',
        data: doc
      });
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in read zone',
      error: e
    });
  }
};

/**
* @api {PUT} /api/zones/:_id Update a zone
* @apiVersion 0.0.0
* @apiName UpdateZone
* @apiGroup zone
* @apiPermission admin, manager
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Zone unique ID
* @apiParamExample {json} Request-Example:
* {
*    "name": String,
*   "locations": {
*      "coordinates": [
*          [longitude, latitude]
*      ],
*      "type": "Number"
*    },
*    "status": "Active",
*    "foodProviders": [Object],
*    "deliveryBoys": [Object]
*  }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a hub
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*    "success": true,
*    "message": "Update a hub",
*    "data":
*      {
*        "_id": String,
*        "createdAt": Date,
*        "updatedAt": Date,
*        "name": String,
*        "locations": {
*          "_id": String,
*          "coordinates": [
*              [longitude, latitude],
*          ],
*          "type": String
*        },
*        "status": String,
*        "foodProviders": [Object],
*        "deliveryBoys": [Object]
*      }
*  }
*
*/

export const updateOne = (req, res, next) => {
  try {
    logger.info('inside api update', req.body);
    // const newHub = new Zone(req.body);
    Zone.findOne({_id: req.params._id}).exec()
    .then((doc) => {
      logger.info('hub to be UPDATED: ', doc);
      if (!doc) {
        throw new Error('no hub found for update');
      }
      doc.name = req.body.name;
      if (req.body.locations && req.body.locations.coordinates) {
        doc.locations.coordinates = req.body.locations.coordinates;
      }
      if (req.body.deliveryBoys) {
        doc.deliveryBoys = req.body.deliveryBoys;
      }
      if (req.body.foodProviders) {
        doc.foodProviders = req.body.foodProviders;
      }
      return doc.save();
    })
    .then((doc) => {
      req.body.deliveryBoys.forEach(function (dBoy) {
        DeliveryBoy.update({
          _id: dBoy._id
        },{
          $set: {
            isAssociated: 'true',
            'locations.coordinates': getPolygonCentroid(doc.locations.coordinates[0])
          }
        }).then(vur => logger.info(`dBoy association status changed`));
      });

      req.body.foodProviders.forEach(function (foodProvider) {
        FoodProvider.update({
          _id: foodProvider._id
        },{
          $set: { isAssociated: 'true' }
        }).then(vur => logger.info(`food provider association status changed`));
      });
      return res.status(200).json(placeHolder(true, 'Update a hub', doc))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateHub',
      error: e
    });
  }
};

/**
* @api {DELETE} /api/zones/:_id Delete a zone
* @apiVersion 0.0.0
* @apiName DeleteZone
* @apiGroup zone
* @apiPermission admin, manager
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Zones unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a zone
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Deleted a zone",
*  }
*/

export const deleteOne = (req, res, next) => {
  try {
    Zone.findOne({_id: req.params._id}).exec()
    .then((doc) => {
      logger.info('zone to be DELETED: ', doc);
      doc.status = 'Archived';
      return doc.save();
    })
    .then((doc) => {
      return res.status(200).json(placeHolder(true, 'Deleted a zone'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    // logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteHub',
      error: e
    });
  }
};
