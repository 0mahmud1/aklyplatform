import express from 'express';

import { logger } from 'libs/logger.js';
import { getAll, createOne, readOne, updateOne, deleteOne } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const zoneRoutes = express.Router();

zoneRoutes.use(['/zones','/zones/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in zones route.`);
    if (req.method === 'GET') {
        next();
    } else {
        checkAuthToken(req, res, next);
    }
});

zoneRoutes.route('/zones')
  // All active hubs (accessed at GET /api/zones)
  .get(getAll)
  // Create a hub (accessed at POST /api/zones)
  //.post(isAuthorizedRoles("admin", "manager"), createHub);
  .post(isAuthorizedRoles("administrator", "manager"), createOne);

zoneRoutes.route('/zones/:_id')
  // Read a hub (accessed at GET /api/zones/:_id)
  .get(readOne)
  // Update a hub (accessed at PUT /api/zones/:_id)
  .put(isAuthorizedRoles("administrator", "manager"), updateOne)
  // Delete a hub (accessed at DELETE /api/zones/:_id)
  .delete(isAuthorizedRoles("administrator", "manager"), deleteOne);

export default zoneRoutes;
