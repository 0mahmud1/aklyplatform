import Tag from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

/*
*  RESTful CRUD APIs for Tag
*/

/**
* @api {GET} /api/tags All active tags
* @apiName GetTags
* @apiGroup Tag
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Get all active tags
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Get all active tags',
*    "data": [
*    {
*     "_id": "String",
*      "createdAt": Date,
*      "updatedAt": Date,
*      "name": "String",
*      "__v": 0,
*      "images": [
*         "link to active image",
*         "link to inactive image"
*      ],
*      "status": "String"
*    }
*  }
*/

export const getTags = (req, res, next) => {
  try {
    logger.info('request to get active tags');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param

    //this promise returns number of total object
    let findCountPromise = Tag.find(query).count();

    //this promise returns actual data
    let findDataPromise = Tag.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active tags',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getTags',
      error: e
    });
  }
};

/**
* @api {POST} /api/tags Create a tag
* @apiName CreateTag
* @apiGroup Tag
* @apiHeader {String} Authorization valid access token
* @apiPermission administrator, manager
* @apiParamExample {JSON} Request-Example:
*  {
*    "name": "String",
*    "images": [
*      "link to active image",
*      "link to inactive image"
*    ],
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a tag
* @apiSuccess (200) {Object} data see  Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 Created
*  {
*    'success': true,
*    'message': 'Create a tag',
*    "data": {
*        "__v": 0,
*        "createdAt": Date,
*        "updatedAt": Date,
*        "name": "String",
*        "_id": "String",
*        "images": [
*          "link to active image",
*          "link to inactive image"
*        ],
*        "status": "Active"
*      }
*  }
*/

export const createTag = (req, res, next) => {
  try {
    const newTag = new Tag(req.body);

    newTag.save()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Create a tag',
        data: response
      })
    })
    .catch((error) => {
      res.status(400).json({
        success: false,
        message: 'Failed to create a tag',
        data: error
      })
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createTag',
      error: e
    });
  }

};

/**
* @api {GET} /api/tags/:_id Read a tag
* @apiName ReadTag
* @apiGroup Tag
* @apiParam {String} _id Tags unique ID
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a tag
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*  "success": true,
*  "message": "Read a tag",
*  "data": {
*    "_id": "String",
*    "createdAt": Date,
*    "updatedAt": Date,
*    "name": String,
*    "__v": 0,
*    "images": [
*      "link to active image",
*      "link to inactive image"
*    ],
*    "status": "Active"
*   }
*  }
*/


export const readTag = (req, res, next) => {
  /*Tag.findById(req.params._id).exec((err, tag) => {
  if (err) return next(err)
  return res.status(200).json({
  success: true,
  message: 'Read a tag',
  data: tag
})
})*/
try {
  Tag.findOne({_id: req.params._id, status: 'Active'}).exec()
  .then((tag) => {
    logger.info('my tag: ...', tag);
    if (!tag) {
      logger.warn('query return null');
      return next(errorPlaceHolder(403, false, 'no data found'));
    }

    return res.status(200).json({
      success: true,
      message: 'Read a tag',
      data: tag
    })
  })
  .catch((error) => {
    next(error);
  });
} catch (e) {
  logger.debug(e.stack);
  logger.error(e);
  res.status(500).json({
    success: false,
    message: 'Error in readTag',
    error: e
  });
}
};

/**
* @api {PUT} /api/tags/:_id Update a tag
* @apiName UpdateTag
* @apiGroup Tag
* @apiPermission administrator, manager
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Tags unique ID
* @apiParamExample {JSON} Request-Example:
*  {
*    'name': "String",
*    "images": [
*      "link to active image",
*      "link to inactive image"
*    ],
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a tag
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Update a tag',
*    "data": {
*        "_id": "String",
*        "createdAt": Date,
*        "updatedAt": Date,
*        "name": "String",
*        "__v": 0,
*        "images": [
*           "link to active image",
*           "link to inactive image"
*        ],
*        "status": "Active"
*      }
*  }
*/

export const updateTag = (req, res, next) => {
  try {
    logger.info('update request in process');

    Tag.findOne({_id: req.params._id}).exec()
    .then((tag) => {
      logger.info('tag to be UPDATED: ', tag);
      if (!tag) {
        throw new Error('no tag found for update');
      }
      Object.assign(tag, tag ,req.body);
      return tag.save();
    })
    .then((updatedTag) => {
      return res.status(200).json(placeHolder(true, 'Update a tag', updatedTag))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateTag',
      error: e
    });
  }
};

/**
* @api {DELETE} /api/tags/:_id Delete a tag
* @apiName DeleteTag
* @apiGroup Tag
* @apiPermission administrator, manager
 * @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Tags unique ID
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Delete a tag
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Delete a tag'
*  }
*/

export const deleteTag = (req, res, next) => {
  try {
    logger.info('delete request in process');

    Tag.findOne({_id: req.params._id}).exec()
    .then((tag) => {
      logger.info('tag to be DELETED: ', tag);
      tag.status = 'Archived';
      return tag.save();
    })
    .then((deletedTag) => {
      return res.status(200).json(placeHolder(true, 'Deleted a tag'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteTag',
      error: e
    });
  }
  // exec((err, tag) => {
  //   if (err) return next(err)
  //
  //   tag.status = 'Archived'
  //
  //   tag.save((err, tag) => {
  //     if (err) return next(err)
  //     return res.status(200).json({
  //       success: true,
  //       message: 'Delete a tag'
  //     })
  //   })
  // })
};
