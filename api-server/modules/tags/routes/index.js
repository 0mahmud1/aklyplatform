import express from 'express'

import { logger } from 'libs/logger.js';
import { getTags, createTag, readTag, updateTag, deleteTag } from '../controllers'
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const tagRoutes = express.Router();

tagRoutes.use(['/tags','/tags/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in tags route.`);

    if (req.method === 'GET') {
        next();
    } else {
        checkAuthToken(req, res, next);
    }
});

tagRoutes.route('/tags')
  // All active tags (accessed at GET /api/tags)
  .get(getTags)
  // Create a tag (accessed at POST /api/tags)
  .post(isAuthorizedRoles('administrator', 'manager'), createTag);

tagRoutes.route('/tags/:_id')
  // Read a tag (accessed at GET /api/tags/:_id)
  .get(readTag)
  // Update a tag (accessed at PUT /api/tags/:_id)
  .put(isAuthorizedRoles('administrator', 'manager'), updateTag)
  // Delete a tag (accessed at DELETE /api/tags/:_id)
  .delete(isAuthorizedRoles('administrator', 'manager'), deleteTag);

export default tagRoutes;
