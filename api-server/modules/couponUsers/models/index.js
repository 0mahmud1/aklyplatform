import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const couponUserSchema = new Schema({
  user: {                                 // who used this coupon / details about him
    type: Schema.Types.Mixed
  },
  coupon: {                                    // which coupon an user used
    type: Schema.Types.Mixed
  },
  count: {                                     // how many times an user used this coupon
    type: Number
  },
  status: {
    type: String,
    enum: ['Active', 'Archived'],
    default: 'Active'
  },
  createdAt: Date,
  updatedAt: Date
});

// on every save, add the date
couponUserSchema.pre('save', function(next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

// the schema is useless so far
// we need to create a model using it
const CouponUser = mongoose.model('CouponUser', couponUserSchema);

// make this available in our applications
export default CouponUser;
