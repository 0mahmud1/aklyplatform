import CouponUser from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {GET} /api/couponUsers/:_id Read a couponUser
* @apiName readCouponUser
* @apiGroup CouponUser
* @apiParam {String} _id CouponUser unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a couponUser
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Read a couponUser",
*      "data": {
*         "__v": 0,
*         "_id": "String",
*         "user": Object,
*         "coupon": Object,
*         "count": Number,
*         "createdAt": Date,
*         "updatedAt": Date,
*      }
*  }
*/
export const readCouponUser = (req, res, next) => {
  logger.info('request in read coupon user route');
  try {
    CouponUser.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((couponUser) => {
      console.log('coupon user: ...', couponUser);
      if (!couponUser) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      return res.status(200).json({
        success: true,
        message: 'Read a couponUser',
        data: couponUser
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readCouponUser',
      error: e
    });
  }
};
