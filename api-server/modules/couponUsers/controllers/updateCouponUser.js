import CouponUser from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {PUT} /api/couponUsers/:_id Update a couponUser
* @apiName updateCouponUser
* @apiGroup CouponUser
* @apiParam {String} _id CouponUser unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a couponUser
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "Update a couponUser",
*      "data": {
*         "__v": 0,
*         "_id": "String",
*         "user": Object,
*         "coupon": Object,
*         "count": Number,
*         "createdAt": Date,
*         "updatedAt": Date,
*      }
*  }
*/
export const updateCouponUser = (req, res, next) => {
  logger.info('request in update coupon user route');
  try {
    CouponUser.findOne({_id: req.params._id , status: 'Active'}).exec()
    .then((couponUser) => {
      console.log('couponUser to be UPDATED: ', couponUser);
      if (!couponUser) {
        throw new Error('no couponUser found for update');
      }
      Object.assign(couponUser, couponUser ,req.body);
      return couponUser.save();
    })
    .then((updatedCouponUser) => {
      return res.status(200).json(placeHolder(true, 'Update a coupon user', updatedCouponUser))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateCouponUser',
      error: e
    });
  }
};
