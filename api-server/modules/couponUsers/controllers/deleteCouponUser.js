import CouponUser from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {DELETE} /api/couponUsers/:_id Delete a couponUser
* @apiName deleteCouponUser
* @apiGroup CouponUser
* @apiParam {String} _id CouponUser unique ID
* @apiPermission All
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Delete a couponUser
* @apiSuccess (200) {Object} data See Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  {
*      "success": true,
*      "message": "delete a couponUser",
*  }
*/
export const deleteCouponUser = (req, res, next) => {
  try {
    logger.info('delete request in coupon user route');

    CouponUser.findOne({_id: req.params._id, status: 'Active'}).exec()
    .then((couponUser) => {
      logger.info('couponUser to be DELETED: ', couponUser);
      couponUser.status = 'Archived';
      return couponUser.save();
    })
    .then((deletedCouponUser) => {
      return res.status(200).json(placeHolder(true, 'Deleted a couponUser'))
    })
    .catch((error) => {
      return next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in deleteCouponUser',
      error: e
    });
  }
};
