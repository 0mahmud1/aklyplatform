import CouponUser from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/*
*  RESTful CRUD APIs for CouponUser
*/

/**
* @api {GET} /api/couponUsers All active couponUsers
* @apiVersion 0.0.0
* @apiName getCouponUsers
* @apiGroup CouponUser
* @apiHeader {String} Authorization valid access token
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active couponUsers
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": Boolean,
*      "message": "get active couponUsers",
*      "data":[
*        {
*          "__v": 0,
*          "_id": "String",
*          "user": Object,
*          "coupon": Object,
*          "count": Number,
*          "createdAt": Date,
*          "updatedAt": Date,
*        }
*      ]
*  }
*/
export const getCouponUsers = (req, res, next) => {
  try {
    logger.info('request to get active coupon users');

    let limit = parseInt(req.query.limit);
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    let query = {status: 'Active'};
    // @todo insert required queries in query param

    //this promise returns number of total object
    let findCountPromise = CouponUser.find(query).count();

    //this promise returns actual data
    let findDataPromise = CouponUser.find(query).limit(limit).skip(skip);

    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active coupon users',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error(error);
        next(error)
      })
  } catch (err) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getCouponUsers',
      error: e
    });
  }
};
