import CouponUser from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import async from 'async';

/**
* @api {POST} /api/couponUsers Create a couponUser
* @apiVersion 0.0.1
* @apiName createCouponUser
* @apiDescription create new couponUser
* @apiGroup couponUser
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Create a couponUser
* @apiSuccess (200) {Object} See Success-Response
* @apiSuccessExample {json} Success-Response:
*{
*   "success": true
*   "message": "Create a couponUser"
*   "data": {
*    "__v": 0,
*    "_id": "String",
*    "user": Object,
*    "coupon": Object,
*    "count": Number,
*    "createdAt": Date,
*    "updatedAt": Date,
*   }-
*}
*/


export const createCouponUser = (req, res, next) => {
  logger.info('request in create coupon user route ',req.body);
  try {
    const newCouponUser = new CouponUser(req.body);
    newCouponUser.save()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Create a CouponUser',
        data: response
      })
    })
    .catch((error) => {
      res.status(400).json({
        success: false,
        message: 'Failed to create a CouponUser',
        data: error
      })
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createCouponUser',
      error: e
    });
  }

};
