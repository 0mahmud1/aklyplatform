import express from 'express';
import { logger } from 'libs/logger.js';

import { getCouponUsers } from '../controllers/getCouponUsers.js';
import { createCouponUser } from '../controllers/createCouponUser.js';
import { readCouponUser } from '../controllers/readCouponUser.js';
import { updateCouponUser } from '../controllers/updateCouponUser.js';
import { deleteCouponUser } from '../controllers/deleteCouponUser.js';

const couponUserRoutes = express.Router();

couponUserRoutes.route('/couponUsers')
  // get all active couponUsers (accessed at GET /api/couponUsers)
  .get(getCouponUsers)
  // create a couponUser (accessed at POST /api/couponUsers)
  .post(createCouponUser)

couponUserRoutes.route('/couponUsers/:_id')
  // Read a couponUser (accessed at GET /api/couponUsers/:_id)
  .get(readCouponUser)
  // Update a couponUser (accessed at PUT /api/couponUsers/:_id)
  .put(updateCouponUser)
  // Delete a couponUser (accessed at DELETE /api/couponUsers/:_id)
  .delete(deleteCouponUser)

export default couponUserRoutes;
