import express from 'express';

import { logger } from 'libs/logger.js';
import { getAll, createOne, readOne, updateOne } from '../controllers';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers';

let _ = require('lodash');

const vanConfirmationRoutes = express.Router();

vanConfirmationRoutes.use(['/vanconfirmations', '/vanconfirmations/:_id'],(req, res, next) => {
    logger.info(`a ${req.method} request in vanconfirmations route.`);
    checkAuthToken(req, res, next);
});

vanConfirmationRoutes.route('/vanconfirmations')
    .get(getAll)
    .post(isAuthorizedRoles('administrator', 'manager','foodProvider'), createOne);

vanConfirmationRoutes.route('/vanconfirmations/:_id')
// Read a user (accessed at GET /api/administrator/:_id)
    .get(readOne)
    // Update a user (accessed at PUT /api/administrator/:_id)
    .put(isAuthorizedRoles('administrator', 'manager','foodProvider'), updateOne);
    // Delete a user (accessed at DELETE /api/administrator/:_id)
    // .delete(isAuthorizedRoles('administrator', 'manager'), deleteOne);

// router.post('/authenticate', authController.authenticateUser);

export default vanConfirmationRoutes;
