import VanConfirmation from '../models';
import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import { generateSequence } from 'modules/deliveries/lib/index.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'

/*
*  RESTful CRUD APIs for VanConfirmation
*/

/**
* @api {GET} /api/vanconfirmations All active vanconfirmations
* @apiName GetVanConfirmations
* @apiGroup VanConfirmation
* @apiHeader {String} Authorization valid access token
* @apiSuccess (200) {Boolean} success
* @apiSuccess (200) {String} message
* @apiSuccess (200) {Object[]} data
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Get all active vanConfirmations',
*    'data': [{
*      'van': Schema.Types.Mixed,
*      'foodProvider': Schema.Types.Mixed,
*      'date': String,
*      'status': Boolean,
*      'createdBy': Schema.Types.Mixed,
*      'createdAt': Date,
*      'updatedAt': Date
*    }]
*  }
*/

export const getAll = (req, res, next) => {
  logger.info('get request in van confirmation');
  try {
    console.log(req.query);
    VanConfirmation.find(req.query)
    .then((vanConfirmations) => {
      return res.status(200).json({
        success: true,
        message: 'get active vanConfirmations',
        data: vanConfirmations
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in getAll VanConfirmation',
      error: e
    });
  }
};

/**
* @api {POST} /api/vanconfirmations Create a vanconfirmation
* @apiName CreateVanConfirmation
* @apiGroup VanConfirmation
* @apiHeader {String} Authorization valid access token
* @apiParamExample {JSON} Request-Example:
*  {
*    'van': Schema.Types.Mixed,
*    'foodProvider': Schema.Types.Mixed,
*    'date': String,
*    'status': Boolean,
*    'createdBy': Schema.Types.Mixed,
*    'createdAt': Date,
*    'updatedAt': Date
*  }
*
* @apiSuccess (201) {Boolean} success
* @apiSuccess (201) {String} message
* @apiSuccess (201) {Object} data
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 201 Created
*  {
*    'success': true,
*    'message': 'Create a vanConfirmation',
*    'data': {
*      'van': Schema.Types.Mixed,
*      'foodProvider': Schema.Types.Mixed,
*      'date': String,
*      'status': Boolean,
*      'createdBy': Schema.Types.Mixed,
*      'createdAt': Date,
*      'updatedAt': Date
*    }
*  }
*/

export const createOne = (req, res, next) => {
  try {
    const newVanConfirmation = new VanConfirmation(req.body);

    newVanConfirmation.save()
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Create a vanConfirmation',
        data: response
      })
    })
    .catch((error) => {
      res.status(400).json({
        success: false,
        message: 'Failed to create a vanConfirmation',
        data: error
      })
    })
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in createOne VanConfirmation',
      error: e
    });
  }
};

/**
* @api {GET} /api/vanconfirmations/:_id Read a vanconfirmation
* @apiName ReadVanConfirmation
* @apiGroup VanConfirmation
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id VanConfirmations unique ID
* @apiSuccess (200) {Boolean} success
* @apiSuccess (200) {String} message
* @apiSuccess (200) {Object} data
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Read a vanConfirmation',
*    'data': {
*      'van': Schema.Types.Mixed,
*      'foodProvider': Schema.Types.Mixed,
*      'date': String,
*      'status': Boolean,
*      'createdBy': Schema.Types.Mixed,
*      'createdAt': Date,
*      'updatedAt': Date
*    }
*  }
*/

export const readOne = (req, res, next) => {
  try {
    VanConfirmation.findOne({_id: req.params._id}).exec()
    .then((vanConfirmation) => {
      logger.info('my vanConfirmation: ...', vanConfirmation);
      if (!vanConfirmation) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }

      return res.status(200).json({
        success: true,
        message: 'Read a vanConfirmation',
        data: vanConfirmation
      })
    })
    .catch((error) => {
      next(error);
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readOne VanConfirmation',
      error: e
    });
  }
};

/**
* @api {PUT} /api/vanconfirmations/:_id Update a vanconfirmation
* @apiName UpdateVanConfirmation
* @apiGroup VanConfirmation
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id VanConfirmations unique ID
* @apiParamExample {JSON} Request-Example:
*  {
*      'van': Schema.Types.Mixed,
*      'foodProvider': Schema.Types.Mixed,
*      'date': String,
*      'status': Boolean,
*      'createdBy': Schema.Types.Mixed,
*      'createdAt': Date,
*      'updatedAt': Date
*  }
*
* @apiSuccess (200) {Boolean} success
* @apiSuccess (200) {String} message
* @apiSuccess (200) {Object} data
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*    'success': true,
*    'message': 'Update a vanConfirmation',
*    'data': {
*      'van': Schema.Types.Mixed,
*      'foodProvider': Schema.Types.Mixed,
*      'date': String,
*      'status': Boolean,
*      'createdBy': Schema.Types.Mixed,
*      'createdAt': Date,
*      'updatedAt': Date
*    }
*  }
*/

export const updateOne = (req, res, next) => {
  logger.info('update request in process');

  try {
    VanConfirmation.findOne({_id: req.params._id}).exec()
    .then((vanConfirmation) => {
      // logger.info('vanConfirmation to be UPDATED: ', vanConfirmation);
      if (!vanConfirmation) {
        throw new Error('no vanConfirmation found for update');
      }

      // vanConfirmation.name = req.body.name;
      Object.assign(vanConfirmation, vanConfirmation, {...req.body});

      vanConfirmation.save()
      .then(data => {
        console.log('vanConfirmation ->', data.van._id);
        let channelName = 'vanConfirmation-' + data.van._id;
        let information = "van confirmation update";
        pubnubNotification(channelName, information, data);

        /*AklyPubNub.publish({
          message: {
            pn_gcm: {
              data: {
                text: "van confirmation update",
                data: data,
              }
            },
            text: 'van confirmation update',
            data: data,
          },
          channel: 'vanConfirmation-' + data.van._id,
          // meta: {
          //   "data": data
          // }   // publish extra meta with the request
        }, function (status, response) {
          if (status.error) {
            // handle error
            console.log(status)
          } else {
            console.log("message Published time-token:", response.timetoken)
          }
        });*/
        /**
         * generate sequence using routific
         */
        generateSequence(data.toObject())
        .then((res)=>{

        });
      });
    })
    .then((updatedVanConfirmation) => {
      return res.status(200).json(placeHolder(true, 'Update a vanConfirmation', updatedVanConfirmation))
    })
    .catch((error) => {
      return next(errorPlaceHolder(403, false, error.message, error));
    });
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in updateOne VanConfirmation',
      error: e
    });
  }
};

