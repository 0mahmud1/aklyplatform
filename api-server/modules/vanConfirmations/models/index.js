import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const vanConfirmationSchema = new Schema({
  van: {
    type: Schema.Types.Mixed,
    required: true
  },
  provider: {
    type: Schema.Types.Mixed,
    required: true
  },
  deliveryDate: {
    type: String,
    required: true
  },
  shift: {
    type: String,
    enum: ['Lunch', 'Dinner'],
  },
  orderCount: {
    type: Number,
    default: 1,
  },
  status: {
    type: Boolean,
    default: false,
  },
  createdBy: Schema.Types.Mixed,
  createdAt: Date,
  updatedAt: Date,
});

vanConfirmationSchema.pre('save', function (next) {
  // get the current date
  const currentDate = new Date();
  // change the updated_at field to current date
  this.updatedAt = currentDate;
  // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});

const VanConfirmation = mongoose.model('vanConfirmation', vanConfirmationSchema);

// make this available in our applications
export default VanConfirmation;
