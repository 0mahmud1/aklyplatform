import express from 'express';

import { logger } from 'libs/logger';
import { authenticateUser, changePassword , changePicture } from './controllers.js';

const authRoutes = express.Router();

authRoutes.use(['/authenticate'],(req, res, next) => {
  logger.info(`a ${req.method} request in authentication route.`);

  if (req.method === 'POST') {
    next();
  } else {
    return res.status(404).json({
      success: false,
      message: 'Wrong request method.',
    });
  }

});

authRoutes.route('/authenticate').post(authenticateUser);
authRoutes.route('/changePassword').post(changePassword);
authRoutes.route('/changePicture/:_id').put(changePicture);
export default authRoutes;
