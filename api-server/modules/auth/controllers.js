// import User from '../models';
import jwt from 'jsonwebtoken';
import config from 'config';
import sha1 from 'sha1';
import _ from 'lodash';
import axios from 'axios';
import formidable from 'formidable';
import sanitizeHtml from 'sanitize-html';
import AWS from 'aws-sdk';
import fs from 'fs';
import path from 'path';
import Chance from 'chance';



// import jwt from 'express-jwt'

import { logger } from 'libs/logger.js';
import Administrator from 'modules/administrators/models/index.js';
import Customer from 'modules/customers/models/index.js';
import DeliveryBoy from 'modules/deliveryBoys/models/index.js';
import FoodProvider from 'modules/providers/models/index.js';
import Van from 'modules/vans/models/index.js';

/**
* @api {POST} /api/authenticate authenticate user from auth0
* @apiVersion 0.0.0
* @apiName AuthenticateUser
* @apiGroup Auth0
* @apiPermission all
* @apiParam {String} email email of valid user
* @apiParam {String} password valid user password
* @apiParamExample {Object} Request-Example:
* {
*    "email": String,
*    "password": String
* }
*
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message Here is auth0 token
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": "Here is auth0 token",
*   "data": {
*     "id_token": String,
*     "access_token": String,
*     "token_type": String
*   }
* }
*/

export function authenticateUser(req, res, next) {
  logger.info('call authenticateUser');
  try {
    const params = req.body;
    let auth0 = {
      'client_id': config.get('AUTH0.CLIENT_ID'),
      'username': params.email,
      'password': params.password,
      'connection': config.get('AUTH0.CONNECTION'),
      'grant_type': 'password',
      'scope': 'openid name email roles'
    };
    axios.post(config.get('AUTH0.BASE_URL') + 'oauth/ro', auth0)
    .then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Here is auth0 token',
        data: response.data
      })
    }).catch((error) => {
      console.error('authenticateUser...(err): ', error.response.data);
      return next({
        status: error.response.status,
        message: error.response.data.error_description,
        error: error.response.data
      })
    });
  } catch (e) {
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in authenticateUser',
      error: e
    });
  }
}

export function checkAuthToken(req, res, next) {
  logger.info('call checkAuthToken');
  try {
    // check header for auth0 token
    let token = req.headers['authorization'];
    // decode token
    if (token) {
      // verifies secret and checks exp
      const userToken = token.replace('Bearer ', '');
      const secretKey = new Buffer(config.get('AUTH0.CLIENT_SECRET'));

      logger.info(userToken);
      logger.info(config.get('AUTH0.CLIENT_SECRET'));
      jwt.verify(userToken, secretKey, function (err, decoded) {
        if (err) {
          logger.error('Failed to authenticate token.');
          logger.error(err);
          return res.status(401).json({
            success: false,
            message: 'Failed to authenticate token.',
            error: err
          });
        } else {
          logger.info('token decoded: ', decoded);
          req.decoded = decoded;
          if (_.has(decoded, 'roles')) {

            switch (decoded.roles[0]) {
              case 'admin':
                Administrator.findOne({ auth0: decoded.sub }).exec()
                .then(data => {
                  if (data) {
                    req.user = data;
                    req.user.roles = decoded.roles;
                    next();
                  } else {
                    return res.status(404).json({
                      success: false,
                      message: 'User not found in system',
                      error: err
                    });
                  }
                });
                break;
              case 'foodProvider':
                FoodProvider.findOne({ auth0: decoded.sub }).exec()
                .then(data => {
                  if (data) {
                    req.user = data;
                    req.user.roles = decoded.roles;
                    next();
                  } else {
                    return res.status(404).json({
                      success: false,
                      message: 'User not found in system',
                      error: err
                    });
                  }
                });
                break;
              case 'vanOperator':
                Van.findOne({ auth0: decoded.sub }).exec()
                .then(data => {
                  if (data) {
                    req.user = data;
                    req.user.roles = decoded.roles;
                    next();
                  } else {
                    return res.status(404).json({
                      success: false,
                      message: 'User not found in system',
                      error: err
                    });
                  }
                });
                break;
              case 'deliveryBoy':
                DeliveryBoy.findOne({ auth0: decoded.sub }).exec()
                .then(data => {
                  if (data) {
                    req.user = data;
                    req.user.roles = decoded.roles;
                    next();
                  } else {
                    return res.status(404).json({
                      success: false,
                      message: 'User not found in system',
                      error: err
                    });
                  }
                });
                break;
              default:
                Customer.findOne({ auth0: decoded.sub }).exec()
                .then(data => {
                  if (data) {
                    req.user = data;
                    req.user.roles = decoded.roles;
                    next();
                  } else {
                    next();
                    // return res.status(404).json({
                    //   success: false,
                    //   message: 'User not found in system',
                    //   error: err
                    // });
                  }
                });
            }

          } else { // remove future when roles assign for customer
            Customer.findOne({ auth0: decoded.sub }).exec()
              .then(data => {
                if (data) {
                  req.user = data;
                  req.user.roles = ['customer'];
                  next();
                } else {
                  // return res.status(404).json({
                  //   success: false,
                  //   message: 'User not found in system',
                  //   error: err
                  // });
                  next();
                }
              })
          }
        }
      });
    } else {
      // if there is no token
      // return an error
      logger.error('no token provided.');
      return res.status(403).json({
        success: false,
        message: 'No token provided.',
      });
    }
  } catch (e) {
    logger.debug(e.stack);
    logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in checkAuthToken',
      error: e
    });
  }
}

/**
 * checked authorized role(s)
 * @param allowed
 * @returns {function(*, *, *)}
 */
export const isAuthorizedRoles = (...allowed) => {
  return (req, res, next) => {
    try {
      if (req.decoded) {
        try {
          logger.info('check isAuthorizedRoles');
          logger.debug(allowed);
          logger.debug(req.decoded);
          if (_.intersection(allowed, req.decoded.roles instanceof Array ? req.decoded.roles : [req.decoded.roles]).length > 0) {
            logger.info('user authorize for this request');
            next();
          } else {
            logger.info('Unauthorized user access');
            return res.status(401).send({
              success: false,
              message: 'Unauthorized user access'
            });
          }
        } catch (e) {
          return res.status(500).send({
            success: false,
            message: 'Error in check user authorization',
            error: e,
          });
        }
      } else {
        return res.status(405).send({
          success: false,
          message: 'token object not found'
        });
      }
    } catch (e) {
      logger.debug(e.stack);
      logger.error(e);
      res.status(500).json({
        success: false,
        message: 'Error in isAuthorizedRoles',
        error: e
      });
    }

  }
};


/**
* @api {POST} /api/changePassword change user password
* @apiVersion 0.0.0
* @apiName ChangePassword
* @apiGroup Auth0
* @apiPermission all
* @apiHeader {String} Authorization valid access token
* @apiParam {String} email email of valid user
* @apiParam {String} password valid user password
* @apiParam {String} newPassword new password to be changed
* @apiParamExample {Object} Request-Example:
* {
*    "email": String,
*    "password": String,
*    "newPassword": String
* }
*
* @apiSuccess (200) {Boolean} success return true if succeed, otherwise return false
* @apiSuccess (200) {String} message password updated successfully
* @apiSuccess (200) {Object} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
* {
*   "success": true,
*   "message": String,
*   "data": {
*     "name": String,
*     "email": String,
*     "email_verified": Boolean,
*     "user_id": String,
*     "picture": String,
*     "nickname": String,
*     "identities": [
*       {
*         "user_id": String,
*         "provider": String,
*         "connection": String,
*         "isSocial": Boolean
*       }
*     ],
*     "updated_at": Date,
*     "created_at": Date,
*     "last_password_reset": Date,
*     "app_metadata": {
*       "roles": [
*         String
*       ]
*     },
*     "last_ip": String,
*     "last_login": Date,
*     "logins_count": Number
*   }
* }
*/
export function changePassword(req, res, next){
    try {
        const params = req.body;
        let auth0 = {
            'client_id': config.get('AUTH0.CLIENT_ID'),
            'username': params.email,
            'password': params.password,
            'connection': config.get('AUTH0.CONNECTION'),
            'grant_type': 'password',
            'scope': 'openid name email roles'
        };
        axios.post(config.get('AUTH0.BASE_URL') + 'oauth/ro', auth0)
            .then((response) => {
                try {
                    let decoded = jwt.decode(response.data.id_token);
                    let user_id = decoded.sub;
                    //crete token
                    const authToken = jwt.sign({
                        "aud": config.get('AUTH0.API_KEY'),
                        "scopes": {
                            "users": {
                                "actions": ["read","update"]
                            }
                        }
                    }, new Buffer(config.get('AUTH0.GLOBAL_CLIENT_SECRET'), 'base64'));
                    //token created
                    const axiosConfig = {
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + authToken
                        }
                    };
                    const url = config.get('AUTH0.BASE_URL') + 'api/v2/users/'+user_id;
                    let authData = {
                        "connection": config.get('AUTH0.CONNECTION'),
                        "password": params.newPassword,
                    };
                    axios.patch(url,authData,axiosConfig)
                        .then((response) => {
                            "use strict";
                            return res.status(200).json({
                                success: true,
                                message: 'password updated successfully',
                                data: response.data
                            })
                        })
                        .catch((error) => {
                            console.error('Can\'t update password for User in Auth0...', error);
                            const err = new Error(error.response.data.message);
                            err.status = error.response.status;
                            return next(err);
                        });
                } catch (e){
                    console.error(e);
                }

            }).catch((error) => {
            console.error('authenticateUser...(err): ', error.response.data);
            return next({
                status: error.response.status,
                message: "no change",
                error: error.response.data
            })
        });
    } catch (error) {
        console.error('authenticateUser...(err): ', error);
        return next({
            status: error.response.status,
            message: "no change",
            error: error.response.data
        })
    }
}


export function changePicture(req, res, next){
  console.log('changePicture..server');
  try {
    console.log('Inside Try changePicture');
    AWS.config.update({
    	accessKeyId: config.get('AWS.accessKeyId'),
    	secretAccessKey: config.get('AWS.secretAccessKey'),
    });

    const form = new formidable.IncomingForm();
    form.maxFieldsSize = 25097152;
    form.parse(req, function(err, fields, files) {
      console.log('Files =>>>',files);
      if (!_.isEmpty(files)) {
        const file = files.file;
        fs.readFile(file.path, function (err, data) {
          if (err) throw err; // Something went wrong!

          const s3bucket = new AWS.S3({ params: { Bucket: config.get('AWS.bucket'),ACL: 'public-read' } });
          s3bucket.createBucket(function () {
            const params = {
              Key: 'users/' + new Chance().guid() + path.extname(file.name),
              Body: data
            };
            s3bucket.upload(params, function (err, data) {
              // Whether there is an error or not, delete the temp file
              fs.unlink(file.path, function (err) {
                if (err) {
                    console.error(err);
                }
                console.log('Temp File Delete');
              });
              console.log("PRINT FILE:", file);
              if (err) {
                console.error('ERROR MSG: ', err.message, err);
                return res.json({
                  success: false,
                  message: err.message
                });
              } else {
                console.log('Successfully uploaded data', data);
                let newUser;
                let query = { _id: req.params._id };
                if(fields.type === 'Customer') {
                  Customer.findOne(query).exec((err, newUser) => {
                    if (err) return next(err);
                    saveUserPicture(newUser, file, data, res);
                  });
                }
              }
            });
          });
        });
      } else {
        console.log('File Not Found');
        return res.status(404).json({
          success: false,
          message: 'No Image Found',
          error: err
        });
      }
   });

  } catch(err) {
    console.log('err',err);
    return res.status(404).json({
      success: false,
      message: 'Unable to update a user.',
      error: err
    });
  }

}

function saveUserPicture(newUser,file,data,res) {
  newUser.avatar = {
    name: file.name,
    key: data.Key,
    location: data.Location
  };
  newUser.save((err, user) => {
    if (err) {
      console.log('err',err);
      return res.status(404).json({
        success: false,
        message: 'Unable to update a user.',
        error: err
      });
    }
    console.log('Successfully ', user);
    return res.status(200).json({
      success: true,
      message: 'User is updated successfully!',
      user: user
    });
  });
}
