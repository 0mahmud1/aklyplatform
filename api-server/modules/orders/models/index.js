import mongoose, { Schema } from 'mongoose';
import shortid from 'shortid';
import { AklyPubNub } from 'libs/pubnub.js';
import { logger } from 'libs/logger.js';
import { pubnubNotification } from '../../../libs/pubnub_notifications.js'


const geoLocationSchema = new Schema({
  type: {
    type: String,
    default: 'Point',
  },
  coordinates: {
    type: [Number]
  }
});

// create a schema
export const orderSchema = new Schema({
  orderDate: {
    type: Date,
    // required: [true, 'Date is required.']
  },
  orderRefId: {
    type: String
  },
  coupon: {
    type: Schema.Types.Mixed
  },
  discount: {
    type: Schema.Types.Mixed
  },
  subTotal: {
    type: Number
  },
  orderType: {
    type: String,
    enum: ['onDemand','subscription'],
    required: [true, 'either of order type must be set']
  },
  planType: {
    type: String,
    enum: ['lunch','fullDay'],
    default: 'fullDay'
  },
  deliveries: {
    type: [Schema.Types.Mixed],
    required: [true, 'Foods item is required.']
  },
  provider: {
    type: Schema.Types.Mixed,
    // required: [true, 'Provider is required.']
  },
  zone: {
    type: Schema.Types.Mixed,
  },
  address: {
    type: Schema.Types.Mixed,
    required: [true, 'Address is required.']
  },
  locations: {
    type: geoLocationSchema,
    index: '2dsphere'
  },
  total: {
    type: Number,
    // required: [true, 'Price is required.']
  },
  load: {
    type: Number,
  },
  paymentType: {
    type: String,
    enum: ['cash', 'card'],
    // required: [true, 'Payment Type is required.']
  },
  deliveryNote: {
    type: String
  },
  status: {
    type: String,
    enum: ['pending', 'cooking', 'ready', 'onTheWay', 'accepted', 'denied', 'completed', 'cancel','Archived'],
    default: 'pending'
  },
  createdBy: {
    type: Schema.Types.Mixed,
    required: [true, 'Created by is required.']
  },
  mealPlan: {
    type: String,
  },
  mealPlanDetails: {
    type: Schema.Types.Mixed
  },
  rates: {
    type: [Schema.Types.Mixed]
  },
  firstDeliveryDate: {
    type: Date,
  },
  lastDeliveryDate: {
      type: Date,
  },
  createdAt: Date,
  updatedAt: Date,
}, { versionKey: false });

// on every save, add the date
orderSchema.pre('save', function(next) {
  // get the current date
  let currentDate = new Date();
    // change the updated_at field to current date
  this.updatedAt = currentDate;
    // if created_at doesn't exist, add to that field
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  if (this.planType === "") {
    this.planType = "lunch"
  }
  if (!this.orderRefId) {
    this.orderRefId = `akly-${shortid.generate()}`;
  }
  next();
});

orderSchema.post('save', function (result) {
  console.log('------- update order ----- ');
  console.log(result);
  console.log('------- end update order ----- ');
  let channelName = 'orders-admin';
  let information = 'New Order Inserted';
  pubnubNotification(channelName,information);

});


// the schema is useless so far
// we need to create a model using it
const Order = mongoose.model('Order', orderSchema);

// make this available in our applications
export default Order;
