import async from 'async';

import Order from '../models';
import { logger } from 'libs/logger.js';
import _ from 'lodash';



/**
 * @apiIgnore Do not use this api
* @api {PUT} /api/orders/:_id Update an order
* @apiName UpdateOrder
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiPermission Administrator, Manager, Customer, FoodProvider, DeliveryBoy
* @apiParam {String} _id Order unique ID
* @apiParam {Object} data see Request-Example
*
*  @apiParamExample {json} Request-Example:
*  {
*     "orderRefId": String,
*      "orderDate": Date,
*      "subTotal": Number,
*      "address": Object,
*      "locations": [longitude,latitude],
*      "total": Number,
*      "orderType": String,
*      "paymentType": String,
*      "status": String,
*      "deliveryNote": String,
*      "planType": String,
*      "deliveries":[
*        {
*        "total": Number,
*        "deliveryDate": Date,
*        "meals": [
*            {
*              "quantity": Number
*              "item": {
*                "_id": String,
*                "createdAt": Date,
*                "updatedAt": Date,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": String,
*                "status": String,
*                "images": [
*                  String
*                ],
*                "tags": [
*                  {
*                    "status": String,
*                    "images": [
*                      String
*                    ],
*                    "name": String,
*                    "_id": String
*                  },
*                ]
*              },
*            }
*          ]
*        }
*     ]
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Update a order
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": Boolean,
*      "message": "Update a order",
*      "data": {
*          "_id":String,
*          "orderRefId": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "load": Number,
*          "zone": Object,
*          "provider": Object,
*          "createdBy": Object,
*          "orderDate": Date,
*          "subTotal": Number,
*          "address": Object,
*          "locations": Object,
*          "total": Number,
*          "orderType": String,
*          "paymentType": String,
*          "status": String,
*          "deliveryNote": String,
*          "planType": String,
*          "deliveries":[
*            "meals": [
*                {
*                  "item": {
*                    "_id": String,
*                    "createdAt": Date,
*                    "updatedAt": Date,
*                    "name": String,
*                    "price": Number,
*                    "recipe": String,
*                    "ingredients": String,
*                    "calorie": String,
*                    "protein": String,
*                    "fat": String,
*                    "carbs": String,
*                    "description": String,
*                    "youtubeUrl": String,
*                    "status": String,
*                    "load": Number,
*                    "images": [
*                      String
*                    ],
*                    "provider": [],
*                    "tags": [
*                      {
*                        "status": String,
*                        "images": [
*                          String
*                        ],
*                        "name": String,
*                        "updatedAt": Date,
*                        "createdAt": Date,
*                        "_id": String
*                      },
*                    ]
*                  },
*                  "quantity": Number
*                }
*              ],
*              "total": Number,
*              "load": Number,
*              "deliveryDate": Date
*            }
*          ]
*        }
*  }
*/

export const updateOrder = (req, res, next) => {
  logger.info('-------function updateOrder --------');
  logger.info('function caller email: ', req.decoded.email);
    try {
    Order.findOne({_id: req.params._id}).exec()
    .then((order) => {
      logger.info('order to be UPDATED: ', order.orderRefId);
      if (!order) {
        logger.error('no order found for update');
        logger.error('error created by: ',req.decoded.email)
        throw new Error('no order found for update');
      }
      Object.assign(order, order, req.body);
      return order.save();
    })
    .then((updatedOrder) => {
      return res.status(200).json(placeHolder(true, 'Update a order', updatedOrder))
    })
    .catch((error) => {
      logger.error('error created by: ',req.decoded.email)
      logger.error(error)
      return next(errorPlaceHolder(403, false, error.message, error));
    });
  } catch (err) {
      logger.debug(err.stack);
      logger.error('error created by: ',req.decoded.email)
      logger.error(e);
      res.status(500).json({
        success: false,
        message: 'Error in updateOrder',
        error: err
      });
  }
};
