import async from 'async';

import Order from '../models';
import { logger } from 'libs/logger.js';
import _ from 'lodash';

/*
*  RESTful CRUD APIs for Order
*/

/**
* @api {GET} /api/orders All active orders
* @apiVersion 0.0.0
* @apiName GetOrders
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message get active orders
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": Boolean,
*      "message": "get active orders",
*      "data":[
*        {
*          "_id":String,
*          "orderRefId": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "load": Number,
*          "zone": Object,
*          "provider": Object,
*          "createdBy": Object,
*          "orderDate": Date,
*          "subTotal": Number,
*          "address": Object,
*          "locations": Object,
*          "total": Number,
*          "orderType": String,
*          "paymentType": String,
*          "status": String,
*          "deliveryNote": String,
*          "planType": String,
*          "deliveries":[
*            {
*              "meals": [
*                {
*                  "item": {
*                    "_id": String,
*                    "createdAt": Date,
*                    "updatedAt": Date,
*                    "name": String,
*                    "price": Number,
*                    "recipe": String,
*                    "ingredients": String,
*                    "calorie": String,
*                    "protein": String,
*                    "fat": String,
*                    "carbs": String,
*                    "description": String,
*                    "youtubeUrl": String,
*                    "status": String,
*                    "load": Number,
*                    "images": [
*                      String
*                    ],
*                    "provider": [],
*                    "tags": [
*                      {
*                        "status": String,
*                        "images": [
*                          String
*                        ],
*                        "name": String,
*                        "updatedAt": Date,
*                        "createdAt": Date,
*                        "_id": String
*                      },
*                    ]
*                  },
*                  "quantity": Number
*                }
*              ],
*              "total": Number,
*              "load": Number,
*              "deliveryDate": Date
*            }
*          ]
*        }
*      ]
*  }
*/
export const getOrders = (req, res, next) => {
  try {
    logger.info('request to get active orders');
    logger.info('requested by : ',req.decoded.email);

    let query = {} , limit = 0;
    if(req.decoded.roles[0] == 'administrator') {         // if admin request for ordersS
      query = {};
    }
    if(req.decoded.roles[0] == 'customer') {              // if customer request for orders
      query['createdBy.email'] = req.decoded.email;
    }
    if(req.decoded.roles[0] == 'foodProvider') {          // if food provider request for orders
      query['provider.email'] = req.decoded.email;
    }
    if(req.query.limit) {                    // if order limit is defined
      limit = parseInt(req.query.limit);
    }
    if (limit < 0) {
      limit = 0;
    }
    let skip = parseInt(req.query.page ? (limit * (req.query.page - 1)) : 0); //if there is no skip set skip to 0
    let page = req.query.page ? req.query.page : 1; //if there is no page number set page to 1

    // @todo insert required queries in query param

    if (req.query['orderType']) {       // fetch orders by orderType
      query['orderType'] = req.query['orderType']
    }
    if(req.query['orderRefId']) {       // fetch orders by order reference id
      query['orderRefId'] = RegExp(req.query['orderRefId'],'i');
    }
    if(req.query['email']) {            // fetch orders by customer email
      query['createdBy.email'] = req.query['email'];
    }
    if(req.query['customerEmail']) {          // search orders by customer email , in order table
      let customerEmail = req.query['customerEmail'];
      let str = RegExp.escape(customerEmail);
      query['createdBy.email'] = RegExp(str,'i');
    }
    if(req.query['foodProviderEmail']) {          // search orders by food provider email , in order table
      let foodProviderEmail = req.query['foodProviderEmail'];
      let str = RegExp.escape(foodProviderEmail);
      query['provider.email'] = RegExp(str,'i');
    }


    //this promise returns number of total object
    let findCountPromise = Order.find(query).count();
    console.log('api order query=>',query)
    //this promise returns actual data
    let findDataPromise;
    if (req.query['sorted']) {
      findDataPromise = Order.find(query).limit(limit).skip(skip).sort({createdAt: -1})
    } else {
      findDataPromise = Order.find(query).limit(limit).skip(skip).sort({createdAt: -1});
    }


    Promise.all([findCountPromise , findDataPromise])
      .then(response => {
        console.log('Success in get orders request');
        return res.status(200).send({
          total: response[0], //return value of findCountPromise
          limit: limit ? limit : 0, //if there is no limit set limit to 0
          skip: skip,
          page: parseInt(page),
          success: true,
          message: 'get active orders',
          data: response[1] //return value of findDataPromise
        })
      })
      .catch((error) => {
        logger.debug(error.stack);
        logger.error('error created by: ',req.decoded.email)
        logger.error(error);
        next(error)
      })
  } catch (err) {
    logger.debug(err.stack);
    logger.error('error created by: ',req.decoded.email)
    logger.error(err);
    res.status(500).json({
      success: false,
      message: 'Error in getOrders',
      error: err
    });
  }
};
