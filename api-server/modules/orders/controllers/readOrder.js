import async from 'async';

import Order from '../models';
import { logger } from 'libs/logger.js';
import _ from 'lodash';

/**
* @api {GET} /api/orders/:_id Read an order
* @apiName ReadOrder
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Order unique ID
* @apiPermission all
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Read a order
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": Boolean,
*      "message": "Read a order",
*      "data": {
*          "_id":String,
*          "orderRefId": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "load": Number,
*          "zone": Object,
*          "provider": Object,
*          "createdBy": Object,
*          "orderDate": Date,
*          "subTotal": Number,
*          "address": Object,
*          "locations": Object,
*          "total": Number,
*          "orderType": String,
*          "paymentType": String,
*          "status": String,
*          "deliveryNote": String,
*          "planType": String,
*          "deliveries":[
*            "meals": [
*                {
*                  "item": {
*                    "_id": String,
*                    "createdAt": Date,
*                    "updatedAt": Date,
*                    "name": String,
*                    "price": Number,
*                    "recipe": String,
*                    "ingredients": String,
*                    "calorie": String,
*                    "protein": String,
*                    "fat": String,
*                    "carbs": String,
*                    "description": String,
*                    "youtubeUrl": String,
*                    "status": String,
*                    "load": Number,
*                    "images": [
*                      String
*                    ],
*                    "provider": [],
*                    "tags": [
*                      {
*                        "status": String,
*                        "images": [
*                          String
*                        ],
*                        "name": String,
*                        "updatedAt": Date,
*                        "createdAt": Date,
*                        "_id": String
*                      },
*                    ]
*                  },
*                  "quantity": Number
*                }
*              ],
*              "total": Number,
*              "load": Number,
*              "deliveryDate": Date
*            }
*          ]
*        }
*  }
*/

export const readOrder = (req, res, next) => {
  try {
    logger.info('----------called readOrder-------------')
    logger.info('function caller email: ', req.decoded.email);
    Order.findOne({_id: req.params._id}).exec()
    .then((order) => {
      if (!order) {
        logger.warn('query return null');
        return next(errorPlaceHolder(403, false, 'no data found'));
      }
      console.log('read order successfull');
      return res.status(200).json({
        success: true,
        message: 'Read a order',
        data: order
      })
    })
    .catch((error) => {
        logger.error('---------error in readOrder----------')
        logger.error('error created by: ',req.decoded.email)
        logger.error(error)
        next(error);
    });

  } catch (err) {
    logger.error('---------error in readOrder----------')
      logger.error('error created by: ',req.decoded.email)
      logger.debug(err.stack);
      logger.error(e);
    res.status(500).json({
      success: false,
      message: 'Error in readOrders',
      error: e
    });
  }
};
