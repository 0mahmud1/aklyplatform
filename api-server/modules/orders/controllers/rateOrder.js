import async from 'async';

import Order from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';

/**
* @api {PUT} /api/orders/rates/:_id RATE an order
* @apiName RateOrder
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Order unique ID
* @apiPermission customer
* @apiParamExample {json} Request-Example(onDemand):
* {
*	“rates”: {
*		“foodQuality”: number,    // 1 to 5, depending on how customer likes it
*		“deliveryTime”: number  //1 to 5, depending on how fast food is delivered
*    }
* }
* @apiParamExample {json} Request-Example(onDemand):
* {
*	“rates”: {
*		“foodQuality”: number,    // 1 to 5, depending on how customer likes it
*		“deliveryTime”: number  //1 to 5, depending on how fast food is delivered
*    }
* }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message rated an order
* @apiSuccessExample {JSON} Success-Response(onDEmand):
*  HTTP/1.1 200 OK
*  {
*    "success": true,
*    "message": "Rated an order",
*    "data": [
*        {
*            "foodQuality": number,
*            "deliveryTime": number
*        }
*           ]
* }
*
* @apiSuccessExample {JSON} Success-Response(Subscription):
*  HTTP/1.1 200 OK
*  {
*   "success": true,
*   "message": "Rated an order",
*   "data": [
*       {
*           "foodQuality": number,
*           "mealVariety": number,
*           "week": number
*       },
*          ]
* }
*/

export const rateOrder = (req, res, next) => {
    logger.info('-------function rateOrder --------');
    logger.info('function caller email: ', req.decoded.email);
    try {
    Order.findOne({_id: req.params._id, "createdBy.email":req.decoded.email}).exec() //"createdBy.email":req.decoded.email
    .then((order) => {
      if(!order) {                              // if no order found
        logger.error('error created by: ',req.decoded.email)
        res.status(500).json({
          success: false,
          message: 'No Order Found',
        });
      } else {
        if(order.orderType === 'onDemand') {      // if order type is on demand
          if(req.body.rates) {                    // if body has rates
            let rates = req.body.rates;           // assign rates in request body to rates
            order.rates = [rates];                // only one element will be in the rates array
            return order.save();                  // save order
          }
        } else if (order.orderType === 'subscription') {      // if order type is subscription
          if(req.body.rates) {                                // if body has rates
            let rates = req.body.rates;                       // assign rates in request body to rates
            let week = rates.week;                            // assign week in request body to week
            let orderRates = order.rates;
            _.remove(orderRates, function(r) {                // remove correspponding week elemnts from order rates array
              return r.week === week;                         // if any data found for the given week
            });
            orderRates.push(rates);                           // push the new rates to the array
            order.rates = orderRates;                         // save rates to the order
            return order.save();
          }
        }
      }

    })
    .then((ratedOrder) => {
      console.log('rated order rates ',ratedOrder.rates);
      return res.status(200).json(placeHolder(true, 'Rated an order',ratedOrder.rates))
    })
    .catch((error) => {
      logger.error('error created by: ',req.decoded.email)
      logger.error(error)
      return next(error);
    });
  } catch (err) {
      logger.debug(e.stack);
      logger.error('error created by: ',req.decoded.email)
      logger.error(e);
      res.status(500).json({
        success: false,
        message: 'Error in rateOrder',
        error: e
      });
  }
};
