import async from 'async';

import Order from '../models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';

/**
* @api {DELETE} /api/orders/:_id DELETE an order
* @apiName DeleteOrder
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiParam {String} _id Order unique ID
* @apiPermission Administrator, Manager
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Deleted a order
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": true,
*      "message": "Deleted a order",
*  }
*/

export const deleteOrder = (req, res, next) => {
    logger.info('-------function deleteOrder --------');
    logger.info('function caller email: ', req.decoded.email);
    try {
    Order.findOne({_id: req.params._id}).exec()
    .then((order) => {
      logger.info('order to be DELETED: ', order.orderRefId);
      order.status = 'Archived';
      return order.save();
    })
    .then((deletedOrder) => {
      return res.status(200).json(placeHolder(true, 'Deleted a order'))
    })
    .catch((error) => {
      logger.error('error created by: ',req.decoded.email)
      logger.error(error)
      return next(error);
    });
  } catch (err) {
      logger.debug(e.stack);
      logger.error('error created by: ',req.decoded.email)
      logger.error(e);
      res.status(500).json({
        success: false,
        message: 'Error in deleteOrder',
        error: e
      });
  }
};
