import async from 'async';
import MealPlan from '../../meal-plans/models';
import Order from '../models';
import Delivery from '../../deliveries/models';
import { logger } from 'libs/logger.js';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';
import _ from 'lodash';
import shortid from 'shortid';
import moment from 'moment';
/**
* @api {PUT} /api/orders/stop/:orderRefId stop an order
* @apiName StopOrder
* @apiGroup Order
* @apiHeader {String} Authorization valid access token
* @apiParam {String} orderRefId Order unique orderRefId
* @apiPermission customer
* @apiParamExample {json} Request-Example(onDemand):
* {
*  "date" : Date  // delivery date - which order should be stopped
* }
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message stopped an order
*/


export const stopOrder = (req, res, next) => {
    logger.info('-------function stopOrder --------');
    logger.info('function caller email: ', req.decoded.email);
    try {
      let countCanceledOrder = checkValidity(req.params.orderRefId); // check number of cancel order
      Promise.all([countCanceledOrder])
        .then(response => {
          if(response[0].planType === 'lunch' && response[0].count >= 10) { // if meal plan type is lunch and number of cancel order >=10
            res.status(500).json({
              success: false,
              message: 'Order can not be canceled more than 10 days for lunch mealplan'
            });
          } else if(response[0].planType === 'fullDay' && response[0].count >= 12) { // // if meal plan type is fullDay and number of cancel order >=12
            res.status(500).json({
              success: false,
              message: 'Order can not be canceled more than 12 days for fullday mealplan'
            });
          } else {
            let query = {};
            if(req.body.date) {
              query['deliveryDate'] = req.body.date;
            }
            query['orderRefId'] = req.params.orderRefId;
            query['createdBy.email'] = req.decoded.email;
            // find all deliveries for the corresponding cancelation date and orderRefId
            Delivery.find(query).exec()
            .then((deliveries) => {
                async.map(deliveries, createDelivery, function (error, result) {
                  logger.info('result',result.length)
                    if(error){
                      logger.error(error);
                    } else {
                      logger.info('results ',result.length);
                      // update delivery status to cancel
                      Delivery.update({orderRefId: req.params.orderRefId , deliveryDate: (req.body.date)}, {$set: { status: 'canceled' }}, {"multi": true} , function(err,result){
                        if(err) {
                          logger.debug(err.stack);
                          logger.error('error created by: ',req.decoded.email)
                          logger.error(err);
                        } else {
                          console.log('update result ',result);
                          // find all deliveries with the order ref id
                          Delivery.find({orderRefId : req.params.orderRefId}).exec()
                          .then((deliveries) => {
                            console.log('newDeliveries ',deliveries.length);
                            return res.status(200).json({
                              success: true,
                              message: 'Order stoped successfully',
                              data: deliveries
                            })
                          })
                          .catch((error) => {
                            res.status(500).json({
                              success: false,
                              message: 'Error in stopOrder',
                              error: error
                            });
                          })
                        }
                      });
                    }
                });

              })
            .catch((error) => {
              logger.debug(error.stack);
              logger.error('error created by: ',req.decoded.email)
              logger.error(error);
              res.status(500).json({
                success: false,
                message: 'Error in stopOrder',
                error: error
              });
            })
          }
        })
        .catch((error) => {
          logger.debug(error.stack);
          logger.error('error created by: ',req.decoded.email)
          logger.error(error);
          next(error)
        })

    } catch (err) {
        logger.debug(err.stack);
        logger.error('error created by: ',req.decoded.email)
        logger.error(err);
        res.status(500).json({
          success: false,
          message: 'Error in stopOrder',
          error: err
        });
    }
};


function createDelivery(newDelivery,callback) {
  console.log('new delivery ',newDelivery._id);
  return new Promise(function(resolve, reject) {
    try {

      Order.findOne({orderRefId: newDelivery.orderRefId}).exec()     // find order by orderRefId
        .then((order) => {
          if(order) {
            const delivery = {};
            let mealPlanType = order.planType;
            let lastDeliveryDate = order.lastDeliveryDate;
            // if lastDeliveryDate is Thursday for lunch meal plan type
            if(moment(lastDeliveryDate).format('dddd') === 'Thursday' && mealPlanType === 'lunch') {
              delivery.deliveryDate = moment(new Date(lastDeliveryDate) ).add(3,'days').format('YYYY-MM-DD');
              // update lastDeliveryDate of order to 3 days later if last delivery day is thursday it will set to sunday
              Order.update({_id:order._id}, {$set: { lastDeliveryDate: delivery.deliveryDate }}, {upsert: true}, function(error){
                if(error) {
                  logger.error(error);
                  logger.debug(error.stack);
                  reject(error);
                }
              })
            }
            // if lastDeliveryDate is Thursday for fullDay meal plan type
            else if(moment(lastDeliveryDate).format('dddd') === 'Thursday' && mealPlanType === 'fullDay') {
              delivery.deliveryDate = moment(new Date(lastDeliveryDate)).add(2,'days').format('YYYY-MM-DD');
              // update lastDeliveryDate of order to 2 days later like if last delivery day is thursday it will set to satday
              Order.update({_id:order._id}, {$set: { lastDeliveryDate: delivery.deliveryDate }}, {upsert: true}, function(error){
                if(error) {
                  logger.error(error);
                  logger.debug(error.stack);
                  reject(error);
                }
              })
            } else {
              delivery.deliveryDate = moment(new Date(lastDeliveryDate)).add(1,'days').format('YYYY-MM-DD');
              // update lastDeliveryDate of order to 1 days later like if last delivery day is monday it will set to tuesday
              Order.update({_id:order._id}, {$set: { lastDeliveryDate: delivery.deliveryDate }}, {upsert: true}, function(error){
                if(error) {
                  logger.error(error);
                  logger.debug(error.stack);
                  reject(error);
                }
              })
            }
            delivery.orderRefId = newDelivery.orderRefId;
            delivery.orderType = newDelivery.orderType;
            delivery.planType = newDelivery.planType;
            delivery.paymentType = newDelivery.paymentType;
            delivery.deliveryNote = newDelivery.deliveryNote;
            delivery.address = newDelivery.address;
            delivery.locations = newDelivery.locations;
            delivery.zone = newDelivery.zone;
            delivery.provider = newDelivery.provider;
            delivery.mealPlan = newDelivery.mealPlan;
            delivery.van = newDelivery.van;
            delivery.deliveryBoy = newDelivery.deliveryBoy;
            delivery.shift = newDelivery.shift;
            delivery.load = newDelivery.load;
            delivery.createdBy = newDelivery.createdBy;
            delivery.deliveryRefId = `akly-D-${shortid.generate()}`;
            delivery.createdBy = newDelivery.createdBy;
            delivery.createdAt = new Date();
            // find meal plan by using meal plan id
            MealPlan.findOne({_id: newDelivery.mealPlan }).exec()
              .then((mealPlan) => {
                if(mealPlan) {
                  let plans = mealPlan.plans;
                  let filteredPlans = _.filter(plans,(plan) => {
                    return plan.week === 3 && plan.day.toLowerCase() === moment(delivery.deliveryDate).format('dddd').toLowerCase() && plan.slot.toLowerCase() === delivery.shift.toLowerCase()
                  });
                  delivery.meals = filteredPlans[0].foods;  // set meals to new delivery meals
                  delivery.defaultMeals = filteredPlans[0].foods;
                  delivery.load = 0;                        // calculate load
                  delivery.meals.map((food) => {
                      delivery.load += parseInt((food.item.load * parseInt(food.quantity)));
                  })
                  delivery.total = _.sumBy(delivery.meals, function (m) {         // calculate total price
                    return Number(m.item.price) * m.quantity;
                  });
                  delivery.subTotal = delivery.total;
                  delivery.quantity = delivery.meals.length;                      // calculate quantity
                  Delivery.create(delivery,(err,newOne) => {                      // create new delivery
                    if(err) {
                      console.log('err',err);
                      callback(err,null);
                      reject(err);
                    }else {
                      console.log('new delivery created ',newOne._id);
                      callback(null,newOne);
                      resolve(newOne);
                    }
                  })
                }
              })
              .catch((error) => {
                logger.error(error);
                logger.debug(error.stack);
                callback(error,null);
                //reject(error);
              })
          }
        })
        .catch((error) => {
          logger.error(error);
          logger.debug(error.stack);
          callback(error,null);
          //reject(error);
        })

    } catch (e) {
      logger.error(e);
      logger.debug(e.stack);
      callback(e,null);
      //reject(e);
    }
  });

}


function checkValidity(orderRefId) {
  return new Promise(function(resolve , reject) {
    try {
      Order.findOne({orderRefId: orderRefId}).exec()
      .then((order) => {
        let planType = order.planType;
        let options = {status: 'canceled','createdBy.email' : order.createdBy.email,orderRefId: orderRefId,shift: 'Lunch',planType: planType};
        Delivery.find(options).count((err,count) => {
          console.log('Count =>>',count);
          if(err) reject(err);
          resolve({'count': count , 'planType' : planType });
        });
      })
      .catch((error) => {
        logger.error(error);
        logger.debug(error.stack);
        reject(error);
      })
    } catch(error) {
      logger.error(error);
      logger.debug(error.stack);
      reject(error);
    }
  })
}
