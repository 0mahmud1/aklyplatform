import async from 'async';
import moment from 'moment'
import Order from '../models';
import MealPlan from '../../meal-plans/models';
import config from 'config'
import CouponUser from '../../couponUsers/models';
import { logger } from 'libs/logger.js';
import { sendTransactionalEmail } from 'libs/emails.js'
import getFoodProvider from '../libs/foodProvider';
import { generateDeliveries, prepareDelivery } from '../libs/delivery';
import { checkValidity } from '../../coupons/libs/checkValidity';
import _ from 'lodash';
import notifyOndemand from '../libs/onDemandInform'
import Customer from '../../customers/models';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';

/**
 * @api {POST} /api/orders Create an order
 * @apiPermission all
 * @apiName CreateOrder
 * @apiGroup Order
 * @apiHeader {String} Authorization valid access token
 * @apiParamExample {json} Request-Example:
 *  {
 *      "orderDate": Date,
*      "subTotal": Number,
*      "address": Object,
*      "locations": [longitude,latitude],
*      "total": Number,
*      "orderType": String,
*      "paymentType": String,
*      "deliveryNote": String,
*      "planType": String,
*      "deliveries":[
*        {
*        "total": Number,
*        "load": Number,
*        "deliveryDate": Date,
*        "meals": [
*            {
*              "quantity": Number
*              "item": {
*                "_id": String,
*                "name": String,
*                "price": Number,
*                "recipe": String,
*                "ingredients": String,
*                "calorie": String,
*                "protein": String,
*                "fat": String,
*                "carbs": String,
*                "description": String,
*                "youtubeUrl": String,
*                "status": String,
*                "load": Number,
*                "images": [
*                  String
*                ],
*                "tags": [
*                  {
*                    "status": String,
*                    "images": [
*                      String
*                    ],
*                    "name": String,
*                    "_id": String
*                  },
*                ]
*              },
*            }
*          ]
*        }
*     ]
*  }
*
* @apiSuccess (200) {Boolean} success return true if succeeds , otherwise false
* @apiSuccess (200) {String} message Order placed successfull
* @apiSuccess (200) {Object[]} data see Success-Response
* @apiSuccessExample {JSON} Success-Response:
*  HTTP/1.1 200 OK
*  {
*      "success": Boolean,
*      "message": "Order placed successfull",
*      "data": {
*          "_id":String,
*          "orderRefId": String,
*          "createdAt": Date,
*          "updatedAt": Date,
*          "load": Number,
*          "zone": Object,
*          "provider": Object,
*          "createdBy": Object,
*          "orderDate": Date,
*          "subTotal": Number,
*          "address": Object,
*          "locations": Object,
*          "total": Number,
*          "orderType": String,
*          "paymentType": String,
*          "status": String,
*          "deliveryNote": String,
*          "planType": String,
*          "deliveries":[
*            "meals": [
*                {
*                  "item": {
*                    "_id": String,
*                    "createdAt": Date,
*                    "updatedAt": Date,
*                    "name": String,
*                    "price": Number,
*                    "recipe": String,
*                    "ingredients": String,
*                    "calorie": String,
*                    "protein": String,
*                    "fat": String,
*                    "carbs": String,
*                    "description": String,
*                    "youtubeUrl": String,
*                    "status": String,
*                    "load": Number,
*                    "images": [
*                      String
*                    ],
*                    "provider": [],
*                    "tags": [
*                      {
*                        "status": String,
*                        "images": [
*                          String
*                        ],
*                        "name": String,
*                        "updatedAt": Date,
*                        "createdAt": Date,
*                        "_id": String
*                      },
*                    ]
*                  },
*                  "quantity": Number
*                }
*              ],
*              "total": Number,
*              "load": Number,
*              "deliveryDate": Date
*            }
*          ]
*        }
*  }
*/

export const createOrder = (req, res, next) => {
  try {
    logger.info('-------function createOrder --------');
    logger.info('function caller email: ', req.decoded.email);
    logger.info('order body: ', req.body);
    if (req.body.planType === "") {
      req.body.planType = "fullDay"
    }

    const newOrder = new Order(req.body);   // assign all data from request body to newOrder object

    async.parallel({
      getCustomer: function(callback) {     // function to find customer
        Customer.findOne({ auth0: req.decoded.sub }).exec()
        .then((response) => {
          logger.info('-------success in createOrder / getCustomer--------');
          newOrder.createdBy = response;      // save info - who creates the order
          callback(null, response);
        })
        .catch((err) => {
          logger.error(err);
          logger.error('error created by: ',req.decoded.email)
          callback(err,null);
        });
      },
      prepareDeliveries: function(callback) {  // function to prepare delivery
        let deliveries = req.body.deliveries;

        let sortedDeliveryDates = _.sortBy(req.body.deliveries, (delivery)=>{
          return moment(delivery.deliveryDate)
        });
          newOrder.firstDeliveryDate = sortedDeliveryDates[0].deliveryDate
          newOrder.lastDeliveryDate = sortedDeliveryDates[sortedDeliveryDates.length-1].deliveryDate
        async.map(deliveries, prepareDelivery, function (error, result) {
          // console.log('inside prepareDeliveries =>>>',result);
            if(error){
              logger.error(error);
              logger.error('error created by: ',req.decoded.email);
              callback(error,null)
            } else {
              logger.info('Success in createOrder / prepareDeliveries');
              callback(null, result);
            }
        });
      },
      getFoodProvider: function (callback) {    // function to find food provider
        logger.info('-------function createOrder / getFoodProvider--------');
        let params = {
          locations: req.body.locations,
          orderType: req.body.orderType
        };
        // params.locations.coordinates[0] = 90.41306;
        // params.locations.coordinates[1] = 23.81446;
        if (_.isArray(req.body.deliveries[0].deliveryDate)) { // remove soon
          params.locations.coordinates[0] = req.body.address.geoLocation.lng;
          params.locations.coordinates[1] = req.body.address.geoLocation.lat;
        }
        // console.log('location params are', params);
        getFoodProvider(params)   // call getFoodProvider from libs/foodProvider
        .then(result=> {
          logger.info('success in createOrder / getFoodProvider')
          callback(null, result);
        }).catch((error) => {
            logger.error('error created by: ',req.decoded.email)
            logger.error('getFoodProvider error', error);
            callback(error,null);
        });
      },
      getMealPlan: function(callback) {
        if(newOrder.orderType==='subscription') {
            MealPlan.findOne({'_id': newOrder.mealPlan}).exec()
                .then((mealPlan) => {
                    if(mealPlan) {
                        let mealPlanDetails = {};
                        mealPlanDetails._id = mealPlan._id;
                        mealPlanDetails.name = mealPlan.name;
                        mealPlanDetails.calorie = mealPlan.calorie;
                        mealPlanDetails.totalPrice = mealPlan.totalPrice;
                        mealPlanDetails.setPrice = mealPlan.setPrice;
                        mealPlanDetails.description = mealPlan.description;
                        newOrder.mealPlanDetails = mealPlanDetails;
                        callback(null, mealPlan);
                    } else {
                        callback(null,{})
                    }
                })
                .catch((err) => {
                    logger.error(err);
                    logger.error('error created by: ',req.decoded.email)
                    callback(err,null);
                });
        } else {
            callback(null,{})
        }


      },
      checkCoupon: function(callback) {
        if(req.body.coupon) {
          const code = req.body.coupon.code;
          const userEmail = req.decoded.email;
          const options = {
            userEmail,
            code
          };
          checkValidity(options, (err,response) => {
            if(err) {
              console.log('catch error here ',err);
              logger.error("Coupon check validity error")
              logger.error(err)
              callback(err, null)
            }
            else {
              //console.log('response ',response);
              callback(null,response);
            }
          })
        } else {
          callback(null,{})
        }
      }
    }, function(err, results) {
      if (!err) {
        logger.info('no error before order');
        const mealPlan = results.getMealPlan;
        newOrder.provider = results.getFoodProvider.data.provider;
        newOrder.zone = results.getFoodProvider.data.zone;
        newOrder.deliveries = results.prepareDeliveries;
        if(newOrder.orderType === 'onDemand') {
          newOrder.subTotal = _.sumBy(results.prepareDeliveries, 'total');
          let checkCoupon = results.checkCoupon;
          if(!_.isEmpty(checkCoupon)) {
            if(checkCoupon.isValidate && !checkCoupon.isExpired && !checkCoupon.isExceedLimit) {
              console.log('new order total onDemand',newOrder.subTotal);
              if(checkCoupon.coupon.couponType === 'fixed') {
                newOrder.total = newOrder.subTotal - checkCoupon.coupon.value;
              } else if(checkCoupon.coupon.couponType === 'percentage') {
                let total = newOrder.subTotal - ((newOrder.subTotal * checkCoupon.coupon.value) / 100);
                newOrder.total = total;
              } else {
                newOrder.total = newOrder.subTotal;
              }
            }
          } else {
            newOrder.total = newOrder.subTotal;
          }


        } else {
          newOrder.subTotal = mealPlan.setPrice;
          let checkCoupon = results.checkCoupon;
          if(!_.isEmpty(checkCoupon)) {
            if(checkCoupon.isValidate && !checkCoupon.isExpired && !checkCoupon.isExceedLimit) {
              console.log('new order total ',newOrder.subTotal);
              if(checkCoupon.coupon.couponType === 'fixed') {
                newOrder.total = newOrder.subTotal - checkCoupon.coupon.value;
              } else if(checkCoupon.coupon.couponType === 'percentage') {
                let total = newOrder.subTotal - ((newOrder.subTotal * checkCoupon.coupon.value) / 100);
                newOrder.total = total;
              } else {
                newOrder.total = newOrder.subTotal;
              }
            }
          } else {
            newOrder.total = newOrder.subTotal;
          }
        }
        newOrder.load = _.sumBy(results.prepareDeliveries, 'load');
        newOrder.save().then((order) => {
          logger.info('order saved in database. orderRefId %s', order.orderRefId);
          if (req.body.coupon) {
            CouponUser.findOne({                  // find a coupon user by email and coupon code
              'user.email': req.decoded.email,
              'coupon.code': order.coupon.code
            }).exec()
            .then((couponUser) => {
              couponUser.coupon = order.coupon;
              couponUser.count = couponUser.count + 1 ;
              couponUser.save();
            })
            .catch((error) => {
              console.log('Error in finding coupon user');
            });
          }
          generateDeliveries({ _id: order._id , mealPlan: mealPlan }).then(reply => {  // generate deliveries for order id
            try {
              logger.info('total generated deliveries is: %s', reply);
              logger.info('email need to be sent for reference :  %s', order.orderRefId);

              if(order.orderType==="onDemand") {
                let emails = config.get('OREDR_NOTIFICATION.EMAIL');
                if(emails.length===2) {
                  notifyOndemand(emails[0],emails[1],order)
                }
              }

              let messageBody = "";
              if(order.orderType === 'onDemand') {
                messageBody = 'Your Order has been confirmed! Soon you\'ll get updates from our team for delivering the food :)';
                messageBody += '<br><br>You created this order in ' + moment(order.createdAt).format('MMMM Do YYYY');
                messageBody += '<br><br>The foods you selected are: <br><br>';
                let foodLists = order.deliveries[0].meals;
                foodLists.map((list,index) => {
                  messageBody += (index + 1) + '. ' +list.item.name + " (" + list.quantity + "pcs)<br>";
                })
                messageBody += '<br>Total bill : ' + order.total;
              } else {
                messageBody = 'Your Order has been confirmed! Soon you\'ll get updates from our team for delivering the food :) '
                messageBody += '<br><br>You have given a <b>' + order.orderType + '</b> order';
                messageBody += '<br><br>First delivery will start from ' + moment(order.firstDeliveryDate).format('MMMM Do YYYY') + ' and your last delivery is at ' + moment(order.lastDeliveryDate).format('MMMM Do YYYY');
                messageBody += '<br><br>You have selected <b>' + order.planType + '</b> meal plan.';
                messageBody += '<br><br>Meal Plan details : <br>Name: '+order.mealPlanDetails.name + '<br>Total Price: ' + order.total + ' QR' + '<br>Total Calorie: '+order.mealPlanDetails.description + ' kCal';

              }

              sendTransactionalEmail(null,order.createdBy,{           // send mail to customer who created order
                subject: `Dear ${order.createdBy.name}, Order Confirmed - ${order.orderRefId}`,
                html: messageBody
              },function(err,res) {
                if(err) {
                  logger.error('Failed to send email')
                  logger.error('error created by: ',req.decoded.email)
                  logger.error(err);
                  return res.status(400).json({
                    success: false,
                    message: 'Failed to send email',
                    data: err
                  });
                }
                logger.info('Email sent successfully');
              })
              return res.status(200).json({
                success: true,
                message: 'Order placed successfully',
                data: order
              })
            } catch (e) {
                logger.error('error created by: ',req.decoded.email)
                logger.error(e)
                next(e)
            }
          }).catch(err =>{
            logger.error('Failed to create generate deliveries')
            logger.error('error created by: ',req.decoded.email)
            logger.error(err);
            return res.status(400).json({
              success: false,
              message: 'Failed to create generate deliveries',
              data: err
            });
          })
        }).catch((error) => {
          logger.error('Failed to create a order');
          logger.error('error created by: ',req.decoded.email)
          logger.error(error);
          return res.status(400).json({
            success: false,
            message: 'Failed to create a order',
            data: error
          })
        });
      } else {
        logger.error('error in create food');
        logger.error('error created by: ',req.decoded.email)
        logger.error(err);
        return res.status(404).json({
          success: false,
          message: err.message,
          data: err
        })
      }
    });

  } catch (e) {
    logger.error('Error in createOrder');
    logger.error('error created by: ',req.decoded.email)
    logger.debug(e.stack);
    logger.error(e);
    return res.status(500).json({
      success: false,
      message: 'Error in createOrder',
      error: e
    });
  }
};
