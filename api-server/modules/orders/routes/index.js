import express from 'express';

import { logger } from 'libs/logger.js';
import { getOrders } from '../controllers/getOrders.js';
import { createOrder } from '../controllers/createOrder.js';
import { readOrder } from '../controllers/readOrder.js';
import { updateOrder } from '../controllers/updateOrder.js';
import { deleteOrder } from '../controllers/deleteOrder.js';
import { rateOrder } from '../controllers/rateOrder.js';
import { stopOrder } from '../controllers/stopOrder.js';
import { checkAuthToken, isAuthorizedRoles } from '../../auth/controllers.js';

const orderRoutes = express.Router();

orderRoutes.use(['/orders','/orders/rates/:_id','/orders/:_id'],(req, res, next) => {
  logger.info(`a ${req.method} request in orders route.`);
  checkAuthToken(req, res, next);
});
orderRoutes.route('/orders')
  // All active orders (accessed at GET /api/orders)
  .get(getOrders)
  // Create a order (accessed at POST /api/orders)
  .post(createOrder);

orderRoutes.route('/orders/rates/:_id')
  // give rating to specific order
  .put(isAuthorizedRoles("customer"), rateOrder)

orderRoutes.route('/orders/stop/:orderRefId')
  // give rating to specific order
  .put(isAuthorizedRoles("customer"), stopOrder)

// @todo will implement roles. pause for client problem
orderRoutes.route('/orders/:_id')
  // Read a order (accessed at GET /api/orders/:_id)
  .get(readOrder)
  // Update a order (accessed at PUT /api/orders/:_id)
  .put(isAuthorizedRoles("administrator", "manager", "customer", "foodProvider", "deliveryBoy"), updateOrder)
  // Delete a order (accessed at DELETE /api/orders/:_id)
  .delete(isAuthorizedRoles("administrator", "manager"), deleteOrder);

export default orderRoutes;
