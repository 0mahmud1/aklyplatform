/**
 * Created by sajid on 6/12/17.
 */
import async from 'async';
import moment from 'moment'
import Order from '../models';
import CouponUser from '../../couponUsers/models';
import { logger } from 'libs/logger.js';
import { sendTransactionalEmail } from 'libs/emails.js'
import getFoodProvider from '../libs/foodProvider';
import { generateDeliveries, prepareDelivery } from '../libs/delivery';
import { checkValidity } from '../../coupons/libs/checkValidity';
import _ from 'lodash';

import Customer from '../../customers/models';
import { placeHolder, errorPlaceHolder } from 'libs/placeholder.js';


export default function notifyOndemand(receiver1, receiver2, order) {

    let messege =`Order Reference Id : ${order.orderRefId}<br>Order Time: ${order.createdAt}<br><br>Customer Details:<br>Name: ${order.createdBy.name}<br>Email: ${order.createdBy.email}<br>Phone: ${order.address.phone}<br>${order.address.label}# ${order.address.apt}<br>${order.address.street}<br>`
    if(order.deliveryNote) messege+=`<br>Customer's Note: ${order.deliveryNote}<br>`
     messege += "<br>Customer ordered for<br>"
     _.map(order.deliveries[0].meals, meal=>{
         messege+=meal.item.name.toString()+" ("+meal.quantity +"pcs)<br>"
    })
    console.log(messege)
    let recepient = []
    let recepient1 = {
        email: receiver1
    }
    recepient.push(recepient1)
    let recepient2 = {
        email: receiver2
    }
    recepient.push(recepient2)

    _.forEach(recepient, item=>{
        sendTransactionalEmail(null,item,{           // send mail to receiver 1
            subject: `${order.createdBy.name} Ordered onDemand - ${order.orderRefId}`,
            html: messege
        },function(err,res) {
            if(err) {
                logger.error('Failed to send email to', item.email)
                logger.error(err);
                return res.status(400).json({
                    success: false,
                    message: 'Failed to send email to '+item.email,
                    data: err
                });
            }
            logger.info('Email sent successfully to '+item.email);
        })
    })




}