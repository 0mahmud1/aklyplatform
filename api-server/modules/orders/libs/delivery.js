/**
 * Created by zahed on 11/4/16.
 */
import async from 'async';
import _ from 'lodash';
import moment from 'moment';
import shortid from 'shortid';

import { logger } from 'libs/logger.js';
import Orders from 'modules/orders/models/index';
import MealPlan from 'modules/meal-plans/models/index';
import VanConfirmation from 'modules/vanConfirmations/models/index';
import Delivery from 'modules/deliveries/models/index';
import Food from 'modules/foods/models/index';

export function generateDeliveries(options) {
  logger.info('called in generateDeliveries ');
  return new Promise(function(resolve, reject) {
    try {
      Orders.findOne({ _id: options._id }).exec()
      .then(order => {
          logger.info('Order Found - orderRefId = ',order.orderRefId);
          const deliveries = _.map(order.deliveries, (data) => {
          const delivery = {};
          delivery.orderRefId = order.orderRefId;
          delivery.deliveryDate = moment(new Date(data.deliveryDate)).format('YYYY-MM-DD');
          delivery.orderType = order.orderType;
          delivery.planType = order.planType;
          delivery.paymentType = order.paymentType;
          delivery.deliveryNote = order.deliveryNote;
          delivery.address = order.address;
          delivery.locations = order.locations;
          delivery.zone = order.zone;
          delivery.provider = order.provider;
          delivery.mealPlan = order.mealPlan;
          delivery.van = {};
          delivery.deliveryBoy = {};
          delivery.shift = data.shift;
          delivery.meals = data.meals;
          delivery.quantity = data.quantity;
          delivery.total = data.total;
          delivery.load = data.load;
          delivery.subTotal = delivery.total;
          delivery.createdBy = data.createdBy;
          delivery.deliveryRefId = `akly-D-${shortid.generate()}`;
          delivery.createdBy = order.createdBy;
          delivery.createdAt = new Date();
          if(order.orderType==='subscription') {
            const mealPlan = options.mealPlan;
            if(mealPlan) {
              let minWeek = parseInt(moment(order.firstDeliveryDate).format('w'));
              let maxWeek = parseInt(moment(order.lastDeliveryDate).format('w'));
              let currentWeek = parseInt(moment(delivery.deliveryDate).format('w'));
              let weekNumber = currentWeek - minWeek;
              let plans = mealPlan.plans;
              let filteredPlans = _.filter(plans,(plan) => {
                return plan.week === weekNumber && plan.day.toLowerCase() === moment(delivery.deliveryDate).format('dddd').toLowerCase() && plan.slot.toLowerCase() === delivery.shift.toLowerCase()
              });
              delivery.defaultMeals = filteredPlans[0].foods;
              return delivery;
            } else {
              delivery.defaultMeals = delivery.meals;
              return delivery;
            }

          } else {
              delivery.defaultMeals = delivery.meals;
              return delivery;
          }

        });

        Delivery.insertMany(deliveries)
        .then(deliveries => {
          logger.info('total deliveries %s', deliveries.length);
          resolve(deliveries.length);
        })
        .catch(function(err) {
          logger.error('unable to save in deliveries');
          logger.error(err);
          reject(e);
        });
      });

    } catch (e) {
      logger.error(e);
      reject(e);
    }
  });
}

export function prepareDelivery(options, callback) {
  try {
    logger.info('---------calling prepareDelivery---------');
    // @todo will implement later
    // if (_.isArray(options.shift)) {
    //   options = checkIdiotSashau(options);
    //   console.log('-------------- changed data ----------');
    //   logger.info(options);
    //   console.log('-------------- end changed data ----------');
    //   //callback('shift should be string', null);
    // }
    // if (_.isArray(options.deliveryDate)) {
    //   callback('deliveryDate should be string', null);
    // }
    // if (_.isArray(options.quantity)) {
    //   callback('deliveryDate should be string', null);
    // }
    const delivery = {};

    //delivery.deliveryDate = options.deliveryDate;
    //delivery.deliveryDate = moment(options.deliveryDate).toDate();
    delivery.deliveryDate = moment(options.deliveryDate).format();
    delivery.shift = options.shift;
    delivery.load = 0;
    delivery.total = 0;
    delivery.quantity = 0;

    function prepareMeal(data, cb) {      // generate meal
      logger.info('---------calling prepareDelivery/prepareMeal---------');
      // console.log(data);
      const meal = {};
      //@todo will implement later
      // if (_.isArray(data._id)) {
      //   callback('food id should be string', null);
      // }
      // if (_.isArray(data.quantity)) {
      //   callback('food quantity should be string', null);
      // }
      meal.quantity = parseInt(data.quantity);
      Food.findOne({_id: data.item._id , status: 'Active'}).exec()
      .then(food => {
        if (food) {
          meal.item = food;
          delivery.load += parseInt((food.load * parseInt(data.quantity)));
          delivery.total += (food.price * parseInt(data.quantity));
          delivery.quantity += parseInt(data.quantity);
          cb(null, meal);
        } else {
          cb(new Error("Food not found"),null)
        }
      }).catch(e =>{
          logger.error('---------error in prepareDelivery/prepareMeal---------');
          logger.error(e)
          cb(e, null);
      });
    }

    async.map(options.meals, prepareMeal, function (error, result) {    // call prepareMeal
      logger.info('reply from prepareMeal');
      logger.info('total meal %s', result.length);
      if (!error) {
        delivery.meals = result;    // set deliver meals
        // console.log('delivery =>>>',delivery);
        callback(null, delivery);
      } else {
        logger.error(error)
        callback(error, null)
      }
    });

    // callback(null, options);
  } catch (e) {
      logger.error('error in prepareDelivery');
      logger.error(e);
      callback(e, null)
  }
}
