/**
 * Created by zahed on 10/24/16.
 */
import async from 'async';
import colors from 'colors';
import _ from 'lodash';
import { logger } from 'libs/logger';
import Zone from 'modules/zones/models/index';
import FoodProvider from 'modules/providers/models/index';

const isPointInPolygon = function (point, polygonPoint) {
  logger.info('call isPointInPolygon');
  let inside = false;
  let i;
  let j;
  let x1;
  let y1;
  let x2;
  let y2;
  let intersect;

  for (i = 0, j = polygonPoint.length - 1; i < polygonPoint.length; j = i, i = i + 1) {
    x1 = polygonPoint[i].lat;
    y1 = polygonPoint[i].lng;
    x2 = polygonPoint[j].lat;
    y2 = polygonPoint[j].lng;

    intersect = ((y1 > point.lng) !== (y2 > point.lng))
      && (point.lat < (x2 - x1) * (point.lng - y1) / (y2 - y1) + x1);
    if (intersect) { inside = !inside; }
  }
  return inside;
};

function haversineDistance(source, destination) {
  function toRad(x) {
    return x * Math.PI / 180;
  }

  const R = 6371; // km

  const distanceLat = toRad(destination.lat - source.lat);
  const distanceLon = toRad(destination.lng - source.lng);

  const a = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) +
    Math.cos(toRad(source.lat)) * Math.cos(toRad(destination.lat)) *
    Math.sin(distanceLon / 2) * Math.sin(distanceLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
}

export default function getFoodProvider(options) {
  // step 1: check geo-location in options
  // step 2: check geo-location within zone
  // step 3: if yes check nearest food provider

  return new Promise(function(resolve, reject) {
    try {
      logger.info('call getFoodProvider');
      console.log('getFoodProvider...options >>>',options);     // console in terminal only
      const reply = {};
      if (!_.has(options.locations, 'coordinates')) {           // if no co-ordinates found in request
        logger.error('no coordinates found in request'.red);
        reply.success = false;
        reply.message = 'coordinates didn\'t find in object';
        reply.data = null;

        reject(reply);
      } else {
        Zone.findOne({      // find zone by given co-ordinates
          status: 'Active',
          locations: {
            $geoIntersects: {
              $geometry:{
                type : "Point",
                coordinates : options.locations.coordinates }
              }
            }
        }).exec()
        .then(zone => {
          console.log('Zone found >>>',zone);
          if (_.isNull(zone)) {       // if no zone found
            logger.error('no zone found'.red);
            reply.success = false;
            reply.message = 'zone didn\'t find in given geo-location';
            reply.data = null;
            reject(reply);
          } else {            // if zone found
            logger.info('Zone Found');
            const zoneData = zone._doc;
            const foodProviders = zoneData.foodProviders;   //  food providers in zone
            if (_.isEmpty(zoneData)) {    // if zone data is empty
              logger.error('no zone found'.red);
              reply.success = false;
              reply.message = 'zone didn\'t find in given geo-location';
              reply.data = null;

              reject(reply);
            } else if (!_.has(zoneData, 'foodProviders') || _.isEmpty(foodProviders)) { // if no providers found in zone
              logger.error('no food provider found'.red);
              reply.success = false;
              reply.message = 'Food provider didn\'t find in zone';
              reply.data = { zone: zoneData };
              reject(reply);
              return;
            } else {
              logger.info('calculate nearest food provider'.cyan);

              const providerIds = _.map(zoneData.foodProviders, (data) => {
                return data._id;
              });

              FoodProvider.find({     // find food provider using provider IDs
                _id: { $in : providerIds },
                providerType: { $in: [options.orderType]}
              }).exec().then((providers) => {
                logger.info("Providers found total ",providers.length);
                if (_.isEmpty(providers)) {     // if no food provider found
                  logger.error('no food provider found'.red);
                  reply.success = false;
                  reply.message = 'Food provider didn\'t find in zone by your order type';
                  reply.data = { zone: zoneData };
                  reject(reply);
                  return;
                }
                try {
                  let selectedFoodProvider;
                  if (providers.length > 1) {   // if more then 1 food provider found , calculate nearest food provider
                    let providerList = _.map(providers, (data) => {
                      let locData = {
                        lat: options.locations.coordinates[1],
                        lng: options.locations.coordinates[0],
                      };
                      let providerLoc = {
                        lat: data.locations.coordinates[1],
                        lng: data.locations.coordinates[0],
                      };
                      data.distance = haversineDistance(locData, providerLoc);
                      return data;
                    });
                    selectedFoodProvider = _.minBy(providerList, 'distance');
                    logger.debug('nearest FP ', _.minBy(providerList, 'distance').toObject());
                  } else {          // if only 1 food provider found , select him
                    selectedFoodProvider = providers[0];
                  }

                  reply.success = true;
                  reply.message = 'Food Provider found';
                  reply.data = { zone: zoneData, provider: selectedFoodProvider };

                  logger.info('food provider found');
                  resolve(reply);
                } catch (error) {
                  reply.success = false;
                  reply.message = 'Error in calculation food provider';
                  reply.data = { zone: zoneData, error: error };
                  logger.error(error);
                  reject(reply);
                }
              }).catch( error =>{
                reply.success = false;
                reply.message = 'Error in food provider query';
                reply.data = { zone: zoneData, error: error };
                logger.error(error);
                reject(reply);
              });
            }
          }
        })
        .catch((e) => {
            logger.error(e);
            reject(e);
        });
      }
    } catch (e) {
        logger.debug(e.stack);
        logger.error(e.reason);
        reject(e);
    }
  });
}
