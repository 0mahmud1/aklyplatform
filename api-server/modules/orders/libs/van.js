/**
 * Created by zahed on 10/30/16.
 */
import _ from 'lodash';
import { logger } from 'libs/logger.js';

function haversineDistance(source, destination) {
  function toRad(x) {
    return x * Math.PI / 180;
  }

  const R = 6371; // km

  const distanceLat = toRad(destination.lat - source.lat);
  const distanceLon = toRad(destination.lng - source.lng);

  const a = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) +
    Math.cos(toRad(source.lat)) * Math.cos(toRad(destination.lat)) *
    Math.sin(distanceLon / 2) * Math.sin(distanceLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  return R * c;
}

export default function assignOrderSerial(options) {
  try {

  } catch (e) {
    logger.debug(e.stack);
    logger.error(e.reason);

    throw new Error(e.reason);
  }
}

export default function getVan() {

}