import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import config from 'config';
import colors from 'colors';
import chalk from 'chalk';
import cors from 'cors';
import compression from 'compression';
import 'app-module-path/register';
import { addPath } from 'app-module-path';
addPath(__dirname);

import { logger } from './libs/logger';
import { configureAdminUser } from './modules/administrators/lib/config.js';
import { setTimeZone } from './libs/timezone.js';
const app = express();

/**
 * Load all server routes file
 */
import administratorRoutes from './modules/administrators/routes/index';
import authRoutes from './modules/auth/routes.js';
import tagRoutes from './modules/tags/routes';
import categoryRoutes from './modules/categories/routes';
import zoneRoutes from './modules/zones/routes';
import foodRoutes from './modules/foods/routes';
import mealPlanRoutes from './modules/meal-plans/routes';
import orderRoutes from './modules/orders/routes';
import dBoyRoutes from './modules/deliveryBoys/routes';
import fpRoutes from './modules/providers/routes'
import vanRoutes from './modules/vans/routes';
import deliveryRoutes from './modules/deliveries/routes';
import vanConfirmationRoutes from './modules/vanConfirmations/routes';
import customerRoutes from './modules/customers/routes';
import bmiRoutes from './modules/bmi/routes';
import supportRoutes from './modules/supports/routes';
import couponRoutes from './modules/coupons/routes';
import couponUserRoutes from './modules/couponUsers/routes';
// Use mongoose native promises
mongoose.Promise = Promise;

/**
 * Connect to mongodb
 */
if (config.get('MONGO_USER')) {
  const DBOptions = {
    user: config.get('MONGO_USER'),
    pass: config.get('MONGO_PASSWORD')
  };
  mongoose.connect(process.env.MONGO_URL || config.get('MONGO_URL'), DBOptions);
} else {
  mongoose.connect(process.env.MONGO_URL || config.get('MONGO_URL'));
}
mongoose.connection.on('connected', function () {
  logger.info(chalk.underline(`APP MONGODB@${mongoose.version}:`), chalk.magenta(config.get('MONGO_URL')));
});

mongoose.connection.on('disconnected', function () {
  logger.info(`Mongoose disconnected to: ${chalk.red.bold(config.get('MONGO_URL'))}`);
});

process.on('SIGINT', function () {
  logger.info(chalk.red.bold('\nMongoose disconnected through app termination\n'));
  process.exit(0);
});

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({limit: '50mb',extended: true, parameterLimit:50000}));
app.use(cors());
// need to enable compression in server
app.use(compression());
app.use(bodyParser.json({limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/**
 *  App Routes
 */
// for ssl config route
app.get('/.well-known/acme-challenge/GbNcvl6IY0cxDoRw5GORIqzjXH1Z3k-bgheT8B1EHCA',function (req,res) {
  res.send('GbNcvl6IY0cxDoRw5GORIqzjXH1Z3k-bgheT8B1EHCA.iRyyrEqqet-FXhuhy0BDdEq-CXB-Wri0RSDlC9Mqujw')
})



let apiRoutes = [
  authRoutes,
  tagRoutes,
  categoryRoutes,
  zoneRoutes,
  mealPlanRoutes,
  foodRoutes,
  orderRoutes,
  administratorRoutes,
  dBoyRoutes,
  fpRoutes,
  vanRoutes,
  deliveryRoutes,
  vanConfirmationRoutes,
  customerRoutes,
  bmiRoutes,
  supportRoutes,
  couponRoutes,
  couponUserRoutes,
];

app.use('/api', apiRoutes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// production error handler
// no stacktraces leaked to user
if (app.get('env') === 'production') {
  app.use(function (err, req, res, next) {
    console.log('error helper: ', err);
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: {}
    });
  });
}

logger.info(chalk.underline('APP ENVIRONMENT: '), chalk.magenta(app.get('env')));

// development error handler
// will print stacktrace. ERROR middleware contains 4 parameters.
app.use(function (err, req, res, next) {
  logger.error('error middleware: ', req.originalUrl, err);
  res.status(err.status || 500);
  return res.json({
    message: err.message,
    error: err
  });
});

//set time zone
setTimeZone();

// create default admin user
configureAdminUser();

export default app;
