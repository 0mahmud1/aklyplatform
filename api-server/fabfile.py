from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists

import sys, os

abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)

# fab prod install  install + deploy
# fab prod deploy
# --------------------------------------------
# Akly Platform AWS staging Machines
# --------------------------------------------

class FabricException(Exception):
    pass

def dev():
    print "Connecting to Akly AWS api server dev Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.warn_only = True

    env.development_env = 'dev'

    env.key_filename = abspath('akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '52.77.231.224'    #akly-dev-api
    ]
    env.app_path = '/home/ubuntu/aklyplatform'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets',
        'node_modules'
    ]
    return


def stage():
    print "Connecting to Akly AWS api server stage Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.warn_only = True
    env.development_env = 'stage'

    env.key_filename = abspath('akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '54.255.209.192'    #akly-stage-api
    ]
    env.app_path = '/home/ubuntu/aklyplatform'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets',
        'node_modules'
    ]
    return

def prod():
    print "Connecting to Akly AWS api server productions Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.warn_only = True
    env.development_env = 'prod'

    env.key_filename = abspath('akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '54.179.128.141',    #akly-prod-api1
        '54.255.178.160',    #akly-prod-api2
        '54.255.178.114',    #akly-prod-api3
    ]
    env.app_path = '/home/ubuntu/aklyplatform'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets',
        'node_modules'
    ]
    return

# --------------------------------------------
# Installing Akly Staging Platform
# --------------------------------------------

def install():
    if env.setup:
        print 'Start installing Akly Platform'
        install_nginx()
        nginx_config()
        install_nodejs()
        install_pm2()
        sync_code_base()
        npm_install()
        setup_pm2()
        print 'Finished installing Akly Platform'
    return

def install_nginx():
    print 'Installing NGINX'
    sudo('apt-get update')
    sudo("apt-get install -y nginx")
    return

def nginx_config():
    print 'Configuring NGINX'
    default_config='/etc/nginx/sites-enabled/default'
    if exists(default_config):
        sudo('rm /etc/nginx/sites-enabled/default')
        print 'Deleted NGINX default config'

    print 'Install NGINX config'
    put('%s' % (env.nginx_config), '/etc/nginx/sites-enabled/', use_sudo=True)

    print 'Restarting NGINX'
    sudo("service nginx stop")
    sudo("service nginx start")
    return

def uninstall_nginx():
    print 'Uninstalling NGINX'
    sudo('apt-get purge nginx nginx-common')
    sudo('apt-get autoremove')
    return

def install_pm2():
    print 'Installing PM2'
    sudo('npm install -g pm2')
    return


def install_nodejs():
    print 'Installing latest nodejs '
    run('cd ~;curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh')
    sudo('cd ~; bash nodesource_setup.sh')
    sudo('apt-get -y install nodejs build-essential')
    return

def setup_pm2():

    if env.development_env == 'stage':
        print 'pm2 stage config'
        run('cd %s/; pm2 start --name api-server npm  -- run stage' % env.app_path)

    if env.development_env == 'dev':
        print 'pm2 dev config'
        run('cd %s/; pm2 start --name api-server npm -- run dev' % env.app_path)

    if env.development_env == 'prod':
        print 'pm2 stage config'
        run('cd %s/; pm2 start --name api-server npm -- run prod' % env.app_path)


    if env.ubuntu_version == '14.04':
        # deamon for ubuntu 14.04
        sudo('env PATH=$PATH:/usr/local/bin pm2 startup -u ubuntu')
    else:
        # deamon for  ubuntu 16.04
        run('pm2 startup systemd')
        sudo('env PATH=$PATH:/usr/bin pm2 startup systemd -u ubuntu --hp /home/ubuntu')
        #sudo('su -c "env PATH=$PATH:/usr/bin pm2 startup systemd -u ubuntu --hp /home/ubuntu"')
    return

def delete_pm2():
    print 'restart pm2'

    sudo('pm2 delete api-server')

def deploy():
    print 'Start deploying Akly Platform for staging API server'
    #delete_pm2()
    sync_code_base()
    npm_install()
    sudo('kill $(lsof -t -i:3000)')
    #setup_pm2()
    print 'Successfully finished deployment'

def npm_install():
    print 'npm install'
    run('cd %s;npm install;' %  env.app_path);

def sync_code_base():
    print 'Syncing code base'
    rsync_project(env.app_path, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    return


# --------------------------------------------
# fabric script end
# --------------------------------------------
