import config from 'config'
import axios from 'axios'
import {logger} from './logger.js'

export function sendTransactionalEmail(sender, receiver,data,callback) {
  logger.info('IN EMAIL SEND METHOD');
  try{
    let body = {
      key: config.get('MANDRILL.API_KEY'),
      message: {
        html: data.html,
        text: 'backup text if html fails to render',
        subject: data.subject,
        from_email: "no-reply@inov.io",
        from_name: 'Akly Admin',
        to: [
          {
            email: receiver.email,
            name: receiver.name,
            type: "to"
          }
        ],
        headers: {
          'Reply-To': "no-reply-to@inov.io"
        },
        important: true,
        track_opens: null,
        track_clicks: null,
        auto_text: null,
        auto_html: null,
        inline_css: null,
        url_strip_qs: null,
        preserve_recipients: null,
        view_content_link: null,
        tracking_domain: null,
        signing_domain: null,
        return_path_domain: null,
        merge: true,
        merge_language: "mailchimp"
      },
      async: false,
      ip_pool: "Main Pool",
      send_at: new Date().toString()
    }

    // console.log('email body ',body)

    axios.post(`${config.get('MANDRILL.BASE_URL')}messages/send.json`, body)
      .then(res => {
        callback(null,res);
      })
      .catch(err => {
        callback(err,null);
      })
  } catch(e) {
    logger.error(e)
    callback(e,null);
  }
}


/**
 *
 * {
    "key": "frI6PKFFZEOZhEshpKy6nQ",
    "message": {
        "html": "<p>Jomeeh! Email Asche! thank you mandrill</p>",
        "text": "Example text content",
        "subject": "Sample Transactional Email",
        "from_email": "no-reply@inov.io",
        "from_name": "Fosuuu",
        "to": [
            {
                "email": "fa@inov.io",
                "name": "Faysal Ahmed",
                "type": "to"
            }
        ],
        "headers": {
            "Reply-To": "no-reply-to@inov.io"
        },
        "important": true,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": null,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "merge": true,
        "merge_language": "mailchimp"
    },
    "async": false,
    "ip_pool": "Main Pool",
    "send_at": "Mon Jan 09 2017 12:48:41 GMT+0600 (BDT)"
}
 *
 */
