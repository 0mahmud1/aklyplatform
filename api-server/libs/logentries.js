import config from 'config';
import Logger from 'le_node';

export const logger = new Logger({
  token: config.get('LOGENTRIES.TOKEN')
});
