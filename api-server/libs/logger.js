// import bunyan from 'bunyan';
// import moment from 'moment';
// import chalk from 'chalk';
// // import json from 'json';
//
// const ringbuffer = new bunyan.RingBuffer({ limit: 1000 });
//
// function MyRawStream() {}
//
// function syntaxHighlight(json) {
//   let levelColor = chalk.white;
//
//   json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
//   return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
//     var cls = 'number';
//     if (/^"/.test(match)) {
//       if (/:$/.test(match)) {
//         console.log('here');
//         levelColor = chalk.red;
//       } else {
//         levelColor = chalk.green;
//       }
//     } else if (/true|false/.test(match)) {
//       levelColor = chalk.red;
//     } else if (/null/.test(match)) {
//       levelColor = chalk.red;
//     }
//     console.log(levelColor(match));
//   });
// }
// MyRawStream.prototype.write = function (rec) {
//   let levelColor = chalk.white;
//   // console.log(rec);
//   if (bunyan.nameFromLevel[rec.level] === 'error') {
//     levelColor = chalk.red;
//   } else if (bunyan.nameFromLevel[rec.level] === 'debug') {
//     levelColor = chalk.magenta;
//   } else if (bunyan.nameFromLevel[rec.level] === 'info') {
//     levelColor = chalk.blue;
//   }
//   console.log(
//     '[%s:%s] %s: %s [%s: %s]',
//     chalk.green(moment(rec.time).format('lll')),
//     chalk.dim(rec.hostname),
//     levelColor(bunyan.nameFromLevel[rec.level]),
//     rec.msg,
//     // syntaxHighlight(JSON.stringify(rec)),
//     chalk.cyan(rec.src.file),
//     chalk.cyan(rec.src.line),
//   );
// };
//
// export const logger = bunyan.createLogger({
//   name: 'akly-log',
//   src: true,
//   // stream: process.stderr,
//   // streams: [{
//   //   stream: new MyRawStream(),
//   //   type: 'raw',
//   //   serializers: bunyan.stdSerializers,
//   //   path: '/var/log/akly-log.log',
//   // }]
// });
//
//
import winston from 'winston';
import moment from 'moment';
import config from 'config';
import Logger from 'le_node';
import Logentries from 'winston-logentries';

const configs = {
  levels: {
    error: 0,
    debug: 1,
    warn: 2,
    data: 3,
    info: 4,
    verbose: 5,
    silly: 6
  },
  colors: {
    error: 'red',
    debug: 'blue',
    warn: 'yellow',
    data: 'grey',
    info: 'green',
    verbose: 'cyan',
    silly: 'magenta'
  }
};
winston.emitErrs = true;

export const logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      filename: '/var/tmp/akly-api-server.log',
      handleExceptions: true,
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
      colorize: false,
      timestamp :true,
    }),
    new winston.transports.Console({
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp :true,
      prettyPrint: true,
    }),
    new winston.transports.Logentries({
      token: config.get('LOGENTRIES.TOKEN'),
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp :true,
      prettyPrint: true,
    })
  ],
  levels: configs.levels,
  colors: configs.colors,
  exitOnError: false
});
