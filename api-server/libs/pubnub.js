/**
 * Created by zahed on 10/26/16.
 */
import config from 'config';
import PubNub from 'pubnub';

export const AklyPubNub = new PubNub({
  subscribeKey: config.get('PUBNUB.SUBSCRIBEKEY'),
  publishKey: config.get('PUBNUB.PUBLISHKEY'),
});