export const placeHolder = (success, message, data) => {
  return {
    success: success,
    message: message,
    data: data
  }
};

export const errorPlaceHolder = (status, success, message, data) => {
  return {
    status: status,
    success: success,
    message: message,
    data: data
  }
};