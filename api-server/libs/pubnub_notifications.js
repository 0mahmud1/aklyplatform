/**
 * Created by mahmud on 03/11/17.
 */
import { logger } from 'libs/logger.js';
import { AklyPubNub } from 'libs/pubnub.js';

export function  pubnubNotification(channelName,information, data) {
 try{
   data = data? data:{};
   AklyPubNub.publish({
     message: {
       pn_gcm: {
         data: {
           text: information,
           data: data
         }
       },
       text: information,
       data: data
     },
     channel: channelName,
   }, function (status, response) {
     if (status.error) {
       // handle error
       console.log(status)
     } else {
       logger.info('-------- pubnub log ---------')
       logger.info('information ==>>>',information)
       logger.info('data ==>>',data)
       console.log('channel: ',  channelName)
       console.log("message Published time-token:", response.timetoken)
       logger.info('-------- end pubnub log ---------')
     }
   });

 } catch(e) {
   logger.error(e)
 }
}

export function  pubnubNotificationiOS(channelName,options) {
    try{
        AklyPubNub.publish({
            message: {
                pn_gcm: {
                 messageType: options.messageType,
                 orderId:options.orderId,
                 orderRefId:options.orderRefId
                },
                messageType: options.messageType,
                orderId:options.orderId,
                orderRefId:options.orderRefId
            },
            channel: channelName,
        }, function (status, response) {
            if (status.error) {
                // handle error
                console.log(status)
            } else {
                logger.info('-------- pubnub log ---------')
                logger.info('data ==>>',options)
                console.log('channel: ',  channelName)
                console.log("message Published time-token:", response.timetoken)
                logger.info('-------- end pubnub log ---------')
            }
        });

    } catch(e) {
        logger.error(e)
    }
}

export function  pubnubNotificationiOSsubscription(channelName,options) {
    try{
        AklyPubNub.publish({
            message: {
                pn_gcm: {
                 messageType: options.messageType,
                 orderId:options.orderId,
                 orderRefId:options.orderRefId,
                 weekNum: options.weekNum
                },
                messageType: options.messageType,
                orderId:options.orderId,
                orderRefId:options.orderRefId,
                weekNum: options.weekNum
            },
            channel: channelName,
        }, function (status, response) {
            if (status.error) {
                // handle error
                console.log(status)
            } else {
                logger.info('-------- pubnub log ---------')
                logger.info('data ==>>',options)
                console.log('channel: ',  channelName)
                console.log("message Published time-token:", response.timetoken)
                logger.info('-------- end pubnub log ---------')
            }
        });

    } catch(e) {
        logger.error(e)
    }
}
