import moment from 'moment-timezone';

export const setTimeZone = () => {
  console.log('setTimeZone');
  moment.tz.setDefault('Asia/Qatar');
}
