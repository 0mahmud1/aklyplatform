/**
 * Created by zahed on 12/24/16.
 */

export function getPolygonCentroid(pts) {
  let first = pts[0];
  let last = pts[pts.length-1];
  if (first.x != last.x || first.y != last.y) pts.push(first);
  let twiceArea=0;
  let x=0;
  let y=0;
  let nPts = pts.length;
  let p1;
  let p2;
  let f;

  for ( let i=0, j=nPts-1 ; i<nPts ; j=i++ ) {
    p1 = pts[i]; p2 = pts[j];
    f = p1[1] * p2[0] - p2[1] * p1[0];
    twiceArea += f;
    x += ( p1[1] + p2[1] ) * f;
    y += ( p1[0] + p2[0] ) * f;
  }
  f = twiceArea * 3;
  return [y/f, x/f];
}