import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import axios from 'axios';

import reducers from './reducers';

const checkToken =  (store) => (next) => (action) => {
  axios.defaults.headers.common['authorization'] = store.getState().auth.token ? 'Bearer ' + store.getState().auth.token : null;
	return next(action);
};

const middlewares = applyMiddleware(thunk, checkToken, logger());
const store = createStore(reducers, middlewares);

export default store;
