import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import cors from 'cors';
import { readFileSync } from 'jsonfile';

// path for manifest json file
const manifestPath = `${process.cwd()}/public/build/manifest.json`;

//read the manifest.json file
const manifest = readFileSync(manifestPath);

// js and css bundle maping to objects
const jsBundle = manifest['/build/dashboard.js'];

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());

// config static dir
app.use(favicon(path.join(__dirname, 'public', 'images/favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));

// route handler

app.get('/.well-known/acme-challenge/SwttUF8bi2VgBIOcDTACEWuihA3LKfreA-nfhMVc3HU', function (req, res) {
  res.send('SwttUF8bi2VgBIOcDTACEWuihA3LKfreA-nfhMVc3HU.iRyyrEqqet-FXhuhy0BDdEq-CXB-Wri0RSDlC9Mqujw');
});

app.get('*', function (req, res) {
  res.render('index', { title: 'Local :: Akly Platform', jsBundle });
});

export default app;
