import { combineReducers } from 'redux';

import authenticateReducer from './modules/profile/reducers/index.js';
import MealPlanReducer from './modules/subscription/reducers/mealPlanReducer.js';
import HubsReducer from './modules/checkout/reducers';
// import foodReducer from './modules/foods/reducers';
import cuMealPlan from './modules/subscription/reducers/index.js';
import onDemand from './modules/onDemand/reducers/index.js';
import orderReducer from './modules/orders/reducers';

import { userProfileReducer } from './modules/bmi/reducers/userProfileReducer';
import { bodyMassIndexReducer } from './modules/bmi/reducers/bodyMassIndexReducer';
import { suggestionReducer } from './modules/bmi/reducers/suggestionReducer';


import foodsReducer from './modules/core/reducers/index.js'
import tagReducer from './modules/core/reducers/tagReducer'
import subscriptionEdit from './modules/orders/reducers/index.js'
import PromoReducer from './modules/checkout/reducers/promoReducer.js'

const reducers = combineReducers({
  auth: authenticateReducer,
  mealPlans: MealPlanReducer,
  hubs: HubsReducer,
  foods: foodsReducer,
  //foods: foodReducer,
  tags: tagReducer,
  cuMealPlan: cuMealPlan,
  onDemand: onDemand,
  orders: orderReducer,
  // subscriptionEdit: subscriptionEdit,
  userProfile: userProfileReducer,
  bodyMassIndex: bodyMassIndexReducer,
  suggestions: suggestionReducer,
  promo: PromoReducer
});

export default reducers;
