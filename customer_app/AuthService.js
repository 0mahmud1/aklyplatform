import { EventEmitter } from 'events';
import Auth0Lock from 'auth0-lock';
import { browserHistory } from 'react-router';
import toastr from 'toastr';
import axios from 'axios';

import store from './store';
import { readUserByAuth0 } from './modules/profile/actions/user.js';

export default class Auth0LockService extends EventEmitter {
  constructor(clientId, domain, options) {
    super();

    // Configure Auth0
    this.lock = new Auth0Lock(clientId, domain, options);

    // Add callback for lock `authenticated` event
    this.lock.on('authenticated', this._doAuthentication.bind(this));

    // Add callback for lock `authorization_error` event
    this.lock.on('authorization_error', this._authorizationError.bind(this));

    // binds login functions to keep this context
    this.login = this.login.bind(this);
  }

  _doAuthentication(authResult) {
    console.log('AUTH SUCCESS: ', authResult);

    // Saves the user token
    this.setToken(authResult.idToken);
    store.dispatch({ type: 'AUTHENTICATION_SUCCESS', payload: { id_token: authResult.idToken } });
    console.log('current window location: ', window.location);
    axios.defaults.headers.common['authorization'] = authResult.idToken;
    store.dispatch(readUserByAuth0(authResult.idToken));
  }

  _authorizationError(error) {
    // Unexpected authentication error
    console.log('Authentication Error', error);
  }

  login(redirectUrl) {
    this.lock.show();
  }

  setProfile(profile) {
    // Saves profile data to localStorage
    localStorage.setItem('profile', JSON.stringify(profile));

    // Triggers profile_updated event to update the UI
    this.emit('profile_updated', profile);
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem('profile');
    return profile ? JSON.parse(localStorage.profile) : {};
  }

  getMongoProfile() {
    // Retrieves the profile data from mongodb
    store.dispatch();
  }

  setToken(idToken) {
    // Saves user token to localStorage
    localStorage.setItem('jwt', idToken);
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem('jwt');
  }

  logout() {
    // Clear user token and profile data from localStorage
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
  }
}
