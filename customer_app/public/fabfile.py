from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists

import sys, os

abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)

# --------------------------------------------
# Akly Platform AWS staging Machines
# --------------------------------------------

class FabricException(Exception):
    pass

def dev():
    print "Connecting to Akly AWS client app dev Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.development_env = 'dev'

    env.key_filename = abspath('../../akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '54.169.154.183'
    ]
    env.app_path = '/home/ubuntu/aklyplatform_customer_app'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets'
    ]
    return


def stage():
    print "Connecting to Akly AWS client app stage Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.development_env = 'stage'

    env.key_filename = abspath('../../akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '13.228.28.114'
    ]
    env.app_path = '/home/ubuntu/aklyplatform_customer_app'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets'
    ]
    return

def prod():
    print "Connecting to Akly AWS client app stage Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '16.04'
    env.development_env = 'prod'

    env.key_filename = abspath('../../akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '54.169.96.216',
        # '54.179.179.212',
        # '54.254.135.28'
    ]
    env.app_path = '/home/ubuntu/customer_app'

    env.graceful = False
    env.home = '/home/ubuntu'

    env.rsync_exclude = [
        '.git',
        '.gitignore',
        '*.pyc',
        '*.pem',
        '.npm',
        '*.py',
        '*.conf',
        '/fonts',
        '/stylesheets'
    ]
    return

# --------------------------------------------
# Installing Akly Staging Platform
# --------------------------------------------

def install():
    if env.setup:
        print 'Start installing Akly Platform'
        install_nginx()
        nginx_config()

def install_nginx():
    print 'Installing NGINX'
    sudo('apt-get update')
    sudo("apt-get install -y nginx")
    return

def nginx_config():
    default_config='/etc/nginx/sites-enabled/default'
    if exists(default_config):
        sudo('rm /etc/nginx/sites-enabled/default')
        print 'Deleted NGINX default config'

    print 'Install NGINX config'
    put('%s' % (env.nginx_config), '/etc/nginx/sites-enabled/', use_sudo=True)

    print 'Restarting NGINX'
    sudo("service nginx stop")
    sudo("service nginx start")
    return

def uninstall_nginx():
    print 'Uninstalling NGINX'
    sudo('apt-get purge nginx nginx-common')
    sudo('apt-get autoremove')
    return


def deploy():
    #generate_code_base()
    sync_code_base()

def generate_code_base():

    if env.development_env == 'stage':
        print 'Generating stage code base'
        local('cd ..;npm install;npm run stage;');
    elif env.development_env == 'dev':
        print 'Generating dev code base'
        local('cd ..;npm install;npm run dev;');
    else:
        print 'Generating prod code base'
        local('cd ..;npm install;npm run prod;');
    return

def sync_code_base():
    print 'Syncing code base'
    rsync_project(env.app_path, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    return
def test2():

    print(abspath('..'))

# --------------------------------------------
# fabric script end
# --------------------------------------------
