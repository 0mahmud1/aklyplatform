import './toastrConfig';
import 'bootstrap';
import 'select2';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import axios from 'axios';

import store from './store';
import Routes from './routes';

import './public/stylesheets/css/main.css';

axios.defaults.baseURL = process.env.API_URL;
axios.defaults.headers.common['authorization'] = store.getState().auth.token ? 'Bearer ' + store.getState().auth.token : null;

window.mapsLoaded = () => {
  console.log('google map loaded', google);
  store.dispatch({ type: 'GOOGLE_LOADED' });
};

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('app')
);
