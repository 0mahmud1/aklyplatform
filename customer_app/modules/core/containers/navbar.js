import React, {
  Component,
  PropTypes,
} from 'react';
import { connect } from 'react-redux'

import NavBarCAComponent from  '../layouts/navbar'

class NavBarCAContainer extends Component {
  render() {
    return (
      <NavBarCAComponent {...this.props}/>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    deliveries: store.onDemand.deliveries
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    logout: () => dispatch({type:'USER_LOGOUT'})
  }
};


export default connect(mapStateToProps,mapDispatchToProps)(NavBarCAContainer);
