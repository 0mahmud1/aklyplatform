import axios from 'axios';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js';

export function getFoods() {
  return function (dispatch) {
    dispatch({ type: 'GET_FOOD_PENDING' });
    axios.get(
      API.FOODS
    ).then((response) => {
      console.log('get food... : ', response);
      dispatch({ type: 'GET_FOOD_RESOLVED', payload: response });
      //toastr.success('Food List Loaded');
    })
    .catch((err) => {
      dispatch({ type: 'GET_FOOD_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getTags() {
  return function (dispatch) {
    dispatch({ type: 'GET_TAGS_PENDING' });
    axios.get(
      API.TAGS
    ).then((response) => {
      dispatch({ type: 'GET_TAGS_RESOLVED', payload: response });
      //toastr.success('tags List Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_TAGS_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}


