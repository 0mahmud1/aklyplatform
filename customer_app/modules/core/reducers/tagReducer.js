import _ from 'lodash'
import store from '../../../store.js'
let defaultState = {
  filterTagList: [],
  foodTags: []
};
const tagReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_FILTER_TAGLIST':
      let selectedTags = [...state.filterTagList];
      selectedTags.push(findTag(state.foodTags, action.payload));
      return Object.assign({}, state, { filterTagList: selectedTags });

    case 'LIST_ALL_FOOD_TAGS':
      let allTags = listAllTags(store.getState().foods.foodList);
      console.log('ALL TAGS:    ', allTags);
      console.log(store);
      return Object.assign({}, state, { foodTags: allTags });

    case 'REMOVE_FILTER_TAGLIST' :
      let rmTags = [...state.filterTagList];
      _.remove(rmTags, function (n) {
        return n._id === action.payload;
      });

      return Object.assign({}, state, { filterTagList: rmTags });

    case 'SELECT_ALL_TAG':
      return Object.assign({}, state, { filterTagList: action.payload ? state.foodTags : [] });

    case 'RESET_FILTER':
      return Object.assign({}, state, { filterTagList: [] });

    default :
      return state;
  }
};

export default tagReducer;

function listAllTags(items) {
  let tags = _.flatten(items.map(item => item.tags));
  return _.uniqBy(tags, tag => tag._id);
}

function findTag(list, _id) {
  return _.find(list, (o) => {
    return o._id === _id;
  });
}
