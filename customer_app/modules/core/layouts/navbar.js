import React, { Component, PropTypes } from 'react';
import { Link, IndexLink, browserHistory } from 'react-router';
import _ from 'lodash'


import NavbarLogoComponent from '../components/logo/logo-navbar.js'
import InRoomDineIconComponent from '../components/icon/in-room-dining.js'
import FoodIconComponent from '../components/icon/food.js'
import InfoIconComponent from '../components/icon/info.js'
import TelephoneIconComponent from '../components/icon/telephone.js'
import CartIconComponent from '../components/icon/cart.js'

import Auth0Lock from 'auth0-lock';

class NavBarCAComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      itemOrdered: localStorage.getItem('onDemand') ? _.sumBy(this.props.deliveries, (item)=>{return item.quantity}) : 0
    }
  }

  componentDidUpdate(prevProps, prevState) {
     if(prevProps.deliveries !== this.props.deliveries)
     {
         let total = _.sumBy(this.props.deliveries, (item)=>{
           return item.quantity
         })
         this.setState({
             itemOrdered: total
         })
     }
  }

  componentDidMount() {
    this.modalJQuery();
  }



  modalJQuery(){
    $('a[name=modal]').click(function (e) {
      e.preventDefault();
      if (localStorage.getItem('jwt')) {
        var id = $('a[name=modal]').attr('href');
        $(id).css({visibility: 'visible'});
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        $('#mask').css({width: maskWidth, height: maskHeight});
        $('#mask').fadeIn(10);
        $('#mask').fadeTo(10, 0.70);
        var winH = $(window).height();
        var winW = $(window).width();
        $(id).css('top', parseInt(winH / 2 - 200) + $(document).scrollTop());
        $(id).css('left', winW / 2 - $(id).width() / 2);
        $(id).fadeIn(800);
      }
    });

    //if close button is clicked
    $('.close').click(function (e) {
      e.preventDefault();
      $('#mask').hide();
      $('.window').hide();
      $('.popup').hide();
    });

    $('a[name=startBMI]').click(function (e) {
      $('#mask').hide();
      $('.window').hide();
      $('.popup').hide();
    });

    $('#mask').click(function (e) {
      e.preventDefault();
      $('#mask').hide();
      $('.window').hide();
      $('.popup').hide();
    });
  }

  logout() {
    this.props.logout();
    localStorage.removeItem('jwt');
    localStorage.removeItem('userProfile');
    localStorage.removeItem('auth0Object');
    browserHistory.push('/');
  }

  login() {
    let options = {
      additionalSignUpFields: [
        {
          name: 'full_name',
          placeholder: 'Enter your full name',
          icon: "/icons/user.svg"
        },
      ],
      auth: {
        params: {scope: 'openid email roles app_metadata'},
        redirectUrl: process.env.BASE_URL + window.location.pathname,
        responseType: 'token',
      },
      rememberLastLogin: false,
      theme: {
        logo: process.env.AKLY_LOGO,
        labeledSubmitButton: false
      },
      avatar: null
    };

    let auth = new Auth0Lock(process.env.AUTH0_CLIENT_ID, process.env.AUTH0_BASE_URL, options);
    console.log('here am i');

    console.log('auth0 runtime: ', options.auth, process.env.AUTH0_BASE_URL);

    if (!localStorage.getItem('jwt')) {
      auth.show();
    }
  }

  generateCustomerGreeting() {
    return this.props.auth && this.props.auth.user ?
      <li className="dropdown canavbar-dropdown">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">
          <span className='canavbar-propic'><img src='/images/profilephoto.jpg' alt='profile image'/></span>
          Hi, {this.props.auth.user.email}
          <span className="caret ca-caret"></span></a>
        <ul className="dropdown-menu canavbar-dropdown-menu">
          {/*<li><a href="#">My Profile</a></li>
           <li><a href="#">My Accounts</a></li>
           <li><a href="#">Settings</a></li>
           <li><a href="#">Help</a></li>*/}
          <li className='arrow-up'></li>

          <li><Link to='/profile'>Profile</Link></li>
          <li><Link to='/profile/settings'>Settings</Link></li>
          <li><Link to='/orders'>Order List</Link></li>
          <li><a href="#" onClick={this.logout.bind(this)}>Logout</a></li>
        </ul>
      </li> :
      <li className="dropdown canavbar-dropdown">
        <a href="#" className="dropdown-toggle temp-login" data-toggle="dropdown" role="button" aria-haspopup="true"
           aria-expanded="false">
          <button className="btn butn butn_default" onClick={() => this.login()}>Login</button>
        </a>
      </li>
  }

  render() {
    return (
      <div className='canavbar'>
        <nav className="navbar navbar-default canavbar-layout">
          <div className="container ca-container">
            <div className="navbar-header">
              <NavbarLogoComponent/>
            </div>
            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav canavbar-nav canavbar-nav-mp">
                <li>
                  <IndexLink to='/' activeClassName='current-link'><InRoomDineIconComponent /> Menu</IndexLink>
                </li>
                <li className="dropdown canavbar-dropdown">
                  {/*<a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"*/}
                     {/*aria-expanded="false">*/}
                    <Link to={`/subscription`} activeClassName='current-link'><FoodIconComponent /> Meal Plan</Link>
                  {/*</a>*/}
                  {/*<ul className="dropdown-menu canavbar-dropdown-menu">*/}
                    {/*/!*<li><a href="#">My Profile</a></li>*/}
                     {/*<li><a href="#">My Accounts</a></li>*/}
                     {/*<li><a href="#">Settings</a></li>*/}
                     {/*<li><a href="#">Help</a></li>*!/*/}
                    {/*<li className='arrow-up'></li>*/}
                    {/*<li><Link to={`/subscription`}>Akly Meal</Link></li>*/}
                  {/*</ul>*/}
                </li>
                {/* <li><Link to={`/subscription`} activeClassName='current-link'><FoodIconComponent /> Meal Plan</Link></li> */}
                <li><a href='#popup1' name='modal' onClick={() => this.login()}><InfoIconComponent />Personalized</a></li>
                <li><a href="#"><TelephoneIconComponent /> Contact</a></li>
              </ul>
              <ul className="nav navbar-nav navbar-right canavbar-nav-right">
                <li>
                    {this.cartIcon()}
                </li>
                {this.generateCustomerGreeting()}

              </ul>
            </div>
          </div>
        </nav>
        <div id='popup1' className='popup mainmodal'>
          <a href='javascript:void(0);' className='close'></a>
          <h3>Get your personalised meal plan</h3>
          <p>
            The Akly recommendation engine takes in your basic vitals and recommends a tailored weight objective for
            you. Once your weight objective is determined, Akly recommends tailored meal plans for you
          </p>
          <Link to='/bmi' name='startBMI' className='button'>Get started</Link>
        </div>
        <div id='mask' className='popup_mask'></div>
      </div>
    )
  }

    warning(){
      if(this.state.itemOrdered === 0){
        alert('Select atleast one item to proceed')
      }
    }

  cartIcon() {
        if(this.state.itemOrdered !==0){
            return (
                <Link to={`/summary`}>
                  <CartIconComponent />
                  <span className="badge ca-badge">{this.state.itemOrdered}</span>
                </Link>
            )
        } else {
            return(
                <Link to={`/`} onClick={this.warning.bind(this)}>
                  <CartIconComponent />
                </Link>
            )
        }
    }
}

export default NavBarCAComponent;
