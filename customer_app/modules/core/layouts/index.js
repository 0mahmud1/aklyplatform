import React, { Component, PropTypes } from 'react';
import NavBarCAContainer from '../containers/navbar';

class CoreLayoutCA extends Component {
  static contextTypes = {
    router: PropTypes.object,
  };

  render() {
    console.log('CoreLayoutCA...render ', this.props, ' ,,,');
    let children = null;
    if (this.props.children) {

      children = React.cloneElement(this.props.children, {
        auth0Lock: this.props.routes[1].auth0Lock //sends auth instance to children
      })
      console.log('cccc ', children)
    }
    return (
      <div className="customer_app fluid-lt">

        <div className='row'>
          <div className='col-sm-12 col-md-12 col-lg-12'>
            <NavBarCAContainer {...this.props} auth0Lock={this.props.routes[1].auth0Lock} />
            {children}
          </div>
        </div>
      </div>

    )
  }
}

CoreLayoutCA.propTypes = {
  // This component gets the task to display through a React prop.
  // We can use propTypes to indicate it is required
  children: PropTypes.element.isRequired
}

export default CoreLayoutCA;
