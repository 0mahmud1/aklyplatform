import React, {
  Component,
  PropTypes,
} from 'react';
import ReactDatePicker from 'react-datepicker'
//import {getAllDaysOfNextMonth} from '../../libs'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'

class DatePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: moment()
        };

        this.handleChange = this.handleChange.bind(this);
        this.isFuture = this.isFuture.bind(this);
  }

  handleChange(date) {
    console.log('datepicker: ', date, this)
    this.setState({
      date: date
    });
    this.props.onChange(date)
  }

  isFuture(date){
    return moment().diff(date, 'days') < 1
  }

  render() {
    return (
      <ReactDatePicker startDate={moment()} filterDate={this.isFuture} selected={this.state.date} onChange={this.handleChange}/>
      /*<ReactDatePicker
        selected={this.state.date}
        selectsStart  startDate={moment()}
        endDate={moment(new Date('2016-12-31'))}
        onChange={this.handleChange.bind(this)} />*/
    );
  }
}
export default DatePicker;



