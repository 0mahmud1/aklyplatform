'use strict'

import React, {Component, PropTypes} from 'react'

class TickIconComponent extends Component {
	render() {
		return (
            <div className='tick_icon'>
                <svg viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" onClick={this.props.clickTickIcon}><path d="M13 0C5.82 0 0 5.82 0 13s5.82 13 13 13 13-5.82 13-13S20.18 0 13 0zm-2.647 20L4 13.894 5.866 12.1l4.487 4.3 9.78-9.4L22 8.806 10.353 20z" fill={this.props.fillColor} fillRule="evenodd"/></svg>
            </div>
		)
	}
}

export default TickIconComponent