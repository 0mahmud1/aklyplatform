'use strict'

import React, {Component, PropTypes} from 'react'
import Collapse from 'react-collapse'
import SubscriptionCollapsedComponent from '../mealplan/SubscriptionCollapsed'

class SubscriptionCollapseComponent extends Component {
  render() {
    return (
      <Collapse isOpened={this.props.collapsed ? true: false}>
        <SubscriptionCollapsedComponent {...this.props}/>
      </Collapse>
    )
  }
}

export default SubscriptionCollapseComponent