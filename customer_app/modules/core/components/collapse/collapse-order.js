'use strict'

import React, {Component, PropTypes} from 'react'
import Collapse from 'react-collapse'

class CollapseOrderComponent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Collapse isOpened={this.props.collapsed ? true: false}>
         <div className='od-orders'>
           {this.props.deliveries.meals.map(meal => {
             return <div key={meal.item._id} className='od-singleorder'>
               <div className='od-singleorder-name'>
                 <p>{meal.item.name}</p>
               </div>
               <div className='od-singleorder-quantity'>
                 <p>{meal.quantity}</p>
               </div>
             </div>
           })}

               
             {/*<div className='od-singleorder'>
                 <div className='od-singleorder-name'>
                    <p>Spicy Chicken Burger with Cheese</p>
                </div>
                <div className='od-singleorder-quantity'>
                    <p>20</p>
                </div>
            </div> */}
         </div>
      </Collapse>
    )
  }
}

export default CollapseOrderComponent