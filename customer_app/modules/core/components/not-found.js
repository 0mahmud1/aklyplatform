'use strict'

import React from 'react';

const NotFoundComponent = () => {
    return (
        <div className='mp_wrapper akly_mp' data-hook='mainPageWrapper'>
           <div className='mp_content'>
               <div className='mp_heading_mb'>
                   <div className='row'>
                       <div className='col-lg-12'>
                       </div>
                   </div>
               </div>


               <div className='mp_body'>
                   <div className='row'>
                       <div className='col-lg-12'>
                           <div className='notfound_message'>
                               <h2>Sorry, page not found..</h2>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    );
};

export default NotFoundComponent;