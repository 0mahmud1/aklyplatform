import React from 'react';

const NotFound = ({className, style, text}) => {
    return (
        <div className={'no-address-text ' + className}>
            <p style={style}>{text}</p>
        </div>
    );
}

NotFound.propTypes = {
    className: React.PropTypes.string,
    style: React.PropTypes.object,
    text: React.PropTypes.string
};

NotFound.defaultProps = {
    className: ''
};

export default NotFound;