import React from 'react';

const Button = ({id, className, style, onClick, text}) => {
    return <button id={id} className={'btn butn ' + className} style={style} onClick={onClick}>{text}</button>
};

Button.propTypes = {
    id: React.PropTypes.string,
    className: React.PropTypes.string,
    style: React.PropTypes.object,
    onClick: React.PropTypes.func,
    text: React.PropTypes.string
};

Button.defaultProps = {
    className: 'butn_default',
    id: null
};

export default Button;