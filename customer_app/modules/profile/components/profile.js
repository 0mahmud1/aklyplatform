'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import moment from 'moment'
import toastr from 'toastr'
import ReactDOM from 'react-dom'

import CollapseOrderComponent from '../../core/components/collapse/collapse-order.js'

class ProfileComponentCA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsedState: {},
      clicked: false,
      file: null,
      imagePreviewUrl: this.props.auth.userProfile && this.props.auth.userProfile.avatar ? this.props.auth.userProfile.avatar.location +'?'+ Math.random() : null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.auth && nextProps.auth.userProfile && nextProps.auth.userProfile.avatar && nextProps.auth.userProfile.avatar.location) {
      console.log('nextProps<<<', nextProps.auth.userProfile.avatar.location);
      if (this.state.imagePreviewUrl != nextProps.auth.userProfile.avatar.location) {
        this.setState({
          imagePreviewUrl: nextProps.auth.userProfile.avatar.location,
        });
      }
      console.log('nextProps>>>', this.state.imagePreviewUrl);
    }
  }
  shouldComponentUpdate(nextProps,nextState){
    if(this.state.imagePreviewUrl !== nextState.imagePreviewUrl){
      console.log('shouldComponentUpdate>>>>>');
      return true;
    }

    console.log('shouldComponentUpdate  false');
    return false;

  }

  orderCollapse() {
    this.setState({clicked: !this.state.clicked});
  }
  changeCollapseState(order) {
    console.log('clicking', this, order)

    let newState = Object.assign({}, this.state.collapsedState);
    if(_.isUndefined(newState[order._id])) {
      newState[order._id] = true
    } else {
      newState[order._id] = !newState[order._id]
    }

    this.setState({
      collapsedState: newState
    })

    console.log('**', this.state.collapsedState);
  }

  handleSubmit(event) {
    event.preventDefault()
    let _this = this
    // console.log('this context',_this)
    // if(_this.refs.phone.value < 0 || _this.refs.phone.value === undefined){
    //   toastr.warning('Input Valid Phone Number')
    //   return
    // }
    // if(_this.refs.password.value === undefined){
    //   toastr.warning('Input Valid Password')
    //   return
    // }
    // if(_this.refs.height.value < 0){
    //   toastr.warning('Input Valid Height')
    //   return
    // }
    // if(_this.refs.weight.value < 0){
    //   toastr.warning('Input Valid Weight')
    //   return
    // }
    // if(_this.refs.age.value < 0){
    //   toastr.warning('Input Valid Age')
    //   return
    // }
    let user = {
      profile: {
        phone: _this.refs.phone.value,
        height: _this.refs.height.value,
        weight: _this.refs.weight.value,
        age: _this.refs.age.value
      }
    }

    console.log('update to be made: ', user.profile)

    _this.props.updateUser(_this.props.auth.userProfile._id, user, () => {

    })
  }

  handleChange(event) {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];
    console.log("File Size =>>>",(file.size/1024/1024) , " MB");
    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    }

    reader.readAsDataURL(file);
  }

  handleSubmitPicture(event) {
    event.preventDefault();
    // create a FormData object
    const userType = 'Customer';
    let user = new FormData();
    let _this = this;
    user.append('type', userType);
    if (this.state.file)
      user.append('file', this.state.file);

    // call the action
    this.props.changePicture(this.props.auth.userProfile._id,user, function (err, data) {
        if (data) {
          console.log('imagePreviewUrl<<<', data.data.user.avatar.location, _this.props.auth.userProfile.avatar.location);
        }
    });

     //Clear form
    ReactDOM.findDOMNode(this.refs.file).value = '';
    this.setState({
      file: null,
      imagePreviewUrl: null
    });
  }

  componentDidMount() {
    $('.js-example-select').select2({
      minimumResultsForSearch: -1
    });
  }

  pastOrders() {
    return <div className='caprof-pstorders'>
      <h2 className='caprof-pstorders-heading'>Past Orders</h2>
      <div className='caprof-ordertbl'>
        <table className="table caprof-table">
          <thead>
          <tr>
            <th>Order Number</th>
            <th>Date</th>
            <th>Type</th>
            <th>Amount</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          {this.generateOnDemandOrderList()}
          </tbody>
        </table>
      </div>
    </div>
  }

  generateOnDemandOrderList() {
    let toggleArrow;
    let collapsed = this.state.clicked;
    collapsed ? toggleArrow = 'icon-collapsed' : toggleArrow = 'icon-not-collapsed';
    return this.props.orders.orderList.map(order => {
      return <tr key={order._id}>
        <td>{order.orderRefId}</td>
        <td>{moment(order.createdAt).format('MMM D')}</td>
        <td className='order-type'>
          <span>{order.orderType}</span>
          <CollapseOrderComponent deliveries={order.deliveries[0]} collapsed={this.state.collapsedState[order._id]}/>
        </td>
        <td>QR {order.total}</td>
        <td><span className={'collapse-indicator ' + toggleArrow}
                  onClick={this.changeCollapseState.bind(this, order)}></span></td>
      </tr>
    })
  }

  profileSummary() {
    let user = this.props.auth.userProfile;
    let imageSource = this.state.imagePreviewUrl || '/images/profilePhoto.jpg';
    console.log('userProfile', user)

    return <div className="media caprof-imgname">
      <div className="media-left caprof-img">
          <img className="media-object" src={imageSource} alt='profile photo' width='150px'/>
      </div>
      <div className="media-body caprof-name">
        <h1 className="media-heading">{user.name ? user.name : user.email}</h1>
        <p className='caprof-location'>Doha, Qatar</p>
      </div>
      <form className='mt-15' onSubmit={this.handleSubmitPicture.bind(this)} encType='multipart/form-data'>
        <div className='caprof-uploadbtn'>
          <input
            type='file'
            accept="image/*"
            ref='file'
            onChange={this.handleChange.bind(this)} />
        </div>
        <div className='caprof-updatebtn'>
          <button type='submit' className='btn butn butn_default'>Update</button>
        </div>
      </form>
    </div>
  }

  profileDetails() {
    let user = this.props.auth.userProfile
    return <div className='caprof-acdtls-content'>
      <h4 className='caprof-acdtls-heading'>Account Details</h4>
      <div className='caprof-acdtls-form'>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div className='ca_geninput'>
            <div className="form-group no_mb">
              <div className="input-group ca_input_group">
                <div className="input-group-addon ca_input-group_addon"><img className="" src="/images/mail.svg"
                                                                             alt="email icon"/></div>
                <input type="email" className="form-control ca_frmctrl" readOnly="readOnly" value={user.email} ref='email'
                       placeholder="Email"/>
              </div>
            </div>
          </div>

          <div className='ca_geninput'>
            <div className="form-group no_mb">
              <div className="input-group ca_input_group">
                <div className="input-group-addon ca_input-group_addon"><img className=""
                                                                             src="/images/android.svg"
                                                                             alt="Phone Number"/></div>
                <input type="number"
                       className="form-control ca_frmctrl"
                       ref='phone'
                       defaultValue={user.phone ? user.phone : ''}
                       placeholder="Phone Number"/>
              </div>
            </div>
          </div>

          {/*<div className='ca_geninput'>
           <div className="form-group no_mb">
           <div className="input-group ca_input_group">
           <div className="input-group-addon ca_input-group_addon"><img className=""
           src="/images/android.svg"
           alt="Phone Number"/></div>
           <input type="password" className="form-control ca_frmctrl" id="exampleInputAmount"
           ref='password' placeholder="Password"/>
           </div>
           </div>
           </div>*/}

          <div className='ca_geninput'>
            <div className="form-group no_mb">
              <div className="input-group ca_input_group">
                <div className="input-group-addon ca_input-group_addon"><img className=""
                                                                             src="/images/android.svg"
                                                                             alt="Phone Number"/></div>
                <input type="number"
                       className="form-control ca_frmctrl"
                       ref='height'
                       defaultValue={user.height ? user.height : ''}
                       placeholder="Height"/>
              </div>
            </div>
          </div>

          <div className='ca_geninput'>
            <div className="form-group no_mb">
              <div className="input-group ca_input_group">
                <div className="input-group-addon ca_input-group_addon"><img className=""
                                                                             src="/images/android.svg"
                                                                             alt="Phone Number"/></div>
                <input type="number"
                       className="form-control ca_frmctrl"
                       ref='weight'
                       defaultValue={user.weight ? user.weight : ''}
                       placeholder="Weight"/>
              </div>
            </div>
          </div>

          <div className='ca_geninput'>
            <div className="form-group no_mb">
              <div className="input-group ca_input_group">
                <div className="input-group-addon ca_input-group_addon"><img className=""
                                                                             src="/images/android.svg"
                                                                             alt="Phone Number"/></div>
                <input type="number"
                       className="form-control ca_frmctrl"
                       defaultValue={user.age ? user.age : null}
                       ref='age'
                       placeholder="Age"/>
              </div>
            </div>
          </div>

          <div className="ca_geninput caprof-select2">
            <div className="form-group">
              <div className="mp_chkoutconfirm_select2">
                <select ref='payment' className="js-example-select form-control ca_select2">
                  <option value="cash">Fitness Objectives</option>
                </select>
              </div>
            </div>
          </div>

          <div className='caprof-savebtn'>
            <button className='btn butn-ocnf'>Save</button>
          </div>
        </form>
      </div>
    </div>
  }

  render() {
    return (
      <div className='caprof container ca-container'>
        <div className='caprof-dtls'>
          <div className='caprof-orders'>
            <div className='caprof-orders-content'>
              {this.profileSummary()}
              {this.pastOrders()}
            </div>
          </div>
          <div className='caprof-acdtls'>
            {this.profileDetails()}
          </div>
        </div>
      </div>
    )
  }
}

export default ProfileComponentCA
