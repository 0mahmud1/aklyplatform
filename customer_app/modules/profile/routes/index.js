import React from 'react'
import { Route, IndexRoute } from 'react-router' 
import ProfileContainer from '../containers/profile.js'
import SettingContainer from '../containers/settings.js'
import * as AuthService from '../../../services/auth.js'


export default function(){
    return(
        <Route path='profile' onEnter={AuthService.isAuthorization}>
            <IndexRoute component={ProfileContainer}/>
            <Route path='settings' component={SettingContainer}/>
        </Route>
    )
}