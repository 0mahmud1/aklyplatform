import * as constants from '../constants.js';
import jwt_decode from 'jwt-decode';

/**
 * All reducers get two parameters passed in, state and action that occurred
 *  > state isn't entire apps state, only the part of state that this reducer is responsible for
 */

// "state = null" is set so that we don't throw an error when app first boots up

const defaultState = {
  token: localStorage.getItem('jwt') ? localStorage.getItem('jwt') : null,
  loggingIn: false,
  user: localStorage.getItem('jwt') ? jwt_decode(localStorage.getItem('jwt')) : null,
  userProfile: localStorage.getItem('userProfile') ? JSON.parse(localStorage.getItem('userProfile')) : null,
};


const authenticateReducer = (state = defaultState, action) => {
  switch (action.type) {
    case constants.AUTHENTICATION_PENDING:
      return Object.assign({}, state, {
        loggingIn: true,
      });

    case constants.AUTHENTICATION_SUCCESS :
      return Object.assign({}, state, {
        token: (action.payload.id_token) ? action.payload.id_token : null,
        user: (action.payload.id_token) ? jwt_decode(action.payload.id_token) : null,
        loggingIn: false,
      });

    case constants.AUTHENTICATION_REJECTED:
      return Object.assign({}, state, {
        loggingIn: false,
      });

    case constants.USER_LOGOUT:
      localStorage.removeItem('userProfile');
      return Object.assign({}, state, {
        token: null,
        user: null,
        userProfile: null,
      });

    case 'GOOGLE_LOADED':
      return Object.assign({}, state, { googleLoaded: true });

    case 'USER_READ_PENDING':
      return Object.assign({}, state, state);
    case 'USER_READ_RESOLVED':
      return Object.assign({}, state, { userProfile: action.payload });
    case 'USER_READ_REJECTED':
      return Object.assign({}, state, state);
      
    case 'CUSTOMER_CHANGE_PASSWORD_PENDING':
      return state;

    case 'CUSTOMER_CHANGE_PASSWORD_SUCCESS':
      return state;

    case 'CUSTOMER_CHANGE_PASSWORD_REJECT':
      return state;

    case 'CUSTOMER_CHANGE_PICTURE_PENDING':
      return state;

    case 'CUSTOMER_CHANGE_PICTURE_SUCCESS':
      return state;

    case 'CUSTOMER_CHANGE_PICTURE_REJECT':
      return state;

    default :
      return state;
  }
};

export default authenticateReducer;
