import React, {
  Component,
  PropTypes,
} from 'react';

import ProfileComponentCA from '../components/profile';
import ReactDOM from 'react-dom'
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { getOrdersOfCustomer } from '../../orders/actions/order.js';
import { updateUser , changePicture, readUserByAuth0 } from '../actions/user';


class ProfileContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      loadder : false
    }
  }
  changePicture(_id, data, cb){
    let _this = this;
    console.log('loader',this.state.loadder);
      _this.setState({loadder : true});

      _this.props.changeProfilePicture(_id, data, function(err, data){
          if(data){
            console.log(data);
            console.log('state');
            _this.props.refreshCustomer(localStorage.jwt);
            _this.setState({loadder : false});
            cb(null , data);
          } else {
            console.log(err);
          }
      });
  }
  componentWillMount() {
    this.props.getOrders(this.props.auth.user.email);
  }

  insertLoader() {
    return (
      <div>
        <div className="spinner" style={{marginBottom: '15px'}}> </div>
        <p className='text-center fs-18'>Uploading Image. Please wait a while..</p>
      </div>

    );
  }

  render() {
    let shouldLoad = this.state.loadder;
    return (
      shouldLoad ? this.insertLoader() :
      <ProfileComponentCA {...this.props} changePicture={this.changePicture.bind(this)}/>
    );
  }
}
const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    orders: store.orders,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getOrders: (email) => dispatch(getOrdersOfCustomer(email)),
    updateUser: (_id, patch, cb) => dispatch(updateUser(_id, patch, cb)),
    changeProfilePicture: (_id,data,cb) => dispatch(changePicture(_id,data,cb)),
    refreshCustomer: (header) => dispatch(readUserByAuth0(header)),

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
