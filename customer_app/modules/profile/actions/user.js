import axios from 'axios';
import { browserHistory } from 'react-router';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js';

export function readUserByAuth0(header) {
  // REDUCER IN DASHBOARD MODULES/authenticate folder
  return function (dispatch) {
    //dispatch({type: 'testing'});
    //console.log(axiosHeader);
    axios.get(
      API.CUSTOMER_CHECK,
      {
        headers: {
          authorization: header,
        },
      }
    ).then((response) => {
      //console.log(response);
      dispatch({ type: 'USER_READ_RESOLVED', payload: response.data.data });
      localStorage.setItem('userProfile', JSON.stringify(response.data.data));
      //toastr.success('customer loaded');
    }).catch((err) => {
      dispatch({ type: 'USER_READ_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function updateUser(_id, patch, cb) {
  // REDUCER IN DASHBOARD MODULES/authenticate folder
  return function (dispatch) {
    axios.put(
      'api/customers/' + _id,
      patch
    ).then((response) => {
      //console.log(response);
      dispatch({
        type: 'CUSTOMER_UPDATE_RESOLVED',
        payload: response.data.data,
      });
      localStorage.setItem('userProfile', JSON.stringify(response.data.data));
      toastr.success('customer updated successfully');
      if (cb) {
        cb();
      }
    }).catch((err) => {
      dispatch({ type: 'CUSTOMER_UPDATE_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function changePassword(data) {
  return function (dispatch) {
    dispatch({ type: 'CUSTOMER_CHANGE_PASSWORD_PENDING' });
    axios.post(
      API.CHANGE_PASSWORD,
      data
    ).then((response) => {
      toastr.success('password changed');
      dispatch({ type: 'CUSTOMER_CHANGE_PASSWORD_SUCCESS' });
    }).catch((err) => {
      toastr.warning('Invalid password');
      dispatch({ type: 'CUSTOMER_CHANGE_PASSWORD_REJECT' });
    });
  };
}

export function changePicture(_id,data,cb) {
  return function (dispatch) {
    dispatch({ type: 'CUSTOMER_CHANGE_PICTURE_PENDING' });
    axios.put(
      API.CHANGE_PICTURE + '/'+_id,
      data
    ).then((response) => {
      toastr.success('picture changed');
      cb(null, response);
      dispatch({ type: 'CUSTOMER_CHANGE_PICTURE_SUCCESS' });
    }).catch((err) => {
      toastr.warning('Invalid picture');
      cb(err, null);
      dispatch({ type: 'CUSTOMER_CHANGE_PICTURE_REJECT' });
    });
  };
}
