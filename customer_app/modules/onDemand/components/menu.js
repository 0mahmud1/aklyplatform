'use strict'


import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import _ from 'lodash'
import LazyLoad from 'react-lazyload';
import Lightbox from 'react-image-lightbox';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default class MenuComponentCA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flip: {},
      shrink: false,
      nutritionImage: '',
      isOpen: false
    };

    this.hideLightbox = this.hideLightbox.bind(this);
  }

  showLightbox(food) {
    console.log('food.nutritionImage', food.nutritionImage);
    console.log('this.state.isOpen', this.state.isOpen);
    this.setState (
      {
        nutritionImage: food.nutritionImage,
        isOpen: true
      }
    );
  }

  hideLightbox() {
    this.setState(
      {
        nutritionImage: '',
        isOpen: false
      }
    )
  }

  componentDidUpdate(prevProps) {
    if(this.props.foods.foodList.length !== prevProps.foods.foodList.length || !this.props.tags.foodTags) {
      this.props.listAllFoodTags()
    }
  }

  flip(identifier) {
    let flipState = this.state.flip;
    flipState[identifier] = !this.state.flip[identifier]
    this.setState({flip: flipState});
  }

  shrinkSidebar() {
    this.setState({
      shrink: !this.state.shrink
    });
  }
  // nutrationModal() {
  //   return
  //   (
  //       <Rodal visible={this.state.modalShow} onClose={this.hideModal.bind(this)} height={500} width={300}>
  //         <div className='binary_prompt_content'>
  //           <div className='prompt_text'>
  //               <img src={this.state.nutrationImage} alt="add food" className="home-modal_img"/>
  //           </div>
  //         </div>
  //       </Rodal>
  //   )
  // }

  generateFoods() {
    let _this = this
    return this.props.foods.filteredFoodList.length > 0  ?
      this.props.foods.filteredFoodList.map((food) => {
        let existingFood = null
        if(_this.props.onDemand.deliveries) {
          existingFood = _.find(_this.props.onDemand.deliveries, function (f) {
            return f.item._id == food._id
          })
          //console.log('** ', existingFood)
        }
        return  <ReactCSSTransitionGroup
                  key={food._id}
                  transitionName="ondemand-menu"
                  transitionAppear={true}
                  transitionAppearTimeout={500}
                  transitionEnter={false}
                  transitionLeave={false}>
                  <div key={food._id} className='col-sm-6 col-md-4 col-lg-4 menu-shadow'>
                    <div className={`fcard fcard-od ${existingFood ? 'fcard-mod': ''}`}>
                      <div className={`fcard-od-3d ${this.state.flip[food._id] ? 'fcard-od-3d-flipped': ''} `}>
                        <div className='fcard-od-front'>
                          <button className='fadd' onClick={() => this.props.addFoodToCart(food)}>
                            <img src="/images/plus.svg" alt="add food"/>
                          </button>

                          <button className='finfo' onClick={this.showLightbox.bind(this, food)}>
                            <img src="/images/food-card/info.svg" alt="Food information"/>
                          </button>
                          <div className='fctrl'>
                            <button className='fdecre' onClick={() => this.props.reduceFoodQtyToCart(food)}>
                              <img src="/images/minus.svg" alt="decrement food"/>
                            </button>
                            <button className='fincre' onClick={() => this.props.addFoodToCart(food)}>
                              <img src="/images/plus-orange.svg" alt="increment food"/>
                              <span>Add</span>
                            </button>
                          </div>
                          <div className='fnumb'>
                            <p>{`${existingFood ? existingFood.quantity: ''}`}</p>
                            <p><img src='/images/bag.svg' alt='food selectd'/>In your aklybox</p>
                          </div>
                          <LazyLoad height={200}>
                            <img src={food.images[0]} alt={food.name} className='fimg'/>
                          </LazyLoad>

                          <div className='fdtl'>
                            <div className='fname'>
                              <p>{food.name}</p>
                            </div>
                            <div className='fcal'>
                              <img src='/images/bonfire.svg' alt='food calorie'/>
                              <span>{food.calorie}K</span>
                            </div>
                            <div className='fprice'>
                                <p><sup>QR</sup>{food.price}</p>
                          </div>
                          </div>
                          <div className='fcard-overlay'>
                          </div>
                        </div>
                        <div className='fcard-od-back'>
                          <div className='fcard-od-back-content'>
                            <div className='fcard-od-flipback'>
                              <button className='fcard-od-flipbackbtn' onClick={this.flip.bind(this, food._id)}>
                                <img src='/images/food-card/info-red.svg' alt='red info icon'/>
                              </button>
                            </div>


                            <div className='fcard-od-exinfo'>
                              <p className='exinfo-heading'>
                                More Information
                              </p>

                              <ul className='exinfo-list'>
                                <li>{food.calorie}K <span>cal</span></li>
                                <li>{food.protein}g <span>protein</span></li>
                                <li>{food.fat}g <span>fat</span></li>
                                <li>{food.carbs}g <span>carb</span></li>
                              </ul>
                            </div>

                            {/*<div className='fcard-od-backbtn'>
                              <button className='btn' onClick={this.flip.bind(this,food._id)}><img
                                src='/images/food-card/back.svg' alt='flip back'/>Back
                              </button>
                            </div>*/}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </ReactCSSTransitionGroup>
      }) : null
  }

  getTagImage(tag) {
    //console.log('GTI', this, tag)
    if(!this.props.tags.filterTagList || this.props.tags.filterTagList.length == 0)
      return tag.images[1]

    return _.includes(this.props.tags.filterTagList.map(ft => ft._id) , tag._id) ? tag.images[0] : tag.images[1]
  }

  tagFilterChange(tag) {
    //console.log('*** ', )
    if(ReactDOM.findDOMNode(this.refs.tagSelectAll).checked) {
      ReactDOM.findDOMNode(this.refs.tagSelectAll).checked = false
      this.props.selectAllTag(false)
    }

    if(!this.props.tags.filterTagList || this.props.tags.filterTagList.length == 0) {
      this.props.addFilterTag(tag._id)
      this.props.filterFood();
      return
    }
    let isExist = _.find(this.props.tags.filterTagList, t => {
      return t._id == tag._id
    })

    if(isExist) {
      this.props.removeFilterTag(tag._id)
    } else {
      this.props.addFilterTag(tag._id)
    }

    this.props.filterFood();
  }

  generateFoodTags() {
    /**
     * to handle un-expected issue we're checking if tag is an object. there might have some data where tag inserted as string
     *
     */

    return this.props.foods.filteredFoodList.length > 0 && this.props.tags.foodTags ? this.props.tags.foodTags.map(tag => {
      return typeof tag === 'object' ?<li key={tag._id} className='camenu-categ' onClick={this.tagFilterChange.bind(this,tag)}>
        <div className='camenu-categ-container'>
          <span className='camenu-categ-icon'>
            <img src={this.getTagImage(tag)} alt=''/>
          </span>
          <span className='camenu-categ-name'>{tag.name}</span>
        </div>
      </li> : null
    }) : null
  }

  selectAllTag(event) {
    this.props.selectAllTag(event.target.checked)
    this.props.filterFood();
  }

  render() {
    let toggleIndicatorClass = this.state.shrink ? 'glyphicon-chevron-right' : 'glyphicon-chevron-left';
    let toggleSidebarCollpase = this.state.shrink ? 'camenu-sidebar-collapsed' : '';
    let toggleCardWrapper = this.state.shrink ? 'camenu-card-wrapper-collapsed' : '';

    const fallbackImage = 'http://umwnewmedia.org/wp-content/themes/professional-plus/assets/images/placeholder2.jpg';
    const isOpen = this.state.isOpen;
    return (
      <div className='camenu-wrapper container ca-container'>
        <div className={'camenu-sidebar ' + toggleSidebarCollpase}>
          <div className='camenu-sidebar-content'>
            <form>
              <div className='camenu-filter-box'>
                <div className='camenu-filter-box-content'>
                  <div className='camenu-selectall'>
                    <p className='camenu-selectall-heading'>
                      Filters
                    </p>
                    <div className="checkbox camenu-checkbox">
                      <label>
                        Select All
                        <input ref="tagSelectAll" defaultChecked="checked" onChange={this.selectAllTag.bind(this)} type="checkbox"/>
                      </label>
                    </div>
                  </div>
                  <div className='camenu-toggle-indicator'>
                    <span className='camenu-toggle-icon' onClick={this.shrinkSidebar.bind(this)}><i
                      className={'glyphicon ' + toggleIndicatorClass}></i></span>
                  </div>
                </div>
              </div>
              <ul className='camenu-categories'>
                  {this.generateFoodTags()}
              </ul>
            </form>
          </div>
        </div>

        <div className={'camenu-card-wrapper ' + toggleCardWrapper}>
          <div className='row'>
            {this.generateFoods()}
          </div>
        </div>
        {isOpen &&
          <Lightbox
              mainSrc={this.state.nutritionImage || fallbackImage}
              onCloseRequest={this.hideLightbox}
              enableZoom={false}
          />
        }

      </div>
    )
  }
}
