import React from 'react'
import { Route, IndexRoute } from 'react-router' 
import MenuContainer from '../containers/menu.js'



export default function(){
    return(
        <Route path='/'>
            <IndexRoute component={MenuContainer}/>
        </Route>
    )
}