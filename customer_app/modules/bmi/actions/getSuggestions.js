/**
 * Created by IamMohaiminul on 1/15/17.
 */
import axios from 'axios';

/*
 * Get meal plan suggestions
 */
export const getSuggestions = (_statisticId, userRecommend, callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_SUGGESTION_REQUEST' });
    axios.get(
      'api/bmi/suggestion/' + _statisticId,
      {
        params: {
          userRecommend,
        },
      }
    ).then((response) => {
      dispatch({ type: 'FETCH_SUGGESTION_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_SUGGESTION_FAILURE', payload: error.response });
      callback(error.response, null);
    });
  };
};
