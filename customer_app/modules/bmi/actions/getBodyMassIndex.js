/**
 * Created by IamMohaiminul on 1/15/17.
 */
import axios from 'axios';
import toastr from 'toastr';
import { browserHistory } from 'react-router';

/*
 * Get Body Mass Index (BMI)
 */
export const getBodyMassIndex = (callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_BODY_MASS_INDEX_REQUEST' });
    axios.get(
      'api/bmi'
    ).then((response) => {
      dispatch({ type: 'FETCH_BODY_MASS_INDEX_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_BODY_MASS_INDEX_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
