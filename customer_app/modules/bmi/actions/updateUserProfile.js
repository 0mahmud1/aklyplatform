/**
 * Created by IamMohaiminul on 1/15/17.
 */
import axios from 'axios';
import toastr from 'toastr';
import { browserHistory } from 'react-router';

/*
 * Update user profile data
 */
export const updateUserProfile = (userProfile, callback) => {
  console.log('updateUserProfile... ', userProfile);
  return function (dispatch) {
    dispatch({ type: 'UPDATE_USER_PROFILE_REQUEST' });
    axios.post(
      'api/bmi/user-profile',
      userProfile
    ).then((response) => {
      dispatch({ type: 'UPDATE_USER_PROFILE_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'UPDATE_USER_PROFILE_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
