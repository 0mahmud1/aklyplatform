/**
 * Created by IamMohaiminul on 1/15/17.
 */
import axios from 'axios';
import toastr from 'toastr';
import { browserHistory } from 'react-router';

/*
 * Get user profile data
 */
export const getUserProfile = (callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_USER_PROFILE_REQUEST' });
    axios.get(
      'api/bmi/user-profile'
    ).then((response) => {
      dispatch({ type: 'FETCH_USER_PROFILE_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_USER_PROFILE_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
