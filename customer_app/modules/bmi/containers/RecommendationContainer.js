/**
 * Created by IamMohaiminul on 1/15/17.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import RecommendationComponent from '../components/RecommendationComponent';

class RecommendationContainer extends Component {
  render() {
    return (
      <RecommendationComponent
        suggestions={this.props.suggestions}
        saveAllDays={this.props.saveAllDays}
        addToBox={this.props.addToBox} />
    );
  }
}

// Get apps store and pass it as props to RecommendationContainer
//  > whenever store changes, the RecommendationContainer will automatically re-render
// "store.userProfile" is set in reducers.js
function mapStateToProps(store) {
  return {
    suggestions: store.suggestions,
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    saveAllDays: (date) => dispatch({ type: 'SAVE_ALL_DAYS', payload: date }),
    addToBox: (mealPlan) => dispatch({ type: 'ADD_TO_BOX', payload: mealPlan }),
  };
};

// We don't want to return the plain RecommendationContainer (component) anymore,
// we want to return the smart Container
//  > RecommendationContainer is now aware of state and actions
export default connect(mapStateToProps, mapDispatchToProps)(RecommendationContainer);
