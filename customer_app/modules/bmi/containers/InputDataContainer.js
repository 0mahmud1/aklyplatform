/**
 * Created by IamMohaiminul on 1/15/17.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import toastr from 'toastr';

import { getUserProfile } from '../actions/getUserProfile.js';
import { updateUserProfile } from '../actions/updateUserProfile.js';

import InputDataComponent from '../components/InputDataComponent';

class InputDataContainer extends Component {
  componentWillMount() {
    this.props.getUserProfile(function (err, res) {
      if (err) {
        toastr.error(err.message, 'Body Mass Index');
        browserHistory.push('/');
      } else {
        //toastr.success('Fetch user profile data successfully', 'Body Mass Index');
      }
    });
  }

  render() {
    return (
      <InputDataComponent
        userProfile={this.props.userProfile}
        updateUserProfile={this.props.updateUserProfile} />
    );
  }
}

// Get apps store and pass it as props to InputDataContainer
//  > whenever store changes, the InputDataContainer will automatically re-render
// "store.userProfile" is set in reducers.js
function mapStateToProps(store) {
  return {
    userProfile: store.userProfile,
  };
}

// Get actions and pass them as props to to InputDataContainer
//  > now InputDataContainer has this.props.getUserProfile
//  > now InputDataContainer has this.props.updateUserProfile
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getUserProfile: getUserProfile,
    updateUserProfile: updateUserProfile,
  }, dispatch);
}

// We don't want to return the plain InputDataContainer (component) anymore,
// we want to return the smart Container
//  > InputDataContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(InputDataContainer);
