/**
 * Created by IamMohaiminul on 1/15/17.
 */

/*
* This reducer will always return an array of users no matter what
* You need to return something, so if there are no users then just return an empty array
*/
export const suggestionReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_SUGGESTION_REQUEST':
      return state;
    case 'FETCH_SUGGESTION_FAILURE':
      return state;
    case 'FETCH_SUGGESTION_SUCCESS':
      return action.payload.data;
    default:
      return state;
  };
};
