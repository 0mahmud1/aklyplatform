/**
 * Created by IamMohaiminul on 1/15/17.
 */

/*
* This reducer will always return an array of users no matter what
* You need to return something, so if there are no users then just return an empty array
*/
export const userProfileReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_USER_PROFILE_REQUEST':
      return state;
    case 'FETCH_USER_PROFILE_FAILURE':
      return state;
    case 'FETCH_USER_PROFILE_SUCCESS':
      return action.payload.data.userProfile;
    default:
      return state;
  };
};
