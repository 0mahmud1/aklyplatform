/**
 * Created by IamMohaiminul on 1/14/17.
 */
import React, { Component } from 'react';
import _ from 'lodash';
import { Link, browserHistory } from 'react-router';
import toastr from 'toastr';
import Rodal from 'rodal';

export default class ResultComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstTab: true,
      classification: '',
      recommend: '',
      visible: false
    };
  }

  toggleVisible() {
    this.setState( {
      visible: !this.state.visible
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.bodyMassIndex) {
      if (nextProps.bodyMassIndex.statistic
        && nextProps.bodyMassIndex.statistic.bodyMassIndex
        && nextProps.bodyMassIndex.statistic.bodyMassIndex.category
        && !_.isEqual(
          nextProps.bodyMassIndex.statistic.bodyMassIndex.category,
          this.state.classification
        )) {
        this.setState({
          classification: nextProps.bodyMassIndex.statistic.bodyMassIndex.category,
        });
      }

      if (nextProps.bodyMassIndex.statistic
        && nextProps.bodyMassIndex.statistic.recommendation
        && nextProps.bodyMassIndex.statistic.recommendation.value
        && !_.isEqual(
          nextProps.bodyMassIndex.statistic.recommendation.value,
          this.state.recommend
        )) {
        this.setState({
          recommend: nextProps.bodyMassIndex.statistic.recommendation.value,
        });
      }
    }
  }

  render() {
    if (!_.isEmpty(this.props.bodyMassIndex)) {
      let recommendMsg = this.props.bodyMassIndex.statistic.recommendation.value.toLowerCase();
      let bodyMassIndex = this.props.bodyMassIndex.statistic.bodyMassIndex;
      let dailyCalorieAllowance = this.props.bodyMassIndex.statistic.dailyCalorieAllowance;
      if (recommendMsg === 'maintain') {
        recommendMsg = 'maintain your current'
      }
      return (
        <div className='bmi-body'>
          <div className="header">
            <h2>Step 2</h2>
          </div>

          <div className="bmi-container">

            <div className="box">
              {/* LEFT-SIDE: SECOND TAB  */}
              <div className="box_tab active" id="view2">
      					{/* LEFT-SIDE: SECOND TAB: FIRST PAGE */}
      					<div className={this.state.firstTab ? 'box_tab_page active' : 'box_tab_page'} id="page1">
      						<div className="title">Your result</div>

      						<div className="circle">
      							<p className="txt1">BMI Result</p>
      							<p className="txt2">{bodyMassIndex.value}</p>
      							<p className="txt3">{bodyMassIndex.category}</p>
      						</div>
      					</div>

      					{/* LEFT-SIDE: SECOND TAB: SECOND PAGE */}
      					<div className={this.state.firstTab ? 'box_tab_page' : 'box_tab_page active'} id="page2">
      						<div className="title">Your result</div>
      						<p className="text4">BMI Classification</p>

          			  <ul className="your_results">
            				<li className={this.state.classification === 'Underweight' ? 'active' : ''}>
            					<span className="column1 text-effect">Underweight</span>
            					<span className="column2 text-effect">&lt; 18.5</span>
            				</li>
            				<li className={this.state.classification === 'Normal' ? 'active' : ''}>
            					<span className="column1 text-effect">Normal</span>
            					<span className="column2 text-effect">18.5 - 24.9</span>
            				</li>
            				<li className={this.state.classification === 'Overweight' ? 'active' : ''}>
            					<span className="column1 text-effect">Overweight</span>
            					<span className="column2 text-effect">25.0 - 29.9</span>
            				</li>
            				<li className={this.state.classification === 'Obese' ? 'active' : ''}>
            					<span className="column1 text-effect">Obese</span>
            					<span className="column2 text-effect">&gt; 30.0</span>
            				</li>
          			  </ul>
      					</div>

      					<div className="dot_slider_bottom">
          				<span
                    className={this.state.firstTab ? 'slider-dot active' : 'slider-dot'}
                    onClick={this.changeTab.bind(this)}>
          				</span>
                  <span
                    className={this.state.firstTab ? 'slider-dot' : 'slider-dot active'}
                    onClick={this.changeTab.bind(this)}>
                  </span>
          			</div>
      				</div>
            </div>

            <div className="box1">
              <ul className="numbering">
                  <li><Link to="/bmi">1</Link></li>
        					<li className="active"><Link to="javascript:void(0);">2</Link></li>
        					<li><Link to="/bmi/recommendation">3</Link></li>
              </ul>

              <div className="box1_tabs_container">
                {/* RIGHT-SIDE:  SECOND TAB */}
                <div className="box1_tab active">
                  <div className="box1-content">
                    <p>
                      To maintain your current weight, your daily calorie allowance is :
                    </p>

                    <span className="count">{dailyCalorieAllowance.value}</span>
                    <span className="unit">Kcal/day</span>

                    <p className="txt5">
                      Akly recommends you to {recommendMsg} weight
                    </p>

                    <Link
                      to="javascript:void(0);"
                      className={this.state.recommend === 'Lose' ?
                        'recommend_button active' : 'recommend_button'}
                      onClick={this.changeRecommend.bind(this, 'Lose')}>
                      Lose
                    </Link>
                    <Link
                      to="javascript:void(0);"
                      className={this.state.recommend === 'Maintain' ?
                        'recommend_button active' : 'recommend_button'}
                      onClick={this.changeRecommend.bind(this, 'Maintain')}>
                      Maintain
                    </Link>
                    <Link
                      to="javascript:void(0);"
                      className={this.state.recommend === 'Gain' ?
                        'recommend_button active' : 'recommend_button'}
                      onClick={this.changeRecommend.bind(this, 'Gain')}>
                      Gain
                    </Link>

                  </div>
                </div>
              </div>

              <div className="both-buttons">
                <Link to="/bmi" className="button-back nav-button">
                  <i className="fa fa-chevron-left" aria-hidden="true"></i> Back
                </Link>
                <Link
                  to="javascript:void(0);"
                  className="button-next nav-button enabled"
                  onClick={this.handleSubmit.bind(this)}>
                  Next <i className="fa fa-chevron-right" aria-hidden="true"></i>
                </Link>
              </div>

            </div>

          </div>

          <div
            id="myBMIModal"
            className="modal fade"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="mySmallModalLabel">
            <div className="modal-dialog modal-sm" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 className="modal-title" id="myModalLabel">Body Mass Index</h4>
                </div>
                <div className="modal-body">
                  <p>This is not what Akly recommend, are you sure you want to proceed?</p>
                </div>
              </div>
            </div>
          </div>

        </div>
      );
    } else {
      return (<p>Loading...</p>);
    }
  }

  handleSubmit(event) {
    const _this = this;
    event.preventDefault();

    localStorage.setItem('userRecommend', this.state.recommend);

    console.log('handleSubmit...', localStorage.getItem('userRecommend'));
    this.props.getSuggestions(
      this.props.bodyMassIndex.statistic._id,
      this.state.recommend,
      function (err, res) {
      if (err) {
        toastr.error(err.message, 'Body Mass Index');
      } else {
        //toastr.success('getSuggestions', 'Body Mass Index');
        browserHistory.push('/bmi/recommendation');
      }
    });
  }

  changeTab(event) {
    event.preventDefault();
    this.setState({
      firstTab: !this.state.firstTab,
    });
  }

  changeRecommend(value, event) {
    event.preventDefault();
    let recommend = this.props.bodyMassIndex.statistic.recommendation.value;
    this.setState({
      recommend: value,
    });
    if (value != recommend) {
      $('#myBMIModal').modal('toggle');
    }
  }
}
