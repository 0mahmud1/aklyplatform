/**
 * Created by IamMohaiminul on 1/14/17.
 */

import React from 'react';
import { Route, IndexRoute } from 'react-router';

import InputDataContainer from '../containers/InputDataContainer';
import CalculatingComponent from '../components/CalculatingComponent';
import ResultContainer from '../containers/ResultContainer';
import RecommendationContainer from '../containers/RecommendationContainer';
import * as AuthService from '../../../services/auth.js'


export default function () {
  return (
    <Route path='bmi' onEnter={AuthService.isAuthorization}>
      <IndexRoute components={InputDataContainer} />
      <Route path='calculating' component={CalculatingComponent} />
      <Route path='result' component={ResultContainer} />
      <Route path='recommendation' component={RecommendationContainer} />
    </Route>
  );
}
