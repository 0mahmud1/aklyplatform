import axios from 'axios';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js';

export function sendSupportQuery(supportQuery, cb) {
  // REDUCER IN DASHBOARD MODULES/authenticate folder
  return function (dispatch) {
    axios.post(
      API.SUPPORT,
      supportQuery
    ).then((response) => {
      console.log('frm dispatch ', response);
      if (cb) {
        cb(null,response);
      }
    }).catch((err) => {
      console.log('err , ', err);
      cb(err,null);
    });
  };
}
