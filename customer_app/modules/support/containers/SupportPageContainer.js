import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { sendSupportQuery } from '../actions/index.js';
import SupportPageComponent from '../components/SupportPageComponent';

class SupportPageContainer extends Component {
  componentWillMount() {
  }

  render() {
    return (
      <SupportPageComponent {...this.props} />
    );
  }
}

// SubscriptionOrderListContainer.propTypes = {};
// SubscriptionOrderListContainer.defaultProps = {};

const mapStateToProps = (store) => {
  return {
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    sendSupportQuery: (supportQuery,cb) => dispatch(sendSupportQuery(supportQuery,cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SupportPageContainer);
