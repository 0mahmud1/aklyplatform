import React from 'react'
import { Route, IndexRoute } from 'react-router'
import SupportPageContainer from '../containers/SupportPageContainer.js'

export default function () {
    return (
        <Route path='support'>
            <IndexRoute  component={SupportPageContainer} />
        </Route>
    )
}