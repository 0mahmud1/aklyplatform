import axios from 'axios';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js'

export function getZones() {
  return function (dispatch) {
    return axios.get(
      API.ZONES
    ).then((response) => {
      dispatch({ type: 'GET_HUB_RESOLVED', payload: response.data.data });
      return response.data.data;
    }).catch((err) => {
      dispatch({ type: 'GET_HUB_REJECTED', payload: err });
    });
  };
}
export function checkPromoCode(code, cb) {
  return function (dispatch) {
    dispatch({ type: 'GET_CHECK_PROMO_PENDING'});
    try{
      axios.get(
      API.CHECK_API + '/' + code
    ).then((response) => {
      
        dispatch({ type: 'GET_CHECK_PROMO_RESOLVED', payload: response.data });
        cb(null, response.data)
        
    }).catch((err) => {
      dispatch({ type: 'GET_CHECK_PROMO_REJECTED', payload: err });
    });
    } catch(e){
      console.error(e);
    }
    
  };
}