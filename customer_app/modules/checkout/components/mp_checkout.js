'use strict'

import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Link, browserHistory } from 'react-router'
import Rodal from 'rodal'
import $ from 'jquery'
import PackIconComponent from '../../core/components/icon/pack.js'
import PickupIconComponent from '../../core/components/icon/pickup.js'
import TickIconComponent from '../../core/components/icon/tick.js'
import _ from 'lodash'
import moment from 'moment'
import toastr from 'toastr'

import Button from '../../core/components/button/Button';
import NotFound from '../../core/components/text/NotFound';

class MPCheckoutComponent extends Component {
  //props.auth.userProfile.address[0]
  constructor(props) {
    console.log('props...', props);
    super(props);
    this.state = {
      selectedAddress: props.auth.userProfile && props.auth.userProfile.address.length > 0 ? props.auth.userProfile.address[0] : {
        street: '',
        apt: ''
      },
      email: props.auth.userProfile && props.auth.userProfile.email ? props.auth.userProfile.email : '',
      phone: props.auth.userProfile && props.auth.userProfile.phone ? props.auth.userProfile.phone : '',
      modalShow: false,
      showAddress: false,
      showNewAddressPanel: false,
      promoModalShow: false,
      promoMessage: null,
      promo: null

    };
    this.createNewAddress = this.createNewAddress.bind(this);
    this.showAddressModal = this.showAddressModal.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.auth.userProfile && this.props.auth.userProfile && this.props.auth.userProfile.address.length > 0) {
      this.setState({ selectedAddress: this.props.auth.userProfile.address[0] });
    }
    if (!prevProps.auth.userProfile && this.props.auth.userProfile && this.props.auth.userProfile.email) {
      this.setState({ email: this.props.auth.userProfile.email });
    }

    $('.rodal-close, .rodal-mask, .addrmodal-list').on('click', () => {
      if ($('html').hasClass('no-overflow')) {
        $('html').removeClass('no-overflow')
      }
    });
  }

  componentDidMount() {
    let _this = this;
    $('.customer-address').select2({
      minimumResultsForSearch: -1
    }).on('select2:select', function (e) {
      _this.setState({
        selectedAddress: JSON.parse(e.target.value)
      })
      console.log('select2.. ', e.target.value)
      _this.props.setMarkerForAddress(e.target.value);
    });

    let selectAddress = document.getElementById('selectAddress');

    selectAddress.addEventListener('click', () => {
      $('html').addClass('no-overflow');
    }, false);

    $('.rodal-close, .rodal-mask, .addrmodal-list').on('click', () => {
      if ($('html').hasClass('no-overflow')) {
        $('html').removeClass('no-overflow')
      }
    });

    $('[data-toggle="tooltip"]').tooltip();

  }

  generateAddressList() {
    return this.props.auth.userProfile && this.props.auth.userProfile.address ?
      this.props.auth.userProfile.address.map((ad) => {
        return <option key={JSON.stringify(ad)} value={JSON.stringify(ad)}> {ad.label}</option>
      }) : null
  }

  generateSubTotal() {
    let allPrices = this.props.mealPlan.deliveries.map(o => Number(o.total));
    return _.sum(allPrices).toFixed(2)
  }

  generateSubTotalOnDemand() {
    // let allPrices = this.props.onDemand.deliveries.map(o => Number(o.total));
    // return _.sum(allPrices).toFixed(2)
    return this.props.onDemand.total.toFixed(2)
  }

  handlePhoneNoChange(event) {
    this.setState({
      phone: event.target.value
    });
  }

  showTotal() {
    if (!this.props.onDemand.deliveries || this.props.onDemand.deliveries.length === 0 && this.props.mealPlan.deliveries.length >= 0)
      return this.generateSubTotal()
    else if (!this.props.mealPlan.deliveries || this.props.mealPlan.deliveries.length === 0 && this.props.onDemand.deliveries.length >= 0)
      return this.generateSubTotalOnDemand()
  }

  confirmModal() {
    let orderRef = this.state.orderRefId ? this.state.orderRefId : null
    return <Rodal visible={this.state.modalShow} onClose={() => { this.setState({ modalShow: false }); browserHistory.push('/') }} height={600}>
      <div className='mp-ocnf'>
        <h1 className='ocnf-heading'>Your Order Successfully Placed</h1>
        <div className='onumb'>
          <p>Order Reference #<span>{orderRef}</span></p>
        </div>

        {/*<p className='dmsg'>{this.props.auth.userProfile.name}, your order has been successfully placed, We will deliver your food between 11am and*/}
        {/*2pm. </p>*/}
        <p className='dmsg'>Your order has been successfully placed, We will deliver your food <span style={{ color: '#f77316' }}> between 11am and
          2pm.</span> </p>
        <p className='qnumb'>If you have any questions, please call us at <span style={{ color: '#f77316' }}> +9933 4422 </span></p>
        <Link to={`/`} className='btn butn-ocnf'>
          Go to Menu
        </Link>
      </div>
    </Rodal>
  }
  PromoModal() {
    return <Rodal visible={this.state.promoModalShow} onClose={() => { this.setState({ promoModalShow: false }) }} height={600}>
      <div className='mp-ocnf'>
        <h1 className='ocnf-heading'>{this.state.promoMessage}</h1>

        <button onClick={() => { this.setState({ promoModalShow: false }) }}>
          close
        </button>
      </div>
    </Rodal>
  }

  showAddressModal() {
    this.setState(prevState => ({
      showAddress: !prevState.showAddress
    }));
  }
  showNewAddressPanel() {
    this.setState({
      showNewAddressPanel: true
    })
  }
  cancelAddressCreation() {
    this.setState({
      showNewAddressPanel: false
    })
  }
  getAddressList() {
    const addressList = this.props.auth.userProfile && this.props.auth.userProfile.address ? this.props.auth.userProfile.address : [];

    return addressList.map(ad => {
      return (
        <li key={JSON.stringify(ad)} value={ad} className='addrmodal-list' onClick={this.setAddress.bind(this, ad)}>
          <p style={{ fontWeight: '600' }}>{ad.street}</p>
          <p>Apartment: {ad.apt}</p>
          <p>Location: {ad.label}</p>
          <p>Phone: {ad.phone}</p>
        </li>
      )
    })
  }

  setAddress(address) {
    this.setState({
      selectedAddress: address,
      showNewAddressPanel: false,
      showAddress: false
    })
    this.props.setMarkerForAddress(JSON.stringify(address));
  }
  getStreet() {
    return $('#search-place-checkout').val();
  }

  addressModal() {
    return (
      <Rodal
        visible={this.state.showAddress}
        onClose={this.showAddressModal}
        height={600}
        className='addrmodal'
      >
        <h3 className='addrmodal-header'>Address List</h3>
        <ul>
          {this.getAddressList()}
        </ul>

      </Rodal>
    );
  }

  showSubTotal() {
    let subTotal = (this.showTotal() - this.promoDiscount()).toFixed(2)
    subTotal = subTotal < 0 ? 0 : subTotal
    return subTotal;
  }

  render() {
    const total = this.showTotal();
    const BtnFontSize = {
      fontSize: '16px'
    };

    return (
      <div>
        <div className='caadditionalbar mpcheckoutbar'>
          <div className='caadditionalbar-content mpcheckoutbar-content container ca-container'>
            <div className='ca_steprogress'>
              <div className='steprogress detailed_view'>
                <div className='progress_section'>
                  <div className='stprogress'>
                    <div className='progressbar_section'>
                      <div className='stprogressbar'>
                        <div className='horizontal_box'>
                          <div className='base_bar'></div>
                          <div className='base_bar'></div>
                        </div>
                      </div>
                    </div>

                    <div className='progressbar_section'>
                      <div className='horizontal_box progress_status_section'>
                        <div className='steps step1'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <PackIconComponent fillColor='#ED731F' />
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>

                        <div className='steps step2'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <PickupIconComponent fillColor='#ED731F' />
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>

                        <div className='steps step3'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <TickIconComponent fillColor='#A7A7A7' />
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='mp_chkout container ca_container'>
          <div>
            <div className='row'>
              <div className='col-lg-8'>
                <div className='mp_chkoutaddr'>
                  <div className='addr-headwrapper'>
                    <div className='addr_header'>
                      <h3>Select Address</h3>
                      <p>Drag the map to select your address, Please select your marker within our defined area</p>
                    </div>
                  </div>

                  <div className='addrmap'>
                    <div className='addr_gmap' id='checkout-map'></div>
                    {/*<input id='search-place-checkout' className='controls gmap-search' type='text' placeholder='Please Search'/>*/}
                  </div>
                  <div className='addr_d'>
                    <div className='addr_input'>
                      <div className='input-headerwrapper'>
                        <div className='input_header'>
                          <h4>Enter Your Address</h4>
                          {!this.state.showNewAddressPanel ?
                            <span
                              className='add-addr-icon'
                              onClick={this.showNewAddressPanel.bind(this)}
                              data-toggle="tooltip"
                              data-placement="top"
                              title="Add Address">
                              <i className='glyphicon glyphicon-plus'></i>
                            </span>
                            : null
                          }
                        </div>
                        {
                          !this.state.showNewAddressPanel ?
                            <div>
                              <Button
                                text='Select'
                                style={BtnFontSize}
                                onClick={this.showAddressModal}
                                id='selectAddress'
                              />
                            </div>
                            : null
                        }
                      </div>
                      <div id='addAddressForm' hidden={!this.state.showNewAddressPanel}>
                        <div className='input_form'>
                          <div className='ca_geninput'>
                            <div className="form-group input-group no_mb" style={{ width: '100%' }}>
                              <input
                                type="text"
                                ref='addressTextBox'
                                id='search-place-checkout'
                                placeholder='Please Search'
                                className="form-control ca_frmctrl pl-10imp" />
                            </div>
                          </div>

                          <div className='ca_geninput'>
                            <div className="form-group no_mb">
                              <input type="text" className="form-control ca_frmctrl pl-10imp" id="apt-number"
                                placeholder="Apt Number" />
                            </div>
                          </div>

                          <div className='ca_geninput'>
                            <div className="form-group no_mb">
                              <select className="form-control ca_frmctrl pl-10imp" id="identifier">
                                <option value="">Please Select</option>
                                <option value="Home">Home</option>
                                <option value="Office">Office</option>
                                <option value="Apartment">Apartment</option>
                              </select>
                            </div>
                          </div>

                          <div className='ca_geninput'>
                            <div className="form-group no_mb">
                              <input type="text" className="form-control ca_frmctrl pl-10imp" id="phone-number"
                                placeholder="Phone Number" />
                            </div>
                          </div>
                          <div className='mt-25'>
                            {/*<Button*/}
                            {/*text='Save'*/}
                            {/*style={BtnFontSize}*/}
                            {/*onClick={this.createNewAddress.bind(this)}*/}
                            {/*/>*/}

                            <Button
                              className='butn_danger ml-10'
                              text='Cancel'
                              style={BtnFontSize}
                              onClick={this.cancelAddressCreation.bind(this)}
                            />
                          </div>

                        </div>
                      </div>
                      {this.state.selectedAddress.street === '' ?
                        <NotFound text='No existing address found' style={{ color: '#f77316' }} />
                        :
                        <div className='addr-selected' hidden={this.state.showNewAddressPanel}>
                          <p>{this.state.selectedAddress.street}</p>
                          <p>Apartment: {this.state.selectedAddress.apt}</p>
                          <p>Location: {this.state.selectedAddress.label}</p>
                          <p>Phone: {this.state.selectedAddress.phone}</p>
                        </div>
                      }
                    </div>
                    <div className='addr_location'>
                      <ul className='dloc'>
                        <li className='fstn'>
                          <div className="media">
                            <div className="media-left">
                              <a href="#">
                                <img className="media-object" src="/images/location.svg" alt="akly food station" />
                              </a>
                            </div>
                            <div className="media-body">
                              <h4 className="media-heading">Akly Food Station</h4>
                              <p>36 Lexinton Ave, Newyork, 08827</p>
                            </div>
                          </div>
                        </li>
                        <li className='d_txt'>
                          <p>We will deliver between 11am to 2pm</p>
                        </li>
                        <li className='cloc'>
                          <div className="media">
                            <div className="media-left">
                              <a href="#">
                                <img className="media-object" src="/images/location.svg" alt="..." />
                              </a>
                            </div>
                            <div className="media-body">
                              <h4 className="media-heading">Your Location</h4>
                              <p>{this.state.selectedAddress.street} , {this.state.selectedAddress.apt}</p>
                            </div>
                          </div>
                        </li>
                        <li className='cloc1'>
                          <div className="media">
                            <div className="media-left">
                              <a href="#">
                                <img className="" src="/images/android.svg" alt="Phone Number" />
                              </a>
                            </div>
                            <div className="media-body">
                              <p>{this.state.selectedAddress.phone}</p>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className='col-lg-4'>
                <div className='mp_chkoutconfirm'>
                  <div className='payment_type'>
                    <div className='payment_header'>
                      <h4>Payment Type</h4>
                    </div>
                    <div className="ca_geninput">
                      <div className="form-group">
                        <div className="mp_chkoutconfirm_select2">
                          <select ref='payment' className="js-example-select form-control ca_select2">
                            <option value="cash">Cash on Delivery</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='final_info'>
                    <div className='additional_info'>
                      <div className='additional_info_header'>
                        <h4>Confirm</h4>
                      </div>
                      <div className='ca_geninput'>
                        <div className="form-group no_mb">
                          <div className="input-group ca_input_group">
                            <div className="input-group-addon ca_input-group_addon"><img className="" src="/images/mail.svg" alt="email icon" /></div>
                            <input type="email" className="form-control ca_frmctrl" id="exampleInputAmount" ref='email' placeholder="Email" value={this.state.email} disabled={this.state.email} />
                            <div className="input-group-addon ca_input_group_icontext">Ok <span><svg width="18" height="14" viewBox="0 0 18 14" xmlns="http://www.w3.org/2000/svg"><title>F9CF4483-CB0A-4ADE-9527-8C3B7EC55719</title><path d="M16.24.892c-.22-.217-.578-.217-.797 0l-8.386 8.2c-.22.22-.578.22-.796 0L2.588 5.418c-.11-.11-.252-.163-.395-.164-.145 0-.292.053-.402.164L.167 6.85c-.11.11-.167.246-.167.388 0 .143.06.293.167.4l3.705 3.82c.22.22.58.572.797.79l1.592 1.577c.22.216.577.216.796 0L17.833 3.258c.22-.216.22-.572 0-.79L16.24.893z" fill="#53CA45" fillRule="evenodd" /></svg></span></div>
                          </div>
                        </div>
                      </div>
                      {/*
                       <div className='ca_geninput'>
                         <div className="form-group no_mb">
                           <div className="input-group ca_input_group">
                             <div className="input-group-addon ca_input-group_addon"><img className="" src="/images/android.svg" alt="Phone Number"/></div>
                             <input type="text" className="form-control ca_frmctrl" id="exampleInputAmount" ref='phone' placeholder="Phone Number" value={this.state.phone} onChange={this.handlePhoneNoChange.bind(this)}/>
                             <div className="input-group-addon ca_input_group_icontext">Ok <span><svg width="18" height="14" viewBox="0 0 18 14" xmlns="http://www.w3.org/2000/svg"><title>F9CF4483-CB0A-4ADE-9527-8C3B7EC55719</title><path d="M16.24.892c-.22-.217-.578-.217-.797 0l-8.386 8.2c-.22.22-.578.22-.796 0L2.588 5.418c-.11-.11-.252-.163-.395-.164-.145 0-.292.053-.402.164L.167 6.85c-.11.11-.167.246-.167.388 0 .143.06.293.167.4l3.705 3.82c.22.22.58.572.797.79l1.592 1.577c.22.216.577.216.796 0L17.833 3.258c.22-.216.22-.572 0-.79L16.24.893z" fill="#53CA45" fillRule="evenodd"/></svg></span></div>
                           </div>
                         </div>
                       </div>
                      */}
                      <div className="ca_geninput dnotes">
                        <div className="form-group ca_textarea no_mb">
                          <div className='label_w_icon'>
                            <img src='/images/clipboard.svg' alt='Delivery Notes' />
                            <label htmlFor="comment" className="ca_input_label">Delivery Notes</label>
                          </div>
                          <textarea ref='deliveryNote' className="form-control ca_frmctrl" id="comment" placeholder="Say Something!"></textarea>
                        </div>
                      </div>

                      <div className='ca_geninput'>
                        <div className="form-group no_mb">
                          <div className="input-group ca_input_group">
                            <div className="input-group-addon ca_input-group_addon"><img className="" src="/images/discount.svg" alt="Phone Number" /></div>
                            <input type="text" className="form-control ca_frmctrl" id="exampleInputAmount" ref="promoCode" placeholder="Promo Code" />
                            <div className="input-group-addon ca_input-group_addon no_btmbdr"><button type="submit" onClick={this.checkPromoCode.bind(this)} className="btn butn_promo">Apply</button></div>
                          </div>
                          {this.state.promo && this.state.promo.coupon ? this.state.promo.coupon.couponType === 'percentage' ?
                            'you get promo discount ' + this.state.promo.coupon.value + '%' : 'you get promo discount ' + this.state.promo.coupon.value + ' tk'
                            : null}
                        </div>
                      </div>
                    </div>

                    <div className='confirm_total'>
                      {/*
                       <div className="dqtity">
                         <div className='dqtity_text'>
                           <p>Delivery : </p>
                         </div>
                         <div className='dqtity_numb'>
                           <p><span>QR</span> 15</p>
                         </div>
                       </div>
                      */}

                      <div className="tamnt">
                        <div className='tamnt_text'>
                          <p>Total : </p>
                        </div>

                        <div className='tamnt_numb'>
                          <p><sup><span>QR</span></sup> {total}</p>
                        </div>
                      </div>

                      <div className="tamnt">
                        <div className='tamnt_text'>
                          <p>Sub Total : </p>
                        </div>

                        <div className='tamnt_numb'>
                          <p><sup><span>QR</span></sup> {this.showSubTotal()}</p>
                        </div>
                      </div>


                    </div>
                  </div>
                  <div className='chkout_btn'>
                    <button onClick={this.handleSubmit.bind(this)} className='btn butn_chkout'>Checkout</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.confirmModal()}
        {this.addressModal()}
        {this.PromoModal()}
      </div>
    )
  }

  createNewAddress(cb) {
    //event.preventDefault();

    try {
      let _this = this;
      let user = this.props.auth.userProfile;
      let address = {
        street: $('#search-place-checkout').val(),
        apt: $('#apt-number').val(),
        label: $('#identifier').val(),
        geoLocation: this.props.newGeoLocation,
        phone: $('#phone-number').val()
      };

      console.log("createNewAddress ", address);
      //console.log(this.props.auth);

      if (!address.apt) {
        toastr.warning('Apt no required');  //& unique Label
        return;
      }
      if (!address.phone) {
        toastr.warning('Phone Number Required');
        return;
      }
      if (!this.props.addressInZone(this.props.newGeoLocation, this.props.hubs.hubList)) {
        toastr.warning('your location is not inside our serving area')
        return;
      }
      if (user.address) {
        user.address.push(address)
      } else {
        user.address = [];
        user.address.push(address)
      }
      console.group('list of user address');
      console.log(user.address);
      console.groupEnd();

      this.props.updateUser(user._id, user, function () {
        let tempHeader = localStorage.getItem('jwt');
        //_this.props.refreshCustomer(tempHeader);
        _this.setState({
          selectedAddress: address,
        });
        console.log('address here ', address);
        cb(null, address);
        // $('#search-place-checkout').val('');
        // $('#apt-number').val('');
        // $('#identifier').val('');
        // $('#phone-number').val('');
      });

    } catch (e) {
      toastr.error('Sorry unable to create address');
      console.group('error in createNewAddress');
      console.log(e);
      console.log(e.stack);
      console.groupEnd()
      cb(e, null);
    }
  }
  promoDiscount() {
    let promoDiscount = 0;
    if (this.state.promo && this.state.promo.coupon) {
      if (this.state.promo.coupon.couponType === 'percentage') {
        promoDiscount = ((this.showTotal() * this.state.promo.coupon.value) / 100)
      } else {
        promoDiscount = this.state.promo.coupon.value

      }
    }
    return promoDiscount.toFixed(2)

  }
  createOrderObject() {
    let self = this;
    /*order = {
      orderDate: new Date(),
      subTotal: self.generateSubTotal(),
      deliveries: self.props.mealPlan.deliveries,
      address: self.state.selectedAddress,
      total: self.generateSubTotal(),
      orderType: 'subscription',
      paymentType: 'cash'
    }*/
    let orderType, order
    if (!this.props.onDemand.deliveries || this.props.onDemand.deliveries.length === 0 && this.props.mealPlan.deliveries.length >= 0)
      orderType = 'subscription'
    else if (!this.props.mealPlan.deliveries || this.props.mealPlan.deliveries.length === 0 && this.props.onDemand.deliveries.length >= 0)
      orderType = 'onDemand'

    if (!orderType) {
      toastr.error('Something went wrong!, please order your food again')
      return
    }

    if (!self.state.selectedAddress.geoLocation) {
      toastr.error('please create your address first');
      return;
    }
    if (this.state.promo) {
    }
    console.log('Address');
    console.log(self.state.selectedAddress);
    // if(!self.state.selectedAddress.phone){
    //   toastr.error('please put your phone number');
    //   return ;
    // }
    const deliveryNote = ReactDOM.findDOMNode(self.refs.deliveryNote).value.trim();

    if (orderType == 'subscription') {
      let originalDeliveries = self.props.mealPlan.deliveries;
      let filteredDeliveries = _.remove(originalDeliveries, function (o) {
        return (localStorage.getItem('planType') === 'lunch' && o.shift === 'Dinner')
      })
      // get start and end date of delivery
      let totalDelivery = self.props.mealPlan.deliveries.length
      // console.log('first delivery date======',self.props.mealPlan.deliveries[0].deliveryDate)
      // console.log('last delivery date======',self.props.mealPlan.deliveries[totalDelivery-1].deliveryDate)

      order = {
        orderDate: new Date(),
        subTotal: self.generateSubTotal() - this.promoDiscount(),
        deliveries: originalDeliveries,
        address: self.state.selectedAddress,
        locations: { coordinates: [self.state.selectedAddress.geoLocation.lng, self.state.selectedAddress.geoLocation.lat] },
        total: self.generateSubTotal(),
        orderType: orderType,
        planType: self.props.mealPlan.planType,
        paymentType: 'cash',
        deliveryNote: deliveryNote,
        mealPlan: self.props.mealPlan.mealPlanId,
        firstDeliveryDate: self.props.mealPlan.deliveries[0].deliveryDate,
        lastDeliveryDate: self.props.mealPlan.deliveries[totalDelivery - 1].deliveryDate
      }


    }

    if (orderType == 'onDemand') {
      order = {
        orderDate: new Date(),
        subTotal: self.generateSubTotalOnDemand() - this.promoDiscount(),
        deliveries: [{ deliveryDate: moment(), meals: self.props.onDemand.deliveries }],
        address: self.state.selectedAddress,
        locations: { coordinates: [self.state.selectedAddress.geoLocation.lng, self.state.selectedAddress.geoLocation.lat] },
        total: self.generateSubTotalOnDemand(),
        orderType: orderType,
        paymentType: 'cash',
        deliveryNote: deliveryNote
      }

    }

    if (self.state.promo && self.state.promo.coupon) {
      order.coupon = self.state.promo.coupon
    }

    return order


  }

  callCreateOrder(order) {
    console.log('show order');
    console.log('<<<<<<<<', order, '>>>>>>>>>>>');
    let self = this;
    if (!order)
      return

    self.props.createOrder(order, function (res) {
      console.log('call back ', res)
      self.setState({ orderRefId: res.data.data.orderRefId })
      self.setState({ modalShow: true })
      self.setState({ promo: null })

      //browserHistory.push(`/confirmation/${res.data.data._id}`)
    })
  }

  checkPromoCode() {
    let self = this,
      code = ReactDOM.findDOMNode(self.refs.promoCode).value.trim()
    self.setState({ promo: null });

    this.props.checkPromoCode(code, function (err, res) {
      if (res) {

        if (!res.data.isValidate) {
          self.setState({ promoMessage: "coupon is not valided" });
          self.setState({ promoModalShow: true });

        } else if (res.data.isExpired) {
          self.setState({ promoMessage: "coupon is already Expired" });
          self.setState({ promoModalShow: true });

        } else if (res.data.isExceedLimit) {
          self.setState({ promoMessage: "coupon is already user limit exceed limit" });
          self.setState({ promoModalShow: true });

        } else {
          self.setState({ promo: res.data });

        }

      }
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    let self = this, order;
    let newAddress = null;
    if (this.state.showNewAddressPanel === true) {
      this.createNewAddress(function (err, address) {
        if (err) return;
        else {
          newAddress = address;
          console.log('newAddress ', newAddress);
          order = self.createOrderObject()
          self.callCreateOrder(order);
        }

      });

    } else {
      order = self.createOrderObject()
      self.callCreateOrder(order);
    }

  }
}

export default MPCheckoutComponent
