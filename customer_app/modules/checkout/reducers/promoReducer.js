/*
 * This reducer will always return an array of categories no matter what
 * You need to return something, so if there are no categories then just return an empty array
 */

const defaultState = {
    promo: {},
};


const PromoReducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'GET_CHECK_PROMO_PENDING':
            return state
        case 'GET_CHECK_PROMO_RESOLVED':
            return Object.assign({}, state, { promo: action.payload });

        case 'GET_CHECK_PROMO_REJECTED':
            return state
        default:
            return state
    }
};

export default PromoReducer
