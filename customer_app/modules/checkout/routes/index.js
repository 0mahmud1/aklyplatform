import React from 'react'
import { Route, IndexRoute } from 'react-router' 
import checkoutContainer from '../containers/MPCheckout.js'
import * as AuthService from '../../../services/auth.js'

export default function(){
    return(
        <Route path='checkout' onEnter={AuthService.isAuthorization}>
            <IndexRoute component={checkoutContainer}/>
        </Route>
    )
}