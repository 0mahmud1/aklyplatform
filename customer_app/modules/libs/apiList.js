
/*
    api constants variable
*/
export const ZONES = 'api/zones';
export const FOODS = 'api/foods';
export const TAGS =  'api/tags';
export const ORDERS = 'api/orders';
export const DELIVERIES = 'api/deliveries';
export const CUSTOMER_CHECK = 'api/customers/check';
export const CUSTOMER = 'api/customers';
export const CHANGE_PASSWORD = 'api/changePassword';
export const CHANGE_PICTURE = 'api/changePicture';
export const MEAL_PLAN = 'api/meal-plans';
export const SUPPORT =  'api/support';
export const CHECK_API = 'api/coupons/validate'