import React from 'react'
import { Route, IndexRoute } from 'react-router'
import CoreLayoutCA from '../../core/layouts/'
import MealPlanDetailsContainer from '../containers/MealPlanCustomerContainer'
import SubscriptionEditContainer from '../containers/SubscriptionEditContainer'
import SetMealplanDetailsContainer from '../containers/SetMealplanDetailsContainer'



export default function () {
    return (
        <Route path='subscription'>
            <IndexRoute component={MealPlanDetailsContainer} />
            <Route path=':orderRefId' component={SubscriptionEditContainer} />
            <Route path='mealplans' >
                <Route path=':_id' component={SetMealplanDetailsContainer} />
            </Route>
        </Route>
    )
}