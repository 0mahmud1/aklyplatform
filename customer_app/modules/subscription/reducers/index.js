
import _ from 'lodash'
import moment from 'moment'
import toastr from 'toastr'
import {getTotalOfSingleDelivery} from '../../libs/index.js'
import store from '../../../store.js'



const formattedDeliveries = localStorage.getItem('deliveries') ?JSON.parse(localStorage.getItem('deliveries')).map((o) =>{
  o.deliveryDate = moment(o.deliveryDate)
  return o;
}) : null

const defaultState = {
  mealPlanId: '',
  startDate: moment(),
  selectedWeek: 0,
  planType: 'lunch',
  deliveries: localStorage.getItem('deliveries') ? [...formattedDeliveries]:[]
}


const cuMealplan = (state = defaultState, action) => {
  switch (action.type) {
    case 'SELECT_WEEK':
      return Object.assign({}, state, {selectedWeek: action.payload});

    case 'SAVE_ALL_DAYS':
      return Object.assign({}, state, {allDays: action.payload});

    case 'SET_MEALPLAN_TYPE':
      localStorage.setItem('planType',action.payload);
      return Object.assign({}, state, {planType: action.payload});

    case 'GET_DELIVERIES_FOR_UPDATE_RESOLVED':
      originalDeliveries = action.payload;
      originalDeliveries.forEach(function (delivery) {
        delivery.deliveryDate = moment(delivery.deliveryDate)
      })
      return Object.assign({}, state, { deliveries: originalDeliveries });

      case 'COPY_MEALPLAN':
      // if(state.selectedWeek !== 0)
      //   return state
      console.log("mealplan to be coppied====", store.getState().mealPlans.mealPlanList[action.payload]._id.toString())
      localStorage.setItem('selectedMealplan',store.getState().mealPlans.mealPlanList[action.payload]._id.toString());
      // Object.assign({}, state, {mealPlanId: store.getState().mealPlans.mealPlanList[action.payload]._id.toString()})
      let mealPlan = Object.assign({},store.getState().mealPlans.mealPlanList[action.payload]),
        cpDeliveries = [],
        //originalDeliveries = [...state.deliveries],
        originalDeliveries = [],
        selectedWeek = state.selectedWeek;
        console.log('meal plan >>>>',mealPlan);
        for( let week = 0; week < 4 ; week++ ) {
          for(let i =0; i<state.allDays[week].length;i++) {
            let weekDayName = state.allDays[week][i].format('dddd');
            let filteredMealPlans = _.filter(mealPlan.plans, function (o) {
              return o.day.toLowerCase() == weekDayName.toLowerCase() && o.week == week
            })
           console.log('length should be ', filteredMealPlans)
            filteredMealPlans.forEach(function (p) {
              if(localStorage.getItem('planType') === 'lunch' && p.slot === 'dinner') {
                console.log('dinner not added');
              } else {
                cpDeliveries.push({
                  deliveryDate: state.allDays[week][i],
                  shift: p.slot[0].toUpperCase()+p.slot.slice(1),
                  meals: [...p.foods],
                  total: getTotalOfSingleDelivery(p.foods).toFixed(2)
                })
              }
            })
          }
        }


      let rmx = _.remove(originalDeliveries,function (rm) {
        return _.findIndex(cpDeliveries, (cpd) =>
        {
          return cpd.deliveryDate.format('YYYY-MM-DD') == rm.deliveryDate.format('YYYY-MM-DD') && cpd.shift.toLowerCase() == rm.shift.toLowerCase()
        }) >= 0;
      })

      console.log('odd ',originalDeliveries, rmx)
      //toastr.success("Meal Plan selected");
      return Object.assign({},state,{
        mealPlanId: store.getState().mealPlans.mealPlanList[action.payload]._id.toString(),
        deliveries: [...originalDeliveries,...cpDeliveries]
      })


    case 'ADD_TO_BOX':
      let mealPlanAdd2Box = Object.assign({}, action.payload);
      console.log("ADD_TO_BOX paylodad=======", action.payload)
      let cpDeliveriesAdd2Box = [];
      let originalDeliveriesAdd2Box = [...state.deliveries];
      //let selectedWeekAdd2Box = state.selectedWeek;

      for( let selectedWeekAdd2Box =0; selectedWeekAdd2Box<4;selectedWeekAdd2Box++) {
          for(let i = 0; i < state.allDays[selectedWeekAdd2Box].length; i++) {
              let weekDayNameAdd2Box = state.allDays[selectedWeekAdd2Box][i].format('dddd');
              let filteredMealPlansAdd2Box = _.filter(mealPlanAdd2Box.plans, function (plan) {
                  return plan.day.toLowerCase() == weekDayNameAdd2Box.toLowerCase();
              });
              filteredMealPlansAdd2Box.forEach(function (p) {
                  cpDeliveriesAdd2Box.push({
                      deliveryDate: state.allDays[selectedWeekAdd2Box][i],
                      shift: p.slot[0].toUpperCase()+p.slot.slice(1),
                      meals: [...p.foods],
                      total: getTotalOfSingleDelivery(p.foods).toFixed(2),
                  });
              })
          };
      }

      let rmxAdd2Box = _.remove(originalDeliveriesAdd2Box,function (rm) {
        return _.findIndex(cpDeliveriesAdd2Box, (cpd) =>
        {
          return cpd.deliveryDate.format('YYYY-MM-DD') == rm.deliveryDate.format('YYYY-MM-DD') && cpd.shift.toLowerCase() == rm.shift.toLowerCase()
        }) >= 0;
      });
      // console.log('ADD_TO_BOX: ', originalDeliveriesAdd2Box, rmxAdd2Box);
      // return state;
      return Object.assign({}, state, {
        mealPlanId: action.payload._id.toString(),
        deliveries: [...originalDeliveriesAdd2Box, ...cpDeliveriesAdd2Box]
      });


    case 'ADD_FOOD_MEALPLAN':
      let delivery = Object.assign({}, state.selectedDeliveryFoodMenu),
        deliveries = [...state.deliveries];

      let existingIndex = _.findIndex(deliveries, dv => dv.deliveryDate.format('YYYY-MM-DD') ===  delivery.deliveryDate.format('YYYY-MM-DD') && dv.shift == delivery.shift)
      console.log('>> ', delivery, deliveries);
      if(existingIndex == -1) {
        deliveries.push(delivery);
      } else {
        deliveries[existingIndex] = delivery
      }
      //return state
      return Object.assign({}, state, {deliveries: deliveries, selectedDeliveryFoodMenu: null})


    case 'SHOW_SUBSCRIPTION_FOODLIST':
      let hasEntry = _.find(state.deliveries,function (d) {
        return d.deliveryDate.format('YYYY-MM-DD') == action.payload.deliveryDate.format('YYYY-MM-DD') && d.shift.toLocaleLowerCase() == action.payload.shift.toLowerCase()
      })
      //console.log('has entry ', hasEntry)
      return Object.assign({}, state, {selectedDeliveryFoodMenu: hasEntry ? hasEntry: action.payload});

    case 'HIDE_SUBSCRIPTION_FOODLIST':
      return Object.assign({}, state, {selectedDeliveryFoodMenu: null});

    case 'ADD_FOOD_TEMP':
      let tempDelivery = Object.assign({}, state.selectedDeliveryFoodMenu)
      if (!tempDelivery.meals) {
        tempDelivery.meals = [];
        tempDelivery.meals.push({item: action.payload, quantity: 1});
        tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
        return Object.assign({}, state, {selectedDeliveryFoodMenu: tempDelivery})
      }


      let tempMealIndex = _.findIndex(tempDelivery.meals, function (o) {
        return o.item._id === action.payload._id
      });
      console.log('index: ', tempMealIndex)

      if (tempMealIndex == -1) {
        tempDelivery.meals.push({item: action.payload, quantity: 1});
        tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
        return Object.assign({}, state, {selectedDeliveryFoodMenu: tempDelivery})
      }

      console.log('index: ', tempDelivery.meals[tempMealIndex])
      tempDelivery.meals[tempMealIndex].quantity += 1
      tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
      return Object.assign({}, state, {selectedDeliveryFoodMenu: tempDelivery})

    case 'REDUCE_FOOD_TEMP':
      let tmpDeliveryReduce = Object.assign({}, state.selectedDeliveryFoodMenu);
      let tmpDeliveryMealsReduce = [...tmpDeliveryReduce.meals]
      let tmpMealIndexReduce = _.findIndex(tmpDeliveryMealsReduce, function (o) {
        return o.item._id === action.payload._id
      });

      if(tmpDeliveryMealsReduce[tmpMealIndexReduce].quantity == 1) {
        console.log('shd delete item')
        tmpDeliveryMealsReduce.splice(tmpMealIndexReduce,1)
        console.log(',, ', tmpDeliveryMealsReduce)
        tmpDeliveryReduce.meals = tmpDeliveryMealsReduce
      } else {
        tmpDeliveryMealsReduce[tmpMealIndexReduce].quantity -= 1
        tmpDeliveryReduce.meals = tmpDeliveryMealsReduce
      }
      tmpDeliveryReduce.total = getTotalOfSingleDelivery(tmpDeliveryReduce.meals);

      return Object.assign({}, state, {selectedDeliveryFoodMenu: tmpDeliveryReduce})


    case 'REMOVE_MEAL_FROM_DELIVERY':
      let deliveryIndex = _.findIndex(state.deliveries,(d) => {
        return d.deliveryDate.format('YYYY-MM-DD') == action.payload.delivery.deliveryDate.format('YYYY-MM-DD')
          &&
          d.shift.toLocaleLowerCase() == action.payload.delivery.shift.toLowerCase()
      })

      if( deliveryIndex !== -1) {
        _.remove(state.deliveries[deliveryIndex].meals,(r) => {
          return r.item._id == action.payload.meal.item._id
        })
      }
      state.deliveries[deliveryIndex].total = getTotalOfSingleDelivery(state.deliveries[deliveryIndex].meals);
      return Object.assign({}, state, {deliveries: state.deliveries})

    case 'CLEAR_MEALPLAN_DELIVERY':
      localStorage.removeItem('deliveries');
      localStorage.removeItem('selectedMealplan');
      //toastr.success('Meal Plan Cleared');
      return Object.assign({},state,{mealPlanId: '',deliveries:[]})

    default:
      return state
  }
}
export default cuMealplan
