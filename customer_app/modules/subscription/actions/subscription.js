import axios from 'axios';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js';

export function getMealPlans(planType) {
  return function (dispatch) {
    dispatch({ type: 'GET_MEALPLAN_PENDING'});
    axios.get(
      API.MEAL_PLAN,{
        params: {
          planType: planType,
        },
      }
    ).then((response) => {
      console.log('get meal plans... : ', response.data);
      dispatch({ type: 'GET_MEALPLAN_RESOLVED', payload: response.data });
      //toastr.success('MealPlans Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_MEALPLAN_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function readMealPlanAction(_id) {
  return function (dispatch) {
    dispatch({ type: 'READ_MEALPLAN_PENDING' });
    axios.get((API.MEAL_PLAN +'/' + _id))
      .then((response) => {
        dispatch({ type: 'READ_MEALPLAN_RESOLVED', payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: 'READ_MEALPLAN_REJECTED', payload: err });
        console.warn('Read mealPlanAction: ', err);
      });
  };
}


