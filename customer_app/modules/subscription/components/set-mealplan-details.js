import React, {Component} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {browserHistory} from 'react-router'

export default class SetMealplanDetailsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          selectedWeek : 0
        }
        this.backToSubscriptionPage = this.backToPrevPage.bind(this, '/subscription');
    }

    backToPrevPage(routeName) {
        return browserHistory.push(routeName);
    }

    pageBackIcon() {
        return (
            <span className='route-back' onClick={this.backToSubscriptionPage}>
                <i className='glyphicon glyphicon-arrow-left'></i>
            </span>
        );
    }


    mealPlanInfo() {
        const mealPlanObj = this.props.mealPlan;

        const mealPlanType = mealPlanObj.planType === 'fullDay' ? 'Full Day' : 'Lunch Only';

        return (
            <div className='mpinfo-card'>
                <div className='upper-sec'>
                    <div className='info-header'>
                        <h2>{mealPlanObj.name}</h2>
                        <div className='info-type'>
                            <p>{mealPlanType}</p>
                        </div>
                    </div>

                    <div className='info-description'>
                        <p>{mealPlanObj.description}</p>
                    </div>
                    <div className='info-category'>
                        <p>
                            <span>Category: </span> {mealPlanObj.categories.join(',')}
                        </p>
                    </div>
                </div>

                <div className='lower-sec'>
                    <div className='info-numbers'>
                        <div className='numbers-block'>
                            <p className='numbers-heading'>Total Price</p>
                            <p className='numbers-number'>{mealPlanObj.totalPrice}</p>
                        </div>

                        <div className='numbers-block'>
                            <p className='numbers-heading'>Set Price</p>
                            <p className='numbers-number'>{mealPlanObj.setPrice}</p>
                        </div>

                        <div className='numbers-block'>
                            <p className='numbers-heading'>Calories (Kcal/week)</p>
                            <p className='numbers-number'>{mealPlanObj.calorie}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    generateTabs() {
        const TabNames = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

        return TabNames.map(el => {
            return (
                <Tab key={el}>{el}</Tab>
            )
        })
    }

    generateTableHeader() {
        const headerNames = ['Food Name', 'Quantity', 'Calorie', 'Price'];

        return headerNames.map((name, index) => {
            return (
                <th key={index}>{name}</th>
            )
        })
    }

    generateTableRow(day) {
        //console.log('generateTableRow >>>',day);
        const _this = this;
        let filteredMealPlans = _.filter(_this.props.mealPlan.plans, function (o) {
          return o.day.toLowerCase() == day.toLowerCase() && o.week == _this.state.selectedWeek
        })
        //console.log('filteredMealPlans >>>',filteredMealPlans);
        return filteredMealPlans.map((mealPlan, index) => {
          return mealPlan.foods.map((food) => {
            return (
                <tr key={food._id}>
                    <td>{food.item.name}</td>
                    <td>{food.quantity}</td>
                    <td>{food.item.calorie}</td>
                    <td>QR {food.item.price}</td>
                </tr>
            )
          });
        })
    }

    generateFoods() {
        let days = [];
        {
          this.props.mealPlan.planType === 'lunch' ?
          days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday']
          :
          days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday','Saturday']
        }
        return days.map((day, index) => {
            return (
                <div key={index} className='day-card'>
                    <div className='day-name day-devider'>
                        <h4 className='day-nametext'>{day}</h4>
                    </div>
                    <div className='day-foods'>
                        <table className='table'>
                            <thead>
                                <tr>
                                    {this.generateTableHeader()}
                                </tr>
                            </thead>
                            <tbody>
                                {this.generateTableRow(day)}
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        })
    }

    generateTabPanels() {
        const PanelItems = ['week1', 'week2', 'week3', 'week4'];

        return PanelItems.map(el => {
            return (
                <TabPanel key={el}>
                    {this.generateFoods()}
                </TabPanel>
            )
        })
    }
    handleTabSelect(index, last) {
      this.setState({
        selectedWeek : index
      })
    }
    render() {
      //console.log('SetMealplanDetailsComponent >>>',this.props.mealPlan);
        return (
            <div className='set-mp-details container ca_container'>
                <div className='row'>
                    <div className='col-lg-12'>
                        {this.pageBackIcon()}
                        {this.mealPlanInfo()}

                        {/* tab start */}
                        <div className='react-tabs-customtheme'>
                            <Tabs onSelect={this.handleTabSelect.bind(this)}>
                                <TabList>
                                    {this.generateTabs()}
                                </TabList>
                                {this.generateTabPanels()}
                            </Tabs>
                        </div>
                        {/* tab end */}
                    </div>
                </div>
            </div>
        );
    }
}
