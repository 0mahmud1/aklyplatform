'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import _ from 'lodash'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class SubscriptionEditFoodListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false
    };
  }

  show() {
    this.setState({
      clicked: !this.state.clicked
    });
  }

  componentDidMount() {
    console.log('cmdm', this.props.foods);
    if (this.props.tags.foodTags.length < 1) {
      this.props.createFoodTags();
    }
  }

  addFoodToTemp(food) {
    console.log(food.name)
    this.props.addFoodTemp(food);
  }

//fcard-mod
  generateFoodList() {
    let _this = this;
    return this.props.foods.filteredFoodList.map((food) => {
      let existingFood = null
      if(_this.props.mealPlan.selectedDeliveryFoodMenu && _this.props.mealPlan.selectedDeliveryFoodMenu.meals) {
        existingFood = _.find(_this.props.mealPlan.selectedDeliveryFoodMenu.meals, function (f) {
          return f.item._id == food._id
        })
        //console.log('** ', existingFood)
      }

      return <div key={food._id} className='col-lg-3 fcard-mbottom menu-shadow'>
        <div className={`fcard ${existingFood ? 'fcard-mod': ''}`} >
          <button className='fadd' onClick={this.addFoodToTemp.bind(this, food)}>
            <img src='/images/plus.svg' alt="add food"/>
          </button>
          <div className='fctrl'>
            <button className='fdecre' onClick={() => this.props.reduceFoodTemp(food)}>
              <img src="/images/minus.svg" alt="decrement food" />
            </button>
            <button className='fincre' onClick={this.addFoodToTemp.bind(this, food)}>
              <img src="/images/plus-orange.svg" alt="decrement food"/>
              <span>Add</span>
            </button>
          </div>
          <div className='fnumb'>
            <p>{`${existingFood ? existingFood.quantity: ''}`}</p>
            <p><img src='/images/bag.svg' alt='food selectd'/>In your aklybox</p>
          </div>
          <img src={food.images[0]} alt="food image" className='fimg'/>

          <div className='fdtl'>
            <div className='fname'>
              <p>{food.name}</p>
            </div>
            <div className='fcal'>
              <img src='/images/bonfire.svg' alt='food calorie'/>
              <span>{food.calorie} Kcal</span>
            </div>
            <div className='fprice'>
            <p><sup>QR</sup>{food.price}</p>
          </div>
          </div>

          <div className='fcard-overlay'>
          </div>

        </div>
      </div>
    })
  }

  checkBoxChange(event) {
    console.log('.., ', event.target.value, event.target.checked);
    if (event.target.checked) {
      this.props.addFilterTag(event.target.value)
      this.props.filterFood()
    } else {
      this.props.removeFilterTag(event.target.value)
      this.props.filterFood()
    }

  }

  getTagImage(tag) {
    if(!this.props.tags.filterTagList || this.props.tags.filterTagList.length == 0)
      return tag.images[0]

    return _.includes(this.props.tags.filterTagList.map(ft => ft._id) , tag._id) ? tag.images[0] : tag.images[1]
  }

  generateFoodTagList() {
    return this.props.tags.foodTags ?
      this.props.tags.foodTags.map((tag) => {
        return <li className='sitem' key={tag._id}>
          <div className='sitem-wrapper'>
            <div className='sitem-tag'>
                <img src={this.getTagImage(tag)} alt=""/>
              <span>{tag.name}</span>
            </div>

            <div className='sitem-checkbox'>
              <label>
                <div className="checkbox">
                  <input type="checkbox" value={tag._id} onChange={this.checkBoxChange.bind(this)}/>
                </div>
              </label>
            </div>
          </div>
        </li>
      }) : <li>hello</li>

  }

  render() {
    let stateClass= this.state.clicked? 'mp_foodlist_dropdown_show' : 'mp_foodlist_dropdown_hide';

    return (
      <ReactCSSTransitionGroup
        transitionName="subscription-menu"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}>
          <div className='mp_foodlist'>
              <div className='mp_foodlist_header'>
                  <div className="mp-foodlist-header-container container ca-container">
                       <div className='food_list_qsort'>
              <div className='food_list_q'>
                <h4>Add lunch (3)</h4>
              </div>
              <div className='food_list_sorticon'>
                <button className='btn butn_filter' onClick={this.show.bind(this)}><img src='/images/filter.svg'
                                                                                        alt='filter food'/></button>
              </div>
              <div className={'ca-dropdown ' + stateClass}>
                <div className='dropdown_sorticon'>
                  <button className='btn butn_filter' onClick={this.show.bind(this)}><img src='/images/filter-white.svg'
                                                                                          alt='filter food'/></button>
                </div>
                <ul className='ca_dropdown_items'>
                  <form>
                    {this.generateFoodTagList()}
                  </form>
                </ul>
              </div>
            </div>
            <div className='food_list_appr'>
              <button className='btn butn_fc' onClick={() => this.props.hideFoodPopUp()}><img src='/images/roundcross.svg' alt='cancel'
                                                   /></button>
              <button className='btn butn_fo' onClick={() => this.props.addFood()}><img src='/images/roundtick.svg' alt='ok' /></button>
            </div>
                </div>
          </div>

          <div className='flist'>
                <div className='flist-container container ca-container'>
                    <div className='row'>
                      {this.generateFoodList()}
                    </div>
                </div>
          </div>
          {/*<div className='flist-backdrop'></div>*/}
        </div>
        </ReactCSSTransitionGroup>
    )
  }
}

export default SubscriptionEditFoodListComponent
