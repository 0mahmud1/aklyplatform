import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { getMealPlans, readMealPlanAction } from '../actions/subscription';
import { getFoods } from '../../core/actions/index.js'
import SetMealplanDetailsComponent from '../components/set-mealplan-details.js'

class SetMealplanDetailsContainer extends Component {
  componentWillMount() {
    this.props.readMealPlanAction(this.props.params._id)
  }

  render() {
    const shouldRender = !_.isEmpty(this.props.mealPlan) ;
    console.log("MealPlanEditContainer...shouldRender=>>>",shouldRender);
    return (
      shouldRender ?
      <SetMealplanDetailsComponent {...this.props} />
      :
      <div className="spinner"></div>
    );
  }
}

// SubscriptionOrderListContainer.propTypes = {};
// SubscriptionOrderListContainer.defaultProps = {};

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    mealPlan: store.mealPlans.mealPlan,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getMealPlans: () => dispatch(getMealPlans()),
    readMealPlanAction: (_id) => dispatch(readMealPlanAction(_id)),
    setPlanType: (type) => dispatch({ type: 'SET_MEALPLAN_TYPE', payload: type}),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SetMealplanDetailsContainer);
