import React from 'react'
import { Route, IndexRoute } from 'react-router' 
import OrdersListContainer from '../containers/SubscriptionOrderList.js'
import OrderDetailsContainer from '../containers/SubscriptionOrderDetails.js'
import * as AuthService from '../../../services/auth.js'


export default function(){
    return(
        <Route path='orders' onEnter={AuthService.isAuthorization}>
            <IndexRoute component={OrdersListContainer}/>
            {/*<Route path=':_id' component={OrderDetailsContainer} />*/}
        </Route>
    )
}