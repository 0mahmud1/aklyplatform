import axios from 'axios';
import { browserHistory } from 'react-router';
import toastr from 'toastr';
import * as API from '../../libs/apiList.js';


export function createOrder(order, cb) {
  console.log('createOrder ===>>  action', order);
  // REDUCER IN DASHBOARD MODULES/authenticate folder
  return function (dispatch) {
    axios.post(
      API.ORDERS,
      order
    ).then((response) => {
      console.log('frm dispatch ', response);
      dispatch({ type: 'CUSTOMER_ORDER_CREATE_RESOLVED', payload: response.data.data });
      dispatch({ type: 'CLEAR_ONDEMAND_DELIVERY' });
      //toastr.success('Order created successfully!');
      if (cb) {
        cb(response);
      }
    }).catch((err) => {
      //dispatch({type: 'Order faile', payload: err});
      toastr.error(err.data.message);
      console.log('err , ', err);
    });
  };
}

export function getOrder(_id, cb) {
  return function (dispatch) {
    axios.get(
      API.ORDERS + '/' + _id
    ).then((response) => {
      //console.log(response);
      dispatch({ type: 'READ_ORDER_RESOLVED', payload: response });
      if (cb) {
        cb(response.data.data);
      }

      //toastr.success('order loaded');
    }).catch((err) => {
      dispatch({ type: 'READ_ORDER_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getOrderByRefId(orderRefId, cb) {
  return function (dispatch) {
    axios.get(
      API.ORDERS, {
        params: {
          orderRefId: orderRefId,
        },
      }
    ).then((response) => {
      //console.log(response);
      dispatch({ type: 'READ_ORDER_RESOLVED', payload: response });
      if (cb) {
        cb(null,response.data.data);
      }

      //toastr.success('order loaded');
    }).catch((err) => {
      dispatch({ type: 'READ_ORDER_REJECTED', payload: err });
      toastr.error(err,null);
    });
  };
}

export function getOrdersOfCustomer(email) {
  return function (dispatch) {
    axios.get(
      API.ORDERS, {
        params: {
          email: email,
        },
      }
    ).then((response) => {
      //console.log(response);
      dispatch({ type: 'GET_ORDERS_RESOLVED', payload: response });
    }).catch((err) => {
      dispatch({ type: 'GET_ORDERS_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getDeliveriesByOrderRefId(orderRefId) {
  return function (dispatch) {
    dispatch({ type: 'GET_DELIVERIES_FOR_UPDATE_PENDING' });
    axios.get(
      API.DELIVERIES, {
        params: {
          orderRefId: orderRefId,
        },
      }
    ).then((response) => {
      console.log('get Delivery List... : ', response);
      dispatch({ type: 'GET_DELIVERIES_FOR_UPDATE_RESOLVED', payload: response.data.data });
      //toastr.success('Delivery List Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_DELIVERIES_FOR_UPDATE_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function createDelivery(data, cb) {
  return function (dispatch) {
    axios.post(
      API.DELIVERIES,
      data
    ).then((response) => {
      //console.log('Post Delivery success... : ', response);
      dispatch({
        type: 'CREATE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED',
        payload: response.data.data,
      });
      if (cb) {
        cb(null,response.data.data);
      }
    }).catch((err) => {
      console.log('Post Delivery error... : ', err);
      cb(null,err);
    });
  };
}

export function updateDelivery(_id, data, cb) {
  return function (dispatch) {
    dispatch({ type: 'UPDATE_DELIVERY_PENDING' });
    axios.put(
      API.DELIVERIES + '/' + _id,
      data,
      { params : { updateOrder: true}}
    ).then((response) => {
      console.log('Update Delivery success... : ', response);
      dispatch({
        type: 'UPDATE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED',
        payload: response.data.data,
      });
      if (cb) {
        cb(null,response.data.data);
      }
    })
    .catch((err) => {
      console.log('Update Delivery error... : ', err);
      cb(err,null);
    });
  };
}

export function deleteDelivery(_id,cb) {
  return function (dispatch) {
    axios.delete(
      API.DELIVERIES +'/' + _id
    ).then((response) => {
      console.log('Delete Delivery success... : ', response);
      dispatch({
        type: 'DELETE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED',
        payload: response.data.data,
      });
      if (cb) {
        cb(null,response.data.data);
      }
    }).catch((err) => {
      console.log('Delete Delivery error... : ', err);
      cb(err,null);
    });
  };
}
