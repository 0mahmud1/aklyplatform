import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { getOrdersOfCustomer } from '../actions/order';
import SubscriptionOrderList from '../components/subscription-order-list';

class SubscriptionOrderListContainer extends Component {
  componentWillMount() {
    this.props.getOrders(this.props.auth.user.email);
  }

  render() {
    return (
      this.props.orders.orderList ?
      <SubscriptionOrderList {...this.props} /> : null
    );
  }
}

// SubscriptionOrderListContainer.propTypes = {};
// SubscriptionOrderListContainer.defaultProps = {};

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    orders: store.orders,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getOrders: (email) => dispatch(getOrdersOfCustomer(email)),
    setPlanType: (type) => dispatch({ type: 'SET_MEALPLAN_TYPE', payload: type}),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionOrderListContainer);
