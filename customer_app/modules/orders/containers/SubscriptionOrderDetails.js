import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import SubscriptionOrderDetails from '../components/subscription-order-details';
import { getFoods } from '../../core/actions/index.js';
import { createDelivery, updateDelivery, deleteDelivery, getOrder, getDeliveriesByOrderRefId } from '../actions/order.js';
import SubscriptionEditDelivery from '../components/subscription-delivery-edit';

class SubscriptionOrderDetailsContainer extends Component {
  componentWillMount() {
    let _this = this;
    this.props.getOrder(this.props.params._id, order => {
      _this.props.getDeliveriesByOrderRefId(order.orderRefId);
    });
    this.props.getFoods();
  }

  render() {
    return (
      this.props.orders.order && this.props.subscriptionEdit.deliveries && this.props.subscriptionEdit.deliveries.length > 0 ?
        this.props.foods.foodList.length > 0 && this.props.subscriptionEdit.selectedDelivery ?
          <SubscriptionEditDelivery {...this.props} /> : <SubscriptionOrderDetails {...this.props} />
        : null
    );
  }
}

// SubscriptionOrderListContainer.propTypes = {};
// SubscriptionOrderListContainer.defaultProps = {};

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    orders: store.orders,
    subscriptionEdit: store.subscriptionEdit,
    foods: store.foods,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getOrder: (email, cb) => dispatch(getOrder(email, cb)),
    getDeliveriesByOrderRefId: (ref) => dispatch(getDeliveriesByOrderRefId(ref)),
    getFoods: () => dispatch(getFoods()),
    selectDelivery: (dv) => dispatch({
      type: 'SELECT_SPECIFIC_DELIVERY',
      payload: dv,
    }),
    selectDeliveryDate: (date) => dispatch({
      type: 'SELECT_DAY_FOR_NEW_DELIVERY',
      payload: date,
    }),
    createNewDelivery: (data, cb) => dispatch(createDelivery(data, cb)),
    updateSubscriptionDelivery: (_id, data, cb) => dispatch(updateDelivery(_id, data, cb)),
    deleteSubscriptionDelivery: (data) => dispatch(deleteDelivery(data)),
    createFoodTags: () => dispatch({ type: 'LIST_ALL_FOOD_TAGS' }),
    addFilterTag: (tag) => dispatch({ type: 'ADD_FILTER_TAGLIST', payload: tag }),
    removeFilterTag: (tag) => dispatch({ type: 'REMOVE_FILTER_TAGLIST', payload: tag }),
    filterFood: () => dispatch({ type: 'FILTER_FOOD' }),
    addFoodTemp: (food) => dispatch({ type: 'ADD_FOOD_DELIVERY_EDIT_TEMP', payload: food }),
    reduceFoodTemp: (food) => dispatch({ type: 'REDUCE_FOOD_DELIVERY_EDIT_TEMP', payload: food }),
    resetSelectedDelivery: () => dispatch({ type: 'RESET_SELECTED_DELIVERY' }),
    addDeliveryToList: (data) => dispatch({ type: 'ADD_DELIVERY_TO_LIST', payload: data }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionOrderDetailsContainer);
