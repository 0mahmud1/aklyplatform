import React, {
  Component,
  PropTypes,
} from 'react';
import {browserHistory} from 'react-router'

import _ from 'lodash'
import moment from 'moment'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import NoContent from '../../core/components/no-content.js';

class SubscriptionOrderList extends Component {
  generateOrderList() {
    const _this = this;
    let orders = _.filter(this.props.orders.orderList, o => o.orderType === 'subscription')

    return orders.map(order => {
    //   return <tr onClick={() => browserHistory.push(`/subscription-order/${order._id}`)} key={order._id}>
    //     <td>{order.orderRefId}</td>
    //     <td>{moment(order.orderDate).format('YYYY-MM-DD')}</td>
    //     <td>{order.total}</td>
    //   </tr>
      return <tr onClick={() => {this.props.setPlanType(order.planType);localStorage.setItem('deliveries', JSON.stringify(order.deliveries));browserHistory.push('/subscription/'+order.orderRefId)}} key={order._id}>
        <td>{order.orderRefId}</td>
        <td>{moment(order.orderDate).format('YYYY-MM-DD')}</td>
        <td>{order.planType}</td>
        <td>{order.total}</td>
      </tr>
    })
  }

  tableSkeleton() {
    if (!this.props.orders || this.props.orders.orderList.length == 0)
      return (
        <div className='nocontent-wrapper container ca-container'>
          <NoContent text='No data available' />
        </div>
      );

    return <table className="table table-hover subscription-order">
      <thead>
      <tr>
        <th>Ref Id</th>
        <th>Order date</th>
        <th>Order Type</th>
        <th>Total</th>
      </tr>
      </thead>
      <tbody>
        {this.generateOrderList()}
      </tbody>
    </table>
  }

  generateTabs() {
        const TabNames = ['On Demand', 'Subscription'];

        return TabNames.map(el => {
            return (
                <Tab key={el}>{el}</Tab>
            )
        })
    }

  generateOnDemandTabPanel() {
    return (
      <TabPanel>
        <h3>On Demand table goes here..</h3>
      </TabPanel>
    );
  }

  generateSubscriptionTabPanel() {
    return (
      <TabPanel>
        {this.tableSkeleton()}
      </TabPanel>
    );
  }

  render() {
    return (
        <div className="container ca-container" style={{height: 'calc(100vh - 84px)'}}>
            <div className='order-list'>
              <div className='react-tabs-customtheme'>
                <Tabs>
                  <TabList>
                    {this.generateTabs()}
                  </TabList>

                  {this.generateOnDemandTabPanel()}

                  {this.generateSubscriptionTabPanel()}
                </Tabs>
              </div>
            </div>
        </div>
    )
  }
}
export default SubscriptionOrderList;
