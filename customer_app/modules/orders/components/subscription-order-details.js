import React, {
  Component,
  PropTypes,
} from 'react';

import _ from 'lodash'
import moment from 'moment'

import NoContent from '../../core/components/no-content.js';

class SubscriptionOrderDetails extends Component {
  generateOrderList() {
    let orders = _.filter(this.props.subscriptionEdit.deliveries, o => o.orderType === 'subscription')

    return orders.map(order => {
      return <tr onClick={() => this.props.selectDelivery(order)} key={order._id}>
        <td>{order.deliveryRefId}</td>
        <td>{moment(order.deliveryDate).format('YYYY-MM-DD')}</td>
        <td>{order.total}</td>
      </tr>
    })
  }

  tableSkeleton() {
    if (!this.props.subscriptionEdit.deliveries || this.props.subscriptionEdit.deliveries.length == 0)
      return (
        <div className='nocontent-wrapper container ca-container'>
          <NoContent text='No data available' />
        </div>
      );

    return <table className="table table-hover subscription-order">
      <thead>
      <tr>
        <th>Ref Id</th>
        <th>Delivery date</th>
        <th>Total</th>
      </tr>
      </thead>
      <tbody>
      {this.generateOrderList()}
      </tbody>
    </table>


  }

  render() {
    return (
        <div className="container ca-container" style={{height: 'calc(100vh - 84px)'}}>
            <div style={{paddingTop: '4rem'}}>
                {this.tableSkeleton()}
                <div className='text-center'>
                    <button className="btn butn butn_default" style={{fontSize: '16px'}} onClick={() => this.props.selectDelivery({})}>Add New</button>
                </div>
            </div>
        </div>
    )
  }
}
export default SubscriptionOrderDetails;
