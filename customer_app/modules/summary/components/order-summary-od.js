'use strict'

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import toastr from 'toastr'


import PackIconComponent from '../../core/components/icon/pack.js'
import PickupIconComponent from '../../core/components/icon/pickup.js'
import TickIconComponent from '../../core/components/icon/tick.js'
//import CollapseComponent from './collapse/collapse.js'

class ODSummaryComponent extends Component {
  componentDidMount() {
    let _this = this;
    $(".js-example-select").select2({
      minimumResultsForSearch: -1
    }).on('change',function (event) {
      //console.log(_this, event.target.getAttribute('data'));
      _this.props.changeFoodQtyToCart({_id: event.target.getAttribute('data')},Number(event.target.value)) // emulating food data, only _id will be there
    })
    ;
  }

  generateDeliveries() {
    return this.props.onDemand.deliveries && this.props.onDemand.deliveries.length > 0 ?
      this.props.onDemand.deliveries.map(dlv => {
        return <li key={dlv.item._id} className='caod_singlefood'>
          <div className='fd'>
            <div className="media fd-media">
              <div className="media-left fd-img">
                <img className="media-object" src={dlv.item.images[1]} alt="food imafe"/>
              </div>
              <div className="media-body fd-name">
                <p className="media-heading">{dlv.item.name}</p>
              </div>
            </div>
          </div>
          <div className='fdq-select'>
            <select ref={dlv.item._id} className="js-example-select ca_select2" data={dlv.item._id}  defaultValue={dlv.quantity.toString()}>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className='cal_summary'>
            <div className='cal_icon'></div>
            <div className='cal_text'>
              <p>400 Kcal</p>
            </div>
          </div>
          <div className='osum_price'>
            <p>QR {Number(dlv.item.price * dlv.quantity).toFixed(2)}</p>
          </div>

          <div className='odfd-cancel' onClick={() => this.props.removeItemFromCart(dlv.item)}>
            <img src='/images/cross.svg' alt="Delete Food"/>
          </div>
        </li>
      }) : null
  }

  continueToCheckout() {
    //console.log('xxx ',this.props)

    this.props.clearMealPlanDeliveries()
    if(!this.props.onDemand.deliveries || this.props.onDemand.deliveries.length === 0){
      toastr.warning('No Food Selected');
      return;
    }

    localStorage.setItem('onDemand', JSON.stringify(this.props.onDemand.deliveries))
    if(this.props.auth.user && this.props.auth.userProfile) {
      browserHistory.push('/checkout')
    } else {
      this.props.auth0Lock.login('/checkout');
    }
  }

  render() {
    let total = this.props.onDemand.total
    return (
      <div>
        <div className='caadditionalbar mpcheckoutbar'>
          <div className='caadditionalbar-content mpcheckoutbar-content container ca-container'>
            <div className='ca_steprogress'>
              <div className='steprogress detailed_view'>
                <div className='progress_section'>
                  <div className='stprogress'>
                    <div className='progressbar_section'>
                      <div className='stprogressbar'>
                        <div className='horizontal_box'>
                          <div className='base_bar'></div>
                          <div className='base_bar'></div>
                        </div>
                      </div>
                    </div>

                    <div className='progressbar_section'>
                      <div className='horizontal_box progress_status_section'>
                        <div className='steps step1'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <PackIconComponent fillColor='#ED731F'/>
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>

                        <div className='steps step2'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <PickupIconComponent fillColor='#A7A7A7'/>
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>

                        <div className='steps step3'>
                          <div className='stepsicons'>
                            <div className='vertical_box'>
                              <ol className='nostyle vertical_box'>
                                <li className='step_progress_icon'>
                                  <div className='icon_wrapper'>
                                    <TickIconComponent fillColor='#A7A7A7'/>
                                  </div>
                                </li>
                              </ol>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='mp-osum container ca-container'>
          <div className='mp-osum-content'>
            <div className='mp-osum-details'>
              <div className='osdtls-header'>
                <h3>
                  Order Summary
                </h3>
              </div>
              <div className='osumlist-od'>
                <ul className='caod_foods'>
                  {this.generateDeliveries()}
                  <li className='osum_ttlp'>
                    <p>Total:</p>
                    <p><sup>QR</sup>{total}</p>

                  </li>
                </ul>
                <p className="text-center"><button className="btn butn butn_default" onClick={this.continueToCheckout.bind(this)}>Continue</button></p>
              </div>
            </div>
            <div className='mp-osum-img'>
              <div className='osum-foodimg'>
                <img src='/images/osum.png' alt='order summary image'/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ODSummaryComponent
