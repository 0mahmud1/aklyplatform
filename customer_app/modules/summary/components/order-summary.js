'use strict'

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'

import PackIconComponent from '../../core/components/icon/pack.js'
import PickupIconComponent from '../../core/components/icon/pickup.js'
import TickIconComponent from '../../core/components/icon/tick.js'
import CollapseComponent from '../../core/components/collapse/collapse.js'

import moment from 'moment'
import _ from 'lodash'

class MPOrderSummaryComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {clicked: false};
  }

  show() {
    this.setState({clicked: !this.state.clicked});
  }

  calculateOneDaySum(date) {
    //let date = moment(d);

    let filteredDeliveries = this.props.mealPlan.deliveries.filter((fd) =>{
      return fd.deliveryDate.format('YYYY-MM-DD') === date
    });
    console.log('ccod ', date, filteredDeliveries)

    return _.sumBy(filteredDeliveries,sum => Number(sum.total)).toFixed(2)
  }

  calculateOneDayProperties(date,property) {
    //let date = moment(d);

    let filteredDeliveries = this.props.mealPlan.deliveries.filter((fd) =>{
      return fd.deliveryDate.format('YYYY-MM-DD') === date
    });
    console.log('ccod calorie ', date, filteredDeliveries.length)

    return _.sumBy(filteredDeliveries, aDay => _.sumBy(aDay.meals, meal=> Number(meal.item[property])))

    //return _.sumBy(filteredDeliveries.meals, aDay => Number(aDay.item[property]))
  }

  continueToCheckout() {
    //console.log('xxx ',this.props)
    if(this.props.auth.user && this.props.auth.userProfile) {
      this.props.clearOnDemandCart()
      browserHistory.push('/checkout')
    } else {
      this.props.clearOnDemandCart()
      this.props.auth0Lock.login('/checkout');
    }
  }


  componentDidMount() {
    $(".js-example-select").select2({
      minimumResultsForSearch: -1
    });
  }

  generateDeliveryList() {
    let uniqDates = this.props.mealPlan.deliveries.length > 0 ? _.uniq(this.props.mealPlan.deliveries.map(i=> i.deliveryDate.format('YYYY-MM-DD'))) : []
    return this.props.mealPlan.deliveries.length > 0 ?
      uniqDates.map((d) => {
        return <li key={`${d}`} className='cadp_singleday'>
          <div className='abovefold_content'>
            <div className='collapse_indicator icon_not_collapsed'>

            </div>
            <div className='day_name'>
              <p>{moment(d).format('dddd Do MMM')}</p>
            </div>
            <div className='cal_stat'>
              <div className='cal_summary'>
                <div className='cal_icon'></div>
                <div className='cal_text'>
                  <p>{this.calculateOneDayProperties(d,'calorie')} Kcal</p>
                </div>
              </div>
              <div className='stat_protein'>
                <p>Protein {this.calculateOneDayProperties(d,'protein')}g</p>
              </div>
              <div className='stat_carbs'>
                <p>Carbs {this.calculateOneDayProperties(d,'carbs')}g</p>
              </div>
              <div className='stat_fats'>
                <p>Fats {this.calculateOneDayProperties(d,'fat')}g</p>
              </div>
            </div>
            <div className='osum_price'>
              <p>QR {this.calculateOneDaySum(d)}</p>
            </div>
          </div>
        </li>
      }) : <h1>sorry :( </h1>
  }

  getTotalOfDeliveries() {
    return this.props.mealPlan.deliveries.length > 0 ?
      _.sumBy(this.props.mealPlan.deliveries,function (dv) {
        return Number(dv.total)
      }).toFixed(2)
      : 0
  }

  render() {
    let first = this.props.mealPlan.deliveries.length>0 ? this.props.mealPlan.deliveries[0].deliveryDate : moment(),
      last = this.props.mealPlan.deliveries.length>0 ? this.props.mealPlan.deliveries[this.props.mealPlan.deliveries.length-1].deliveryDate : moment()
    return (
      <div>
        <div className='caadditionalbar mpcheckoutbar'>
         <div className='caadditionalbar-content mpcheckoutbar-content container ca-container'>
             <div className='ca_steprogress'>
            <div className='steprogress detailed_view'>
              <div className='progress_section'>
                <div className='stprogress'>
                  <div className='progressbar_section'>
                    <div className='stprogressbar'>
                      <div className='horizontal_box'>
                        <div className='base_bar'></div>
                        <div className='base_bar'></div>
                      </div>
                    </div>
                  </div>

                  <div className='progressbar_section'>
                    <div className='horizontal_box progress_status_section'>
                      <div className='steps step1'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <PackIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>

                      <div className='steps step2'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <PickupIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>

                      <div className='steps step3'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <TickIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

        <div className='mp-osum container ca-container'>
          <div className='mp-osum-content'>
            <div className='mp-osum-details'>
              <div className='osdtls-header'>
                <h3>
                  Order Summary
                </h3>
                <p>
                  You have selected a customized meal plan from <span>{`${first.format('MMMM Do')} to ${last.format('MMMM Do')}`}</span>
                </p>
              </div>
              <div className='osumlist'>
                <ul className='cadp_days'>
                  {this.generateDeliveryList()}

                  <li className='osum_ttlp'>
                    <p>Total:</p>
                    <p><sup>QR</sup>{this.getTotalOfDeliveries()}</p>

                  </li>
                </ul>
                <p className="text-center"><button className="btn butn butn_default" onClick={this.continueToCheckout.bind(this)}>Continue</button></p>
              </div>
            </div>
            <div className='mp-osum-img'>
              <div className='osum-foodimg'>
                <img src='/images/osum.png' alt='order summary image'/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MPOrderSummaryComponent
