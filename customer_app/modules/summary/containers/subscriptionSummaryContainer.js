import React, {
  Component,
  PropTypes,
} from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';

import MPOrderSummaryComponent from '../components/order-summary';

class OrderSummaryContainer extends Component {
  render() {
    return (
      <MPOrderSummaryComponent {...this.props} />
    );
  }
}

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    mealPlan: store.cuMealPlan,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clearOnDemandCart: () => dispatch({ type: 'CLEAR_ONDEMAND_DELIVERY' }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderSummaryContainer);
