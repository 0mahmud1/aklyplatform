import React, {
  Component,
  PropTypes,
} from 'react';

import { connect } from 'react-redux';
import ODSummaryComponent from '../components/order-summary-od';

class ODSummaryContainer extends Component {
  render() {
    return (
      <ODSummaryComponent {...this.props} />
    );
  }
}

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    onDemand: store.onDemand,
    foods: store.foods,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changeFoodQtyToCart: (food, quantity) => dispatch({ type: 'CHANGE_FOOD_QTY_CART', payload: { food, quantity }}),
    removeItemFromCart: (food) => dispatch({ type: 'REMOVE_ITEM_CART', payload: food }),
    clearMealPlanDeliveries: () => dispatch({ type: 'CLEAR_MEALPLAN_DELIVERY' }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ODSummaryContainer);
