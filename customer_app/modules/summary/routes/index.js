import React from 'react'
import { Route, IndexRedirect } from 'react-router'
import subscriptionSummaryContainer from '../containers/subscriptionSummaryContainer.js'
import onDemandSummaryContainer from '../containers/onDemanSummaryContainer.js'

export default function () {
    return (
        <Route path='summary'>
            <IndexRedirect to='on-demand' />
            <Route path='on-demand' component={onDemandSummaryContainer} />
            <Route path='subscription' component={subscriptionSummaryContainer} />
        </Route>
    )
}