from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project

import sys, os

abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)

# --------------------------------------------
# Akly Platform AWS development Machines
# --------------------------------------------

class FabricException(Exception):
    pass

def dev():
    print "Connecting to Akly AWS dev Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.ubuntu_version = '14.04'

    env.key_filename = abspath('akly.pem')
    env.nginx_config = abspath('nginx.conf')

    env.hosts = [
        '54.251.159.245'
        ]
    env.serverpath = '/home/ubuntu/aklyplatform'


    env.graceful = False
    env.home = '/home/ubuntu'

    # mongodb settings
    # env.mongo_user = 'admin'
    # env.mongo_password = 'urbanchef1qweqwe23'

    env.rsync_exclude = [
        ".git",
        "client-app/node_modules",
        "client-app/modules/config.js"
        "bundle.js",
        "bundle.*.js",
        "api-server/node_modules",

    ]

    return





# def mongoDev():
#     print "Connecting to Akly Platform AWS mongoDB dev Ec2 machine"
#
#     env.setup = True
#     env.user = 'ubuntu'
#     env.key_filename = abspath('akly.pem')
#     env.hosts = [
#         '54.179.163.106'
#         ]
#
#     return

# --------------------------------------------
# Installing akly Platform
# --------------------------------------------

def install_nginx():

    print 'Installing Nginx'
    sudo('apt-get -y install nginx')

    put('%s' % (env.nginx_config),
        '/etc/nginx/sites-enabled/',
        use_sudo=True)

    sudo("service nginx restart")
    return

def install():
    if env.setup:
        print 'Start installing UrbanChef Platform'
        install_nodejs()
        install_pm2()

def install_pm2():
    print 'Installing PM2'
    sudo('npm install -g pm2')
    return


def install_nodejs():
    print 'Installing latest nodejs '
    run('cd ~;curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh')
    sudo('cd ~; bash nodesource_setup.sh')
    sudo('apt-get -y install nodejs build-essential')
    return

# def install_mongodb():
#     print 'Installing mongodb'
#     sudo('apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10')
#     run('echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list')
#     sudo('apt-get update')
#     sudo('apt-get install -y mongodb-org')





def deploy(restart_service=None):

    # sync code base
    sync_code_base()
    client_build()
    run('pm2 restart api-server')
    run('pm2 restart /usr/bin/http-server --name api-server-doc -- -p 5000 -d false')
    return


def sync_code_base():
    print 'syncing code base'
    rsync_project(env.serverpath, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    return



def client_build():
    run('cd %s/client-app/; npm install; npm run dev ' % env.serverpath)
    run('pm2 restart client-app')
    return

def install_npm():
    # print 'Installing npm for client and server'
    try:
        t = run('cd %s/client-app/; npm install ' % env.serverpath)
        print t

    except FabricException:
        pass
    run('cd %s/client-app/; npm run prod ' % env.serverpath)
    try:
        run('cd %s/api-server/; npm install ' % env.serverpath)

    except FabricException:
        pass
    return

def setup_pm2():
    print 'pm2 config'
    run('cd %s/client-app/; pm2 start --name client-app npm -- start' % env.serverpath)
    run('cd %s/api-server/; pm2 start --name api-server npm -- start' % env.serverpath)


    if env.ubuntu_version == '14.04':
        # deamon for ubuntu 14.04
        sudo('env PATH=$PATH:/usr/local/bin pm2 startup -u ubuntu')
    else:
        # deamon for  ubuntu 16.04
        run('pm2 startup systemd')
        sudo('env PATH=$PATH:/usr/bin pm2 startup systemd -u ubuntu --hp /home/ubuntu')
        #sudo('su -c "env PATH=$PATH:/usr/bin pm2 startup systemd -u ubuntu --hp /home/ubuntu"')
    return



# --------------------------------------------
# fabric script end
# --------------------------------------------
