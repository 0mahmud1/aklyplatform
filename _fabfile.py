from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project

import sys, os

# sys.path.append(os.path.abspath(os.path.dirname(__file__)))
# os.environ['DJANGO_SETTINGS_MODULE'] = 'alemhealth.settings'
abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)

# --------------------------------------------
# Akly Platform AWS development Machines
# --------------------------------------------


def dev():
    print "Connecting to Akly AWS dev Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.key_filename = abspath('akly.pem')
    env.hosts = [
        '54.179.163.106'
        ]
    env.serverpath = '/home/ubuntu/aklyplatform'

    # # local config paths
    #env.urbanchef_conf = 'dev-ops/dev/urbanchef.conf'
    #env.mrdoc_conf = 'dev-ops/dev/mrdoc.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mongodb settings
    env.mongo_user = 'admin'
    env.mongo_password = 'urbanchef1qweqwe23'

    env.rsync_exclude = [
        ".git",
        "node_modules",
        "client-app/modules/config.js"
        "bundle.js",
        "bundle.*.js"
    ]

    return


#  test server for mobile development
def test():
    print "Connecting to UrbanChef AWS test Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.key_filename = abspath('urbanchef.pem')
    env.hosts = [
        '54.179.174.158'
        ]
    env.serverpath = '/home/ubuntu/urbanchef'

    # # local config paths
    env.urbanchef_conf = 'dev-ops/dev/urbanchef.conf'
    env.mrdoc_conf = 'dev-ops/dev/mrdoc.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mongodb settings
    env.mongo_user = 'admin'
    env.mongo_password = 'urbanchef1qweqwe23'

    env.rsync_exclude = [
        "platform/.meteor/local",
        "marketing/.meteor/local",
        ".git",
        "node_modules",
        "platform/packages/npm-container/",
        #"docs"
    ]

    return

def celery():

    print "Connecting to UrbanChef AWS dev celery Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.key_filename = abspath('urbanchef.pem')
    env.hosts = [
        '54.169.237.183'
        ]
    env.serverpath = '/home/ubuntu/urbanchef'

    # # local config paths
    env.celery_conf = 'dev-ops/dev/celery.conf'
    env.celery_beat_conf = 'dev-ops/dev/celery-beat.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mongodb settings
    env.mongo_user = 'admin'
    env.mongo_password = 'urbanchef1qweqwe23'

    # env.rsync_exclude = [
    #     "platform/",
    #     "marketing/",
    #     ".git",
    #     "node_modules",
    #     "platform/packages/npm-container/",
    #     #"docs"
    # ]

    return

def mongoDev():
    print "Connecting to Akly Platform AWS mongoDB dev Ec2 machine"

    env.setup = True
    env.user = 'ubuntu'
    env.key_filename = abspath('akly.pem')
    env.hosts = [
        '54.179.163.106'
        ]

    return

# --------------------------------------------
# Installing akly Platform
# --------------------------------------------


def install():
    if env.setup:
        print 'Start installing UrbanChef Platform'
        install_meteor()
        install_supervisor()
        create_supervisor_services()

def install_nvm():
    print "Installing nvm (Node Version Manager) "
    run('curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash')

def graphicsmagick():
    print "Installing graphicsmagick"
    sudo('apt-get install graphicsmagick')

def install_mongodb():
    print 'Installing mongodb'
    sudo('apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10')
    run('echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list')
    sudo('apt-get update')
    sudo('apt-get install -y mongodb-org')


def deploy(restart_service=None):

    # sync code base
    sync_code_base()

    # meteor auto start when code change , so we can skip restart service
    #print 'Restart all supervisor services ...'
    if restart_service == "restart":
        sudo('supervisorctl reread all')
        sudo('supervisorctl restart all')


def sync():
    print 'rsync API-SERVER code base'
    # local('rsync -rave "ssh -i %s" %s  %s/* %s@%s:%s' % (
    #     env.key_filename,
    #     ' '.join(env.rsync_exclude),
    #     abspath(''), env.user,
    #     env.hosts[0],
    #     env.serverpath
    #     )
    # )

    #rsync_project(env.serverpath, '/' + abspath() + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    rsync_project(env.serverpath, abspath('') + '*', exclude=env.rsync_exclude, delete=True, default_opts='-rvz')
    #rsync_project(env.serverpath + '/platform/.meteor/packages', abspath('') + 'platform/.meteor/packages', default_opts='-rvz')


    # local('rsync -rave "ssh -i %s"   %s.meteor/packages %s@%s:%s/.meteor/' % (
    #     env.key_filename,
    #     #abspath(''),
    #     #' '.join(env.rsync_exclude),
    #     abspath(''), env.user,
    #     env.hosts[0],
    #     env.serverpath
    #     )
    # )


def sync_celery_code_base():
    print 'rsync UrbanChef celery code base'
    rsync_project(env.serverpath, abspath('') + '/dev-ops/celery/*', delete=True, default_opts='-rvz')


def generate_docs():

    mrdoc_exclude = '.meteor,client,packages,private,public,tests,lib/config,routes,server/config,server/fixtures,server/startup'

    local('mr-doc -s %s/platform/ -o %s/docs/ -n "UrbanChef" -i %s' % (abspath(''), abspath(''), mrdoc_exclude))

# --------------------------------------------
# fabric script end
# --------------------------------------------
