import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* Use home page. */
app.get('*', function (req, res) {
  res.render('index', {
    title: 'Local :: Food Provider',
    stylesheet: app.get('env') === 'production' ? '/build/bundle.min.css' : '/build/bundle.css',
    javascript: app.get('env') === 'production' ? '/build/bundle.min.js' : '/build/bundle.js',
  });
});

export default app;
