var config = require('config');
var path = require('path');
var webpack = require('webpack');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var CompressionPlugin = require('compression-webpack-plugin');

// start for execute additional shell command before and after webpack command if not in localhost

if (config.util.getEnv('NODE_ENV') != 'localhost') {
  // start webpack additional shell command

  var exec = require('child_process').exec;

  function puts(error, stdout, stderr) {
    console.log(stdout);
  }

  function WebpackShellPlugin(options) {
    var defaultOptions = {
      onBuildStart: [],
      onBuildEnd: [],
    };
    this.options = Object.assign(defaultOptions, options);
  }

  WebpackShellPlugin.prototype.apply = function (compiler) {
    const options = this.options;

    compiler.plugin('compilation', compilation => {
      if (options.onBuildStart.length) {
        console.log('Executing pre-build scripts');
        options.onBuildStart.forEach(script => exec(script, puts));
      }
    });

    compiler.plugin('emit', (compilation, callback) => {
      if (options.onBuildEnd.length) {
        console.log('Executing post-build scripts');
        options.onBuildEnd.forEach(script => exec(script, puts));
      }

      callback();
    });
  };

  // end webpack additional shell command

  // push the plugin to the plugins array when not in localhost environment
  plugins.push(
    new WebpackShellPlugin({
      onBuildEnd: ['python public/build.py'],
    })
  );
}


module.exports = {
  devtool: config.util.getEnv('NODE_ENV') == 'localhost' ? 'eval' : 'cheap-module-source-map',
  entry: [
    'babel-polyfill',
    './modules/index.jsx', // client appʼs entry point
  ],
  output: {
    path: path.join(__dirname, 'public/build'),
    filename: config.util.getEnv('NODE_ENV') == 'localhost' ? 'bundle.js' : 'bundle.[hash].min.js',
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        include: [
          path.resolve(__dirname),
        ],
        loaders: ['babel'],
      }, {
        test: /\.(css)$/,
        include: [
          path.resolve(__dirname),
        ],
        loader: ExtractTextWebpackPlugin.extract('style-loader', 'css-loader')
      }, {
        test: /\.(png|jpg|jpeg|gif)$/,
        include: [
          path.resolve(__dirname),
        ],
        loader: 'url-loader?limit=10000&name=images/[name].[ext]',
      }, {
        test: /\.(svg|eot|woff|woff2|ttf)(\?.*$|$)/,
        include: [
          path.resolve(__dirname),
        ],
        loader: 'url-loader?limit=10000&name=fonts/[name].[ext]',
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['build', 'index.html'], {
      root: path.join(__dirname, 'public'),
      verbose: true,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(config.util.getEnv('NODE_ENV')),
        BASE_URL: JSON.stringify(config.get('BASE_URL')),
        API_URL: JSON.stringify(config.get('API_URL')),
        SUBSCRIBE_KEY: JSON.stringify(config.get('PUBNUB.SUBSCRIBE_KEY')),
        PUBLISH_KEY: JSON.stringify(config.get('PUBNUB.PUBLISH_KEY')),
      },
    }),
    new ExtractTextWebpackPlugin(
      config.util.getEnv('NODE_ENV') == 'localhost' ? 'bundle.css' : 'bundle.[hash].min.css'
    ),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      output: {
        comments: false,
      },
      compressor: {
        warnings: false,
      },
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    })
  ],
};