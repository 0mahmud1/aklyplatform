import axios from 'axios';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';

import reducers from './reducers';

const checkToken = (store) => (next) => (action) => {
  let authToken = store.getState().auth.token ? store.getState().auth.token : null;
  axios.defaults.headers.common['authorization'] = authToken;
  return next(action);
};

const store = createStore(
  reducers,
  applyMiddleware(thunk, checkToken, logger())
);

export default store;
