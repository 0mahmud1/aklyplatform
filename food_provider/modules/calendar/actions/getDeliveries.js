import axios from 'axios';
import moment from 'moment';

export const getDeliveries = (dateRange = null, callback) => {
  if (!dateRange) {
    dateRange = {
      start: moment().startOf('month'),
      end: moment().endOf('month')
    };
  }
  console.log('getDeliveries(): ', dateRange);
  return function (dispatch) {
    dispatch({ type: 'FETCH_ORDER_REQUEST' });
    axios.get('deliveries', {
      params: {
        orderType: localStorage.getItem('orderType') ? localStorage.getItem('orderType') : 'onDemand',
        deliveryDateRange: [
          moment(dateRange.start).format('YYYY-MM-DD'),
          moment(dateRange.end).format('YYYY-MM-DD')].toString(),
      },
    }).then((response) => {
      dispatch({ type: 'FETCH_ORDER_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      console.error('getOrders:', error);
      dispatch({ type: 'FETCH_ORDER_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
