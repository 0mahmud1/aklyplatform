import React from 'react';
import { Route, IndexRoute } from 'react-router';
import toastr from 'toastr';

import AppLayout from '../../core/layouts/App.jsx';
import CalendarContainer from '../containers/Index.jsx';
import { isAuthorized } from '../../libs/auth.js';

export default function () {
  return (
    <Route path='calendar' component={AppLayout} onEnter={isAuthorized}>
      <IndexRoute component={CalendarContainer} />
    </Route>
  );
};
