import React, { Component } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import moment from 'moment';
import toastr from 'toastr';

class CalendarComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      order: null,
      dateRange: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.makeEvents(nextProps.orders);
  }

  componentDidMount() {
    const _this = this;
    const { calendar } = this.refs;

    // for fullCalendar
    $(calendar).fullCalendar({
      header: {
        left:   '',
        center: 'prev, title, next',
        right:  '',
      },
      dayClick: function (day) {
        console.log('dayClick(): ', moment(day).format('dddd, MMMM Do YYYY'));
      },
      eventClick: function (calEvent, jsEvent, view) {
        let orders = _.filter(_this.props.orders, function (order) {
          return order._id == calEvent.id;
        });

        if (orders.length > 0) {
          _this.setState({
            order: _.head(orders),
          });
          _this.calendarModal(
            $('#calendarModal'),
            $('#calendarModal').hasClass('hidden'),
            jsEvent.clientX,
            jsEvent.clientY
          );
        }
      },
      viewRender: function(view, element) {
        let dateRange = {
          start: view.currentRange.start,
          end: moment(view.currentRange.end).subtract(1, 'days'),
        }
        console.log('viewRender(): ', dateRange);
        _this.setState({dateRange : dateRange});
        _this.getDeliveries(dateRange);
      },
    });

    $('.fp-modal-container').click(function () {
      _this.calendarModal($('#calendarModal'), false);
    });

    // for orderType dropdown
    $('#orderType').select2({
        minimumResultsForSearch: -1,
    });

    $('#orderType').on('change', function (evt) {
      localStorage.setItem('orderType', evt.target.value);
      _this.getDeliveries(_this.state.dateRange);
    });

    if (localStorage.getItem('orderType')) {
      $('#orderType').val(localStorage.getItem('orderType'));
      $('#orderType').trigger('change');
    }
  }

  calendarModal(elementDOM, isVisible, clientX, clientY) {
    let fpModal = elementDOM.find('.fp-modal');
    let offsetTop = fpModal.offset().top;

    if (isVisible) {
      if (clientX > window.screen.width * 0.3) {
        fpModal.css({
          top: clientY + 35 + offsetTop,
          left: clientX - 400,
        });

        fpModal.removeClass('fp-modal-leftaligned').addClass('fp-modal-rightaligned');
      }

      else {
        fpModal.css({
          top: clientY + 35 + offsetTop,
          left: clientX - 120,
        });
        fpModal.removeClass('fp-modal-rightaligned').addClass('fp-modal-leftaligned');
      }

      elementDOM.removeClass('hidden');

    }

    else {
      elementDOM.addClass('hidden');
    }
  }

  render() {
    if (this.props.orders) {
      return (
        <div className="fp-calendar">
          <div className='fp-hdropdown-pos fp-hdropdown-pos-calendar'>
            <div className="dropdown fp-hdropdown">
              <select id="orderType">
                <option value="onDemand">On Demand</option>
                <option value="subscription">Subscription</option>
              </select>
            </div>
          </div>

  				<div ref="calendar"></div>

          <div className="fp-modal-container hidden" id="calendarModal">
        		<div className="fp-modal fp-modal-leftaligned">
        		  <div className="fp-orderedfood__items">
                {this.renderMeals()}
        			</div>
        		</div>
        	</div>

  			</div>
      );
    } else {
      return null;
    }
  }

  getDeliveries(dateRange = null) {
    this.props.getDeliveries(dateRange, function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        //toastr.success(res.message, 'Food Provider, getDeliveries');
      }
    });
  }

  renderMeals() {
    if (this.state.order && this.state.order.meals) {
      return this.state.order.meals.map((meal) => {
        return (
          <div className="fp-fooditem" key={meal.item._id}>
            <div className="fp-fooditem__imagebox">
              <img
                src={meal.item.images[1]}
                alt={meal.item.name}
                className="fp-fooditem__image" />
            </div>
            <Link
              to={'/foods/' + meal.item._id}
              className="fp-fooditem__title">
              {meal.item.name}
            </Link>
            <div className="fp-fooditem__number fp-fooditem__number_orderedfood">
              {meal.quantity}
            </div>
          </div>
        );
      });
    } else {
      return null;
    }
  }

  // makeEvents(orders) {
  //   const { calendar } = this.refs;
  //   $(calendar).fullCalendar('removeEvents');
  //   orders.map((order) => {
  //     $(calendar).fullCalendar('renderEvent', {
  //       id: order._id,
  //       title: order.deliveryRefId,
  //       start: moment(order.deliveryDate).format('YYYY-MM-DD'),
  //     }, true);
  //   });
  // }
  asyncFunc1(orders) {
    let events = [];
    orders.map((order) => {
      events.push({
        id: order._id,
        title: order.deliveryRefId,
        start: moment(order.deliveryDate).format('YYYY-MM-DD')
      })
    });
    return events;
  }
  makeEvents(orders) {
    const { calendar } = this.refs;
    $(calendar).fullCalendar('removeEvents');
    Promise.all([
        this.asyncFunc1(orders),
    ])
    .then(([result1]) => {
      // console.log('result >>',result1);
      $(calendar).fullCalendar('addEventSource', result1);
    })
    .catch(err => {
      console.error(err);
    });

  }
}

export default CalendarComponent;
