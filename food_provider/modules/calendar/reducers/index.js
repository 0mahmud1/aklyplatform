export const calendarReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_CALENDAR_REQUEST':
      return state;
    case 'FETCH_CALENDAR_FAILURE':
      return state;
    case 'FETCH_CALENDAR_SUCCESS':
      return action.payload.data;
    default:
      return state;
  }
};
