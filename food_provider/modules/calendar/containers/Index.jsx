import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import toastr from 'toastr';

import CalendarComponent from '../components/Index.jsx';

import { getDeliveries } from '../actions/getDeliveries';

class CalendarContainer extends Component {
  componentWillMount() {
    this.props.getDeliveries(function (err, res) {
      if (err) {
        toastr.error(err.message, 'Calendar');
      } else {
        //toastr.success(res.message, 'Calendar');
      }
    });
  }

  render() {
    return (
      <CalendarComponent
        orders={this.props.orders}
        getDeliveries={this.props.getDeliveries} />
    );
  }
}

function mapStateToProps(store) {
  return {
    orders: store.orders,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getDeliveries: getDeliveries,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(CalendarContainer);
