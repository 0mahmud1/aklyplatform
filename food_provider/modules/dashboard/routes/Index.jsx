import React from 'react';
import { Route, IndexRoute } from 'react-router';
import toastr from 'toastr';

import AppLayout from '../../core/layouts/App.jsx';
import DashboardContainer from '../containers/Index.jsx';
import { isAuthorized } from '../../libs/auth.js';

export default function () {
  return (
    <Route path='dashboard' component={AppLayout} onEnter={isAuthorized}>
      <IndexRoute component={DashboardContainer} />
    </Route>
  );
};
