export const dashboardReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_DASHBOARD_REQUEST':
      return state;
    case 'FETCH_DASHBOARD_FAILURE':
      return state;
    case 'FETCH_DASHBOARD_SUCCESS':
      return action.payload.data;
    default:
      return state;
  }
};
