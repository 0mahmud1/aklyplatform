import React, { Component } from 'react';

import NoContent from '../../core/components/NoContent.jsx';

class DashboardContainer extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-12'>
          <NoContent text='We are still cooking this page'/>
        </div>
      </div>
    );
  }
}

export default DashboardContainer;
