import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import DashboardComponent from '../components/Index.jsx';

class DashboardContainer extends Component {
  render() {
    return (
      <DashboardComponent />
    );
  }
}

function mapStateToProps(store) {
  return {
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(DashboardContainer);
