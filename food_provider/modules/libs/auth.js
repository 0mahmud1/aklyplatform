
/*
* all these helper functions are for checking route authorization & log out purposes
* */
import store from '../store'

'use strict';

import { browserHistory } from 'react-router';
import toastr from 'toastr';

export function isAuthorized(nextState, replaceState) {
  let user = store.getState().auth.user;
  //console.log('Authorize user expiration ', new Date(user.exp*1000).getTime())
  if(!user || !user.roles || user.roles[0] !== 'foodProvider' || new Date(user.exp*1000).getTime() < new Date().getTime())
  {
    toastr.warning('Need to login.');
    replaceState('/login');
  }
}

export function signOut(nextState, replaceState) {
    localStorage.removeItem('access_token');
    localStorage.removeItem('auth0Object');
    localStorage.removeItem('email');
    localStorage.removeItem('jwt');
    // toastr.success('Logout successfully', 'Auth');
    replaceState('/login');
}
