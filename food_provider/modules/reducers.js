import { combineReducers } from 'redux';

import { authReducer } from './auth/reducers/index.js';
import { dashboardReducer } from './dashboard/reducers/index.js';
import { calendarReducer } from './calendar/reducers/index.js';
import { ordersReducer, notifyReducer } from './orders/reducers/index.js';
import { foodReducer } from './foods/reducers/index.js';

const reducers = combineReducers({
  auth: authReducer,
  dashboard: dashboardReducer,
  calendar: calendarReducer,
  orders: ordersReducer,
  notify: notifyReducer,
  food: foodReducer,
});

export default reducers;
