export const ordersReducer = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_ORDER_REQUEST':
      return state;
    case 'FETCH_ORDER_FAILURE':
      return state;
    case 'FETCH_ORDER_SUCCESS':
      return action.payload.data;
    default:
      return state;
  }
};

export const notifyReducer = (state = 0, action) => {
  switch (action.type) {
    case 'FETCH_ORDER_SUCCESS':
      let cnt = 0;
      action.payload.data.map((order) => {
        if (order.status == 'pending') {
          cnt = cnt + 1;
        }
      });
      return cnt;
    default:
      return state;
  }
};
