import React, { Component } from 'react';
import { Link } from 'react-router';
import toastr from 'toastr';

class VanItemComponent extends Component {
    componentDidMount() {
      $('.fp-openpanel').on('click', function () {
        $(this).children('.collapse').collapse('toggle');
        $(this).children('.fp-openpanel__header').toggleClass('fp-c-collapsed');
      });
    }
  render() {
    if (this.props.van) {
      return (
        <div className="fp-ocontent__container" role="tablist" aria-multiselectable="true">
          <div className="fp-openpanel">
            <div className="fp-openpanel__header fp-byvanheader fp-customicon" role="tab">
              <div className="fp-byvanheader__titlebox">
                <div className="fp-byvanheader__title">{this.props.van.name}</div>
                <div className="fp-byvanheader__subtitle">{this.props.van.email}</div>
              </div>
              <Link
                to="javascript:void(0)"
                className="fp-proceed-button"
                onClick={this.proceed.bind(this)}>
                Proceed
              </Link>
            </div>

            <div className="collapse fp-openpanel__content fp-byvancont" role="tabpanel">
              <table className="fp-byvancont__table">
                <thead className="fp-ot__headblock">
                  <tr>
                    <th className="fp-ot__header fp-ot__qty">Qty</th>
                    <th className="fp-ot__header">Order</th>
                    <th className="fp-ot__header">Price</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderOrders()}
                </tbody>
              </table>
            </div>

          </div>
        </div>
      );
    } else {
      return (<p>Loading...</p>);
    }
  }

  proceed(event) {
    event.preventDefault();
    console.log('click proceed...');
    this.props.vanProceed(this.props.van, function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        toastr.success(res.message, 'Food Provider');
      }
    })
  }

  renderOrders() {
    if (this.props.van) {
      return this.props.van.orders.map((order) => {
        return (
          <tr key={order._id}>
            <td className="fp-ot__qty fp-ot__column">{order.quantity}</td>
            <td className="fp-ot__order fp-ot__column">
              <div className="fp-fooditem__imagebox">
                <img src="/images/food.jpeg" alt="" className="fp-fooditem__image" />
              </div>
              <Link to="javascript:void(0)" className="fp-ot__foodtitle">
                {order.deliveryRefId}
              </Link>
            </td>
            <td className="fp-ot__price fp-ot__column">qr {order.subTotal}</td>
          </tr>
        );
      });
    } else {
      return null;
    }
  }

}

export default VanItemComponent;
