import React, { Component } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import Rodal from 'rodal'
import toastr from 'toastr';
import jsPDF from 'jspdf';
import moment from 'moment';
import html2canvas from 'html2canvas'

class OrderItemComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalShow: false,
      buttonValue: {
        pending: 'Accept',
        cooking: 'Ready',
        accepted: 'On The Way',
        denied: 'Ready',
      },
    };

    this.modalShow = this.modalShow.bind(this);
  }

  componentDidMount() {
    $('.collapseTarget').on('click', function () {
      $(this).next('.collapse').collapse('toggle');
      $(this).children('.fp-openpanel__header').toggleClass('fp-c-collapsed');
    });
  }

  modalShow() {
    this.setState(
      {
        modalShow: !this.state.modalShow
      }
    )
  }

  NoDboyModal() {
    return <Rodal visible={this.state.modalShow} onClose={this.modalShow} height={160}>
      <div className='binary_prompt_content'>
          <div className='prompt_text'>
            <p>No Delivery Boy Found Now. Please Try Again Later.</p>
          </div>
          <div className='prompt_btn'>
            <button className='btn butn butn_default' onClick={this.modalShow} >
              <span>Ok</span>
            </button>
          </div>
        </div>
    </Rodal>
  }

  getStatusBg() {
    let getBg;

    if(this.props.order) {
      switch (this.props.order.status) {
        case 'pending':
          getBg = 'status-pending';
          break;

        case 'cooking':
          getBg = 'status-cooking';
          break;

        case 'ready':
          getBg = 'status-ready';
          break;

        case 'onTheWay':
          getBg = 'status-ontheway';
          break;

        case 'accepted':
          getBg = 'status-accepted';
          break;

        case 'denied':
          getBg = 'status-denied';
          break;

        case 'completed':
          getBg = 'status-completed';
          break;

        case 'cancel':
          getBg = 'status-cancel';
          break;

        case 'Archived':
          getBg = 'status-archived';
      }
    }

    else {
      getBg = null;
    }

    return getBg;
  }


  render() {

    const statusBg = this.getStatusBg();

    if (this.props.order) {
      return (
        <div>
          <div className="fp-openpanel fp-preparing">
            <div className='collapseTarget' style={{padding: '2rem'}}>
              <div
                role="tab"
                className="fp-openpanel__header fp-preparing__header fp-ordersheader fp-customicon">
                <div className="fp-ordersheader__titlebox">
                  <div className="fp-ordersheader__title">{this.props.order.deliveryRefId}</div>
                </div>

                <a href="#" className={"fp-preparing-button " + statusBg}>{this.props.order.status}</a>
              </div>
            </div>

            <div className="collapse fp-openpanel__content fp-orderscont" role="tabpanel">
                <div className="fp-orderscont__rightside col-md-12 col-sm-12 col-xs-12">
                  <div className="fp-orderscont__container contwidth">
                    <div className="fp-orderscont__title">Order Details</div>
                    <div className="fp-orderscont__text">
                        {
                          this.props.order.deliveryNote ===""? null :
                              <p><strong>Delivery Note :</strong> {this.props.order.deliveryNote}</p>
                        }

                      <p><strong>Order Time :</strong> {moment(this.props.order.createdAt).format("dddd, MMMM Do YYYY, h:mm:ss a")}</p>
                    </div>
                    <div className="fp-orderscont__title">Customer Details</div>
                    <div className="fp-orderscont__text">
                      <p><strong>Name :</strong> {this.props.order.createdBy.name}</p>
                      <p><strong>Phone :</strong> {this.props.order.address.phone}</p>
                      <p><strong>Email :</strong> {this.props.order.createdBy.email}</p>
                      <p><strong>Address: </strong>{this.props.order.address.label} # {this.props.order.address.apt}<br/>
                          {this.props.order.address.street}</p>

                    </div>
                    <br/>
                    { this.props.order.deliveryBoy ?
                      <span>
                        <div className="fp-orderscont__title">Delivery Boy</div>
                        <div className="fp-orderscont__text">
                          {this.props.order.deliveryBoy.name}<br/>
                          {this.props.order.deliveryBoy.email}<br/>
                          {this.props.order.deliveryBoy.phone}
                        </div>
                      </span>
                      : null
                    }
                    <div className='print-button'>
                      <button className='btn butn butn_default' onClick={this.printOrder.bind(this)}>Print</button>
                    </div>
                  </div>

                </div>
              <div id={this.props.order.deliveryRefId} className="fp-orderscont__leftside col-md-12 col-sm-12 col-xs-12 order-table-layout">
                <table className="fp-orderscont__table">
                  <thead className="fp-ot__headblock fp-ot__headblock_noborder">
                    <tr>
                      <th className="fp-ot__header fp-ot__header_qty">Qty</th>
                      <th className="fp-ot__header">Order</th>
                      <th className="fp-ot__header">Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderItems()}
                  </tbody>
                </table>
              </div>

              <div className='order-status-button'>
                { this.props.order.orderType == 'onDemand' &&
                  _.indexOf(['pending', 'cooking', 'accepted', 'denied'], this.props.order.status) >= 0 ?
                  <Link
                    to="javascript:void(0)"
                    className="fp-proceed-button fp-proceed-button_relative"
                    onClick={this.changeStatus.bind(this)}>
                    {this.state.buttonValue[this.props.order.status]}
                  </Link>
                  : null
                }
              </div>
            </div>
            {this.NoDboyModal()}
          </div>
        </div>

      );
    } else {
      return null;
    }
  }

  changeStatus(event) {
    const _this = this;
    event.preventDefault();
    this.props.changeOnDemandStatus(this.props.order, function (err, res) {
      if (err) {

        toastr.error(err.message, 'Food Provider');
        _this.setState({modalShow:true});
      } else {
        //toastr.success(res.message, 'Food Provider, changeOnDemandStatus');
        _this.props.callAction();
      }
    });
  }

  printOrder(){
    console.log(this.props.order)
    let prnt = document.getElementById(this.props.order.deliveryRefId)
      console.log(prnt)
      html2canvas(prnt).then((canvas)=>{
        let image = canvas.toDataURL('image/png')
        let doc = new jsPDF('1','pt','a4')
        doc.text(this.props.order.deliveryRefId, 55, 60)
        doc.addImage(image, 'PNG', 20, 90)
        doc.output('dataurlnewwindow');
        doc.save(this.props.order.deliveryRefId)
      })
  }

  renderItems() {
    if (this.props.order) {
      return this.props.order.meals.map((meal) => {
        return (
          <tr key={meal.item._id}>
            <td className="fp-ot__qty fp-ot__column">{meal.quantity}</td>
            <td className="fp-ot__order fp-ot__column">
              <div className="fp-fooditem__imagebox">
                <img src={meal.item.images[1]} alt="" className="fp-fooditem__image" />
              </div>
              <Link to={'/foods/' + meal.item._id} className="fp-ot__foodtitle">
                {meal.item.name}
              </Link>
            </td>
            <td className="fp-ot__price fp-ot__column">qr {meal.item.price}</td>
          </tr>
        );
      });
    } else {
      return null;
    }
  }
}

export default OrderItemComponent;
