/**
 * Created by sajid on 4/9/17.
 */
import React, { Component } from 'react';

const Style = {
    paddingTop: '25px',
    fontSize: '18px',
    color: '#ed731f',
    textAlign: 'center'
};

class NoORderComponent extends Component{
    render() {
        return(
            <div style={Style}>
                <p>{this.props.message}</p>
            </div>
        )
    }
}

export default NoORderComponent
