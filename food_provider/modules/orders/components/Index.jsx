  import React, { Component } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import moment from 'moment';
import toastr from 'toastr';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import 'react-select/dist/react-select.css'
import FoodItemComponent from './FoodItem.jsx';
import OrderItemComponent from './OrderItem.jsx';
import VanItemComponent from './VanItem.jsx';
import NoORderComponent from './noOrder';

class OrderComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: moment(),
      value: this.renderOptions(),
      checkedOnDemand: false,
      checkedSubscription: false
    };

    this.handleStartDate = this.handleStartDate.bind(this);
    this.callAction = this.callAction.bind(this);
    this.handleChangeOnDemand = this.handleChangeOnDemand.bind(this)
    this.handleChangeSubscription = this.handleChangeSubscription.bind(this)
  }

  componentDidMount() {
    const _this = this;

    $('#orderType').select2({
      minimumResultsForSearch: -1,
    });

    $('#orderType').on('change', function (evt) {
      localStorage.setItem('orderType', evt.target.value);
      _this.callAction(_this.state.startDate);
    });

    if (localStorage.getItem('orderType')) {
      $('#orderType').val(localStorage.getItem('orderType'));
      $('#orderType').trigger('change');
    }
  }

  handleChangeOnDemand(){
    this.setState({
        checkedOnDemand: !this.state.checkedOnDemand
    })
  }

  handleChangeSubscription(){
    this.setState({
        checkedSubscription: !this.state.checkedSubscription
    })
  }


  render() {
      let sameDate = false  ;
      let today = moment(new Date()).format("YYYY-MM-DD");
      // console.log('this.props.orders', this.props.orders)
      if (this.props.orders.length >0) {
          let deliveryDate = moment(new Date(this.props.orders[0].deliveryDate)).format("YYYY-MM-DD")
          sameDate = moment(today).isSame(deliveryDate)
          let todayDate = moment().format('dddd')
          let deliveryDay = moment(deliveryDate).format('dddd')
          // console.log('today', today)
          // console.log('todayDate',todayDate)
          // console.log('deliveryDay',deliveryDay)
          // console.log('deliveryDate', deliveryDate)
          // console.log('sameDate', sameDate)
          let sameWeek = moment(today).isSame(deliveryDate, 'week')
          if (todayDate === 'Thursday' && deliveryDay === 'Saturday' && sameWeek === true) sameDate = true
          console.log('sameDate', sameDate)
      }
      let onDemandOrders = _.filter(this.props.orders , function(order) {
        return order.orderType == 'onDemand'
      })
      let subscriptionOrders = _.filter(this.props.orders , function(order) {
        return order.orderType == 'subscription'
      })

    return (
      <div className="fp-order">
        <div className='fp-hdropdown-pos'>
          {/* <div className="dropdown fp-hdropdown">
            <select id="orderType">
              <option value="onDemand">On Demand</option>
              <option value="subscription">Subscription</option>
            </select>
          </div> */}
        </div>
        <div className="fp-order__header text-center">
          {/* <div className="fp-order__date">Sunday, October 9th 2016</div> */}

          {/* <Select className="fp-select__multi"
            	name="form-field-name"
              multi
              simpleValue
              value={this.state.value}
            	options={this.renderOptions()}
            	onChange={this.logChange.bind(this)}
          /> */}

          <div className="fp-order__date fp-datepicker">
            <DatePicker
              dateFormat="dddd, MMMM Do YYYY"
              selected={this.state.startDate}
              onChange={this.handleStartDate} />
          </div>
        </div>

        {/*split screen markup starts here*/}
        <div className='row'>

          {/* left side  */}
          <div className='col-sm-6' style={{borderRight: '1px solid #e3e3e3', height: 'calc(100vh - 300px)'}}>
            <h2 className="txtcenter">On Demand</h2>
            <hr/>
            <div className='text-center'>
              <ul className="fp-order__nav fp-nav" role="tablist">
                <li
                  role="presentation"
                  className="fp-nav__item active">
                  <a href="#onDemandFoods" aria-controls="onDemandFoods" role="tab" data-toggle="tab">Foods</a>
                </li>
                <li
                  role="presentation"
                  className="fp-nav__item">
                  <a href="#onDemandOrders" aria-controls="onDemandOrders" role="tab" data-toggle="tab">Orders</a>
                </li>
              </ul>
            </div>

            <div className='tab-content fp-order__content fp-ocontent'>
              {/* FOODS */}
              <div
                id="onDemandFoods" role="tabpanel"
                className="fp-ocontent__pane tab-pane active">
                  { onDemandOrders && onDemandOrders.length>0 ?
                      <div className="fp-food">
                          {this.renderFoods(onDemandOrders , 'onDemand')}
                      </div> :
                      <NoORderComponent message = {"You do not have any order today"}/>
                  }
              </div>
              {/* ORDERS */}
              <div
                id="onDemandOrders" role="tabpanel"
                className="fp-ocontent__pane tab-pane">
                <div className="fp-ocontent__container" role="tablist" aria-multiselectable="true">
                  <div className='fp-orderitem-filter'>
                    <div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" onChange={ this.handleChangeOnDemand.bind(this) } />
                          show completed order
                        </label>
                      </div>
                    </div>
                  </div>
                  {this.renderOrders(onDemandOrders , 'onDemand')}
                </div>
              </div>
            </div>
          </div>

          {/* Right side  */}
          <div className='col-sm-6'>
            <h2 className="txtcenter">Subscription</h2>
            <hr/>
            <div className='text-center'>
              <ul className="fp-order__nav fp-nav" role="tablist">
                <li
                  role="presentation"
                  className="fp-nav__item active">
                  <a href="#subscriptionFoods" aria-controls="subscriptionFoods" role="tab" data-toggle="tab">Foods</a>
                </li>
                <li
                  role="presentation"
                  className="fp-nav__item">
                  <a href="#subscriptionOrders" aria-controls="subscriptionOrders" role="tab" data-toggle="tab">Orders</a>
                </li>
                <li
                  role="presentation"
                  className="fp-nav__item">
                  <a href="#subscriptionByVan" aria-controls="subscriptionByVan" role="tab" data-toggle="tab">By Van</a>
                </li>
              </ul>
            </div>

            <div className='tab-content fp-order__content fp-ocontent'>
              {/* FOODS */}
              <div
                id="subscriptionFoods" role="tabpanel"
                className="fp-ocontent__pane tab-pane active">
                  { subscriptionOrders && subscriptionOrders.length>0 ?
                      <div className="fp-food">
                          {this.renderFoods(subscriptionOrders , 'subscription')}
                      </div> :
                      <NoORderComponent message = {"You do not have any order today"}/>
                  }
              </div>
              {/* ORDERS */}
              <div
                id="subscriptionOrders" role="tabpanel"
                className="fp-ocontent__pane tab-pane">
                {subscriptionOrders && subscriptionOrders.length>0 && sameDate ?
                  <Link
                    to="javascript:void(0)"
                    className="fp-proceed-button fp-order__button"
                    onClick={this.subscriptions.bind(this)}>
                    PROCEED
                  </Link> : null
                }
                <div className="fp-ocontent__container" role="tablist" aria-multiselectable="true">
                  <div className='fp-orderitem-filter'>
                    <div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" onChange={ this.handleChangeSubscription.bind(this) } />
                          show completed order
                        </label>
                      </div>
                    </div>
                  </div>
                  {this.renderOrders(subscriptionOrders , 'subscription')}
                </div>
              </div>
              {/* BY VAN */}
              <div
                id="subscriptionByVan" role="tabpanel"
                className="fp-ocontent__pane tab-pane">
                {this.renderVans(subscriptionOrders , 'subscription')}
              </div>
            </div>
          </div>
        </div>


      </div>
    );
  }

  logChange(value) {
  	console.log("Selected: " + value);
    this.setState({value});
  }

  filterByStatusOnDemand(orders ) {
    if(this.state.checkedOnDemand === true ) {
      return orders
    }
    else {
        let filteredOrders = _.filter(orders, (eachOrder)=>{
          return eachOrder.status !== "completed"
        })
        return filteredOrders
    }
  }
  filterByStatusSubscription(orders ) {
    if(this.state.checkedSubscription === true) {
      return orders
    }
    else {
        let filteredOrders = _.filter(orders, (eachOrder)=>{
          return eachOrder.status !== "completed"
        })
        return filteredOrders
    }
  }

  renderOptions() {
    let options = [
          	{ value: 'pending', label: 'pending' },
          	{ value: 'cooking', label: 'cooking' },
            { value: 'ready', label: 'ready' },
            { value: 'cooking', label: 'cooking' },
            { value: 'onTheWay', label: 'onTheWay' },
            { value: 'accepted', label: 'accepted' },
            { value: 'denied', label: 'denied' },
            { value: 'completed', label: 'completed' },
            { value: 'cancel', label: 'cancel' }
          ];
    return options;
  }

  subscriptions(event) {
    event.preventDefault();
    this.props.putSubscriptions(function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        //toastr.success(res.message, 'Food Provider, subscriptions');
      }
    });
  }

  callAction(date) {
    this.props.getOrders(date, function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        //toastr.success(res.message, 'Food Provider, callAction');
      }
    });
  }

  handleStartDate(date) {
    this.setState({
      startDate: date,
    });
    this.callAction(date);
  }

  generateItems(orders , orderType) {
    let items = [];
    if (orders) {
      orders.map((order) => {
        order.meals.map((meal) => {
          if (meal.item && meal.item._id) {
            let index = _.findIndex(items, function (item) {
              return item._id == meal.item._id;
            });

            if (index >= 0) {
              items[index] = Object.assign({}, items[index], {
                quantity: items[index].quantity + meal.quantity,
              });
            } else {
              items.push(Object.assign({}, meal.item, {
                quantity: meal.quantity,
              }));
            }
          }
        });
      });
    }

    return items;
  }

  renderFoods(orders , orderType) {
      return this.generateItems(orders , orderType).map((item) => {
        return (
          <FoodItemComponent
            key={item._id}
            item={item} />
        );
      });
  }

  renderOrders(orders , orderType) {
    //console.log('renderOrders >>> ',orders , orderType);
    const _this = this;
    if (orders && orders.length>0){
      if(orderType == 'onDemand')
       orders = this.filterByStatusOnDemand(orders);
      if(orderType == 'subscription')
       orders = this.filterByStatusSubscription(orders);
      return orders.map((order) => {
        return (
          <OrderItemComponent
            key={order._id}
            order={order}
            callAction={this.callAction}
            changeOnDemandStatus={this.props.changeOnDemandStatus} />
        );
      });
    } else {
        return(
            <NoORderComponent message = {"You do not have any order today"}/>
        )
    }
  }

  generateVans(orders , orderType) {
    let vans = [];
    if (orders) {
      orders.map((order) => {
        if (order.van && order.van._id) {
          let index = _.findIndex(vans, function (van) {
            return van._id == order.van._id;
          });

          if (index >= 0) {
            let orders = vans[index].orders;
            orders.push(order);
            vans[index] = Object.assign({}, vans[index], {
              orders: orders,
            });
          } else {
            vans.push(Object.assign({}, order.van, {
              orders: [order],
            }));
          }
        }
      });
    }

    return vans;
  }

  renderVans(orders , orderType) {
    if (orders && orderType == 'subscription') {
      console.log('renderVans >>>',orders , orderType);
      return this.generateVans(orders , orderType).map((van) => {
        return (
          <VanItemComponent
            key={van._id}
            van={van}
            vanProceed={this.props.vanProceed} />
        );
      });
    } else {
      return null;
    }
  }

}

export default OrderComponent;
