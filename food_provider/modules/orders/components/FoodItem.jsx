import React, { Component } from 'react';
import { Link } from 'react-router';

class FoodItemComponent extends Component {
  render() {
    return (
      <div className="fp-food__row">
        <div className="fp-fooditem__imagebox">
          <img
            src={this.props.item.images[2]}
            alt={this.props.item.name}
            className="fp-fooditem__image" />
        </div>
        <Link to={'/foods/' + this.props.item._id} className="fp-ot__foodtitle">
          {this.props.item.name}
        </Link>
        <div className="fp-food__number">
          {this.props.item.quantity}
        </div>
      </div>
    );
  }
}

export default FoodItemComponent;
