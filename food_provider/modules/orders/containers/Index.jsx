import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import toastr from 'toastr';
import moment from 'moment';
import { browserHistory } from 'react-router';

import OrderComponent from '../components/Index.jsx';

import { getOrders } from '../actions/getOrders';
import { changeOnDemandStatus } from '../actions/changeOnDemandStatus';
import { putSubscriptions } from '../actions/putSubscriptions';
import { vanProceed } from '../actions/vanProceed';

class OrderContainer extends Component {
  componentWillMount() {
    this.props.getOrders(moment(), function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        //toastr.success(res.message, 'Food Provider, componentWillMount');
      }
    });
  }

  render() {
    return (
      <OrderComponent
        orders={this.props.orders}
        getOrders={this.props.getOrders}
        changeOnDemandStatus={this.props.changeOnDemandStatus}
        putSubscriptions={this.props.putSubscriptions}
        vanProceed={this.props.vanProceed} />
    );
  }
}

function mapStateToProps(store) {
  return {
    orders: store.orders,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getOrders: getOrders,
    changeOnDemandStatus: changeOnDemandStatus,
    putSubscriptions: putSubscriptions,
    vanProceed: vanProceed,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(OrderContainer);
