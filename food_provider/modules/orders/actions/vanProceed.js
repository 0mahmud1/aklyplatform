import axios from 'axios';

export const vanProceed = (van, callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_VAN_PROCEED_REQUEST' });
    axios.put('deliveries/subscriptions/ready', {
      'van.email': van.email,
    }).then((response) => {
      dispatch({ type: 'FETCH_VAN_PROCEED_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_VAN_PROCEED_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
