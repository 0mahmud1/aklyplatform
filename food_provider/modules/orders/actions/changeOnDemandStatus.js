import axios from 'axios';

export const changeOnDemandStatus = (orderObj, callback) => {
  let status = null;
  if (orderObj.status == 'pending') {
    status = 'cooking';
  } else if (orderObj.status == 'cooking') {
    status = 'ready';
  } else if (orderObj.status == 'accepted') {
    status = 'onTheWay';
  } else if (orderObj.status == 'denied') {
    status = 'ready';
  }

  return function (dispatch) {
    dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_REQUEST' });
    axios.put('deliveries/' + orderObj._id, {
      status: status,
    }).then((response) => {
      dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
