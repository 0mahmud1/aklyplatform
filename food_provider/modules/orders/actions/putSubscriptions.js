import axios from 'axios';

export const putSubscriptions = (callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_REQUEST' });
    axios.put('deliveries/subscriptions')
      .then((response) => {
      dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      dispatch({ type: 'FETCH_CHANGE_ORDER_STATUS_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
