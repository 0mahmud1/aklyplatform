import axios from 'axios';
import moment from 'moment';

export const getOrders = (deliveryDate, callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_ORDER_REQUEST' });
    let params = {};
    if(deliveryDate.format('dddd') === 'Thursday') {
      params = {
        //orderType: localStorage.getItem('orderType') ? localStorage.getItem('orderType') : 'onDemand',
        deliveryDate: [moment(deliveryDate).format('YYYY-MM-DD'),moment(deliveryDate).add(2,'d').format('YYYY-MM-DD')].toString(),
        sortByOrderDate : true
      }
    }
    else {
      params = {
        //orderType: localStorage.getItem('orderType') ? localStorage.getItem('orderType') : 'onDemand',
        deliveryDate: moment(deliveryDate).format('YYYY-MM-DD'),
        sortByOrderDate : 'true'
      }
    }
    axios.get('deliveries', {
      params: params,
    }).then((response) => {
      dispatch({ type: 'FETCH_ORDER_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      console.error('getOrders:', error);
      dispatch({ type: 'FETCH_ORDER_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
