export const foodReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_FOOD_REQUEST':
      return state;
    case 'FETCH_FOOD_FAILURE':
      return state;
    case 'FETCH_FOOD_SUCCESS':
      return action.payload.data.data;
    default:
      return state;
  }
};
