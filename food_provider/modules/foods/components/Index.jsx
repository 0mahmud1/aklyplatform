import React, {Component} from 'react';
import ReactPlayer from 'react-player'

class FoodComponent extends Component {

  showRecipe() {
    console.log('showRecipe =>>');
    if(this.props.food.recipe) {
      // var $jQueryObject = $($.parseHTML(this.props.food.recipe));
      // return $("<li></li>").html($jQueryObject.html());
      return <li dangerouslySetInnerHTML={{__html: this.props.food.recipe}} />
    }
  }
  render() {
    let _this = this;
    let ingredients = this.props.food.ingredients.split(","); //makes array of ingredient for list view

    return (
        <div className="fp-recipe">
          <div className="fp-recipe__title fp-rtitle">
            <div className="fp-rtitle__text">{this.props.food.name}</div>
            <div className="fp-rtitle__time">
              Preparation Time
              <span className="fp-rtitle__timebutton fp-customicon">1h 30m</span>
            </div>
          </div>

          <div className="fp-recipe__content fp-rcontent">
            {
              this.props.food.youtubeUrl ?
              <ReactPlayer url={this.props.food.youtubeUrl} className="fp-video-player"/>
              :
              <div className="fp-rcontent__image">
                <div className="fp-rcontent__overlay">
                  <img src={this.props.food.images[0]} alt = {this.props.food.name}/>
                </div>
                {/*<span className="fp-play fp-customicon"></span>*/}
              </div>
            }

            <div className="fp-rcontent__const fp-const">
              <span className="fp-const__item fp-customicon fp-const__item_protein">{this.props.food.protein} Protein</span>
              <span className="fp-const__item fp-customicon">{this.props.food.carbs} Carbs</span>
              <span className="fp-const__item fp-customicon">{this.props.food.calorie} Cals</span>
              <span className="fp-const__item fp-customicon">{this.props.food.fat} Fat</span>
            </div>

            <div className="fp-rcontent__text">
              <h3>Ingredients</h3>
              <ul>
                {
                  ingredients.map((ingredient)=>{
                    return (<li key={ingredient}>{ingredient}</li>);
                  })
                }
              </ul>

              <h3>Recipe</h3>
              <ul>
                {this.showRecipe()}
              </ul>
            </div>
          </div>
        </div>
    );
  }
}

export default FoodComponent;


// <iframe width="420" height="315"
//   src={this.props.food.youtubeUrl}>
// </iframe>
