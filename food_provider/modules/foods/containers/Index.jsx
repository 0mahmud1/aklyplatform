import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

import FoodComponent from '../components/Index.jsx';
import { readFood } from '../actions/index.js';

class FoodContainer extends Component {
  
  componentWillMount() {
    this.props.readFood(this.props.params._id);
  }
  
  render() {
    let shouldLoad = !_.isEmpty(this.props.food);
    return (
      shouldLoad ?
      <FoodComponent food={this.props.food}/>
      :<h3>Loading..</h3>
    );
  }
}

function mapStateToProps(store) {
  return {
    food : store.food
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    readFood : readFood
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(FoodContainer);
