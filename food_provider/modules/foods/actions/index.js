import axios from 'axios';
import toastr from 'toastr';

export function readFood(_id) {
  return function (dispatch) {
    dispatch({type: 'FETCH_FOOD_REQUEST'});
    axios.get('foods/' + _id)
      .then((response) => {
        dispatch({type: 'FETCH_FOOD_SUCCESS', payload: response});
        //toastr.success('Food details loaded');
      })
      .catch((err) => {
        dispatch({type: 'FETCH_FOOD_FAILURE'});
        toastr.error(err);
      });
  }
}
