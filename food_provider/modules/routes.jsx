import React, { Component } from 'react';
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router';

import CoreLayout from './core/layouts/Index.jsx';
import NotFoundComponent from './core/components/NotFound.jsx';

import AuthRoute from './auth/routes/Index.jsx';
import DashboardRoute from './dashboard/routes/Index.jsx';
import CalendarRoute from './calendar/routes/Index.jsx';
import OrderRoute from './orders/routes/Index.jsx';
import FoodRoute from './foods/routes/Index.jsx';

import ProfileRoute from './profile/routes/Index.jsx';
import { isAuthorized , signOut } from './libs/auth.js';


class Routes extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' onEnter={isAuthorized}>
          <IndexRedirect to='dashboard'/>
          {DashboardRoute()}
          {CalendarRoute()}
          {OrderRoute()}
          {FoodRoute()}
          {ProfileRoute()}
        </Route>
        {AuthRoute()}
        <Route path='logout' onEnter={signOut} />
        <Route path='*' component={CoreLayout}>
          <IndexRoute component={NotFoundComponent} />
        </Route>
      </Router>
    );
  }
}

export default Routes;
