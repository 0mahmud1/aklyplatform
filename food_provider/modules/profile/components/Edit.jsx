import React, {Component} from 'react';

export default class Profile extends Component {
    render() {
        return (
            <div className='fp-page-wrapper'>
                <form>
                    <div className='fp-profile-basicinfo'>
                        <div className='fp-profile-secheading mb-20'>
                            <h4>Food provider information</h4>
                        </div>
                        <div className='row'>
                            <div className='col-sm-4 col-md-4'>
                                <div className='fp-input mb-20'>
                                    <label htmlFor='name'>Name</label>
                                    <input type='text' className='form-control' value='name' />
                                </div>

                                <div className='fp-input'>
                                    <label htmlFor='email'>Email</label>
                                    <input type='email' className='form-control' value='email@email.com' />
                                </div>
                            </div>
                            <div className='col-sm-4 col-md-4'>
                                <div className='fp-input mb-20'>
                                    <label htmlFor='password'>Password</label>
                                    <input type='password' className='form-control' placeholder='password' value='xxxxxx' />
                                </div>

                                <div className='fp-input mb-20'>
                                    <label htmlFor='changePassword'>Change Password</label>
                                    <input type='password' className='form-control' placeholder='Change Password' />
                                </div>

                                <div className='fp-input'>
                                    <label htmlFor='confirmPassword'>Confirm Password</label>
                                    <input type='password' className='form-control' placeholder='Confirm Password' />
                                </div>
                            </div>
                            <div className='col-sm-4 col-md-4'>
                                <p className='mb-5'>Created at - <span>1 February, 2017</span></p>
                                <p>Updated at - <span>5 February, 2017</span></p>
                            </div>
                        </div>
                    </div>

                    <div className='fp-profile-map'>
                        <div className='row'>
                            <div className='col-sm-3 col-md-3'>
                                <div className='fp-profile-secheading mb-20'>
                                    <h4>Others information</h4>
                                </div>
                                <div className='fp-profile-radio'>
                                    <div className="radio mt-0">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" />
                                            On Demand
                                        </label>
                                    </div>

                                    <div className="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" />
                                            Subscription
                                        </label>
                                    </div>

                                    <div className="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" />
                                            Both
                                        </label>
                                    </div>
                                </div>

                                <div className='fp-profile-vaninfo mt-25'>
                                    <h4 className='mb-15'>Van Information</h4>
                                    <p className='mb-5'>Name - <span>Van123</span></p>
                                    <p className='mb-5'>Email - <span>Van123@example.com</span></p>
                                    <p className='mb-5'>Address - <span>Van123, Doha</span></p>
                                    <p>Capacity - <span>20</span></p>
                                </div>
                            </div>
                            <div className='col-sm-9 col-md-9'>
                                <div className='fp-profile-gmap'>
                                    <img src='/images/gmap.jpg' />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
