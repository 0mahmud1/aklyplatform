import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router';
import moment from 'moment';
import toastr from 'toastr';

class ProfileComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isGoogleMap: false,
    };
    this.initMap = this.initMap.bind(this);
  }

  componentWillMount() {
    const _this = this;
    if(window.google && window.google.maps){
      _this.setState({
        isGoogleMap: true,
      });
    }
    window.mapsLoaded = () => {
      _this.setState({
        isGoogleMap: true,
      });
    };
  }

  componentDidMount() {
    console.log('componentDidMount...');
    if (this.state.isGoogleMap && this.props.profile) {
      this.initMap();
    }
  }
  componentDidUpdate() {
    console.log('componentDidUpdate...');
    if (this.state.isGoogleMap && this.props.profile) {
      this.initMap();
    }
  }

  render() {
    console.log('profile: ', this.state.isGoogleMap, this.props.profile );
    if (this.state.isGoogleMap && this.props.profile) {
      return (
        <div className='fp-page-wrapper'>
          <div className='fp-profile-basicinfo'>
            <div className='fp-profile-secheading mb-20'>
              <h4>Food provider information</h4>
            </div>
            <div className='row'>
              <div className='col-sm-4 col-md-4'>
                <div className='fp-input mb-20'>
                  <label htmlFor='name'>Name</label>
                  <p>{this.props.profile.name}</p>
                </div>
                <div className='fp-input mb-20'>
                  <label htmlFor='email'>Email</label>
                  <p>{this.props.profile.email}</p>
                </div>
                <div className='fp-input mb-20'>
                  <label htmlFor='providerType'>Provider Type</label>
                  <p>{this.props.profile.providerType.join(', ')}</p>
                </div>
                <div className='fp-input mb-20'>
                  <label htmlFor='createdAt'>Created at</label>
                  <p>{moment(this.props.profile.createdAt).format('dddd, MMMM Do YYYY')}</p>
                </div>
                <div className='fp-input'>
                  <label htmlFor='updatedAt'>Updated at</label>
                  <p>{moment(this.props.profile.updatedAt).format('dddd, MMMM Do YYYY')}</p>
                </div>
              </div>

              <div className='col-sm-4 col-md-4'>
                <div className='fp-input mb-20'>
                  <label htmlFor='oldPassword'>Old Password</label>
                  <input
                    ref="oldPassword"
                    type='password'
                    className='form-control'
                    placeholder='Old Password' />
                </div>
                <div className='fp-input mb-20'>
                  <label htmlFor='newPassword'>New Password</label>
                  <input
                    ref="newPassword"
                    type='password'
                    className='form-control'
                    placeholder='New Password' />
                </div>
                <div className='fp-input mb-20'>
                  <label htmlFor='confirmPassword'>Confirm Password</label>
                  <input
                    ref="confirmPassword"
                    type='password'
                    className='form-control'
                    placeholder='Confirm Password' />
                </div>
                <Link
                  to="javascript:void(0);"
                  className="btn btn-default"
                  onClick={this.handleSubmit.bind(this)}>
                  Change Password
                </Link>
              </div>

            </div>
          </div>
          <div className='fp-profile-map'>
            <div className='row'>
              <div className='col-sm-3 col-md-3'>
                <div className='fp-profile-vaninfo mt-25'>
                  <h4 className='mb-15'>Van Information</h4>
                  {this.renderVans()}
                </div>
              </div>
              <div className='col-sm-9 col-md-9'>
                <div className='fp-profile-gmap'>
                  <div ref='googleMap' className='gmap'></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  handleSubmit(event) {
    const _this = this;
    event.preventDefault();

    const oldPassword = ReactDOM.findDOMNode(this.refs.oldPassword).value;
    const newPassword = ReactDOM.findDOMNode(this.refs.newPassword).value;
    const confirmPassword = ReactDOM.findDOMNode(this.refs.confirmPassword).value;

    console.log('Password: ', oldPassword, newPassword, confirmPassword);

    if (oldPassword.length < 1) {
      toastr.warning('Old Password is required.', 'Food Provider');
      return false;
    }

    if (newPassword.length < 1) {
      toastr.warning('New Password is required.', 'Food Provider');
      return false;
    }

    if (newPassword != confirmPassword) {
      toastr.warning('New & Confirm Password does not match.', 'Food Provider');
      return false;
    }

    this.props.changePassword(this.props.profile.email, oldPassword, newPassword, function (err, res) {
      if (err) {
        toastr.error(err.message, 'Food Provider');
      } else {
        toastr.success(res.message, 'Food Provider');

        // clear all input fields
        ReactDOM.findDOMNode(_this.refs.oldPassword).value = '';
        ReactDOM.findDOMNode(_this.refs.newPassword).value = '';
        ReactDOM.findDOMNode(_this.refs.confirmPassword).value = '';
      }
    });
  }

  renderVans() {
    if (this.props.profile && this.props.profile.vans) {
      return this.props.profile.vans.map((van) => {
        return (
          <span key={van._id}>
            <p className='mb-5'>Name - <span>{van.name}</span></p>
            <p className='mb-5'>Email - <span>{van.email}</span></p>
            <p className='mb-5'>Address - <span>{van.address}</span></p>
            <p>Capacity - <span>{van.capacity}</span></p>
            <br/>
          </span>
        );
      });
    } else {
      return null;
    }
  }

  initMap() {
    if (this.props.profile.locations && this.props.profile.locations.coordinates) {
      const uluru = {
        lng: this.props.profile.locations.coordinates[0],
        lat: this.props.profile.locations.coordinates[1],
      };
      const map = new google.maps.Map(ReactDOM.findDOMNode(this.refs.googleMap), {
        zoom: 4,
        center: uluru,
      });
      const marker = new google.maps.Marker({
        position: uluru,
        map: map,
      });
    }
  }
}

export default ProfileComponent;
