import axios from 'axios';

export const changePassword = (email, oldPassword, newPassword, callback) => {
  return function (dispatch) {
    dispatch({ type: 'FETCH_CHANGE_PASSWORD_REQUEST' });
    axios.post('changePassword', {
      email: email,
      password: oldPassword,
      newPassword: newPassword,
    }).then((response) => {
      dispatch({ type: 'FETCH_CHANGE_PASSWORD_SUCCESS', payload: response.data });
      callback(null, response.data);
    }).catch((error) => {
      console.error('getOrders:', error);
      dispatch({ type: 'FETCH_CHANGE_PASSWORD_FAILURE' });
      callback(error.response.data, null);
    });
  };
};
