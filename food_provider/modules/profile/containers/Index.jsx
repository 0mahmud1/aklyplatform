import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import toastr from 'toastr';

import ProfileComponent from '../components/Index.jsx';

import { changePassword } from '../actions/changePassword';

class ProfileContainer extends Component {
  render() {
    console.log('ProfileContainer...');
    return (
      <ProfileComponent
        profile={this.props.profile}
        changePassword={this.props.changePassword} />
    );
  }
}

function mapStateToProps(store) {
  return {
    profile: store.auth.profile,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    changePassword: changePassword,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ProfileContainer);
