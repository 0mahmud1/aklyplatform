import React from 'react';
import { Route, IndexRoute } from 'react-router';

import AppLayout from '../../core/layouts/App.jsx';
import ProfileContainer from '../containers/Index.jsx';
import { isAuthorized } from '../../libs/auth.js';


export default function () {
  return (
    <Route path='profile' component={AppLayout} onEnter={isAuthorized}>
      <IndexRoute component={ProfileContainer} />
    </Route>
  );
};
