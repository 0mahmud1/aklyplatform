import axios from 'axios';
import { browserHistory } from 'react-router';

export function authorizeUser(user, callback) {
  return function (dispatch) {
    dispatch({ type: 'FETCH_AUTH_REQUEST' });
    axios.post('authenticate', user)
      .then((response) => {
        dispatch({ type: 'FETCH_AUTH_SUCCESS', payload: response.data });
        localStorage.setItem('jwt', response.data.data.id_token);
        localStorage.setItem('access_token', response.data.data.access_token);
        localStorage.setItem('email', response.data.data.email);
        localStorage.setItem('auth0Object', JSON.stringify(response.data.data));
        browserHistory.push('/dashboard'); // redirect to dashboard
        callback(null, response.data);
      })
      .catch((error) => {
        console.error('authorizeUser(): ', error);
        dispatch({ type: 'FETCH_AUTH_FAILURE' });
        callback(error.response.data, null);
      });
  };
}

// export function logoutUser(user) {
//   return function (dispatch) {
//     dispatch({type: 'USER_LOGOUT'});
//     localStorage.removeItem('access_token','auth0Object','email','jwt');
//     browserHistory.push('/login');
//   }
// }
