import axios from 'axios';

export function getUserProfile(callback) {
  return function (dispatch) {
    dispatch({ type: 'FETCH_USER_PROFILE_REQUEST' });
    axios.get('foodproviders/check')
      .then((response) => {
        dispatch({ type: 'FETCH_USER_PROFILE_SUCCESS', payload: response.data });
        callback(null, response.data);
      })
      .catch((error) => {
        console.error('getUserProfile(): ', error);
        dispatch({ type: 'FETCH_USER_PROFILE_FAILURE' });
        callback(error.response.data, null);
      });
  };
}
