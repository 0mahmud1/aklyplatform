import jwtDecode from 'jwt-decode';

const defaultState = {
  loggingIn: false,
  token: localStorage.getItem('jwt') ? localStorage.getItem('jwt') : null,
  user: localStorage.getItem('jwt') ? jwtDecode(localStorage.getItem('jwt')) : null,
};

export const authReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'FETCH_AUTH_REQUEST':
      return Object.assign({}, state, {
        loggingIn: true,
      });
    case 'FETCH_AUTH_FAILURE':
      return Object.assign({}, state, {
        loggingIn: false,
      });
    case 'FETCH_AUTH_SUCCESS':
      return Object.assign({}, state, {
        loggingIn: false,
        token: action.payload.data.id_token ? action.payload.data.id_token : null,
        user: (action.payload.data.id_token) ? jwtDecode(action.payload.data.id_token) : null,
      });

    case 'USER_LOGOUT':
      return Object.assign({}, state, {
        token: null,
        user: null,
      });

    case 'FETCH_USER_PROFILE_REQUEST':
      return state;
    case 'FETCH_USER_PROFILE_SUCCESS':
      return Object.assign({}, state, { profile: action.payload.data });
    case 'FETCH_USER_PROFILE_FAILURE':
      return state;

    default:
      return state;
  }
};
