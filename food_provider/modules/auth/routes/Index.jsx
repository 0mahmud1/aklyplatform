import React from 'react';
import { Route, IndexRoute } from 'react-router';
import toastr from 'toastr';

import AuthLayout from '../layouts/Index.jsx';
import LoginContainer from '../containers/Login.jsx';

export default function () {
  return (
    <Route path='login' component={AuthLayout}>
      <IndexRoute component={LoginContainer} />
    </Route>
  );
};
