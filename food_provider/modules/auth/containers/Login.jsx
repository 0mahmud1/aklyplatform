import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import toastr from 'toastr';

import { authorizeUser } from '../actions/index.js';

import LoginComponent from '../components/Login.jsx';

class LoginContainer extends Component {
  render() {
    return (
      <LoginComponent
        loggingIn={this.props.loggingIn}
        authorizeUser={this.props.authorizeUser} />
    );
  }
}

function mapStateToProps(store) {
  return {
    loggingIn: store.auth.loggingIn,
    auth: store.auth,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    authorizeUser: authorizeUser,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(LoginContainer);
