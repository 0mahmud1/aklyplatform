import React, { Component } from 'react';

class NotFoundComponent extends Component {
  render() {
    return (
      <div className='row'>
        <div className='col-xs-12'>
          <h1>Not Found</h1>
        </div>
      </div>
    );
  }
}

export default NotFoundComponent;
