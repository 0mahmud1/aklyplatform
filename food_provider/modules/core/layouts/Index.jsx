import React, { Component } from 'react';

class CoreLayout extends Component {
  render() {
    return (
      <div className='container-fluid'>
        <div id='container'>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default CoreLayout;
