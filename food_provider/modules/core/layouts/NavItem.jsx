import React, {Component} from 'react';
import {Link, IndexLink, withRouter} from 'react-router';

class NavItem extends Component {
    render() {
        const { router } = this.props
        const { index, onlyActiveOnIndex, to, children, ...props } = this.props
        let isActive
        if( router.isActive('/',true) && index ) isActive = true
        else  isActive = router.isActive(to)
        const LinkComponent = index ?  IndexLink : Link

        return (
            <div className={'fp-menu__icon fp-menu-icon' + (isActive ? ' fp-menu__icon_active ' : '')}>
                <LinkComponent to={to} className="fp-menu-icon__box fp-menu-icon__dashboardbox">{children}</LinkComponent>
            </div>
        );
    }
}

NavItem = withRouter(NavItem)

export default NavItem
