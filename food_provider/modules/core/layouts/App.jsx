import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import _ from 'lodash';
import moment from 'moment';
import toastr from 'toastr';
import Push from 'push.js';
import { Howl } from 'howler';

import { pubnub } from '../../../pubnub';
import { getOrders } from '../../orders/actions/getOrders';
import { getUserProfile } from '../../auth/actions/getUserProfile';

import NavItem from './NavItem.jsx';

class AppLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cntNotify: 0,
    };
  }
  componentWillUnmount(){
    let _this = this;
    pubnub.unsubscribe({
        channels: [`deliveries-${_this.props.auth.profile._id}`],
      });
  }
  componentWillMount() {
    const _this = this;

    _this.props.getUserProfile(function (err, res) {
      if (err) {
        //toastr.error(err.message, 'User Profile');
      } else {
        //toastr.success(res.message, 'User Profile');
      }
    });

    const { router, index, onlyActiveOnIndex, to, children, ...props } = _this.props;

    pubnub.addListener({
      message: function (msg) {
        console.log('PubNub message: ', msg);
                  // Push.create('New Ondemand Order',{timeout: 5000});
        if(msg.message.text === "new delivery inserted"){
          toastr.success('Order Place');
          Push.create('New Ondemand Order ' + msg.message.data.orderRefId + ' Placed',{timeout: 5000});
          var sound = new Howl({
          src: ['/sounds/sound.mp3']
        });
        sound.play();
      } else {
          toastr.success('Delivery Update As ' + msg.message.data.status);
          //Push.create('A Delivery' + msg.message.data.deliveryRefId + ' is Update As ' + msg.message.data.status,{timeout: 5000});

        }
        

        _this.props.getOrders(moment(), function (err, res) {
          if (err) {
            toastr.error(err.message, 'Food Provider');
          } else {
            //toastr.success(res.message, 'Food Provider');
          }
        });
      },

      presence: function (presence) {
        console.log('PubNub presence: ', presence);
      },

      status: function (status) {
        console.log('PubNub status: ', status);
        if (status.category === "PNConnectedCategory") {
          if (!router.isActive('/calendar', true)) {
            _this.props.getOrders(moment(), function (err, res) {
              if (err) {
                toastr.error(err.message, 'Food Provider');
              } else {
                //toastr.success(res.message, 'Food Provider');
              }
            });
          }
        }
  
      },
    });

    if (_this.props.auth && _this.props.auth.profile && _this.props.auth.profile._id) {
      pubnub.subscribe({
        channels: [`deliveries-${_this.props.auth.profile._id}`],
        withPresence: false,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      cntNotify: nextProps.notify,
    });

    if (nextProps.auth && nextProps.auth.profile && nextProps.auth.profile._id) {
      pubnub.subscribe({
        channels: [`deliveries-${nextProps.auth.profile._id}`],
        withPresence: false,
      });
    }
  }

  render() {
    const pathName = this.props.location.pathname;
    return (
      <div>
        <header className="fp-head">
          <div className="fp-head__title">
            <h1>{_.capitalize(pathName.split('/')[1])}</h1>
          </div>
          {/*<div className="fp-head__dropdown">
           <div className="dropdown fp-hdropdown">
           <select id="orderType">
           <option value="ondemand">on demand</option>
           <option value="subscription">subscription</option>
           </select>
           </div>
           </div>*/}
          <div className="fp-head__logo logo">
            <a href="/">
              <img src="/images/akly-logo.svg" alt="" className="fp-logo__img"/>
            </a>
          </div>
          <div className="fp-head__notification fp-notification">
            <div className="fp-notification__icon fp-customicon fp-icon-bell">
              <div className="fp-notification__number">
                {this.state.cntNotify}
              </div>
            </div>
          </div>
          {/* <div className="fp-head__search search">
            <input type="text" className="fp-search__field" placeholder="Search"/>
            <div className="fp-search__icon fp-customicon fp-icon-search"></div>
          </div> */}
        </header>

        <div className="fp-container fp-wrapper clearfix">
          <div className="fp-menu col-md-1 col-sm-1 col-xs-1">
            {/* ICON CODE */}
            <NavItem to='/dashboard'>
              <svg viewBox="0 0 22 22" className="fp-menu-icon__dashboard">
                <g strokeWidth="1" fill="none" fillRule="evenodd">
                  <path
                    d="M0,15.9981266 C0,18.2028783 1.79051625,20 3.99922997,20 L16.00077,20 C18.2075324,20 20,18.2115094 20,16.0052944 L20,9.20036563 L18.7746847,9.61683565 L14.0538389,9.6114114 C13.5162693,9.61079373 12.9024674,9.19193258 12.7055406,8.67588489 L11.3982114,5.25002194 C11.2001863,4.73109621 10.8567712,4.72463963 10.6287192,5.2291725 L6.98012591,13.3011711 C6.74814651,13.8143929 6.35729571,13.8170334 6.09836406,13.326476 L5.13937929,11.5096373 C4.88076454,11.0196803 4.2202079,10.6190056 3.66469255,10.6160463 L0.138308549,10.5972606 L0,15.9981266 Z"
                    fill="#fff" className="fp-menu-icon__fill"></path>
                  <path
                    d="M20,13.2003656 L20,16.0052944 C20,18.2115094 18.2075324,20 16.00077,20 L3.99922997,20 C1.79051625,20 0,18.2028783 0,15.9981266 L-4.52437703e-06,10.6195672"
                    stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                    className="fp-menu-icon__stroke"></path>
                  <path
                    d="M0,5.97166478 L0,3.99976838 L0,3.99976838 C0,1.7907573 1.79246765,0 3.99922997,0 L16.00077,0 C18.2094838,0 20,1.79652877 20,3.9947582 L20,5.30647028 L20,9.55555736"
                    stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                    className="fp-menu-icon__stroke"></path>
                  <path
                    d="M0.138308549,10.5972606 L3.66469255,10.6160463 C4.2202079,10.6190056 4.88076454,11.0196803 5.13937929,11.5096373 L6.09836406,13.326476 C6.35729571,13.8170334 6.74814651,13.8143929 6.98012591,13.3011711 L10.6287192,5.2291725 C10.8567712,4.72463963 11.2001863,4.73109621 11.3982114,5.25002194 L12.7055406,8.67588489 C12.9024674,9.19193258 13.5162693,9.61079373 14.0538389,9.6114114 L19.9839478,9.6182251"
                    stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                    className="fp-menu-icon__stroke"></path>
                </g>
              </svg>
              {/*<div>Summery</div>*/}
            </NavItem>

            <NavItem to='/calendar'>
              <svg viewBox="0 0 22 22" className="fp-menu-icon__calendar">
                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                  <path
                    d="M20.0033748,11.6281865 L20,17.3614823 C20,19.3709801 18.2075324,21 16.00077,21 L3.99922997,21 C1.79051625,21 0,19.3710711 0,17.3614823 L0.0033748357,11.6281865"
                    stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                    className="fp-menu-icon__stroke"></path>
                  <rect stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                        x="0" y="2" width="20" height="6" rx="3" className="fp-menu-icon__stroke"></rect>
                  <path d="M4.5,0.5 L4.5,4" stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                        className="fp-menu-icon__stroke"></path>
                  <path d="M15.5,0.5 L15.5,4" stroke="#9B9B9B" strokeLinecap="round" strokeLinejoin="round"
                        className="fp-menu-icon__stroke"></path>
                  <circle fill="#9B9B9B" cx="4" cy="12" r="1" className="fp-menu-icon__fill"></circle>
                  <circle fill="#9B9B9B" cx="4" cy="17" r="1" className="fp-menu-icon__fill"></circle>
                  <circle fill="#9B9B9B" cx="10" cy="12" r="1" className="fp-menu-icon__fill"></circle>
                  <circle fill="#9B9B9B" cx="10" cy="17" r="1" className="fp-menu-icon__fill"></circle>
                  <circle fill="#9B9B9B" cx="16" cy="12" r="1" className="fp-menu-icon__fill"></circle>
                  <circle fill="#9B9B9B" cx="16" cy="17" r="1" className="fp-menu-icon__fill"></circle>
                </g>
              </svg>
            </NavItem>

            <NavItem to='/orders'>
              <svg viewBox="0 0 22 22" className="fp-menu-icon__orderlist">
                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                  <path
                    d="M20,5.30038368 L20,17.6302068 C20,18.9337135 19.0081969,19.9904144 17.7832341,19.9904144 L5.69676387,20 M0,14.8031342 L0,2.35193832 C0,1.05299865 0.997082436,0 2.22246684,0 L14.8451359,0"
                    stroke="#979797" strokeLinecap="round" strokeLinejoin="round"
                    className="fp-menu-icon__stroke"></path>
                  <g transform="translate(4.000000, 6.000000)" stroke="#979797" className="fp-menu-icon__stroke">
                    <ellipse cx="6" cy="1.21081749" rx="0.763977251" ry="0.763060836"></ellipse>
                    <path
                      d="M10.5148484,6.42020665 C9.97584475,4.4344774 8.15869782,2.97387833 6,2.97387833 C3.83182259,2.97387833 2.0082024,4.44733356 1.47812476,6.44639703"
                      strokeLinecap="round" strokeLinejoin="round" className="fp-menu-icon__fill"></path>
                    <path d="M0.381988625,6.6875 L11.5714286,6.6875" strokeLinecap="round"
                          strokeLinejoin="round"></path>
                  </g>
                  <circle stroke="#979797" cx="2" cy="18" r="1" className="fp-menu-icon__stroke"></circle>
                  <polyline stroke="#979797" strokeLinecap="round" strokeLinejoin="round"
                            transform="translate(17.085274, 2.085274) rotate(-45.000000) translate(-17.085274, -2.085274)"
                            points="15.1362514 1.08527368 15.1362514 3.08527368 19.034296 3.08527368"
                            className="fp-menu-icon__stroke"></polyline>
                </g>
              </svg>
            </NavItem>

            <NavItem to='/profile'>
              <svg viewBox="0 0 18 22" className="fp-menu-icon__profile">
                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                  <g transform="translate(-31.000000, -349.000000)" stroke="#9B9B9B" className="fp-menu-icon__stroke">
                    <g transform="translate(28.000000, 348.000000)">
                      <g transform="translate(4.000000, 2.000000)">
                        <ellipse cx="8" cy="3.9375" rx="4" ry="3.9375"></ellipse>
                        <path
                          d="M5.17718254,19.8108131 C6.82157191,20.034927 8.22135802,19.9976823 8.22135802,19.9976823 L8.22135802,19.9976823 C8.22135802,19.9976823 9.42149297,20.0314322 10.8789549,19.8413552 M14.4313687,18.7989543 C15.3526134,18.2680161 16,17.5035978 16,16.4036507 C16,15.5851087 15.8349216,14.7998389 15.5319272,14.0712812 C15.0089433,12.8137533 14.0750724,11.725185 12.8699924,10.9261122 C11.5211501,10.0317125 9.83253011,9.5 8,9.5 C3.581722,9.5 0,12.5908697 0,16.4036507 C0,17.518533 0.702939376,18.2887129 1.69772992,18.8204527"
                          strokeLinecap="round" strokeLinejoin="round"></path>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </NavItem>

            <div onClick={() => { window.location.replace('/logout');}} className="fp-menu__icon fp-menu__icon_bottom fp-menu-icon">
              <div className="fp-menu-icon__box fp-menu-icon__logoutbox">
                <svg viewBox="0 0 22 20" className="fp-menu-icon__logout">
                  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round"
                     strokeLinejoin="round">
                    <g transform="translate(-28.000000, -718.000000)" stroke="#979797" className="fp-menu-icon__stroke">
                      <g transform="translate(28.000000, 716.000000)">
                        <g transform="translate(1.000000, 3.000000)">
                          <path
                            d="M5,2.29168048 L5,2.29168048 C6.59226948,0.866560706 8.69493669,0 11,0 C15.9705627,0 20,4.02943725 20,9 C20,13.9705627 15.9705627,18 11,18 C8.69493669,18 6.59226948,17.1334393 5,15.7083195"></path>
                          <g
                            transform="translate(6.000000, 9.000000) rotate(-90.000000) translate(-6.000000, -9.000000) translate(3.000000, 3.000000)">
                            <path d="M3,0.56 L3,11.56"></path>
                            <path
                              d="M1.00655997,0.986494367 L4.42767045,0.986494367 C4.74132924,0.986494367 4.99559993,1.24197285 4.99559993,1.54874456 L4.99559993,4.93564393"
                              transform="translate(3.001080, 2.961069) rotate(-45.000000) translate(-3.001080, -2.961069)"></path>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg>
              </div>
            </div>
          </div>

          <div className="fp-main col-md-11 col-md-offset-1 col-sm-11 col-sm-offset-1 col-xs-11 col-xs-offset-1">
            <div className="fp-content">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store) {
  return {
    auth: store.auth,
    notify: store.notify,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getOrders: getOrders,
    getUserProfile: getUserProfile,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AppLayout);
