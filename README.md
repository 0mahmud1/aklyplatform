# Akly platform

Akly platofrm description

### Version
1.0.0

### Tech

Akly Platform uses a number of open source projects to work properly:

* nodejs
* expressjs
* mongodb
* webpack
* react
* redux

Akly platform  requires [Node.js](https://nodejs.org/) v4+ to run.

### API Server
###### Go to the server directory & install all dependency
```sh
$ cd api-server && npm install
```
###### Start server in normal mode
```sh
$ npm run start
```
###### Start server in debug mode
```sh
$ npm run debug
```
http://localhost:3000/
### Client App
###### Go to the server directory & install all dependency
```sh
$ cd client-app && npm install
```
###### Start app in normal mode
```sh
$ npm run start
```
###### Start app in debug mode
```sh
$ npm run debug
```
http://localhost:4000/
### API Documentation Server
###### Go to the server directory
```sh
$ cd api-server
```
###### Generate documentation & start server
```sh
$ npm run docs

```
http://localhost:5000/

### Deployment Provess

####  Client app

- go to the client directory

```sh
  $ cd client-app

```
- run build command for individual different deployment type like
  dev,stage and prod

```sh
  $  npm run dev/stage/prod
```  

- go to the public directory

```sh
  $ cd public
```
- deploy your local build file with fab command for individual different deployment type like
  dev,stage and prod

```sh
  $ fab dev/stage/prod deploy
```

####  Server API

- go to the api-server directory

```sh
  $ cd api-server

```
- deploy your local code with fab command for individual different deployment type like
  dev,stage and prod

```sh
  $ fab dev/stage/prod deploy
```


### Todos

 - Write Tests
 - Add Code Comments