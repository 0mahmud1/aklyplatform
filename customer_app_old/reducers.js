import { combineReducers } from 'redux';

import authenticateReducer from './modules/auth/reducers';
import MealPlanReducer from './modules/mealplan/reducers';
import HubsReducer from './modules/hubs/reducers';
import foodReducer from './modules/foods/reducers';
import cuMealPlan from './modules/reducers/';
import onDemand from './modules/reducers/onDemand';
import orderReducer from './modules/orders/reducers';
import subscriptionEdit from './modules/reducers/subscriptions';

import { userProfileReducer } from './modules/bmi/reducers/userProfileReducer';
import { bodyMassIndexReducer } from './modules/bmi/reducers/bodyMassIndexReducer';
import { suggestionReducer } from './modules/bmi/reducers/suggestionReducer';

const reducers = combineReducers({
  auth: authenticateReducer,
  mealPlans: MealPlanReducer,
  hubs: HubsReducer,
  foods: foodReducer,
  cuMealPlan: cuMealPlan,
  onDemand: onDemand,
  orders: orderReducer,
  subscriptionEdit: subscriptionEdit,
  userProfile: userProfileReducer,
  bodyMassIndex: bodyMassIndexReducer,
  suggestions: suggestionReducer,
});

export default reducers;
