var webpack = require('webpack');
var config = require('config');
var path = require('path');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var WebpackManifestPlugin = require('webpack-manifest-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var postcssImport = require('postcss-import');
var postcssUrl = require('postcss-url');
var autoprefixer = require('autoprefixer');

module.exports = {
  devtool: 'eval-source-map',
  entry: {
    dashboard: './index.js',
  },
  debug: true,
  output: {
    path: path.join(__dirname, 'public/build'),
    filename: '[name].js',
  },
  module: {
    noParse: [
      /aws\-sdk/,
    ],
    loaders: [
      {
        test: /\.css$/,
        include: [path.resolve(__dirname)],
        loader: ExtractTextWebpackPlugin.extract('style-loader', 'css-loader!postcss-loader'),
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        include: __dirname,
        loaders: ['babel'],
      }, {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'node_modules/react-switch-button/src/react-switch-button.js'),
        ],
        loaders: ['babel'],
      }, {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10000&name=images/[name].[ext]',
        include: [
          path.resolve(__dirname),
        ],
      }, {
        test: /\.(svg|eot|woff|woff2|ttf|otf)$/,
        loader: 'url-loader?limit=10000&name=fonts/[name].[ext]',
        include: [
          path.resolve(__dirname),
        ],
      }, {
        test: /\.json$/,
        loader: 'json-loader',
      }, {
        test: /jquery-mousewheel/,
        loader: 'imports?define=>false&this=>window',
      }, {
        test: /malihu-custom-scrollbar-plugin/,
        loader: 'imports?define=>false&this=>window',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        BASE_URL: JSON.stringify(config.get('BASE_URL')),
        API_URL: JSON.stringify(config.get('API_URL')),
        AUTH0_BASE_URL: JSON.stringify(config.get('AUTH0_BASE_URL')),
        AUTH0_CLIENT_ID: JSON.stringify(config.get('AUTH0_CLIENT_ID')),
        AUTH0_CONNECTION: JSON.stringify(config.get('AUTH0_CONNECTION')),
        AKLY_LOGO: JSON.stringify(config.get('AKLY_LOGO')),
      },
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new ExtractTextWebpackPlugin('[name].css'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new WebpackManifestPlugin({
      basePath: '/build/',
    }),
    new CleanWebpackPlugin(['build', 'index.html'], {
      root: path.join(__dirname, 'public'),
      verbose: true,
    }),
  ],
  postcss: function (webpack) {
    return [
      postcssImport({ addDependencyTo: webpack }),
      postcssUrl(),
      autoprefixer({
        browsers: ['last 5 versions'],
      }),
    ];
  },

  resolve: {
    alias: {
      devConfig: 'dev.config.js'
    },
    root: [
      path.resolve('.'),
      path.resolve('./node_modules'),
    ],
  },
};
