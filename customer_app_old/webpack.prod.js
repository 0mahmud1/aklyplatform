var webpack = require('webpack');
var config = require('config');
var path = require('path');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var WebpackManifestPlugin = require('webpack-manifest-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var postcssImport = require('postcss-import');
var postcssUrl = require('postcss-url');
var autoprefixer = require('autoprefixer');
var CompressionPlugin = require('compression-webpack-plugin');

// start for execute additional shell command before and after webpack command

var exec = require('child_process').exec;

function puts(error, stdout, stderr) {
  console.log(stdout);
}

function WebpackShellPlugin(options) {
  var defaultOptions = {
    onBuildStart: [],
    onBuildEnd: [],
  };
  this.options = Object.assign(defaultOptions, options);
}

WebpackShellPlugin.prototype.apply = function (compiler) {
  const options = this.options;

  compiler.plugin('compilation', compilation => {
    if (options.onBuildStart.length) {
      console.log('Executing pre-build scripts');
      options.onBuildStart.forEach(script => exec(script, puts));
    }
  });

  compiler.plugin('emit', (compilation, callback) => {
    if (options.onBuildEnd.length) {
      console.log('Executing post-build scripts');
      options.onBuildEnd.forEach(script => exec(script, puts));
    }

    callback();
  });
};

// end webpack additional shell command

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: {
    dashboard: './index.js',
  },
  output: {
    path: path.join(__dirname, 'public/build'),
    filename: '[name].[hash].js',
  },
  module: {
    noParse: [
      /aws\-sdk/,
    ],

    loaders: [
      {
        test: /\.css$/,
        include: [path.resolve(__dirname)],
        loader: ExtractTextWebpackPlugin.extract('style-loader', 'css-loader!postcss-loader'),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: __dirname,
        loaders: ['babel'],
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'node_modules/react-switch-button/src/react-switch-button.js'),
        ],
        loaders: ['babel'],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader?limit=10000&name=images/[name].[ext]',
        include: [
          path.resolve(__dirname),
        ],
      },
      {
        test: /\.(svg|eot|woff|woff2|ttf|otf)$/,
        loader: 'url-loader?limit=10000&name=fonts/[name].[ext]',
        include: [
          path.resolve(__dirname),
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        BASE_URL: JSON.stringify(config.get('BASE_URL')),
        API_URL: JSON.stringify(config.get('API_URL')),
        AUTH0_BASE_URL: JSON.stringify(config.get('AUTH0_BASE_URL')),
        AUTH0_CLIENT_ID: JSON.stringify(config.get('AUTH0_CLIENT_ID')),
        AUTH0_CONNECTION: JSON.stringify(config.get('AUTH0_CONNECTION')),
        AKLY_LOGO: JSON.stringify(config.get('AKLY_LOGO')),
      },
    }),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new ExtractTextWebpackPlugin('[name].[hash].css'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new WebpackManifestPlugin({
      basePath: '/build/',
    }),
    new CleanWebpackPlugin(['build'], {
      root: path.join(__dirname, 'public'),
      verbose: true,
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      output: {
        comments: false,
      },
      compressor: {
        warnings: false,
      },
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
    new WebpackShellPlugin({
      onBuildEnd: ['python public/build.py'],
    }),
  ],
  postcss: function (webpack) {
    return [
      postcssImport({ addDependencyTo: webpack }),
      postcssUrl(),
      autoprefixer({
        browsers: ['last 5 versions'],
      }),
    ];
  },

  resolve: {
    root: [
      path.resolve('.'),
    ],
  },
};
