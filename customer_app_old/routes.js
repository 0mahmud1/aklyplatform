import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import Auth0LockService from './AuthService';
import * as AuthService from './modules/libs/auth';

import CoreLayoutCA from './modules/core/layouts/';
import NotFoundComponent from './modules/ui/components/not-found';
import SupportPageContainer from './modules/containers/SupportPageContainer';
import HomeComponentCA from './modules/core/components/home';
import MealPlanDetailsContainer from './modules/containers/MealPlanCustomerContainer';
import SubscriptionEditContainer from './modules/containers/SubscriptionEditContainer';
import OrderSummaryContainer from './modules/containers/OrderSummaryContainer';
import MPCheckoutContainer from './modules/containers/MPCheckout';
import MPOrderConfirmContainer from './modules/containers/MPConfirmation';
import ProfileContainer from './modules/containers/profile';
import UiRouteCA from './modules/ui/routes';
import MenuContainer from './modules/containers/menu';
import ODSummaryContainer from './modules/containers/ODSummaryContainer';
import SubscriptionOrderListContainer from './modules/containers/SubscriptionOrderList';
import SubscriptionOrderDetailsContainer from './modules/containers/SubscriptionOrderDetails';
import SetMealplanDetailsContainer from './modules/containers/SetMealplanDetailsContainer';
import SettingsContainer from './modules/containers/Settings';

import BMIRoutes from './modules/bmi/routes';

console.log('BASE_URL>>>', process.env.BASE_URL);

//set time zone
import moment from 'moment-timezone';
moment.tz.setDefault('Asia/Qatar');


let options = {
  additionalSignUpFields: [
    {
      name: 'full_name',
      placeholder: 'Enter your full name',
      icon: "/icons/user.svg"
    },
  ],
  auth: {
    params: {
      scope: 'openid email roles app_metadata',
    },
    redirectUrl: process.env.BASE_URL + '/checkout',
    responseType: 'token',
  },
  rememberLastLogin: false,
  theme: {
    logo: process.env.AKLY_LOGO,
    labeledSubmitButton: false
  },
  avatar: null
};

const auth0Lock = new Auth0LockService(
  process.env.AUTH0_CLIENT_ID,
  process.env.AUTH0_BASE_URL,
  options
);

class Routes extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/'>
          <Route components={CoreLayoutCA} auth0Lock={auth0Lock}>
            <IndexRoute components={MenuContainer} />
            <Route path={`subscription`} component={MealPlanDetailsContainer} />
            <Route path='summary' component={OrderSummaryContainer}/>
            <Route path='checkout' component={MPCheckoutContainer}/>
            <Route path={`on-demand-summary`} component={ODSummaryContainer} />
            <Route path='confirmation/:_id' component={MPOrderConfirmContainer}/>
            <Route path={`profile`} component={ProfileContainer} />
            <Route path={`order-list`} component={SubscriptionOrderListContainer} />
            <Route path={`order-list/:_id`} component={SubscriptionOrderDetailsContainer} />
            <Route path={`profile/settings`} component={SettingsContainer}/>
            <Route path={`subscription/:orderRefId`} component={SubscriptionEditContainer}/>
            <Route path={'mealplans/:_id'} component={SetMealplanDetailsContainer} />
            <Route path={'support'} component={SupportPageContainer} />
            {UiRouteCA()}
          </Route>
          {BMIRoutes()}
          <Route path='*' component={NotFoundComponent}/>
        </Route>
      </Router>
    );
  }
}

export default Routes;
