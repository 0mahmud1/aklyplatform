import React, {
  Component,
  PropTypes,
} from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import moment from 'moment'


import SubscriptionEditDetailsComponent from '../ui/components/SubscriptionEditDetails';
import { getMealPlans, getFoods } from '../actions/subscription';
import { getDeliveriesByOrderRefId, createDelivery, updateDelivery, deleteDelivery, getOrderByRefId } from '../actions/order';
import SubscriptionEditFoodListComponent from '../ui/components/SubscriptionEditFoodList';
import { getAllDaysOfNextMonth } from '../libs';

class SubscriptionEditContainer extends Component {
  componentWillMount() {

    this.props.getDeliveriesByOrderRefId(this.props.params.orderRefId);
    this.props.getOrderByRefId(this.props.params.orderRefId, (err, res) => {
      if(res) {
        let allDays = getAllDaysOfNextMonth(moment(res[0].orderDate),res[0].planType === 'fullDay' ? true : false );
        this.props.saveAllDays(allDays);
      }

    });
    this.props.getMealPlans();
    this.props.getFoods();
  }

  render() {
    console.log('SubscriptionEditContainer...', this.props);
    let isDaysGenerated = (this.props.mealPlan.allDays);
    let isPopUpSelected = (this.props.mealPlan.selectedDeliveryFoodMenu);
    return (
      isDaysGenerated ?
        isPopUpSelected ?
        <SubscriptionEditFoodListComponent {...this.props} /> : <SubscriptionEditDetailsComponent {...this.props} />
        : null
    );
  }
}

const mapStateToProps = (store) => {
  return {
    mealPlan: store.cuMealPlan,
    foods: store.foods,
    dbMealPlans: store.mealPlans,
    order: store.orders.order[0],
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getMealPlans: () => dispatch(getMealPlans()),
    getFoods: () => dispatch(getFoods()),
    getDeliveriesByOrderRefId: (orderRefId) => dispatch(getDeliveriesByOrderRefId(orderRefId)),
    getOrderByRefId: (orderRefId,cb) => dispatch(getOrderByRefId(orderRefId,cb)),
    selectWeek: (number) => dispatch({ type: 'SELECT_WEEK', payload: number }),
    saveAllDays: (data) => dispatch({ type: 'SAVE_ALL_DAYS', payload: data }),
    addFood: () => dispatch({ type: 'ADD_FOOD_MEALPLAN' }),
    authenticate: (id_token) => dispatch({
      type: 'AUTHENTICATION_SUCCESS',
      payload: {
        id_token: id_token,
      },
    }),
    invokeFoodPopUp: (deliveryDate, shift) => dispatch({
      type: 'SHOW_FOODLIST',
      payload: {
        deliveryDate,
        shift,
      },
    }),
    hideFoodPopUp: () => dispatch({ type: 'HIDE_FOODLIST' }),
    createFoodTags: () => dispatch({ type: 'LIST_ALL_FOOD_TAGS' }),
    addFilterTag: (tag) => dispatch({ type: 'ADD_FILTER_TAGLIST', payload: tag }),
    removeFilterTag: (tag) => dispatch({ type: 'REMOVE_FILTER_TAGLIST', payload: tag }),
    filterFood: () => dispatch({ type: 'FILTER_FOOD' }),
    addFoodTemp: (food) => dispatch({ type: 'ADD_FOOD_TEMP', payload: food }),
    reduceFoodTemp: (food) => dispatch({ type: 'REDUCE_FOOD_TEMP', payload: food }),
    copyMealPlan: (index) => dispatch({ type: 'COPY_MEALPLAN', payload: index }),
    setMealPlan: (deliveries) => dispatch({ type: 'SET_MEALPLANS', payload: deliveries }),
    clearAllMealPlan: () => dispatch({ type: 'CLEAR_MEALPLAN_DELIVERY' }),
    removeMealFromDelivery: (delivery, meal) => dispatch({
      type: 'REMOVE_MEAL_FROM_DELIVERY',
      payload: {
        delivery,
        meal,
      },
    }),
    createDelivery: (data,cb) => dispatch(createDelivery(data,cb)),
    updateDelivery: (_id,data,cb) => dispatch(updateDelivery(_id,data,cb)),
    deleteDelivery: (_id,cb) => dispatch(deleteDelivery(_id,cb)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionEditContainer);
