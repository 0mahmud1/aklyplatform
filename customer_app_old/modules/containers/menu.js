import React, {
  Component,
  PropTypes,
} from 'react';
import { connect } from 'react-redux';

import { getFoods } from '../actions/subscription';

import MenuComponentCA from '../ui/components/menu';

class MenuContainer extends Component {
  componentDidMount() {
    this.props.getFoods();
  }

  componentWillUnmount() {
    this.props.resetFilterFoodTag();
  }

  render() {
    return (
      <MenuComponentCA {...this.props }/>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    foods: store.foods,
    onDemand: store.onDemand,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getFoods: () => dispatch(getFoods()),
    addFoodToCart: (food) => dispatch({ type: 'ADD_FOOD_CART', payload: food }),
    reduceFoodQtyToCart: (food) => dispatch({ type: 'REDUCE_FOOD_QTY_CART', payload: food }),
    listAllFoodTags: () => dispatch({ type: 'LIST_ALL_FOOD_TAGS' }),
    filterFood: () => dispatch({ type: 'FILTER_FOOD' }),
    selectAllTag: (value) => dispatch({ type: 'SELECT_ALL_TAG', payload: value }),
    addFilterTag: (tag) => dispatch({ type: 'ADD_FILTER_TAGLIST', payload: tag }),
    removeFilterTag: (tag) => dispatch({ type: 'REMOVE_FILTER_TAGLIST', payload: tag }),
    resetFilterFoodTag: () => dispatch({ type: 'RESET_FILTER' }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuContainer);
