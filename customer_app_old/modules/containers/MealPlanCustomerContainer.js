import React, {
  Component,
  PropTypes,
} from 'react';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';

import MealPlanDetailsComponent from '../ui/components/mealplan-details';
import { getMealPlans, getFoods } from '../actions/subscription';
import MPFoodListComponent from '../ui/components/mp_foodlist';
import { getAllDaysOfNextMonth } from '../libs';

class MealPlanCustomerContainer extends Component {
  componentWillMount() {
    this.props.getMealPlans(localStorage.getItem('planType') ? localStorage.getItem('planType') : 'lunch');
    this.props.getFoods();
    let allDays = getAllDaysOfNextMonth(this.props.mealPlan.startDate,localStorage.getItem('planType') === 'fullDay' ? true : false);
    this.props.saveAllDays(allDays);
  }

  render() {
    console.log('MealPlanCustomerContainer...', this.props);
    let isDaysGenerated = (this.props.mealPlan.allDays);
    let isPopUpSelected = (this.props.mealPlan.selectedDeliveryFoodMenu);
    return (
      isDaysGenerated ?
        isPopUpSelected ?
        <MPFoodListComponent {...this.props} /> : <MealPlanDetailsComponent {...this.props} />
        : null
    );
  }
}

const mapStateToProps = (store) => {
  return {
    mealPlan: store.cuMealPlan,
    foods: store.foods,
    dbMealPlans: store.mealPlans,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getMealPlans: (planType) => dispatch(getMealPlans(planType)),
    getFoods: () => dispatch(getFoods()),
    selectWeek: (number) => dispatch({ type: 'SELECT_WEEK', payload: number }),
    setPlanType: (type) => dispatch({ type: 'SET_MEALPLAN_TYPE', payload: type}),
    saveAllDays: (data) => dispatch({ type: 'SAVE_ALL_DAYS', payload: data }),
    addFood: () => dispatch({ type: 'ADD_FOOD_MEALPLAN' }),
    authenticate: (id_token) => dispatch({
      type: 'AUTHENTICATION_SUCCESS',
      payload: {
        id_token: id_token,
      },
    }),
    invokeFoodPopUp: (deliveryDate, shift) => dispatch({
      type: 'SHOW_FOODLIST',
      payload: {
        deliveryDate,
        shift,
      },
    }),
    hideFoodPopUp: () => dispatch({ type: 'HIDE_FOODLIST' }),
    createFoodTags: () => dispatch({ type: 'LIST_ALL_FOOD_TAGS' }),
    addFilterTag: (tag) => dispatch({ type: 'ADD_FILTER_TAGLIST', payload: tag }),
    removeFilterTag: (tag) => dispatch({ type: 'REMOVE_FILTER_TAGLIST', payload: tag }),
    filterFood: () => dispatch({ type: 'FILTER_FOOD' }),
    addFoodTemp: (food) => dispatch({ type: 'ADD_FOOD_TEMP', payload: food }),
    reduceFoodTemp: (food) => dispatch({ type: 'REDUCE_FOOD_TEMP', payload: food }),
    copyMealPlan: (index) => dispatch({ type: 'COPY_MEALPLAN', payload: index }),
    clearAllMealPlan: () => dispatch({ type: 'CLEAR_MEALPLAN_DELIVERY' }),
    removeMealFromDelivery: (delivery, meal) => dispatch({
      type: 'REMOVE_MEAL_FROM_DELIVERY',
      payload: {
        delivery,
        meal,
      },
    }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MealPlanCustomerContainer);
