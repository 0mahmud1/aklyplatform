/**
 * Created by Sajid on 2/1/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SettingsComponent from '../ui/components/settings_component';
import { changePassword } from '../actions/user';

class SettingsContainer extends Component{
  render() {
    return (
      <SettingsComponent
        email = {this.props.email}
        changePassword = {this.props.changePassword}
      />
  );
  }
}

const mapStateToProps = (store) => {
  return {
    email: store.auth.user.email,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    changePassword: (data) => { dispatch(changePassword(data))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer);
