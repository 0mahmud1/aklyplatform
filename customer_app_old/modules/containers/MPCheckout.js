import React, {
  Component,
  PropTypes,
} from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import MPCheckoutComponent from '../ui/components/mp_checkout';
import { updateUser, readUserByAuth0 } from '../actions/user';
import { createOrder } from '../actions/order';
import { getZones } from '../actions/common';
import axios from 'axios';

class MPCheckoutContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newGeoLocationLat: null,
      newGeoLocationLng: null,
      map: null,
      marker: null,
      address: '',
    };
  }

  setMarkerForAddress(value) {
    let _this = this;
    let marker = null;
    let address = JSON.parse(value);
    if (address && address.geoLocation) {
      console.log('setMarkerForAddress... ', typeof address, address.geoLocation);
      if (_this.state.marker) {
        marker = _this.state.marker;
        _this.setState({
          newGeoLocationLat: address.geoLocation.lat,
          newGeoLocationLng: address.geoLocation.lng,
        });
        marker.setPosition({
          lat: address.geoLocation.lat,
          lng: address.geoLocation.lng,
        });
        _this.state.map.setCenter({
          lat: address.geoLocation.lat,
          lng: address.geoLocation.lng,
        });
      } else {
        marker = new google.maps.Marker({
          map: _this.state.map,
          draggable: true,
          position: address.geoLocation,
        });
        _this.setState({
          newGeoLocationLat: address.geoLocation.lat,
          newGeoLocationLng: address.geoLocation.lng,
          marker: marker,
        });
        _this.state.map.setCenter({
          lat: address.geoLocation.lat,
          lng: address.geoLocation.lng,
        });
      }
    }
  }

  mapLoader() {
    let _this = this;
    let map = null;
    let marker = null;

    map = new google.maps.Map(document.getElementById('checkout-map'), {
      center: { lat: 25.27932, lng: 51.52245 },
      zoom: 15,
    });

    let input = document.getElementById('search-place-checkout') ;
    let searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    this.setMarker();

    searchBox.addListener('places_changed', function () {
      let place = searchBox.getPlaces()[0], marker; //WE ARE ONLY CONCERN ABOUT THE FIRST ELEMENT
      console.log('select primary area   ', place);

      if (!place.geometry) {
        return;
      }

      map.setCenter(place.geometry.location);

      if (!_this.state.marker) {
        marker = new google.maps.Marker({
          map: map,
          title: place.name,
          draggable: true,
          position: place.geometry.location,
        });
        _this.setState({
          newGeoLocation: { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() },
          marker: marker,
        });
      } else {
        marker = _this.state.marker;
        marker.setPosition({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() });
        _this.setState({
          newGeoLocation: { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() },
        });
      }

      // _this.drawZones(map, _this.props.hubs.hubList)
      //_this.addressInZone(_this.state.newGeoLocation,_this.props.hubs.hubList)
      //reactDOM.props.saveMarkerPosition({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});

      google.maps.event.addListener(marker, 'dragend', function (evt) {
        console.log('marker dragged ', evt.latLng.lat(), evt.latLng.lng());
        _this.setState({
          newGeoLocation: { lat: evt.latLng.lat(), lng: evt.latLng.lng() },
        });
        //_this.addressInZone(_this.state.newGeoLocation,_this.props.hubs.hubList)
        //reactDOM.props.saveMarkerPosition({lat: evt.latLng.lat(), lng: evt.latLng.lng()});
      });
    });

    this.setState({
      map: map,
    });
  }

  addressInZone(pin, zones) {
    let _this = this;
    console.log('this: ', _this);
    if (!pin || !zones) {
      //toastr.error('please point your address in google maps')
      return;
    }

    let inZone = false;
    let formatPosition = new google.maps.LatLng(pin.lat, pin.lng);
    zones.forEach(function (zone) {
      let zonePoly = new google.maps.Polygon({ paths: _this.convertPolygonArrayToObject(zone.locations.coordinates[0]) });
      if (google.maps.geometry.poly.containsLocation(formatPosition, zonePoly)) {
        inZone = true;
        return inZone;
      }
    });
    console.log('checking marker in zone ', inZone);
    return inZone;
  }

  setMarker() {
    console.log('setMarker: ', this);
    let _this = this;
    let marker;
    navigator.geolocation.getCurrentPosition(function (position) {
      if (_this.state.marker && _this.state.map) {
        _this.state.marker.setPosition({ lat: position.coords.latitude, lng: position.coords.longitude });
        _this.drawZones(_this.state.map, _this.props.hubs.hubList);
        _this.state.map.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude });
        _this.setState({
          newGeoLocation: { lat: position.coords.latitude, lng: position.coords.longitude },
        });
      } else {
        _this.drawZones(_this.state.map, _this.props.hubs.hubList);
        _this.state.map.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude });
        marker = new google.maps.Marker({
          map: _this.state.map,
          draggable: true,
          position: { lat: position.coords.latitude, lng: position.coords.longitude },
        });
        _this.setState({
          newGeoLocation: { lat: position.coords.latitude, lng: position.coords.longitude },
          marker: marker,
        });
      }

      $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude,
        function (response) {
          _this.setState({
            address: response.results[0].formatted_address,
          });
          $('#search-place-checkout').val(response.results[0].formatted_address);
        });
      });
  }

  drawZones(map, zones) {
    console.log('drawing zone');
    let _this = this;
    zones.forEach(function (zone) {
      var zpolygon = new google.maps.Polygon({
        paths: _this.convertPolygonArrayToObject(zone.locations.coordinates[0]),
        strokeColor: '#32CD32',
        strokeOpacity: 1,
        strokeWeight: 2,
        fillColor: '#32CD32',
        fillOpacity: 0.4,
      });
      zpolygon.setMap(map);
    });
  }

  convertGeoArrayToObject(point) {
    return { lng: point[0], lat: point[1] };
  }

  convertPolygonArrayToObject(polygon) {
    let pl = [];
    polygon.forEach(point => {
      pl.push({ lng: point[0], lat: point[1] });
    });
    return pl;
  }

  componentDidMount() {
    if (this.props.auth && this.props.auth.googleLoaded) {
      this.mapLoader();
    }

    this.props.getZones()
    .then(res => {
      this.drawZones(this.state.map, this.props.hubs.hubList);
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.auth) {
      if ((!prevProps.auth.googleLoaded && this.props.auth.googleLoaded)) {
        this.mapLoader();
      }
    }
  }

  render() {
    console.group('MPCheckoutContainer => render => state');
    console.log(this.state);
    console.groupEnd();

    return (
      this.props.auth && this.props.auth.googleLoaded ?
        <MPCheckoutComponent
          {...this.props}
          addressInZone={this.addressInZone.bind(this)}
          newGeoLocation={this.state.newGeoLocation}
          setMarker={this.setMarker.bind(this)}
          setMarkerForAddress={this.setMarkerForAddress.bind(this)} />
      : null
    );
  }
}

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    mealPlan: store.cuMealPlan,
    onDemand: store.onDemand,
    foods: store.foods,
    hubs: store.hubs,
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    updateUser: (_id, patch, cb) => dispatch(updateUser(_id, patch, cb)),
    refreshCustomer: (header) => dispatch(readUserByAuth0(header)),
    createOrder: (order, cb) => dispatch(createOrder(order, cb)),
    getZones: () => dispatch(getZones()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MPCheckoutContainer);
