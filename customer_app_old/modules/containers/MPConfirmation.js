import React, {
  Component,
  PropTypes,
} from 'react';

import MPOrderConfirmComponent from '../ui/components/order-confirm';
import { connect } from 'react-redux';

import { getOrder } from '../actions/order';

class MPOrderConfirmContainer extends Component {
  componentWillMount() {
    this.props.getOrder(this.props.params._id);
  }

  render() {
    return (
      <MPOrderConfirmComponent {...this.props }/>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    auth: store.auth,
    orders: store.orders,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getOrder: (_id) => dispatch(getOrder(_id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MPOrderConfirmContainer);
