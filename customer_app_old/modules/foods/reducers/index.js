import _ from 'lodash'

let defaultState = {
  //food: {},
  foodList: [],
  filteredFoodList: [],
  searchResult: [],
  quickList: [], // for mealplan module,
  filterTagList: []

};

const foodReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'CREATE_FOOD_PENDING':
      return Object.assign({}, state, { uploading: true });

    case 'CREATE_FOOD_RESOLVED':
      return Object.assign({}, state, { uploading: false });
    case 'GET_FOOD_RESOLVED':
      console.log(action.payload.data.data);
      return Object.assign({}, state,
        {
          foodList: action.payload.data.data,
          filteredFoodList: action.payload.data.data,
          totalPage: action.payload.data.limit == 0 ? 1 : Math.ceil(action.payload.data.total / action.payload.data.limit),
        });

    case 'READ_FOOD_RESOLVED':
      return Object.assign({}, state, { food: action.payload.data.data });

    case 'SEARCH_FOOD':
      let str = new RegExp(action.payload, 'gi');
      let searchResult = [];
      if (str.length == 0)
        return state;
      for (var i = 0; i < state.foodList.length; i++) {
        if (str.test(state.foodList[i].name)) {
          searchResult.push(state.foodList[i]);
        }
      }

      return Object.assign({}, state, { searchResult: searchResult });

    case 'PUSH_QUICK_LIST':
      return Object.assign({}, state, { quickList: state.quickList.concat(action.payload) });

    case 'REMOVE_ITEM_FROM_QUICK_LIST':
      _.remove(state.quickList, function (foodItem) {
      	return foodItem._id === action.payload._id;
      });

      console.log('filter food item: ', state.quickList);

      return Object.assign({}, state, { quickList: state.quickList });

    case 'LIST_ALL_FOOD_TAGS':
      let allTags = listAllTags(state.foodList);
      console.log('ALL TAGS:    ', allTags);
      return Object.assign({}, state, { foodTags: allTags });

    case 'FILTER_FOOD':
      if (!state.filterTagList || state.filterTagList.length == 0)
        return Object.assign({}, state, { filteredFoodList: state.foodList });

      let filteredFood = _.filter(state.foodList, function (food) {
        return _.intersection(state.filterTagList.map(ft => ft._id), food.tags.map(t => t._id)).length > 0;
      });

      //console.log('TagBy filter ', filteredFood)

      return Object.assign({}, state, { filteredFoodList: filteredFood });

    case 'ADD_FILTER_TAGLIST':
      let selectedTags = [...state.filterTagList];
      selectedTags.push(findTag(state.foodTags, action.payload));
      return Object.assign({}, state, { filterTagList: selectedTags });

    case 'REMOVE_FILTER_TAGLIST' :
      let rmTags = [...state.filterTagList];
      _.remove(rmTags, function (n) {
        return n._id === action.payload;
      });

      return Object.assign({}, state, { filterTagList: rmTags });

    case 'SELECT_ALL_TAG':
      return Object.assign({}, state, { filterTagList: action.payload ? state.foodTags : [] });

    case 'RESET_FILTER':
      return Object.assign({}, state, { filterTagList: [] });

    default :
      return state;
  }
};

export default foodReducer;

function listAllTags(items) {
  let tags = _.flatten(items.map(item => item.tags));
  return _.uniqBy(tags, tag => tag._id);
}

function findTag(list, _id) {
  return _.find(list, (o) => {
    return o._id === _id;
  });
}
