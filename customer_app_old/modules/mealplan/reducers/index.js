import * as constants from '../constants';
import _ from 'lodash';

const defaultState = {
  mealPlan: {},
  mealPlanList: [],
  mealBuilder: [],
};

const MealPlanReducer = (state = defaultState, action) => {
  let modifiedMealbuilder = [].concat(state.mealBuilder), dayFound;
  switch (action.type) {
    case 'MODAL_CONTENT':
      return Object.assign({}, state, { modalContent: action.payload });

    case 'PUSH_ITEM': //COMPARATIVELY BIG CASE.
      modifiedMealbuilder = [].concat(state.mealBuilder);
      dayFound = _.find(state.mealBuilder, function (item) {
        return item.day == action.payload.day && item.slot == action.payload.slot;
      });

      console.log('day found ', dayFound);
      if (!dayFound) {
        let data = {
          day: action.payload.day,
          slot: action.payload.slot,
          foods: [{ item: action.payload.food, quantity: 1 }],
        };
        return Object.assign({}, state, { mealBuilder: state.mealBuilder.concat(data) });
      } else {
        let position = _.findIndex(state.mealBuilder, { day: action.payload.day, slot: action.payload.slot });
        console.log('position index ', position, 'current builder', state.mealBuilder);
        if (position != -1) {
          // already added food on specific day,slot
          // chances are this food is already added, then INCREMENT OR add that food(in next else loop)
          console.log('existing food ', state.mealBuilder[position].foods);
          let isFoodAdded = _.findIndex(state.mealBuilder[position].foods, function (o) {
            return action.payload.food._id === o.item._id;
          });

          console.log('isThisFoodAdded ', isFoodAdded);
          if (isFoodAdded != -1) {
            // DO INCREMENT
            console.log('SHOULD INCREMENT');
            let updatedFoodList = state.mealBuilder[position].foods;
            updatedFoodList[isFoodAdded].quantity++;
            modifiedMealbuilder[position].foods = updatedFoodList;
            console.log(updatedFoodList, modifiedMealbuilder);
            return Object.assign({}, state, { mealBuilder: modifiedMealbuilder });
          } else {
            console.log('new food in the list');
            let updatedFoodList = state.mealBuilder[position].foods;
            updatedFoodList.push({
              item: action.payload.food,
              quantity: 1,
            });
            modifiedMealbuilder[position].foods = updatedFoodList;
            console.log(updatedFoodList, modifiedMealbuilder);
            return Object.assign({}, state, { mealBuilder: modifiedMealbuilder });
          }
        }
      }

    case 'REDUCE_ITEM':
      modifiedMealbuilder = [].concat(state.mealBuilder),
        dayFound = _.find(state.mealBuilder, function (item) {
          return item.day == action.payload.day && item.slot == action.payload.slot;
        });

      //let  quantity = action.payload.food.quantity;
      let mealdayIndex = _.findIndex(state.mealBuilder, { day: action.payload.day, slot: action.payload.slot });
      let foodIndex = findIndexfromObjectArray(dayFound.foods, action);

      console.log('....  ', mealdayIndex, foodIndex);
      if (foodIndex == -1 || mealdayIndex == -1) return state;

      dayFound.foods[foodIndex].quantity--;
      console.log('found in reduce item ', dayFound.foods);
      if (dayFound.foods[foodIndex].quantity < 1) {
        console.log('>>>bD ', foodIndex, dayFound.foods[foodIndex].quantity, dayFound.foods);
        dayFound.foods.splice(foodIndex, 1);
        console.log('>>>aD ', foodIndex, dayFound.foods[foodIndex] ? dayFound.foods[foodIndex].quantity : '-', dayFound.foods);
      }

      if (dayFound.foods[foodIndex]) {
        modifiedMealbuilder[mealdayIndex].foods[foodIndex] = dayFound.foods[foodIndex];
      } else {
        modifiedMealbuilder[mealdayIndex].foods.splice(foodIndex, 1);
      }

      console.log('>>> ', modifiedMealbuilder);
      return Object.assign({}, state, { mealBuilder: modifiedMealbuilder });

    case 'INCREASE_ITEM':
      modifiedMealbuilder = [].concat(state.mealBuilder),
        dayFound = _.find(state.mealBuilder, function (item) {
          return item.day == action.payload.day && item.slot == action.payload.slot;
        });

      let _mealdayIndex = _.findIndex(state.mealBuilder, { day: action.payload.day, slot: action.payload.slot });
      let _foodIndex = findIndexfromObjectArray(dayFound.foods, action);

      dayFound.foods[_foodIndex].quantity++;
      modifiedMealbuilder[_mealdayIndex].foods[_foodIndex] = dayFound.foods[_foodIndex];

      return Object.assign({}, state, { mealBuilder: modifiedMealbuilder });

    case constants.READ_MEALPLAN_RESOLVED:
      return Object.assign({}, state, { mealPlan: action.payload, mealBuilder: action.payload.plans });

    case constants.GET_MEALPLAN_PENDING:
      return Object.assign({}, state, { loading: true });

    case constants.GET_MEALPLAN_RESOLVED:
      return Object.assign({}, state,
        {
          mealPlanList: action.payload.data,
          loading: false,
          totalPage: action.payload.limit == 0 ? 1 : Math.ceil(action.payload.total / action.payload.limit) // if limit is 0 set total number of page to one
        });

    case constants.GET_MEALPLAN_REJECTED:
      return Object.assign({}, state, { loading: false });

    case 'RESET_MEALPLAN_REDUCER':
      return defaultState;

    default:
      return state;
  }
};

function findIndexfromObjectArray(list, action) {
  if (list.length <= 0)
    return -1;
  for (var i = 0; i < list.length; i++) {
    console.log('item:  ', list[i]);
    if (list[i].item._id == action.payload.food.item._id) return i;
  }

  return -1;
}

export default MealPlanReducer;
