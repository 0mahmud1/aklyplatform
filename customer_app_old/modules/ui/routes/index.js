'use strict'

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import UiLayoutCA from '../layouts'

import MealPlanDetailsComponent from '../components/mealplan-details.js'
import MPCheckoutComponent from '../components/mp_checkout.js'
import MPFoodListComponent from '../components/mp_foodlist.js'
import MPOrderSummaryComponent from '../components/order-summary.js'
import ODSummaryComponent from '../components/order-summary-od.js'
import MPOrderConfirmComponent from '../components/order-confirm.js'
import ProfileComponentCA from '../components/profile.js'
import SetMealplanDetails from '../components/set-mealplan-details.js'

export default function () {
  return (
    <Route path='ui' component={UiLayoutCA}>
        <IndexRoute component={MealPlanDetailsComponent} />
        <oute path='checkout' component={MPCheckoutComponent} />
        <oute path='foodlist' component={MPFoodListComponent} />
        <oute path='order' component={MPOrderSummaryComponent} />
        <oute path='confirm' component={MPOrderConfirmComponent} />
        <oute path='profile' component={ProfileComponentCA} />
        <oute path='set-mealplan-details' component={SetMealplanDetails} />

        {/* <IndexRoute component={MealEditComponent} />
        <oute path='mealadd' component={MealAddComponent} />
        <oute path='mealtagadd' component={MealTagAddComponent} />
        <oute path='mealtaginline' component={MealTagInlineEditComponent} />
        <oute path='mealplan' component={MealPlanComponent} />
    <oute path='vanadd' component={VanAddComponent} /> */}
    </Route>
  )
}