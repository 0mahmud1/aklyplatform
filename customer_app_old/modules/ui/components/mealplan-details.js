'use strict'

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
//import SwitchButton from 'react-switch-button'
import CollapseComponent from './collapse/collapse.js'
//import Auth0Lock from 'auth0-lock';
import DatePicker from './datepicker'
import {getAllDaysOfNextMonth} from '../../libs'
import _ from 'lodash'
import moment from 'moment'
import Switch from 'rc-switch'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import ModalComponent from './modal/modal.js'

class MealPlanDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.props.setPlanType(localStorage.getItem('planType') ? localStorage.getItem('planType') : 'lunch');
    this.modalShow = this.modalShow.bind(this);
    this.modalHide = this.modalHide.bind(this);
    this.state = {collapsedState: {},visible: false,mealPlanForHome : localStorage.getItem('planType') === 'fullDay' ? true : false};
  }

  modalShow() {
    this.setState({visible: true});
  }

  modalHide() {
    this.setState({visible: false});
  }

  changeCollapseState(dd) {
    let newState = Object.assign({}, this.state.collapsedState);
    if(_.isUndefined(newState[dd])) {
      newState[dd] = true
    } else {
      newState[dd] = !newState[dd]
    }
    this.setState({
      collapsedState: newState
    })
  }
  generateEachdayRow() {
    const selectedWeek = this.props.mealPlan.selectedWeek;
    const days = this.props.mealPlan.allDays[selectedWeek];

    return (
      days && days.length > 0 ?
        days.map((day) => {
          let collapseState = false;
          let changeCollapseClass = this.state.collapsedState[day.format('YYYY-MM-DD')]? 'icon_collapsed': 'icon_not_collapsed';
          return   <li key={day.format('dddd')} className='cadp_singleday' onClick={() =>this.changeCollapseState(day.format('YYYY-MM-DD'))}> {/*single day component*/}
            <div className='abovefold_content'>
              <div className={'collapse_indicator ' + changeCollapseClass}>

              </div>
              <div className='day_name'>
                <p>{day.format('dddd Do MMM \'YY')}</p>
              </div>
              {/*<div className='cal_stat'>
                <div className='cal_summary'>
                  <div className='cal_icon'></div>
                  <div className='cal_text'>
                    <p>400 Kcal</p>
                  </div>
                </div>
                <div className='stat_protein'>
                  <p>Protein 50g</p>
                </div>
                <div className='stat_carbs'>
                  <p>Carbs 20g</p>
                </div>
                <div className='stat_fats'>
                  <p>Fats 30g</p>
                </div>
              </div>*/}
            </div>

            <CollapseComponent collapsed={this.state.collapsedState[day.format('YYYY-MM-DD')]} deliveryDate={day} cadpLunchClass={this.state.mealPlanForHome ? "" : 'is-office'} {...this.props} />
          </li>
        }) : null
    )
  }

  selectedWeekStyle(value) {
    return this.props.mealPlan.selectedWeek == value ? 'weekbox-slot-clicked' : '';
  }

  switchOnChange(value) {
    console.log('switchOnChange..',value);
    if(value) {
      this.props.setPlanType('fullDay');
    } else {
      this.props.setPlanType('lunch');
    }
    this.setState({
      mealPlanForHome : value
    });
    let allDays = getAllDaysOfNextMonth(this.props.mealPlan.startDate,value);
    this.props.saveAllDays(allDays);
    this.props.getMealPlans(value ? 'fullDay' : 'lunch');
  }

  goToCheckoutPage() {
    let originalDeliveries = this.props.mealPlan.deliveries;
    if(localStorage.getItem('planType') === 'lunch') {
      let filteredDeliveries = _.remove(originalDeliveries, function (o) {
        return ( o.shift === 'Dinner' || o.shift === 'Breakfast' || o.deliveryDate.format('dddd') === 'Saturday')
      })
    }
    localStorage.setItem('deliveries', JSON.stringify(originalDeliveries));
    browserHistory.push('/summary');
  }

  render() {
    console.log('mealplan-details...render...',this.props , "time =>>",moment().format())
    const collapsed = this.state.clicked
    return (
      <div>
        <div className='caadditionalbar'>
         <div className='caadditionalbar-content container ca-container'>
            <div className='campdetails_bradcrumb'>
            <ol className="breadcrumb cabreadcrumb">
              <li className="active"><a href="#">Meal Plan</a></li>
              {/*<li><a href="#">Katagonic Diet Plan</a></li>*/}
            </ol>
          </div>
          <div className='caadditionalbar-switch'>
              <div className='custom-switch'>
                  <div className='switch-withtext'>
                    <span className='switch-text'>Lunch Only</span>
                      <Switch onChange={this.switchOnChange.bind(this)} checked={localStorage.getItem('planType') === 'fullDay' ? true : false}/>
                    <span className='switch-text'>Full Day</span>
                  </div>
                  {this.state.mealPlanForHome ?
                    <p className='switched-text'>
                      For Home, you will be able to select breakfast, lunch & dinner for 6 days
                    </p>
                    :
                    <p className='switched-text'>
                      For office, you will be able to select lunch for 5 days
                    </p>
                  }
              </div>
            </div>

          <div className='campdetails_chckout'>
            <button className='btn butn_chkout_v2' onClick={this.goToCheckoutPage.bind(this)}>Checkout</button>
          </div>
         </div>
        </div>
        <div className='camp_details container ca-container'>
          <div className='row'>
            <div className='col-lg-3'>
              <div className="row">
                <DatePicker
                  onChange={(date) => this.props.saveAllDays(getAllDaysOfNextMonth(moment(date.format('YYYY-MM-DD')),this.state.mealPlanForHome))}
                />
              </div>
              <div className="row">
                <div className='weekly_sec'>
                  <div className='ca_setmpselect'>
                    <a href='#' className='setmpselect_btn' onClick={this.modalShow}>
                      <img src='/images/list.svg' alt='choose meal plan' />
                      <span>Choose a set meal plan</span>
                    </a>
                  </div>
                  <div className='ca_weeknav'>
                    <div className='ca_weekbox'>
                      <div className='weekbox_header'>
                        <div className='weekbox_icon'></div>
                        <div className='weekbox_heading'>
                          <img src='/images/date.svg' />
                          <span>Weekly Schedule</span>
                        </div>
                      </div>
                      <div className='weekbox_slots'>
                        <div className={`weekbox_slot weekbox_slot1 ${this.selectedWeekStyle(0)}`}  onClick={() => this.props.selectWeek(0)}>
                          <p>Week 1</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot2 ${this.selectedWeekStyle(1)}`} onClick={() => this.props.selectWeek(1)}>
                          <p>Week 2</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot3 ${this.selectedWeekStyle(2)}`} onClick={() => this.props.selectWeek(2)}>
                          <p>Week 3</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot4 ${this.selectedWeekStyle(3)}`} onClick={() => this.props.selectWeek(3)}>
                          <p>Week 4</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div className='col-lg-9'>
              <div className='week_breakdown'>
                {/*<div className='cadp_header'>
                  <div className='cadp_heading'>
                    <h3>Katogenic Diet Plan</h3>
                  </div>
                  <div className='cadp_button'>
                    <button className='btn butn butn_default'>Edit</button>
                  </div>
                </div>*/}

                <div className='cadp_choice row'>
                  <div className='cadp_weeknumb col-lg-10'>
                    <h4>Week {this.props.mealPlan.selectedWeek+1}</h4>
                  </div>
                  <div className='cadp_weeknumb col-lg-2' >
                    <button style={{float:'right'}} className='btn butn butn_danger' onClick={() => {this.props.clearAllMealPlan()}}>Clear</button>
                  </div>

                  {/*<div className='cadp_switch'>
                    <SwitchButton name="switch-5" label="Lunch Only" labelRight="Lunch & Dinner" defaultChecked={true}/>
                  </div>*/}
                </div>

                <div className='cadp_daystat'>
                  <ul className='cadp_days'>
                    <ReactCSSTransitionGroup
                    transitionName="common"
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={200}>
                    {this.generateEachdayRow()}
                  </ReactCSSTransitionGroup>

                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>
        <ModalComponent {...this.props} visibilityState={this.state.visible} onCloseEvent={this.modalHide}
                        modalHeight={550} modalWidth={410}/>
      </div>
    )
  }
}

export default MealPlanDetailsComponent
