import React, {Component} from 'react';

export default class NoContent extends Component {
    render(){
        return(
            <div className='nocontent'>
                <p className='nocontent-text'>{this.props.text}</p>
            </div>
        );
    }
}