/**
 * Created by Sajid on 2/1/17.
 */
import React, {Component, PropTypes} from 'react'
//import ReactDOM from 'react-dom'
import toastr from 'toastr'
class SettingsComponent extends Component{
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault()
        let _this = this
        if(_this.refs.newPassword.value !== _this.refs.confirmNewPassword.value ) {
            //if new password and confirm password does not match, warning show
            toastr.warning('New password and Confirm Password  does not match')
        } else {
            //toastr.success("same password")
            //new password and confirm password match, now check old password is correct or not
            let data = {
                email : _this.props.email,
                password : _this.refs.oldPassword.value,
                newPassword : _this.refs.newPassword.value
            }
            // console.log("change password values ",JSON.stringify(data))
            this.props.changePassword(data)

        }
    }

    render(){
        return (
            <div className='container ca-container'>
                <div style={{paddingTop: '4rem'}}>
                    <form onSubmit={this.handleSubmit}>
                        <div className='settings_input'>
                            <div className="form-group no_mb">
                                <input id="oldPassword" ref="oldPassword" name="oldPassword" type="password" placeholder="Old Password" className="form-control" required=""/>
                            </div>
                        </div>

                        <div className='settings_input'>
                            <div className="form-group no_mb">
                                <input id="newPassword" ref="newPassword" name="newPassword" type="password" placeholder="New Password" className="form-control" required=""/>
                            </div>
                        </div>

                        <div className='settings_input'>
                            <div className="form-group no_mb">
                                <input id="confirmNewPassword" ref="confirmNewPassword" name="confirmNewPassword" type="password" placeholder="Confirm New Password" className="form-control input-md" required=""/>
                            </div>
                        </div>

                        <div className='settings_btn text-center'>
                            <button id="changePassword" name="changePassword" className="btn butn butn_default" style={{fontSize: '16px'}}>Change Password</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default SettingsComponent
