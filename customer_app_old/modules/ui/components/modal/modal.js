'use strict'

import React, {Component, PropTypes} from 'react'
import Rodal from 'rodal'
import toastr from 'toastr'
import {Link} from 'react-router';


class ModalComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: this.props.visibilityState,
    };
  }

  addMealPlan(index) {
    console.log('addMealPlan~: ', index, this.props)
    //this.props.clearAllMealPlan();
    toastr.success('Diet plan selected');
    this.props.copyMealPlan(index);
    this.props.onCloseEvent();
  }

  // generateMealPlanList() {
  //   let index = 0;
  //   return this.props.dbMealPlans.mealPlanList.map((mp) => {
  //     mp.index = index++
  //     return (
  //       <li
  //         className='prompt-sdp-sitem'
  //         key={mp._id}
  //         onClick={this.addMealPlan.bind(this, mp.index)}>
  //         <div className="media prompt-sdp-media">
  //           <div className="media-left prompt-sdp-body">
  //             <div className='prompt-sdp-img'>
  //               <img className="media-object" src="/images/healthy-living-web.gif" alt="food image"/>
  //             </div>
  //           </div>
  //           <div className="media-body prompt-sdp-body">
  //             <h4 className="media-heading prompt-sdp-pname">{mp.name}</h4>
  //           </div>
  //         </div>
  //       </li>
  //     );
  //   })
  // }

  generateMealPlanList() {
    let index = 0;
    console.log('mealPlanList modal', this.props.dbMealPlans.mealPlanList);
    console.log('planType ...',localStorage.getItem('planType'));
    return this.props.dbMealPlans.mealPlanList.map((mp) => {
      mp.index = index++

      return (
        <div
          className='col-md-3 col-lg-3 prompt-sdp-sitem'
          key={mp._id}
          onClick={this.addMealPlan.bind(this, mp.index)} >
            <div className='prompt-sdp-mpimage'>
                <img src={mp.imageUrl} alt="mealplan image"/>
                <Link to={'/mealplans/' + mp._id} className="mpinfo">
                  <img src="/images/food-card/info.svg" alt="Mealplan information" />
                </Link>
                <div className='prompt-sdp-mpcal'>
                  <img src='/images/flame_white.svg' alt='calorie image' />
                  <p style={{fontSize: '18px', fontWeight: 700}}>{mp.calorie}</p>
                  <p>Kcal/week</p>
                </div>
            </div>
            <p className='dpprice'>QR {mp.setPrice}</p>
            <div className='prompt-sdp-info'>
              <h4 className='dpname'>{mp.name}</h4>
            </div>
        </div>
      );
    })
  }

  render() {
    let shouldLoad = this.props.dbMealPlans.mealPlanList ? true : false;
    console.log('modalVisible: ', this.state.visible);
    return (
      shouldLoad ?
      <Rodal visible={this.props.visibilityState} onClose={this.props.onCloseEvent} height={this.props.modalHeight}
             width={this.props.modalWidth} className="ca_modal">
        <div className='prompt-sdp'>
          <div className='prompt-sdp-header'>
            <h4>Select Your Plan</h4>
          </div>
          {/*<div className='prompt-sdp-add'>
            <button className='btn butn-sdp-add'>+ Add new diet plan</button>
          </div>*/}
          <div className='row'>
            {this.generateMealPlanList()}
          </div>
        </div>
      </Rodal>
      : null
    )
  }
}

export default ModalComponent
