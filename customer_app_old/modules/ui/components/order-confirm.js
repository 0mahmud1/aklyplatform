'use strict'

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'

import PackIconComponent from './icon/pack.js'
import PickupIconComponent from './icon/pickup.js'
import TickIconComponent from './icon/tick.js'
import CollapseComponent from './collapse/collapse.js'

class MPOrderConfirmComponent extends Component {
    constructor(props) {
      super(props);
      this.state = {clicked: false};
    }

	show() {
	  this.setState({clicked: !this.state.clicked});
	}

	componentDidMount() {
        $(".js-example-select").select2({
            minimumResultsForSearch: -1
        });
	}

	render() {
        const collapsed = this.state.clicked ? true : false;
		return (
			<div>
  <div className='caadditionalbar mpcheckoutbar'>
         <div className='caadditionalbar-content mpcheckoutbar-content container ca-container'>
             <div className='ca_steprogress'>
            <div className='steprogress detailed_view'>
              <div className='progress_section'>
                <div className='stprogress'>
                  <div className='progressbar_section'>
                    <div className='stprogressbar'>
                      <div className='horizontal_box'>
                        <div className='base_bar'></div>
                        <div className='base_bar'></div>
                      </div>
                    </div>
                  </div>

                  <div className='progressbar_section'>
                    <div className='horizontal_box progress_status_section'>
                      <div className='steps step1'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <PackIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>

                      <div className='steps step2'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <PickupIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>

                      <div className='steps step3'>
                        <div className='stepsicons'>
                          <div className='vertical_box'>
                            <ol className='nostyle vertical_box'>
                              <li className='step_progress_icon'>
                                <div className='icon_wrapper'>
                                  <TickIconComponent fillColor='#A7A7A7'/>
                                </div>
                              </li>
                            </ol>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>

				<div className='mp-ocnf'>
				    <h1 className='ocnf-heading'>Your Order Successfully Placed</h1>
				    <div className='onumb'>
				        <p>Order Number <span>{this.props.orders.order.orderRefId}</span></p>
				    </div>
                    {/*<p className='dmsg'>{this.props.auth.userProfile.name}, your order has been successfully placed, We will deliver your food between 11am and 2pm. </p>*/}
				    <p className='dmsg'>Your order has been successfully placed, We will deliver your food <span style={{color: '#f77316'}}> between 11am and
              2pm.</span> </p>
				    <p className='qnumb'>If you have any questions, please call us at <span style={{color: '#f77316'}}> +9933 4422 </span>  </p>
				    <a href='#' className='btn butn-ocnf'>
				        Go to Menu
				    </a>
				</div>
			</div>
		)
	}
}

export default MPOrderConfirmComponent