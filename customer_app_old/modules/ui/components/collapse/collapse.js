'use strict'

import React, {Component, PropTypes} from 'react'
import Collapse from 'react-collapse'
import CadpCollapsedComponent from '../mealplan/cadp_collapsed'

class CollapseComponent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Collapse isOpened={this.props.collapsed}>
        <CadpCollapsedComponent {...this.props}/>
      </Collapse>
    )
  }
}

export default CollapseComponent
