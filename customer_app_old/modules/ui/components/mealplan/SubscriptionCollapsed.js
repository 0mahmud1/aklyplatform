'use strict'

import React, {Component, PropTypes} from 'react'
import _ from 'lodash'
import toastr from 'toastr'

class SubscriptionCollapsedComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      temp: 0
    }
  }

  addFoodButtonClicked(deliveryDate,shift) {
    console.log('pop up food menu should be for ',deliveryDate,' ', shift)
    this.props.invokeFoodPopUp(deliveryDate,shift);
  }

  getDeliveryRefId(shift) {
    let _this = this;
    let selfDelivery = this.props.mealPlan.deliveries.find((o) => {
      return o.deliveryDate.format('YYYY-MM-DD') == _this.props.deliveryDate.format('YYYY-MM-DD') && o.shift == shift
    })

    //return _.sumBy(selfDelivery.meals,x => x.quantity)
    return selfDelivery ? selfDelivery.deliveryRefId : null
  }

  totalOrderAccordingToDateAndShift(shift) {
    let _this = this;
    let selfDelivery = this.props.mealPlan.deliveries.find((o) => {
      return o.deliveryDate.format('YYYY-MM-DD') == _this.props.deliveryDate.format('YYYY-MM-DD') && o.shift == shift
    })

    //return _.sumBy(selfDelivery.meals,x => x.quantity)
    return selfDelivery ? _.sumBy(selfDelivery.meals,x => x.quantity): 0
  }

  generateItemsAccordingDayAndShift(shift) {
    let _this = this;
    let selfDelivery = this.props.mealPlan.deliveries.find((o) => {
      return o.deliveryDate.format('YYYY-MM-DD') == _this.props.deliveryDate.format('YYYY-MM-DD') && o.shift.toLowerCase() == shift.toLowerCase()
    })

    console.log('my self delivery meals ', selfDelivery, ' for shift ', shift)
    return selfDelivery && selfDelivery.meals ?
      selfDelivery.meals.map((meal) => {
      return <div key={meal.item._id} className='cadp_mitem'>
        <div className="media">
          <div className="media-left">
            <a href="#" className='cadp_mimage'>
              <img className="media-object" src={meal.item.images[1]} alt="spicy chicken"/>
            </a>
          </div>
          <div className="media-body">
            <div className='cadp_mname'>
              <p>{meal.item.name} <span>({meal.quantity})</span></p>
            </div>
            <div className='cadp_mstat'>
              <div className='cadp_mstat_cal'>
                <div className='cadp_mstat_icon'></div>
                <div className='cadp_mstat_text'>
                  <p>{meal.item.calorie} Kcal</p>
                </div>
              </div>
              <div className='cadp_mstat_protein'>
                <p>Protein {meal.item.protein}g</p>
              </div>
              <div className='cadp_mstat_carbs'>
                <p>Carbs {meal.item.carbs}g</p>
              </div>
            </div>
            <div className='cadp_mstat_price'>
              <p>QR {meal.item.price}</p>
            </div>
          </div>
          <div className="media-right">
            <div className='cadp_mitem_remove'>
              <span className='glyphicon glyphicon-remove' onClick={this.removeMealFromDelivery.bind(this,selfDelivery, meal)}></span>
            </div>
          </div>
        </div>
      </div>
    }) : null;
  }

  removeMealFromDelivery(delivery, meal) {
    //console.log('clicked',delivery);
    if(delivery.meals.length === 1) {
      toastr.error('all meals can not be deleted');
      return;
    } else {
      this.props.removeMealFromDelivery(delivery,meal);
    }

  }

  render() {
    console.log('SubscriptionCollapsedComponent ----',this.props);

    let isOffice = this.props.cadpLunchClass;

    return (
        <div className={'cadp_collapsed ' + isOffice}>
          <div className='cadp_breakfast'>
            <div className='cadp_mheader'>
              <div className='cadp_mheading'>
                <h4>Breakfast</h4>
              </div>
              <div className='cadp_madd' style={{cursor:'pointer'}}>
                  <img src='/images/add.svg' alt='Add food' onClick={this.addFoodButtonClicked.bind(this,this.props.deliveryDate,'Breakfast')}/>
              </div>
              <div className='cadp_mnumb'>
                <p>{this.totalOrderAccordingToDateAndShift('Breakfast')}</p>
              </div>
            </div>
            <p className='delivery-id'>Delivery Ref Id: {this.getDeliveryRefId('Breakfast')}</p>
            <div className='cadp_mbreakdown'>
              {this.generateItemsAccordingDayAndShift('Breakfast')}
            </div>
          </div>
          <div className='cadp_lunch'>
            <div className='cadp_mheader pb-10imp'>
              <div className='cadp_mheading'>
                <h4>Lunch</h4>
              </div>
              <div className='cadp_madd' style={{cursor:'pointer'}}>
                <img src='/images/add.svg' alt='Add food' onClick={this.addFoodButtonClicked.bind(this,this.props.deliveryDate,'Lunch')}/>
              </div>
              <div className='cadp_mnumb'>
                <p>{this.totalOrderAccordingToDateAndShift('Lunch')}</p>
              </div>
            </div>
            <p className='delivery-id'>Delivery Ref Id: {this.getDeliveryRefId('Lunch')}</p>
            <div className='cadp_mbreakdown'>
              {this.generateItemsAccordingDayAndShift('Lunch')}
            </div>
          </div>
          <div className='cadp_dinner'>
            <div className='cadp_mheader'>
              <div className='cadp_mheading'>
                <h4>Dinner</h4>
              </div>
              <div className='cadp_madd' style={{cursor:'pointer'}}>
                  <img src='/images/add.svg' alt='Add food' onClick={this.addFoodButtonClicked.bind(this,this.props.deliveryDate,'Dinner')}/>
              </div>
              <div className='cadp_mnumb'>
                <p>{this.totalOrderAccordingToDateAndShift('Dinner')}</p>
              </div>
            </div>
            <p className='delivery-id'>Delivery Ref Id: {this.getDeliveryRefId('Dinner')}</p>
            <div className='cadp_mbreakdown'>
              {this.generateItemsAccordingDayAndShift('Dinner')}
            </div>
          </div>
        </div>
    )
  }
}

export default SubscriptionCollapsedComponent
