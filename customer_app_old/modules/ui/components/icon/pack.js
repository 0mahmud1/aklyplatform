'use strict'

import React, {Component, PropTypes} from 'react'

class PackIconComponent extends Component {
	render() {
		return (
			<div className='pack_icon'>
				<svg viewBox="0 0 42 34" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" onClick={this.props.clickPackIcon}><g fill={this.props.fillColor} fillRule="evenodd" preserveAspectRatio="xMidYMid meet" onClick={this.props.clickPackIcon}><path d="M26.188 4.327L21 1.297c-1.606-.926-3.644-.926-5.312 0l-5.064 2.907 7.905 4.574 7.658-4.45zM8.4 5.564L2.718 8.84C1.112 9.767.062 11.56.062 13.415v5.996c0 1.917.988 3.648 2.656 4.575l4.94 2.844v-9.52c0-1.3.68-2.535 1.854-3.154l6.794-4.018L8.4 5.564zm12.353 11.25l2.656-2.658-4.757-2.78-6.856 4.08c-1.112.68-1.853 1.853-1.853 3.15v9.46l5.806 3.338c1.606.927 3.644.927 5.312 0l5.497-3.153v-.308l-5.745-5.75-.062-5.377zm13.28-7.974l-5.498-3.215-7.597 4.513 4.756 2.782h4.447l6.548 6.553v-7.17c-.062-1.918-1.05-2.536-2.656-3.463z"/><path d="M31.253 34L19.826 22.625v-6.12l4.51-4.512h6.114L41.876 23.43 31.253 34zM21.68 21.822l9.573 9.582 8.03-8.037-9.574-9.582h-4.634l-3.397 3.4v4.637z"/><ellipse cx="26.868" cy="18.978" rx="1.853" ry="1.855"/></g></svg>
			</div>
		)
	}
}

export default PackIconComponent