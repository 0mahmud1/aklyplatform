'use strict'

import React, { Component, PropTypes } from 'react'
import { Link, browserHistory } from 'react-router'
import toastr from 'toastr'
import shortid from 'shortid';
//import SwitchButton from 'react-switch-button'
import SubscriptionCollapseComponent from './collapse/SubscriptionCollapse'
//import Auth0Lock from 'auth0-lock';
import DatePicker from './datepicker'
import { getAllDaysOfNextMonth } from '../../libs'

import _ from 'lodash'
import moment from 'moment'

import ModalComponent from './modal/modal.js'

class SubscriptionEditDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.modalShow = this.modalShow.bind(this);
    this.modalHide = this.modalHide.bind(this);
    this.state = {collapsedState: {},visible: false};
  }

  modalShow() {
    this.setState({visible: true});
  }

  modalHide() {
    this.setState({visible: false});
  }

  changeCollapseState(dd) {
    let newState = Object.assign({}, this.state.collapsedState);
    if(_.isUndefined(newState[dd])) {
      newState[dd] = true
    } else {
      newState[dd] = !newState[dd]
    }
    this.setState({
      collapsedState: newState
    });
  }
  generateEachdayRow() {
    const selectedWeek = this.props.mealPlan.selectedWeek;
    const days = this.props.mealPlan.allDays[selectedWeek];
    const today = moment().add(2,'days').format('YYYY-MM-DD');
    const generalClass = 'cadp_singleday subscription-singleday cursor-autoimp';
    const noEditClass = 'cadp_singleday subscription-singleday cursor-autoimp no-edit-order';
    return (
      days && days.length > 0 ?
        days.map((day) => {
          let _this = this;
          let breakfastDelivery = this.props.mealPlan.deliveries.find((o) => {
            return o.deliveryDate.format('YYYY-MM-DD') == day.format('YYYY-MM-DD') && o.shift == 'Breakfast'
          });
          let lunchDelivery = this.props.mealPlan.deliveries.find((o) => {
            return o.deliveryDate.format('YYYY-MM-DD') == day.format('YYYY-MM-DD') && o.shift == 'Lunch'
          });
          let dinnerDelivery = this.props.mealPlan.deliveries.find((o) => {
            return o.deliveryDate.format('YYYY-MM-DD') == day.format('YYYY-MM-DD') && o.shift == 'Dinner'
          })
          let collapseState = false;
          let changeCollapseClass = this.state.collapsedState[day.format('YYYY-MM-DD')]? 'icon_collapsed': 'icon_not_collapsed';
          return <li key={day.format('dddd')} className={today < day.format('YYYY-MM-DD') ? generalClass : noEditClass}> {/*single day component*/}
            <div className='abovefold_content'>
              <div className={'collapse_indicator cursor-pointer ' + changeCollapseClass} onClick={() =>this.changeCollapseState(day.format('YYYY-MM-DD'))}>

              </div>
              <div className='day_name'>
                <p>{day.format('dddd Do MMM \'YY')}</p>
              </div>
              <div className={today < day.format('YYYY-MM-DD') ? 'cal_stat' : 'cal_stat hide'}>
                <div className='cal_summary'>
                  <div className='cal_text subscription-icon-circle subscription-icon-ok' onClick={this.updateSingleDayMealPlan.bind(this,breakfastDelivery,lunchDelivery,dinnerDelivery)}>
                    <span className='glyphicon glyphicon-ok subscription-icon'></span>
                  </div>
                </div>
                <div className='stat_protein subscription-icon-circle subscription-icon-remove' onClick={this.deleteSingleDayMealPlan.bind(this,breakfastDelivery,lunchDelivery,dinnerDelivery)}>
                  <span className='glyphicon glyphicon-remove subscription-icon' ></span>
                </div>
              </div>

              {/* <div className='cal_stat'>
                <div className='cal_summary'>
                  <div className='cal_icon'></div>
                  <div className='cal_text'>
                    <p>400 Kcal</p>
                  </div>
                </div>
                <div className='stat_protein'>
                  <p>Protein 50g</p>
                </div>
                <div className='stat_carbs'>
                  <p>Carbs 20g</p>
                </div>
                <div className='stat_fats'>
                  <p>Fats 30g</p>
                </div>
              </div> */}
            </div>

            <SubscriptionCollapseComponent collapsed={this.state.collapsedState[day.format('YYYY-MM-DD')]} deliveryDate={day} cadpLunchClass={this.props.order && this.props.order.planType === 'fullDay' ? "" : 'is-office'} {...this.props} />
          </li>
        }) : null
    )
  }

  selectedWeekStyle(value) {
    return this.props.mealPlan.selectedWeek == value ? 'weekbox-slot-clicked' : '';
  }

  updateSingleDayMealPlan(breakfastDelivery,lunchDelivery,dinnerDelivery) {
    const _this = this;
    console.log('updateSingleDayMealPlan>>>>',breakfastDelivery,'-----',lunchDelivery,'-----',dinnerDelivery);
    try {
      if(breakfastDelivery && !breakfastDelivery._id ) {
        _this.createNewDelivery(breakfastDelivery);
      } else if(breakfastDelivery !== undefined){
        breakfastDelivery.load = breakfastDelivery.meals.length;
        breakfastDelivery.quantity = breakfastDelivery.meals.length;
        breakfastDelivery.subTotal = breakfastDelivery.total;
        this.props.updateDelivery(breakfastDelivery._id,breakfastDelivery,(err,res) => {
          if(err) console.error(err);
          this.props.getDeliveriesByOrderRefId(this.props.params.orderRefId);
        });
      }
      if(lunchDelivery && !lunchDelivery._id ) {
        _this.createNewDelivery(lunchDelivery);
      } else if(lunchDelivery !== undefined){
        lunchDelivery.load = lunchDelivery.meals.length;
        lunchDelivery.quantity = lunchDelivery.meals.length;
        lunchDelivery.subTotal = lunchDelivery.total;
        this.props.updateDelivery(lunchDelivery._id,lunchDelivery,(err,res) => {
          if(err) console.error(err);
          this.props.getDeliveriesByOrderRefId(this.props.params.orderRefId);
        });
      }
      if(dinnerDelivery && !dinnerDelivery._id ) {
        _this.createNewDelivery(dinnerDelivery);
      } else if(dinnerDelivery !== undefined) {
        dinnerDelivery.load = dinnerDelivery.meals.length;
        dinnerDelivery.quantity = dinnerDelivery.meals.length;
        dinnerDelivery.subTotal = dinnerDelivery.total;
        this.props.updateDelivery(dinnerDelivery._id,dinnerDelivery,(err,res) => {
          if(err) console.error(err);
          this.props.getDeliveriesByOrderRefId(this.props.params.orderRefId);
        });
      }
      toastr.success('Mealplan updated');
    } catch(error){
      toastr.error(error);
    }

  }

  deleteSingleDayMealPlan(breakfastDelivery,lunchDelivery,dinnerDelivery) {
    try {
      this.props.deleteDelivery(breakfastDelivery._id, (err,res) => {
        if(err) console.error(err);
      });
      this.props.deleteDelivery(lunchDelivery._id, (err,res) => {
        if(err) console.error(err);
      });
      this.props.deleteDelivery(dinnerDelivery._id, (err,res) => {
        if(err) console.error(err);
      });
      toastr.success('Mealplan deleted');
    } catch(error){
      toastr.error(error);
    }
  }

  createNewDelivery(newDelivery) {
    const order = this.props.order;
    // const mealsLength = newDelivery.meals.length;
    const delivery = {};
    delivery.orderRefId = order.orderRefId;
    delivery.deliveryDate = moment(newDelivery.deliveryDate).format('YYYY-MM-DD');
    // delivery.planType = order.planType;
    // delivery.deliveryBoy = {};
    delivery.shift = newDelivery.shift;
    delivery.meals = newDelivery.meals;
    // delivery.deliveryRefId = `akly-D-${shortid.generate()}`;
    this.props.createDelivery(delivery, (err,res) => {
      if(err) console.error(err);
      console.log('createDelivery >>>',res);
      this.props.getDeliveriesByOrderRefId(this.props.params.orderRefId);
    });
  }

  render() {
    console.log('SubscriptionEditDetailsComponent...render...',this.props);
    const collapsed = this.state.clicked
    return (
      <div>
        <div className='caadditionalbar'>
         <div className='caadditionalbar-content container ca-container'>
            <div className='campdetails_bradcrumb'>
            <ol className="breadcrumb cabreadcrumb">
              <li className="active"><a href="#">Meal Plan</a></li>
              {/*<li><a href="#">Katagonic Diet Plan</a></li>*/}
            </ol>
          </div>
          <div className='caadditionalbar-switch'>
                <p className='switched-text'>Order Ref Id: {this.props.params.orderRefId} </p>
            </div>
         </div>
        </div>
        <div className='camp_details container ca-container subscription-mp'>
          <div className='row'>
            <div className='col-lg-3'>
              <div className="row">
                <div className='weekly_sec'>
                  <div className='ca_weeknav'>
                    <div className='ca_weekbox'>
                      <div className='weekbox_header'>
                        <div className='weekbox_icon'></div>
                        <div className='weekbox_heading'>
                          <img src='/images/date.svg' />
                          <span>Weekly Schedule</span>
                        </div>
                      </div>
                      <div className='weekbox_slots'>
                        <div className={`weekbox_slot weekbox_slot1 ${this.selectedWeekStyle(0)}`}  onClick={() => this.props.selectWeek(0)}>
                          <p>Week 1</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot2 ${this.selectedWeekStyle(1)}`} onClick={() => this.props.selectWeek(1)}>
                          <p>Week 2</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot3 ${this.selectedWeekStyle(2)}`} onClick={() => this.props.selectWeek(2)}>
                          <p>Week 3</p>
                        </div>
                        <div className={`weekbox_slot weekbox_slot4 ${this.selectedWeekStyle(3)}`} onClick={() => this.props.selectWeek(3)}>
                          <p>Week 4</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className='weekly_sec'>
                  <div className='ca_weeknav'>
                    <div className='ca_weekbox'>
                      <div className='weekbox_header mb-15'>
                        <div className='weekbox_icon'></div>
                        <div className='weekbox_heading'>
                          <img src='/images/location.svg' />
                          <span>Delivery address</span>
                        </div>
                      </div>
                      <div>
                        <div className='mb-15'>
                          <p className='fw-700 fs-14 bold-color'>Phone</p>
                          <p className='fs-14'>{this.props.mealPlan.deliveries[0] && this.props.mealPlan.deliveries[0].address ? this.props.mealPlan.deliveries[0].address.phone : ''}</p>
                        </div>

                        <div>
                          <p className='fw-700 fs-14 bold-color'>Your location</p>
                          <p className='fs-14'>{this.props.mealPlan.deliveries[0] && this.props.mealPlan.deliveries[0].address ? this.props.mealPlan.deliveries[0].address.street : ''}</p>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div className='col-lg-9'>
              <div className='week_breakdown'>
                {/*<div className='cadp_header'>
                  <div className='cadp_heading'>
                    <h3>Katogenic Diet Plan</h3>
                  </div>
                  <div className='cadp_button'>
                    <button className='btn butn butn_default'>Edit</button>
                  </div>
                </div>*/}

                <p style={{marginBottom: '15px', color: '#ed680c', textAlign: 'center'}}>** You can not edit a particular order until 2 days have elapsed.</p>
                <div className='cadp_choice row'>
                  <div className='cadp_weeknumb col-lg-12'>
                    <h4>Week {this.props.mealPlan.selectedWeek+1}</h4>
                    <p className='subscription-type'>Order type: {this.props.order && this.props.order.planType === 'lunch' ? 'Lunch' : 'Full Day'}</p>
                  </div>

                  {/*<div className='cadp_switch'>
                    <SwitchButton name="switch-5" label="Lunch Only" labelRight="Lunch & Dinner" defaultChecked={true}/>
                  </div>*/}
                </div>

                <div className='cadp_daystat'>
                  <ul className='cadp_days'>
                    {this.generateEachdayRow()}
                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>
        <ModalComponent {...this.props} visibilityState={this.state.visible} onCloseEvent={this.modalHide}
                        modalHeight={550} modalWidth={410}/>
      </div>
    )
  }
}

export default SubscriptionEditDetailsComponent
