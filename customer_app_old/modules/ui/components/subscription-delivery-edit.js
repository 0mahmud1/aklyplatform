'use strict'

import React, {Component, PropTypes} from 'react'
import DatePicker from './datepicker'
import {Link} from 'react-router'
import _ from 'lodash'
import moment from 'moment'
import Rodal from 'rodal'

class SubscriptionEditDelivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
      rodalVisible: false
    };
  }

  rodalShow() {
    this.setState({ rodalVisible: true });
  }

  rodalHide() {
    this.setState({ rodalVisible: false });
  }

  show() {
    this.setState({
      clicked: !this.state.clicked
    });
  }

  componentDidMount() {
    console.log('cmdm', this.props.foods);
    if (!this.props.foods.foodTags) {
      this.props.createFoodTags();
    }
    this.props.selectDeliveryDate(moment().format('YYYY-MM-DD'))
  }

  componentWillUnmount() {
    this.props.resetSelectedDelivery()
  }


  addFoodToTemp(food) {
    console.log(food.name)
    this.props.addFoodTemp(food);
  }

//fcard-mod
  generateFoodList() {
    let _this = this;
    return this.props.foods.filteredFoodList.map((food) => {
      let existingFood = null
      if(_this.props.subscriptionEdit.selectedDelivery && _this.props.subscriptionEdit.selectedDelivery.meals) {
        existingFood = _.find(_this.props.subscriptionEdit.selectedDelivery.meals, function (f) {
          return f.item._id == food._id
        })
        //console.log('** ', existingFood)
      }

      return <div key={food._id} className='col-lg-3 fcard-mbottom menu-shadow'>
        <div className={`fcard ${existingFood ? 'fcard-mod': ''}`} >
          <button className='fadd' onClick={this.addFoodToTemp.bind(this, food)}>
            <img src='/images/plus.svg' alt="add food"/>
          </button>
          <div className='fctrl'>
            <button className='fdecre' onClick={() => this.props.reduceFoodTemp(food)}>
              <img src="/images/minus.svg" alt="decrement food" />
            </button>
            <button className='fincre' onClick={this.addFoodToTemp.bind(this, food)}>
              <img src="/images/plus-orange.svg" alt="decrement food"/>
              <span>Add</span>
            </button>
          </div>
          <div className='fnumb'>
            <p>{`${existingFood ? existingFood.quantity: ''}`}</p>
            <p><img src='/images/bag.svg' alt='food selectd'/>In your aklybox</p>
          </div>
          <img src={food.images[0]} alt="food image" className='fimg'/>

          <div className='fdtl'>
            <div className='fname'>
              <p>{food.name}</p>
            </div>
            <div className='fcal'>
              <img src='/images/bonfire.svg' alt='food calorie'/>
              <span>700 Kcalories</span>
            </div>
            <div className='fprice'>
              <p><sup>QR</sup>{food.price}</p>
            </div>
          </div>

          <div className='fcard-overlay'>
          </div>

        </div>
      </div>
    })
  }

  checkBoxChange(event) {
    console.log('.., ', event.target.value, event.target.checked);
    if (event.target.checked) {
      this.props.addFilterTag(event.target.value)
      this.props.filterFood()
    } else {
      this.props.removeFilterTag(event.target.value)
      this.props.filterFood()
    }

  }

  getTagImage(tag) {
    if(!this.props.foods.filterTagList || this.props.foods.filterTagList.length == 0)
      return tag.images[0]

    return _.includes(this.props.foods.filterTagList.map(ft => ft._id) , tag._id) ? tag.images[0] : tag.images[1]
  }

  generateFoodTagList() {
    return this.props.foods.foodTags ?
      this.props.foods.foodTags.map((tag) => {
        return <li className='sitem' key={tag._id}>
          <div className='sitem-wrapper'>
            <div className='sitem-tag'>
              <img src={this.getTagImage(tag)} alt=""/>
              <span>{tag.name}</span>
            </div>

            <div className='sitem-checkbox'>
              <label>
                <div className="checkbox">
                  <input type="checkbox" value={tag._id} onChange={this.checkBoxChange.bind(this)}/>
                </div>
              </label>
            </div>
          </div>
        </li>
      }) : <li>No tag Found :(</li>

  }

  showDatePicker() {
    return this.props.subscriptionEdit.selectedDelivery && !this.props.subscriptionEdit.selectedDelivery._id ? // we are ensuring this selection is from database data
      <DatePicker
        onChange={(date) => this.props.selectDeliveryDate(date)}
      /> : null
  }

  handleSubmit() {
    let _this = this;
    let obj = Object.assign({},this.props.subscriptionEdit.selectedDelivery,{
      orderRefId: this.props.orders.order.orderRefId
    })
    //
    if(this.props.subscriptionEdit.selectedDelivery._id) {
      // edit delivery
      console.log('should update delivery', this.props.subscriptionEdit.selectedDelivery);
      this.props.updateSubscriptionDelivery(this.props.subscriptionEdit.selectedDelivery._id,this.props.subscriptionEdit.selectedDelivery, updatedData => {

      })
    } else {
      // add delivery
      console.log('should ADD delivery', obj);
      _this.props.createNewDelivery(obj)
      this.props.createNewDelivery(obj,data => {
        console.log('callback called')
      })
    }
  }

  render() {
    let stateClass= this.state.clicked? 'mp_foodlist_dropdown_show' : 'mp_foodlist_dropdown_hide';

    return (
      <div className='mp_foodlist'>
          <div className='mp_foodlist_header'>
            <div className="mp-foodlist-header-container container ca-container">
              <div className='food_list_qsort'>
                <div className='food_list_q'>
                  {/* {this.showDatePicker()} */}
                  <h4 style={{'marginBottom': '10px'}}>Orders from January 1, 2017 to january 7, 2017</h4>
                  <p style={{'color': '#5d5d5d'}}>Total: QR 600</p>
                </div>
              </div>
              <div className='food_list_appr'>
                <p style={{'color': '#5d5d5d'}}>Order Id: 1234567</p>
              </div>
            </div>
          </div>
        <div className='mp_foodlist_header'>
          <div className="mp-foodlist-header-container container ca-container">
            <div className='food_list_qsort'>
              <div className='food_list_q'>
                {this.showDatePicker()}
                <h4>Add lunch (3)</h4>
              </div>
              <div className='food_list_sorticon'>
                <button className='btn butn_filter' onClick={this.show.bind(this)}><img src='/images/filter.svg'
                                                                                        alt='filter food'/></button>
              </div>
              <div className={'ca-dropdown ' + stateClass}>
                <div className='dropdown_sorticon'>
                  <button className='btn butn_filter' onClick={this.show.bind(this)}><img src='/images/filter-white.svg'
                                                                                          alt='filter food'/></button>
                </div>
                <ul className='ca_dropdown_items'>
                  <form>
                    {this.generateFoodTagList()}
                  </form>
                </ul>
              </div>
            </div>
            <div className='food_list_appr'>
              <button className='btn butn_fc' onClick={() => this.props.resetSelectedDelivery()}><img src='/images/roundcross.svg' alt='cancel'
              /></button>
              <button className='btn butn_fo' onClick={this.rodalShow.bind(this)}><img src='/images/roundtick.svg' alt='ok'/></button>
            </div>
          </div>
        </div>

        <div className='flist'>
          <div className='flist-container container ca-container'>
            <div className='row'>
              {this.generateFoodList()}
            </div>
          </div>
        </div>
        {/*<div className='flist-backdrop'></div>*/}
        <Rodal visible={this.state.rodalVisible} onClose={this.rodalHide.bind(this)} height={160}>
          <div>
            <div className='binary_prompt_content'>
              <div className='prompt_text'>
                <p>Are you sure you want to confirm changes?</p>
              </div>
              <div className='prompt_btn'>
                <button className='btn butn butn_default' onClick={this.handleSubmit.bind(this)}  >
                  <span>yes</span>
                </button>
                <button className='btn butn butn_danger' onClick={this.rodalHide.bind(this)} >
                  <span>no</span>
                </button>
              </div>
            </div>
          </div>
        </Rodal>
      </div>
    )
  }
}

export default SubscriptionEditDelivery
