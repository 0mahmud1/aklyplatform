import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import toastr from 'toastr';


class SupportPageComponent extends Component {
  heading() {
    const headingTextStyle = {
      fontSize: '18px',
      color: '#f77316',
      textAlign: 'center'
    };

    return (
      <div className='mp_heading_mb'>
        <div className='row'>
          <div className='col-lg-12'>
            <div className='mp_heading mb-25' style={{paddingTop: '25px'}}>
              <h3 style={headingTextStyle}>Enter Your Query Here</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
  form() {
    return(
      <div className="mp_body">
        <div className="row">
          <div className="col-lg-12">
            <div className="meal_edit">
              <form>
                <div className='settings_input'>
                  <div className="form-group mb-25">
                    <input ref="name" type="text" placeholder="Name" className="form-control" />
                  </div>
                  <div className="form-group mb-25">
                    <input ref="email" type="email" placeholder="Email" className="form-control" required=""/>
                  </div>
                </div>
                <div className="geninput mb-25">
                  <div className="form-group">
                    <textarea className="form-control geninput_textareactrl textarea_descheight" style={{resize: 'none', height: '150px', width: '400px', margin: '0 auto'}} ref='comment' placeholder='Enter your comment here...'/>
                  </div>
                </div>
                <div className="user_create_actionbtn actionbutn">
                  <div className="row">
                    <div className="col-md-12 col-lg-12 text-center">
                      <button type="submit" onClick={this.handleSubmit.bind(this)} className="btn butn butn_default">Send</button>
                      <button type="button" className="btn butn butn_back" style={{marginLeft: '10px'}}>Back</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
  handleSubmit(event) {
    event.preventDefault();
    const _this = this;
    const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
    const email = ReactDOM.findDOMNode(this.refs.email).value.trim();
    const comment = ReactDOM.findDOMNode(this.refs.comment).value.trim();

    if (email.length < 1) {
      toastr.warning('Email Address is required.');
      return false;
    }

    const supportQuery = { name , email , comment };
    console.log('supportQuery >>>',supportQuery);
    this.props.sendSupportQuery(supportQuery,function(err,res){
      if(err) {
        toastr.error('Sorry! Email not sent! Try Again!')
      } else {
        // browserHistory.push('/');
        toastr.success("Email sent successfully!");
        ReactDOM.findDOMNode(_this.refs.name).value = '';
        ReactDOM.findDOMNode(_this.refs.email).value = '';
        ReactDOM.findDOMNode(_this.refs.comment).value = '';
      }

    });
  }
  render () {
    // console.log('SupportPageComponent >>>',this.props);
    return (
      <div>
        {this.heading()}
        {this.form()}
      </div>
    )
  }
}
export default SupportPageComponent;
