'use strict'

import React, {Component, PropTypes} from 'react'

// import navBarCAComponent from '../components/navbar.js'

class UiLayoutCA extends Component {
	render() {
		console.log('UiLayoutCA...render');
		return (
			<div>
				{/* <navBarCAComponent /> */}
				{this.props.children}
			</div>
		)
	}
}

UiLayoutCA.propTypes = {
	// This component gets the task to display through a React prop.
	// We can use propTypes to indicate it is required
	children: PropTypes.element.isRequired
}

export default UiLayoutCA