/*
* all these helper functions are for checking route authorization & log out purposes
* */
import { browserHistory } from 'react-router';
import toastr from 'toastr';

import store from '../../store';

/**
 * check user authentication from react router
 * @param nextState
 * @param replaceState
 * @returns void
 */
export function isLoggedIn(nextState, replaceState) {
	if (!store.getState().auth.user) {
		toastr.warning('Please login.');
		replaceState('/');
	}
}
