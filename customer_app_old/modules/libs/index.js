import moment from 'moment';
import _ from 'lodash';

export function getDateRanges(startDate, endDate, addFn, interval) {

  addFn = addFn || Date.prototype.addDays;
  interval = interval || 1;

  var retVal = [];
  var current = new Date(startDate);

  while (current <= endDate) {
    retVal.push(new Date(current));
    current = addFn.call(current, interval);
  }

  return retVal;
}

export function getAllIndexes(arr, val) {
  let indexes = [];
  let i;
  for (i = 0; i < arr.length; i++)
    if (arr[i].format('dddd') === val)
      indexes.push(i);
  return indexes;
}

export function getAllDaysOfNextMonth(initDate,forHome) {
  let from = moment(initDate.format('YYYY-MM-DD')).add(1,'d');
  let baseYear = from.year();
  let afterFourWeek = moment(initDate.format('YYYY-MM-DD')).add(4, 'w');
  let allDays = [];
  let weekends = ['Friday', 'Saturday'];
  let byWeeks;
  console.log('forHome...',forHome);
  if(forHome) {
    while (from <= afterFourWeek) {
      allDays.push(moment(from.toDate()).add(1, 'd')); // adding new moment obj
      from.add(1, 'd');  // for exiting the loop
      if (from.format('dddd').includes(weekends[0])) {
        allDays.pop();
      }
    }
  } else {
    while (from <= afterFourWeek) {
      allDays.push(moment(from.toDate()).add(1, 'd')); // adding new moment obj
      from.add(1, 'd');  // for exiting the loop
      if (from.format('dddd').includes(weekends[0]) || from.format('dddd').includes(weekends[1])) {
        allDays.pop();
      }
    }
  }


  //console.log('FINAL ' ,allDays)
  byWeeks = _.groupBy(allDays, function (day) {
    return day.week();
  });

  //let flatten = _.toArray(byWeeks);
  console.log('by week ', byWeeks);
  return _.sortBy(_.toArray(byWeeks), days => days[0].year());
}

export function getTotalOfSingleDelivery(source) {
  return _.sumBy(source, function (m) {
    return Number(m.item.price) * m.quantity;
  });
}

// export function getDateName(firstDate) {
//   let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
//   //let d = new Date(dateString);
//   //let dayName = days[d.getDay()];
//
// }

//getDateAfterFourWeek

//moment().format('dddd');
