/**
 * Created by IamMohaiminul on 1/14/17.
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import { Link, browserHistory } from 'react-router';
import toastr from 'toastr';

class InputDataComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      height: 0,
      weight: 0,
      age: 0,
      gender: '',
      activityLevel: '',
      activityLevels: ['Sedentary', 'Little', 'Moderate', 'Heavy', 'Super Star'],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userProfile) {
      if (nextProps.userProfile.height
        && !_.isEqual(nextProps.userProfile.height, this.state.height)) {
        this.setState({ height: nextProps.userProfile.height });
      }

      if (nextProps.userProfile.weight
        && !_.isEqual(nextProps.userProfile.weight, this.state.weight)) {
        this.setState({ weight: nextProps.userProfile.weight });
      }

      if (nextProps.userProfile.age
        && !_.isEqual(nextProps.userProfile.age, this.state.age)) {
        this.setState({ age: nextProps.userProfile.age });
      }

      if (nextProps.userProfile.gender
        && !_.isEqual(nextProps.userProfile.gender, this.state.gender)) {
        this.setState({ gender: nextProps.userProfile.gender });
      }

      if (nextProps.userProfile.activityLevel
        && !_.isEqual(nextProps.userProfile.activityLevel, this.state.activityLevel)) {
        this.setState({ activityLevel: nextProps.userProfile.activityLevel });
      }
    }
  }

  render() {
    if (this.props.userProfile) {
      return (
        <div className='bmi-body'>
          <div className="header">
      			<h2>Step 1</h2>
      		</div>

          <div className="bmi-container">

            <div className="box">
              {/* LEFT-SIDE: FIRST TAB  */}
              <div className="box_tab active" id="view1">
      					<div className="title">Input your data</div>

      					<div className="image">
      						<div className="image1">
      							<img src="/images/client-image.svg" width="43" height="47" />
      						</div>
      					</div>

      					<div className="txt-align">
                  We will use this information to calculate your Body Mass Index and determine your daily callories need.
                </div>

      				</div>
            </div>

            <div className="box1">
      				<ul className="numbering">
      					<li className="active"><Link to="javascript:void(0);">1</Link></li>
      					<li><Link to="/bmi/result">2</Link></li>
      					<li><Link to="/bmi/recommendation">3</Link></li>
      				</ul>

              <div className="box1_tabs_container">
                {/* RIGHT-SIDE:  FIRST TAB */}
                <div className="box1_tab active">
      						<div className="measurement">
      							<table className="input_data">
                      <tbody>
        								<tr className="rows">
        									<td className="column_1">Height</td>
        									<td className="column_2">
        										<input
                              ref="height"
                              type="text"
                              className="measurement_input"
                              value={this.state.height}
                              onClick={this.clickHeight.bind(this)}
                              onChange={this.changeHeight.bind(this)}
                              placeholder="not set" />
        									</td>
        									<td className="column_3 effects">cm</td>
        								</tr>
        								<tr className="rows">
        									<td className="column_1">Weight</td>
        									<td className="column_2">
        										<input
                              ref="weight"
                              type="text"
                              className="measurement_input"
                              value={this.state.weight}
                              onClick={this.clickWeight.bind(this)}
                              onChange={this.changeWeight.bind(this)}
                              placeholder="not set" />
        									</td>
        									<td className="column_3 effects">kg</td>
        								</tr>
        								<tr className="rows">
        									<td className="column_1">Gender</td>
        									<td className="column_2">
        										<input
                              id="radio1"
                              name="gender"
                              type="radio"
                              value="Male"
                              checked={this.state.gender === 'Male'}
                              onChange={this.changeGender.bind(this)} />
        										<label htmlFor="radio1">Male</label>
        									</td>
        									<td className="column_3">
        										<input
                              id="radio3"
                              name="gender"
                              type="radio"
                              value="Female"
                              checked={this.state.gender === 'Female'}
                              onChange={this.changeGender.bind(this)} />
        										<label htmlFor="radio3">Female</label>
        									</td>
        								</tr>
        								<tr className="rows">
        									<td className="column_1">Age</td>
        									<td className="column_2">
                            <input
                              ref="age"
                              type="text"
                              className="measurement_input"
                              value={this.state.age}
                              onClick={this.clickAge.bind(this)}
                              onChange={this.changeAge.bind(this)}
                              placeholder="not set" />
                          </td>
        									<td className="column_3 effects">years old</td>
        								</tr>
        								<tr className="rows">
        									<td className="column_1">Activity Level</td>
        									<td className="column_2 divided" colSpan="2">
        										<select
                              ref="activityLevel"
                              className="activity_select"
                              value={this.state.activityLevel}
                              onChange={this.changeActivityLevel.bind(this)}>
                              <option value="">Please Select One...</option>
        											{this.renderActivityLevel()}
        										</select>
        									</td>
        								</tr>
                      </tbody>
      							</table>
      						</div>
      					</div>
              </div>

              <div className="both-buttons">
                <Link to="/" className="button-back nav-button">
                  <i className="fa fa-chevron-left" aria-hidden="true"></i> Back
                </Link>
                <Link
                  to="javascript:void(0);"
                  className="button-next nav-button enabled"
                  onClick={this.handleSubmit.bind(this)}>
                  Next <i className="fa fa-chevron-right" aria-hidden="true"></i>
                </Link>
              </div>

            </div>

          </div>
        </div>
      );
    } else {
      return (<p>Loading...</p>);
    }
  }

  renderActivityLevel() {
    return this.state.activityLevels.map((level) => {
      return (
        <option value={level} key={level}>{level}</option>
      );
    });
  }

  handleSubmit(event) {
    const _this = this;
    event.preventDefault();

    let userProfile = {
      height: this.state.height,
      weight: this.state.weight,
      age: this.state.age,
      gender: this.state.gender,
      activityLevel: this.state.activityLevel,
    };

    // Validations
    if (!userProfile.height || !parseFloat(userProfile.height) > 0) {
      toastr.warning('Height is required.', 'Body Mass Index');
      return false;
    }

    if (!userProfile.weight || !parseFloat(userProfile.weight) > 0) {
      toastr.warning('Weight is required.', 'Body Mass Index');
      return false;
    }

    if (!userProfile.age || !parseFloat(userProfile.age) > 0) {
      toastr.warning('Age is required.', 'Body Mass Index');
      return false;
    }

    if (!userProfile.gender) {
      toastr.warning('Gender is required.', 'Body Mass Index');
      return false;
    }

    if (!userProfile.activityLevel) {
      toastr.warning('Activity Level is required.', 'Body Mass Index');
      return false;
    }

    console.log('handleSubmit...', userProfile);
    this.props.updateUserProfile(userProfile, function (err, res) {
      if (err) {
        toastr.error(err.message, 'Body Mass Index');
      } else {
        //toastr.success('Update user profile successfully', 'Body Mass Index');
        browserHistory.push('/bmi/calculating');
      }
    });
  }

  clickHeight(event) {
    if (parseFloat(event.currentTarget.value) == 0) {
      ReactDOM.findDOMNode(this.refs.height).value = '';
    }
  }

  changeHeight(event) {
    this.setState({
      height: event.currentTarget.value,
    });
  }

  clickWeight(event) {
    if (parseFloat(event.currentTarget.value) == 0) {
      ReactDOM.findDOMNode(this.refs.weight).value = '';
    }
  }

  changeWeight(event) {
    this.setState({
      weight: event.currentTarget.value,
    });
  }

  changeGender(event) {
    this.setState({
      gender: event.currentTarget.value,
    });
  }

  clickAge(event) {
    if (parseFloat(event.currentTarget.value) == 0) {
      ReactDOM.findDOMNode(this.refs.age).value = '';
    }
  }

  changeAge(event) {
    this.setState({
      age: event.currentTarget.value,
    });
  }

  changeActivityLevel(event) {
    this.setState({
      activityLevel: event.currentTarget.value,
    });
  }
}

export default InputDataComponent;
