/**
 * Created by IamMohaiminul on 1/14/17.
 */
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import moment from 'moment';
import toastr from 'toastr';
import Rodal from 'rodal';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import _ from 'lodash';

import { getAllDaysOfNextMonth } from '../../libs';

import Slider from 'react-slick';

export default class RecommendationComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedKcal: 0.00,
      mealPlanIndex: -1,
      visible: false,
      mealPlan: {},
    };
  }

  componentWillMount() {
    this.props.saveAllDays(getAllDaysOfNextMonth(moment(),true));
  }

  render() {
    if (!_.isEmpty(this.props.suggestions)) {
      let userRecommend = localStorage.getItem('userRecommend');
      let dailyCalorie = parseFloat(this.props.suggestions.statistic.dailyCalorieAllowance.value);
      let dailyAllowance = 0;
      let weeklyAllowance = 0;
      if (userRecommend === 'Lose') {
        dailyAllowance = dailyCalorie - 500;
        weeklyAllowance = (dailyCalorie - 500) * 7;
      } else if (userRecommend === 'Gain') {
        dailyAllowance = dailyCalorie + 500;
        weeklyAllowance = (dailyCalorie + 500) * 7;
      } else {
        dailyAllowance = dailyCalorie;
        weeklyAllowance = dailyCalorie * 7;
      }
    let _this = this;
    let settings = {
      dots: false,
      infinite: false,
      speed: 250,
      slidesToShow: 1,
      slidesToScroll: 2,
      arrows: true
    };
      return (
        <div className='bmi-body'>
          <div className="header">
            <h2>Step 3</h2>
          </div>

          <div className="bmi-container">

            <div className="box">
              {/* LEFT-SIDE: THIRD TAB  */}
              <div className="box_tab active" id="view3">
                <div className="title">Recommendation</div>
                <p className="gd">
                  To {userRecommend.toLowerCase()} your current weight you need:
                </p>
                <ul className="kcal-area">
                  <li>
                    <span className="number-kcal">{dailyAllowance.toFixed(2)}</span>
                    <small className="kcal-lft">Kcal left</small>
                    <span className="kcal-day">DAILY</span>
                  </li>
                  {/*<li>
                    <span className="number-kcal">{weeklyAllowance.toFixed(2)}</span>
                    <small className="kcal-lft">Kcal left</small>
                    <span className="kcal-day">THIS WEEK</span>
                  </li>*/}
                </ul>
                <p className="meal-desc">
                  Try one of the recommended meal plans to meet your calorie requirements and reach your goal
                </p>
                <Link to="/" className="go-home-btn">Go to home</Link>
              </div>
            </div>

            <div className="box1">
              <ul className="numbering">
                <li><Link to="/bmi">1</Link></li>
      					<li><Link to="/bmi/result">2</Link></li>
      					<li className="active"><Link to="javascript:void(0);">3</Link></li>
              </ul>

              <div className="box1_tabs_container">
                {/* RIGHT-SIDE:  SECOND TAB */}
                <div className="box1_tab active">

                  <ul className="tabs responsivetabs">
      							<li><a href="javascript:void(0);" rel="tab-1">Meal Plan</a></li>
      						</ul>

                  <div id="tab-1" className="tabs_content active">
                    <div className="food-slider">
                      <div className="content_6 contenthr-slider">
                        <div className="images_container">
                          {this.renderMealPlans()}
                        </div>
                      </div>
                      <div className="total-box">{this.state.selectedKcal} Kcal selected</div>
                    </div>
                  </div>

                </div>
              </div>

              <div className="both-buttons">
                <Link to="/bmi/result" className="button-back nav-button">
                  <i className="fa fa-chevron-left" aria-hidden="true"></i> Back
                </Link>
                <Link
                  to="javascript:void(0);"
                  className="button-next nav-button enabled"
                  onClick={this.handleRedirect.bind(this)}>
                  Add to box
                </Link>
              </div>

            </div>

          </div>

          <Rodal visible={this.state.visible} onClose={this.toggleRodal.bind(this)}>
            <div className='bmi-modal-content'>
              <div className='bmi-modal-header'>
                <h3 className='bmi-modal-heading'>
                  Caffeine Diet Plan
                </h3>
                <p className='bmi-modal-tagline'>
                  by Akly Expert Team
                </p>
              </div>
              <div className='bmi-modal-meals'>
                {/*
                tabs are dicativated as there are not enough foods.
                <div className='bmi-modal-tabs'>
                  <Tabs>
                    <TabList>
                      <Tab>Week 1</Tab>
                      <Tab>Week 2</Tab>
                      <Tab>Week 3</Tab>
                      <Tab>Week 4</Tab>
                    </TabList>
                    <TabPanel>
                        <h1>Week 1</h1>
                    </TabPanel>
                    <TabPanel>
                        <h1>Week 2</h1>
                    </TabPanel>
                    <TabPanel>
                        <h1>Week 3</h1>
                    </TabPanel>
                    <TabPanel>
                        <h1>Week 4</h1>
                    </TabPanel>
                  </Tabs>
                </div>
                */}
                <div className='bmi-modal-dayblock-wrapper'>
                  <Slider {...settings}>
                    {_this.renderDays(_this.state.mealPlan)}
                  </Slider>
                </div>
              </div>
              <div className='bmi-modal-closebtn'>
                <button onClick={this.toggleRodal.bind(this)}>Close</button>
              </div>

            </div>
          </Rodal>

        </div>
      );
    } else {
      browserHistory.push('/bmi/result');
      return (<p>Loading...</p>);
    }
  }

  renderMealPlans() {
    let mealPlans = this.props.suggestions.mealPlans;
    if (mealPlans.length > 0) {
      return mealPlans.map((mealPlan, index) => {
        mealPlan.index = index;
        let totalPrice = 0;
        mealPlan.plans.map(item =>{
            item.foods.map(food_item =>{
              totalPrice += food_item.item.price
            })
        });
        return (
          <div
            key={mealPlan._id}
            className={this.state.mealPlanIndex === index ?
              'itembox selected' : 'itembox'}
            onClick={this.selectMealPlan.bind(this, mealPlan, index)}>
            <div className="img_container">
              <button className="finfo" onClick={this.toggleRodal.bind(this, mealPlan)}>
                <img src="/images/food-card/info.svg" alt="Food information"/>
              </button>
              <div className="food_image">
                <img src="/images/food-1.png" className="img-responsive" />
              </div>
              <div className="circle_block">
                <img src="/images/flame_white.svg" className="cal_image" alt="" />
                <div className="cal_number">{mealPlan.calorie}</div>
                <div className="cal_text">Kcal/day</div>
              </div>
            </div>
            <div className="title">
              Total Price : {totalPrice}/week<br/>{mealPlan.name}<br/>{mealPlan.description}<br/>
            </div>
          </div>
        );
      });
    } else {
      return (
        <h4 className="text-center">
          No meal plans where found in your recommended calorie range.
        </h4>
      );
    }
  }

  selectMealPlan(mealPlan, event) {
    this.setState({
      selectedKcal: mealPlan.calorie,
      mealPlanIndex: mealPlan.index,
    });
    console.log('--->>>>',mealPlan)
    this.props.addToBox(mealPlan);
  }

  toggleRodal(mealPlan = {}, event) {
    this.setState({
      visible: !this.state.visible,
      mealPlan: mealPlan,
    });
  }

  renderDays() {
    if (!_.isEmpty(this.state.mealPlan)) {
      const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday','friday'];
      return days.map((day) => {
        let plans = _.filter(this.state.mealPlan.plans, function (plan) {
          return plan.day == day;
        });

        return (
          <div {...this.props}>
            <div className='bmi-modal-dayblock' key={day}>
                <h4>{_.capitalize(day)}</h4>
                <div className='bmi-modal-dayblock-meals'>
                  {this.renderFoods(plans)}
              </div>
            </div>
          </div>
        );
      });
    } else {
      return (<div></div>);
    }
  }

  renderFoods(plans) {
    return plans.map((plan) => {
      return plan.foods.map((food) => {
        return (
          <div className='bmi-modal-meal-wrapper'>
           <div className='bmi-modal-meal'>
              <div className='bmi-modal-mealcontent'>
                <div className='bmi-modal-mealimg'>
                  <img src={food.item.images[0]} alt={food.item.name}/>
                </div>
                <p className='bmi-modal-mealname'>
                  {food.item.name} ({_.capitalize(plan.slot)})
                </p>
              </div>
            </div>
          </div>
        );
      });
    });
  }

  handleRedirect(event) {
    const _this = this;
    event.preventDefault();

    if (this.state.mealPlanIndex < 0) {
      toastr.warning('Need to select a meal plan.', 'Body Mass Index');
    } else {
      browserHistory.push('/summary');
    }
  }
}
