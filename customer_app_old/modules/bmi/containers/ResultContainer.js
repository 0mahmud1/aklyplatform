/**
 * Created by IamMohaiminul on 1/15/17.
 */
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import toastr from 'toastr';

import ResultComponent from '../components/ResultComponent';

import { getBodyMassIndex } from '../actions/getBodyMassIndex';
import { getSuggestions } from '../actions/getSuggestions';

class ResultContainer extends Component {
  componentWillMount() {
    this.props.getBodyMassIndex(function (err, res) {
      if (err) {
        toastr.error(err.message, 'Body Mass Index');
        browserHistory.push('/bmi');
      } else {
        //toastr.success('Fetch body mass index successfully', 'Body Mass Index');
      }
    });
  }

  render() {
    return (
      <ResultComponent
        bodyMassIndex={this.props.bodyMassIndex}
        getSuggestions={this.props.getSuggestions} />
    );
  }
}

// Get apps store and pass it as props to ResultContainer
//  > whenever store changes, the ResultContainer will automatically re-render
// "store.bodyMassIndex" is set in reducers.js
function mapStateToProps(store) {
  return {
    bodyMassIndex: store.bodyMassIndex,
  };
}

// Get actions and pass them as props to to InputDataContainer
//  > now InputDataContainer has this.props.getBodyMassIndex
//  > now InputDataContainer has this.props.getSuggestions
function matchDispatchToProps(dispatch) {
  return bindActionCreators({
    getBodyMassIndex: getBodyMassIndex,
    getSuggestions: getSuggestions,
  }, dispatch);
}

// We don't want to return the plain ResultContainer (component) anymore,
// we want to return the smart Container
//  > ResultContainer is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(ResultContainer);
