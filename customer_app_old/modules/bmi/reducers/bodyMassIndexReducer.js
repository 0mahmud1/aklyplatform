/**
 * Created by IamMohaiminul on 1/15/17.
 */

/*
* This reducer will always return an array of users no matter what
* You need to return something, so if there are no users then just return an empty array
*/
export const bodyMassIndexReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_BODY_MASS_INDEX_REQUEST':
      return state;
    case 'FETCH_BODY_MASS_INDEX_FAILURE':
      return state;
    case 'FETCH_BODY_MASS_INDEX_SUCCESS':
      return action.payload.data;
    default:
      return state;
  };
};
