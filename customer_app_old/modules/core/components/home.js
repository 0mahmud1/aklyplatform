'use strict'

import React, { Component } from 'react'

export default class HomeComponentCA extends Component {

	render() {
		return (
       <div className='home-wrapper container ca-container' style={{'marginTop': 40+'px'}}>
         <div className='row'>
           <div className='col-lg-12'>
             <div className='home-img'>
               <img src='/images/meal_home.jpg' alt='Home Image' style={{'height': 100+'%', 'width':100+'%'}}/>
             </div>
           </div>
         </div>
       </div>
		)
	}
}
