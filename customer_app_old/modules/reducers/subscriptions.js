import _ from 'lodash';
import moment from 'moment';
import { getTotalOfSingleDelivery } from '../libs';

const subscriptionEdit = (state = {}, action) => {
  let delivery;
  let meals;
  let selectedDate;
  let deliveries;
  switch (action.type) {
    case 'GET_DELIVERIES_FOR_UPDATE_RESOLVED':
      return Object.assign({}, state, { deliveries: action.payload });

    case 'CREATE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED':
      deliveries = [...state.deliveries];
      deliveries.push(action.payload);
      return Object.assign({}, state, { deliveries: deliveries, selectedDelivery: null });

    case 'UPDATE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED':
      deliveries = [...state.deliveries];
      let index = _.findIndex(deliveries, function (db) {
        return db._id === action.payload._id;
      });

      console.log('index: ', index);
      if (index == -1)
        return state;

      deliveries[index] = action.payload;
      return Object.assign({}, state, { deliveries: deliveries, selectedDelivery: null });

    case 'SELECT_SPECIFIC_DELIVERY':
      return Object.assign({}, state, { selectedDelivery: action.payload });

    case 'SELECT_DAY_FOR_NEW_DELIVERY':
      state.selectedDelivery.deliveryDate = action.payload;
      return Object.assign({}, state, { selectedDelivery: state.selectedDelivery });

    case 'ADD_FOOD_DELIVERY_EDIT':
      return state;

    case 'ADD_FOOD_DELIVERY_EDIT_TEMP':
      let tempDelivery = Object.assign({}, state.selectedDelivery);
      if (!tempDelivery.meals) {
        tempDelivery.meals = [];
        tempDelivery.meals.push({ item: action.payload, quantity: 1 });
        tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
        return Object.assign({}, state, { selectedDelivery: tempDelivery });
      }

      let tempMealIndex = _.findIndex(tempDelivery.meals, function (o) {
        return o.item._id === action.payload._id;
      });

      console.log('index: ', tempMealIndex);
      if (tempMealIndex == -1) {
        tempDelivery.meals.push({ item: action.payload, quantity: 1 });
        tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
        return Object.assign({}, state, { selectedDelivery: tempDelivery });
      }

      console.log('index: ', tempDelivery.meals[tempMealIndex]);
      tempDelivery.meals[tempMealIndex].quantity += 1;
      tempDelivery.total = getTotalOfSingleDelivery(tempDelivery.meals);
      return Object.assign({}, state, { selectedDelivery: tempDelivery });

    case 'REDUCE_FOOD_DELIVERY_EDIT_TEMP':
      let tmpDeliveryReduce = Object.assign({}, state.selectedDelivery);
      let tmpDeliveryMealsReduce = [...tmpDeliveryReduce.meals];
      let tmpMealIndexReduce = _.findIndex(tmpDeliveryMealsReduce, function (o) {
        return o.item._id === action.payload._id;
      });

      if (tmpDeliveryMealsReduce[tmpMealIndexReduce].quantity == 1) {
        console.log('shd delete item');
        tmpDeliveryMealsReduce.splice(tmpMealIndexReduce, 1);
        console.log(',, ', tmpDeliveryMealsReduce);
        tmpDeliveryReduce.meals = tmpDeliveryMealsReduce;
      } else {
        tmpDeliveryMealsReduce[tmpMealIndexReduce].quantity -= 1;
        tmpDeliveryReduce.meals = tmpDeliveryMealsReduce;
      }

      tmpDeliveryReduce.total = getTotalOfSingleDelivery(tmpDeliveryReduce.meals);

      return Object.assign({}, state, { selectedDelivery: tmpDeliveryReduce });

    case 'RESET_SELECTED_DELIVERY':
      return Object.assign({}, state, { selectedDelivery: null });

    // case 'CREATE_DELIVERY_IN_UPDATE_SUBSCRIPTION_RESOLVED':
    //   state.deliveries.push(action.payload)

      return Object.assign({}, state, { deliveries: state.deliveries });

    default:
      return state;
  }
};

export default subscriptionEdit;
