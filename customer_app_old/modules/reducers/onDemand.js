import _ from 'lodash';
import moment from 'moment';
import { getTotalOfSingleDelivery } from '../libs';

const formattedCart = localStorage.getItem('onDemand') ? JSON.parse(localStorage.getItem('onDemand')) : null;

const defaultState = {
  deliveries: localStorage.getItem('onDemand') ? [...formattedCart] : null,
  total: localStorage.getItem('onDemand') ? getTotalOfSingleDelivery([...formattedCart]) : 0,
};

const onDemand = (state = defaultState, action) => {
  let deliveries;
  let foodIndex;
  let total;
  switch (action.type) {
    case 'ADD_FOOD_CART':
      if (!state.deliveries) {
        deliveries = [];
        deliveries.push({ item: action.payload, quantity: 1 });
        total = getTotalOfSingleDelivery(deliveries);
        localStorage.setItem('onDemand', JSON.stringify(deliveries));
        return Object.assign({}, state, { deliveries: deliveries, total: total });
      }

      deliveries = [...state.deliveries];
      foodIndex = _.findIndex(state.deliveries, function (o) {
        return o.item._id === action.payload._id;
      });

      //console.log('index: ', foodIndex)
      if (foodIndex == -1) {
        deliveries.push({ item: action.payload, quantity: 1 });
        total = getTotalOfSingleDelivery(deliveries);
        localStorage.setItem('onDemand', JSON.stringify(deliveries));
        return Object.assign({}, state, { deliveries: deliveries, total: total });
      }

      console.log('index: ', deliveries[foodIndex]);
      deliveries[foodIndex].quantity += 1;
      total = getTotalOfSingleDelivery(deliveries);
      localStorage.setItem('onDemand', JSON.stringify(deliveries));
      return Object.assign({}, state, { deliveries: deliveries, total: total });

    case 'REDUCE_FOOD_QTY_CART':
      deliveries = [...state.deliveries];
      foodIndex = _.findIndex(deliveries, function (o) {
        return o.item._id === action.payload._id;
      });

      if (deliveries[foodIndex].quantity == 1) {
        console.log('shd delete item');
        deliveries.splice(foodIndex, 1);
        console.log(',, ', foodIndex);
      } else {
        deliveries[foodIndex].quantity -= 1;
      }

      total = getTotalOfSingleDelivery(deliveries);
      localStorage.setItem('onDemand', JSON.stringify(deliveries));
      return Object.assign({}, state, { deliveries: deliveries, total: total });

    case 'CHANGE_FOOD_QTY_CART':
      deliveries = [...state.deliveries];
      foodIndex = _.findIndex(deliveries, function (o) {
        return o.item._id === action.payload.food._id;
      });

      deliveries[foodIndex].quantity = action.payload.quantity;

      total = getTotalOfSingleDelivery(deliveries);
      localStorage.setItem('onDemand', JSON.stringify(deliveries));
      return Object.assign({}, state, { deliveries: deliveries, total: total });

    case 'REMOVE_ITEM_CART':
      deliveries = [...state.deliveries];
      foodIndex = _.findIndex(deliveries, function (o) {
        return o.item._id === action.payload._id;
      });

      deliveries.splice(foodIndex, 1);
      total = getTotalOfSingleDelivery(deliveries);
      localStorage.setItem('onDemand', JSON.stringify(deliveries));
      return Object.assign({}, state, { deliveries: deliveries, total: total });

    case 'CLEAR_ONDEMAND_DELIVERY':
      localStorage.removeItem('onDemand');
      return Object.assign({}, state, { deliveries: [], total: 0 });

    default:
      return state;
  }
};

export default onDemand;
