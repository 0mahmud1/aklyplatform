import axios from 'axios';
import toastr from 'toastr';

export function getMealPlans(planType) {
  return function (dispatch) {
    dispatch({ type: 'anything' });
    axios.get(
      'api/meal-plans',{
        params: {
          planType: planType,
        },
      }
    ).then((response) => {
      console.log('get meal plans... : ', response.data);
      dispatch({ type: 'GET_MEALPLAN_RESOLVED', payload: response.data });
      //toastr.success('MealPlans Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_MEALPLAN_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function readMealPlanAction(_id) {
  return function (dispatch) {
    dispatch({ type: 'READ_MEALPLAN_PENDING' });
    axios.get(('api/meal-plans/' + _id))
      .then((response) => {
        dispatch({ type: 'READ_MEALPLAN_RESOLVED', payload: response.data.data });
      })
      .catch((err) => {
        dispatch({ type: 'READ_MEALPLAN_REJECTED', payload: err });
        console.warn('Read mealPlanAction: ', err);
      });
  };
}

export function getFoods() {
  return function (dispatch) {
    dispatch({ type: 'GET_FOOD_PENDING' });
    axios.get(
      'api/foods'
    ).then((response) => {
      console.log('get food... : ', response);
      dispatch({ type: 'GET_FOOD_RESOLVED', payload: response });
      //toastr.success('Food List Loaded');
    })
    .catch((err) => {
      dispatch({ type: 'GET_FOOD_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}
