import axios from 'axios';
import toastr from 'toastr';

export function getTags() {
  return function (dispatch) {
    dispatch({ type: 'GET_TAGS_PENDING' });
    axios.get(
      'api/tags'
    ).then((response) => {
      dispatch({ type: 'GET_TAGS_RESOLVED', payload: response });
      //toastr.success('tags List Loaded');
    }).catch((err) => {
      dispatch({ type: 'GET_TAGS_REJECTED', payload: err });
      toastr.error(err);
    });
  };
}

export function getZones() {
  return function (dispatch) {
    return axios.get(
      'api/zones'
    ).then((response) => {
      dispatch({ type: 'GET_HUB_RESOLVED', payload: response.data.data });
      return response.data.data;
    }).catch((err) => {
      dispatch({ type: 'GET_HUB_REJECTED', payload: err });
    });
  };
}

export function sendSupportQuery(supportQuery, cb) {
  // REDUCER IN DASHBOARD MODULES/authenticate folder
  return function (dispatch) {
    axios.post(
      'api/support',
      supportQuery
    ).then((response) => {
      console.log('frm dispatch ', response);
      if (cb) {
        cb(null,response);
      }
    }).catch((err) => {
      console.log('err , ', err);
      cb(err,null);
    });
  };
}
