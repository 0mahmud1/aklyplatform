import os
import json
from pprint import pprint
import fileinput
from os import listdir
from os.path import isfile, join

script_dir = os.path.dirname(__file__)
build_dir_file_path = os.path.join(script_dir, 'build/')
sample_html_file_path = os.path.join(script_dir, 'sample.index.html')
html_file_path = os.path.join(script_dir, 'index.html')

# list all files in build dir
onlyfiles = [f for f in listdir(build_dir_file_path) if isfile(join(build_dir_file_path, f))]

hash_key = None
for build_file in onlyfiles:
    print build_file
    if build_file.find('dashboard') != -1:
        # find the hash_key from build dir
        hash_key = build_file.replace('dashboard.', '').replace('.css', '').replace('.js', '').replace('.map', '')
        break

# create a new index file from sample index html file
if hash_key:
    filedata = None
    with open(sample_html_file_path, 'r') as file :
      filedata = file.read()

    # Replace the target string
    filedata = filedata.replace('[hash]', hash_key)

    # Write the file out again
    with open(html_file_path, 'w') as file:
      file.write(filedata)
